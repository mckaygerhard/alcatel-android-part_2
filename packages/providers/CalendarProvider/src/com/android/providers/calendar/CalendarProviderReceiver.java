
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 18/12/2015|     kang.yu        PR  1047716 |                  */
/*    | |[Contacts ]Contact special date can not sync with calendar after change it*/
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.providers.calendar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.android.providers.calendar.CalendarDatabaseHelper.Tables;

import java.lang.Exception;

import android.database.DatabaseUtils;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;

public class CalendarProviderReceiver extends BroadcastReceiver {
    private static final String TAG = "CalendarProviderReceiver";
    private final int MESSAGE_EVENT_INSERT = 0;
    private final int MESSAGE_EVENT_UPDATE1 = 1;
    private final int MESSAGE_EVENT_UPDATE2 = 2;
    private final int MESSAGE_EVENT_DELETE1 = 3;
    private final int MESSAGE_EVENT_DELETE2 = 4;
    @Override
    public void onReceive(Context context, Intent intent) {
        int flag = intent.getIntExtra("flag", 0);
        Log.d(TAG, "flag=" + flag);
        switch (flag) {
            case MESSAGE_EVENT_INSERT:
                try {
                    ContentValues values = intent.getParcelableExtra("values");
                    context.getContentResolver().insert(Events.CONTENT_URI, values);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                break;
            case MESSAGE_EVENT_UPDATE1:
                long eventId = intent.getLongExtra("contact_data_id", 0);
                long date = intent.getLongExtra("date", 0);
                int eventType = intent.getIntExtra("eventType", 100);
                String eventName = intent.getStringExtra("eventName");
                String name = intent.getStringExtra("name");
                Cursor mCursor = null;
                try {
                    mCursor = context.getContentResolver().query(Events.CONTENT_URI, null,
                            "contact_data_id =" + eventId, null, null);
                    if (mCursor != null && mCursor.getCount() > 0) {
                        ContentValues updateValues = new ContentValues();
                        if (date != 0) {
                            updateValues.put(Events.DTSTART, date);
                        }
                        updateValues.put(Events.TITLE, name + " " + eventName);
                        if (eventType != 100) {
                            updateValues.put("contactEventType", eventType);
                        }
                        context.getContentResolver().update(Events.CONTENT_URI, updateValues,
                                "contact_data_id =" + eventId, null);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                } finally {
                    if (mCursor!=null) {
                        mCursor.close();
                    }
                }
                break;
            case MESSAGE_EVENT_UPDATE2:
                Cursor calendarcursor = null;
                String lastInsertName = intent.getStringExtra("displayName");
                long contact_id = intent.getLongExtra("contact_id", 0);
                try {
                    calendarcursor = context.getContentResolver().query(Events.CONTENT_URI, null,
                            "contact_id =" + contact_id, null, null);
                    while (null != calendarcursor && calendarcursor.moveToNext()) {
                        // Load the event into a ContentValues object.
                        ContentValues oldValues = new ContentValues();
                        ContentValues updateValues = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(calendarcursor, oldValues);
                        String title = oldValues.get(Events.TITLE).toString();
                        Log.e(TAG, "title    = " + title);
                        String id = oldValues.get(Events._ID).toString();
                        updateValues.put(Events._ID, id);
                        String leftTtile = title.substring(0, title.lastIndexOf(" "));
                        String rightTtile = title.substring(title.lastIndexOf(" "));
                        //[FEATURE]-ADD-BEGIN by TCTNB.(Huan_Liu),02/16/2016,Defect  1542213 ,
                        if (lastInsertName.equals("")) {
                            lastInsertName = context.getString(R.string.my_birthday);
                        }
                        //[FEATURE]-ADD-END by TCTNB.(Huan_Liu)
                        updateValues.put(Events.TITLE, lastInsertName + " " + rightTtile);
                        context.getContentResolver().update(Events.CONTENT_URI, updateValues,
                                "_id =" + id, null);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                } finally {
                    if (calendarcursor != null) {
                        calendarcursor.close();
                    }
                }
                break;
            case MESSAGE_EVENT_DELETE1:
                String temp = intent.getStringExtra("temp");
                int deleteCount = 0;
                try {
                    if (temp.equals("1")) {
                        int contact_data_id = intent.getIntExtra("contact_data_id", 0);
                        deleteCount = context.getContentResolver().delete(Events.CONTENT_URI,
                                "contact_data_id =" + contact_data_id, null);
                    } else if (temp.equals("2")) {
                        String id = intent.getStringExtra("id");
                        Log.d(TAG, "CalendarProviderReceiver----id========="+id);
                        if (id != null && id != "") {
                            deleteCount = context.getContentResolver().delete(Events.CONTENT_URI, "contact_id =" + id,
                                    null);
                        }
                    }
                    Log.d(TAG, "deleteCount ===== " + deleteCount);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                break;
            case MESSAGE_EVENT_DELETE2:
                try {
                    String str = intent.getStringExtra("delete_selection");
                    String subStr= str.substring(str.indexOf("(")+1, str.indexOf(")"));
                    String[] deleteId = subStr.split(",");
                    String id = null;
                    for (int i = 0; i < deleteId.length; i++) {
                        id = deleteId[i];
                        context.getContentResolver().delete(Events.CONTENT_URI, "contact_id =" + id,
                                null);
                    }
                }catch (Exception e){
                    Log.e(TAG,e.getMessage());
                }
            default:
                break;
        }
    }
}


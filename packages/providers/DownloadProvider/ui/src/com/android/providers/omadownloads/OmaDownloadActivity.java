/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiushou.Liu                                                      */
/*  Email  :  Qiushou.Liu@tcl-mobile.com                                       */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/04/2014|qiushou.liu           |     FR736884         |[HOMO][Orange][28]*/
/*           |                      |                      | 11 - DOWNLOAD_01 */
/* ----------|----------------------|----------------------|----------------- */
/* 03/16/2015|youcai.chen           |     PR940050         |[SMC][media]andro */
/*           |                      |                      |id.process.media  */
/*           |                      |                      |happen crash due  */
/*           |                      |                      |to java.lang.Ille */
/*           |                      |                      |galStateException */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.providers.downloads.ui.omadownloads;

import static android.app.DownloadManager.COLUMN_LOCAL_URI;//[BUGFIX]-Add by TCTNB.Rui.Liu,02/27/2014,608067,
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ExpandableListActivity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.provider.Downloads;
import android.util.TctLog;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,01/08/2014,583429,
import com.android.providers.downloads.Constants;
import com.android.providers.downloads.OpenHelper;
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
import com.android.providers.downloads.ui.R;

import java.io.File;
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,02/27/2014,608067,
import java.io.FileNotFoundException;
import java.io.IOException;
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
import android.net.WebAddress;
import android.util.TctLog;
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/21/2013,556045,
//Can't view download files in download list
import com.android.providers.downloads.ui.omadownloads.DateSortedExpandableListAdapter;
import android.content.ActivityNotFoundException;
//[BUGFIX]-Add-END by TCTNB.Rui.Liu

//[DUGFIX]-Add-BEGIN by TCTNJ.qingfu.su,12/23/2013,PR-560723
import android.database.DataSetObserver;
//[DUGFIX]-Add-END by TCTNJ.qingfu.su
/**
 * Class OmaDownloadActivity
 */
public class OmaDownloadActivity extends ExpandableListActivity {
    /** the log of the OmaDownloadActivity */
    private static final String TAG = "OmaDownloadActivity";

    /** the view of ExpandableList */
    private ExpandableListView mListView;

    /** the cursor */
    private Cursor mDownloadCursor;
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/21/2013,556045,
    private Cursor mDateSortedCursor;
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
    /** the adapter of the omaDownload */
    private OmaDownloadAdapter mDownloadAdapter;

    /** the status of the OmaDownload */
    private int mStatusColumnId;

    /** the item id of the OmaDownloadActivity*/
    private int mIdColumnId;

    /** the item id of the title */
    private int mTitleColumnId;

    /** the position of the current contextMenu */
    private long mContextMenuPosition;

    private DownloadsChangeObserver mDownloadObserver;

    /** the activity title */
    private final String mTitle = "";
    private RelativeLayout relative;

    /** the id of InstallNotifyUri */
    private int mInstallNotifyUriId;

    /** installNotifyUri for media object */
    private String mInstallNotifyURI;

    /** the nextUrl for media object */
    private String mNextUrl;
    private String objUrl;

    private final boolean mismatchTypeId = false;
    private final boolean mismatchSizeId = false;
    private final boolean urlValidId = false;

    /** the new object uri in db */
    private Uri uri;

    /** which group will show */
    int groupToShow;

    private View mSelectedView;

    /** download flag */
    private boolean flag = true;

    /** the constant of nextUrl */
    public static final String NEXT_URL = "nextURL";

    /** the constant of installNotifyUrl */
    public static final String INSTALL_NOTIFY_URL = "installNotifyURL";

//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/21/2013,556045,
    private int  mLocalUriColumnId ;
    private int mMediaTypeColumnId ;
    private Uri viewUri ;
    private String mimeType ;
//[BUGFIX]-Add-END by TCTNB.Rui.Liu

    //[DUGFIX]-Add-BEGIN by TCTNJ.qingfu.su,12/23/2013,PR-560723
    private MyDataSetObserver mDataSetObserver = new MyDataSetObserver();
    //[DUGFIX]-Add-END by TCTNJ.qingfu.su
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDownloadObserver = new DownloadsChangeObserver(Downloads.Impl.CONTENT_URI);
        doInit(getIntent());
        //[DUGFIX]-Add-BEGIN by TCTNJ.qingfu.su,12/23/2013,PR-560723
        mDownloadCursor.registerDataSetObserver(mDataSetObserver);
        //[DUGFIX]-Add-END by TCTNJ.qingfu.su
        // finish();
        mContext = this;
    }
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,03/14/2014,617179,CHECK_DD_DA_2 - nexturl.
      @Override
      protected void onNewIntent(Intent intent) {
          mNextUrl = intent.getStringExtra(NEXT_URL);
          TctLog.i("OmaDownloadActivity","DownloadActivity onNewIntnet mNextUrl = "+mNextUrl);
          mInstallNotifyURI = intent.getStringExtra(INSTALL_NOTIFY_URL);
      }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu


    private void doInit(Intent aIntent) {
        setContentView(R.layout.oma_downloads_page);
//        Intent mIntent = getIntent();

        // if aIntent is not null, get the nextUrl and InstallNotifyUri from aIntent
        if(aIntent != null) {
            mNextUrl = aIntent.getStringExtra(NEXT_URL);
            mInstallNotifyURI = aIntent.getStringExtra(INSTALL_NOTIFY_URL);
        }

        // set title
        //setTitle(getText(R.string.download_title));

        mListView = (ExpandableListView) findViewById(android.R.id.list);
        mListView.setEmptyView(findViewById(R.id.empty));

        setListViewValues();

        startDownload(aIntent);
    }


    private void setListViewValues() {
        try{
            if(null != mDownloadCursor){
                mDownloadCursor.close();
            }
        }catch(SQLiteException e){
            TctLog.e(TAG,"SQLiteException = "+e);
        }finally{
            TctLog.d(TAG,"mDownloadCursor has been close..");
        }
         mDownloadCursor = getContentResolver().query(Downloads.Impl.CONTENT_URI,
                new String[] { Downloads.Impl._ID, Downloads.Impl.COLUMN_TITLE,
                    Downloads.Impl.COLUMN_STATUS,
                     Downloads.Impl.COLUMN_TOTAL_BYTES,
                     Downloads.Impl.COLUMN_CURRENT_BYTES,
                     Downloads.Impl.COLUMN_DESCRIPTION,
                     Downloads.Impl.COLUMN_NOTIFICATION_PACKAGE,
                     Downloads.Impl.COLUMN_LAST_MODIFICATION,
                     Downloads.Impl.COLUMN_VISIBILITY, Downloads.Impl._DATA,
                     Downloads.Impl.COLUMN_MIME_TYPE,
                 }, null,null,Downloads.Impl.COLUMN_LAST_MODIFICATION + " DESC");

         // only attach everything to the listbox if we can access
         // the download database. Otherwise, just show it empty
         if (mDownloadCursor != null) {
             mStatusColumnId = mDownloadCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_STATUS);
             mIdColumnId = mDownloadCursor.getColumnIndexOrThrow(Downloads.Impl._ID);
             mTitleColumnId = mDownloadCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_TITLE);
             // Create a list "controller" for the data
             mDownloadAdapter = new OmaDownloadAdapter(this, mDownloadCursor,
                     mDownloadCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_LAST_MODIFICATION));

             TctLog.d("******** OmaDownloadAcitvity **mDownloadAdapter**", "" + mDownloadAdapter);
             setListAdapter(mDownloadAdapter);

             mListView.expandGroup(0);
             // mListView.setAdapter(mDownloadAdapter);
             mListView.setOnCreateContextMenuListener(this);
         }
    }

    //[DUGFIX]-Add-BEGIN by TCTNJ.qingfu.su,12/23/2013,PR-560723
    private class MyDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            if (mDownloadCursor != null && mDownloadCursor.getCount() > 1) {
                return;
            }
            // may need to switch to or from the empty view
            ensureSomeGroupIsExpanded();
        }
    }

    /**
     * If no group is expanded in the date-sorted list, expand the first one.
     */
    private void ensureSomeGroupIsExpanded() {
        mListView.post(new Runnable() {
            public void run() {
                if (mDownloadAdapter != null && mDownloadAdapter.getGroupCount() == 0) {
                    return;
                }
                for (int group = 0; group < mDownloadAdapter.getGroupCount(); group++) {
                    if (mListView.isGroupExpanded(group)) {
                        return;
                    }
                }
                mListView.expandGroup(0);
            }
        });
    }
    //[DUGFIX]-Add-END by TCTNJ.qingfu.su

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mNextUrl != null && flag) {
                Cursor cursor = getContentResolver().query(Downloads.Impl.CONTENT_URI,
                        new String[] { Downloads.Impl.COLUMN_STATUS }, null, null, null);

              //[BUGFIX]-MOD-BEGIN by TCTNB.Zexiang.Qi,20/01/2014, PR-591234 cursor not closed
              try {
                if (cursor != null && cursor.getCount() >= 1) {
                    int flagStatus = 0;

                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        int statusId = cursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_STATUS);
                        flagStatus = cursor.getInt(statusId);
                    }

                    if (flagStatus == Downloads.Impl.STATUS_SUCCESS) {
                        new Thread(new Runnable() {
                            public void run() {
                                Uri uri = Uri.parse(mNextUrl);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.putExtra("OmaDownloadback", true);//PR285618-ChuanCheng-001
                                startActivity(intent);
                            }
                        }).start();
                        flag = false;
                        return true;
                    }
                }
              } finally {
              if (cursor != null) {
                 cursor.close();
              }
              }
              //[BUGFIX]-MOD-END by TCTNB.Zexiang.QI,20/01/2014, PR-591234
            }
        }
      //[BUGFIX]-Add-BEGIN by TSCD.youcai.chen,03/16/2015,PR-940050
        try {
            return super.onKeyUp(keyCode, event);
        } catch (IllegalStateException e) {
            //avoid crashing if getting this under onSaveInstanceState() has been called
            e.printStackTrace();
            return true;
        }
      //[DUGFIX]-Add-END by TSCD.youcai.chen
    }


    /**
     * assert the sdCard is full or not
     * @return the flag of full boolean
     */
    private boolean isSDCardFull() {
        File path = null;
        boolean extraSdcardfull = true;


        // assert the external sdcard is or not full
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            path = Environment.getExternalStorageDirectory();
            extraSdcardfull = sdCardIsFull(path);
        }

        // assert the sdcard is or not full
        path = Environment.getExternalStorageDirectory();
        boolean sdcardfull = sdCardIsFull(path);


        TctLog.d(TAG, "sdcrad path = " + path.toString());

        if (extraSdcardfull && sdcardfull) {
            return true;
        }

        return false;
    }


    private boolean sdCardIsFull(File aFilePath) {
         StatFs statFs = new StatFs(aFilePath.getPath());
         long blockSize = statFs.getBlockSize();
         long totalBlocks = statFs.getBlockCount();
         long availableBlocks = statFs.getAvailableBlocks();

         if (availableBlocks * blockSize == 0) {
             return true;
         }

         return false;
    }


    private static final int TYPE_MOBIL = 1;
    private static final int TYPE_WIFI = 2;
    private static int sAllowedNetworkWorkType = 1;


    private int getActiveNetworkType(Context context) {
        ConnectivityManager connManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = null;

        if (connManager != null)
            networkinfo = connManager.getActiveNetworkInfo();

        if (networkinfo != null) {
            boolean isWifiActive = (networkinfo.getType() == ConnectivityManager.TYPE_WIFI);
            sAllowedNetworkWorkType = isWifiActive ? TYPE_WIFI : TYPE_MOBIL;
        }
        return sAllowedNetworkWorkType;
    }

    private void startDownload(Intent i) {
        // assert the sdcard is or not
        if (Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED)) {
            // assert the sdcard is or not full
            if (isSDCardFull()) {
                new Thread(new Runnable() {
                    public void run() {
                        Looper.prepare();
                        Toast.makeText(
                                OmaDownloadActivity.this,
                                OmaDownloadActivity.this
                                        .getResources()
                                        .getString( R.string.dialog_insufficient_space_on_external),
                                Toast.LENGTH_SHORT).show();
                        Looper.loop();
                    }
                }).start();
            } else {
                String url = i.getData() == null ? "" : i.getData().toString();
                String type = i.getType();
                Uri mDestinationUri;
                String filename = URLUtil.guessFileName(url, null, type);
//[FEATURE]-Add-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                String cookies=(String)i.getExtra("Cookie");
//[FEATURE]-Add-END by TCTNB.xihe.lu
                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                if (file.exists()) {
                    if (!file.isDirectory()) {
                        throw new IllegalStateException(file.getAbsolutePath()
                                + " already exists and is not a directory");
                    }
                } else {
                    if (!file.mkdir()) {
                        throw new IllegalStateException(
                                "Unable to create directory: " + file.getAbsolutePath());
                    }
                }

                mDestinationUri = Uri.withAppendedPath(Uri.fromFile(file),
                        filename);
                if ("application/vnd.oma.dd+xml".equals(type)) {
                    ContentValues values = new ContentValues();
                    values.put(Downloads.Impl.COLUMN_URI, url);
                    values.put(Downloads.Impl.COLUMN_IS_VISIBLE_IN_DOWNLOADS_UI,true);
                    values.put(Downloads.Impl.COLUMN_VISIBILITY, Downloads.Impl.VISIBILITY_VISIBLE);
                    values.put(Downloads.Impl.COLUMN_IS_PUBLIC_API, true);
                    values.put(Downloads.Impl.COLUMN_ALLOWED_NETWORK_TYPES, getActiveNetworkType(this));
                    values.put(Downloads.Impl.COLUMN_NOTIFICATION_PACKAGE, getPackageName());
                    values.put(Downloads.Impl.COLUMN_MIME_TYPE,type);
//[FEATURE]-Add-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                    values.put(Downloads.Impl.COLUMN_COOKIE_DATA, cookies);
//[FEATURE]-Add-END by TCTNB.xihe.lu
                    if (url != null) {
                        values.put(Downloads.Impl.COLUMN_DESTINATION, Downloads.Impl.DESTINATION_FILE_URI);
                        values.put(Downloads.Impl.COLUMN_FILE_NAME_HINT, mDestinationUri.toString());
                    } else {
                        values.put(
                                Downloads.Impl.COLUMN_DESTINATION,
                                Downloads.Impl.DESTINATION_CACHE_PARTITION_PURGEABLE);
                    }
                    WebAddress webAddress;
                    try {
                        webAddress = new WebAddress(url);
                        values.put(Downloads.Impl.COLUMN_DESCRIPTION, webAddress.getHost());
                    } catch (Exception e) {

                    }
                    uri = this.getContentResolver().insert( Downloads.Impl.CONTENT_URI, values);
                }
            }
        } else {
            Toast.makeText(OmaDownloadActivity.this,
                    OmaDownloadActivity.this.getResources().getString(
                            R.string.dialog_media_not_found), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDownloadAdapter.refreshData();
        //[DUGFIX]-Add-BEGIN by TCTNJ.qingfu.su,12/23/2013,PR-560723
//[BUGFIX]-Mod-BEGIN by TCTNB.Rui.Liu,12/30/2013,578461,
//Try to play the inexistent file cause media force close
        try {
        mDownloadCursor.unregisterDataSetObserver(mDataSetObserver);
        } catch (IllegalStateException e) {
            TctLog.i("OmaDownloadActivity",
                    "throw a IllegalStateException e = " + e);
        }
//[BUGFIX]-Mod-END by TCTNB.Rui.Liu
        //[DUGFIX]-Add-END by TCTNJ.qingfu.su
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo) {
        if (mDownloadCursor != null) {
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
            long packedPosition = info.packedPosition;

            // Only show a context menu for the child views
            if (!mDownloadAdapter.moveCursorToPackedChildPosition(packedPosition)) {
                return;
            }

            mContextMenuPosition = packedPosition;
            menu.setHeaderTitle(mDownloadCursor.getString(mTitleColumnId));

            MenuInflater inflater = getMenuInflater();
            int status = mDownloadCursor.getInt(mStatusColumnId);
            if (Downloads.Impl.isStatusSuccess(status)) {
                inflater.inflate(R.menu.downloadhistorycontextfinished, menu);
            }
            else if (Downloads.Impl.isStatusError(status)) {
                inflater.inflate(R.menu.downloadhistorycontextfailed, menu);
            }
            else {
                // In this case, the download is in progress. Set a
                // ContentObserver so that we can know when it completes,
                // and if it does, we can then update the context menu
                Uri track = ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI,
                        mDownloadCursor.getLong(mIdColumnId));
                // if (mContentObserver != null) {
                // getContentResolver().unregisterContentObserver(
                // mContentObserver);
                // }
                // mContentObserver = new ChangeObserver(track);
                // mSelectedView = v;
                // getContentResolver().registerContentObserver(track, false,
                // mContentObserver);
                inflater.inflate(R.menu.downloadhistorycontextrunning, menu);
            }
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v,
            int groupPosition, int childPosition, long id) {
        mDownloadAdapter.moveCursorToChildPosition(groupPosition, childPosition);

        hideCompletedDownload();

        int status = mDownloadCursor.getInt(mStatusColumnId);
        TctLog.d("***********OMADownloadActivity**status*", "" + status);

        if (Downloads.Impl.isStatusSuccess(status)) {
            // Open it if it downloaded successfully
            openOrDeleteCurrentDownload(false);
        }
        else {
            // Check to see if there is an error.
            checkStatus(id);
        }
        return true;
    }


    private void hideCompletedDownload() {
        int status = mDownloadCursor.getInt(mStatusColumnId);

        int visibilityColumn = mDownloadCursor
                .getColumnIndexOrThrow(Downloads.Impl.COLUMN_VISIBILITY);
        int visibility = mDownloadCursor.getInt(visibilityColumn);

        if (Downloads.Impl.isStatusCompleted(status)
                && visibility == Downloads.Impl.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) {
            ContentValues values = new ContentValues();
            values.put(Downloads.Impl.COLUMN_VISIBILITY, Downloads.Impl.VISIBILITY_VISIBLE);
            getContentResolver().update(
                    ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI,
                            mDownloadCursor.getLong(mIdColumnId)), values, null, null);
        }
    }


    private void openOrDeleteCurrentDownload(boolean delete) {
        int packageColumnId = mDownloadCursor
                .getColumnIndexOrThrow(Downloads.Impl.COLUMN_NOTIFICATION_PACKAGE);
        String packageName = mDownloadCursor.getString(packageColumnId);
        Intent intent = new Intent(delete ? Intent.ACTION_DELETE
                : Downloads.Impl.ACTION_NOTIFICATION_CLICKED);
        Uri contentUri = ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI,
                mDownloadCursor.getLong(mIdColumnId));
        TctLog.d("OMADownloadActivity**********packageColumnId***", "" + packageColumnId);
        TctLog.d("OMADownloadActivity**********packageName***", "" + packageName);
        TctLog.d("OMADownloadActivity**********contentUri***", "" + contentUri);

        intent.setData(contentUri);
        intent.setPackage(packageName);
        sendBroadcast(intent);
//[BUGFIX]-Mod-BEGIN by TCTNB.Rui.Liu,11/21/2013,556045,
        final long id = ContentUris.parseId(intent.getData());
        openCurrentDownload(this,id);//[BUGFIX]-Add by TCTNB.Rui.Liu,01/08/2014,583429,
//[BUGFIX]-Mod-END by TCTNB.Rui.Liu
    }

//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,01/08/2014,583429,
//Miata show other file to user while view the OMAdownloaded file from download
private void openCurrentDownload(Context context, long id) {
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,02/27/2014,608067,
//The prompt when play a non-existent OMA files is NOK.
        final DownloadManager downManager = (DownloadManager) context
                .getSystemService(Context.DOWNLOAD_SERVICE);
        downManager.setAccessAllDownloads(true);
        final Cursor cursor = downManager.query(new DownloadManager.Query()
                .setFilterById(id));
        try {
            if (!cursor.moveToFirst()) {
                throw new IllegalArgumentException("Missing download " + id);
            }
            final Uri localUri = getCursorUri(cursor, COLUMN_LOCAL_URI);
            try {
                getContentResolver().openFileDescriptor(localUri, "r").close();
            } catch (FileNotFoundException exc) {
                TctLog.d("Download", "Failed to open download as the file can not find!");
                Toast.makeText(context, R.string.dialog_file_missing_body,
                        Toast.LENGTH_LONG).show();
                return;
            } catch (IOException exc) {
            }
        } finally {
            cursor.close();
        }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
        if (!OpenHelper.startViewIntent(this, id, 0)) {
            Toast.makeText(this, R.string.download_no_application_title, Toast.LENGTH_SHORT).show();
        }
}
//[BUGFIX]-Add-END by TCTNB.Rui.Liu

//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,02/27/2014,608067,
//The prompt when play a non-existent OMA files is NOK.
    private static String getCursorString(Cursor cursor, String column) {
        return cursor.getString(cursor.getColumnIndexOrThrow(column));
    }

    private static Uri getCursorUri(Cursor cursor, String column) {
        return Uri.parse(getCursorString(cursor, column));
    }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu

//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/21/2013,556045,
//Can't view download files in download list
    private void openCurrentDownload() {
        DownloadManager mDownloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        mDownloadManager.setAccessAllDownloads(true);
        DownloadManager.Query baseQuery = new DownloadManager.Query()
                .setOnlyIncludeVisibleInDownloadsUi(true);
        mDateSortedCursor = mDownloadManager.query(baseQuery);
        if(mDateSortedCursor!=null){
            //startManagingCursor(mDateSortedCursor);//[BUGFIX]-Add by TCTNB.yuan.luo,05/13/2013,PR-451508
           mDateSortedCursor.moveToPosition(DateSortedExpandableListAdapter.mPosition);
           mLocalUriColumnId =
                mDateSortedCursor.getColumnIndexOrThrow(DownloadManager.COLUMN_LOCAL_URI);
           mMediaTypeColumnId =
                mDateSortedCursor.getColumnIndexOrThrow(DownloadManager.COLUMN_MEDIA_TYPE);
           String fileUri=mDateSortedCursor.getString(mLocalUriColumnId);
           if (fileUri!=null)
               viewUri = Uri.parse(fileUri);
           mimeType = mDateSortedCursor.getString(mMediaTypeColumnId);
           mDateSortedCursor.close();
        }
        if(viewUri!=null && mimeType!=null){
           Intent intent = new Intent(Intent.ACTION_VIEW);
           intent.setDataAndType(viewUri, mimeType);
           try {
                startActivity(intent);
           } catch (ActivityNotFoundException ex) {
                Toast.makeText(this, R.string.download_no_application_title, Toast.LENGTH_LONG).show();
           }
        }
    }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
    private void deleteCurrentDownload() {
        TctLog.d(TAG, "delete sdcard file start");
        String data = mDownloadCursor.getString(mDownloadCursor
                .getColumnIndexOrThrow(Downloads.Impl._DATA));

        TctLog.d(TAG, "file path = " + data);
        if (null != data && !"".equals(data)) {
            File file = new File(data);
            file.delete();
            TctLog.d(TAG, "start delete sdcard file end");
        }

        Uri contentUri = ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI,
                mDownloadCursor.getLong(mIdColumnId));
        int deleted = getContentResolver().delete(contentUri, null, null);

        setListViewValues();
        //[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,12/27/2013,578461,
        //Try to play the inexistent file cause media force close
        ContentValues values = new ContentValues();
        values.put(Downloads.Impl.COLUMN_DELETED, 1);
        getContentResolver().update(contentUri, values, null, null);
        //[BUGFIX]-Add-END by TCTNB.Rui.Liu
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mDownloadCursor != null) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.downloadhistory, menu);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean showCancel = getCancelableCount() > 0;
        menu.findItem(R.id.download_menu_cancel_all).setEnabled(showCancel);

        boolean showClear = getClearableCount() > 0;
        menu.findItem(R.id.download_menu_clear_all).setEnabled(showClear);

        return super.onPrepareOptionsMenu(menu);
    }


    private int getCancelableCount() {
        // Count the number of items that will be canceled.
        int count = 0;
        if (mDownloadCursor != null) {
            for (mDownloadCursor.moveToFirst(); !mDownloadCursor.isAfterLast();
                    mDownloadCursor.moveToNext()                                    ) {
                int status = mDownloadCursor.getInt(mStatusColumnId);
                if (!Downloads.Impl.isStatusCompleted(status)) {
                    count++;
                }
            }
        }

        return count;
    }


    private int getClearableCount() {
        int count = 0;
        if (mDownloadCursor.moveToFirst()) {
            while (!mDownloadCursor.isAfterLast()) {
                int status = mDownloadCursor.getInt(mStatusColumnId);
                if (Downloads.Impl.isStatusCompleted(status)) {
                    count++;
                }
                mDownloadCursor.moveToNext();
            }
        }
        return count;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.download_menu_cancel_all:
            flag = false;
            promptCancelAll();
            return true;
        case R.id.download_menu_clear_all:
            promptClearList();
            return true;
        }
        return false;
    }


    private void promptCancelAll() {
        int count = getCancelableCount();

        // If there is nothing to do, just return
        if (count == 0) {
            return;
        }

        // Don't show the dialog if there is only one download
        if (count == 1) {
            cancelAllDownloads();
            return;
        }

        String msg = getString(R.string.download_cancel_dlg_msg, count);
        new AlertDialog.Builder(this).setTitle(R.string.download_cancel_dlg_title)
                // .setIcon(R.drawable.ssl_icon)
                .setMessage(msg).setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                cancelAllDownloads();

                                Toast.makeText(getApplicationContext(),
                                        R.string.download_canceled,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }).setNegativeButton(android.R.string.cancel, null).show();
    }


    /**
     * cancel all downloads
     */
    private void cancelAllDownloads() {
        if (mDownloadCursor.moveToFirst()) {
            StringBuilder where = new StringBuilder();
            boolean firstTime = true;

            while (!mDownloadCursor.isAfterLast()) {
                int status = mDownloadCursor.getInt(mStatusColumnId);
                if (!Downloads.Impl.isStatusCompleted(status)) {
                    if (firstTime) {
                        firstTime = false;
                    }
                    else {
                        where.append(" OR ");
                    }
                    where.append("( ");
                    where.append(Downloads.Impl._ID);
                    where.append(" = '");
                    where.append(mDownloadCursor.getLong(mIdColumnId));
                    where.append("' )");
                }
                String mInstallNotifyUri = mDownloadCursor.getString(mInstallNotifyUriId);
                if (mInstallNotifyUri != null) {
                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                    new ODStatusReportor(mInstallNotifyUri,
                            ODDefines.INSTALLNOTIFY_REPORT_USER_CANCELLED,mContext).start();
                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                }
                mDownloadCursor.moveToNext();
            }

            if (!firstTime) {
                getContentResolver().delete(Downloads.Impl.CONTENT_URI, where.toString(), null);
            }
        }
    }


    /**
     * clear the list data
     */
    private void promptClearList() {
        new AlertDialog.Builder(this).setTitle(R.string.download_clear_dlg_title)
                // .setIcon(R.drawable.ssl_icon)
                .setMessage(R.string.download_clear_dlg_msg).setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                clearAllDownloads();
                            }
                        }).setNegativeButton(android.R.string.cancel, null).show();
    }


    /**
     * clear the all downloads
     */
    private void clearAllDownloads() {
        if (mDownloadCursor.moveToFirst()) {
            StringBuilder where = new StringBuilder();
            boolean firstTime = true;

            while (!mDownloadCursor.isAfterLast()) {
                int status = mDownloadCursor.getInt(mStatusColumnId);
                if (Downloads.Impl.isStatusCompleted(status)) {
                    if (firstTime) {
                        firstTime = false;
                    }
                    else {
                        where.append(" OR ");
                    }
                    where.append("( ");
                    where.append(Downloads.Impl._ID);
                    where.append(" = '");
                    where.append(mDownloadCursor.getLong(mIdColumnId));
                    where.append("' )");
                }
                mDownloadCursor.moveToNext();
            }
            if (!firstTime) {
                getContentResolver().delete(Downloads.Impl.CONTENT_URI, where.toString(), null);
            }
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // if not reach the current position menu, return false
        if (!mDownloadAdapter.moveCursorToPackedChildPosition(mContextMenuPosition)) {
            return false;
        }

        switch (item.getItemId()) {
        case R.id.download_menu_open:
            hideCompletedDownload();
            openOrDeleteCurrentDownload(false);
            return true;

        case R.id.download_menu_delete:
            new AlertDialog.Builder(this)
                    .setTitle(R.string.download_delete_file)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(mDownloadCursor.getString(mTitleColumnId))
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    deleteCurrentDownload();
//                                    openOrDeleteCurrentDownload(true);
                                }
                            }).show();
            break;

        case R.id.download_menu_clear:
            getContentResolver().delete(
                    ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI,
                            mDownloadCursor.getLong(mIdColumnId)), null, null);
            return true;

        case R.id.download_menu_cancel:
            flag = false;

            if (mInstallNotifyURI != null) {
                //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                new ODStatusReportor(mInstallNotifyURI,
                        ODDefines.INSTALLNOTIFY_REPORT_USER_CANCELLED,mContext).start();
                //[FEATURE]-Mod-END by TCTNB.guoju.yao
            }

            clearFromDownloads(mDownloadCursor.getLong(mIdColumnId));
            return true;
        }
        return false;
    }


    private void clearFromDownloads(long id) {
        getContentResolver().delete(
                ContentUris.withAppendedId(Downloads.Impl.CONTENT_URI, id),
                null, null);
    }


    private class DownloadsChangeObserver extends ContentObserver {
        public DownloadsChangeObserver(Uri uri) {
            super(new Handler());
        }

        @Override
        public void onChange(boolean selfChange) {
            StringBuilder wherequery = new StringBuilder(Downloads.Impl.COLUMN_TITLE);
            wherequery.append("=");
            wherequery.append("'");
            wherequery.append(mTitle);
            wherequery.append("'");
        }
    }

    // cr69383 daming.zou for oma downloads begin,this function is used to
    // ensure a file is a dd file
    public boolean ensureIesDD(String filename) {
        if (filename.contains(".")) {
            String[] nametemp = filename.split("\\.");

            //[PLATFORM]-Mod by TCTNB.(QiuRuifeng), 2013/9/20 PR-504324, reason [ID card][OMA DRM]Need support OMA DRM 1.0 (FW Lock + Separate Delivery).
            if (nametemp[nametemp.length - 1].equals("dd")||nametemp[nametemp.length - 1].equals("dm")) {
                TctLog.i("oma", "this is a dd");
                return true;
            }
            else {
                TctLog.i("oma", "this is not a dd");
                return false;
            }
        }
        else {
            TctLog.i("oma", "this is not a dd");
        }
        return false;
    }


    private int checkStatus(final long id) {
        TctLog.d("**********OMADownloadActivity***id", "" + id);
        int groupToShow = mDownloadAdapter.groupFromChildId(id);
        if (-1 == groupToShow)
            return 0;
        int status = mDownloadCursor.getInt(mStatusColumnId);
        if (!Downloads.Impl.isStatusError(status)) {
            return groupToShow;
        }
        if (status == Downloads.Impl.STATUS_FILE_ERROR) {
            String title = mDownloadCursor.getString(mTitleColumnId);
            if (title == null || title.length() == 0) {
                title = getString(R.string.download_unknown_filename);
            }
            String msg = getString(R.string.download_file_error_dlg_msg, title);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.download_file_error_dlg_title)
                    .setIcon(android.R.drawable.ic_popup_disk_full)
                    .setMessage(msg)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNegativeButton(R.string.retry,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    resumeDownload(id);
                                }
                            }).show();
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.download_failed_generic_dlg_title)
                    // .setIcon(R.drawable.ssl_icon)
                    .setMessage(OmaDownloadAdapter.getErrorText(status))
                    .setPositiveButton(android.R.string.ok, null).show();
        }
        return groupToShow;
    }

    /**
     * Resume a given download
     *
     * @param id
     *            Row id of the download to resume
     */
    private void resumeDownload(final long id) {
        // the relevant functionality doesn't exist in the download manager
    }
}

/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiushou.Liu                                                     */
/*  Email  :  Qiushou.Liu@tcl-mobile.com                                      */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/04/2014|qiushou.liu           |     FR736884         |[HOMO][Orange][28]*/
/*           |                      |                      | 11 - DOWNLOAD_01 */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/


package com.android.providers.downloads.ui.omadownloads;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.Downloads;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.providers.downloads.ui.R;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;


/**
 * Class OmaDownloadAdapter
 */
public class OmaDownloadAdapter extends DateSortedExpandableListAdapter {
    private final static String DRM_MIMETYPE_MESSAGE_STRING = "application/vnd.oma.drm.message";

    private final static String DRM_MIMETYPE_CONTENT_STRING = "application/vnd.oma.drm.content";//PR927585-Li-Zhao

    private final static String DRM_MIMETYPE_DR_STRING = "application/vnd.oma.drm.rights+xml";//PR927585-Li-Zhao

    private Context mContext;

    private int mTitleColumnId;

    private int mDescColumnId;

    private int mStatusColumnId;

    private int mTotalBytesColumnId;

    private int mCurrentBytesColumnId;

    private int mMimeTypeColumnId;

    private int mDateColumnId;

    public OmaDownloadAdapter(Context aContext, Cursor aCursor, int aDateIndex) {
        super(aContext, aCursor, aDateIndex);
        mContext = aContext;

        mTitleColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_TITLE);
        mDescColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_DESCRIPTION);
        mStatusColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_STATUS);
        mTotalBytesColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_TOTAL_BYTES);
        mCurrentBytesColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_CURRENT_BYTES);
        mMimeTypeColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_MIME_TYPE);
        mDateColumnId = aCursor.getColumnIndexOrThrow(Downloads.Impl.COLUMN_LAST_MODIFICATION);
    }


    @Override
    public View getChildView(int aGroupPosition, int aChildPosition,
            boolean aIsLastChild, View aConvertView, ViewGroup aParent) {
        if (null == aConvertView || !(aConvertView instanceof RelativeLayout)) {
            aConvertView = LayoutInflater.from(mContext).inflate(R.layout.oma_download_item, null);
        }

        if (! moveCursorToChildPosition(aGroupPosition, aChildPosition)) {
            return aConvertView;
        }

        ImageView imageView = (ImageView) aConvertView.findViewById(R.id.download_icon);
        String mimeType = getString(mMimeTypeColumnId);
        if (null == mimeType) {
            imageView.setVisibility(View.INVISIBLE);
        }
        //PR927585-Li-Zhao begin
        else if (DRM_MIMETYPE_MESSAGE_STRING.equalsIgnoreCase(mimeType)
                || DRM_MIMETYPE_CONTENT_STRING.equalsIgnoreCase(mimeType)
                || DRM_MIMETYPE_DR_STRING.equalsIgnoreCase(mimeType)) {
            //imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_launcher_drm_file));
            //imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_launcher_drm_file));
            imageView.setVisibility(View.VISIBLE);
        }
        //PR927585-Li-Zhao end
        else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromParts("file", "", null), mimeType);
            PackageManager pm = mContext.getPackageManager();
            List<ResolveInfo> list = pm.queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);

            if (list.size() > 0) {
                Drawable icon = list.get(0).activityInfo.loadIcon(pm);
                imageView.setImageDrawable(icon);
                imageView.setVisibility(View.VISIBLE);
            }
            else {
                imageView.setVisibility(View.INVISIBLE);
            }
        }

        TextView textView = (TextView) aConvertView.findViewById(R.id.download_title);
        String title = getString(mTitleColumnId);
        if (null == title) {
             title = mContext.getResources().getString(R.string.download_unknown_filename);
        }

        textView.setText(title);

        textView = (TextView) aConvertView.findViewById(R.id.domain);
        textView.setText(getString(mDescColumnId));

        long totalBytes = getLong(mTotalBytesColumnId);
        int status = getInt(mStatusColumnId);
        if (Downloads.Impl.isStatusCompleted(status)) {
            View view = aConvertView.findViewById(R.id.progress_text);
            view.setVisibility(View.GONE);

            view = aConvertView.findViewById(R.id.download_progress);
            view.setVisibility(View.GONE);

            textView = (TextView) aConvertView.findViewById(R.id.complete_text);
            textView.setVisibility(View.VISIBLE);
            if (Downloads.Impl.isStatusError(status)) {
                textView.setText(getErrorText(status));
            }
            else {
                textView.setText(mContext.getResources().getString(R.string.download_success,
                        Formatter.formatFileSize(mContext, totalBytes)));
            }

            long time = getLong(mDateColumnId);
            Date date = new Date(time);
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
            textView = (TextView) aConvertView.findViewById(R.id.complete_date);
            textView.setVisibility(View.VISIBLE);
            textView.setText(df.format(date));
        }
        else {
            textView = (TextView) aConvertView.findViewById(R.id.progress_text);
            textView.setVisibility(View.VISIBLE);

            View progress = aConvertView.findViewById(R.id.download_progress);
            progress.setVisibility(View.VISIBLE);

            View v = aConvertView.findViewById(R.id.complete_date);
            v.setVisibility(View.GONE);

            v = aConvertView.findViewById(R.id.complete_text);
            v.setVisibility(View.GONE);

            if (Downloads.Impl.STATUS_PENDING == status) {
                textView.setText(mContext.getResources().getText(R.string.download_pending));
            }
            else if (Downloads.Impl.STATUS_WAITING_FOR_NETWORK  == status) {
                textView.setText(
                        mContext.getResources().getText(R.string.download_pending_network));
            }
            else {
                ProgressBar pb = (ProgressBar) progress;

                StringBuilder sb = new StringBuilder();

                if (Downloads.Impl.STATUS_RUNNING  == status) {
                    sb.append(mContext.getResources().getText(R.string.download_running));
                }
                else {
                    sb.append(mContext.getResources().getText(R.string.download_running_paused));
                }

                if (totalBytes > 0) {
                    long currentBytes = getLong(mCurrentBytesColumnId);
                    int progressAmount = (int)(currentBytes * 100 / totalBytes);
                    sb.append(' ');
                    sb.append(progressAmount);
                    sb.append("% (");
                    sb.append(Formatter.formatFileSize(mContext, currentBytes));
                    sb.append("/");
                    sb.append(Formatter.formatFileSize(mContext, totalBytes));
                    sb.append(")");
                    pb.setIndeterminate(false);
                    pb.setProgress(progressAmount);
                }
                else {
                    pb.setIndeterminate(true);
                }
                textView.setText(sb.toString());
            }
        }

        return aConvertView;
    }

    public static int getErrorText(int aStatus) {
        switch (aStatus) {
            // 406
            case Downloads.Impl.STATUS_NOT_ACCEPTABLE:
                return R.string.download_not_acceptable;

            // 411
            case Downloads.Impl.STATUS_LENGTH_REQUIRED:
                return R.string.download_length_required;

            // 412
            case Downloads.Impl.STATUS_PRECONDITION_FAILED:
                return R.string.download_precondition_failed;

            // 490
            case Downloads.Impl.STATUS_CANCELED:
                return R.string.download_canceled;

            // 492
            //case Downloads.Impl.STATUS_FILE_ERROR:
            //    return R.string.download_file_error;

            // 400
            case Downloads.Impl.STATUS_BAD_REQUEST:
            // 491
            case Downloads.Impl.STATUS_UNKNOWN_ERROR:
            default:
                return R.string.download_error;
        }
    }
}

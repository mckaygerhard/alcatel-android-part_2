/**
 * Class/ODDownloadEngine
 * Copyright @ 2011 TCT, Inc. All Rights Reserved
 * Revision History:
 * Modification Tracking
 *      Author         Date      Version
 * --------------- ------------ --------- ------------------------------------
 *   yanli.zhang   2012-05-08     0.1
 *   zonghui.li    2012-11-07     add this file for drm
 */

/**
 * ODDownloadEngine.java
 *
 * @copyright:
 * Copyright (c) 2010 TCT Inc, all rights reserved.
 */

//package com.android.providers.omadownloads;
/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiushou.Liu                                                      */
/*  Email  :  Qiushou.Liu@tcl-mobile.com                                       */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/04/2014|qiushou.liu           |     FR736884         |[HOMO][Orange][28]*/
/*           |                      |                      | 11 - DOWNLOAD_01 */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.providers.downloads.ui.omadownloads;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.WebAddress;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Downloads;
import android.util.TctLog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.Toast;
import com.android.providers.downloads.ui.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

// [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/05/2013,PR-560722,
import android.app.ActivityManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import java.util.List;
// [BUGDIX]-MOD-END by TSNJ.(chao.yuan),

import org.apache.http.HttpConnection;
// [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536,
import android.util.Log;
// [BUGDIX]-MOD-END by TSNJ.(chao.yuan),

import android.provider.Settings;

/**
 * Class ODDownloadEngine
 */
public class ODDownloadEngine extends Activity {
    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536,
    private TextView mTextMessage;
    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
    private TextView mTextView;// the content of a Dialog.
    private StringBuffer mSb;// save the content of DD file temporarily
    private StringBuffer mediaTypes;

    /**
     * mObject[0]:the information to show for user mObject[1]:the media object
     * download uri mObject[2]:the installNotifyURI mObject[3]:the media object
     * size mObject[6]:the nextURL mObject[7]:the mediaTypes mObject[4]:check
     * the mandatory attributes is missing or not mObject[5]:check the DDVersion
     * attribute is exist or not,if exist,and is be supported by devices,the
     * value is true,other else is false;if not exist,the value is true.
     */
    private Object[] mObject;
    private String mObjectURL;
    private String mInstallNotifyURI;
    private BigDecimal mObjcetSize;
    private String mNextUrl;
    private String[] mMediaTypes;
    private String mDDUrl;
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,03/13/2014,617185,
    private String mMediaObjectName;
//[BUGFIX]-Add-END by TCTNB.Rui.Liu

    private boolean mDelDdFile = true;

    private final static String AUTH_SERVICE_PACKAGE = "com.android.aidl.action.httpauth";
    private final static int MESSAGE_MISMATCH_TYPE = 1;
    private final static int MESSAGE_MISMATCH_SIZE = 2;
    private final static int MESSAGE_URL_INVALIED = 3;
    private final static int MESSAGE_URL_VALIED = 4;
    private final static int MAX_CONNECT_RETRY = 10;
    private static com.android.httpauth.IHttpAuthService mAuthService;
    private  static String sInstallNotifyURI;

    private Context mContext;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog);
        mContext = this;
        Intent intent = getIntent();
        bindService(this);
        String fileName = intent
                .getStringExtra(ODDefines.DD_FILEPATH_IDENTIFIER);
        Long sdcardSize = intent.getLongExtra(ODDefines.SD_CARD_AVAILABLE_SIZE,
                0);
        mDDUrl=intent.getStringExtra("DDURL");

        TctLog.d("ODDownloadEngine:", "" + fileName+" dd url="+mDDUrl);
        // fileName = "/mnt/sdcard/download/forward.dd";
        if (fileName != null) {
            File file = new File(fileName);
            // read the dd file and parse it
            readFile(file, sdcardSize);
        } else {
            finish();
        }

        //PR-285674,Modified by yanli.zhang begin.
        getContentResolver().delete(
                 Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI,
                 Downloads.Impl._DATA + " = '"
                         + fileName + "'", null);
         //PR-285674,Modified by yanli.zhang end.
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/05/2013,PR-560722,
    private int getIntentActivities(String data,String type)
    {
        PackageManager pm = this.getPackageManager();
        Intent intent = new Intent();
        Uri uri = Uri.parse(data);
        intent.setDataAndType(uri, type);
        intent.setAction(Intent.ACTION_VIEW);
        List<ResolveInfo> list = pm.queryIntentActivities(intent, 0);
        return list.size();
    }
    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),

    /*
     * Download agent SHALL support Download Descriptor
     * delivery by MMS mechanism
     */
    String getAttachedDdFile(Uri uri) {
        InputStream in = null;
        FileOutputStream out = null;

        if (uri == null) {
            return null;
        }
        String fileName = Environment.getDownloadCacheDirectory() + "/"
                + uri.getLastPathSegment();
        try {
            in = getContentResolver().openInputStream(uri);
            out = new FileOutputStream(fileName);

            int b = -1;
            while ((b = in.read()) != -1) {
                out.write(b);
            }
        } catch (Exception e) {
            // drop it silently
        } finally {
            try {
                if (in != null)
                    in.close();
                if (out != null)
                    out.close();
            } catch (Exception e) {
            }
        }
        return fileName;
    }


    /**
     * Read file and parse it
     *
     * @param afile
     *            ---the DD file
     */
    private void readFile(final File aFile, long sdcardSize) {
        // check the file is already exist or not.
        TctLog.d("******** ODDownloadEngine ** aFile.exists() *", "" + aFile.exists());
        TctLog.d("ODDownloadEngine:", "" + aFile);
        if (aFile.exists()) {
            mObject = new ODDownloadDescriptorParser().parseXml(this, aFile);// start to parse it.
            TctLog.d("********ODDownloadEngine**mDelDdFile*", "" + mDelDdFile);
             if (mDelDdFile) {
                 aFile.delete();
             }
            mDelDdFile = true;

            if (mObject != null) {
                mInstallNotifyURI = (mObject[2] != null) ? (String) mObject[2]
                        : null;
                mNextUrl = (mObject[6] != null) ? (String) mObject[6] : null;
                mMediaObjectName = (mObject[8] != null) ?(String)mObject[8]:null;//[BUGFIX]-Add by TCTNB.Rui.Liu,03/13/2014,617185,

                sInstallNotifyURI = mInstallNotifyURI;
                //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,07/14/2014,704961
                // fuwenliang, OMA download, begin
                // fuwenliang, Defect: 982588 , begin
                //Settings.System.putString(mContext.getContentResolver(), "ODNotifyURI",mInstallNotifyURI);
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("ODNotifyURI", mInstallNotifyURI);
                editor.commit();
                // fuwenliang, Defect: 982588 , end
                // fuwenliang, OMA download, end
                TctLog.d("ODDownloadEngine:","sInstallNotifyURI = "+sInstallNotifyURI);
                //[FEATURE]-Mod-END by TCTNB.guoju.yao
                /**
                 * if one of the mandatory attributes is missing, or DDVersion
                 * is not be supported by this device, then delete the dd file
                 * from device.
                 */
                if (mObject[4].equals(false)) {

                    Toast.makeText(
                            ODDownloadEngine.this,
                            ODDownloadEngine.this.getResources().getString(
                                    R.string.download_invalid_descriptor),
                            Toast.LENGTH_SHORT).show();
                    finish();
                } else if (mObject[5].equals(false)) {

                    Toast.makeText(
                            ODDownloadEngine.this,
                            ODDownloadEngine.this.getResources().getString(
                                    R.string.download_invalid_ddversion),
                            Toast.LENGTH_SHORT).show();
                    finish();

                } else if (mObject[4].equals(true) && mObject[5].equals(true)) {
                    mSb = (StringBuffer) mObject[0];
                    mObjectURL = (String) mObject[1];
                    mObjcetSize = (BigDecimal) mObject[3];

                    if (mObjcetSize == null) {

                        if (mInstallNotifyURI != null) {
                            new Thread(new Runnable() {

                                public void run() {
                                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                                    new ODStatusReportor(mContext)
                                            .sendAndRecevie(
                                                    aFile,
                                                    mInstallNotifyURI,
                                                    ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR);
                                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                                }
                            }).start();
                            // new ODStatusReportor(mInstallNotifyURI,
                            // ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR)
                            // .start();
                        }
                        Toast.makeText(
                                ODDownloadEngine.this,
                                ODDownloadEngine.this.getResources().getString(
                                        R.string.download_invalid_descriptor),
                                Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (!getHESpace(sdcardSize)) {
                        // not enough space in sdcard
                        if (mInstallNotifyURI != null) {
                            new Thread(new Runnable() {
                                public void run() {
                                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                                    new ODStatusReportor(mContext)
                                            .sendAndRecevie(
                                                    aFile,
                                                    mInstallNotifyURI,
                                                    ODDefines.INSTALLNOTIFY_REPORT_INSUFFICIENT_MEMORY);
                                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                                }
                            }).start();
                            Toast.makeText(ODDownloadEngine.this,
                                            ODDownloadEngine.this
                                                    .getResources()
                                                    .getString(
                                                            R.string.dialog_insufficient_space_on_external),
                                            Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }else {

                        mediaTypes = (StringBuffer) mObject[7];
                        TctLog.d("XmlTest", "mediaTypes = " + mediaTypes);
                        mMediaTypes = mediaTypes.toString().split(",");

                        int count = 0;
                        for (String type : mMediaTypes) {
                            count = !type.equals("null") ? count + 1 : count;
                        }
                        if (count < 1) {
                            if (mInstallNotifyURI != null) {
                                new Thread(new Runnable() {
                                    public void run() {
                                        //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                                        new ODStatusReportor(mContext)
                                                .sendAndRecevie(
                                                        aFile,
                                                        mInstallNotifyURI,
                                                        ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR);
                                        //[FEATURE]-Mod-END by TCTNB.guoju.yao
                                    }
                                }).start();
                                // new ODStatusReportor(mInstallNotifyURI,
                                // ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR)
                                // .start();
                            }
                            Toast.makeText(
                                    ODDownloadEngine.this,
                                    ODDownloadEngine.this
                                            .getResources()
                                            .getString(
                                                    R.string.download_invalid_descriptor),
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            /**
                             * if objectURI is not null,and the content of dd
                             * file is not null, show the content dialog to the
                             * user
                             */
                            //about force close happend when ObjectURL is start with ftp://,
                            //the objectURL should start with "http://" or "https://",in Browser we use
                            //HttpURLConnection to request network!
                            String mScheme = "";
                            if(mObjectURL != null) {
                                mScheme = Uri.parse(mObjectURL).getScheme();
                                if(mScheme==null && mDDUrl!=null){
                                    mScheme =Uri.parse(mDDUrl).getScheme();
                                    if(mScheme!=null && mDDUrl.lastIndexOf("/")!=-1){
                                        mObjectURL=mDDUrl.substring(0, mDDUrl.lastIndexOf("/")+1)+mObjectURL;
                                        }
                                   }

                               }
                            if (mObjectURL != null && ("http".equalsIgnoreCase(mScheme)) || "https".equalsIgnoreCase(mScheme)) {
                                if (mSb.length() > 0) {
                                    boolean flag = false;
                                    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536,
                                    boolean wma_flag = false;
                                    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
                                    for (String type : mMediaTypes) {
                                    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536,
                                        String apk_type = "application/vnd.android.package-archive";
                                        String wma_type = "audio/x-ms-wma";
                                        if(type.equals(apk_type))
                                        {
                                            flag = true;
                                            break;
                                        }
                                        else if (getIntentActivities(mObjectURL,type) > 0)
                                        {
                                            if(type.equals(wma_type))
                                            {
                                                flag = false;
                                                wma_flag = true;
                                            }
                                            else
                                            {
                                                flag = true;
                                            }
                                            break;
                                        }
                                    }
                                    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
                                    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536,
                                    if(wma_flag && !flag){
                                        this.showDialog(ODDefines.DOWNLOAD_UNSUPPORT_DIALOG_ID);
                                    }else if(!flag){
                                        this.showDialog(ODDefines.DOWNLOAD_UNSUPPORT_CONFIRM_ID);
                                    }else{
                                        this.showDialog(ODDefines.MEDIA_INFORMATION_DIALOG_ID);
                                    }
                                    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
                                } else {
                                    TctLog.d("XmlTest", "Go on!");
                                }
                            } else {
                                // send status report "954 Loder Error" to
                                // server
                                new Thread(new Runnable() {

                                    public void run() {
                                        if (mInstallNotifyURI != null) {
                                            //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                                            new ODStatusReportor(mContext)
                                                    .sendAndRecevie(
                                                            aFile,
                                                            mInstallNotifyURI,
                                                            ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR);
                                            //[FEATURE]-Mod-END by TCTNB.guoju.yao
                                        }
                                    }
                                }).start();

                                Toast.makeText(
                                        ODDownloadEngine.this,
                                        ODDownloadEngine.this
                                                .getResources()
                                                .getString(
                                                        R.string.download_invalid_descriptor),
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }// end if
                        }// end if
                    }
                }
            }// end if
        }// end if
      //[BUGFIX]-Mod-BEGIN by TCTNJ.(pingwen.tu),01/28/2014, PR-594571,597334,
      //[Download][RecentAPP]Download should nothing while enter it from recent APP
        else {
           Intent intentDownload = new Intent();
           intentDownload.setClass(ODDownloadEngine.this, OmaDownloadActivity.class);
           ODDownloadEngine.this.startActivity(intentDownload);
           finish();
        }
     //[BUGFIX]-Mod-END  by TCTNJ.(pingwen.tu)
    }

    @Override
    protected Dialog onCreateDialog(int aId) {
        switch (aId) {
        case ODDefines.MEDIA_INFORMATION_DIALOG_ID:
            return showDownloadConfirmationDlg();
        // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/05/2013,PR-560722,
        case ODDefines.DOWNLOAD_UNSUPPORT_DIALOG_ID:
            return showUnsupportDlg();
        // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
        // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536,
        case ODDefines.DOWNLOAD_UNSUPPORT_CONFIRM_ID:
            return showDownloadUnsupportConfirmDlg();
        // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
        }
        return null;
    }

    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/05/2013,PR-560722,
    private Dialog showUnsupportDlg()
    {
        AlertDialog alert = new AlertDialog.Builder(this).setTitle(ODDownloadEngine.this.getResources().getString(
                R.string.download_dlg_title)).setMessage(R.string.download_not_acceptable).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int arg1) {
            // TODO Auto-generated method stub
            if (mInstallNotifyURI != null) {
                //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                new ODStatusReportor(
                        mInstallNotifyURI,
                            ODDefines.INSTALLNOTIFY_REPORT_ACCEPTABLE_CONTENT,mContext)
                            .start();
                //[FEATURE]-Mod-END by TCTNB.guoju.yao
                }
                finish();
            }
        }).create();
        alert.setCanceledOnTouchOutside(false);
        return alert;
    }
    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/26/2013,PR-578536, 473
    private Dialog showDownloadUnsupportConfirmDlg() {
        Builder builder = new Builder(this);
        LayoutInflater inf = getLayoutInflater();
        View view = inf.inflate(R.layout.dialog_confirm, null);
        mTextMessage = (TextView) view.findViewById(R.id.text_message);
        mTextView = (TextView) view.findViewById(R.id.text);
        mTextMessage.setText(R.string.download_confirm);
        if (mSb != null) {
            mTextView.setText(mSb.toString());
        }

        builder.setTitle(ODDownloadEngine.this.getResources().getString(
                R.string.download_dlg_title)
                + "?");
        builder.setView(view);

        // user choose to download
        builder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        TctLog.d("XmlTest", "objectURL: " + mObjectURL);

                        new Thread(new Runnable() {
                            /*
                             * Use thread to void ANR problem
                             */
                            public void run() {
                                checkMediaAction();
                            }

                        }).start();
                    }
                });

        // user choose not to download
        builder.setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (mInstallNotifyURI != null) {
                            //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                            new ODStatusReportor(
                                    mInstallNotifyURI,
                                    ODDefines.INSTALLNOTIFY_REPORT_USER_CANCELLED,mContext)
                                    .start();
                            //[FEATURE]-Mod-END by TCTNB.guoju.yao
                        }
                        finish();
                    }
                });
        AlertDialog mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);
        return mAlertDialog;
    }

    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
    // show a confirmation dialog to user, user can choose whether to download
    // the media object
    private Dialog showDownloadConfirmationDlg() {
        Builder builder = new Builder(this);
        LayoutInflater inf = getLayoutInflater();
        View view = inf.inflate(R.layout.dialog, null);
        mTextView = (TextView) view.findViewById(R.id.text);

        if (mSb != null) {
            mTextView.setText(mSb.toString());
        }

        builder.setTitle(ODDownloadEngine.this.getResources().getString(
                R.string.download_dlg_title)
                + "?");
        builder.setView(view);

        // user choose to download
        builder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        TctLog.d("XmlTest", "objectURL: " + mObjectURL);

                        new Thread(new Runnable() {
                            /*
                             * Use thread to void ANR problem
                             */
                            public void run() {
                                checkMediaAction();
                            }

                        }).start();
                    }
                });

        // user choose not to download
        builder.setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (mInstallNotifyURI != null) {
                            //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                            new ODStatusReportor(
                                    mInstallNotifyURI,
                                    ODDefines.INSTALLNOTIFY_REPORT_USER_CANCELLED,mContext)
                                    .start();
                            //[FEATURE]-Mod-END by TCTNB.guoju.yao
                        }
                        finish();
                    }
                });
        AlertDialog mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);
        return mAlertDialog;
    }


    /*
     * Function to get http authoraztion information.
     */
    private static String[] getHttpAuth(String host) {
        try {
            return mAuthService.getCredential(host, "android:auth");
        } catch (Exception e) {
            return null;
        }
    }

    // bind the AuthService
    private void bindService(Context context) {
        Intent serviceIntent = new Intent(AUTH_SERVICE_PACKAGE);
        serviceIntent.setPackage("com.android.browser");
        context.bindService(serviceIntent, mServerConnect,
                Context.BIND_AUTO_CREATE);
    }

    // unbind the AuthService
    private void unbindService(Context context) {
        if (null == context) {
            return;
        }
        context.unbindService(mServerConnect);
    }

    /*
     * Use the remote service in Browser to query http authoraztion information
     * and return it to client.
     */
    ServiceConnection mServerConnect = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAuthService = com.android.httpauth.IHttpAuthService.Stub
                    .asInterface(service);
        }

        public void onServiceDisconnected(ComponentName name) {
            mAuthService = null;
        }
    };

    Handler mPostStatusHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_MISMATCH_TYPE:
                // MismatchSize
                if (mInstallNotifyURI != null) {
                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                    new ODStatusReportor(mInstallNotifyURI,
                            ODDefines.INSTALLNOTIFY_REPORT_ATTRIBUTE_MISMATCH,mContext)
                            .start();
                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                }

                Toast.makeText(
                        ODDownloadEngine.this,
                        ODDownloadEngine.this.getResources().getString(
                                R.string.omadownload_error_mismatch_type),
                        Toast.LENGTH_SHORT).show();

                break;

            case MESSAGE_MISMATCH_SIZE:
                // MismatchType
                if (mInstallNotifyURI != null) {
                    new ODStatusReportor(mInstallNotifyURI,
                            ODDefines.INSTALLNOTIFY_REPORT_ATTRIBUTE_MISMATCH,mContext)
                            .start();
                }

                Toast.makeText(
                        ODDownloadEngine.this,
                        ODDownloadEngine.this.getResources().getString(
                                R.string.omadownload_error_mismatch_size),
                        Toast.LENGTH_SHORT).show();

                break;

            case MESSAGE_URL_INVALIED:
                // The Url is invalid
                //[BUGFIX]-Mod-BEGIN by TCTNJ.(guoju.yao),12/06/2013, PR-560720,change Toast
                if (mInstallNotifyURI != null) {
                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                    new ODStatusReportor(mInstallNotifyURI,
                            ODDefines.INSTALLNOTIFY_REPORT_LOADER_ERROR,mContext)
                            .start();
                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                    Toast.makeText(
                        ODDownloadEngine.this,
                        ODDownloadEngine.this.getResources().getString(
                                R.string.bookmark_url_load_error),
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(
                        ODDownloadEngine.this,
                        ODDownloadEngine.this.getResources().getString(
                                R.string.bookmark_url_not_valid),
                        Toast.LENGTH_SHORT).show();
                }
                //[BUGFIX]-Mod-END  by TCTNJ.(guoju.yao)
                break;

            case MESSAGE_URL_VALIED:
                downloadMediaAction();
                break;

            default:
                break;
            }

            /*
             * back to Download history activity
             */
            Intent intentDownload = new Intent();
            intentDownload.putExtra(OmaDownloadActivity.NEXT_URL, mNextUrl);
            intentDownload.putExtra(OmaDownloadActivity.INSTALL_NOTIFY_URL,
                    sInstallNotifyURI);
            intentDownload.setClass(ODDownloadEngine.this,
                    OmaDownloadActivity.class);
            intentDownload.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ODDownloadEngine.this.startActivity(intentDownload);
            finish();
        }
    };

    private void checkMediaAction() {

        int messageType = MESSAGE_URL_INVALIED;

        if(Uri.parse(mObjectURL).getScheme().equals("https")){
            messageType=MESSAGE_URL_VALIED;
        }else{
        URL url = null;
        HttpURLConnection con = null;
        BigDecimal totalSize = null;
        String mMediaType = null;
        boolean urlValid = false;
        String username = null;
        String password = null;

        try {
            url = new URL(mObjectURL);
            final String host = url.getHost();
            String[] credentials = getHttpAuth(host);
            if (credentials != null && credentials.length == 2) {
                username = credentials[0];
                password = credentials[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            url = new URL(mObjectURL);
            int counts = 0;
            while (counts < MAX_CONNECT_RETRY) {
                /*
                 * Try MAX_CONNECT_RETRY times when connect failed
                 */
                byte[] encodedPassword = (username + ":" + password).getBytes();
                String encodedAuth = Base64.encodeBytes(encodedPassword);
                con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setRequestMethod("GET");
                con.setRequestProperty("Authorization", "Basic " + encodedAuth);
                con.setConnectTimeout(3000);

                int state = con.getResponseCode();
                if (state == 200) {
                    urlValid = true;
                    messageType = MESSAGE_URL_VALIED;
                    break;
                }
                counts++;
            }

            if (urlValid) {
                totalSize = new BigDecimal(con.getContentLength());
                mMediaType = con.getContentType();

                con.disconnect();
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,538866,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,11/18/2013,559012,
                boolean checkSizeState = true;
                //[BUGFIX]-Mod-BEGIN by TCTNB.Tianjun Xu,02/13/2014,PR594697,
                //OMA file "image" can't download successful by ATT network.
                boolean sizeMatchNeed = true;//getResources().getBoolean(com.android.internal.R.bool.feature_tctfw_OmaSizeMatch_on);
                android.util.Log.d("harvey", "size match is " + sizeMatchNeed);
				//[BUGFIX]-Mod-END by TCTNB.Tianjun Xu
                if(sizeMatchNeed) {
                    checkSizeState = (mObjcetSize.compareTo(totalSize) == 0) ||
                                                  (mObjcetSize.compareTo(new BigDecimal(-1)) == 0);
                }else {
                    checkSizeState = true;
                }
                if (checkSizeState) {
                //[FEATURE]-Mod-END by TCTNB.guoju.yao
//[FEATURE]-Mod-END by TCTNB.xihe.lu
                    int count = 0;
                    if (mMediaTypes != null) {
                        for (String str : mMediaTypes) {
                            // we only compare the xxx instead of audio/xxx or
                            // video/xxx when the mediaType
                            // in dd file is audio/xxx, but mediaType got from
                            // getContentType method is video/xxx
                            boolean mFlag = str.contains("audio/") && mMediaType.contains("video/");
                            if (mFlag) {
                                String subStr = str.substring(str.indexOf("/") + 1, str.length());
                                String subMMediaType = mMediaType.substring(
                                        mMediaType.indexOf("/") + 1, mMediaType.length());
                                count = (subMMediaType.equalsIgnoreCase(subStr)) ? count + 1
                                        : count;
                            } else {
                                count = (mMediaType.equalsIgnoreCase(str)) ? count + 1 : count;
                            }
                            // count = (mMediaType.equals(str)) ? count + 1
                            // : count;
                        }
                    }
                    if (count < 1) {
                        messageType = MESSAGE_MISMATCH_TYPE;
                    }
                } else {
                    messageType = MESSAGE_MISMATCH_SIZE;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            con = null;
        }
        }

        if (mPostStatusHandler != null)
            mPostStatusHandler.sendEmptyMessage(messageType);
    }

    private void downloadMediaAction() {
        Uri uri = Uri.parse(mObjectURL);
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String filename = URLUtil.guessFileName(uri.toString(), null, null);
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,03/13/2014,617185,
//CHECK_DD_DA_4 - name attribute.
        if (null != mMediaObjectName) {
            filename = mMediaObjectName;
            TctLog.i("ODDownloadEngine"," ODDownloadEngine downloadMediaAction filename = "+filename);
        }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
        if (file.exists()) {
            if (!file.isDirectory()) {
                throw new IllegalStateException(file.getAbsolutePath()
                        + " already exists and is not a directory");
            }
        } else {
            if (!file.mkdir()) {
                throw new IllegalStateException(
                        "Unable to create directory: "
                                + file.getAbsolutePath());
            }
        }
        Uri mDestinationUri;
        TctLog.d("zyl","filename--"+filename);
        mDestinationUri = Uri.withAppendedPath(Uri.fromFile(file),
                filename);
        ContentValues values = new ContentValues();
        values.put(Downloads.Impl.COLUMN_URI, mObjectURL);
        // fuwenliang, defect988721, begin
        values.put(Downloads.Impl.COLUMN_MIME_TYPE, null != mMediaTypes ? mMediaTypes[0] : null);
        // fuwenliang, defect988721, end
        values.put(Downloads.Impl.COLUMN_IS_VISIBLE_IN_DOWNLOADS_UI, true);
        values.put(Downloads.Impl.COLUMN_VISIBILITY,
                Downloads.Impl.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        values.put(Downloads.Impl.COLUMN_IS_PUBLIC_API, true);
        values.put(Downloads.Impl.COLUMN_ALLOWED_NETWORK_TYPES,
                getActiveNetworkType(this));
        values.put(Downloads.Impl.COLUMN_NOTIFICATION_PACKAGE, getPackageName());
        if (uri != null) {
            values.put(Downloads.Impl.COLUMN_DESTINATION,
                    Downloads.Impl.DESTINATION_FILE_URI);
            // fuwenliang, defect1534889, begin
            String name = null;
            String hint = mDestinationUri.toString();
            String suffix = "";
            final File file1 = new File(Uri.parse(hint).getPath());
            name = file1.getName();
            final int dotIndex = name.lastIndexOf('.');
            final boolean missingExtension = dotIndex < 0;
            if (missingExtension) {
                final int dotIndexUrl  = mObjectURL.lastIndexOf('.');
                if (dotIndexUrl >= 0) {
                    suffix = mObjectURL.substring(dotIndexUrl);
                }
            }
            hint += suffix;
            TctLog.i("ODDownloadEngine", "[downloadMediaAction] hint = " + hint + " suffix = " + suffix
                    + " name = " + name);
            //values.put(Downloads.Impl.COLUMN_FILE_NAME_HINT,
                    //mDestinationUri.toString());
            values.put(Downloads.Impl.COLUMN_FILE_NAME_HINT,
                    hint);
            // fuwenliang, defect1534889, end
        } else {
            values.put(
                    Downloads.Impl.COLUMN_DESTINATION,
                    Downloads.Impl.DESTINATION_CACHE_PARTITION_PURGEABLE);
        }
        WebAddress webAddress;
        try {
            webAddress = new WebAddress(mObjectURL);
            values.put(Downloads.Impl.COLUMN_DESCRIPTION, webAddress.getHost());
        } catch (Exception e) {
            // This only happens for very bad urls, we want to chatch the
            // exception here
        }
        ODDownloadEngine.this.getContentResolver().insert(
                Downloads.Impl.CONTENT_URI, values);
        TctLog.d("XmlTest", "objectURL: " + mObjectURL);

    }

    @Override
    protected void onDestroy() {
        unbindService(this);
        super.onDestroy();
    }

    public static String getInstallNotifyURL() {
        return sInstallNotifyURI;
    }

    private boolean getHESpace(long available) {
        boolean bIsESpace = false;
        BigDecimal availableSize = new BigDecimal(available);
        if (mObjcetSize.compareTo(availableSize) <= 0) {
            bIsESpace = true;
        }
        return bIsESpace;
    }

    private static final int TYPE_MOBIL = 1;
    private static final int TYPE_WIFI = 2;
    private static int sAllowedNetworkWorkType = 1;

    private int getActiveNetworkType(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = null;
        if (connManager != null)
            networkinfo = connManager.getActiveNetworkInfo();

        if (networkinfo != null) {
            boolean isWifiActive = (networkinfo.getType() == ConnectivityManager.TYPE_WIFI);
            sAllowedNetworkWorkType = isWifiActive ? TYPE_WIFI : TYPE_MOBIL;
        }
        return sAllowedNetworkWorkType;
    }
}

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.android.providers.downloads;

import android.content.Context;
import android.drm.DrmManagerClient;
/* MODIFIED-BEGIN by Zhenhua.Fan, 2016-09-07,BUG-2813541*/
import com.tct.drm.TctDrmManagerClient;
import android.text.TextUtils;
/* MODIFIED-END by Zhenhua.Fan,BUG-2813541*/

import java.io.File;
//[FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/8/20, TCT DRM solution
import java.util.HashMap;
import java.util.Map;
//[FEATURE]-Add-END by TCTNB.Yang.Hu,2015/8/20, TCT DRM solution

public class DownloadDrmHelper {

    /** The MIME type of special DRM files */
    public static final String MIMETYPE_DRM_MESSAGE = "application/vnd.oma.drm.message";

    /** The extensions of special DRM files */
    public static final String EXTENSION_DRM_MESSAGE = ".dm";

    public static final String EXTENSION_INTERNAL_FWDL = ".fl";

    //[FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/8/20, TCT DRM solution
    public static final String TAG = "DownloadDrmHelper";
    public static final String MIMETYPE_DRM_SD = "application/vnd.oma.drm.content";
    public static final String EXTENSION_DRM_SD = ".dcf";
    public static final String DRM_MIMETYPE_RIGHTS_XML_STRING = "application/vnd.oma.drm.rights+xml";
    public static final String DRM_MIMETYPE_RIGHTS_WBXML_STRING = "application/vnd.oma.drm.rights+wbxml";
    public static final String DRM_MIMETYPE_FL_STRING = "forward-lock";
    public static final String DRM_MIMETYPE_CD_STRING = "combined-delivery";
    public static final String DRM_MIMETYPE_SD_STRING = "separate-delivery";
    private static final Map<String, String> mimeTypeToExtensionMap = new HashMap<String, String>();
    static {
       add("audio/mpeg","mp3");
       add("audio/mp3","mp3");
       add("video/mp3","mp3");
       add("audio/mp4","mp4");
       add("video/mp4","mp4");
       add("audio/3gpp","3gpp");
       add("video/3gpp","3gpp");
       add("audio/aac","aac");
       add("video/aac","aac");
       add("audio/mp4a-latm","aac");
       add("video/mp4a-latm","aac");
       add("video/mpeg4","mp4");
       add("audio/x-m4a","m4a");
       add("audio/sp-midi","smf");
    };
    //[FEATURE]-Add-END by TCTNB.Yang.Hu,2015/8/20, TCT DRM solution

    /**
     * Checks if the Media Type needs to be DRM converted
     *
     * @param mimetype Media type of the content
     * @return True if convert is needed else false
     */
    public static boolean isDrmConvertNeeded(String mimetype) {
        return MIMETYPE_DRM_MESSAGE.equals(mimetype);
    }

    /**
     * Modifies the file extension for a DRM Forward Lock file NOTE: This
     * function shouldn't be called if the file shouldn't be DRM converted
     */
    public static String modifyDrmFwLockFileExtension(String filename) {
        if (filename != null) {
            int extensionIndex;
            extensionIndex = filename.lastIndexOf(".");
            if (extensionIndex != -1) {
                filename = filename.substring(0, extensionIndex);
            }
            filename = filename.concat(EXTENSION_INTERNAL_FWDL);
        }
        return filename;
    }

    /**
     * Return the original MIME type of the given file, using the DRM framework
     * if the file is protected content.
     */
    public static String getOriginalMimeType(Context context, File file, String currentMime) {
        final DrmManagerClient client = new DrmManagerClient(context);
        try {
            /* MODIFIED-BEGIN by Zhenhua.Fan, 2016-09-05,BUG-2813541*/
            //BUGFIX-ADD-BEGIN BY NJTS.WEI.HUANG 2016-01-18 FOR 1210664
            final String rawFile = file.toString();
            return client.getOriginalMimeType(rawFile);
            //BUGFIX-ADD-END BY NJTS.WEI.HUANG
            /* MODIFIED-END by Zhenhua.Fan,BUG-2813541*/
        } finally {
            client.release();
        }
    }

    //[FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/8/20, TCT DRM solution
    public static boolean isDrmConvertNeededFL(String mimetype) {
        return MIMETYPE_DRM_MESSAGE.equals(mimetype);
    }

    public static boolean isDrmRightObjects(String mimeType) {
        return DRM_MIMETYPE_RIGHTS_XML_STRING.equals(mimeType) ||
               DRM_MIMETYPE_RIGHTS_WBXML_STRING.equals(mimeType);
    }

    public static boolean isDrmConvertNeededSD(String mimetype) {
        return MIMETYPE_DRM_SD.equals(mimetype);
    }

    public static String modifyDrmSDFileExteneion(String filename) {
        if (filename != null) {
            int extensionIndex;
            extensionIndex = filename.lastIndexOf(".");
            if (extensionIndex != -1) {
                filename = filename.substring(0, extensionIndex);
            }
            filename = filename.concat(EXTENSION_DRM_SD);
        }

        return filename;
    }

    public static String getExtensionFromMimeType(String mimeType) {
        if (mimeType == null || mimeType.isEmpty()) {
            return null;
        }
        return mimeTypeToExtensionMap.get(mimeType);
    }

    private static void add(String mimeType, String extension) {
        if (!mimeTypeToExtensionMap.containsKey(mimeType)) {
            mimeTypeToExtensionMap.put(mimeType, extension);
        }
    }
    //[FEATURE]-Add-END by TCTNB.Yang.Hu,2015/8/20, TCT DRM solution
}

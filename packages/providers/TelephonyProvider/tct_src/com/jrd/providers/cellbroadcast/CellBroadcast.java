/*
 * Copyright (C) 2011-2012, Code Aurora Forum. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 * Neither the name of Code Aurora Forum, Inc. nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  bo.xu                                                           */
/*  Email  :  Bo.Xu@tcl-mobile.com                                            */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : packages/providers/TelephonyProvider/tct_src/com/jrd/providers/*/
/*             cellbroadcast/CellBroadcast.java                               */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/06/2013|yugang.jia            |FR-516039             |SMSCB             */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.jrd.providers.cellbroadcast;

import android.net.Uri;
import android.provider.BaseColumns;

public final class CellBroadcast {

    public static final String AUTHORITY = "com.jrd.provider.CellBroadcast";

    // This class cannot be instantiated
    private CellBroadcast() {
    }

    public static final class Cellbroadcasts implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
                + "/Cellbroadcasts");

        public static final String MESSAGE = "message";

        public static final String CHANNEL = "channel";

        public static final String CREATED = "created";

        public static final String DEFAULT_SORT_ORDER = "created DESC";

    }

    public static final class SpecialURI implements BaseColumns {
        // query: Cellbroadcasts left outer join Channel on Cellbroadcasts.
        public static final Uri CONTENT_URI_CBCH = Uri.parse("content://" + AUTHORITY + "/cbch");

    }

    public static final class CBLanguage implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/CBLanguage");

        public static final String CBLANGUAGE = "cblanguage";
    }
    //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
    //Tone needed for CellBroadcast message received
    public static final class CBRingtone implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/CBRingtone");

        public static final String CBRINGTONE = "cbringtone";
    }
    //[FEATURE]-Add-END by TCTNB.bo.xu
    public static final class Channel implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/Channel");

        public static final String INDEX = "mesid";

        public static final String NAME = "name";

        public static final String Enable = "isenable";

        public static final String CREATED = "created";

        public static final String DEFAULT_SORT_ORDER = "created DESC";

    }
}

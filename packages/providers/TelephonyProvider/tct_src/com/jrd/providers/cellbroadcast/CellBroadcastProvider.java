/*
 * Copyright (C) 2011-2012, Code Aurora Forum. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 * Neither the name of Code Aurora Forum, Inc. nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  bo.xu                                                           */
/*  Email  :  Bo.Xu@tcl-mobile.com                                            */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : packages/providers/TelephonyProvider/tct_src/com/jrd/providers/*/
/*             cellbroadcast/CellBroadcast.java                               */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/06/2013|yugang.jia            |FR-516039             |SMSCB             */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.jrd.providers.cellbroadcast;

import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.tct.libs.util.TLog;

import com.jrd.providers.cellbroadcast.CellBroadcast;
import com.jrd.providers.cellbroadcast.CellBroadcast.CBLanguage;
import com.jrd.providers.cellbroadcast.CellBroadcast.CBRingtone;//[FEATURE]-Add by TCTNB.bo.xu,04/09/2013,FR-399953,
                                                                //Tone needed for CellBroadcast message received
import com.jrd.providers.cellbroadcast.CellBroadcast.Cellbroadcasts;
import com.jrd.providers.cellbroadcast.CellBroadcast.Channel;

public class CellBroadcastProvider extends ContentProvider {
    private static final String TAG = "CellBroadcastProvider";

    private static final String DATABASE_NAME = "cellbroadcast.db";

    private static final int DATABASE_VERSION = 2;

    private static final String CB_TABLE_NAME = "cellbroadcast";

    private static final String CBLANGUAGE_TABLE_NAME = "cblanguage";

    private static final String CHANNEL_TABLE_NAME = "channel";

    private static final String CBRINGTONE_TABLE_NAME = "cbringtone";//[FEATURE]-Add by TCTNB.bo.xu,04/09/2013,FR-399953,
                                                                     //Tone needed for CellBroadcast message received
    private static HashMap<String, String> sBroadcastMap;

    private static HashMap<String, String> sChannelMap;

    private static HashMap<String, String> sLanguageMap;

    private static HashMap<String, String> sRingtoneMap;//[FEATURE]-Add by TCTNB.bo.xu,04/09/2013,FR-399953,
                                                        //Tone needed for CellBroadcast message received

    private static HashMap<String, String> sPecialMap;

    private static final int CBS = 1;

    private static final int CB_ID = 2;

    private static final int CHANNEL = 3;

    private static final int CHANNEL_ID = 4;

    private static final int CBLANGUAGE = 5;

    private static final int CBLANGUAGE_ID = 6;

    private static final int CONTENT_URI_CBCH = 7;

    private static final int CONTENT_URI_CBCH_ID = 8;
    //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
    //Tone needed for CellBroadcast message received
    private static final int CBRINGTONE = 9;

    private static final int CBRINGTONE_ID = 10;
    //[FEATURE]-Add-END by TCTNB.bo.xu
    private static final UriMatcher sUriMatcher;

    /**
     * This class helps open, create, and upgrade the database file.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE " + CB_TABLE_NAME + " (" + Cellbroadcasts._ID
                    + " INTEGER PRIMARY KEY," + Cellbroadcasts.MESSAGE + " TEXT,"
                    + Cellbroadcasts.CHANNEL + " TEXT," + Cellbroadcasts.CREATED + " INTEGER"
                    + ");");
            db.execSQL("CREATE TABLE " + CHANNEL_TABLE_NAME + " (" + Channel._ID
                    + " INTEGER PRIMARY KEY," + Channel.INDEX + " TEXT ," + Channel.NAME + " TEXT,"
                    + Channel.Enable + " TEXT," + Channel.CREATED + " INTEGER" + ");");
            db.execSQL("CREATE TABLE " + CBLANGUAGE_TABLE_NAME + " (" + CBLanguage._ID
                    + " INTEGER PRIMARY KEY," + CBLanguage.CBLANGUAGE + " TEXT" + ");");
            //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
            //Tone needed for CellBroadcast message received
            db.execSQL("CREATE TABLE " + CBRINGTONE_TABLE_NAME + " (" + CBRingtone._ID
                    + " INTEGER PRIMARY KEY," + CBRingtone.CBRINGTONE + " TEXT" + ");");
            //[FEATURE]-Add-END by TCTNB.bo.xu
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            TLog.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
                    + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS notes");
            onCreate(db);
        }
    }

    private DatabaseHelper mOpenHelper;

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (sUriMatcher.match(uri)) {
            case CBS:
                qb.setTables(CB_TABLE_NAME);
                qb.setProjectionMap(sBroadcastMap);
                break;

            case CB_ID:
                qb.setTables(CB_TABLE_NAME);
                qb.setProjectionMap(sBroadcastMap);
                qb.appendWhere(Cellbroadcasts._ID + "=" + uri.getPathSegments().get(1));
                break;
            case CHANNEL:
                qb.setTables(CHANNEL_TABLE_NAME);
                qb.setProjectionMap(sChannelMap);
                break;
            case CHANNEL_ID:
                qb.setTables(CHANNEL_TABLE_NAME);
                qb.setProjectionMap(sChannelMap);
                qb.appendWhere(Channel._ID + "=" + uri.getPathSegments().get(1));
                break;
            case CBLANGUAGE:
                qb.setTables(CBLANGUAGE_TABLE_NAME);
                qb.setProjectionMap(sLanguageMap);
                break;
            case CBLANGUAGE_ID:
                qb.setTables(CBLANGUAGE_TABLE_NAME);
                qb.setProjectionMap(sLanguageMap);
                qb.appendWhere(CBLanguage._ID + "=" + uri.getPathSegments().get(1));
                break;
            //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
            //Tone needed for CellBroadcast message received
            case CBRINGTONE:
                qb.setTables(CBRINGTONE_TABLE_NAME);
                qb.setProjectionMap(sRingtoneMap);
                break;
            case CBRINGTONE_ID:
                qb.setTables(CBRINGTONE_TABLE_NAME);
                qb.setProjectionMap(sRingtoneMap);
                qb.appendWhere(CBRingtone._ID + "=" + uri.getPathSegments().get(1));
                break;
            //[FEATURE]-Add-END by TCTNB.bo.xu
            case CONTENT_URI_CBCH:
                qb.setTables("cellbroadcast LEFT OUTER JOIN channel ON (cellbroadcast.channel = channel.mesid)");
                qb.setProjectionMap(sPecialMap);
                break;
            case CONTENT_URI_CBCH_ID:
                qb.setTables("cellbroadcast LEFT OUTER JOIN channel ON (cellbroadcast.channel = channel.mesid)");
                qb.setProjectionMap(sPecialMap);
                qb.appendWhere(Cellbroadcasts.CHANNEL + "=" + uri.getPathSegments().get(1));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // If no sort order is specified use the default
        String orderBy = null;
        if (TextUtils.isEmpty(sortOrder)) {
        //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
        //Tone needed for CellBroadcast message received
        if (sUriMatcher.match(uri) != CBLANGUAGE && sUriMatcher.match(uri) != CBLANGUAGE_ID
                    && sUriMatcher.match(uri) != CBRINGTONE && sUriMatcher.match(uri) != CBRINGTONE_ID)
        //[FEATURE]-Add-END by TCTNB.bo.xu
                orderBy = Cellbroadcasts.DEFAULT_SORT_ORDER;
        } else {
            orderBy = sortOrder;
        }

        // Get the database and run the query
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);

        // Tell the cursor what uri to watch, so it knows when its source data
        // changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        return "";
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        // Validate the requested uri
        //[FEATURE]-Mod by TCTNB.bo.xu,04/09/2013,FR-399953,
        //Tone needed for CellBroadcast message received
        if (sUriMatcher.match(uri) != CBS && sUriMatcher.match(uri) != CHANNEL
                && sUriMatcher.match(uri) != CBLANGUAGE && sUriMatcher.match(uri) != CBRINGTONE) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        Long now = Long.valueOf(System.currentTimeMillis());
        long rowId = 0;
        Uri muri = null;
        // Make sure that the fields are all set
        switch (sUriMatcher.match(uri)) {
            case CBS:
                if (values.containsKey(Cellbroadcasts.CREATED) == false) {
                    values.put(Cellbroadcasts.CREATED, now);
                }
                rowId = db.insert(CB_TABLE_NAME, Cellbroadcasts.MESSAGE, values);
                muri = Cellbroadcasts.CONTENT_URI;
                break;
            case CHANNEL:
                if (values.containsKey(Channel.CREATED) == false) {
                    values.put(Channel.CREATED, now);
                }
                rowId = db.insert(CHANNEL_TABLE_NAME, Channel.INDEX, values);
                muri = Channel.CONTENT_URI;
                break;
            case CBLANGUAGE:
                rowId = db.insert(CBLANGUAGE_TABLE_NAME, CBLanguage.CBLANGUAGE, values);
                muri = CBLanguage.CONTENT_URI;
                break;
            //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
            //Tone needed for CellBroadcast message received
            case CBRINGTONE:
                rowId = db.insert(CBRINGTONE_TABLE_NAME, CBRingtone.CBRINGTONE, values);
                muri = CBRingtone.CONTENT_URI;
                break;
            //[FEATURE]-Add-END by TCTNB.bo.xu
        }

        if (rowId > 0) {
            Uri CBUri = ContentUris.withAppendedId(muri, rowId);
            getContext().getContentResolver().notifyChange(CBUri, null);
            return CBUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        boolean cbFlag = false;
        switch (sUriMatcher.match(uri)) {
            case CBS:
                count = db.delete(CB_TABLE_NAME, where, whereArgs);
                break;

            case CB_ID:
                String MesId = uri.getPathSegments().get(1);
                if (whereArgs != null && whereArgs[0].equals("cblist")) {
                    // fresh cb mes list screen
                    cbFlag = true;
                    count = db.delete(CB_TABLE_NAME,
                            Cellbroadcasts._ID + "=" + MesId
                                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""),
                            null);
                } else {
                    count = db.delete(CB_TABLE_NAME,
                            Cellbroadcasts._ID + "=" + MesId
                                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""),
                            whereArgs);
                }

                break;
            case CHANNEL:
                count = db.delete(CHANNEL_TABLE_NAME, where, whereArgs);
                break;
            case CHANNEL_ID:
                String chId = uri.getPathSegments().get(1);
                count = db.delete(CHANNEL_TABLE_NAME,
                        Channel._ID + "=" + chId
                                + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""),
                        whereArgs);
                break;
            case CBLANGUAGE:
                count = db.delete(CBLANGUAGE_TABLE_NAME, where, whereArgs);
                break;
            //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
            //Tone needed for CellBroadcast message received
            case CBRINGTONE:
                count = db.delete(CBRINGTONE_TABLE_NAME, where, whereArgs);
                break;
            //[FEATURE]-Add-END by TCTNB.bo.xu
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (cbFlag) {
            // fresh cb mes list screen
            getContext().getContentResolver().notifyChange(
                    CellBroadcast.SpecialURI.CONTENT_URI_CBCH, null);
        } else {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
            case CBS:
                count = db.update(CB_TABLE_NAME, values, where, whereArgs);
                break;

            case CB_ID:
                String MesId = uri.getPathSegments().get(1);
                count = db.update(CB_TABLE_NAME, values, Cellbroadcasts._ID + "=" + MesId
                        + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case CHANNEL:
                count = db.update(CHANNEL_TABLE_NAME, values, where, whereArgs);
                break;
            case CHANNEL_ID:
                String chId = uri.getPathSegments().get(1);
                count = db.update(CHANNEL_TABLE_NAME, values, Channel._ID + "=" + chId
                        + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case CBLANGUAGE:
                count = db.delete(CBLANGUAGE_TABLE_NAME, where, whereArgs);
                break;
            case CBLANGUAGE_ID:
                String lanId = uri.getPathSegments().get(1);
                count = db.delete(CBLANGUAGE_TABLE_NAME,
                        CBLanguage._ID + "=" + lanId
                                + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""),
                        whereArgs);
                break;
            //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
            //Tone needed for CellBroadcast message received
            case CBRINGTONE:
                count = db.delete(CBRINGTONE_TABLE_NAME, where, whereArgs);
                break;
            case CBRINGTONE_ID:
                String ringId = uri.getPathSegments().get(1);
                count = db.delete(CBRINGTONE_TABLE_NAME,
                        CBRingtone._ID + "=" + ringId
                                + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""),
                        whereArgs);
                break;
            //[FEATURE]-Add-END by TCTNB.bo.xu
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // CELL BROADCAST
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "Cellbroadcasts", CBS);
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "Cellbroadcasts/#", CB_ID);

        // CHANNEL
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "Channel", CHANNEL);
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "Channel/#", CHANNEL_ID);

        // SPECIAL URI

        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "cbch", CONTENT_URI_CBCH);
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "cbch/#", CONTENT_URI_CBCH_ID);

        // CBLANGUAGE
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "CBLanguage", CBLANGUAGE);
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "CBLanguage/#", CBLANGUAGE_ID);
        //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
        //Tone needed for CellBroadcast message received
        // CBRINGTONE
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "CBRingtone", CBRINGTONE);
        sUriMatcher.addURI(CellBroadcast.AUTHORITY, "CBRingtone/#", CBRINGTONE_ID);
        //[FEATURE]-Mod-END by TCTNB.bo.xu

        // CHELL BROADCAST
        sBroadcastMap = new HashMap<String, String>();
        sBroadcastMap.put(Cellbroadcasts._ID, Cellbroadcasts._ID);
        sBroadcastMap.put(Cellbroadcasts.MESSAGE, Cellbroadcasts.MESSAGE);
        sBroadcastMap.put(Cellbroadcasts.CHANNEL, Cellbroadcasts.CHANNEL);
        sBroadcastMap.put(Cellbroadcasts.CREATED, Cellbroadcasts.CREATED);

        // SPECIAL MAP
        sPecialMap = new HashMap<String, String>();
        sPecialMap.put(Channel.NAME, Channel.NAME);// for select channel.name
                                                   // from Cellbroadcasts left
                                                   // outer join Channel
        sPecialMap.put(Cellbroadcasts._ID, CB_TABLE_NAME + "." + Cellbroadcasts._ID + " AS "
                + Cellbroadcasts._ID);
        sPecialMap.put(Cellbroadcasts.MESSAGE, Cellbroadcasts.MESSAGE);
        sPecialMap.put(Cellbroadcasts.CHANNEL, Cellbroadcasts.CHANNEL);

        // CHANNEL
        sChannelMap = new HashMap<String, String>();
        sChannelMap.put(Channel._ID, Channel._ID);
        sChannelMap.put(Channel.INDEX, Channel.INDEX);
        sChannelMap.put(Channel.NAME, Channel.NAME);
        sChannelMap.put(Channel.Enable, Channel.Enable);
        sChannelMap.put(Channel.CREATED, Channel.CREATED);

        // CBLANGUAGE
        sLanguageMap = new HashMap<String, String>();
        sLanguageMap.put(CBLanguage._ID, CBLanguage._ID);
        sLanguageMap.put(CBLanguage.CBLANGUAGE, CBLanguage.CBLANGUAGE);

        //[FEATURE]-Add-BEGIN by TCTNB.bo.xu,04/09/2013,FR-399953,
        //Tone needed for CellBroadcast message received
        // CBRINGTONE
        sRingtoneMap = new HashMap<String, String>();
        sRingtoneMap.put(CBRingtone._ID, CBRingtone._ID);
        sRingtoneMap.put(CBRingtone.CBRINGTONE, CBRingtone.CBRINGTONE);
        //[FEATURE]-Mod-END by TCTNB.bo.xu
    }
}

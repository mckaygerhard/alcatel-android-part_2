package com.android.providers.media;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemProperties;
import android.util.Log;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Helper class used to set and check out which process can access DRM files.
 */
public class DrmHelper {
    private static final String TAG = "DrmHelper";
    private static HashMap<Integer, Boolean> sCurrentProcesses;
    private static Set<String> sPermitedProcessNames;
    private static String tctDrm = SystemProperties.get("feature.tct.drm.enabled");
    private static boolean isTctDrmEnable = false;

    static{
        isTctDrmEnable = (tctDrm != null && (tctDrm.equals("true")||tctDrm.equals("1")));
    };
    /**
     * Checks out whether the process can access DRM files or not.
     *
     * @param pid process id.
     * @return true if the process can access DRM files.
     */
    public static synchronized boolean isPermitedAccessDrm(Context context, int pid) {
        if (!isTctDrmEnable) {
            Log.w(TAG, "not support DRM!");
            return false;
        }
        Boolean result = null;// not set
        if (sCurrentProcesses == null) {
            sCurrentProcesses = new HashMap<Integer, Boolean>();
        } else {
            result = sCurrentProcesses.get(pid);
        }
        // no this process
        if (result == null) {
            Log.i(TAG, "permitAccessDrm(" + pid + ") can not get result!");
            if (sPermitedProcessNames == null) {
                // if not set permited process names, here set the default names.
                setDefaultProcessNames();
            }
            sCurrentProcesses.clear();// clear old map
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> list = am.getRunningAppProcesses();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                RunningAppProcessInfo runInfo = list.get(i);
                boolean allow = sPermitedProcessNames.contains(runInfo.processName);
                sCurrentProcesses.put(runInfo.pid, allow);
                Log.i(TAG, "pid=" + runInfo.pid + ", name=" + runInfo.processName + ", allow=" + allow);
            }
            result = sCurrentProcesses.get(pid);
            if (result == null) {
                Log.e(TAG, "Can not get current pid's access drm info! pid=" + pid);
                return true;
            }
        }
        if (!result) {
            Log.i(TAG, "permitAccessDrm(" + pid + ") return " + result);
        }
        return result;
    }

    /**
     * Sets processes that can access DRM files.
     *
     * @param permitedProcessNames the name of processes.
     */
    public static synchronized void setPermitedProcessNames(String[] permitedProcessNames) {
        if (sPermitedProcessNames == null) {
            sPermitedProcessNames = new HashSet<String>();
        } else {
            sPermitedProcessNames.clear();
        }
        if (permitedProcessNames == null) {
            Log.w(TAG, "setPermitedProcessNames() none permited access drm process!");
        } else {
            int length = permitedProcessNames.length;
            for (int i = 0; i < length; i++) {
                sPermitedProcessNames.add(permitedProcessNames[i]);
                Log.i(TAG, "setPermitedProcessNames() add [" + i + "]=" + permitedProcessNames[i]);
            }
        }
    }

    private static void setDefaultProcessNames() {
        String[] permitedProcessNames = new String[] {
                "com.tct.gallery3d",
                "com.android.music",
                "com.jrdcom.music",
                "com.android.gallery",
                "android.process.media",
                "com.android.settings",
                "com.android.gallery3d",
                "com.android.deskclock",
                "com.android.mms",
                "system"
              };
        setPermitedProcessNames(permitedProcessNames);
    }
}

package com.android.providers.media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.os.FileObserver;
import android.os.Handler;
import android.os.Message;
import android.util.TctLog;

import com.android.providers.media.DrmTrackerAdapter.DrmObj;

public class DrmFileObserver extends FileObserver {
    private static DrmFileObserver sInstance;
    private HashMap<String, ArrayList<DrmObj>> drmObjsMap = new HashMap<String, ArrayList<DrmObj>>();
    private Handler drmTaskHandler;
    String mPath;

    public static synchronized DrmFileObserver getInstance(String path,Handler drmTaskHandler){
        if (sInstance == null) {
            sInstance = new DrmFileObserver(path,drmTaskHandler);
        }
        return sInstance;
    }

    private DrmFileObserver(String path, Handler drmTaskHandler) {
        super(path, FileObserver.DELETE | FileObserver.DELETE_SELF | FileObserver.MOVE_SELF
                | FileObserver.MOVED_FROM);
        mPath = path;
        this.drmTaskHandler = drmTaskHandler;
        DrmUtil.logd("FileObserver path: " + path);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Observer path:" + mPath);
        sb.append(" |drmObj- map:" + drmObjsMap);
        return sb.toString();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onEvent(int event, String path) {
        DrmUtil.logd("|event:" + event + "|event->path:[" + path + "]");
        if (event == 32768) {
            return;
        }
        ArrayList<DrmObj> drmList = null;
        ;
        if (path != null) {
            Object obj = drmObjsMap.get(path);
            if (obj == null) {
                DrmUtil.logd("Event come but drmObjMap is NULL!" + "| fileName:" + path);
                return;
            }
            drmList = ((ArrayList<DrmObj>) obj);
        } else {
            drmList = new ArrayList<DrmObj>();
            for (ArrayList<DrmObj> list : drmObjsMap.values()) {
                drmList.addAll(list);
            }
        }
        Message msg = null;
        for (DrmObj drm : drmList) {
            DrmUtil.logd("File Event:" + event + " | drmObj:" + drm);
            msg = drmTaskHandler.obtainMessage(drm.drmType + DrmTrackerAdapter.MSG_DELETE_INTERVAL);
            msg.obj = drm;
            drmTaskHandler.sendMessage(msg);
        }
    }

    boolean removeDrmObj(int drmType, String name) {
        ArrayList<DrmObj> drmObjList = drmObjsMap.get(name);
        if (drmObjList == null) {
            return false;
        }
        Iterator<DrmObj> iterator = drmObjList.iterator();
        DrmObj obj = null;
        while (iterator.hasNext()) {
            obj = iterator.next();
            if (obj.drmType == drmType) {
                iterator.remove();
                break;
            }
        }
        if (drmObjList.isEmpty()) {
            drmObjsMap.remove(name);
        }
        return true;
    }
    boolean containFile(String name) {
        return drmObjsMap.containsKey(name);
    }

    void addDrmObj(DrmObj drmObj) {
        ArrayList<DrmObj> drmObjs = drmObjsMap.get(drmObj.name);
        if (drmObjs == null) {
            drmObjs = new ArrayList<DrmObj>(4);
            DrmUtil.logd("in addDrmObj drmObj="+drmObj);
            drmObjsMap.put(drmObj.name, drmObjs);
        }
        drmObjs.add(drmObj);
        DrmUtil.logd("drmObjsMap="+drmObjsMap.toString());
    }

    boolean isEmpty() {
        return drmObjsMap.isEmpty();
    }

    HashMap<String, ArrayList<DrmObj>> getMaps() {
        return drmObjsMap;
    }

    ArrayList<DrmObj> removeAllDrmObj() {
        Set<String> sets = drmObjsMap.keySet();
        ArrayList<DrmObj> results = new ArrayList<DrmObj>();
        for (String key : sets) {
            results.addAll(drmObjsMap.get(key));
        }
        drmObjsMap.clear();
        return results;
    }

    DrmObj getDrmObj(int drmType) {
        Collection<ArrayList<DrmObj>> values = drmObjsMap.values();
        for (ArrayList<DrmObj> list : values) {
            for (DrmObj obj : list) {
                if (obj.drmType == drmType) {
                    return obj;
                }
            }
        }
        return null;
    }
}

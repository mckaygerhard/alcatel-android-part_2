/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* ==========================================================================   */
/*     Modifications on Features list / Changes Request / Problems Report       */
/* ----------|----------------------|----------------------|--------------------*/
/*    date   |        Author        |         Key          |     comment        */
/* ----------|----------------------|----------------------|--------------------*/
/* 03/11/2016|   feng.cao           |                      |Each ringtone not   */
/*           |                      |                      |choose "original"   */
/*           |                      |                      |remix1 remix2 remix3*/
/* ----------|----------------------|----------------------|--------------------*/
/******************************************************************************/

package com.android.providers.media;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.UserDictionary;
import android.provider.MediaStore.Audio;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.tct.drm.TctDrmManagerClient;
import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;

import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleCursorAdapter;
import android.content.ContentUris;
import android.widget.LinearLayout.LayoutParams;
import android.util.TypedValue;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.widget.HeaderViewListAdapter;
import android.widget.CheckedTextView;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.HashMap;

import android.widget.ArrayAdapter;
import java.util.Iterator;
import android.widget.Spinner;
import android.widget.Button;


import android.content.ComponentName;
import android.content.res.TypedArray;
import android.app.Activity;
import android.app.ProgressDialog;

import java.io.File;
import java.net.URL;

import android.media.AudioManager;
/**
 * The {@link RingtonePickerActivity} allows the user to choose one from all of the
 * available ringtones. The chosen ringtone's URI will be persisted as a string.
 *
 * @see RingtoneManager#ACTION_RINGTONE_PICKER
 */
public final class MixRingtonePickerActivity extends AlertActivity implements
        AdapterView.OnItemSelectedListener, Runnable, DialogInterface.OnClickListener,
        AlertController.AlertParams.OnPrepareListViewListener {

    private static final int POS_UNKNOWN = -1;

    private static final String TAG = "MixRingtonePickerActivity";

    private static final int DELAY_MS_SELECTION_PLAYED = 300;

    private static final String SAVE_CLICKED_POS = "clicked_pos";

    private static final String CRICKET_TITLE = "Cricket";

    private RingtoneManager mRingtoneManager;
    private int mType;

    private Cursor mCursor;
    private Handler mHandler;

    private RingtoneAdapter mMyRingAdapter;
    private RingtoneAdapter mLocalRingAdapter;
    private ArrayList<RingListItem> mLocalRingArrayList;
    private ArrayList<RingListItem> mMyRingArrayList;
    private ArrayList<FXRingInfo> mFXRingList;
    private Cursor mMyCursor;
    private Cursor mFxCursor;
    LinearLayout myRingtonesTitle;
    ListView myRingtonesList;
    ListView mLocalRingtoneList;
    private Uri mSelectedRingtoneUri = null;
    private Ringtone mMyRingtone = null;
    private boolean mHaseMyRingtonesItem = true;
    private boolean mNeedRefreshOnResume = false;
    private int mSelectMyRingtonesPos = -1;
    private ProgressDialog mProgressDialog;

    /** The position in the list of the 'Silent' item. */
    private int mSilentPos = POS_UNKNOWN;

    /** The position in the list of the 'Default' item. */
    private int mDefaultRingtonePos = POS_UNKNOWN;

    /** The position in the list of the last clicked item. */
    private int mClickedPos = POS_UNKNOWN;

    /** The position in the list of the ringtone to sample. */
    private int mSampleRingtonePos = POS_UNKNOWN;

    /** Whether this list has the 'Silent' item. */
    private boolean mHasSilentItem;

    /** The Uri to place a checkmark next to. */
    private Uri mExistingUri;

    /** The number of static items in the list. */
    private int mStaticItemCount;

    /** Whether this list has the 'Default' item. */
    private boolean mHasDefaultItem;

    private boolean mHasMoreRingtonesItem = false;

    /** The Uri to play when the 'Default' item is clicked. */
    private Uri mUriForDefaultItem;

    /**
     * A Ringtone for the default ringtone. In most cases, the RingtoneManager
     * will stop the previous ringtone. However, the RingtoneManager doesn't
     * manage the default ringtone for us, so we should stop this one manually.
     */
    private Ringtone mDefaultRingtone;

    /**
     * The ringtone that's currently playing, unless the currently playing one is the default
     * ringtone.
     */
    private Ringtone mCurrentRingtone;

    /**
     * Keep the currently playing ringtone around when changing orientation, so that it
     * can be stopped later, after the activity is recreated.
     */
    private static Ringtone sPlayingRingtone;

    private final int MORE_RINGTONE_REQUEST = 1000;
//    private boolean clickMoreRingtone = false;
    private AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            switch(focusChange){
                case AudioManager.AUDIOFOCUS_LOSS:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
                        mDefaultRingtone.stop();
                        mDefaultRingtone = null;
                    }
            }
        }
    };

    private DialogInterface.OnClickListener mRingtoneClickListener =
            new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
            // Save the position of most recently clicked item
        	// from RingMix only defalut & silient ring will run here
        	// for other sound, will played in RingtoneAdapter onClick();
            mClickedPos = which;
            mExistingUri = mSelectedRingtoneUri = null;
            stopAnyPlayingRingtone();
			notifyListDataChange(mLocalRingtoneList);
			notifyListDataChange(myRingtonesList);
			Log.i(TAG,"mRingtoneClickListener click");
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
        	Log.i(TAG,"onActivityResult ok");
            switch (requestCode) {
                case MORE_RINGTONE_REQUEST:
                	quitBackFromMoreRing(data);
                    ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(mAudioFocusChangeListener);
                    break;
                default:
            }
        }else{
        	Log.i(TAG,"onActivityResult cancle");
            ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(mAudioFocusChangeListener);
        }
    }

    private void quitBackFromMoreRing(Intent intent) {
        Intent resultIntent = new Intent();
        //[BUGFIX]-Add-BEGIN by TSCD.lei.xiao,01/28/2015,PR-911023,
        //[Clone][4.7][MMS]The phone will flash back when select "More Ringtone".
        //Uri uri = intent.getData();
        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        intent.getLongExtra("android_id", -1));
        //[BUGFIX]-Add-END by TSCD.lei.xiao
        /** add by zonghua-jin for PR927448 begin */
        TctDrmManagerClient drmManager = TctDrmManagerClient.getInstance(this);
        boolean isDrm =drmManager.isDrm(uri);
        if (isDrm) {//is Drm
            String filePath= drmManager.getFilePath(uri);
            //e.g. transfer deleted uri will reset MS
            if(filePath == null || filePath.isEmpty())  {
                return;
            }
            Log.d(TAG, " enter isValidRrmRingtone method path:" + filePath);
            boolean hasCount = drmManager.hasCountConstraint(filePath);
            boolean isValid = drmManager.checkRightsStatus(uri, android.drm.DrmStore.Action.PLAY)==android.drm.DrmStore.RightsStatus.RIGHTS_VALID;
            Log.d(TAG, " check result : " + " hasCount "+hasCount+"  isValid "+isValid);
            if(hasCount){//count right
                //Drm file with count constraint can not set as ringtone
                String toastMsg = String.format(this.getResources().getString(com.android.internal.R.string.drm_ringtone_with_count_constraint), filePath);
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                return;
            }
            if (!isValid) {//no right
                String toastMsg = String.format(this.getResources().getString(com.android.internal.R.string.drm_no_valid_right));
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                return;
            }
        }
		if (uri != null) {
			setRingtone(this.getContentResolver(), uri);
		}
        /** add by zonghua-jin for PR927448 end */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");
        mHandler = new Handler();

        Intent intent = getIntent();

         //Get whether to show the 'More Ringtones' item
//        mHasMoreRingtonesItem = intent.getBooleanExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_MORE_RINGTONES, false);

        mHaseMyRingtonesItem = true;//intent.getBooleanExtra(RingtoneManager.EXTRA_JRD_RINGTONE_SHOW_MY_RINGTONES, true);

        /*
         * Get whether to show the 'Default' item, and the URI to play when the
         * default is clicked
         */
        mHasDefaultItem = intent.getBooleanExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
        mUriForDefaultItem = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI);
        if (mUriForDefaultItem == null) {
            mUriForDefaultItem = Settings.System.DEFAULT_RINGTONE_URI;
        }

        if (savedInstanceState != null) {
            mClickedPos = savedInstanceState.getInt(SAVE_CLICKED_POS, POS_UNKNOWN);
        }
        // Get whether to show the 'Silent' item
        mHasSilentItem = intent.getBooleanExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);

        // Give the Activity so it can do managed queries
        mRingtoneManager = new RingtoneManager(this);

        // Get the types of ringtones to show
        mType = intent.getIntExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, -1);
        if (mType != -1) {
            mRingtoneManager.setType(mType);
        }

//        mCursor = mRingtoneManager.getCursor();
        mCursor = mRingtoneManager.getLocalRingtones();

        mFxCursor = mRingtoneManager.getFXRingtones();
        fillFXRingList(mFxCursor);
        // The volume keys will control the stream that we are choosing a ringtone for
        setVolumeControlStream(mRingtoneManager.inferStreamType());

        // Get the URI whose list item should have a checkmark
        mExistingUri = intent
                .getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI);
        mSelectedRingtoneUri = mExistingUri;
        Log.i(TAG,"create mExistingUri="+mExistingUri);
//        showRingItem();//[BUGFIX]-Mod-BEGIN by TSNJ.wei.zhang,10/23/2014,FR-728500 add more ringtones in settings

        mLocalRingArrayList = new ArrayList<RingListItem>();

        mLocalRingAdapter = new RingtoneAdapter(this,true);
		fillRingArrayList(mLocalRingAdapter, mLocalRingArrayList, mCursor, true);
        final AlertController.AlertParams p = mAlertParams;
		p.mAdapter = mLocalRingAdapter;

//        p.mCursor = mCursor;
        p.mOnClickListener = mRingtoneClickListener;
//        p.mLabelColumn = MediaStore.Audio.Media.TITLE;
        p.mIsSingleChoice = true;
        p.mOnItemSelectedListener = this;
        p.mPositiveButtonText = getString(com.android.internal.R.string.ok);
        p.mPositiveButtonListener = this;
        p.mNegativeButtonText = getString(com.android.internal.R.string.cancel);
        p.mPositiveButtonListener = this;
        p.mOnPrepareListViewListener = this;

        p.mTitle = intent.getCharSequenceExtra(RingtoneManager.EXTRA_RINGTONE_TITLE);
        if (p.mTitle == null) {
            p.mTitle = getString(com.android.internal.R.string.ringtone_picker_title);
        }

        setupAlert();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG,"onSaveInstanceState");
        stopAnyPlayingRingtone();
        outState.putInt(SAVE_CLICKED_POS, mClickedPos);
    }

    public void onPrepareListView(ListView listView) {
    	Log.i(TAG,"onPrepareListView");
    	mLocalRingtoneList = listView;

		if (mHaseMyRingtonesItem) {
			addMyRingtonesItem(listView);
		}

		if (mHaseMyRingtonesItem) {
			addLocalRingtonesItem(listView);
		}

        if (mHasDefaultItem) {
            mDefaultRingtonePos = addDefaultRingtoneItem(listView);

            if (mClickedPos == POS_UNKNOWN && RingtoneManager.isDefault(mExistingUri)) {
                mClickedPos = mDefaultRingtonePos;
            }
        }

        if (mHasSilentItem) {
            mSilentPos = addSilentItem(listView);

            // The 'Silent' item should use a null Uri
            if (mClickedPos == POS_UNKNOWN && mExistingUri == null) {
                mClickedPos = mSilentPos;
                Log.i(TAG,"set client check");
                mAlertParams.mCheckedItem = mClickedPos;
            }
        }

		if (mClickedPos == POS_UNKNOWN) {
			long normalID = findNoramlID(mExistingUri);
			if (normalID < 0) {
				mClickedPos = getListPosition(mRingtoneManager
						.getRingtonePosition(mExistingUri));
			} else {
				mClickedPos = getListPosition(mRingtoneManager
						.getRingtonePosition(ContentUris.withAppendedId(
								MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
								normalID)));
			}
		}

        // Put a checkmark next to an item.
        mAlertParams.mCheckedItem = mClickedPos;
    }

    /**
     * Adds a static item to the top of the list. A static item is one that is not from the
     * RingtoneManager.
     *
     * @param listView The ListView to add to.
     * @param textResId The resource ID of the text for the item.
     * @return The position of the inserted item.
     */
    private int addStaticItem(ListView listView, int textResId) {
        TextView textView = (TextView) getLayoutInflater().inflate(
                com.android.internal.R.layout.select_dialog_singlechoice_material, listView, false);
        textView.setText(textResId);
        listView.addHeaderView(textView);
        mStaticItemCount++;
        return listView.getHeaderViewsCount() - 1;
    }

    private int addDefaultRingtoneItem(ListView listView) {
        if (mType == RingtoneManager.TYPE_NOTIFICATION) {
            return addStaticItem(listView, R.string.notification_sound_default);
        } else if (mType == RingtoneManager.TYPE_ALARM) {
            return addStaticItem(listView, R.string.alarm_sound_default);
        }

        return addStaticItem(listView, R.string.ringtone_default);
    }

    private int addSilentItem(ListView listView) {
        return addStaticItem(listView, com.android.internal.R.string.ringtone_silent);
    }

    /*
     * On click of Ok/Cancel buttons
     */
    public void onClick(DialogInterface dialog, int which) {
    	Log.i(TAG,"onClick mClickedPos="+mClickedPos);
    	Log.i(TAG,"onClick mSilentPos="+mSilentPos);
        boolean positiveResult = which == DialogInterface.BUTTON_POSITIVE;

        // Stop playing the previous ringtone
        mRingtoneManager.stopPreviousRingtone();

        if (positiveResult) {
            Intent resultIntent = new Intent();
            Uri uri = null;

            if(mExistingUri != null){
            	 uri = mExistingUri;
            } else if (mClickedPos == mDefaultRingtonePos) {
                // Set it to the default Uri that they originally gave us
                uri = mUriForDefaultItem;
            } else if (mClickedPos == mSilentPos) {
                // A null Uri is for the 'Silent' item
                uri = null;
            }

            resultIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, uri);
            setResult(RESULT_OK, resultIntent);
        } else {
            setResult(RESULT_CANCELED);
        }

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                mCursor.deactivate();
            }
        });

        finish();
    }

    /*
     * On item selected via keys
     */
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        playRingtone(position, DELAY_MS_SELECTION_PLAYED);
    }

    public void onNothingSelected(AdapterView parent) {
    }

    private void playRingtone(int position, int delayMs) {
        mHandler.removeCallbacks(this);
        mSampleRingtonePos = position;
        if(!mNeedRefreshOnResume)
        mHandler.postDelayed(this, delayMs);
    }

    public void run() {
    	Log.i(TAG,"run mExistingUri="+mExistingUri);
		if (mSelectedRingtoneUri != null) {
			stopAnyPlayingRingtone();
			mMyRingtone = RingtoneManager.getRingtone(this,
					mSelectedRingtoneUri);
			mCurrentRingtone = mMyRingtone;
			if (mMyRingtone != null) {
				mMyRingtone.play();
			}
		} else {
			stopAnyPlayingRingtone();
			if (mSampleRingtonePos == mSilentPos) {
				return;
			}

			Ringtone ringtone;
			if (mSampleRingtonePos == mDefaultRingtonePos) {
				if (mDefaultRingtone == null) {
					mDefaultRingtone = RingtoneManager.getRingtone(this,
							mUriForDefaultItem);
				}
				/*
				 * Stream type of mDefaultRingtone is not set explicitly here.
				 * It should be set in accordance with mRingtoneManager of this
				 * Activity.
				 */
				if (mDefaultRingtone != null) {
					mDefaultRingtone.setStreamType(mRingtoneManager
							.inferStreamType());
				}
				ringtone = mDefaultRingtone;
				mCurrentRingtone = null;
				if (ringtone != null) {
					ringtone.play();
				}
			}
		}
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
        ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(mAudioFocusChangeListener);
        if (!isChangingConfigurations()) {
        	Log.i(TAG,"StopPlayingRingtone");
            stopAnyPlayingRingtone();
        } else {
            saveAnyPlayingRingtone();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
        if (!isChangingConfigurations()) {
        	Log.i(TAG,"StopPlayingRingtone");
            stopAnyPlayingRingtone();
        }
        mNeedRefreshOnResume = true;
    }

    private void saveAnyPlayingRingtone() {
        if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
            sPlayingRingtone = mDefaultRingtone;
        } else if (mCurrentRingtone != null && mCurrentRingtone.isPlaying()) {
            sPlayingRingtone = mCurrentRingtone;
        }
    }

    private void stopAnyPlayingRingtone() {
        if (sPlayingRingtone != null && sPlayingRingtone.isPlaying()) {
            sPlayingRingtone.stop();
        }
        sPlayingRingtone = null;

        if (mMyRingtone != null && mMyRingtone.isPlaying()) {
        	mMyRingtone.stop();
        	mMyRingtone = null;
        }

        if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
            mDefaultRingtone.stop();
        }

        if (mRingtoneManager != null) {
            mRingtoneManager.stopPreviousRingtone();
        }

        //[BUGFIX]-Mod-BEGIN by TSNJ.wei.zhang,10/23/2014,FR-728500 add more ringtones in settings
//        if(mCurrentRingtone != null && mCurrentRingtone.isPlaying()){
//            mCurrentRingtone.stop();
//        }
        //[BUGFIX]-Mod-END by TSNJ.wei.zhang
    }

    private int getRingtoneManagerPosition(int listPos) {
        return listPos - mStaticItemCount;
    }

    private int getListPosition(int ringtoneManagerPos) {

        // If the manager position is -1 (for not found), return that
        if (ringtoneManagerPos < 0) return ringtoneManagerPos;

        return ringtoneManagerPos + mStaticItemCount;
    }

    /**
     * Add my ringtone item to given listview and return it's position.
     *
     * @param listView The listview which need to add more ringtone item.
     * @return The position of more ringtone item in listview
     */
	private int addMyRingtonesItem(ListView listView) {

		myRingtonesTitle = (LinearLayout) getLayoutInflater().inflate(
				R.layout.my_ringtones_title_layout, listView, false);
		myRingtonesList = (ListView) myRingtonesTitle
				.findViewById(R.id.my_ringtones_listview);
		mMyCursor = mRingtoneManager.getMyRingtones();
		mMyRingArrayList = new ArrayList<RingListItem>();

		Log.i(TAG,
				"addMyRine>>>: myRingtonesList len = "
						+ mMyRingArrayList.size());

		mMyRingAdapter = new RingtoneAdapter(this,false);
		fillRingArrayList(mMyRingAdapter, mMyRingArrayList, mMyCursor, false);
		// myAdapter.addAll(mMyRingArrayList);

		myRingtonesList.setAdapter(mMyRingAdapter);
		myRingtonesList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		myRingtonesList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == mSelectMyRingtonesPos) {
					final Intent intent = new Intent("com.alcatel.music5.TRACK_PICKER");
					startActivityForResult(intent, MORE_RINGTONE_REQUEST);
					return;
				}
			}
		});
		mSelectMyRingtonesPos = addSelectRingtonesItem(myRingtonesList);

		listView.addHeaderView(myRingtonesTitle);
		mStaticItemCount++;

		setListViewHeight(myRingtonesList);
		return listView.getHeaderViewsCount() - 1;
	}

    private int addSelectRingtonesItem(ListView listView) {
        LinearLayout selectRingtones = (LinearLayout) getLayoutInflater().inflate(
                R.layout.select_my_ringtone_layout, listView, false);

        listView.addHeaderView(selectRingtones);
//        mStaticItemCount++;
        return listView.getHeaderViewsCount() - 1;
    }

    /**
     * Add Local ringtone item to given listview and return it's position.
     *
     * @param listView The listview which need to add more ringtone item.
     * @return The position of more ringtone item in listview
     */
    private int addLocalRingtonesItem(ListView listView) {
        LinearLayout localRingtonesTitle = (LinearLayout) getLayoutInflater().inflate(
                R.layout.local_ringtones_title_layout, listView, false);

        listView.addHeaderView(localRingtonesTitle);
        mStaticItemCount++;
        return listView.getHeaderViewsCount() - 1;
    }

	private void setListViewHeight(ListView listView) {
		Log.i(TAG, "setListViewHeight");
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}
		int totalHeight = 0;
		Log.i(TAG, "list item count=" + listAdapter.getCount());
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);
			Log.i(TAG, "list item height3=" + listItem.getMeasuredHeight());
			totalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount())) + 38;
		listView.setLayoutParams(params);
	}

    /**
     *
     * @param resolver content resolver
     * @param uri the given uri to set to be ringtones
     */
    private void setRingtone(ContentResolver resolver, Uri uri) {
        /// Set the flag in the database to mark this as a ringtone
        try {
            ContentValues values = new ContentValues(1);
            if (RingtoneManager.TYPE_RINGTONE == mType) {
                values.put(MediaStore.Audio.Media.IS_RINGTONE, "1");
            } else if (RingtoneManager.TYPE_ALARM == mType) {
                values.put(MediaStore.Audio.Media.IS_ALARM, "1");
            } else if (RingtoneManager.TYPE_NOTIFICATION == mType) {
                values.put(MediaStore.Audio.Media.IS_NOTIFICATION, "1");
            } else {
                return;
            }
            resolver.update(uri, values, null, null);
            /// Restore the new uri and set it to be checked after resume
            mExistingUri = uri;
            mSelectedRingtoneUri = uri;
        } catch (UnsupportedOperationException ex) {
            /// most likely the card just got unmounted
            Log.e(TAG, "couldn't set ringtone flag for uri " + uri);
        } catch(IllegalArgumentException ex){
            Log.e(TAG, "couldn't set ringtone flag for uri " + uri);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /// When activity first start, just return. Only when restart we need to refresh in resume.
        if (!mNeedRefreshOnResume) {
            return;
        }
        ListView listView = mAlert.getListView();
        mLocalRingtoneList = listView;
        if (null == listView) {
            Log.e(TAG, "onResume: listview is null, return!");
            return;
        }
        mCursor.requery();
        fillRingArrayList(mLocalRingAdapter,mLocalRingArrayList,mCursor,true);

        mMyCursor.requery();
        fillRingArrayList(mMyRingAdapter,mMyRingArrayList,mMyCursor,false);
		notifyListDataChange(mLocalRingtoneList);
		notifyListDataChange(myRingtonesList);
		Log.i(TAG, "onResume>>>: myRingtonesList len = " + mMyRingArrayList.size());

        mNeedRefreshOnResume = false;

        if (mHaseMyRingtonesItem) {
        	setListViewHeight(myRingtonesList);
        }
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        if (mMyCursor != null && !mMyCursor.isClosed()) {
            mMyCursor.close();
        }
        if (mFxCursor != null && !mFxCursor.isClosed()) {
        	mFxCursor.close();
        }
        super.onDestroy();
    }

    private void fillFXRingList(Cursor cursor){
    	if (cursor == null)
			return;
    	if(mFXRingList == null){
    		mFXRingList = new ArrayList<FXRingInfo>();
    	}
    	while (cursor.moveToNext()) {
    		FXRingInfo item = new FXRingInfo();
    		item.mId = cursor.getLong(cursor
					.getColumnIndex(MediaStore.Audio.Media._ID));
    		item.mFXUri = ContentUris.withAppendedId(
					MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
					item.mId);
    		item.mNormalID = cursor.getLong(cursor
					.getColumnIndex(MediaStore.Audio.Media.MIX_NORMAL_ID));
    		item.mFXType = cursor.getInt(cursor
					.getColumnIndex(MediaStore.Audio.Media.MIX_TYPE));
    		mFXRingList.add(item);
    	}
    }

	private void fillRingArrayList(RingtoneAdapter adapter,
			ArrayList<RingListItem> list, Cursor cursor, boolean local) {
		if (cursor == null || list == null)
			return;
		boolean exists;
		if (cursor.moveToFirst()) {
			do {
				exists = false;
				long curID = cursor.getLong(cursor
						.getColumnIndex(MediaStore.Audio.Media._ID));
				Iterator<RingListItem> iterator = list.iterator();
				while (iterator.hasNext()) {
					RingListItem itemExist = iterator.next();
					if (itemExist.mRingID == curID) {
						exists = true;
						break;
					}
				}
				if (!exists) {
					RingListItem item = new RingListItem();
					item.mRingID = curID;
					item.mRingTitle = cursor.getString(cursor
							.getColumnIndex(MediaStore.Audio.Media.TITLE));
					if (local) {
						item.mRingUri = ContentUris.withAppendedId(
								MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
								item.mRingID);
						if(mFXRingList != null){
							Iterator<FXRingInfo> iteratorFX = mFXRingList.iterator();
							while (iteratorFX.hasNext()) {
								FXRingInfo itemFX = iteratorFX.next();
								if (itemFX.mNormalID == curID) {
									if (itemFX.mFXType == Audio.AudioColumns.MixType.MIX01) {
										item.mFX1Info = itemFX;
									} else if (itemFX.mFXType == Audio.AudioColumns.MixType.MIX02) {
										item.mFX2Info = itemFX;
									} else if (itemFX.mFXType == Audio.AudioColumns.MixType.MIX03) {
										item.mFX3Info = itemFX;
									}
								}
							}
						}
					} else {
						item.mRingUri = ContentUris.withAppendedId(
								MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
								item.mRingID);
					}

					list.add(item);
					adapter.add(item);
				}
			} while (cursor.moveToNext());
		}
	}

	private long findNoramlID(Uri url) {
		long normalID = -1;
		if (mFXRingList != null) {
			Iterator<FXRingInfo> iterator = mFXRingList.iterator();
			while (iterator.hasNext()) {
				FXRingInfo item = iterator.next();
				if (url.toString().equals(item.mFXUri.toString())) {
					normalID = item.mNormalID;
					break;
				}
			}
		}
		Log.i(TAG,"findNoramlID normalID="+normalID);
		return normalID;
	}

    class ViewHolder {
    	CheckedTextView mCheckView;
        RelativeLayout mRingtoneItem;
        ImageView mExpander;
        Spinner mSpinnerFX;
    }

    class RingListItem {
    	long mRingID;
    	boolean mSelected;
    	String mRingTitle;
    	Uri mRingUri;
    	FXRingInfo mSelectFXInfo;
    	FXRingInfo mFX1Info;
    	FXRingInfo mFX2Info;
    	FXRingInfo mFX3Info;
    }

    class FXRingInfo {
    	long mId;
    	long mNormalID;
    	int mFXType;
    	Uri mFXUri;
    }

    private void notifyListDataChange(ListView listView){
        ListAdapter localAdapter = listView.getAdapter();
        if (null != localAdapter && (localAdapter instanceof HeaderViewListAdapter)) {
            /// Get the cursor adapter with the listview
        	localAdapter = ((HeaderViewListAdapter) localAdapter).getWrappedAdapter();
            ((RingtoneAdapter) localAdapter).notifyDataSetChanged();
        }
    }

	private class RingtoneAdapter extends ArrayAdapter<RingListItem> implements
			View.OnClickListener {
		boolean mLocalRing;
		Context mContext;

		public RingtoneAdapter(Context context, boolean localRing) {
			super(context, android.R.layout.simple_list_item_2);
			mContext = context;
			mLocalRing = localRing;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.my_ringtones_item_singlechoice, parent, false);

				holder = new ViewHolder();

				holder.mRingtoneItem = (RelativeLayout) convertView
						.findViewById(R.id.ringtone_item);
		        View fxContainer = convertView
						.findViewById(R.id.FXContainer);
		        CheckedTextView chkView = (CheckedTextView) getLayoutInflater().inflate(
		                com.android.internal.R.layout.select_dialog_singlechoice_material, null, false);
		        chkView.setLines(1);
				RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				lp.addRule(RelativeLayout.LEFT_OF, fxContainer.getId());
				holder.mRingtoneItem.addView(chkView,lp);
				holder.mCheckView = chkView;//(CheckedTextView) convertView
//				.findViewById(R.id.ringtone_chk);

				holder.mRingtoneItem.setOnClickListener(this);

				holder.mSpinnerFX = (Spinner) convertView.findViewById(R.id.spinFX);
				if (mLocalRing) {
					ArrayAdapter<CharSequence> adapter = ArrayAdapter
							.createFromResource(mContext, R.array.musicFXs,
									android.R.layout.simple_spinner_item);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					holder.mSpinnerFX.setAdapter(adapter);

				} else {
					holder.mSpinnerFX.setVisibility(View.GONE);
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			RingListItem item = (RingListItem) getItem(position);

			holder.mCheckView.setText(item.mRingTitle);
			 if(item.mRingTitle.contains(CRICKET_TITLE)){
				 holder.mSpinnerFX.setVisibility(View.GONE);
			 }
            // add by haifeng.zhai for defect 2012061 begin
            if (item.mFX1Info == null && item.mFX2Info == null
                    && item.mFX3Info == null) {
                holder.mSpinnerFX.setVisibility(View.GONE);
            } else {
                if (mLocalRing) {
                    holder.mSpinnerFX.setVisibility(View.VISIBLE);
                }
            }
            // add by haifeng.zhai for defect 2012061 end
			 //Delete by haifeng.zhai for defect 1856196 begin
			 //else{
			 // holder.mSpinnerFX.setVisibility(View.VISIBLE);
			 //}
			//Delete by haifeng.zhai for defect 1856196 end
			boolean fxRingSelected = false;
			if (mLocalRing) {
				int fxType;
				if (item.mFX1Info != null && mSelectedRingtoneUri != null
						&& item.mFX1Info.mFXUri.toString().equals(
								mSelectedRingtoneUri.toString())) {
					fxType = Audio.AudioColumns.MixType.MIX01;
					item.mSelectFXInfo = item.mFX1Info;
				} else if (item.mFX2Info != null && mSelectedRingtoneUri != null
						&& item.mFX2Info.mFXUri.toString().equals(
								mSelectedRingtoneUri.toString())) {
					fxType = Audio.AudioColumns.MixType.MIX02;
					item.mSelectFXInfo = item.mFX2Info;
				} else if (item.mFX3Info != null && mSelectedRingtoneUri != null
						&& item.mFX3Info.mFXUri.toString().equals(
								mSelectedRingtoneUri.toString())) {
					fxType = Audio.AudioColumns.MixType.MIX03;
					item.mSelectFXInfo = item.mFX3Info;
				} else{
					fxType = Audio.AudioColumns.MixType.MIX_NORMAL;
				}
				// setSelection should not invoke OnItemSelectedListener except User choose manual
				// so null listener
				holder.mSpinnerFX.setOnItemSelectedListener(null);
				if (fxType > 0) {
					fxRingSelected = true;
					holder.mSpinnerFX.setSelection(fxType, true);
				} else if (item.mSelectFXInfo != null) {
					holder.mSpinnerFX.setSelection(item.mSelectFXInfo.mFXType,
							true);
				} else {
					holder.mSpinnerFX.setSelection(0, true);
				}
				holder.mSpinnerFX
						.setOnItemSelectedListener(new OnItemSelectedListener() {
							@Override
							public void onItemSelected(AdapterView<?> parent,
									View view, int position, long id) {
								RingListItem ringItemInfo = (RingListItem) parent
										.getTag();
								switch (position) {
								case Audio.AudioColumns.MixType.MIX01:
									ringItemInfo.mSelectFXInfo = ringItemInfo.mFX1Info;
									break;
								case Audio.AudioColumns.MixType.MIX02:
									ringItemInfo.mSelectFXInfo = ringItemInfo.mFX2Info;
									break;
								case Audio.AudioColumns.MixType.MIX03:
									ringItemInfo.mSelectFXInfo = ringItemInfo.mFX3Info;
									break;
								case Audio.AudioColumns.MixType.MIX_NORMAL:
									ringItemInfo.mSelectFXInfo = null;
									break;
								}
								if (ringItemInfo.mSelected) {
									if (ringItemInfo.mSelectFXInfo != null) {
										mSelectedRingtoneUri = ringItemInfo.mSelectFXInfo.mFXUri;
									} else {
										mSelectedRingtoneUri = ringItemInfo.mRingUri;
									}
									mExistingUri = mSelectedRingtoneUri;
						            ((AudioManager) getSystemService(AUDIO_SERVICE)).requestAudioFocus(mAudioFocusChangeListener, AudioManager.STREAM_MUSIC,
						                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
									playRingtone(POS_UNKNOWN, 0);
								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub

							}
						});
			}
			if (mSelectedRingtoneUri != null
					&& item.mRingUri.toString().equals(
							mSelectedRingtoneUri.toString()) || fxRingSelected) {
				holder.mCheckView.setChecked(true);
				item.mSelected = true;
			} else {
				holder.mCheckView.setChecked(false);
				item.mSelected = false;
			}
			holder.mSpinnerFX.setTag(item);
			holder.mRingtoneItem.setTag(item);
			return convertView;
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.ringtone_item:
				RingListItem ringItemInfo = (RingListItem) v.getTag();
				mSelectedRingtoneUri = ringItemInfo.mRingUri;
				if (mLocalRing) {
					if (ringItemInfo.mSelectFXInfo != null) {
						mSelectedRingtoneUri = ringItemInfo.mSelectFXInfo.mFXUri;
					} else {
						mSelectedRingtoneUri = ringItemInfo.mRingUri;
					}
					Log.i(TAG, "mSelectedRingtoneUri fx="
							+ mSelectedRingtoneUri);
				}
				mExistingUri = mSelectedRingtoneUri;
				if (mClickedPos == mSilentPos) {
					mLocalRingtoneList.setItemChecked(mClickedPos, false);
					mClickedPos = -1;
				}
	            ((AudioManager) getSystemService(AUDIO_SERVICE)).requestAudioFocus(mAudioFocusChangeListener, AudioManager.STREAM_MUSIC,
	                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
				playRingtone(POS_UNKNOWN, 0);
				Log.i(TAG, "onItemClick mExistingUri=" + mSelectedRingtoneUri);
				notifyListDataChange(mLocalRingtoneList);
				notifyListDataChange(myRingtonesList);
				break;
			}
		}
    }
}

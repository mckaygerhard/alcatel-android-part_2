package com.android.providers.drm;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import android.app.NotificationManager;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.drm.DrmStore;
import android.drm.mobile1.DrmException;
import android.drm.mobile1.DrmRights;
import android.drm.mobile1.DrmRightsManager;
import android.provider.Telephony;
import android.util.Log;
import android.util.TctLog;

import com.tct.drm.TctDrmManagerClient;
import com.tct.drm.TctDrmStore;

public class DrmPushReceiver extends BroadcastReceiver {
    private static final String TAG = "DrmPushReceiver";
    private TctDrmManagerClient mDrmClient;
    private final int RIGHT_NOTIFICATION_ID = 32;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Telephony.Sms.Intents.WAP_PUSH_RECEIVED_ACTION)) {
            String rightMimeType = intent.getType();
            if (DrmRightsManager.DRM_MIMETYPE_RIGHTS_XML_STRING.equals(rightMimeType) ||
                DrmRightsManager.DRM_MIMETYPE_RIGHTS_WBXML_STRING.equals(rightMimeType)) {
                Log.i(TAG,"onReceived mimeType = " + rightMimeType);
                byte[] rightData = (byte[]) intent.getExtra("data");
                if (rightData == null) {
                    Log.e(TAG, "The rights data is invalid.");
                    return;
                }
                ByteArrayInputStream rightDataStream = new ByteArrayInputStream(rightData);
                try {
                    if (mDrmClient == null) {
                        mDrmClient = TctDrmManagerClient.getInstance(context);
                    }
                    mDrmClient.saveRights(new android.drm.DrmRights(rightData, TctDrmStore.MimeType.DRM_MIMETYPE_SD_STRING), null, null);
                } catch (IOException e) {
                    Log.e(TAG, "IOException occurs when install drm rights.");
                    return;
                }

                NotificationManager nm;
                nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Notification rightNotify = new Notification(android.R.drawable.stat_sys_download_done,
                context.getResources().getText(com.android.internal.R.string.NotifyDrmContent), System.currentTimeMillis());
                rightNotify.defaults |= Notification.DEFAULT_SOUND;
                rightNotify.flags|=Notification.FLAG_AUTO_CANCEL;
                rightNotify.setLatestEventInfo(context, context.getResources().getText(com.android.internal.R.string.NotifyDrmTitle),
                context.getResources().getText(com.android.internal.R.string.NotifyDrmContent), null);
                nm.notify(RIGHT_NOTIFICATION_ID, rightNotify);
                Log.d(TAG, "Install drm rights successfully.");
                return;
            }
            Log.d(TAG, "This is not drm rights push mimetype.");
        }
        Log.d(TAG, "This is not wap push received action.");
    }
}

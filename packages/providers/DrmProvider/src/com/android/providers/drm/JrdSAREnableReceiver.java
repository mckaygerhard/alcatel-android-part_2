package com.android.providers.drm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.android.internal.telephony.TelephonyIntents;

public class JrdSAREnableReceiver extends BroadcastReceiver {
    private final String TAG = "JrdSAREnableReceiver";
    private Uri sarUri = Uri.parse("android_secret_code://727");

    public JrdSAREnableReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equals(TelephonyIntents.SECRET_CODE_ACTION)) {
                Uri uri = intent.getData();
                if (uri.equals(sarUri)) {
                    Intent in = new Intent("android.intent.action.TCT_SAR");
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(in);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}

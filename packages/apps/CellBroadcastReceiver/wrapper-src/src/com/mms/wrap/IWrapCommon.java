package com.mms.wrap;

import android.content.Context;
import android.telephony.SmsManager;

interface IWrapCommon {
    String TAG = WrapConstants.TAG;

    /**[CB]After airplane mode, mobile can not receive CMAS message.PR582740*/
    void setCellBroadcastRangesEmpty();

    /**new CLID variable to re-activate CB for NL,772564*/
    void getCellBroadcastConfig(SmsManager manager);

    /**configuration menu to disable or enable USERPIN,595961*/
    int getSettingSystemMessageUserpinValue(Context context);

    /**configuration menu to disable or enable USERPIN,595961*/
    void putSettingSystemMessageUserpinValue(Context context, int value);

    /**Wap message enable,487411*/
    int getSettingSystemWapMessageValue(Context context);

    /**Wap message enable,487411*/
    void putSettingSystemWapMessageValue(Context context, int value);

    /**Enable making a direct call when put the cellphone near your ear*/
    int getSettingSystemFlipMonitorValue(Context context);

    /**Enable making a direct call when put the cellphone near your ear*/
    void putSettingSystemFlipMonitorValue(Context context, int value);
}

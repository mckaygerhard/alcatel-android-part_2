package com.mms.wrap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.content.Context;

interface IWrapQcom extends IWrapCommon {
    /**515368 The Layout of Jellybean in Israel update*/
    String getTctDigitsAsteriskAndPlusOnly(Matcher matcher);

    /**515368 The Layout of Jellybean in Israel update*/
    Pattern getTctIsraelPattern();

    /*get perso from tct frameworks*/
    public boolean getBooleanFromFrameworksPlf(Context context,String key);
}

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.cellbroadcastreceiver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Telephony;
import android.telephony.CellBroadcastMessage;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;

import java.util.ArrayList;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
import android.content.ActivityNotFoundException;
import android.database.CursorWrapper;
import android.net.Uri;
import android.view.ActionMode;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * This activity provides a list view of received cell broadcasts. Most of the work is handled
 * in the inner CursorLoaderListFragment class.
 */
public class CellBroadcastListActivity extends Activity {

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
	private static final String TAG = "CellBroadcastListActivity";
    public static HashSet<Long> messageCheckedStatus = new HashSet<Long>();
    public static ArrayList<Long> selectedMsgIds = null;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    public static boolean mDuplicateCheckDeletedRecords = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Dismiss the notification that brought us here (if any).
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE))
                .cancel(CellBroadcastAlertService.NOTIFICATION_ID);

        FragmentManager fm = getFragmentManager();
        mDuplicateCheckDeletedRecords = getResources().getBoolean(R.bool.config_regional_wea_duplicated_check_deleted_records);

        // Create the list fragment and add it as our sole content.
        if (fm.findFragmentById(android.R.id.content) == null) {
            CursorLoaderListFragment listFragment = new CursorLoaderListFragment();
            fm.beginTransaction().add(android.R.id.content, listFragment).commit();
        }
    }

    /**
     * List fragment queries SQLite database on worker thread.
     */
    public static class CursorLoaderListFragment extends ListFragment
            implements LoaderManager.LoaderCallbacks<Cursor> {

        // IDs of the main menu items.
        private static final int MENU_DELETE_ALL           = 3;
        private static final int MENU_PREFERENCES          = 4;

        // IDs of the context menu items (package local, accessed from inner DeleteThreadListener).
        static final int MENU_DELETE               = 0;
        static final int MENU_VIEW_DETAILS         = 1;

        // This is the Adapter being used to display the list's data.
        CursorAdapter mAdapter;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        private static String mOrderBy = "service_category ASC,date DESC";

        private ImageButton deleteButton;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            //[BUGFIX]-Add-BEGIN by-shenglong-fang,2016-07-28,defect- 2603742
            // We have a menu item to show in action bar.
            boolean isShowCellbroadcastSettingsMenu = getResources().getBoolean(R.bool.def_hide_Cellbroadcast_settings_from_menu);
            setHasOptionsMenu(isShowCellbroadcastSettingsMenu);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.cell_broadcast_list_screen, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            // Set context menu for long-press.
            ListView listView = getListView();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            //listView.setOnCreateContextMenuListener(mOnCreateContextMenuListener);
		  	listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            listView.setMultiChoiceModeListener(new CellBroadcastMultiChoiceModeListener());
            deleteButton = (ImageButton)getView().findViewById(R.id.delete_button);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            // Create a cursor adapter to display the loaded data.
            mAdapter = new CellBroadcastCursorAdapter(getActivity(), null);
            setListAdapter(mAdapter);

            // Prepare the loader.  Either re-connect with an existing one,
            // or start a new one.
            getLoaderManager().initLoader(0, null, this);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            menu.add(0, MENU_DELETE_ALL, 0, R.string.menu_delete_all).setIcon(
                    android.R.drawable.ic_menu_delete);
            if (UserManager.get(getActivity()).isAdminUser()) {
                menu.add(0, MENU_PREFERENCES, 0, R.string.menu_preferences).setIcon(
                        android.R.drawable.ic_menu_preferences);
            }
        }

        @Override
        public void onPrepareOptionsMenu(Menu menu) {
            menu.findItem(MENU_DELETE_ALL).setVisible(!mAdapter.isEmpty());
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            CellBroadcastListItem cbli = (CellBroadcastListItem) v;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            boolean cmasBroadcastAuthority = getResources().getBoolean(R.bool.def_cmasBroadcastAuthority);
            if(isCMAS() && cmasBroadcastAuthority){
               return;
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            showDialogAndMarkRead(cbli.getMessage());
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        private boolean isCMAS(){
            Cursor cursor = mAdapter.getCursor();
            /*MODIFIED-BEGIN by feng.cao, 2016-04-09,BUG-1877913*/
            if((CellBroadcastMessage.createFromCursor(cursor).getServiceCategory() >=4370 && CellBroadcastMessage.createFromCursor(cursor).getServiceCategory() <= 4381)
            || (CellBroadcastMessage.createFromCursor(cursor).getServiceCategory() >=4383 && CellBroadcastMessage.createFromCursor(cursor).getServiceCategory() <= 4395)){
            /*MODIFIED-END by feng.cao,BUG-1877913*/
                return true;
            }
            return false;
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            Uri listUri = CellBroadcastContentProvider.CONTENT_URI;
            if(getResources().getBoolean(R.bool.config_regional_wea_show_presidential_alert)) {
                listUri = CellBroadcastContentProvider.PRESIDENT_PIN_URI;
            }
            if(mDuplicateCheckDeletedRecords) {
               //return new CursorLoader(getActivity(), listUri,
               //    Telephony.CellBroadcasts.QUERY_COLUMNS, CellBroadcastDatabaseHelper.MESSAGE_DELETED + "=0", null,
               //    Telephony.CellBroadcasts.DELIVERY_TIME + " ASC");
               return new SortedCursorLoader(getActivity(), listUri,
                   Telephony.CellBroadcasts.QUERY_COLUMNS, CellBroadcastDatabaseHelper.MESSAGE_DELETED + "=0", null,
                   mOrderBy);
            }

            //[CMAS]Presidential alerts does not display on the top of other alerts
             /*   return new CursorLoader(getActivity(), CellBroadcastContentProvider.CONTENT_URI,
                    Telephony.CellBroadcasts.QUERY_COLUMNS, null, null,
                    Telephony.CellBroadcasts.DELIVERY_TIME + " DESC");*/
            return new SortedCursorLoader(getActivity(), listUri,
                    Telephony.CellBroadcasts.QUERY_COLUMNS, null, null,
                    mOrderBy);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            // Swap the new cursor in.  (The framework will take care of closing the
            // old cursor once we return.)
            mAdapter.swapCursor(data);
            getActivity().invalidateOptionsMenu();
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            // This is called when the last Cursor provided to onLoadFinished()
            // above is about to be closed.  We need to make sure we are no
            // longer using it.
            mAdapter.swapCursor(null);
        }

        private void showDialogAndMarkRead(CellBroadcastMessage cbm) {
            // show emergency alerts with the warning icon, but don't play alert tone
            Intent i = new Intent(getActivity(), CellBroadcastAlertDialog.class);
            ArrayList<CellBroadcastMessage> messageList = new ArrayList<CellBroadcastMessage>(1);
            messageList.add(cbm);
            i.putParcelableArrayListExtra(CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA, messageList);
            startActivity(i);
        }

        private void showBroadcastDetails(CellBroadcastMessage cbm) {
            // show dialog with delivery date/time and alert details
            CharSequence details = CellBroadcastResources.getMessageDetails(getActivity(), cbm);
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.view_details_title)
                    .setMessage(details)
                    .setCancelable(true)
                    .show();
        }

        private final OnCreateContextMenuListener mOnCreateContextMenuListener =
                new OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu menu, View v,
                            ContextMenuInfo menuInfo) {
                        menu.setHeaderTitle(R.string.message_options);
                        menu.add(0, MENU_VIEW_DETAILS, 0, R.string.menu_view_details);
                        menu.add(0, MENU_DELETE, 0, R.string.menu_delete);
                    }
                };

        @Override
        public boolean onContextItemSelected(MenuItem item) {
            Cursor cursor = mAdapter.getCursor();
            if (cursor != null && cursor.getPosition() >= 0) {
                switch (item.getItemId()) {
                    case MENU_DELETE:
                        confirmDeleteThread(cursor.getLong(cursor.getColumnIndexOrThrow(
                                Telephony.CellBroadcasts._ID)));
                        break;

                    case MENU_VIEW_DETAILS:
                        showBroadcastDetails(CellBroadcastMessage.createFromCursor(cursor));
                        break;

                    default:
                        break;
                }
            }
            return super.onContextItemSelected(item);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch(item.getItemId()) {
                case MENU_DELETE_ALL:
                    confirmDeleteThread(-1);
                    break;

                case MENU_PREFERENCES:

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                    boolean isGandalfVersion = this.getResources().getBoolean(R.bool.def_show_smscb_channels_on);
                    android.util.Log.i(TAG, "isGandalfVersion:"+isGandalfVersion);
                    try {
                        if(isGandalfVersion) {
                            Intent intent = new Intent(getActivity(), CBCMASSettingActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getActivity(), CellBroadcastSettings.class);
                            startActivity(intent);
                        }
                    } catch(ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                    break;

                default:
                    return true;
            }
            return false;
        }

        /**
         * Start the process of putting up a dialog to confirm deleting a broadcast.
         * @param rowId the row ID of the broadcast to delete, or -1 to delete all broadcasts
         */
        public void confirmDeleteThread(long rowId) {
            DeleteThreadListener listener = new DeleteThreadListener(rowId);
            confirmDeleteThreadDialog(listener, (rowId == -1), getActivity());
        }

        /**
         * Build and show the proper delete broadcast dialog. The UI is slightly different
         * depending on whether there are locked messages in the thread(s) and whether we're
         * deleting a single broadcast or all broadcasts.
         * @param listener gets called when the delete button is pressed
         * @param deleteAll whether to show a single thread or all threads UI
         * @param context used to load the various UI elements
         */
        public static void confirmDeleteThreadDialog(DeleteThreadListener listener,
                boolean deleteAll, Context context) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setIconAttribute(android.R.attr.alertDialogIcon)
                    .setCancelable(true)
                    .setPositiveButton(R.string.button_delete, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .setMessage(deleteAll ? R.string.confirm_delete_all_broadcasts
                            : R.string.confirm_delete_broadcast)
                    .show();
        }

        public class DeleteThreadListener implements OnClickListener {
            private final long mRowId;

            public DeleteThreadListener(long rowId) {
                mRowId = rowId;
            }

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                // delete from database on a background thread
                new CellBroadcastContentProvider.AsyncCellBroadcastTask(
                        getActivity().getContentResolver()).execute(
                        new CellBroadcastContentProvider.CellBroadcastOperation() {
                            @Override
                            public boolean execute(CellBroadcastContentProvider provider) {
                                if (mRowId != -1) {
                                    if(mDuplicateCheckDeletedRecords) {
                                       return provider.markItemDeleted(mRowId);
                                    }
                                    return provider.deleteBroadcast(mRowId);
                                } else {
                                    if(mDuplicateCheckDeletedRecords) {
                                        return provider.markAllItemsDeleted();
                                    }
                                    return provider.deleteAllBroadcasts();
                                }
                            }
                        });

                dialog.dismiss();
            }
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        //[CMAS]Presidential alerts does not display on the top of other alerts
        static class SortedCursorLoader extends CursorLoader {

            public SortedCursorLoader(Context context, Uri uri, String[] projection,
                                      String selection, String[] selectionArgs, String sortOrder) {
                super(context, uri, projection, selection, selectionArgs, sortOrder);
            }

            @Override
            public Cursor loadInBackground() {
                return new SortedCursorWrapper(super.loadInBackground());
            }

            class SortedCursorWrapper extends CursorWrapper {
                private int mPos = 0;
                ArrayList<SortEntry> mEntries = new ArrayList<SortEntry>(15);

                public SortedCursorWrapper(Cursor cursor) {
                    super(cursor);

                    if (cursor != null && cursor.getCount() != 0) {
                        int i = 0;
                        while (cursor.moveToNext()) {
                            SortEntry mEntry = new SortEntry();
                            mEntry.order = i;
                            mEntry.mRead = cursor.getInt(cursor
                                    .getColumnIndex(Telephony.CellBroadcasts.MESSAGE_READ));
                            if (!cursor.isNull(cursor
                                    .getColumnIndex(Telephony.CellBroadcasts.CMAS_MESSAGE_CLASS))) {
                                mEntry.isPresidential = cursor
                                        .getInt(cursor.getColumnIndex(Telephony.CellBroadcasts.CMAS_MESSAGE_CLASS)) == 0;
                            }

                            mEntry.date = cursor.getInt(cursor
                                    .getColumnIndex(Telephony.CellBroadcasts.DELIVERY_TIME));
                            mEntries.add(mEntry);
                            i++;
                        }

                        Collections.sort(mEntries, new SortComparator());
                    }
                }

                @Override
                public boolean move(int offset) {
                    return moveToPosition(mPos + offset);
                }
                @Override
                public boolean moveToFirst() {
                    return moveToPosition(0);
                }

                @Override
                public boolean moveToLast() {
                    return moveToPosition(getCount() - 1);
                }

                @Override
                public boolean moveToPosition(int position) {
                    if (position >= 0 && position < mEntries.size()) {
                        mPos = position;
                        int realOrder = mEntries.get(position).order;
                        return mCursor.moveToPosition(realOrder);
                    } else if (position < 0) {
                        mPos = -1;
                    } else {
                        mPos = mEntries.size();
                    }
                    return mCursor.moveToPosition(mPos);
                }

                @Override
                public boolean moveToNext() {
                    return moveToPosition(mPos + 1);
                }

                @Override
                public int getPosition() {
                    return mPos;
                }

                @Override
                public boolean moveToPrevious() {
                    return moveToPosition(mPos - 1);
                }
            }

            class SortComparator implements Comparator {

                @Override
                public int compare(Object lhs, Object rhs) {
                    int ret = 0;
                    SortEntry mLeftEntry = (SortEntry) lhs;
                    SortEntry mRightEntry = (SortEntry) rhs;
                    //Log.d("CMAS", "left: " + mLeftEntry);
                    //Log.d("CMAS", "right: " + mRightEntry);
                    if (mLeftEntry.isPresidential && !mRightEntry.isPresidential) {
                        ret = -1;
                        //Log.d("CMAS", "<isPresidential> is different");
                    } else if (!mLeftEntry.isPresidential && mRightEntry.isPresidential) {
                        ret = 1;
                        //Log.d("CMAS", "<isPresidential> is different");
                    } else {
                        // both are Presidential
                        // compare the read flag first, 0: unread, 1:read
                        if (mLeftEntry.mRead < mRightEntry.mRead) {
                            //Log.d("CMAS", "<mRead> is different");
                            ret = -1;
                        } else if (mLeftEntry.mRead > mRightEntry.mRead) {
                            ret = 1;
                            //Log.d("CMAS", "<mRead> is different");
                        } else {
                            // read is same, compare the time, bigger is later
                            if (mLeftEntry.date == mRightEntry.date) {
                                ret = 0;
                                //Log.d("CMAS", "<all are same>");
                            } else {
                                //Log.d("CMAS", "<date> is different");
                                ret = (mLeftEntry.date > mRightEntry.date ? -1 : 1);
                            }
                        }
                    }
                    //Log.d("CMAS", "compare result: " + ret);
                    return ret;
                }

            }
            class SortEntry {
                int mRead;
                boolean isPresidential;
                int date;
                int order;

                public String toString() {
                    return "read: " + mRead
                            + " isPresidential: " + (isPresidential ? "true" : "false")
                            + " date: " + date
                            + " order: " + order;
                }
            }
        }
        //[Defect 976485]-Add-END by TCTSH.gang-chen@tcl.com-2015/11/25


        //[Defect 996142]-Add-BEGIN by TCTSH.gang-chen@tcl.com, 2015/12/3,refer to PR-578732
        class CellBroadcastMultiChoiceModeListener implements AbsListView.MultiChoiceModeListener {

            private View actionBarView = null;
            private TextView selectedMsgCount = null;
            //private ArrayList<Long> selectedMsgIds = null;//[BUGFIX]-Delete-by TSCD.tianming.lei,02/10/2015,PR-892380
            ActionMode mode;

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                //BEGIN by feng.cao for FR-1697035,03/16/2016
                boolean cmasBroadcastAuthority = getResources().getBoolean(R.bool.def_cmasBroadcastAuthority);
                if(cmasBroadcastAuthority){
                    if(!isCMAS()){
                        if(null ==  selectedMsgIds){
                            selectedMsgIds = new ArrayList<Long>();
                        }
                        //[BUGFIX]-Mod-END-by TSCD.tianming.lei

                        this.mode = mode;
                        deleteButton.setVisibility(View.VISIBLE);
                        deleteButton.setOnClickListener(deleteListener);

                        if (actionBarView == null) {
                            actionBarView = LayoutInflater.from(getActivity()).inflate(
                                    R.layout.cellbroadcast_message_list_multi_select_actionbar, null);
                        }

                        selectedMsgCount = (TextView) (actionBarView.findViewById(R.id.selected_msg_count));
                        TextView actiontext =(TextView)actionBarView.findViewById(R.id.action_bar_title);
                        actiontext.setText(R.string.action_bar_title_choose_messages);

                        mode.setCustomView(actionBarView);

                        return true;
                    }
                    return false;
                }
                //END by feng.cao for FR-1697035,03/16/2016
                //[BUGFIX]-Mod-BEGIN-by TSCD.tianming.lei,02/10/2015,PR-892380
                if(null == selectedMsgIds){
                    selectedMsgIds = new ArrayList<Long>();
                }
                //[BUGFIX]-Mod-END-by TSCD.tianming.lei

                this.mode = mode;
                deleteButton.setVisibility(View.VISIBLE);
                deleteButton.setOnClickListener(deleteListener);

                if (actionBarView == null) {
                    actionBarView = LayoutInflater.from(getActivity()).inflate(
                            R.layout.cellbroadcast_message_list_multi_select_actionbar, null);
                }

                selectedMsgCount = (TextView) (actionBarView.findViewById(R.id.selected_msg_count));
                TextView actiontext =(TextView)actionBarView.findViewById(R.id.action_bar_title);
                actiontext.setText(R.string.action_bar_title_choose_messages);

                mode.setCustomView(actionBarView);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // TODO Auto-generated method stub
                return false;
            }
            View.OnClickListener deleteListener = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    int size = selectedMsgIds.size();
                    if (size > 0) {
                        String message;
                        if (size == 1) {
                            message = getActivity().getString(R.string.one_selected_message_to_be_deleted);
                        } else {
                            message = getActivity().getString(R.string.multi_selected_message_to_be_deleted, size);
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.confirm_dialog_title)
                                .setIconAttribute(android.R.attr.alertDialogIcon)
                                .setCancelable(true)
                                .setPositiveButton(
                                        R.string.button_delete,
                                        new DeleteMessageListener(selectedMsgIds
                                                .toArray(new Long[0])))
                                .setNegativeButton(R.string.button_cancel, null)
                                .setMessage(message)
                                .show();
                    }
                    mode.finish();
                }
            };
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                selectedMsgIds = null;
                deleteButton.setVisibility(View.GONE);
            }

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
                                                  boolean checked) {
                ListView listView = getListView();
              //BEGIN by feng.cao for PR-1877913,04/06/2016
                boolean cmasBroadcastAuthority = getResources().getBoolean(R.bool.def_cmasBroadcastAuthority);
                if(isCMAS() && cmasBroadcastAuthority){
                   if(checked) {
                      listView.setItemChecked(position, false);
                   }
                   return;
                }
                //END by feng.cao for PR-1877913,04/06/2016
                selectedMsgCount.setText(Integer.toString(listView.getCheckedItemCount()));
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                long messageId = cursor
                        .getLong(cursor.getColumnIndex(Telephony.CellBroadcasts._ID));
                long date = cursor.getLong(cursor.getColumnIndex(Telephony.CellBroadcasts.DELIVERY_TIME));
                if (checked) {
                    selectedMsgIds.add(messageId);
                    messageCheckedStatus.add(date);
                } else {
                    selectedMsgIds.remove(messageId);
                    messageCheckedStatus.remove(date);
                }


            }

            public class DeleteMessageListener implements OnClickListener {
                private final Long[] mRowIds;

                public DeleteMessageListener(Long[] rowIds) {
                    mRowIds = rowIds;
                }

                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    // delete from database on a background thread
                    new CellBroadcastContentProvider.AsyncCellBroadcastTask(
                            getActivity().getContentResolver()).execute(
                            new CellBroadcastContentProvider.CellBroadcastOperation() {
                                @Override
                                public boolean execute(CellBroadcastContentProvider provider) {
                                    for (long id : mRowIds) {
                                        provider.deleteBroadcast(id);
                                    }
                                    return true;
                                }
                            });

                    dialog.dismiss();
                }
            }

        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }
}

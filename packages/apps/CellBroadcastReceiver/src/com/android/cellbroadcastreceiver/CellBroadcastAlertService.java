/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ==========================================================================
 *     Modifications on Features list / Changes Request / Problems Report
 * --------------------------------------------------------------------------
 *    date   |        author        |         Key          |     comment
 * ----------|----------------------|----------------------|-----------------
 * ----------|----------------------|----------------------|-----------------
 * 09/28/2015|      gang-chen       |      Task 528385     |[HOMO][HOMO]CB must
 *           |                      |                      |be disabled in 3G
 * ----------|----------------------|----------------------|------------------
 * 10/10/2015|      gang-chen       |      Task528385      |[Pre-CTS][CB]Header
 *           |                      |                      |for CB message
 *           |                      |                      |in notification
 *           |                      |                      |panel should be changed
 * ----------|----------------------|----------------------|-----------------
/* 19/07/2016|    jianglong.pan     |SOLUTION-2520283      |Porting CMASS    
/* ----------|----------------------|----------------------|----------------- 
 *****************************************************************************/

package com.android.cellbroadcastreceiver;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.ActivityManagerNative;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.SystemProperties;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.CellBroadcastMessage;
import android.telephony.TelephonyManager;
import android.telephony.SmsCbCmasInfo;
import android.telephony.SmsCbEtwsInfo;
import android.telephony.SmsCbLocation;
import android.telephony.SmsCbMessage;
import android.telephony.SubscriptionManager;
import android.telephony.CarrierConfigManager;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.PowerManager;
import android.os.SystemProperties;
import com.android.internal.telephony.PhoneConstants;

import java.util.List;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;

/**
 * This service manages the display and animation of broadcast messages.
 * Emergency messages display with a flashing animated exclamation mark icon,
 * and an alert tone is played when the alert is first shown to the user
 * (but not when the user views a previously received broadcast).
 */
public class CellBroadcastAlertService extends Service {
    private static final String TAG = "CellBroadcastAlertService";

    /** Intent action to display alert dialog/notification, after verifying the alert is new. */
    static final String SHOW_NEW_ALERT_ACTION = "cellbroadcastreceiver.SHOW_NEW_ALERT";

    /** Use the same notification ID for non-emergency alerts. */
    static final int NOTIFICATION_ID = 1;

    /** Sticky broadcast for latest area info broadcast received. */
    static final String CB_AREA_INFO_RECEIVED_ACTION =
            "android.cellbroadcastreceiver.CB_AREA_INFO_RECEIVED";
    /** system property to enable/disable broadcast duplicate detecion.  */
    private static final String CB_DUP_DETECTION = "persist.cb.dup_detection";

    /** Check for system property to enable/disable duplicate detection.  */
    static boolean mUseDupDetection = SystemProperties.getBoolean(CB_DUP_DETECTION, true);

    /** Channel 50 Cell Broadcast. */
    static final int CB_CHANNEL_50 = 50;

    /** Channel 60 Cell Broadcast. */
    static final int CB_CHANNEL_60 = 60;
    private static int TIME12HOURS = 12*60*60*1000;
    private boolean mDuplicateCheckDatabase = false;

    @Override
    public void onCreate() {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        context = this;
        cbmList = new ArrayList<CellBroadcastMessage>();
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        super.onCreate();
        mDuplicateCheckDatabase = getResources().getBoolean(
                R.bool.config_regional_wea_duplicated_check_database);
        if (mDuplicateCheckDatabase) {
            initHalfDayCmasList();
        }
    }

    private static final String COUNTRY_BRAZIL = "br";
    private static final String COUNTRY_INDIA = "in";

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
	/**Intent action to verify Chile Alert is Alerting or not*/
    static final String CHILE_ALERT_RETRUE_ACTION = "cellbroadcastreceiver.CHILE_ALERT_RETRUE";

    //CBC notification with pop up and tone alert + vibrate in CHILE
    public static final int CHANNEL1 = 919;
    public static final int CHANNEL2 = 921;
    public static final int CHANNEL3 = 4370;
    public static final int CHANNEL4 = 50;
    Boolean forceVibrateChile = false;

    public static final int MESSAGE_TYPE_COMMON_CELLBROADCAST = 0;
    public static final int MESSAGE_TYPE_ETWS_CELLBROADCAST = 1;
    public static final int MESSAGE_TYPE_CMAS_PRESIDENTITAL_CELLBROADCAST = 3;
    public static final int MESSAGE_TYPE_CMAS_OTHER_CELLBROADCAST = 2;

    static List<CellBroadcastMessage> cbmList = null;
    private static Context context = null;

    private boolean isChileAlerting = false;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /**
     *  Container for service category, serial number, location, body hash code, and ETWS primary/
     *  secondary information for duplication detection.
     */
    private static final class MessageServiceCategoryAndScope {
        private final int mServiceCategory;
        private final int mSerialNumber;
        private final SmsCbLocation mLocation;
        private final int mBodyHash;
        private final boolean mIsEtwsPrimary;
        private final SmsCbEtwsInfo mEtwsWarningInfo;
        private final long mDeliveryTime;
        private final String mMessageBody;

        MessageServiceCategoryAndScope(int serviceCategory, int serialNumber,
                SmsCbLocation location, int bodyHash, boolean isEtwsPrimary) {
            mServiceCategory = serviceCategory;
            mSerialNumber = serialNumber;
            mLocation = location;
            mBodyHash = bodyHash;
            mIsEtwsPrimary = isEtwsPrimary;
            mEtwsWarningInfo = null;
            mMessageBody = null;
            mDeliveryTime = 0;
        }

        MessageServiceCategoryAndScope(int serviceCategory, int serialNumber,
                SmsCbLocation location, int bodyHash, boolean isEtwsPrimary,
                SmsCbEtwsInfo etwsWarningInfo) {
            mServiceCategory = serviceCategory;
            mSerialNumber = serialNumber;
            mLocation = location;
            mBodyHash = bodyHash;
            mIsEtwsPrimary = isEtwsPrimary;
            mEtwsWarningInfo = etwsWarningInfo;
            mMessageBody = null;
            mDeliveryTime = 0;
        }

        MessageServiceCategoryAndScope(int serviceCategory, int serialNumber,
                SmsCbLocation location, String messageBody, long deliveryTime,
                int bodyHash, boolean isEtwsPrimary) {
            mServiceCategory = serviceCategory;
            mSerialNumber = serialNumber;
            mLocation = location;
            mMessageBody = messageBody;
            mDeliveryTime = deliveryTime;
            mBodyHash = bodyHash;
            mIsEtwsPrimary = isEtwsPrimary;
            mEtwsWarningInfo = null;
        }

        @Override
        public int hashCode() {
            if (mEtwsWarningInfo != null) {
                return mEtwsWarningInfo.hashCode() + mLocation.hashCode() + 5 * mServiceCategory
                        + 7 * mSerialNumber + 13 * mBodyHash;
            }
            return mLocation.hashCode() + 5 * mServiceCategory + 7 * mSerialNumber + 13 * mBodyHash;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof MessageServiceCategoryAndScope) {
                MessageServiceCategoryAndScope other = (MessageServiceCategoryAndScope) o;
                if (mEtwsWarningInfo == null && other.mEtwsWarningInfo != null) {
                    return false;
                } else if (mEtwsWarningInfo != null && other.mEtwsWarningInfo == null) {
                    return false;
                } else if (mEtwsWarningInfo != null && other.mEtwsWarningInfo != null
                        && !mEtwsWarningInfo.equals(other.mEtwsWarningInfo)) {
                    return false;
                }
                return (mServiceCategory == other.mServiceCategory &&
                        mSerialNumber == other.mSerialNumber &&
                        mLocation.equals(other.mLocation) &&
                        mBodyHash == other.mBodyHash &&
                        mIsEtwsPrimary == other.mIsEtwsPrimary &&
                        ((mMessageBody == null) ? (other.mMessageBody == null)
                        : (mMessageBody.equals(other.mMessageBody))));
            }
            return false;
        }

        @Override
        public String toString() {
            return "{mServiceCategory: " + mServiceCategory + " serial number: " + mSerialNumber
                    + " location: " + mLocation.toString() + " mEtwsWarningInfo: "
                    + (mEtwsWarningInfo == null ? "NULL" : mEtwsWarningInfo.toString())
                    + " body hash: " + mBodyHash + " mIsEtwsPrimary: " + mIsEtwsPrimary +'}';
        }
    }

    /** Cache of received message IDs, for duplicate message detection. */
    private static final HashSet<MessageServiceCategoryAndScope> sCmasIdSet =
            new HashSet<MessageServiceCategoryAndScope>(8);

    /** Maximum number of message IDs to save before removing the oldest message ID. */
    private static final int MAX_MESSAGE_ID_SIZE = 65535;

    /** List of message IDs received, for removing oldest ID when max message IDs are received. */
    private static final ArrayList<MessageServiceCategoryAndScope> sCmasIdList =
            new ArrayList<MessageServiceCategoryAndScope>(8);

    /** Index of message ID to replace with new message ID when max message IDs are received. */
    private static int sCmasIdListIndex = 0;
    /** List of message IDs received for recent 12 hours. */
    private static final ArrayList<MessageServiceCategoryAndScope> s12HIdList =
        new ArrayList<MessageServiceCategoryAndScope>(8);

    private void initHalfDayCmasList() {
        long now = System.currentTimeMillis();
        // This is used to query necessary fields from cmas table
        // which are related duplicate check
        // for example receive date, cmas id and so on
        String[] project = new String[] {
            Telephony.CellBroadcasts.PLMN,
            Telephony.CellBroadcasts.LAC,
            Telephony.CellBroadcasts.CID,
            Telephony.CellBroadcasts.DELIVERY_TIME,
            Telephony.CellBroadcasts.SERVICE_CATEGORY,
            Telephony.CellBroadcasts.SERIAL_NUMBER,
            Telephony.CellBroadcasts.MESSAGE_BODY};
        Cursor cursor = getApplicationContext().getContentResolver().query(
                Telephony.CellBroadcasts.CONTENT_URI,project,
                Telephony.CellBroadcasts.DELIVERY_TIME + ">?",
                new String[]{now - TIME12HOURS + ""},
                Telephony.CellBroadcasts.DELIVERY_TIME + " DESC");
        if (s12HIdList != null) {
            s12HIdList.clear();
        }
        MessageServiceCategoryAndScope newCmasId;
        int serviceCategory;
        int serialNumber;
        String messageBody;
        long deliveryTime;
        if(cursor != null){
            int plmnColumn = cursor.getColumnIndex(Telephony.CellBroadcasts.PLMN);
            int lacColumn = cursor.getColumnIndex(Telephony.CellBroadcasts.LAC);
            int cidColumn = cursor.getColumnIndex(Telephony.CellBroadcasts.CID);
            int serviceCategoryColumn = cursor.getColumnIndex(
                    Telephony.CellBroadcasts.SERVICE_CATEGORY);
            int serialNumberColumn = cursor.getColumnIndex(
                    Telephony.CellBroadcasts.SERIAL_NUMBER);
            int messageBodyColumn = cursor.getColumnIndex(Telephony.CellBroadcasts.MESSAGE_BODY);
            int deliveryTimeColumn = cursor.getColumnIndex(
                    Telephony.CellBroadcasts.DELIVERY_TIME);
            while(cursor.moveToNext()){
                String plmn = getStringColumn(plmnColumn, cursor);
                int lac = getIntColumn(lacColumn, cursor);
                int cid = getIntColumn(cidColumn, cursor);
                SmsCbLocation location = new SmsCbLocation(plmn, lac, cid);
                serviceCategory = getIntColumn(serviceCategoryColumn, cursor);
                serialNumber = getIntColumn(serialNumberColumn, cursor);
                messageBody = getStringColumn(messageBodyColumn, cursor);
                deliveryTime = getLongColumn(deliveryTimeColumn, cursor);
                newCmasId = new MessageServiceCategoryAndScope(
                        serviceCategory, serialNumber, location, messageBody,
                        deliveryTime, messageBody.hashCode(), false);
                s12HIdList.add(newCmasId);
            }
        }
        if(cursor != null){
            cursor.close();
        }
    }

    private boolean isDuplicated(SmsCbMessage message) {
        if(!mDuplicateCheckDatabase) {
            return false ;
        }
        final CellBroadcastMessage cbm = new CellBroadcastMessage(message);
        long lastestDeliveryTime = cbm.getDeliveryTime();
        int hashCode = message.isEtwsMessage() ? message.getMessageBody().hashCode() : 0;
        MessageServiceCategoryAndScope newCmasId = new MessageServiceCategoryAndScope(
                message.getServiceCategory(), message.getSerialNumber(),
                message.getLocation(), message.getMessageBody(), lastestDeliveryTime,
                hashCode, false);
        Iterator<MessageServiceCategoryAndScope> iterator = s12HIdList.iterator();
        ArrayList<MessageServiceCategoryAndScope> tempMessageList =
                new ArrayList<MessageServiceCategoryAndScope>();
        boolean duplicatedMessage = false;
        while(iterator.hasNext()){
            MessageServiceCategoryAndScope tempMessage =
                    (MessageServiceCategoryAndScope)iterator.next();
            boolean moreThan12Hour = (lastestDeliveryTime - tempMessage
                    .mDeliveryTime >= TIME12HOURS);
            if (moreThan12Hour) {
                s12HIdList.remove(message);
                break;
            } else {
                tempMessageList.add(tempMessage);
                if (tempMessage.equals(newCmasId)) {
                    duplicatedMessage = true;
                    break;
                }
            }
        }
        if (duplicatedMessage) {
            if (tempMessageList != null) {
                tempMessageList.clear();
                tempMessageList = null;
            }
            return true;
        } else {
            if (s12HIdList != null) {
                s12HIdList.clear();
            }
            if (tempMessageList != null) {
                s12HIdList.addAll(tempMessageList);
                tempMessageList.clear();
                tempMessageList = null;
            }
            s12HIdList.add(0, newCmasId);
        }
        return false;
    }

    private String getStringColumn (int column, Cursor cursor) {
        if (column != -1 && !cursor.isNull(column)) {
            return cursor.getString(column);
        } else {
            return null;
        }
    }

    private int getIntColumn (int column, Cursor cursor) {
        if (column != -1 && !cursor.isNull(column)) {
            return cursor.getInt(column);
        } else {
            return -1;
        }
    }

    private long getLongColumn (int column, Cursor cursor) {
        if (column != -1 && !cursor.isNull(column)) {
            return cursor.getLong(column);
        } else {
            return -1;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (Telephony.Sms.Intents.SMS_EMERGENCY_CB_RECEIVED_ACTION.equals(action) ||
                Telephony.Sms.Intents.SMS_CB_RECEIVED_ACTION.equals(action)) {
            handleCellBroadcastIntent(intent);
        } else if (SHOW_NEW_ALERT_ACTION.equals(action)) {
            try {
                if (UserHandle.myUserId() ==
                        ActivityManagerNative.getDefault().getCurrentUser().id) {
                    showNewAlert(intent);
                } else {
                    Log.d(TAG,"Not active user, ignore the alert display");
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        } else if(CHILE_ALERT_RETRUE_ACTION.equals(action)) {
            Log.d(TAG, "CHILE_ALERT_RETRUE_ACTION isChileAlerting before : "+isChileAlerting);
            isChileAlerting = intent.getBooleanExtra("isChileAlerting",false);
            Log.d(TAG, "currentCBMessage.getCmasMessageClass() isChileAlerting after: "+isChileAlerting);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        } else {
            Log.e(TAG, "Unrecognized intent action: " + action);
        }
        return START_NOT_STICKY;
    }

    private void handleCellBroadcastIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            Log.e(TAG, "received SMS_CB_RECEIVED_ACTION with no extras!");
            return;
        }

        SmsCbMessage message = (SmsCbMessage) extras.get("message");

        if (message == null) {
            Log.e(TAG, "received SMS_CB_RECEIVED_ACTION with no message extra");
            return;
        }

        final CellBroadcastMessage cbm = new CellBroadcastMessage(message);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        boolean isEnableSingleSIM = getBaseContext().getResources().getBoolean(R.bool.def_cellbroadcastreceiver_enable_single_sim);
        //int subId = intent.getExtras().getInt(PhoneConstants.SUBSCRIPTION_KEY);
        int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                SubscriptionManager.getDefaultSmsSubscriptionId());

        boolean isReceivedOnlyIn2G = getResources().getBoolean(R.bool.feature_onlyReceiveCbMessageIn2G_on);
        android.util.Log.d(TAG,"isReceivedOnlyIn2G = " + isReceivedOnlyIn2G);
        if (isReceivedOnlyIn2G) {
            TelephonyManager manager = android.telephony.TelephonyManager.getDefault();
            int voicenetworkType = manager.getVoiceNetworkType(subId);
            int networkClass = manager.getNetworkClass(voicenetworkType);
            String mccmnc = manager.getNetworkOperator(subId);

            android.util.Log.d(TAG, "subId = " + subId);
            android.util.Log.d(TAG, "voicenetworkType = " + voicenetworkType);
            android.util.Log.d(TAG, "networkClass = " + networkClass);
            android.util.Log.d(TAG, "mccmnc = " + mccmnc);

            if(networkClass != android.telephony.TelephonyManager.NETWORK_CLASS_2_G){ // MODIFIED by gang-chen, 2016-07-27,BUG-2256539
                android.util.Log.d(TAG, "current is not in 2G, skip the recieving message");
                return;
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (!isMessageEnabledByUser(cbm)) {
            Log.d(TAG, "ignoring alert of type " + cbm.getServiceCategory() +
                    " by user preference");
            return;
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        CBSUtills cbu;
        cbu = new CBSUtills(this);
        int k ;
        Log.w(TAG, "isEnableSingleSIM = " + isEnableSingleSIM + " SubId = " + subId);
        if(TelephonyManager.getDefault().isMultiSimEnabled() && isEnableSingleSIM){
            k = cbu.queryCBLanguage(PhoneConstants.SUB1);
        }else{
            k = cbu.queryCBLanguage();
        }

        Log.d(TAG,"cbLaCode cbm.queryCBLanguage:"+k);
        String cbLaCode = cbm.getLanguageCode();
        Log.d(TAG,"cbLaCode cbm.getLanguageCode:"+cbm.getLanguageCode());
        if ((!(languageDecode(k).equalsIgnoreCase(cbLaCode))) && (!("all".equalsIgnoreCase(languageDecode(k))))) {
            return;
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // If this is an ETWS message, then we want to include the body message to be a factor for
        // duplication detection. We found that some Japanese carriers send ETWS messages
        // with the same serial number, therefore the subsequent messages were all ignored.
        // In the other hand, US carriers have the requirement that only serial number, location,
        // and category should be used for duplicate detection.
        int hashCode = message.isEtwsMessage() ? message.getMessageBody().hashCode() : 0;

        if (mDuplicateCheckDatabase && isDuplicated(message)) {
            return;
        }
        // If this is an ETWS message, we need to include primary/secondary message information to
        // be a factor for duplication detection as well. Per 3GPP TS 23.041 section 8.2,
        // duplicate message detection shall be performed independently for primary and secondary
        // notifications.
        boolean isEtwsPrimary = false;
        if (message.isEtwsMessage()) {
            SmsCbEtwsInfo etwsInfo = message.getEtwsWarningInfo();
            if (etwsInfo != null) {
                isEtwsPrimary = etwsInfo.isPrimary();
            } else {
                Log.w(TAG, "ETWS info is not available.");
            }
        }

        //int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
        //        SubscriptionManager.getDefaultSmsSubscriptionId());
        if (subId == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            subId = SubscriptionManager.getDefaultSubscriptionId();
        }
        boolean carrierDisableDupDetection = false;
        CarrierConfigManager configManager =
                (CarrierConfigManager) getSystemService(Context.CARRIER_CONFIG_SERVICE);
        if (configManager != null) {
            PersistableBundle carrierConfig =
                configManager.getConfigForSubId(subId);
            if (carrierConfig != null) {
                carrierDisableDupDetection =
                    carrierConfig.getBoolean("carrier_disable_etws_cmas_dup_detection");
            }
        }

        if (mUseDupDetection && !carrierDisableDupDetection) {
            // Check for duplicate message IDs according to CMAS carrier requirements. Message IDs
            // are stored in volatile memory. If the maximum of 65535 messages is reached, the
            // message ID of the oldest message is deleted from the list.
            MessageServiceCategoryAndScope newCmasId = new MessageServiceCategoryAndScope(
                    message.getServiceCategory(), message.getSerialNumber(), message.getLocation(),
                    hashCode, isEtwsPrimary, message.getEtwsWarningInfo());

            Log.v(TAG,"newCmasId:" + newCmasId + " hash: " + newCmasId.hashCode()
                    + "body hash:" + hashCode);

            // Add the new message ID to the list. It's okay if this is a duplicate message ID,
            // because the list is only used for removing old message IDs from the hash set.
            if (sCmasIdList.size() < MAX_MESSAGE_ID_SIZE) {
                sCmasIdList.add(newCmasId);
            } else {
                // Get oldest message ID from the list and replace with the new message ID.
                MessageServiceCategoryAndScope oldestCmasId = sCmasIdList.get(sCmasIdListIndex);
                sCmasIdList.set(sCmasIdListIndex, newCmasId);
                Log.d(TAG, "message ID limit reached, removing oldest message ID " + oldestCmasId);
                // Remove oldest message ID from the set.
                sCmasIdSet.remove(oldestCmasId);
                if (++sCmasIdListIndex >= MAX_MESSAGE_ID_SIZE) {
                    sCmasIdListIndex = 0;
                }
            }
            // Set.add() returns false if message ID has already been added
            if (!sCmasIdSet.add(newCmasId)) {
                Log.d(TAG, "ignoring duplicate alert with " + newCmasId);
                return;
            }
        }

        final Intent alertIntent = new Intent(SHOW_NEW_ALERT_ACTION);
        alertIntent.setClass(this, CellBroadcastAlertService.class);
        alertIntent.putExtra("message", cbm);

        // write to database on a background thread
        new CellBroadcastContentProvider.AsyncCellBroadcastTask(getContentResolver())
                .execute(new CellBroadcastContentProvider.CellBroadcastOperation() {
                    @Override
                    public boolean execute(CellBroadcastContentProvider provider) {
                        if (provider.insertNewBroadcast(cbm)) {
                            // new message, show the alert or notification on UI thread
                            startService(alertIntent);
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
    }


//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
//[AT&T][CMAS(CELL BROADCAST)][LTE-BTR-5-4084/4098]:UE didn't receive the
//second Alert after Airplane mode ON and OFF
    public static void clearEmergencyMessageInHistory() {
        Log.i("CMAS", "airplane mode off, clear history");
        sCmasIdSet.clear();
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private void showNewAlert(Intent intent) {
        Bundle extras = intent.getExtras();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        forceVibrateChile = false;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (extras == null) {
            Log.e(TAG, "received SHOW_NEW_ALERT_ACTION with no extras!");
            return;
        }

        CellBroadcastMessage cbm = (CellBroadcastMessage) intent.getParcelableExtra("message");

        if (cbm == null) {
            Log.e(TAG, "received SHOW_NEW_ALERT_ACTION with no message extra");
            return;
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        //CBC notification with pop up and tone alert + vibrate in CHILE
        Log.i(TAG,"receive message from channel:" + Integer.toString(cbm.getServiceCategory()));
        if (getBaseContext().getResources().getBoolean(
                R.bool.feature_cellbroadcastreceiver_forceVibrateForChile_on)
                && (cbm.getServiceCategory() == CHANNEL1 || cbm.getServiceCategory() == CHANNEL2
                || (SystemProperties.getBoolean("ro.ssv.enabled", false) && cbm.getServiceCategory() == CHANNEL4))) {
            Log.i(TAG,"forcevibrate for chile when receive message from channel 919 or 921");
            forceVibrateChile = true;
        }

        if (CellBroadcastConfigService.isEmergencyAlertMessage(cbm)) {
            // start alert sound / vibration / TTS and display full-screen alert
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            if (getResources().getBoolean(R.bool.def_showCbMessageAlertByDate_on)) {
                Log.d(TAG,"bool.def_showCbMessageAlertByDate_on : true");
                openEmergencyAlertNotification(cbm);
            } else if (getResources().getBoolean(R.bool.def_cellbroadcastreceiver_alertOrderForTMO)) {
                boolean hasAlert = pref.getBoolean(CellBroadcastAlertFullScreen.HAS_SHOWING_ALERT, false);
                if(!hasAlert){
                    openEmergencyAlertNotification(cbm);
                    Log.d(TAG, "showNewAlert current no alert");
                } else {
                    //show new alert and add old message to list
                    cbmList.add(cbm);
                    Log.d(TAG,"add new cb to cbmList listsize = "+ cbmList.size());
                }
            } else {
                openEmergencyAlertNotification(cbm);
            }
        } else {
            //BUGZILLA-FR400297,
            if("true".equalsIgnoreCase(SystemProperties.get("ro.cb.channel50.brazil","false"))){
                Log.i(TAG, "#--- ro.cb.channel50.brazil =true ");
                if(cbm.getServiceCategory() == 50){
                     Log.i(TAG, "#--- Receive 50 channel message. msg : " + cbm.getMessageBody());
                     TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                     int simState = telManager.getSimState();
                     Log.i(TAG, "#- Sim state is : " + simState);
                     if(simState != TelephonyManager.SIM_STATE_READY){
                         Log.i(TAG, "#--- Sim state is : " + simState + " not show in the SPN/PLMN");
                         return;
                      }
                     long slotId = 0;
                     slotId = cbm.getSubId();
                     Intent channel50Intent = new Intent("com.jrdcom.action.CHANNEL_50_MSG");
                     channel50Intent.putExtra("channel_50_msg",cbm.getMessageBody());
                     channel50Intent.putExtra(PhoneConstants.SUBSCRIPTION_KEY, slotId);
                     sendBroadcast(channel50Intent);
                     return;
                  }
            }
            //CBC notification with pop up and tone alert + vibrate in CHILE
            if (forceVibrateChile) {
                openChileAlertNotification(cbm , forceVibrateChile );
            }
            else if(getBaseContext().getResources().getBoolean(
                    R.bool.feature_cellbroadcastreceiver_CBReceiverMode_on)){
                addToNotificationBarDisplay(cbm);
            }
            else {
                // add notification to the bar
                addToNotificationBar(cbm);
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    public static void showOtherAlert(){
        if(cbmList != null && cbmList.size() >0){
            CellBroadcastMessage cbm =cbmList.get(0);
            openNewEmergencyAlertNotification(cbm);
            cbmList.remove(0);
            Log.d(TAG,"ltm-- show new alert and remve it from list");
        }
    }

    public static void openNewEmergencyAlertNotification(CellBroadcastMessage message) {
        // Acquire a CPU wake lock until the alert dialog and audio start playing.
        Context mContext =CellBroadcastReceiverApp.getApplication();
        CellBroadcastAlertWakeLock.acquireScreenCpuWakeLock(mContext);

        // Close dialogs and window shade
        Intent closeDialogs = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        mContext.sendBroadcast(closeDialogs);

        // Decide which activity to start based on the state of the keyguard.
        Class<?> c = CellBroadcastAlertDialog.class;
        KeyguardManager km = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);
        if (km.inKeyguardRestrictedInputMode()) {
            // Use the full screen activity for security.
            c = CellBroadcastAlertFullScreen.class;
        }

        ArrayList<CellBroadcastMessage> messageList = new ArrayList<CellBroadcastMessage>(1);
        messageList.add(message);

        Intent alertDialogIntent = createDisplayMessageIntent(mContext, c, messageList);
        alertDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(alertDialogIntent);

    }

    private void addEmergencyToNotificationBar(CellBroadcastMessage message) {
        String messageBody = message.getMessageBody();

        Context context = CellBroadcastReceiverApp.getApplication();
        int unreadTotalMessages = 0;
        int highestLevel = MESSAGE_TYPE_COMMON_CELLBROADCAST;

        Cursor cursor = null;
        NotificationManager notificationManager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
        // step 1: check if there are unread message.
        try {
            cursor = context.getContentResolver().query(
                    Telephony.CellBroadcasts.CONTENT_URI,
                    new String[] {
                            Telephony.CellBroadcasts.MESSAGE_READ,
                    }, Telephony.CellBroadcasts.MESSAGE_READ + "=?", new String[]{"0"}, null);
            if (cursor != null) {
                unreadTotalMessages = cursor.getCount();
                cursor.close();
            }
            cursor = context.getContentResolver().query(
                    CellBroadcastContentProvider.CONTENT_URI,
                    Telephony.CellBroadcasts.QUERY_COLUMNS,
                    Telephony.CellBroadcasts.MESSAGE_READ + "=?", new String[] {"0"},
                    Telephony.CellBroadcasts.DELIVERY_TIME + " DESC");

            if(cursor != null) {
                // step 2: get highest emergency level
                while (cursor.moveToNext()) {

                    int level = MESSAGE_TYPE_COMMON_CELLBROADCAST;
                    if (cursor.isNull(cursor
                            .getColumnIndex(Telephony.CellBroadcasts.CMAS_MESSAGE_CLASS))) {
                        if (cursor.isNull(cursor
                                .getColumnIndex(Telephony.CellBroadcasts.ETWS_WARNING_TYPE))) {
                            level = MESSAGE_TYPE_COMMON_CELLBROADCAST;
                        } else {
                            level = MESSAGE_TYPE_ETWS_CELLBROADCAST;
                        }
                    } else {
                        int messageClass;
                        messageClass = cursor.getInt(cursor
                                .getColumnIndex(Telephony.CellBroadcasts.CMAS_MESSAGE_CLASS));
                        if (messageClass == SmsCbCmasInfo.CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT) {
                            highestLevel = MESSAGE_TYPE_CMAS_PRESIDENTITAL_CELLBROADCAST;
                            break;
                        } else {
                            level = MESSAGE_TYPE_CMAS_OTHER_CELLBROADCAST;
                        }
                    }
                    highestLevel = level > highestLevel ? level : highestLevel;
                }
            }
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Exception e) {
                }
            }
        }

        ArrayList<CellBroadcastMessage> messageList = new ArrayList<CellBroadcastMessage>(1);
        messageList.add(message);

        int channelTitleId = CellBroadcastResources.getDialogTitleResource(message);
        CharSequence channelName = getText(channelTitleId);

        // Create intent to show the new messages when user selects the notification.
        Intent intent = createDisplayMessageIntent(context, CellBroadcastListActivity.class, messageList);
        intent.putExtra(CellBroadcastAlertFullScreen.FROM_NOTIFICATION_EXTRA, true);

        PendingIntent pi = PendingIntent.getActivity(context, NOTIFICATION_ID, intent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_emergency_alert)
                .setTicker(channelName)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pi)
                .setDefaults(Notification.DEFAULT_ALL)
                .setDefaults(Notification.DEFAULT_LIGHTS);
        if(unreadTotalMessages>1){
            // use generic count of unread broadcasts if more than one unread
            builder.setContentTitle(getString(R.string.notification_multiple_title_cmas));
            builder.setContentText(getString(R.string.notification_multiple, unreadTotalMessages));
        } else {
            builder.setContentTitle(channelName).setContentText(messageBody);
        }
        int imageId = R.drawable.ic_emergency_alert;
        if (highestLevel == MESSAGE_TYPE_CMAS_PRESIDENTITAL_CELLBROADCAST) {
            imageId = R.drawable.ic_cmas_presidential_flash;
        }
        Notification noti = builder.build();
        noti.icon = imageId;
        notificationManager.notify(NOTIFICATION_ID, noti);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /**
     * Send broadcast twice, once for apps that have PRIVILEGED permission and
     * once for those that have the runtime one.
     * @param message the message to broadcast
     */
    private void broadcastAreaInfoReceivedAction(CellBroadcastMessage message) {
        Intent intent = new Intent(CB_AREA_INFO_RECEIVED_ACTION);

        intent.putExtra("message", message);
        sendBroadcastAsUser(intent, UserHandle.ALL,
                android.Manifest.permission.READ_PRIVILEGED_PHONE_STATE);
    }

    /**
     * Get preference setting for channel 60
     * @param message the message to check
     * @return true if channel 60 preference is set; false otherwise
     */
    private boolean getChannel60Preference(CellBroadcastMessage message) {
        String country = TelephonyManager.getDefault().
                getSimCountryIso(message.getSubId());

        boolean enable60Channel = SubscriptionManager.
                getResourcesForSubId(getApplicationContext(), message.
                        getSubId()).getBoolean(R.bool.show_india_settings) ||
                COUNTRY_INDIA.equals(country);

        return PreferenceManager.getDefaultSharedPreferences(this).
                getBoolean(CellBroadcastSettings.
                        KEY_ENABLE_CHANNEL_60_ALERTS, enable60Channel);
    }

    /**
     * Filter out broadcasts on the test channels that the user has not enabled,
     * and types of notifications that the user is not interested in receiving.
     * This allows us to enable an entire range of message identifiers in the
     * radio and not have to explicitly disable the message identifiers for
     * test broadcasts. In the unlikely event that the default shared preference
     * values were not initialized in CellBroadcastReceiverApp, the second parameter
     * to the getBoolean() calls match the default values in res/xml/preferences.xml.
     *
     * @param message the message to check
     * @return true if the user has enabled this message type; false otherwise
     */
    private boolean isMessageEnabledByUser(CellBroadcastMessage message) {

        // Check if all emergency alerts are disabled.
        boolean emergencyAlertEnabled = PreferenceManager.getDefaultSharedPreferences(this).
                getBoolean(CellBroadcastSettings.KEY_ENABLE_EMERGENCY_ALERTS, true);

        // Check if ETWS/CMAS test message is forced to disabled on the device.
        boolean forceDisableEtwsCmasTest =
                CellBroadcastSettings.isEtwsCmasTestMessageForcedDisabled(this);

        if (message.isEtwsTestMessage()) {
            return emergencyAlertEnabled &&
                    !forceDisableEtwsCmasTest &&
                    PreferenceManager.getDefaultSharedPreferences(this)
                    .getBoolean(CellBroadcastSettings.KEY_ENABLE_ETWS_TEST_ALERTS, false);
        }

        if (message.isEtwsMessage()) {
            // ETWS messages.
            // Turn on/off emergency notifications is the only way to turn on/off ETWS messages.
            return emergencyAlertEnabled;

        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        boolean enableRMTExerciseTestAlert = getResources().getBoolean(R.bool.def_enableRMTExerciseTestAlert);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (message.isCmasMessage()) {
            switch (message.getCmasMessageClass()) {
                case SmsCbCmasInfo.CMAS_CLASS_EXTREME_THREAT:
                    return emergencyAlertEnabled &&
                            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                            CellBroadcastSettings.KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS, true);

                case SmsCbCmasInfo.CMAS_CLASS_SEVERE_THREAT:
                    return emergencyAlertEnabled &&
                            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                            CellBroadcastSettings.KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS, true);

                case SmsCbCmasInfo.CMAS_CLASS_CHILD_ABDUCTION_EMERGENCY:
                    return emergencyAlertEnabled &&
                            PreferenceManager.getDefaultSharedPreferences(this)
                            .getBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_AMBER_ALERTS, true);

                case SmsCbCmasInfo.CMAS_CLASS_REQUIRED_MONTHLY_TEST:
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                	if(enableRMTExerciseTestAlert){
                		Log.d(TAG,"SmsCbCmasInfo.CMAS_CLASS_REQUIRED_MONTHLY_TEST!");
                		return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                                CellBroadcastSettings.KEY_ENABLE_CMAS_RMT_ALERTS + message.getSubId(), false);
                	}
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                case SmsCbCmasInfo.CMAS_CLASS_CMAS_EXERCISE:
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                	if(enableRMTExerciseTestAlert){
                		Log.d(TAG,"SmsCbCmasInfo.CMAS_CLASS_CMAS_EXERCISE!");
                		return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                                CellBroadcastSettings.KEY_ENABLE_CMAS_EXERCISE_ALERTS + message.getSubId(), false);
                	}
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                case SmsCbCmasInfo.CMAS_CLASS_OPERATOR_DEFINED_USE:
                    return emergencyAlertEnabled &&
                            !forceDisableEtwsCmasTest &&
                            PreferenceManager.getDefaultSharedPreferences(this)
                                    .getBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_TEST_ALERTS,
                                            false);
                default:
                    return true;    // presidential-level CMAS alerts are always enabled
            }
        }

        int serviceCategory = message.getServiceCategory();
        if (serviceCategory == CB_CHANNEL_50) {
            String country = TelephonyManager.getDefault().
                    getSimCountryIso(message.getSubId());
            // save latest area info broadcast for Settings display and send as
            // broadcast
            CellBroadcastReceiverApp.setLatestAreaInfo(message);
            broadcastAreaInfoReceivedAction(message);
            return !(COUNTRY_BRAZIL.equals(country) ||
                    COUNTRY_INDIA.equals(country));
        } else if (serviceCategory == CB_CHANNEL_60) {
            broadcastAreaInfoReceivedAction(message);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            /* MODIFIED-BEGIN by gang-chen, 2016-08-02,BUG-2256539*/
            boolean isIgnore50CbDialog = getResources().getBoolean(R.bool.def_brazil_50cb_ignore_dialog_on);
            Log.d("SCBBrazil", "isIgnore50CbDialog : " + isIgnore50CbDialog);
            boolean needIgnore = (isIgnore50CbDialog && (message.getServiceCategory() == CB_CHANNEL_50));

            //return getChannel60Preference(message);
            return ((!needIgnore) || getChannel60Preference(message));
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

        return true;    // other broadcast messages are always enabled
    }

    /**
     * Display a full-screen alert message for emergency alerts.
     * @param message the alert to display
     */
    private void openEmergencyAlertNotification(CellBroadcastMessage message) {
        // Acquire a CPU wake lock until the alert dialog and audio start playing.
        CellBroadcastAlertWakeLock.acquireScreenCpuWakeLock(this);

        // Close dialogs and window shade
        Intent closeDialogs = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeDialogs);

        // start audio/vibration/speech service for emergency alerts
        Intent audioIntent = new Intent(this, CellBroadcastAlertAudio.class);
        audioIntent.setAction(CellBroadcastAlertAudio.ACTION_START_ALERT_AUDIO);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/31/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        int duration;   // alert audio duration in ms
        if (message.isCmasMessage() && !getResources().getBoolean(R.bool.def_cellbroadcastreceiver_alertAudioRuleForTMO)) {
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
            // CMAS requirement: duration of the audio attention signal is 10.5 seconds.
            duration = 10500;
        } else {
            duration = Integer.parseInt(prefs.getString(
                    CellBroadcastSettings.KEY_ALERT_SOUND_DURATION,
                    CellBroadcastSettings.ALERT_SOUND_DEFAULT_DURATION)) * 1000;
        }
        audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_DURATION_EXTRA, duration);

        if (!getResources().getBoolean(
                R.bool.config_regional_presidential_wea_with_tone_vibrate)
                && message.isEtwsMessage()) {
            // For ETWS, always vibrate, even in silent mode.
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_VIBRATE_EXTRA, true);
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_EXTRA, true);
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_ETWS_VIBRATE_EXTRA, true);
        } else if ((getResources().getBoolean(
            R.bool.config_regional_presidential_wea_with_tone_vibrate))
            && (message.isCmasMessage())
            && (message.getCmasMessageClass()
                == SmsCbCmasInfo.CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT)){
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_VIBRATE_EXTRA, true);
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_TONE_EXTRA, true);
            audioIntent.putExtra(
                    CellBroadcastAlertAudio.ALERT_AUDIO_PRESIDENT_TONE_VIBRATE_EXTRA, true);
        } else {
            // For other alerts, vibration can be disabled in app settings.

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            //audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_VIBRATE_EXTRA,
            //        prefs.getBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_VIBRATE, true));

            boolean vibrateFlag = prefs.getBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_VIBRATE, true);
            boolean audioFlag = prefs.getBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_AUDIO, true);

            if (getResources().getBoolean(R.bool.def_vibrate_priority_system_cbs)) {
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                Log.i(TAG, "def_vibrate_priority_system_cbs =true, should set system high priority for vibrate, "
                                + "and audioManager.getRingerMode() = "
                                + audioManager.getRingerMode());
                if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
                    audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_VIBRATE_EXTRA, false);
                    audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_EXTRA, false);
                } else {
                    audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_VIBRATE_EXTRA, vibrateFlag);
                    audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_EXTRA, audioFlag);
                }
            } else {
                Log.i(TAG,"def_vibrate_priority_system_cbs = false, vibrate follow CBS!");
                audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_VIBRATE_EXTRA, vibrateFlag);
                audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_EXTRA, audioFlag);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

        if (getResources().getBoolean(
                    R.bool.config_regional_wea_alert_tone_enable)) {
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_TONE_EXTRA,
                    prefs.getBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_TONE, true));
        }

        String messageBody = message.getMessageBody();

        if (prefs.getBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_SPEECH, true)) {
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_MESSAGE_BODY, messageBody);

            String preferredLanguage = message.getLanguageCode();
            String defaultLanguage = null;
            if (message.isEtwsMessage()) {
                // Only do TTS for ETWS secondary message.
                // There is no text in ETWS primary message. When we construct the ETWS primary
                // message, we hardcode "ETWS" as the body hence we don't want to speak that out here.
                
                // Also in many cases we see the secondary message comes few milliseconds after
                // the primary one. If we play TTS for the primary one, It will be overwritten by
                // the secondary one immediately anyway.
                if (!message.getEtwsWarningInfo().isPrimary()) {
                    // Since only Japanese carriers are using ETWS, if there is no language specified
                    // in the ETWS message, we'll use Japanese as the default language.
                    defaultLanguage = "ja";
                }
            } else {
                // If there is no language specified in the CMAS message, use device's
                // default language.
                defaultLanguage = Locale.getDefault().getLanguage();
            }

            Log.d(TAG, "Preferred language = " + preferredLanguage +
                    ", Default language = " + defaultLanguage);
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_MESSAGE_PREFERRED_LANGUAGE,
                    preferredLanguage);
            audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_MESSAGE_DEFAULT_LANGUAGE,
                    defaultLanguage);
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        if (getBaseContext().getResources().getBoolean(
                R.bool.feature_cellbroadcastreceiver_forceVibrateForChile_on)
                && message.getServiceCategory() == CHANNEL3) {
            Log.i(TAG,"set chile presientialasert is true");
            audioIntent.putExtra("PresientialAlert",true);
        }

        //[Defect1239965]
        Log.i(TAG, "openEmergencyAlertNotification-isChileAlerting : " + isChileAlerting);
        if (getBaseContext().getResources().getBoolean(
                R.bool.feature_cellbroadcastreceiver_forceVibrateForChile_on)) {
            if (!isChileAlerting) {
                Log.d(TAG, "no Chile Alerting so start Audio alerting");
                if (message.getServiceCategory() == CHANNEL3 || message.getServiceCategory() == CHANNEL1 || message.getServiceCategory() == CHANNEL2) {
                    isChileAlerting = true;
                    Log.d(TAG, "Mark flag isChileAlerting as true");
                }
                Log.d(TAG, "55startService(audioIntent);");
                startService(audioIntent);
            } else
                Log.d(TAG, "a ChileAlert is Alerting so ignore startService(audioIntent);");

        } else {
            startService(audioIntent);
		}
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // Decide which activity to start based on the state of the keyguard.
        Class c = CellBroadcastAlertDialog.class;
        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if (km.inKeyguardRestrictedInputMode()) {
            // Use the full screen activity for security.
            c = CellBroadcastAlertFullScreen.class;
        }

        ArrayList<CellBroadcastMessage> messageList = new ArrayList<CellBroadcastMessage>(1);
        messageList.add(message);

        Intent alertDialogIntent = createDisplayMessageIntent(this, c, messageList);
        alertDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(alertDialogIntent);
    }

    /**
     * Add the new alert to the notification bar (non-emergency alerts), or launch a
     * high-priority immediate intent for emergency alerts.
     * @param message the alert to display
     */
    private void addToNotificationBar(CellBroadcastMessage message) {
        int channelTitleId = CellBroadcastResources.getDialogTitleResource(message);
        CharSequence channelName = getText(channelTitleId);
        String messageBody = message.getMessageBody();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        String poweronScreenEnable = android.provider.Settings.System.getString(CellBroadcastAlertService.context.getContentResolver(),"CBLightEnable");
        if (poweronScreenEnable != null && poweronScreenEnable.equals("on")) {
            int val=android.provider.Settings.System.getInt(getContentResolver(),
                    android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 15000);
            PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE, "CBMessageReceiverService");
            wl.acquire(val);
        }

        //Tone needed for CellBroadcast message received
        //[HOMO][HOMO]Cell Broadcast messages realization for qualcomm/MTK/Broadcom/Spr-
        //eadtrum/etc android smartphones
        //String cbringtone = "content://media/internal/audio/media/7";
        String cbringtone = null;

        Cursor c = this.getContentResolver().query(Uri.parse("content://" + "com.jrd.provider.CellBroadcast" + "/CBRingtone"), null,
                null, null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            cbringtone = c.getString(c
                    .getColumnIndex("cbringtone"));
        }
        Log.d(TAG, "cbringtone = "+cbringtone);
        c.close();

        // Pass the list of unread non-emergency CellBroadcastMessages
        ArrayList<CellBroadcastMessage> messageList = CellBroadcastReceiverApp
                .addNewMessageToList(message);

        // Create intent to show the new messages when user selects the notification.
        Intent intent = createDisplayMessageIntent(this, CellBroadcastAlertDialog.class,
                messageList);
        intent.putExtra(CellBroadcastAlertFullScreen.FROM_NOTIFICATION_EXTRA, true);

        PendingIntent pi = PendingIntent.getActivity(this, NOTIFICATION_ID, intent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

        // use default sound/vibration/lights for non-emergency broadcasts
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_notify_alert)
                .setTicker(channelName)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pi)
                .setCategory(Notification.CATEGORY_SYSTEM)
                .setPriority(Notification.PRIORITY_HIGH)
                .setColor(getResources().getColor(R.color.notification_color))
                .setVisibility(Notification.VISIBILITY_PUBLIC);
                //.setDefaults(Notification.DEFAULT_ALL);

        //builder.setDefaults(Notification.DEFAULT_ALL);

        // increment unread alert count (decremented when user dismisses alert dialog)
        // int unreadCount = messageList.size();
		//[Defect 1018978]Add-BEGIN by TCTSH.gang-chen@tcl.com 28/12/2015
		//for showing wrong unread num
        Cursor cursor = null;
        int unreadCount = 0;
        try{
            cursor = context.getContentResolver().query(
                    Telephony.CellBroadcasts.CONTENT_URI,
                    new String[] {Telephony.CellBroadcasts.MESSAGE_READ,},
                    Telephony.CellBroadcasts.MESSAGE_READ + "=?"+" and "+Telephony.CellBroadcasts.MESSAGE_PRIORITY + "=?", new String[]{"0","0"}, null);
            if (cursor != null) {
                unreadCount = cursor.getCount();
            }
        } catch(Exception e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //[Defect 1018978]Add-END by TCTSH.gang-chen@tcl.com 28/12/2015
        if (unreadCount > 1) {
           if(getBaseContext().getResources().getBoolean(
                R.bool.feature_cellbroadcastreceiver_mutilCBNotification_on)) {
                builder.setContentTitle(channelName).setContentText(messageBody);
            } else {
                // use generic count of unread broadcasts if more than one unread
                builder.setContentTitle(getString(R.string.notification_multiple_title_cb));
                builder.setContentText(getString(R.string.notification_multiple, unreadCount));
            }
        } else {
            if(getBaseContext().getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_displayChannelId)) {
                builder.setContentTitle(channelName+" "+"("+message.getServiceCategory()+")").setContentText(messageBody);
            } else {
                builder.setContentTitle(channelName).setContentText(messageBody);
            }
        }

        //Cell Broadcast messages realization for qualcomm/MTK/Broadcom/Spr-
        //eadtrum/etc android smartphones
        Notification noti = builder.build();

        if (cbringtone != null) {
            noti.sound = Uri.parse(cbringtone);
        }

        String vibrateWhen = android.provider.Settings.System.getString(CellBroadcastAlertService.context.getContentResolver(), "vibrateWhenCB");
        boolean vibrateAlways = false;
        boolean vibrateSilent = false;
        if (vibrateWhen != null) {
            vibrateAlways = vibrateWhen.equals("Always");
            vibrateSilent = vibrateWhen.equals("Silent");
        }
        AudioManager audioManager =
                (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
        boolean nowSilent =
                audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE;
        if (vibrateAlways || vibrateSilent && nowSilent) {
            noti.defaults |= Notification.DEFAULT_VIBRATE;
            Log.i(TAG,"noti.defaults is " + noti.defaults);
        }

        //Additional requirements for Cell Broadcast messages
        String LedEnable = android.provider.Settings.System.getString(CellBroadcastAlertService.context.getContentResolver(),"CBLedEnable");
        if( LedEnable != null && LedEnable.equals("on")) {
            noti.flags |= Notification.FLAG_SHOW_LIGHTS;
            noti.ledARGB = 0xff00ff00;
            noti.ledOnMS = 500;
            noti.ledOffMS = 2000;
        }
        //Click CB message in notification have no effect after click home key
        //light up screen and cb LED indicator cannot be work
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);

        notificationManager.notify(NOTIFICATION_ID, noti);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    static Intent createDisplayMessageIntent(Context context, Class intentClass,
            ArrayList<CellBroadcastMessage> messageList) {
        // Trigger the list activity to fire up a dialog that shows the received messages
        Intent intent = new Intent(context, intentClass);
        intent.putParcelableArrayListExtra(CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA, messageList);
        return intent;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;    // clients can't bind to this service
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    //CBC notification with pop up and tone alert + vibrate in CHILE
    // Chile request
    private void openChileAlertNotification(CellBroadcastMessage cbm , boolean forceVibrate ){
        // Acquire a CPU wake lock until the alert dialog and audio start playing.
        CellBroadcastAlertWakeLock.acquireScreenCpuWakeLock(this);

        // Close dialogs and window shade
        Intent closeDialogs = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeDialogs);

        //start audio and vibrate
        Log.i(TAG,"forcevibrate and specified audio for chile ,CellBroadcastAlertAudio ");
        Intent audioIntent = new Intent(this, CellBroadcastAlertAudio.class);
        audioIntent.setAction(CellBroadcastAlertAudio.ACTION_START_ALERT_AUDIO);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int duration = 10500; //10.5 seconds
        audioIntent.putExtra(CellBroadcastAlertAudio.ALERT_AUDIO_DURATION_EXTRA, duration);//声音响起的间隔
        audioIntent.putExtra("forceVibrate",forceVibrate);

        if (!isChileAlerting) {
            isChileAlerting = true;
            Log.d(TAG, "no Chile Alerting so start chile Alerting");
            startService(audioIntent);
        }else
            Log.d(TAG, "Chile Alerting so ignore startService(audioIntent);");

        // Decide which activity to start based on the state of the keyguard.
        Log.i(TAG,"pop up title, date, time for chile ,CellBroadcastAlertFullScreen ");
        Class c = CellBroadcastAlertDialog.class;
        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if (km.inKeyguardRestrictedInputMode()) {
            // Use the full screen activity for security.
            c = CellBroadcastAlertFullScreen.class;
        }

        ArrayList<CellBroadcastMessage> messageList = new ArrayList<CellBroadcastMessage>(1);
        messageList.add(cbm);

        Intent alertDialogIntent = createDisplayMessageIntent(this, c, messageList);
        alertDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        alertDialogIntent.putExtra("forceVibrate", forceVibrate);
        startActivity(alertDialogIntent);
    }

    private String languageDecode (int i) {
        if (i == 1) {
            return "de";
        } else if (i == 2) {
            return "en";
        } else if (i == 3) {
            return "it";
        } else if (i == 4) {
            return "fr";
        } else if (i == 5) {
            return "es";
        } else if (i == 6) {
            return "nl";
        } else if (i == 7) {
            return "sv";
        } else if (i == 8) {
            return "da";
        } else if (i == 9) {
            return "pt";
        } else if (i == 10) {
            return "fi";
        } else if (i == 11) {
            return "no";
        } else if (i == 12) {
            return "el";
        } else if (i == 13) {
            return "tr";
        } else if (i == 14) {
            return "hu";
        } else if (i == 15) {
            return "pl";
        } else if (i == 16) {
            return "ru";
        } else {
            return "all";
        }
    }

    /**
     * Add the new alert to the notification bar (non-emergency alerts), or launch a
     * high-priority immediate intent for emergency alerts.
     * It is just for Russia Beeln
     * @param message the alert to display
     */
    private void addToNotificationBarDisplay(CellBroadcastMessage message) {
        int channelTitleId = CellBroadcastResources.getDialogTitleResource(message);
        CharSequence channelName = getText(channelTitleId);
        String messageBody = message.getMessageBody();

        String poweronScreenEnable = android.provider.Settings.System.getString(CellBroadcastAlertService.context.getContentResolver(),"CBLightEnable");
        if (poweronScreenEnable != null && poweronScreenEnable.equals("on")) {
                int val=android.provider.Settings.System.getInt(getContentResolver(),
                                 android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 15000);
                PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                                | PowerManager.ON_AFTER_RELEASE, "CBMessageReceiverService");
                wl.acquire(val);
        }

        String cbringtone = null;

        Cursor c = this.getContentResolver().query(Uri.parse("content://" + "com.jrd.provider.CellBroadcast" + "/CBRingtone"), null,
                null, null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            cbringtone = c.getString(c
                    .getColumnIndex("cbringtone"));
        }
        c.close();
         int notificationid;
         int repeatnotification;
         ArrayList<CellBroadcastMessage> messageList = CellBroadcastReceiverApp
                .addNewMessageToListRussia(message);
         notificationid  = CellBroadcastReceiverApp.getNotificationId(message.getServiceCategory());
         repeatnotification = CellBroadcastReceiverApp.getRepeatNotification();
         if(repeatnotification!=0){
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
           notificationManager.cancel(repeatnotification);
           CellBroadcastReceiverApp.setDefaultRepeatnotification();//[defect1604193]Add-by gang-chen
         }
         Intent intent = createDisplayMessageIntent(this, CellBroadcastAlertDialog.class,
                messageList);
          intent.putExtra(CellBroadcastAlertFullScreen.FROM_NOTIFICATION_EXTRA, true);
          PendingIntent pi = PendingIntent.getActivity(this, notificationid, intent,
                 PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);
          // use default sound/vibration/lights for non-emergency broadcasts
          Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_notify_alert)
                .setTicker(channelName)
                .setWhen(System.currentTimeMillis())
                .setCategory(Notification.CATEGORY_SYSTEM)
                .setPriority(Notification.PRIORITY_HIGH)//[Defect1487577]Mod by gang-chen for p1-3
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setContentIntent(pi);

          //builder.setContentTitle(channelName).setContentText(messageBody);
          builder.setContentTitle(channelName + " " + "(" + message.getServiceCategory() + ")")//[Defect1539787]Mod-by gang-chen
                .setContentText(messageBody);

          //eadtrum/etc android smartphones
          Notification noti = builder.build();

          if (cbringtone != null) {
            noti.sound = Uri.parse(cbringtone);
          }

          String vibrateWhen = android.provider.Settings.System.getString(CellBroadcastAlertService.context.getContentResolver(), "vibrateWhenCB");
          boolean vibrateAlways = false;
          boolean vibrateSilent = false;
          if (vibrateWhen != null) {
              vibrateAlways = vibrateWhen.equals("Always");
              vibrateSilent = vibrateWhen.equals("Silent");
          }
          AudioManager audioManager =
              (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
          boolean nowSilent =
            audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE;
          if (vibrateAlways || vibrateSilent && nowSilent) {
              noti.defaults |= Notification.DEFAULT_VIBRATE;
          }

          //Additional requirements for Cell Broadcast messages
          String LedEnable = android.provider.Settings.System.getString(CellBroadcastAlertService.context.getContentResolver(),"CBLedEnable");
          if( LedEnable != null && LedEnable.equals("on")) {
              noti.flags |= Notification.FLAG_SHOW_LIGHTS;
              noti.ledARGB = 0xff00ff00;
              noti.ledOnMS = 500;
              noti.ledOffMS = 2000;
          }
          NotificationManager notificationManager =
              (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

          notificationManager.cancel(notificationid);

          notificationManager.notify(notificationid, noti);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}

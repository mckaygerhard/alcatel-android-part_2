/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ==========================================================================
 *     Modifications on Features list / Changes Request / Problems Report
 * --------------------------------------------------------------------------
 *    date   |        author        |         Key          |     comment
 * ----------|----------------------|----------------------|-----------------
 * ----------|----------------------|----------------------|-----------------
 * 10/10/2015|    gang-chen         |      Task528385      |[Pre-CTS][CB]Header
 *           |                      |                      |for CB message
 *           |                      |                      |in notification
 *           |                      |                      |panel should be changed
 * ----------|----------------------|----------------------|-----------------
 * 10/13/2015|    gang-chen         |      Task667593      |CBC notification with
 *           |                      |     Porting from     |pop up and tone alert
 *           |                      |       FR400297       |+ vibrate in CHILE
 * ----------|----------------------|----------------------|-----------------
 * 10/14/2015|    gang-chen         |      Task665954      |2nd Presidential Alert
 *           |                      |     Porting from     | Displayed on top of
 *           |                      |       #972021        |1st Alert
 * ----------|----------------------|----------------------|-----------------
/* 19/07/2016|    jianglong.pan     |SOLUTION-2520283      |Porting CMASS    
/* ----------|----------------------|----------------------|----------------- 
 *****************************************************************************/
package com.android.cellbroadcastreceiver;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.CellBroadcastMessage;
import android.telephony.SmsCbCmasInfo;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.util.Iterator;
import java.lang.Long;
import java.util.Map;
import java.text.SimpleDateFormat;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * Full-screen emergency alert with flashing warning icon.
 * Alert audio and text-to-speech handled by {@link CellBroadcastAlertAudio}.
 * Keyguard handling based on {@code AlarmAlertFullScreen} class from DeskClock app.
 */
public class CellBroadcastAlertFullScreen extends Activity {
    private static final String TAG = "CellBroadcastAlertFullScreen";

    /**
     * Intent extra for full screen alert launched from dialog subclass as a result of the
     * screen turning off.
     */
    static final String SCREEN_OFF_EXTRA = "screen_off";

    /** Intent extra for non-emergency alerts sent when user selects the notification. */
    static final String FROM_NOTIFICATION_EXTRA = "from_notification";

    /** List of cell broadcast messages to display (oldest to newest). */
    protected ArrayList<CellBroadcastMessage> mMessageList;

    /** Whether a CMAS alert other than Presidential Alert was displayed. */
    private boolean mShowOptOutDialog;

    /** Length of time for the warning icon to be visible. */
    private static final int WARNING_ICON_ON_DURATION_MSEC = 800;

    /** Length of time for the warning icon to be off. */
    private static final int WARNING_ICON_OFF_DURATION_MSEC = 800;

    /** Length of time to keep the screen turned on. */
    private static final int KEEP_SCREEN_ON_DURATION_MSEC = 60000;

    /** Animation handler for the flashing warning icon (emergency alerts only). */
    private final AnimationHandler mAnimationHandler = new AnimationHandler();

    /** Handler to add and remove screen on flags for emergency alerts. */
    private final ScreenOffHandler mScreenOffHandler = new ScreenOffHandler();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    /** List of non-PRESIDENT cell broadcast messages to display (oldest to newest). */
    public static ArrayList<CellBroadcastMessage> mCBMessageList;

    /** List of PRESIDENT cell broadcast messages to display (oldest to newest). */
    public static ArrayList<CellBroadcastMessage> mPresidentMessageList;

	/** Length of time to keep the screen turned on for Chile. */
    private static final int Chile_KEEP_SCREEN_ON_DURATION_MSEC = 10000;
    private boolean forceVibrateChile = false;

    public static final String HAS_SHOWING_ALERT = "hasShowingAlert";
    static CellBroadcastAlertFullScreen instance;
    private Context mContext;

    private CellBroadcastMessage currentCBMessage = null;

    private static final int MSGID1 = 4370;
    private static final int MSGID2 = 4371;
    private static final int MSGID9 = 4378;
    private static final int MSGID12 = 4381;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /**
     * Animation handler for the flashing warning icon (emergency alerts only).
     */
    private class AnimationHandler extends Handler {
        /** Latest {@code message.what} value for detecting old messages. */
        private final AtomicInteger mCount = new AtomicInteger();

        /** Warning icon state: visible == true, hidden == false. */
        private boolean mWarningIconVisible;

        /** The warning icon Drawable. */
        private Drawable mWarningIcon;

        /** The View containing the warning icon. */
        private ImageView mWarningIconView;

        /** Package local constructor (called from outer class). */
        AnimationHandler() {}

        /** Start the warning icon animation. */
        void startIconAnimation() {
            if (!initDrawableAndImageView()) {
                return;     // init failure
            }
            mWarningIconVisible = true;
            mWarningIconView.setVisibility(View.VISIBLE);
            updateIconState();
            queueAnimateMessage();
        }

        /** Stop the warning icon animation. */
        void stopIconAnimation() {
            // Increment the counter so the handler will ignore the next message.
            mCount.incrementAndGet();
            if (mWarningIconView != null) {
                mWarningIconView.setVisibility(View.GONE);
            }
        }

        /** Update the visibility of the warning icon. */
        private void updateIconState() {
            mWarningIconView.setImageAlpha(mWarningIconVisible ? 255 : 0);
            mWarningIconView.invalidateDrawable(mWarningIcon);
        }

        /** Queue a message to animate the warning icon. */
        private void queueAnimateMessage() {
            int msgWhat = mCount.incrementAndGet();
            sendEmptyMessageDelayed(msgWhat, mWarningIconVisible ? WARNING_ICON_ON_DURATION_MSEC
                    : WARNING_ICON_OFF_DURATION_MSEC);
            // Log.d(TAG, "queued animation message id = " + msgWhat);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == mCount.get()) {
                mWarningIconVisible = !mWarningIconVisible;
                updateIconState();
                queueAnimateMessage();
            }
        }

        /**
         * Initialize the Drawable and ImageView fields.
         * @return true if successful; false if any field failed to initialize
         */
        private boolean initDrawableAndImageView() {
            if (mWarningIcon == null) {
                try {
                    mWarningIcon = getResources().getDrawable(R.drawable.ic_warning_large);
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "warning icon resource not found", e);
                    return false;
                }
            }
            if (mWarningIconView == null) {
                mWarningIconView = (ImageView) findViewById(R.id.icon);
                if (mWarningIconView != null) {
                    mWarningIconView.setImageDrawable(mWarningIcon);
                } else {
                    Log.e(TAG, "failed to get ImageView for warning icon");
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Handler to add {@code FLAG_KEEP_SCREEN_ON} for emergency alerts. After a short delay,
     * remove the flag so the screen can turn off to conserve the battery.
     */
    private class ScreenOffHandler extends Handler {
        /** Latest {@code message.what} value for detecting old messages. */
        private final AtomicInteger mCount = new AtomicInteger();

        /** Package local constructor (called from outer class). */
        ScreenOffHandler() {}

        /** Add screen on window flags and queue a delayed message to remove them later. */
        void startScreenOnTimer() {
            addWindowFlags();
            int msgWhat = mCount.incrementAndGet();
            removeMessages(msgWhat - 1);    // Remove previous message, if any.
			//[Task 667593]-Porting from FR400297-BEGIN by TCTSH.gang-chen@tcl.com,10/13/2015
			// [FEATURE]-Add-BEGIN by TCTNB.Dandan.Fang,05/06/2013,FR400297,
            // CBC notification with pop up and tone alert + vibrate in CHILE
            // screen off after 10 seconds for Chile

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            if (forceVibrateChile) {
                sendEmptyMessageDelayed(msgWhat, Chile_KEEP_SCREEN_ON_DURATION_MSEC);
            } else {
                sendEmptyMessageDelayed(msgWhat, KEEP_SCREEN_ON_DURATION_MSEC);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            Log.d(TAG, "added FLAG_KEEP_SCREEN_ON, queued screen off message id " + msgWhat);
        }

        /** Remove the screen on window flags and any queued screen off message. */
        void stopScreenOnTimer() {
            removeMessages(mCount.get());
            clearWindowFlags();
        }

        /** Set the screen on window flags. */
        private void addWindowFlags() {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        /** Clear the screen on window flags. */
        private void clearWindowFlags() {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        @Override
        public void handleMessage(Message msg) {
            int msgWhat = msg.what;
            if (msgWhat == mCount.get()) {
                clearWindowFlags();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
				removeMessages(msgWhat - 1);    // Remove previous message, if any.
                // FR400297,
                // CBC notification with pop up and tone alert + vibrate in CHILE
                // screen off after 10 seconds for Chile
                if (forceVibrateChile) {
                    PowerManager mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    if (true == mPowerManager.isScreenOn()) {
                        Log.d(TAG,
                                "10s, force screen off, removed FLAG_KEEP_SCREEN_ON with id "
                                        + msgWhat);
                        mPowerManager.goToSleep(SystemClock.uptimeMillis());
                    }
                }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                Log.d(TAG, "removed FLAG_KEEP_SCREEN_ON with id " + msgWhat);
            } else {
                Log.e(TAG, "discarding screen off message with id " + msgWhat);
            }
        }
    }

    /** Returns the currently displayed message. */
    CellBroadcastMessage getLatestMessage() {
        int index = mMessageList.size() - 1;
        if (index >= 0) {
            return mMessageList.get(index);
        } else {
            return null;
        }
    }

    /** Removes and returns the currently displayed message. */
    private CellBroadcastMessage removeLatestMessage() {
        int index = mMessageList.size() - 1;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        if(getResources().getBoolean(R.bool.def_showCbMessageAlertByDate_on)){
            if(index >= 0) {
                CellBroadcastMessage removeCbm = mMessageList.get(index);
                int cbSize = mCBMessageList.size();
                int cbPreSize = mPresidentMessageList.size();

                if(removeCbm.getCmasMessageClass() == SmsCbCmasInfo.CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT) {
                    for(int i=0;i<cbPreSize;i++) {
                        CellBroadcastMessage tempPreCbm = mPresidentMessageList.get(i);
                        if(removeCbm == tempPreCbm) {
                            Log.d(TAG, "removeLatestMessage remove mPresidentMessageList index" + i);
                            mPresidentMessageList.remove(i);
                            break;
                        }
                    }
                } else {
                    for(int i=0;i<cbSize;i++) {
                        CellBroadcastMessage tempCbm = mCBMessageList.get(i);
                        if(removeCbm == tempCbm) {
                            Log.d(TAG, "removeLatestMessage remove mCBMessageList index" + i);
                            mCBMessageList.remove(i);
                            break;
                        }
                    }
                }
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (index >= 0) {
            return mMessageList.remove(index);
        } else {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Window win = getWindow();
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        mContext = this;
        // [Task 1157531] Add-END by TCTSH.gang-chen 12/15/2015

        // We use a custom title, so remove the standard dialog title bar
        win.requestFeature(Window.FEATURE_NO_TITLE);

        // Full screen alerts display above the keyguard and when device is locked.
        win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        // Initialize the view.
        LayoutInflater inflater = LayoutInflater.from(this);
        setContentView(inflater.inflate(getLayoutResId(), null));

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        instance = this;

        findViewById(R.id.dismissButton).setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getBaseContext().getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_forceVibrateForChile_on)){

                            currentCBMessage = getLatestMessage();
                            Log.d(TAG, "currentCBMessage.getCmasMessageClass() : " + currentCBMessage.getCmasMessageClass());
                            if ((currentCBMessage!=null)&&(currentCBMessage.getServiceCategory() == CellBroadcastAlertService.CHANNEL3||currentCBMessage.getServiceCategory()==CellBroadcastAlertService.CHANNEL1
                                    ||currentCBMessage.getServiceCategory() == CellBroadcastAlertService.CHANNEL2)){
                                final Intent alertIntent = new Intent(CellBroadcastAlertService.CHILE_ALERT_RETRUE_ACTION);
                                alertIntent.setClass(CellBroadcastAlertFullScreen.this, CellBroadcastAlertService.class);
                                alertIntent.putExtra("isChileAlerting", false);
                                startService(alertIntent);
                                Log.d(TAG,"Chile Alerting stoped");
                            }
                        }
                        if(getResources().getBoolean(R.bool.def_cellbroadcastreceiver_alertOrderForTMO)){
                            CellBroadcastAlertService.showOtherAlert();
                        }

                        dismiss();
                    }
                });
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // Get message list from saved Bundle or from Intent.
        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate getting message list from saved instance state");
            mMessageList = savedInstanceState.getParcelableArrayList(
                    CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA);
        } else {
            Log.d(TAG, "onCreate getting message list from intent");
            Intent intent = getIntent();
            mMessageList = intent.getParcelableArrayListExtra(
                    CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            // If we were started from a notification, dismiss it.
            if (!getResources().getBoolean(R.bool.feature_clearOtherNotificationForRU_on)) {
                clearNotification(intent);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        //ALM Defect-1239965
        mCBMessageList = new ArrayList<CellBroadcastMessage>();
        mPresidentMessageList = new ArrayList<CellBroadcastMessage>();
        initMessageList(mMessageList);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (mMessageList != null) {
            Log.d(TAG, "onCreate loaded message list of size " + mMessageList.size());
        } else {
            Log.e(TAG, "onCreate failed to get message list from saved Bundle");
            finish();
        }

        // For emergency alerts, keep screen on so the user can read it, unless this is a
        // full screen alert created by CellBroadcastAlertDialog when the screen turned off.
        CellBroadcastMessage message = getLatestMessage();
        if (CellBroadcastConfigService.isEmergencyAlertMessage(message) &&
                (savedInstanceState != null ||
                        !getIntent().getBooleanExtra(SCREEN_OFF_EXTRA, false))) {
            Log.d(TAG, "onCreate setting screen on timer for emergency alert");
            mScreenOffHandler.startScreenOnTimer();
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        if (getResources().getBoolean(R.bool.feature_clearOtherNotificationForRU_on)) {
            int notificationId = message.getServiceCategory();
            android.util.Log.d("cdg998","cannel current notification id = "+notificationId);
            cancelNotificationId(notificationId);
        }

        if (getResources().getBoolean(R.bool.def_cellbroadcastreceiver_alertOrderForTMO)) {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(HAS_SHOWING_ALERT, true);
            editor.commit();
        }

        //CBC notification with pop up and tone alert + vibrate in CHILE
        Intent intent = getIntent();
        forceVibrateChile = intent.getBooleanExtra("forceVibrate", false);
        if (forceVibrateChile) {
            Log.i(TAG,"onCreate: startScreenOnTimer ");
            mScreenOffHandler.startScreenOnTimer();
            Log.i(TAG,"onCreate:display specified title, date, time and message content for chile ");
            updateAlertText(message, forceVibrateChile);
        } else {
            updateAlertText(message, false);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    /**
     * Called by {@link CellBroadcastAlertService} to add a new alert to the stack.
     * @param intent The new intent containing one or more {@link CellBroadcastMessage}s.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        ArrayList<CellBroadcastMessage> newMessageList = intent.getParcelableArrayListExtra(
                CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA);
        if (newMessageList != null) {
            Log.d(TAG, "onNewIntent called with message list of size " + newMessageList.size());
            mMessageList.addAll(newMessageList);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            initMessageList(newMessageList);

            // If the new intent was sent from a notification, dismiss it.
            //CBC notification with pop up and tone alert + vibrate in CHILE
            //updateAlertText(getLatestMessage());
            forceVibrateChile = intent.getBooleanExtra("forceVibrate", false);
            if (forceVibrateChile) {
                Log.i(TAG,"onNewIntent:check screen is on or not, if not, start it ");
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                boolean isScreenOn = pm.isScreenOn();
                if (!isScreenOn) {
                    mScreenOffHandler.startScreenOnTimer();
                }
                Log.i(TAG,"onNewIntent:display specified title, date, time and message content for chile ");
                updateAlertText(getLatestMessage(), forceVibrateChile);
            } else {
                updateAlertText(getLatestMessage(), false);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            clearNotification(intent);
        } else {
            Log.e(TAG, "onNewIntent called without SMS_CB_MESSAGE_EXTRA, ignoring");
        }
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    private void initMessageList(ArrayList<CellBroadcastMessage> mMessageList) {
        //[Defect.1239965]
        if (getResources().getBoolean(R.bool.def_showCbMessageAlertByDate_on)) {
            Log.d(TAG, "called with new message list of size " + mMessageList.size());
            Log.d(TAG, "loaded message list of size " + mMessageList.size());

            for (int i = 0;i < mMessageList.size(); i++) {
                CellBroadcastMessage tempCbm = mMessageList.get(i);
                if (tempCbm != null) {
                    if (tempCbm.getCmasMessageClass() == SmsCbCmasInfo.CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT) {
                        mPresidentMessageList.add(tempCbm);
                    } else {
                        mCBMessageList.add(tempCbm);
                    }
                }
            }

            Log.d(TAG, "loaded mPresidentMessageList size " + mPresidentMessageList.size() + "mCBMessageList size" + mCBMessageList.size());

            if (mMessageList.size() > 1) {
                Log.d(TAG, "loaded message list of size " + mMessageList.size());
                mMessageList.clear();
                int cbSize = mCBMessageList.size();
                int cbPreSize = mPresidentMessageList.size();

                for (int i = 0; i < cbSize; i++) {
                    int cbIndex = cbSize - 1 - i;
                    Log.d(TAG, "add mCBMessageList i=" + i + " cbIndex=" + cbIndex);
                    CellBroadcastMessage tempCbm = mCBMessageList.get(cbIndex);
                    mMessageList.add(tempCbm);
                }

                for (int i = 0; i < cbPreSize; i++) {
                    int cbPreIndex = cbPreSize - 1 - i;
                    Log.d(TAG, "add mPresidentMessageList i=" + i + " cbPreIndex=" + cbPreIndex);
                    CellBroadcastMessage tempPreCbm = mPresidentMessageList.get(cbPreIndex);
                    mMessageList.add(tempPreCbm);
                }
                Log.d(TAG, "loaded message list of size" + mMessageList.size());
            }
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /** Try to cancel any notification that may have started this activity. */
    private void clearNotification(Intent intent) {
        if (intent.getBooleanExtra(FROM_NOTIFICATION_EXTRA, false)) {
            Log.d(TAG, "Dismissing notification");
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(CellBroadcastAlertService.NOTIFICATION_ID);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            if (getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_CBReceiverMode_on) ||
                    this.getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_displayChannelId)) {
                cancelNotificationId(CellBroadcastReceiverApp.notificationid);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            CellBroadcastReceiverApp.clearNewMessageList();
        }
    }

    /**
     * Save the list of messages so the state can be restored later.
     * @param outState Bundle in which to place the saved state.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA, mMessageList);
        Log.d(TAG, "onSaveInstanceState saved message list to bundle");
    }

    /** Returns the resource ID for either the full screen or dialog layout. */
    protected int getLayoutResId() {
        return R.layout.cell_broadcast_alert_fullscreen;
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    /** Update alert text when a new emergency alert arrives. */
    private void updateAlertText(CellBroadcastMessage message, boolean forceVibrateChile) {
        if (forceVibrateChile) {
            // title
            String chileTitle = null;
            chileTitle = getResources().getString(R.string.title_chile_cb_dialog);
            setTitle(chileTitle);
            ((TextView) findViewById(R.id.alertTitle)).setText(chileTitle);
        } else {
            int titleId = CellBroadcastResources.getDialogTitleResource(message);
            setTitle(titleId);

            CharSequence stringCharSequence = getText(titleId);
            if (!message.isEmergencyAlertMessage() && this.getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_CBReceiverMode_on)
               || !message.isEmergencyAlertMessage() && this.getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_displayChannelId)
               || !message.isEmergencyAlertMessage() && this.getResources().getBoolean(R.bool.feature_enable_cb_displayChannelId)) {
                ((TextView) findViewById(R.id.alertTitle)).setText(stringCharSequence+" ("+message.getServiceCategory()+")");
            } else {
                ((TextView) findViewById(R.id.alertTitle)).setText(titleId);
            }
        }

        TextView messageView = (TextView) findViewById(R.id.message);
        //((TextView) findViewById(R.id.message)).setText(message.getMessageBody());

        TextView datetime = ((TextView) findViewById(R.id.alertTime));
        datetime.setVisibility(View.VISIBLE);
        TextView date = ((TextView) findViewById(R.id.alertDate));
        date.setVisibility(View.VISIBLE);
        long mDeliveryTime = message.getDeliveryTime();
        Date date1 = new Date(mDeliveryTime);
        SimpleDateFormat timeFormat = null;
        String sTime = null;
        String _12or24Hour = Settings.System.getString(getContentResolver(),Settings.System.TIME_12_24);
        if("24".equals(_12or24Hour)) { //modify by chaobing.huang defect 1840024
            timeFormat = new SimpleDateFormat("HH:mm:ss");
            sTime = timeFormat.format(date1);
        } else {
            timeFormat = new SimpleDateFormat("hh:mm:ss");
            Calendar calendar = timeFormat.getCalendar();
            sTime = timeFormat.format(date1);
            if(calendar.get(Calendar.AM_PM) == 0) {
                sTime += "  AM";
            } else {
                sTime += "  PM";
            }
        }
        String defaultFormat = "";
        String currentFormat = Settings.System.getString(getContentResolver(), Settings.System.DATE_FORMAT);
        SimpleDateFormat dateFormat;

        if (this.getResources().getBoolean(R.bool.def_useUSDateFormat_on)) {
            defaultFormat = "MM/dd/yyyy";
        } else {
            if(currentFormat != null && !currentFormat.equals("")) {
                //[BUGFIX]-Mod-BEGIN by TSCD.dagang.yang,08/25/2015,1052690
                if(this.getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_displayChannelId)) {
                    defaultFormat = currentFormat.replace("-", ".");
                } else {
                    defaultFormat = currentFormat.replace("-", "/");
                }
            } else {
                defaultFormat = "MM/dd/yyyy";
            }
        }
        dateFormat = new SimpleDateFormat(defaultFormat);
        String sDate = dateFormat.format(date1);
        date.setText(sDate);
        datetime.setText(sTime);
        Log.d(TAG,"Message.getDeliverTime : " + sTime + " " + sDate);

        int serviceCategory = message.getServiceCategory();
        boolean isShowCMASDialogId = mContext.getResources().getBoolean(R.bool.def_showCMASDialogId);
        if (isShowCMASDialogId && serviceCategory >= MSGID1 && serviceCategory <= MSGID12) {
            if (serviceCategory >= MSGID2 && serviceCategory <= MSGID9) {
                messageView.setText(CellBroadcastResources.getMessageDetails(this, message));
            } else {
                messageView.setText("ID: MsgId"+(serviceCategory-4369)+"\n"+message.getMessageBody());
            }
        } else {
            messageView.setText(message.getMessageBody());
        }

        if (getResources().getBoolean(R.bool.def_cellbroadcastreceiver_highlight_phonenumber_on)) {
            CBLinkify.addLinks(messageView, CBLinkify.ALL);
        } else {
            Log.d(TAG,"updateAlertText: do not highlight phone number");
            CBLinkify.addLinks(messageView, CBLinkify.EMAIL_ADDRESSES | CBLinkify.WEB_URLS);
        }

        URLSpan[] spans = messageView.getUrls();
        CharSequence text = messageView.getText();
        if (text instanceof Spannable) {
            int end = text.length();
            Spannable sp = (Spannable) messageView.getText();
            SpannableStringBuilder style = new SpannableStringBuilder(text);
            style.clearSpans();
            for (URLSpan url : spans) {
                myUrlSpan myurlSpan = new myUrlSpan(mContext, messageView, url.getURL());
                style.setSpan(myurlSpan, sp.getSpanStart(url), sp.getSpanEnd(url),
                        Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            }
            //messageView.setText(style);
            //messageView.setTextSize(16);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // Set alert reminder depending on user preference
        if (getResources().getBoolean(
                    R.bool.config_regional_wea_alert_reminder_interval)) {
            CellBroadcastAlertReminder.queueAlertReminderAudio(this, true, message);
        } else {
            CellBroadcastAlertReminder.queueAlertReminder(this, true);
        }
    }

    public class myUrlSpan extends ClickableSpan {
        public String mUrl;
        private TextView mTv;
        private Context mContext;
        final String mTelPrefix = "tel:";
        final String mEmailPrefix = "mailto:";
        final String mSmsPrefix = "smsto:";

        myUrlSpan(Context context, TextView mTextView, String url) {
            mUrl = url;
            mTv = mTextView;
            mContext = context;
        }

        @Override
        public void onClick(View widget) {
            if (mUrl.startsWith(mTelPrefix)) {
                URLSpan[] TempSpan = new URLSpan[2];
                TempSpan[0] = new URLSpan(mTelPrefix + mUrl.substring(mUrl.indexOf(":") + 1));
                TempSpan[1] = new URLSpan(mSmsPrefix + mUrl.substring(mUrl.indexOf(":") + 1));
                dealClick(TempSpan);

            } else if (mUrl.startsWith(mEmailPrefix)) {
                // Defect1815658-fujun.yang@jrdcom.com-for [CB]Launch message apk when click the email address of CB messages -begin
                //URLSpan url = new URLSpan(mSmsPrefix + mUrl.substring(mUrl.indexOf(":") + 1));
                URLSpan url = new URLSpan(mEmailPrefix + mUrl.substring(mUrl.indexOf(":") + 1));
                Log.d(TAG,"email url: "+url.getURL());
                // Defect1815658-fujun.yang@jrdcom.com-for [CB]Launch message apk when click the email address of CB messages -end
                url.onClick(mTv);
            } else {
                URLSpan url = new URLSpan(mUrl);
                url.onClick(mTv);
            }
        }

        public void dealClick(URLSpan[] spans) {
            final URLSpan[] Spans = spans;
            ArrayAdapter<URLSpan> adapter = new ArrayAdapter<URLSpan>(mContext,
                    android.R.layout.select_dialog_item, Spans) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    URLSpan span = getItem(position);
                    String url = span.getURL();
                    TextView tv = (TextView) v;
                    if (url.startsWith(mTelPrefix)) {
                        tv.setText(mContext.getResources().getString(R.string.call_phone_att));
                    } else if (url.startsWith(mSmsPrefix)) {
                        tv.setText(mContext.getResources().getString(R.string.Send_message_att));
                    }
                    return v;
                }
            };

            AlertDialog.Builder b = new AlertDialog.Builder(mContext);
            DialogInterface.OnClickListener click = new DialogInterface.OnClickListener() {
                @Override
                public final void onClick(DialogInterface dialog, int which) {
                    if (which >= 0) {
                        Spans[which].onClick(mTv);
                    }
                    dialog.dismiss();
                }
            };

            b.setTitle(spans[0].getURL().substring(spans[0].getURL().indexOf(":") + 1));
            b.setCancelable(true);
            b.setAdapter(adapter, click);

            b.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public final void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            b.show();
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /**
     * Start animating warning icon.
     */
    @Override
    protected void onResume() {
        Log.d(TAG, "onResume called");
        super.onResume();
        CellBroadcastMessage message = getLatestMessage();
        if (message != null && CellBroadcastConfigService.isEmergencyAlertMessage(message)) {
            mAnimationHandler.startIconAnimation();
        }
    }

    /**
     * Stop animating warning icon.
     */
    @Override
    protected void onPause() {
        Log.d(TAG, "onPause called");
        mAnimationHandler.stopIconAnimation();
        super.onPause();
    }

    /**
     * Stop animating warning icon and stop the {@link CellBroadcastAlertAudio}
     * service if necessary.
     */
    void dismiss() {
        Log.d(TAG, "dismissed");

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        if(getResources().getBoolean(R.bool.def_cellbroadcastreceiver_alertOrderForTMO)){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(CellBroadcastReceiverApp.getApplication());
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(HAS_SHOWING_ALERT, false);
            editor.commit();
        }

        if(!getResources().getBoolean(R.bool.feature_clearOtherNotificationForRU_on)) {
            if (this.getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_displayChannelId)
                    ||this.getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_CBReceiverMode_on)) {
                cancelNotificationId(CellBroadcastReceiverApp.notificationid);
            }
            CellBroadcastReceiverApp.clearNewMessageList();
        } 
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // Stop playing alert sound/vibration/speech (if started)
        stopService(new Intent(this, CellBroadcastAlertAudio.class));

        // Cancel any pending alert reminder
        CellBroadcastAlertReminder.cancelAlertReminder();

        // Remove the current alert message from the list.
        CellBroadcastMessage lastMessage = removeLatestMessage();
        if (lastMessage == null) {
            Log.e(TAG, "dismiss() called with empty message list!");
            return;
        }

        // Mark the alert as read.
        final long deliveryTime = lastMessage.getDeliveryTime();

        // Mark broadcast as read on a background thread.
        new CellBroadcastContentProvider.AsyncCellBroadcastTask(getContentResolver())
                .execute(new CellBroadcastContentProvider.CellBroadcastOperation() {
                    @Override
                    public boolean execute(CellBroadcastContentProvider provider) {
                        return provider.markBroadcastRead(
                                Telephony.CellBroadcasts.DELIVERY_TIME, deliveryTime);
                    }
                });

        // Set the opt-out dialog flag if this is a CMAS alert (other than Presidential Alert).
        if (lastMessage.isCmasMessage() && lastMessage.getCmasMessageClass() !=
                SmsCbCmasInfo.CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT) {
            mShowOptOutDialog = true;
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        if (!getResources().getBoolean(R.bool.feature_showOptOutDialog_on)) {
            mShowOptOutDialog = false;
        }

        // If there are older emergency alerts to display, update the alert text and return.
        if(!getResources().getBoolean(R.bool.feature_clearOtherNotificationForRU_on)) {
            CellBroadcastMessage nextMessage = getLatestMessage();
            if (nextMessage != null) {
                updateAlertText(nextMessage, false);
                if (CellBroadcastConfigService.isEmergencyAlertMessage(nextMessage)) {
                    mAnimationHandler.startIconAnimation();
                } else {
                    mAnimationHandler.stopIconAnimation();
                }
                return;
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // Remove pending screen-off messages (animation messages are removed in onPause()).
        mScreenOffHandler.stopScreenOnTimer();

        // Show opt-in/opt-out dialog when the first CMAS alert is received.
        if (mShowOptOutDialog) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (prefs.getBoolean(CellBroadcastSettings.KEY_SHOW_CMAS_OPT_OUT_DIALOG, true)) {
                // Clear the flag so the user will only see the opt-out dialog once.
                prefs.edit().putBoolean(CellBroadcastSettings.KEY_SHOW_CMAS_OPT_OUT_DIALOG, false)
                        .apply();

                KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                if (km.inKeyguardRestrictedInputMode()) {
                    Log.d(TAG, "Showing opt-out dialog in new activity (secure keyguard)");
                    Intent intent = new Intent(this, CellBroadcastOptOutActivity.class);
                    startActivity(intent);
                } else {
                    Log.d(TAG, "Showing opt-out dialog in current activity");
                    CellBroadcastOptOutActivity.showOptOutDialog(this);
                    return; // don't call finish() until user dismisses the dialog
                }
            }
        }

        Log.d(TAG, "finished");
        finish();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        CellBroadcastMessage message = getLatestMessage();
//[FEATURE]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/31/2016, TASK-2781160
//[NL] Fixed CMAS Alert Tone
        boolean isFixedAlert = getResources().getBoolean(R.bool.def_cellbroadcastreceiver_fixedAlert_on);
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        if (message != null && !message.isEtwsMessage() && !forceVibrateChile && !isFixedAlert) {
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
//[FEATURE]-Add-END by TCTNB.(JiangLong Pan)
            switch (event.getKeyCode()) {
                // Volume keys and camera keys mute the alert sound/vibration (except ETWS).
                case KeyEvent.KEYCODE_VOLUME_UP:
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                case KeyEvent.KEYCODE_VOLUME_MUTE:
                case KeyEvent.KEYCODE_CAMERA:
                case KeyEvent.KEYCODE_FOCUS:
                    // Stop playing alert sound/vibration/speech (if started)
                    stopService(new Intent(this, CellBroadcastAlertAudio.class));
                    return true;

                default:
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * Ignore the back button for emergency alerts (overridden by alert dialog so that the dialog
     * is dismissed).
     */
    @Override
    public void onBackPressed() {
        // ignored
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    void cancelNotificationId(Map<Integer,Integer> notificationid){
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Iterator iter = notificationid.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry<Integer, Integer> entry =(Map.Entry<Integer, Integer>) iter.next();
            int val = (int)entry.getValue();
            notificationManager.cancel(val);
        }
    }

    void cancelNotificationId(int notificationId) {
        Log.d(TAG, "cancelNotificationId :" + notificationId);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}

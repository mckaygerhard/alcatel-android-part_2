/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.cellbroadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.os.UserManager;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.CellBroadcastMessage;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaSmsCbProgramData;
import android.util.Log;

import com.android.internal.telephony.cdma.sms.SmsEnvelope;
import com.android.internal.telephony.TelephonyIntents;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
//Add backlater
//import android.os.SsvManager;
import android.os.SystemProperties;
import android.preference.Preference;
import android.telephony.SmsCbMessage;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;

import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.ISms;
import com.android.internal.telephony.gsm.SmsBroadcastConfigInfo;
import com.android.cellbroadcastreceiver.CellBroadcast.Channel;

import java.util.List;

public class CellBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "CellBroadcastReceiver";
    static final boolean DBG = false;    // STOPSHIP: change to false before ship
    private static int mServiceState = -1;

    public static final String CELLBROADCAST_START_CONFIG_ACTION =
            "android.cellbroadcastreceiver.START_CONFIG";

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    private static final String PREFS_NAME = "com.android.cellbroadcastreceiver_preferences";
    public static final String CONFIG_RESET_CBNV_VELUE = "config_reset_cbnv_value";
    public static final String CONFIG_CB_SET_DEFAULT = "config_cb_set_default";
    public static final String CB_ALERT_DEFAULT_SETTING = "enable_cb_alerts_setting";

    //FR708134
    public static final String PRE_MCC_MNC = "pref_key_mcc_mnc";
    public static final String PRE_RESET_CHANNEL = "reset_channel";

    static final String KEY_LAST_SIM_CARD_SERIAL_NUMBER = "key_last_sim_card_serial_number";
    private boolean mEnableSingleSIM = false;
    private static boolean appNeedChange = false;
    private boolean cbEnabled = false;
    private String channelMode = "1";

    public static final String IS_FIRST_BOOT = "is_first_boot";
    private TelephonyManager mTelephonyManager;
    private int ranType;
    private static boolean configTag = false;

    public boolean isHollandSimCard(Context mContext ,int slotId) {
        boolean mIsNLSimCard = false;
        mIsNLSimCard = false;
        if(TelephonyManager.getDefault().isMultiSimEnabled()) {
            int mPhoneId = new Long(slotId).intValue();
            int subscription[] = SubscriptionManager.getSubId(mPhoneId);
            Log.d(TAG,"subscription="+subscription[0]);

            if (mEnableSingleSIM && mPhoneId == 1) {
                return false;
            }

            TelephonyManager mmanger = TelephonyManager.getDefault();
            String numeric = mmanger.getSimOperator(subscription[0]);
            Log.i(TAG, "multisimcard-isHollandSimCard = " + numeric);

            if (numeric != null && numeric.length() > 4) {
                String sub = numeric.subSequence(0, 3).toString();
                Log.i(TAG,"mcc="+sub);
                if(sub.equals("204")){
                    mIsNLSimCard = true;
                }
            }
        } else {
            TelephonyManager manger = TelephonyManager.getDefault();
            String numeric = manger.getSimOperator();
            Log.i(TAG, "isHollandSimCard = " + numeric);

            if(numeric != null && numeric.length() > 4) {
                String sub1 = numeric.subSequence(0, 3).toString();
                Log.i(TAG,"mcc = " + sub1);
                if (sub1.equals("204")) {
                    mIsNLSimCard = true;
                }
            }
        }
        return mIsNLSimCard;
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    public void onReceive(Context context, Intent intent) {
        onReceiveWithPrivilege(context, intent, false);
    }

    protected void onReceiveWithPrivilege(Context context, Intent intent, boolean privileged) {
        if (DBG) log("onReceive " + intent);

        String action = intent.getAction();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        mEnableSingleSIM = context.getResources().getBoolean(R.bool.def_cellbroadcastreceiver_enable_single_sim);
        boolean enableAmberDisplay = context.getResources().getBoolean(R.bool.feature_enable_amber_display);// PR1841597
        Log.d(TAG, "mEnableSingleSIM:" + mEnableSingleSIM);

        SharedPreferences mSettings = context.getSharedPreferences(PREFS_NAME, 0);
        cbEnabled = mSettings.getBoolean("cb_enabled", false);
        channelMode = mSettings.getString("pref_key_choose_channel", "1");

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        boolean isBrazil50CbSupported = context.getResources().getBoolean(R.bool.show_brazil_settings);
        Log.d(TAG, "isBrazil50CbSupported:" + isBrazil50CbSupported);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            //FR-516039,
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            boolean enableCBValue = settings.getBoolean(CONFIG_CB_SET_DEFAULT, true);
            Log.d(TAG,"cdg-ACTION_BOOT_COMPLETED-enableCBValue"+enableCBValue);

            if (enableCBValue) {
                // at last, set check flag
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(CONFIG_CB_SET_DEFAULT, false);
                editor.commit();
                setCustomizedChannels(context);
            }

            //PR-901210
            boolean isFirstBoot = settings.getBoolean(IS_FIRST_BOOT, true);
            Log.d(TAG, "cdg-isFirstBoot : " + isFirstBoot);
            if (isFirstBoot) {
                initEmergencyPref(context);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(IS_FIRST_BOOT, false);
                editor.commit();
            }

            if (SystemProperties.getBoolean("ro.ssv.enabled", false)) {
                String preMccMnc = settings.getString(PRE_MCC_MNC, "");
                String nowMccMnc = SystemProperties.get("persist.sys.lang.mccmnc", "");
                if (nowMccMnc != null && !nowMccMnc.equals("") && !nowMccMnc.equals(preMccMnc)) {
                    Log.i(TAG, "sim changed need to reset channel list");
                    appNeedChange = true;
                    settings.edit().putString(PRE_MCC_MNC, nowMccMnc).apply();
                    settings.edit().putBoolean(CONFIG_RESET_CBNV_VELUE, true).apply();
                    setCustomizedChannels(context);
                } else {
                    Log.i(TAG, "no need to reset channel list");
                }
            }
        } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
            boolean airplaneModeOn = intent.getBooleanExtra("state", false);
            if (DBG) log("onReceive is AIRPLANE_MODE_CHANGED airplaneModeOn: " + airplaneModeOn);
            if (!airplaneModeOn) {
                if (enableAmberDisplay) {
                    startConfigService(context);
                }
                Log.d(TAG, "onReceive is airplane off then do nothing!");
            }

            //PR752652,PR582740
            //[CB]After airplane mode, mobile can not receive CMAS message.
            else {
                //PR974108,
                //[AT&T][CMAS(CELL BROADCAST)][LTE-BTR-5-4084/4098]:UE didn't receive the
                //second Alert after Airplane mode ON and OFF
                CellBroadcastAlertService.clearEmergencyMessageInHistory();

                try {
                    ISms iccISms = ISms.Stub.asInterface(ServiceManager.getService("isms"));
                    if (iccISms != null) {
                        Log.d(TAG, "onReceive is airplane on then setRangesEmpty!");
                        iccISms.tct_setRangesEmpty();
                    }
                } catch (RemoteException ex) {
                    // ignore it
                }
            }

        //FR-1697035
        } else if ("com.android.internal.telephony.uicc.SIM_RECORD_ICCID_READY".equals(action) &&
            !context.getResources().getBoolean(R.bool.def_isResetCmasSetting)){
            resetCMASSettingsIfSimCardChanged(context);

        //PR-1827973
        } else if ("android.intent.action.LAUNCH_DEVICE_RESET".equals(action) &&
                context.getResources().getBoolean(R.bool.def_enableLaunchDeviceReset)){
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            String lastSerialNumber = sp.getString(KEY_LAST_SIM_CARD_SERIAL_NUMBER, "");
            sp.edit().clear().commit();
            sp.edit().putString(KEY_LAST_SIM_CARD_SERIAL_NUMBER, lastSerialNumber).commit();
            initEmergencyPref(context);
            startConfigService(context);
            initCBSetting(context); 

        //PR-1940689
        } else if ("com.android.cmas.rmt.exercise.alerts".equals(action) &&
                context.getResources().getBoolean(R.bool.def_enableRMTExerciseTestAlert)){
            Log.d(TAG,"action: com.android.cmas.rmt.exercise.alerts");
            String alertType = intent.getStringExtra("alert_type");
            boolean alertValue = intent.getBooleanExtra("alert_value", false);
            Log.d(TAG, "#--- set " + alertType + " is " + alertValue);
            enableRmtAndExercise(alertType, alertValue, context);
        } else if ("com.android.broadcast.resetcd".equals(action)) {
            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                for (int j = 0; j < TelephonyManager.getDefault().getPhoneCount(); j++) {
                    if (mEnableSingleSIM && j >= 1) {
                        continue;
                    }
                    Uri uri = Uri.withAppendedPath(CellBroadcast.Channel.CONTENT_URI, "sub" + j);
                    context.getContentResolver().delete(uri, null, null);
                }
            } else {
                context.getContentResolver().delete(CellBroadcast.Channel.CONTENT_URI, null, null);
            }

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String defLanguage = context.getResources().getString(R.string.all_languages);
            ContentValues values = new ContentValues();
            values.put(CellBroadcast.CBLanguage.CBLANGUAGE, defLanguage);
            context.getContentResolver().update(CellBroadcast.CBLanguage.CONTENT_URI, values, null, null);
            sharedPrefs.edit().putString("cb_language", defLanguage).apply();
            //PR949742
            sharedPrefs.edit().putString("pref_key_choose_channel", "1").apply();

            //FR708134
            sharedPrefs.edit().putBoolean(PRE_RESET_CHANNEL, true).apply();
            Log.i(TAG, "mms reset channel >>>>>>>>>");

            context.getContentResolver().delete(CellBroadcast.Channel.CONTENT_URI, null, null);
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(CONFIG_RESET_CBNV_VELUE, true);
            editor.apply();
            setCustomizedChannels(context);
        }

        else if ("com.android.CellBroadcast.resetEmergAlert".equals(action)) {
            Log.d(TAG, "cdg-com.android.CellBroadcast.resetEmergAlert");
            initEmergencyPref(context);
        } else if ("com.android.cellbroadcastreceiver.setstartup".equals(action)) {
            Log.d(TAG,"cdg-com.android.cellbroadcastreceiver.setstartup");
            int subDescription = PhoneConstants.SUB1;
            boolean value = false;
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();

            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                boolean isEnableCB = false;
                //Defect-988226
                Log.i(TAG, "cdgsetstartup-subDescription="+subDescription);
                if (mEnableSingleSIM) {
                    subDescription = PhoneConstants.SUB1;
                } else {
                    subDescription = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, PhoneConstants.SUB1);
                }
                Log.i(TAG, "cdg-setstartup-subDescription final = " + subDescription);


                if (subDescription == 0) {
                    channelMode = mSettings.getString("pref_key_choose_channel_sim1", "1");
                } else if (subDescription == 1) {
                    channelMode = mSettings.getString("pref_key_choose_channel_sim2", "1");
                }
                isEnableCB = intent.getBooleanExtra("startup",true);

                //BUG-1966416
                //SubscriptionManager mSubscriptionManager = SubscriptionManager.from(context);
                //int subId = PhoneConstants.SUB1;
                //SubscriptionInfo mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(subDescription);

                //if (mSubInfoRecord != null) {
                //    subId = mSubInfoRecord.getSubscriptionId();
                //}

                if (isEnableCB) {
                    editor.putBoolean(CB_ALERT_DEFAULT_SETTING, true);
                } else {
                    editor.putBoolean(CB_ALERT_DEFAULT_SETTING, false);
                }
                editor.apply();
                Log.i(TAG, "MultiSim--isEnableCB = " + isEnableCB);

                if (!isEnableCB && isHollandSimCard(context,subDescription)) {
                    return;
                }
            } else {
                boolean isEnableCB = false;
                isEnableCB = intent.getBooleanExtra("startup", true);
                if (isEnableCB) {
                    editor.putBoolean(CB_ALERT_DEFAULT_SETTING, true);
                    Log.d(TAG, "single-setSubscriptionProperty(CB_ALERT_DEFAULT_SETTING):1");
                } else {
                    editor.putBoolean(CB_ALERT_DEFAULT_SETTING, false);
                    Log.d(TAG, "single-setSubscriptionProperty(CB_ALERT_DEFAULT_SETTING):1");
                }
                editor.apply();
                Log.i(TAG, "oneSim--isEnableCB = " + isEnableCB);

                if (!isEnableCB && isHollandSimCard(context, subDescription)) {
                    return;
                }
                channelMode = mSettings.getString("pref_key_choose_channel", "1");
            }

            value = intent.getBooleanExtra("startup", true);
            cbEnabled = value ;

            //FR772564,new CLID variable to re-activate CB for NL
            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
               Log.i(TAG, "if(1) -subDescription = " + subDescription);
                if (subDescription == 0) {
                    editor.putBoolean("enable_channel_sim1", value);
                } else if (subDescription == 1 && !mEnableSingleSIM) {
                    editor.putBoolean("enable_channel_sim2", value);
                }
            } else {
                editor.putBoolean("cb_enabled", cbEnabled);
            }

            editor.apply();

            SmsManager manager = null;
            if (TelephonyManager.getDefault().isMultiSimEnabled() && mEnableSingleSIM) {
                //Defect-988226
                Log.i(TAG, "if(2) -subDescription = " + subDescription);
                SubscriptionManager mSubscriptionManager = SubscriptionManager.from(context);
                int subId = 0;
                SubscriptionInfo mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(PhoneConstants.SUB1);
                if (mSubInfoRecord != null) {
                    subId = mSubInfoRecord.getSubscriptionId();
                }
                manager = SmsManager.getSmsManagerForSubscriptionId(subId);
            } else {
                manager = SmsManager.getDefault();
            }
            CBSUtills cbu = new CBSUtills(context);

            //PR-1015778
            boolean isSsv = SystemProperties.getBoolean("ro.ssv.enabled", false);
            if (!isSsv) {
                ContentValues values = new ContentValues();
                values.put(Channel.Enable, value ? "Enable" : "Disable");
                cbu.updateChannel(values);
            }

            //PR477879  ,
            if ((subDescription == -1) && TelephonyManager.getDefault().isMultiSimEnabled()) {
                // here, disabled all channel
                Log.i(TAG, "if() -subDescription ==1 :"+" here, disabled all channel");
                SmsBroadcastConfigInfo[] cbi = null;
                SmsManager manager1 = SmsManager.getSmsManagerForSubscriptionId(PhoneConstants.SUB1);
                SmsManager manager2 = SmsManager.getSmsManagerForSubscriptionId(PhoneConstants.SUB2);
                manager1.disableCellBroadcastRange(0, 999,ranType);
                manager2.disableCellBroadcastRange(0, 999,ranType);

                //PR-845553
                try {
                    cbi = cbu.getSmsBroadcastConfigInfo(PhoneConstants.SUB1);
                } catch (IllegalArgumentException e) {
                    cbi = null;
                }

                if (cbi != null) {
                    int num = cbi.length;
                    for (int i = 0; i < num; i++) {
                        int index = cbi[i].getFromServiceId();
                        manager1.disableCellBroadcastRange(index, index,ranType);
                    }
                }

                if (!mEnableSingleSIM) {
                    //PR-845553
                    try {
                        cbi = cbu.getSmsBroadcastConfigInfo(PhoneConstants.SUB2);
                    } catch (IllegalArgumentException e) {
                        cbi = null;
                    }

                    if (cbi != null) {
                        int num = cbi.length;
                        for (int i = 0; i < num; i++) {
                            int index = cbi[i].getFromServiceId();
                            manager2.disableCellBroadcastRange(index, index,ranType);

                        }
                    }
                }
                return;
            }
            //[CB][mobile receive CB when disable CB

            if (value) {
                //here, according the channelMode to enable or disabled channel.
                if ("1".equalsIgnoreCase(channelMode)) {
                    Log.d(TAG,"channelMode is My channel list");
                    SmsBroadcastConfigInfo[] cbi = null;
                    if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                        cbi = cbu.getSmsBroadcastConfigInfo(subDescription);
                        Log.d(TAG,"//channelMode is \"My channel list\" subDescription/slot :"+subDescription);

                        //Defect-988226
                        SubscriptionManager mSubscriptionManager = SubscriptionManager.from(context);
                        int subId =PhoneConstants.SUB1;
                        SubscriptionInfo  mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(subDescription);

                        if (mSubInfoRecord != null) {
                            subId= mSubInfoRecord.getSubscriptionId();
                        }
                        manager = SmsManager.getSmsManagerForSubscriptionId(subId);
                    } else {
                        cbi = cbu.getSmsBroadcastConfigInfo();
                    }

                    if (cbi != null) {
                        int num = cbi.length;
                        for (int i = 0; i < num; i++) {
                            int index = cbi[i].getFromServiceId();
                            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                                if (cbi[i].isSelected()) {
                                    manager.enableCellBroadcastRange(index,index,ranType);
                                } else {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                                    if (!(isBrazil50CbSupported && index == 50)) {
                                        manager.disableCellBroadcastRange(index, index, ranType);
                                    } else {
                                        Log.d(TAG, "AisBrazil50CbSupported is true and index == 50 ignore");
                                    }
                                }
                            } else {
                                if (cbi[i].isSelected()) {
                                    manager.enableCellBroadcastRange(index,index,ranType);
                                } else {
                                    if (!(isBrazil50CbSupported && index == 50)) {
                                        manager.disableCellBroadcastRange(index, index, ranType);
                                    } else {
                                        Log.d(TAG, "BisBrazil50CbSupported is true and index == 50 ignore");
                                    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                                }
                            }

                        }
                    }
                } else {
                    Log.d(TAG,"channelMode is 0-999, enabled it");
                    SmsBroadcastConfigInfo[] cbi = null;

                    if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                        manager.enableCellBroadcastRange(0, 999,ranType);
                        cbi = cbu.getSmsBroadcastConfigInfo(subDescription);
                    } else {
                        manager.enableCellBroadcastRange(0, 999,ranType);
                        cbi = cbu.getSmsBroadcastConfigInfo();
                    }

                    //if channel index is more than 999, if it is selected, enabled it , else disabled
                    if(cbi != null) {
                        int num = cbi.length;
                        for (int i = 0; i < num; i++) {
                            int index = cbi[i].getFromServiceId();
                            if (index >= 1000) {
                                if(TelephonyManager.getDefault().isMultiSimEnabled()){
                                    if (cbi[i].isSelected()) {
                                        manager.enableCellBroadcastRange(index,index,ranType);
                                    } else {
                                        manager.disableCellBroadcastRange(index, index,ranType);
                                    }
                                } else {
                                    if (cbi[i].isSelected()) {
                                        manager.enableCellBroadcastRange(index,index,ranType);
                                    } else {
                                        manager.disableCellBroadcastRange(index, index,ranType);
                                    }
                                }

                            }
                        }
                    }
                }
            } else {
                // here, disabled all channel
                SmsBroadcastConfigInfo[] cbi = null;
                /* MODIFIED-BEGIN by gang-chen, 2016-08-09,BUG-2256539*/
                manager.disableCellBroadcastRange(0, 999, ranType);
                cbi = cbu.getSmsBroadcastConfigInfo(subDescription);
                if (cbi != null) {
                    int num = cbi.length;
                    for (int i = 0; i < num; i++) {
                        int index = cbi[i].getFromServiceId();
                        if (!(isBrazil50CbSupported && index == 50)) {
                            manager.disableCellBroadcastRange(index, index, ranType);
                        } else {
                            manager.enableCellBroadcastRange(index, index, ranType);
                            Log.d(TAG, "isBrazil50CbSupported is true and enable 50");
                            /* MODIFIED-END by gang-chen,BUG-2256539*/
                        }

                    }
                }
            }
            //Can not receive channel 4371
        } else
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (TelephonyIntents.ACTION_SERVICE_STATE_CHANGED.equals(action)) {
            if (DBG) log("Intent: " + action);

            int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                    SubscriptionManager.INVALID_SUBSCRIPTION_ID);
            Log.d(TAG, "subscriptionId = " + subId);
            if (!SubscriptionManager.isValidSubscriptionId(subId)) {
                return;
            }

            ServiceState serviceState = ServiceState.newFromBundle(intent.getExtras());
            if (serviceState != null) {
                int newState = serviceState.getState();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                if (enableAmberDisplay) {
                    //do nothing.
                } else {
                    startCbConfig(context.getApplicationContext(), newState, subId);
                }

                //PR795416,after changed simcard,add perso control the CB value
                if ("true".equalsIgnoreCase(SystemProperties.get("ro.config.cb.for.alwe.version","false"))) {
                    Log.i(TAG, "alwe version");
                    enableCBIfSimCardChanged(context, newState);
                }

                /*
                if (newState != mServiceState) {
                    Log.d(TAG, "Service state changed! " + newState + " Full: " + serviceState +
                            " Current state=" + mServiceState);
                    mServiceState = newState;
                    if (((newState == ServiceState.STATE_IN_SERVICE) ||
                            (newState == ServiceState.STATE_EMERGENCY_ONLY)) &&
                            (UserManager.get(context).isSystemUser())) {
                        startConfigService(context.getApplicationContext());
                    }
                }
                */
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
            }
        } else if (TelephonyIntents.ACTION_DEFAULT_SMS_SUBSCRIPTION_CHANGED.equals(action) ||
                CELLBROADCAST_START_CONFIG_ACTION.equals(action)) {
            // Todo: Add the service state check once the new get service state API is done.
            // Do not rely on mServiceState as it gets reset to -1 time to time because
            // the process of CellBroadcastReceiver gets killed every time once the job is done.
            if (UserManager.get(context).isSystemUser()) {
                startConfigService(context.getApplicationContext());
            }
            else {
                Log.e(TAG, "Not system user. Ignored the intent " + action);
            }
        } else if (Telephony.Sms.Intents.SMS_EMERGENCY_CB_RECEIVED_ACTION.equals(action) ||
                Telephony.Sms.Intents.SMS_CB_RECEIVED_ACTION.equals(action)) {
            // If 'privileged' is false, it means that the intent was delivered to the base
            // no-permissions receiver class.  If we get an SMS_CB_RECEIVED message that way, it
            // means someone has tried to spoof the message by delivering it outside the normal
            // permission-checked route, so we just ignore it.

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            Bundle extras = intent.getExtras();
            SmsCbMessage message = (SmsCbMessage) extras.get("message");
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            boolean enableCmasSpanishAlerts = sharedPreferences.getBoolean(
                    CellBroadcastSettings.KEY_ENABLE_CMAS_SPANISH_LANGUAGE_ALERTS, false);
            boolean isShowSpanishLanguageAlerts = context.getResources().getBoolean(R.bool.def_showSpanishLanguageAlerts);
            boolean isEnableSpanishPresidentAlert = context.getResources().getBoolean(R.bool.def_enableSpanishPresidentAlert);
            if (isShowSpanishLanguageAlerts && !enableCmasSpanishAlerts
                    && !isEnableSpanishPresidentAlert
                    && (message.getServiceCategory() == 4383)) {
                Log.d(TAG, "enableCmasSpanishAlerts = "+ enableCmasSpanishAlerts
                        + "isEnableSpanishPresidentAlert = "+ isEnableSpanishPresidentAlert);
                Log.d(TAG, "Ignore SMS CMAS spanish alert: " + message);
                CellBroadcastSettings.cbIntent = intent;
            } else
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
            if (privileged) {
                intent.setClass(context, CellBroadcastAlertService.class);
                context.startService(intent);
            } else {
                loge("ignoring unprivileged action received " + action);
            }
        } else if (Telephony.Sms.Intents.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION
                .equals(action)) {
            if (privileged) {
                CdmaSmsCbProgramData[] programDataList = (CdmaSmsCbProgramData[])
                        intent.getParcelableArrayExtra("program_data_list");
                if (programDataList != null) {
                    handleCdmaSmsCbProgramData(context, programDataList);
                } else {
                    loge("SCPD intent received with no program_data_list");
                }
            } else {
                loge("ignoring unprivileged action received " + action);
            }
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
            // PR1841597
            if (enableAmberDisplay) {
                int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, SubscriptionManager.INVALID_SUBSCRIPTION_ID);
                Log.d(TAG, "subscriptionId = " + subId);
                Log.d(TAG, "configtag:" + configTag);
                if (!configTag) {
                    Log.d(TAG,"onReceive is ACTION_SIM_STATE_CHANGED first config channel sbuId:" + subId);
                    startConfigCMAS(context.getApplicationContext(), subId);
                    startConfigCB(context.getApplicationContext(), subId, SubscriptionManager.getPhoneId(subId));
                    configTag = true;
                }
            }

            boolean isResetCmasSetting = context.getResources().getBoolean(R.bool.def_isResetCmasSetting);
            if ((intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE).equals(IccCardConstants.INTENT_VALUE_ICC_LOADED))
                && isResetCmasSetting) {
                resetCMASSettingsIfSimCardChanged(context);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        } else {
            Log.w(TAG, "onReceive() unexpected action " + action);
        }
    }

    /**
     * Handle Service Category Program Data message.
     * TODO: Send Service Category Program Results response message to sender
     *
     * @param context
     * @param programDataList
     */
    private void handleCdmaSmsCbProgramData(Context context,
                                            CdmaSmsCbProgramData[] programDataList) {
        for (CdmaSmsCbProgramData programData : programDataList) {
            switch (programData.getOperation()) {
                case CdmaSmsCbProgramData.OPERATION_ADD_CATEGORY:
                    tryCdmaSetCategory(context, programData.getCategory(), true);
                    break;

                case CdmaSmsCbProgramData.OPERATION_DELETE_CATEGORY:
                    tryCdmaSetCategory(context, programData.getCategory(), false);
                    break;

                case CdmaSmsCbProgramData.OPERATION_CLEAR_CATEGORIES:
                    tryCdmaSetCategory(context,
                            SmsEnvelope.SERVICE_CATEGORY_CMAS_EXTREME_THREAT, false);
                    tryCdmaSetCategory(context,
                            SmsEnvelope.SERVICE_CATEGORY_CMAS_SEVERE_THREAT, false);
                    tryCdmaSetCategory(context,
                            SmsEnvelope.SERVICE_CATEGORY_CMAS_CHILD_ABDUCTION_EMERGENCY, false);
                    tryCdmaSetCategory(context,
                            SmsEnvelope.SERVICE_CATEGORY_CMAS_TEST_MESSAGE, false);
                    break;

                default:
                    loge("Ignoring unknown SCPD operation " + programData.getOperation());
            }
        }
    }

    private void tryCdmaSetCategory(Context context, int category, boolean enable) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        switch (category) {
            case SmsEnvelope.SERVICE_CATEGORY_CMAS_EXTREME_THREAT:
                sharedPrefs.edit().putBoolean(
                        CellBroadcastSettings.KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS, enable)
                        .apply();
                break;

            case SmsEnvelope.SERVICE_CATEGORY_CMAS_SEVERE_THREAT:
                sharedPrefs.edit().putBoolean(
                        CellBroadcastSettings.KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS, enable)
                        .apply();
                break;

            case SmsEnvelope.SERVICE_CATEGORY_CMAS_CHILD_ABDUCTION_EMERGENCY:
                sharedPrefs.edit().putBoolean(
                        CellBroadcastSettings.KEY_ENABLE_CMAS_AMBER_ALERTS, enable).apply();
                break;

            case SmsEnvelope.SERVICE_CATEGORY_CMAS_TEST_MESSAGE:
                sharedPrefs.edit().putBoolean(
                        CellBroadcastSettings.KEY_ENABLE_CMAS_TEST_ALERTS, enable).apply();
                break;

            default:
                Log.w(TAG, "Ignoring SCPD command to " + (enable ? "enable" : "disable")
                        + " alerts in category " + category);
        }
    }

    /**
     * Tell {@link CellBroadcastConfigService} to enable the CB channels.
     * @param context the broadcast receiver context
     */
    static void startConfigService(Context context) {
        Log.d(TAG, "startConfigService ACTION_ENABLE");
        Intent serviceIntent = new Intent(CellBroadcastConfigService.ACTION_ENABLE_CHANNELS,
                null, context, CellBroadcastConfigService.class);
        Log.d(TAG, "Start Cell Broadcast configuration.");
        context.startService(serviceIntent);
    }

    /**
     * @return true if the phone is a CDMA phone type
     */
    static boolean phoneIsCdma() {
        boolean isCdma = false;

        int subId = SubscriptionManager.getDefaultSmsSubscriptionId();
        if (subId == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            subId = SubscriptionManager.getDefaultSubscriptionId();
        }

        TelephonyManager tm = TelephonyManager.getDefault();
        if (tm != null) {
            isCdma = (tm.getCurrentPhoneType(subId) == TelephonyManager.PHONE_TYPE_CDMA);
        }
        return isCdma;
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    private static void loge(String msg) {
        Log.e(TAG, msg);
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    //PR-1940689
    private void enableRmtAndExercise(String type, boolean value, Context mContext) {
        SharedPreferences pre = PreferenceManager.getDefaultSharedPreferences(mContext);

        SubscriptionManager mSubscriptionManager = SubscriptionManager.from(mContext);
        SubscriptionInfo  mSubInfoRecord1 = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(PhoneConstants.SUB1);
        SubscriptionInfo  mSubInfoRecord2 = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(PhoneConstants.SUB2);
        Log.d(TAG, "mSubInfoRecord1: " + mSubInfoRecord1 + ", mSubInfoRecord2: " + mSubInfoRecord2);
        if (mSubInfoRecord1 != null) {
            int subId1 = mSubInfoRecord1.getSubscriptionId();
            if ("RMT".equals(type)) {
                SharedPreferences.Editor editor = pre.edit();
                editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_RMT_ALERTS + subId1, value);
                editor.commit();
            } else if("EXERCISE".equals(type)) {
                SharedPreferences.Editor editor = pre.edit();
                editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_EXERCISE_ALERTS + subId1, value);
                editor.commit();
            }
            Log.d(TAG, "mContext: " + mContext + ", subId1: " + subId1);
            //CellBroadcastReceiver.startConfigService(mContext, subId1);
            CellBroadcastReceiver.startConfigService(mContext);
       }

       if (mSubInfoRecord2 != null) {
           int subId2 = mSubInfoRecord2.getSubscriptionId();
           if ("RMT".equals(type)) {
               SharedPreferences.Editor editor = pre.edit();
               editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_RMT_ALERTS + subId2, value);
               editor.commit();
           } else if ("EXERCISE".equals(type)) {
               SharedPreferences.Editor editor = pre.edit();
               editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_EXERCISE_ALERTS + subId2, value);
               editor.commit();
           }
           Log.d(TAG, "mContext: " + mContext + ", subId2: " + subId2);
           //CellBroadcastReceiver.startConfigService(mContext, subId2);
           CellBroadcastReceiver.startConfigService(mContext);
       }
    }

    //FR772564,new CLID variable to re-activate CB for NL
    public void sendCBSetIntent(Context mContext, boolean value, int sc_sim_id) {
        Log.i(TAG, "send-CB-enable-intent-simcard-changed");
        String ActionString = "com.android.cellbroadcastreceiver.setstartup";
        Intent intent = new Intent(ActionString);
        intent.putExtra("startup", value);
        //intent.putExtra(MSimConstants.SUBSCRIPTION_KEY,sc_sim_id);
        mContext.sendBroadcast(intent);
    }

    public void enableCBIfSimCardChanged(Context mContext, int newState) {
        if (newState != ServiceState.STATE_IN_SERVICE || newState != ServiceState.STATE_EMERGENCY_ONLY) {
            return;
        }

        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
            TelephonyManager tm = TelephonyManager.getDefault();
            for (int sc_sim_id = 0; sc_sim_id < tm.getPhoneCount(); sc_sim_id++) {
                String currentSerialNumber = tm.getSimSerialNumber(sc_sim_id);
                Log.i(TAG, ",currentSerialNumber = " + currentSerialNumber);
                String lastSerialNumber = null;

                if (sc_sim_id == 0) {
                    lastSerialNumber = sp.getString("key_last_sim_card_serial_number1", "");
                    sp.edit().putString("key_last_sim_card_serial_number1", currentSerialNumber).commit();
                } else if (sc_sim_id == 1 && !mEnableSingleSIM) {
                    lastSerialNumber = sp.getString("key_last_sim_card_serial_number2", "");
                    sp.edit().putString("key_last_sim_card_serial_number2", currentSerialNumber).commit();
                }

                Log.i(TAG, ",lastSerialNumber = " + lastSerialNumber);
                if ((lastSerialNumber != null ) && (lastSerialNumber != "") &&(currentSerialNumber != null) &&!lastSerialNumber.equals(currentSerialNumber)) {
                    if (sc_sim_id == 0) {
                        sp.edit().putBoolean("enable_channel_sim1", true).commit();
                    } else if(sc_sim_id == 1 && !mEnableSingleSIM) {
                        sp.edit().putBoolean("enable_channel_sim2", true).commit();
                    }
                    sendCBSetIntent(mContext, true, sc_sim_id);
                }
            }
        } else {
            TelephonyManager tm = TelephonyManager.getDefault();

            String currentSerialNumber = tm.getSimSerialNumber();
            Log.i(TAG, ",currentSerialNumber = " + currentSerialNumber);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
            String lastSerialNumber = sp.getString("key_last_sim_card_serial_number", "");
            Log.i(TAG, "lastSerialNumber = " + lastSerialNumber);
            if ((lastSerialNumber != null ) && (lastSerialNumber != "") && (currentSerialNumber != null) && !lastSerialNumber.equals(currentSerialNumber)) {
                sp.edit().putString("key_last_sim_card_serial_number", currentSerialNumber).commit();
                sendCBSetIntent(mContext, true, PhoneConstants.SUB1);
            }
        }
    }

    /*
     * set the customized cell broadcast channel from person at first boot
     * @param mContext
     */
    private void setCustomizedChannels(Context mContext) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);

        boolean bResetCbNvValue = settings.getBoolean(CONFIG_RESET_CBNV_VELUE, true);
        Log.i(TAG, " cdg-bResetCbNvValue >>>> " + bResetCbNvValue);
        if (bResetCbNvValue) {
            // step 1, get the channel name and number from perso
            String persoChannelNames = mContext.getResources().getString(
                    R.string.def_cellbroadcastreceiver_customized_channel_names);
            String persoChannelNumbersAndPolicy = mContext.getResources().getString(
                    R.string.def_cellbroadcastreceiver_customized_channel_numbers_policy);

            // FR708134
            Log.d(TAG,"cdg-setCustomizedChannels-ro.ssv.enabled :"+SystemProperties.getBoolean("ro.ssv.enabled", false));
            if (SystemProperties.getBoolean("ro.ssv.enabled", false)) {
                String cMccMnc = SystemProperties.get("persist.sys.lang.mccmnc", "");
                String operator = SystemProperties.get("ro.ssv.operator.choose", "");
                boolean resetCb = settings.getBoolean(PRE_RESET_CHANNEL, false);
                Log.i(TAG, "setCustomizedChannels:lang.mccmnc = " + cMccMnc + "  operator = " + operator);
                Log.i(TAG, "resetCb = " + resetCb + "   appNeedChange = " + appNeedChange);
                if (resetCb || appNeedChange) {
                    mContext.getContentResolver().delete(CellBroadcast.Channel.CONTENT_URI, null, null);

                    SharedPreferences.Editor editor = settings.edit();
                    Log.i(TAG, "load ssv channel: name = " + persoChannelNames + ",   number = " + persoChannelNumbersAndPolicy);
                    editor.putBoolean(PRE_RESET_CHANNEL, false);
                    editor.commit();
                    //Add backlater
                    //SsvManager.getInstance().appChangeComplete("cb");
                }
            }

            if (DBG) Log.i(TAG, "customized channel names: " + persoChannelNames);
            if (DBG) Log.i(TAG, "customized channel numbers: " + persoChannelNumbersAndPolicy);

            int namesLen = persoChannelNames.trim().length();
            int numbersLen = persoChannelNumbersAndPolicy.trim().length();
            int channellengh = namesLen;

            // step 1a, check the if the settings is valid
            if ((namesLen == 0 && numbersLen != 0)
                    || (numbersLen == 0 && namesLen != 0)) {
                if (DBG) Log.e(TAG, "detected length not equal, please check the perso settings");
            } else if (namesLen == 0 && numbersLen == 0) {
                if (DBG) Log.i(TAG, "empty customized channal");
                return;
            } else {
                String[] channelNames = persoChannelNames.split(";");
                String[] channelNumbersAndPolicy = persoChannelNumbersAndPolicy.split(";");

                int len = channelNumbersAndPolicy.length;
                String[] channelNumbers = new String[len];
                String[] channelPolicy = new String[len];
                channellengh = channelNumbers.length;

                if (channelNames.length != channelNumbers.length) {
                    System.out.println("detected wrong perso values, please check the perso settings");
                    channellengh = channelNumbers.length > channelNames.length ? channelNames.length : channelNumbers.length;
                    //return;
                }

                for (int i = 0; i < len; i++) {
                    String[] channelInfo = channelNumbersAndPolicy[i].split(",");
                    if (channelInfo.length == 2) {
                        channelNumbers[i] = channelInfo[0].trim();
                        channelPolicy[i] = channelInfo[1].trim();

                        if (channelPolicy[i].equals("+")) channelPolicy[i] = "Enable";
                        else if (channelPolicy[i].equals("-")) channelPolicy[i] = "Enable";
                        else if (channelPolicy[i].equals("*")) channelPolicy[i] = "Disable";
                        if (DBG) Log.i(TAG, "get right channel number and policy: " + channelNumbers[i] + "," + channelPolicy[i]);
                    } else {
                        if (DBG) Log.e(TAG, "detected wrong number and policy, please check the perso settings");
                        return;
                    }
                }

                // second, save channels into databases
                // FR-516039,
                int length = channellengh;
                if (length > 0) {
                    for (int i = 0; i < length; i++) {
                        ContentValues values = new ContentValues();
                        values.put(CellBroadcast.Channel.NAME, channelNames[i].trim());
                        values.put(CellBroadcast.Channel.INDEX, channelNumbers[i]);
                        values.put(CellBroadcast.Channel.Enable, channelPolicy[i]);
                        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                            for (int j = 0; j < TelephonyManager.getDefault().getPhoneCount(); j++) {
                                if (mEnableSingleSIM && j >= 1) {
                                    continue;
                                }
                                Uri uri = Uri.withAppendedPath(CellBroadcast.Channel.CONTENT_URI, "sub" + j);
                                mContext.getContentResolver().insert(uri, values);
                            }
                        } else {
                            mContext.getContentResolver().insert(CellBroadcast.Channel.CONTENT_URI,
                                    values);
                        }
                    }
                }
            }

            // at last, set check flag
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(CONFIG_RESET_CBNV_VELUE, false);
            editor.commit();
        }
    }

    private void resetCMASSettingsIfSimCardChanged(Context mContext) {
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(
                Context.TELEPHONY_SERVICE);

        String currentSerialNumber = tm.getSimSerialNumber();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sp.edit();

        String lastSerialNumber = sp.getString(KEY_LAST_SIM_CARD_SERIAL_NUMBER, "");

        if (!lastSerialNumber.equals(currentSerialNumber)) {
            editor.clear().commit();
            editor.putString(KEY_LAST_SIM_CARD_SERIAL_NUMBER, currentSerialNumber).commit();

            //ALM task 1697031
            if (mContext.getResources().getBoolean(R.bool.cellbroadcastreceiver_tf_resetcmas)){
                initEmergencyPref(mContext);
            }

            startConfigService(mContext);
        }
    }

    //PR-901210
    private void initEmergencyPref(Context context) {
        Log.d(TAG,"initEmergencyPref");
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();

        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_EXTREME_THREAT_ALERTS, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_SEVERE_THREAT_ALERTS, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_AMBER_ALERTS, true);

        //PR1696740
        boolean isShowSpanishLanguageAlerts = context.getResources().getBoolean(R.bool.def_showSpanishLanguageAlerts);
        if (isShowSpanishLanguageAlerts) {
            editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_SPANISH_LANGUAGE_ALERTS, false);
        }

        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_EMERGENCY_ALERTS, true);
        editor.putString(CellBroadcastSettings.KEY_ALERT_SOUND_DURATION, "4");
        editor.putString(CellBroadcastSettings.KEY_ALERT_REMINDER_INTERVAL, "0");
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_VIBRATE, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_SPEECH, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_ETWS_TEST_ALERTS, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CHANNEL_50_ALERTS, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CHANNEL_60_ALERTS, true);
        editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_CMAS_TEST_ALERTS, false);
        editor.putBoolean(CellBroadcastSettings.KEY_SHOW_CMAS_OPT_OUT_DIALOG, true);

        boolean isShowAudioAlerts = context.getResources().getBoolean(R.bool.def_cellbroadcastreceiver_audioSettingForTMO_on);
        if (isShowAudioAlerts) {
            editor.putBoolean(CellBroadcastSettings.KEY_ENABLE_ALERT_AUDIO, false);
        }

        //CellBroadcastReceiver.startConfigService(context, subId);
        CellBroadcastReceiver.startConfigService(context);
        editor.apply();
    }

    //Defect1953332
    private void initCBSetting(Context context) {
        int subDescription = PhoneConstants.SUB1;
        CBSUtills cbu = new CBSUtills(context);
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        cbu.saveCBLanguage("0" + "", subDescription);
        cbu.saveCBLanguage("0" + "");
        editor.putString("pref_key_choose_channel_sim1", "1");
        editor.putString("pref_key_choose_channel_sim2", "1");
        editor.putString("pref_key_choose_channel", "1");
        cbu.deleteChannel();
        editor.commit();
    }

    private void startConfigCB(Context mContext, int subId, int phoneId) {
        //BUG-1966416
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean cbEnable = sharedPrefs.getBoolean(CB_ALERT_DEFAULT_SETTING, true);
        Log.d(TAG, "CDGstartConfigCB CB_ALERT_DEFAULT_SETTING : " + cbEnable);

        boolean isBrazil50CbSupported = mContext.getResources().getBoolean(R.bool.show_brazil_settings);
        Log.d(TAG, "isBrazil50CbSupported A:" + isBrazil50CbSupported);

        String channelModeSim = null;
        SharedPreferences mSettings = mContext.getSharedPreferences(PREFS_NAME, 0);

        if (!TelephonyManager.getDefault().isMultiSimEnabled()) {//single simcard
            SmsManager manager = SmsManager.getDefault();
            CBSUtills cbu = new CBSUtills(mContext);
            SmsBroadcastConfigInfo[] cbi = cbu.getSmsBroadcastConfigInfo();
            channelModeSim = mSettings.getString("pref_key_choose_channel", "1");

            Log.i(TAG,"startCbConfig-onesimcard-channelModeSim = " + channelModeSim);
            if ("0".equalsIgnoreCase(channelModeSim)) {
                //channelMode is 0-999, enabled it
                manager.enableCellBroadcastRange(0, 999,ranType);
                //if channel index is more than 999, if it is selected, enabled it , else disabled
                if(cbi != null){
                    int num = cbi.length;
                    for (int i=0; i < num; i++) {
                        int index = cbi[i].getFromServiceId();
                        if (index >= 1000) {
                            if (cbEnable && cbi[i].isSelected()) { // MODIFIED by gang-chen, 2016-05-05,BUG-1966416
                                manager.enableCellBroadcastRange(index,index,ranType);
                            } else {
                                manager.disableCellBroadcastRange(index,index,ranType);
                            }
                        }
                    }
                }
            } else {
                //channelMode is "My channel list"
                if (cbi != null) {
                    int num = cbi.length;
                    for (int i=0; i<num;i++) {
                        int index = cbi[i].getFromServiceId();
                        if (cbEnable && cbi[i].isSelected()) { // MODIFIED by gang-chen, 2016-05-05,BUG-1966416
                            manager.enableCellBroadcastRange(index,index,ranType);
                        } else {
                            /* MODIFIED-BEGIN by gang-chen, 2016-08-09,BUG-2256539*/
                            if (!(isBrazil50CbSupported && index == 50)) {
                                manager.disableCellBroadcastRange(index, index, ranType);
                            } else {
                                Log.d(TAG, "CisBrazil50CbSupported is true and index == 50, ignore");
                            }
                            /* MODIFIED-END by gang-chen,BUG-2256539*/
                        }
                    }
                }
            }
        } else {//multi simcard
            SmsManager manager = SmsManager.getSmsManagerForSubscriptionId(subId);
            CBSUtills cbu = new CBSUtills(mContext);
            SmsBroadcastConfigInfo[] cbi = cbu.getSmsBroadcastConfigInfo(phoneId);

            if (phoneId == 0) {
                channelModeSim = mSettings.getString("pref_key_choose_channel_sim1", "1");
            } else if(phoneId == 1) {
                channelModeSim = mSettings.getString("pref_key_choose_channel_sim2", "1");
            }
            Log.i(TAG,"startCbConfig-multisimcard-channelModeSim = " + channelModeSim);

            if ((channelModeSim != null) && ("0".equalsIgnoreCase(channelModeSim))) {
                //channelMode is 0-999, enabled it
                manager.enableCellBroadcastRange(0, 999,ranType);
                //if channel index is more than 999, if it is selected, enabled it , else disabled
                if (cbi != null) {
                    int num = cbi.length;
                    for (int j = 0; j < num; j++) {
                        int index = cbi[j].getFromServiceId();
                        if (index >= 1000) {
                            if (cbEnable && cbi[j].isSelected()) { // MODIFIED by gang-chen, 2016-05-05,BUG-1966416
                                manager.enableCellBroadcastRange(index,index,ranType);
                            } else {
                                manager.disableCellBroadcastRange(index,index,ranType);
                            }
                        }
                    }
                }
            } else {
                //channelMode is "My channel list"
                if (cbi != null) {
                    int num = cbi.length;
                    for (int j = 0; j < num; j++) {
                        int index = cbi[j].getFromServiceId();
                        if (cbEnable && cbi[j].isSelected()) { // MODIFIED by gang-chen, 2016-05-05,BUG-1966416
                            manager.enableCellBroadcastRange(index,index,ranType);
                        } else {
                            /* MODIFIED-BEGIN by gang-chen, 2016-08-09,BUG-2256539*/
                            if (!(isBrazil50CbSupported && index == 50)) {
                                manager.disableCellBroadcastRange(index, index, ranType);
                            } else {
                                Log.d(TAG, "DisBrazil50CbSupported is true and index == 50 ignore");
                            }
                            /* MODIFIED-END by gang-chen,BUG-2256539*/
                        }
                    }
                }
            }
        }
    }

    private void startConfigCMAS(Context mContext,int subId) {
        //if (subId != -1) {
        //    startConfigService(mContext, subId);
        //} else {
            startConfigService(mContext);
        //}
    }

    private void startCbConfig(Context mContext, int newState, int subId) {
        Log.d(TAG,"startCbConfig-newState = " + newState + " subId = " + subId);
        int phoneId = SubscriptionManager.getPhoneId(subId);
        Log.d(TAG,"startCbConfig-phoneId = " + phoneId);

        if (!SubscriptionManager.isValidSubscriptionId(subId)) {
            return;
        }

        if (!SubscriptionManager.isValidPhoneId(phoneId)){
            return;
        }

        if (mEnableSingleSIM && phoneId >= 1) {
            return;
        }

        if (TelephonyManager.getDefault().isMultiSimEnabled()) {//multi simcard
            if (phoneId == 0) {//sim1
                SharedPreferences mSettings = mContext.getSharedPreferences(PREFS_NAME, 0);
                int mServiceStateSim1 = mSettings.getInt("mServiceStateSim1", -1);
                Log.d(TAG,"startCbConfig-mServiceStateSim1 = " + mServiceStateSim1);
                mSettings.edit().putInt("mServiceStateSim1", newState).commit();

                if ((mServiceStateSim1 != newState) && (newState == ServiceState.STATE_IN_SERVICE ||
                        newState == ServiceState.STATE_EMERGENCY_ONLY)) {
                    startConfigCMAS(mContext, subId);
                    Log.d(TAG,"Before startConfigCB-subId : " + subId);
                    startConfigCB(mContext, subId, phoneId);
                }
            } else if(phoneId == 1) {//sim2
                SharedPreferences mSettings = mContext.getSharedPreferences(PREFS_NAME, 0);
                int mServiceStateSim2 = mSettings.getInt("mServiceStateSim2", -1);
                Log.d(TAG, "startCbConfig-mServiceStateSim2 mServiceStateSim2");
                mSettings.edit().putInt("mServiceStateSim2", newState).commit();

                if((mServiceStateSim2 != newState) && (newState == ServiceState.STATE_IN_SERVICE ||
                        newState == ServiceState.STATE_EMERGENCY_ONLY)){
                    startConfigCMAS(mContext, subId);
                    startConfigCB(mContext, subId, phoneId);
                }
            }
        } else {
            SharedPreferences mSettings = mContext.getSharedPreferences(PREFS_NAME, 0);
            int mServiceState0 = mSettings.getInt("mServiceState", -1);
            Log.d(TAG,"startCbConfig - mServiceState0 = " + mServiceState0);
            mSettings.edit().putInt("mServiceState", newState).commit();

            if ((mServiceState0 != newState) && (newState == ServiceState.STATE_IN_SERVICE ||
                    newState == ServiceState.STATE_EMERGENCY_ONLY)) {//single simcard
                startConfigCMAS(mContext, -1);
                startConfigCB(mContext, 0, phoneId);
            }
        }
    }
}

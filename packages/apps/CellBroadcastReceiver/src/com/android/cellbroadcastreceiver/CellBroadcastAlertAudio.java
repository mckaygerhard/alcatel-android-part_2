/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.cellbroadcastreceiver;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;

import java.io.File;
import java.io.FileInputStream;
import static com.android.cellbroadcastreceiver.CellBroadcastReceiver.DBG;

/**
 * Manages alert audio and vibration and text-to-speech. Runs as a service so that
 * it can continue to play if another activity overrides the CellBroadcastListActivity.
 */
public class CellBroadcastAlertAudio extends Service implements TextToSpeech.OnInitListener,
        TextToSpeech.OnUtteranceCompletedListener {
    private static final String TAG = "CellBroadcastAlertAudio";

    /** Action to start playing alert audio/vibration/speech. */
    static final String ACTION_START_ALERT_AUDIO = "ACTION_START_ALERT_AUDIO";

    /** Extra for alert audio duration (from settings). */
    public static final String ALERT_AUDIO_DURATION_EXTRA =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_DURATION";

    /** Extra for message body to speak (if speech enabled in settings). */
    public static final String ALERT_AUDIO_MESSAGE_BODY =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_MESSAGE_BODY";

    /** Extra for text-to-speech preferred language (if speech enabled in settings). */
    public static final String ALERT_AUDIO_MESSAGE_PREFERRED_LANGUAGE =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_MESSAGE_PREFERRED_LANGUAGE";

    /** Extra for text-to-speech default language when preferred language is
        not available (if speech enabled in settings). */
    public static final String ALERT_AUDIO_MESSAGE_DEFAULT_LANGUAGE =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_MESSAGE_DEFAULT_LANGUAGE";

    /** Extra for alert audio vibration enabled (from settings). */
    public static final String ALERT_AUDIO_VIBRATE_EXTRA =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_VIBRATE";

    public static final String ALERT_AUDIO_TONE_EXTRA =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_TONE";

    /** Extra for alert audio ETWS behavior (always vibrate, even in silent mode). */
    public static final String ALERT_AUDIO_ETWS_VIBRATE_EXTRA =
            "com.android.cellbroadcastreceiver.ALERT_AUDIO_ETWS_VIBRATE";

    public static final String ALERT_AUDIO_PRESIDENT_TONE_VIBRATE_EXTRA =
        "com.android.cellbroadcastreceiver.ALERT_AUDIO_PRESIDENT_TONE_VIBRATE";

    private static final String TTS_UTTERANCE_ID = "com.android.cellbroadcastreceiver.UTTERANCE_ID";

    /** Pause duration between alert sound and alert speech. */
    private static final int PAUSE_DURATION_BEFORE_SPEAKING_MSEC = 1000;

    /** Duration of a CMAS alert. */
    private static final int CMAS_DURATION_MSEC = 10500;

    /** Vibration uses the same on/off pattern as the CMAS alert tone */
    private static final long[] sVibratePattern = { 0, 2000, 500, 1000, 500, 1000, 500,
            2000, 500, 1000, 500, 1000};

    private static final int STATE_IDLE = 0;
    private static final int STATE_ALERTING = 1;
    private static final int STATE_PAUSING = 2;
    private static final int STATE_SPEAKING = 3;

    private int mState;

    private TextToSpeech mTts;
    private boolean mTtsEngineReady;

    private String mMessageBody;
    private String mMessagePreferredLanguage;
    private String mMessageDefaultLanguage;
    private boolean mTtsLanguageSupported;
    private boolean mEnableVibrate;
    private boolean mEnableAudio;

    private Vibrator mVibrator;
    private MediaPlayer mMediaPlayer;
    private AudioManager mAudioManager;
    private TelephonyManager mTelephonyManager;
    private int mInitialCallState;
    private boolean mAudioManagerIsChanged;
    private int mOldRingerMode;
    private int mOldStreamVolume;

    private PendingIntent mPlayReminderIntent;

    // Internal messages
    private static final int ALERT_SOUND_FINISHED = 1000;
    private static final int ALERT_PAUSE_FINISHED = 1001;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    private static final int ALERT_SOUND_START_NA = 1100;
    private static final int CHILE_VIBRATOR_LOOP = 1002;
    private static final int CHILE_REPEAT_DURATION_VIBRATOR_SCREENOFF = 1000;
    private static final int CHILE_REPEAT_DURATION_VIBRATOR = 11000;
    boolean forceVibrateChile = false;
    boolean isPresientialAlert = false;

    public static final String ALERT_AUDIO_EXTRA = "com.android.cellbroadcastreceiver.ALERT_AUDIO";
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ALERT_SOUND_START_NA:
                    if (DBG) log("ALERT_SOUND_START_NA");
                    int duration = (int) msg.obj;
                    play(duration , forceVibrateChile);     // in milliseconds
                    break;
                case ALERT_SOUND_FINISHED:
                    if (DBG) log("ALERT_SOUND_FINISHED");
                    stop();     // stop alert sound
                    // if we can speak the message text
                    if (mMessageBody != null && mTtsEngineReady && mTtsLanguageSupported) {
                        mHandler.sendMessageDelayed(mHandler.obtainMessage(ALERT_PAUSE_FINISHED),
                                PAUSE_DURATION_BEFORE_SPEAKING_MSEC);
                        mState = STATE_PAUSING;
                    } else {
                        if (DBG) log("MessageEmpty = " + (mMessageBody == null) +
                                ", mTtsEngineReady = " + mTtsEngineReady +
                                ", mTtsLanguageSupported = " + mTtsLanguageSupported);
                        stopSelf();
                        mState = STATE_IDLE;
                    }
                    break;

                case ALERT_PAUSE_FINISHED:
                    if (DBG) log("ALERT_PAUSE_FINISHED");
                    int res = TextToSpeech.ERROR;
                    if (mMessageBody != null && mTtsEngineReady && mTtsLanguageSupported) {
                        if (DBG) log("Speaking broadcast text: " + mMessageBody);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                        if(!getResources().getBoolean(R.bool.def_ssv_emergency_cb_alert_always_on)) {
                            Bundle params = new Bundle();
                            // Play TTS in notification stream.
                            params.putInt(TextToSpeech.Engine.KEY_PARAM_STREAM,
                                    AudioManager.STREAM_NOTIFICATION);
                            // Use the non-public parameter 2 --> TextToSpeech.QUEUE_DESTROY for TTS.
                            // The entire playback queue is purged. This is different from QUEUE_FLUSH
                            // in that all entries are purged, not just entries from a given caller.
                            // This is for emergency so we want to kill all other TTS sessions.
                            res = mTts.speak(mMessageBody, 2, params, TTS_UTTERANCE_ID);
                            mState = STATE_SPEAKING;
                        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                    }
                    if (res != TextToSpeech.SUCCESS) {
                        loge("TTS engine not ready or language not supported or speak() failed");
                        stopSelf();
                        mState = STATE_IDLE;
                    }
                    break;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
                case CHILE_VIBRATOR_LOOP:
                    mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    mVibrator.vibrate(sVibratePattern, -1);
                    mHandler.sendMessageDelayed(
                            mHandler.obtainMessage(CHILE_VIBRATOR_LOOP), CHILE_REPEAT_DURATION_VIBRATOR)
                    ;
                    break;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

                default:
                    loge("Handler received unknown message, what=" + msg.what);
            }
        }
    };

    private final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String ignored) {
            // Stop the alert sound and speech if the call state changes.
            if (state != TelephonyManager.CALL_STATE_IDLE
                    && state != mInitialCallState) {
                stopSelf();
            }
        }
    };

    /**
     * Callback from TTS engine after initialization.
     * @param status {@link TextToSpeech#SUCCESS} or {@link TextToSpeech#ERROR}.
     */
    @Override
    public void onInit(int status) {
        if (DBG) log("onInit() TTS engine status: " + status);
        if (status == TextToSpeech.SUCCESS) {
            mTtsEngineReady = true;
            mTts.setOnUtteranceCompletedListener(this);
            // try to set the TTS language to match the broadcast
            setTtsLanguage();
        } else {
            mTtsEngineReady = false;
            mTts = null;
            loge("onInit() TTS engine error: " + status);
        }
    }

    /**
     * Try to set the TTS engine language to the preferred language. If failed, set
     * it to the default language. mTtsLanguageSupported will be updated based on the response.
     */
    private void setTtsLanguage() {

        String language = mMessagePreferredLanguage;
        if (language == null || language.isEmpty() ||
                TextToSpeech.LANG_AVAILABLE != mTts.isLanguageAvailable(new Locale(language))) {
            language = mMessageDefaultLanguage;
            if (language == null || language.isEmpty() ||
                    TextToSpeech.LANG_AVAILABLE != mTts.isLanguageAvailable(new Locale(language))) {
                mTtsLanguageSupported = false;
                return;
            }
            if (DBG) log("Language '" + mMessagePreferredLanguage + "' is not available, using" +
                    "the default language '" + mMessageDefaultLanguage + "'");
        }

        if (DBG) log("Setting TTS language to '" + language + '\'');

        try {
            int result = mTts.setLanguage(new Locale(language));
            if (DBG) log("TTS setLanguage() returned: " + result);
            mTtsLanguageSupported = (result == TextToSpeech.LANG_AVAILABLE);
        }
        catch (MissingResourceException e) {
            mTtsLanguageSupported = false;
            loge("Language '" + language + "' is not available.");
        }
    }

    /**
     * Callback from TTS engine.
     * @param utteranceId the identifier of the utterance.
     */
    @Override
    public void onUtteranceCompleted(String utteranceId) {
        if (utteranceId.equals(TTS_UTTERANCE_ID)) {
            // When we reach here, it could be TTS completed or TTS was cut due to another
            // new alert started playing. We don't want to stop the service in the later case.
            if (mState == STATE_SPEAKING) {
                stopSelf();
            }
        }
    }

    @Override
    public void onCreate() {
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // Listen for incoming calls to kill the alarm.
        mTelephonyManager =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(
                mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        getBaseContext().registerReceiver(mScreenOffReceiver, filter);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    public void onDestroy() {
        // stop audio, vibration and TTS
        stop();
        // Stop listening for incoming calls.
        mTelephonyManager.listen(mPhoneStateListener, 0);
        // shutdown TTS engine
        if (mTts != null) {
            try {
                mTts.shutdown();
            } catch (IllegalStateException e) {
                // catch "Unable to retrieve AudioTrack pointer for stop()" exception
                loge("exception trying to shutdown text-to-speech");
            }
        }
        if (mEnableAudio) {
            // Release the audio focus so other audio (e.g. music) can resume.
            // Do not do this in stop() because stop() is also called when we stop the tone (before
            // TTS is playing). We only want to release the focus when tone and TTS are played.
            mAudioManager.abandonAudioFocus(null);
        }


//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        getBaseContext().unregisterReceiver(mScreenOffReceiver);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // release CPU wake lock acquired by CellBroadcastAlertService
        CellBroadcastAlertWakeLock.releaseCpuLock();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // No intent, tell the system not to restart us.
        if (intent == null) {
            stopSelf();
            return START_NOT_STICKY;
        }

        // This extra should always be provided by CellBroadcastAlertService,
        // but default to 10.5 seconds just to be safe (CMAS requirement).
        int duration = intent.getIntExtra(ALERT_AUDIO_DURATION_EXTRA, CMAS_DURATION_MSEC);
        if (DBG) log("Duration: " + duration);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        forceVibrateChile = intent.getBooleanExtra("forceVibrate", false);
        isPresientialAlert = intent.getBooleanExtra("PresientialAlert", false);
        Log.i(TAG, "chile, isPresientialAlert= "+isPresientialAlert);
        if (forceVibrateChile) {
            Log.i(TAG, "chile, it is always vibrate,even in silent mode ");
            mEnableVibrate = forceVibrateChile;
            switch (mAudioManager.getRingerMode()) {
                case AudioManager.RINGER_MODE_SILENT:
                case AudioManager.RINGER_MODE_VIBRATE:
                    mEnableAudio = false;
                    break;
                case AudioManager.RINGER_MODE_NORMAL:
                default:
                    mEnableAudio = true;
                    break;
            }
        } else {

            // Get text to speak (if enabled by user)
            mMessageBody = intent.getStringExtra(ALERT_AUDIO_MESSAGE_BODY);
            mMessagePreferredLanguage = intent.getStringExtra(ALERT_AUDIO_MESSAGE_PREFERRED_LANGUAGE);
            mMessageDefaultLanguage = intent.getStringExtra(ALERT_AUDIO_MESSAGE_DEFAULT_LANGUAGE);

            if (getResources().getBoolean(
                    R.bool.config_regional_wea_alert_tone_enable)) {
                mEnableAudio = intent.getBooleanExtra(ALERT_AUDIO_TONE_EXTRA, false);
            }
            mEnableVibrate = intent.getBooleanExtra(ALERT_AUDIO_VIBRATE_EXTRA, true);
            if (!getResources().getBoolean(
                    R.bool.config_regional_presidential_wea_with_tone_vibrate)
                    && intent.getBooleanExtra(ALERT_AUDIO_ETWS_VIBRATE_EXTRA, false)) {
                mEnableVibrate = true;  // force enable vibration for ETWS alerts
            }

            switch (mAudioManager.getRingerMode()) {
                case AudioManager.RINGER_MODE_SILENT:
                    if (DBG) log("Ringer mode: silent");
                    mEnableAudio = false;
                    if (Settings.System.getInt(getContentResolver(),
                                Settings.System.VIBRATE_WHEN_RINGING, 0) == 0) {
                        mEnableVibrate = false;
                    }
                    break;

                case AudioManager.RINGER_MODE_VIBRATE:
                    if (DBG) log("Ringer mode: vibrate");
                    mEnableAudio = false;
                    break;

                case AudioManager.RINGER_MODE_NORMAL:
                default:
                    if (DBG) log("Ringer mode: normal");
                    if (Settings.System.getInt(getContentResolver(),
                                Settings.System.VIBRATE_WHEN_RINGING, 0) == 0) {
                        mEnableVibrate = false;
                    }
                    if (!(getResources().getBoolean(
                            R.bool.config_regional_wea_alert_tone_enable))) {
                        mEnableAudio = true;
                    }
                    break;
            }

            if (getResources().getBoolean(
                    R.bool.config_regional_presidential_wea_with_tone_vibrate)
                    && intent.getBooleanExtra(ALERT_AUDIO_PRESIDENT_TONE_VIBRATE_EXTRA, false)) {
                mEnableVibrate = true;
                mEnableAudio = true;
                changeAudioManagerForWeaPresidential(); //change ringer mode & volume for President WEA
            }

            if (mMessageBody != null && mEnableAudio) {
                if (mTts == null) {
                    mTts = new TextToSpeech(this, this);
                } else if (mTtsEngineReady) {
                    setTtsLanguage();
                }
            }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            if(isPresientialAlert){
                mEnableVibrate = true;
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (mEnableAudio || mEnableVibrate) {

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
            // play(duration);     // in milliseconds
            play(duration , forceVibrateChile);     // in milliseconds

            if (getResources().getBoolean(R.bool.def_cellbroadcastreceiver_alertAudioRuleForTMO)) {
                mHandler.sendMessageDelayed(mHandler.obtainMessage(ALERT_SOUND_START_NA, duration), 60 * 1000);
                mHandler.sendMessageDelayed(mHandler.obtainMessage(ALERT_SOUND_START_NA, duration), 3 * 60 * 1000);
                mHandler.sendMessageDelayed(mHandler.obtainMessage(ALERT_SOUND_START_NA, duration), 5 * 60 * 1000);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        } else {
            stopSelf();
            return START_NOT_STICKY;
        }

        // Record the initial call state here so that the new alarm has the
        // newest state.
        mInitialCallState = mTelephonyManager.getCallState();

        return START_STICKY;
    }

    /**
     * Force RingMode to normal and stream volume to max for presidential WEA
     * messages.
     * mOldRingerMode and mOldStreamVolume will be updated to record orginal settings.
     */
    private void changeAudioManagerForWeaPresidential() {
        mAudioManagerIsChanged = false;

        //save original RingerMode and force it to normal
        mOldRingerMode = mAudioManager.getRingerMode();
        if(mOldRingerMode != AudioManager.RINGER_MODE_NORMAL) {
            mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            mAudioManagerIsChanged = true;
        }

        //save original Stream Volume and force it to max (5)
        mOldStreamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        if(mAudioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) != 5) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 5, 0);
            mAudioManagerIsChanged = true;
        }
    }

    /**
     * Restore RingMode and stream volume to original settings after presidential WEA
     * messages.
     */
    private void restoreAudioManagerIfChanged() {
        if (!mAudioManagerIsChanged) {
            if (DBG) log("AudioManager no change");
            return;
        }

        if (mAudioManager.getRingerMode() != mOldRingerMode) {
            mAudioManager.setRingerMode(mOldRingerMode);
            if (DBG) log("AudioManager restore RingerMode to " + mOldRingerMode);
        }

        if (mAudioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) !=
            mOldStreamVolume) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, mOldStreamVolume, 0);
            if (DBG) log("AudioManager restore stream volume to " + mOldStreamVolume);
        }

        mAudioManagerIsChanged = false;
    }

    // Volume suggested by media team for in-call alarms.
    private static final float IN_CALL_VOLUME = 0.125f;

    /**
     * Start playing the alert sound, and send delayed message when it's time to stop.
     * @param duration the alert sound duration in milliseconds
     */
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    //private void play(int duration) {
    private void play(int duration , boolean forceVibrateChile) {
        //[FEATURE]-Add-END by TCTNB.Dandan.Fang

        // stop() checks to see if we are already playing.
        stop();

        if (DBG) log("play()");

        // Start the vibration first.
        if (mEnableVibrate) {
            // mVibrator.vibrate(sVibratePattern, -1);
            if (forceVibrateChile) {
                mVibrator.vibrate(sVibratePattern, -1);
                mHandler.sendMessageDelayed(
                        mHandler.obtainMessage(CHILE_VIBRATOR_LOOP),
                        CHILE_REPEAT_DURATION_VIBRATOR);
            } else {
                mVibrator.vibrate(sVibratePattern, -1);
                 //ALM-667699
                if((getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_forceVibrateForChile_on) && isPresientialAlert)
                    || getResources().getBoolean(R.bool.def_ssv_emergency_cb_alert_always_on)) {
                    mHandler.sendMessageDelayed(
                            mHandler.obtainMessage(CHILE_VIBRATOR_LOOP),
                            CHILE_REPEAT_DURATION_VIBRATOR);
                }
            }
        }

        if (mEnableAudio) {
            // future optimization: reuse media player object
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnErrorListener(new OnErrorListener() {
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    loge("Error occurred while playing audio.");
                    mp.stop();
                    mp.release();
                    mMediaPlayer = null;
                    return true;
                }
            });

            mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    if (DBG) log("Audio playback complete.");
                    mHandler.sendMessage(mHandler.obtainMessage(ALERT_SOUND_FINISHED));
                    mMediaPlayer.start();
                    return;
                }
            });

            try {
                // Check if we are in a call. If we are, play the alert
                // sound at a low volume to not disrupt the call.
                if (mTelephonyManager.getCallState()
                        != TelephonyManager.CALL_STATE_IDLE) {
                    log("in call: reducing volume");
                    mMediaPlayer.setVolume(IN_CALL_VOLUME, IN_CALL_VOLUME);
                }

                //FR400297,
                //CBC notification with pop up and tone alert + vibrate in CHILE
                if (forceVibrateChile) {
                    Log.i(TAG,"use specified tone for chile");
                    //ALM-667699]
					//[[SMSCB] Cell Broadcast messages have incorrect format]
                    if(getResources().getBoolean(R.bool.def_cellbroadcastreceiver_use_cmas_ringToneForChile)){
                        setDataSourceFromResource(getResources(), mMediaPlayer,R.raw.attention_signal);
                    }else {
                        String cbRingtoneName = this.getResources().getString(R.string.def_cellbroadcastreceiver_ringToneForChile);
                        String defaultToneUri = null;
                        String fileInfo = null;
                        Cursor c = null;
                        try {
                            c = this.getContentResolver().query(
                                    Uri.parse("content://media/internal/audio/media"), null, "_display_name=?",
                                    new String[] {
                                            cbRingtoneName
                                    }, null);

                            if (c != null && c.moveToFirst()) {
                                String id = c.getString(c.getColumnIndex("_id"));
                                defaultToneUri = "content://media/internal/audio/media/" + id;
                                fileInfo = c.getString(c.getColumnIndex("_data"));

                            } else {
                                Log.w(TAG, "Not found default cb ringtone name:" + cbRingtoneName);
                            }
                        } catch (SQLiteException e) {
                            Log.e(TAG, "SQLiteException:" + e.getMessage());
                        } finally {
                            if (c != null) {
                                c.close();
                            }
                        }
                        //chile no cbringtone play  FIXME: 12/16/15
                        //mMediaPlayer.setDataSource(defaultToneUri);
						Log.d(TAG,"defaultToneUri : "+ defaultToneUri);
                        Log.d(TAG,"fileInfo : "+ fileInfo);
                        mMediaPlayer.reset();
                        mMediaPlayer.setDataSource(fileInfo);
                    }

                } else {
                    // start playing alert audio (unless master volume is vibrate only or silent).
                    setDataSourceFromResource(getResources(), mMediaPlayer,
                            R.raw.attention_signal);
                }

//[FEATURE]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/31/2016, TASK-2781160
//[NL] Fixed CMAS Alert Tone
                boolean isFixedAlert = getResources().getBoolean(R.bool.def_cellbroadcastreceiver_fixedAlert_on);
                if (isFixedAlert) {
                    mAudioManager.requestAudioFocus(null, AudioManager.STREAM_ALARM, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                    int mMaxLevel = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, (int)(mMaxLevel), AudioManager.FLAG_FIXED_VOLUME);
                } else {
                    mAudioManager.requestAudioFocus(null, AudioManager.STREAM_NOTIFICATION, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                }

                // if the duration isn't equal to one play of the full 10.5s file then play
                // with looping enabled.
                startAlarm(mMediaPlayer, duration != CMAS_DURATION_MSEC, isFixedAlert);
//[FEATURE]-Add-END by TCTNB.(JiangLong Pan)
            } catch (Exception ex) {
                loge("Failed to play alert sound: " + ex);
            }
        }

        // stop alert after the specified duration, unless we are playing the full 10.5s file once
        // in which case we'll use the end of playback callback rather than a delayed message.
        // This is to avoid the CMAS alert potentially being truncated due to audio playback lag.
        //FR400297 CBC notification with pop up and tone alert + vibrate in CHILE
        if(!getResources().getBoolean(R.bool.def_ssv_emergency_cb_alert_always_on)) {
            if (!forceVibrateChile) {
               	//ALM-667699]
				//[[SMSCB] Cell Broadcast messages have incorrect format]
                if(isPresientialAlert){
                    //do nothing
                } else {
                    //if (duration != CMAS_DURATION_MSEC) {
                    mHandler.sendMessageDelayed(
                            mHandler.obtainMessage(ALERT_SOUND_FINISHED), duration);
                }
               	//[Task 667699]-Add-END-by TCTSH.gang-chen@tcl.com,10/14/2015
            }
        }
        mState = STATE_ALERTING;
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

//[FEATURE]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/31/2016, TASK-2781160
//[NL] Fixed CMAS Alert Tone
    // Do the common stuff when starting the alarm.
    private static void startAlarm(MediaPlayer player, boolean looping, boolean isFixedAlert)
            throws java.io.IOException, IllegalArgumentException, IllegalStateException {
        if (isFixedAlert) {
            player.setAudioStreamType(AudioManager.STREAM_ALARM);
        } else {
            player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
        }
//[FEATURE]-Add-END by TCTNB.(JiangLong Pan)
        player.setLooping(looping);
        player.prepare();
        player.start();
    }

    private static void setDataSourceFromResource(Resources resources,
            MediaPlayer player, int res) throws java.io.IOException {
        AssetFileDescriptor afd = resources.openRawResourceFd(res);
        if (afd != null) {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            afd.close();
        }
    }

    private void playAlertReminderSound() {
        Uri notificationUri = RingtoneManager.getDefaultUri(
                RingtoneManager.TYPE_NOTIFICATION | RingtoneManager.TYPE_ALARM);
        if (notificationUri == null) {
            loge("Can't get URI for alert reminder sound");
            return;
        }
        Ringtone r = RingtoneManager.getRingtone(this, notificationUri);
        if (r != null) {
            log("playing alert reminder sound");
            r.play();
        } else {
            loge("can't get Ringtone for alert reminder sound");
        }
    }

    /**
     * Stops alert audio and speech.
     */
    public void stop() {
        if (DBG) log("stop()");

        if (mPlayReminderIntent != null) {
            mPlayReminderIntent.cancel();
            mPlayReminderIntent = null;
        }

        mHandler.removeMessages(ALERT_SOUND_FINISHED);
        mHandler.removeMessages(ALERT_PAUSE_FINISHED);
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
        // CBC notification with pop up and tone alert + vibrate in CHILE
        if (forceVibrateChile) {
            mHandler.removeMessages(CHILE_VIBRATOR_LOOP);
        }

        if(getResources().getBoolean(R.bool.feature_cellbroadcastreceiver_forceVibrateForChile_on) && isPresientialAlert){
            mHandler.removeMessages(CHILE_VIBRATOR_LOOP);
        }

        if (getResources().getBoolean(R.bool.def_ssv_emergency_cb_alert_always_on)) {
            mHandler.removeMessages(CHILE_VIBRATOR_LOOP);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (mState == STATE_ALERTING) {
            // Stop audio playing
            if (mMediaPlayer != null) {
                try {
                    mMediaPlayer.stop();
                    mMediaPlayer.release();
                } catch (IllegalStateException e) {
                    // catch "Unable to retrieve AudioTrack pointer for stop()" exception
                    loge("exception trying to stop media player");
                }
                mMediaPlayer = null;
            }

            // Stop vibrator
            mVibrator.cancel();
            if (getResources().getBoolean(
                    R.bool.config_regional_presidential_wea_with_tone_vibrate)) {
                restoreAudioManagerIfChanged(); //restore user setting after presidental WEA
            }
        } else if (mState == STATE_SPEAKING && mTts != null) {
            try {
                mTts.stop();
            } catch (IllegalStateException e) {
                // catch "Unable to retrieve AudioTrack pointer for stop()" exception
                loge("exception trying to stop text-to-speech");
            }
        }
        mState = STATE_IDLE;
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    private static void loge(String msg) {
        Log.e(TAG, msg);
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    // CBC notification with pop up and tone alert + vibrate in CHILE
    // fix error :when screen off , vibrator stop.
    BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                if (forceVibrateChile) {
                    Log.i("fangdandan","screen is off , need to turn vibrate on");
                    mHandler.sendMessageDelayed(mHandler
                                    .obtainMessage(CHILE_VIBRATOR_LOOP),
                            CHILE_REPEAT_DURATION_VIBRATOR_SCREENOFF);
                }
            }
        }
    };
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}

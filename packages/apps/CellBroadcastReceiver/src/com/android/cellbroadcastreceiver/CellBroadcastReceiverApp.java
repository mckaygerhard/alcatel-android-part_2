/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.cellbroadcastreceiver;

import android.app.Application;
import android.telephony.CellBroadcastMessage;
import android.util.Log;
import android.preference.PreferenceManager;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * The application class loads the default preferences at first start,
 * and remembers the time of the most recently received broadcast.
 */
public class CellBroadcastReceiverApp extends Application {
    private static final String TAG = "CellBroadcastReceiverApp";

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    public static CellBroadcastReceiverApp instance;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    public void onCreate() {
        super.onCreate();
        // TODO: fix strict mode violation from the following method call during app creation
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related  
		instance = this;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    /** List of unread non-emergency alerts to show when user selects the notification. */
    private static final ArrayList<CellBroadcastMessage> sNewMessageList =
            new ArrayList<CellBroadcastMessage>(4);

    /** Latest area info cell broadcast received. */
    private static Map<Integer, CellBroadcastMessage> sLatestAreaInfo =
            new HashMap<Integer, CellBroadcastMessage>();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283
//Porting CellBroadcast Related
    public static Map<Integer,Integer> notificationid =
            new HashMap<Integer,Integer>();
    private static final int NOTIFICATION_ID = 0;
    private static int repeatnotification = 0;

    static ArrayList<CellBroadcastMessage> addNewMessageToListRussia(CellBroadcastMessage message) {
        Log.d(TAG, "JYG,CellbroadcastReceiverApp,addNewMessageToListRussia,message.getServiceCategory():" + message.getServiceCategory());
        setNotificationId(message.getServiceCategory());
        Iterator iter = sNewMessageList.iterator();
        while (iter.hasNext()) {
            CellBroadcastMessage cbm = (CellBroadcastMessage)iter.next();
            if (cbm.getServiceCategory() == message.getServiceCategory()) {
                Log.d(TAG,"cdg This mesage is repeat id: "+cbm.getServiceCategory());
                clearMessageList(cbm);
                sNewMessageList.add(message);
                return sNewMessageList;
            }
        }
        sNewMessageList.add(message);
        return sNewMessageList;
    }

    static void setNotificationId(int id){
        Log.d(TAG,"JYG,CellbroadcastReceiverApp,setNotificationId,id:"+id);
        notificationid.put(id,id);
    }

    //777440
    synchronized public static CellBroadcastReceiverApp getApplication(){
        return instance;
    }

    static int getNotificationId(int id){
        if(notificationid.get(id)==null||notificationid.get(id)==0){
            Log.d(TAG,"JYG,CellbroadcastReceiverApp,getNotificationId,NOTIFICATION_ID");
            return NOTIFICATION_ID;
        }
        Log.d(TAG,"JYG,CellbroadcastReceiverApp,getNotificationId,id:"+id);
        return notificationid.get(id);
    }

    /** Clears duplicate messages. */
    static void clearMessageList(CellBroadcastMessage cbm) {
        sNewMessageList.remove(cbm);
        repeatnotification = cbm.getServiceCategory();
        Log.d(TAG,"JYG,CellbroadcastReceiverApp,clearMessageList,repeatnotification:"+repeatnotification);
    }

    static void setDefaultRepeatnotification(){
        repeatnotification = 0;
    }

    static int getRepeatNotification() {
        if(repeatnotification!=NOTIFICATION_ID){
            Log.d(TAG,"jia,CellbroadcastReceiverApp,getRepeatNotification,repeatnotification:"+repeatnotification);
            return repeatnotification;
        }
        Log.d(TAG,"JYG,CellbroadcastReceiverApp,getRepeatNotification,NOTIFICATION_ID");
        return NOTIFICATION_ID;
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /** Adds a new unread non-emergency message and returns the current list. */
    static ArrayList<CellBroadcastMessage> addNewMessageToList(CellBroadcastMessage message) {
        sNewMessageList.add(message);
        return sNewMessageList;
    }

    /** Clears the list of unread non-emergency messages. */
    static void clearNewMessageList() {
        sNewMessageList.clear();
    }

    /** Saves the latest area info broadcast received. */
    static void setLatestAreaInfo(CellBroadcastMessage areaInfo) {
        sLatestAreaInfo.put(areaInfo.getSubId(), areaInfo);
    }

    /** Returns the latest area info broadcast received. */
    static CellBroadcastMessage getLatestAreaInfo(int subId) {
        return sLatestAreaInfo.get(subId);
    }
}

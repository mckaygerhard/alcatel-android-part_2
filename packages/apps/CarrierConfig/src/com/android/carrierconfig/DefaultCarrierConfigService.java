/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/16/2016|     Dandan.Fang      |     TASK2657073      |[ACTION] VoLTE/V- */
/*           |                      |                      |oWIFI button con- */
/*           |                      |                      |figure            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.carrierconfig;

import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import android.service.carrier.CarrierIdentifier;
import android.service.carrier.CarrierService;
import android.telephony.CarrierConfigManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import com.android.internal.util.FastXmlSerializer;

//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/16/2016,TASK2657073 ,
//[ACTION] VoLTE/VoWIFI button configure
import java.io.*;
import android.os.SystemProperties;
import android.text.TextUtils;
import java.io.FileNotFoundException;
//[BUGFIX]-Add-END by TCTNB.Dandan.Fang

/**
 * Provides network overrides for carrier configuration.
 *
 * The configuration available through CarrierConfigManager is a combination of default values,
 * default network overrides, and carrier overrides. The default network overrides are provided by
 * this service. For a given network, we look for a matching XML file in our assets folder, and
 * return the PersistableBundle from that file. Assets are preferred over Resources because resource
 * overlays only support using MCC+MNC and that doesn't work with MVNOs. The only resource file used
 * is vendor.xml, to provide vendor-specific overrides.
 */
public class DefaultCarrierConfigService extends CarrierService {

    private static final String TAG = "DefaultCarrierConfigService";

    private XmlPullParserFactory mFactory;
  //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/16/2016,TASK2657073 ,
  //[ACTION] VoLTE/VoWIFI button configure
    private static String region_filename = "/system/etc/region_ver";
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    public DefaultCarrierConfigService() {
        Log.d(TAG, "Service created");
        mFactory = null;
    }

    /**
     * Returns per-network overrides for carrier configuration.
     *
     * This returns a carrier config bundle appropriate for the given network by reading data from
     * files in our assets folder. First we look for a file named after the MCC+MNC of {@code id}
     * and then we read res/xml/vendor.xml. Both files may contain multiple bundles with filters on
     * them. All the matching bundles are flattened to return one carrier config bundle.
     */
    @Override
    public PersistableBundle onLoadConfig(CarrierIdentifier id) {
        Log.d(TAG, "Config being fetched");

        if (id == null) {
            return null;
        }


        PersistableBundle config = null;
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/16/2016,TASK2657073 ,
        //[ACTION] VoLTE/VoWIFI button configure
        String defaultfileName = "carrier_config_" + id.getMcc() + id.getMnc() + ".xml";
        boolean getConfigDefault = false;
        try {
            synchronized (this) {
                if (mFactory == null) {
                    mFactory = XmlPullParserFactory.newInstance();
                }
            }
            Log.i(TAG," getRegionInformation = " + getRegionInformation());
            String region = getRegionInformation();
            String fileName = defaultfileName;
            if(!TextUtils.isEmpty(region)){
                fileName = region+"/" + fileName;
                Log.i(TAG,"Final fileName = " + fileName);
                XmlPullParser parser = mFactory.newPullParser();
                parser.setInput(getApplicationContext().getAssets().open(fileName), "utf-8");
                config = readConfigFromXml(parser, id);
            }else{
                getConfigDefault = true;
            }
        }catch (IOException | XmlPullParserException e) {
            Log.d(TAG, e.toString());
            // We can return an empty config for unknown networks.
            config = new PersistableBundle();

            if(e instanceof FileNotFoundException){
                getConfigDefault = true;
            }
        }

        if(getConfigDefault){
            Log.i(TAG,"Region file get fail. try to get configuration from default file");
            try {
                synchronized (this) {
                    if (mFactory == null) {
                        mFactory = XmlPullParserFactory.newInstance();
                    }
                }

                XmlPullParser parser = mFactory.newPullParser();
                parser.setInput(getApplicationContext().getAssets().open("NA/"+defaultfileName), "utf-8");
                config = readConfigFromXml(parser, id);
            }
            catch (IOException | XmlPullParserException e) {
                Log.d(TAG, e.toString());
                // We can return an empty config for unknown networks.
                config = new PersistableBundle();
            }
        }
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

        // Treat vendor.xml as if it were appended to the carrier config file we read.
        XmlPullParser vendorInput = getApplicationContext().getResources().getXml(R.xml.vendor);
        try {
            PersistableBundle vendorConfig = readConfigFromXml(vendorInput, id);
            config.putAll(vendorConfig);
        }
        catch (IOException | XmlPullParserException e) {
            Log.e(TAG, e.toString());
        }

        return config;
    }

    /**
     * Parses an XML document and returns a PersistableBundle.
     *
     * <p>This function iterates over each {@code <carrier_config>} node in the XML document and
     * parses it into a bundle if its filters match {@code id}. The format of XML bundles is defined
     * by {@link PersistableBundle#restoreFromXml}. All the matching bundles will be flattened and
     * returned as a single bundle.</p>
     *
     * <p>Here is an example document. The second bundle will be applied to the first only if the
     * GID1 is ABCD.
     * <pre>{@code
     * <carrier_config_list>
     *     <carrier_config>
     *         <boolean name="voicemail_notification_persistent_bool" value="true" />
     *     </carrier_config>
     *     <carrier_config gid1="ABCD">
     *         <boolean name="voicemail_notification_persistent_bool" value="false" />
     *     </carrier_config>
     * </carrier_config_list>
     * }</pre></p>
     *
     * @param parser an XmlPullParser pointing at the beginning of the document.
     * @param id the details of the SIM operator used to filter parts of the document
     * @return a possibly empty PersistableBundle containing the config values.
     */
    static PersistableBundle readConfigFromXml(XmlPullParser parser, CarrierIdentifier id)
            throws IOException, XmlPullParserException {
        PersistableBundle config = new PersistableBundle();

        if (parser == null) {
          return config;
        }

        // Iterate over each <carrier_config> node in the document and add it to the returned
        // bundle if its filters match.
        int event;
        while (((event = parser.next()) != XmlPullParser.END_DOCUMENT)) {
            if (event == XmlPullParser.START_TAG && "carrier_config".equals(parser.getName())) {
                // Skip this fragment if it has filters that don't match.
                if (!checkFilters(parser, id)) {
                    continue;
                }
                PersistableBundle configFragment = PersistableBundle.restoreFromXml(parser);
                config.putAll(configFragment);
            }
        }

        return config;
    }

    /**
     * Checks to see if an XML node matches carrier filters.
     *
     * <p>This iterates over the attributes of the current tag pointed to by {@code parser} and
     * checks each one against {@code id} or {@link Build.DEVICE}. Attributes that are not specified
     * in the node will not be checked, so a node with no attributes will always return true. The
     * supported filter attributes are,
     * <ul>
     *   <li>mcc: {@link CarrierIdentifier#getMcc}</li>
     *   <li>mnc: {@link CarrierIdentifier#getMnc}</li>
     *   <li>gid1: {@link CarrierIdentifier#getGid1}</li>
     *   <li>gid2: {@link CarrierIdentifier#getGid2}</li>
     *   <li>spn: {@link CarrierIdentifier#getSpn}</li>
     *   <li>device: {@link Build.DEVICE}</li>
     * </ul>
     * </p>
     *
     * @param parser an XmlPullParser pointing at a START_TAG with the attributes to check.
     * @param id the carrier details to check against.
     * @return false if any XML attribute does not match the corresponding value.
     */
    static boolean checkFilters(XmlPullParser parser, CarrierIdentifier id) {
        boolean result = true;
        for (int i = 0; i < parser.getAttributeCount(); ++i) {
            String attribute = parser.getAttributeName(i);
            String value = parser.getAttributeValue(i);
            switch (attribute) {
                case "mcc":
                    result = result && value.equals(id.getMcc());
                    break;
                case "mnc":
                    result = result && value.equals(id.getMnc());
                    break;
                case "gid1":
                    result = result && value.equals(id.getGid1());
                    break;
                case "gid2":
                    result = result && value.equals(id.getGid2());
                    break;
                case "spn":
                    result = result && value.equals(id.getSpn());
                    break;
                case "device":
                    result = result && value.equals(Build.DEVICE);
                    break;
                default:
                    Log.e(TAG, "Unknown attribute " + attribute + "=" + value);
                    result = false;
                    break;
            }
        }
        return result;
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/16/2016,TASK2657073 ,
    //[ACTION] VoLTE/VoWIFI button configure
    //different region,use different configuration
    String getRegionInformation() throws IOException{
        String regionString = null;
        String regionNumber = SystemProperties.get("def.region.for.perso","");

        Log.i(TAG,"regionNumber = " + regionNumber);
        if(!TextUtils.isEmpty(regionNumber)){
            regionString = parserRegion(regionNumber);
        }
        return regionString;
    }

    private String parserRegion(String num_region) throws IOException {
        HashMap<String, String> attributes = new HashMap<String, String>();
        String result = new String();
        File f = new File(region_filename);
        if (f.exists()) {
            String s;
            String[] m_line = new String[5];
            BufferedReader in = new BufferedReader(new FileReader(
                    region_filename));
            StringBuilder sb = new StringBuilder();
            while ((s = in.readLine()) != null) {
                m_line = s.trim().split("->");
                if (m_line != null && m_line.length > 1 && m_line[0] != null
                        && m_line[0] != null) {
                    android.util.Log.d(TAG, "key is " + m_line[0]
                            + ",value is " + m_line[1]);
                    attributes.put(m_line[0].trim(), m_line[1].trim());
                }
            }
            in.close();
            result = attributes.get(num_region);
        } else {
            android.util.Log.e(TAG, "the file:" + region_filename
                    + "read failed!");
        }
        return result;
    }
      //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
}

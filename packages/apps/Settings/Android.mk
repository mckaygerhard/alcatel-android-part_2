LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#ADD-BEGIN by Dingyi  2016/08/17 SOLUTION 2521545
# Module name should match library/file name to be installed.
LOCAL_MODULE := disableapplist.xml
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := pkgpreventlist/disableapplist.xml
# set class according to lib/file attribute
LOCAL_MODULE_CLASS := ETC
# $(TARGET_OUT) points to system/ folder
LOCAL_MODULE_PATH := $(TARGET_OUT)/etc
include $(BUILD_PREBUILT)
#####################################################################################3

include $(CLEAR_VARS)
#ADD-END by Dingyi  2016/08/17 SOLUTION 2521545
#ADD-qcrilhook by Dandan.Fang  2016/08/30 TASK2817986
LOCAL_JAVA_LIBRARIES := bouncycastle core-oj telephony-common ims-common qcrilhook
LOCAL_STATIC_JAVA_LIBRARIES := \
    android-support-v4 \
    android-support-v13 \
    android-support-v7-recyclerview \
    android-support-v7-preference \
    android-support-v7-appcompat \
    android-support-v14-preference \
    android-support-design \
    jsr305

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
        $(call all-java-files-under, src) \
        $(call all-java-files-under, tct-src) \
        src/com/android/settings/EventLogTags.logtags
LOCAL_SRC_FILES += src/org/codeaurora/wfcservice/IWFCService.aidl \
                   src/org/codeaurora/wfcservice/IWFCServiceCB.aidl

# MODIFIED-BEGIN by bo.yu, 2016-10-27,BUG-3234021
LOCAL_SRC_FILES += src/com/arkamys/audio/ArkamysAudioAPI.aidl
LOCAL_AIDL_INCLUDES := src/com/arkamys/audio/ArkamysAudioAPI.aidl
# MODIFIED-END by bo.yu,BUG-3234021


LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res \
    frameworks/support/v7/preference/res \
    frameworks/support/v14/preference/res \
    frameworks/support/v7/appcompat/res \
    frameworks/support/v7/recyclerview/res \
    frameworks/support/design/res

LOCAL_PACKAGE_NAME := Settings
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

# MODIFIED-BEGIN by BinhongWang, 2016-10-12,TASK-3055671
LOCAL_REQUIRED_MODULES += \
    com.qti.snapdragon.sdk.display
LOCAL_JAVA_LIBRARIES += \
    com.qti.snapdragon.sdk.display
# MODIFIED-END   by BinhongWang, 2016-10-12,TASK-3055671

LOCAL_AAPT_FLAGS := --auto-add-overlay \
    --extra-packages android.support.v7.preference:android.support.v14.preference:android.support.v17.preference:android.support.v7.appcompat:android.support.v7.recyclerview \
    --extra-packages android.support.design
ifneq ($(INCREMENTAL_BUILDS),)
    LOCAL_PROGUARD_ENABLED := disabled
    LOCAL_JACK_ENABLED := incremental
    LOCAL_DX_FLAGS := --multi-dex
    LOCAL_JACK_FLAGS := --multi-dex native
endif

include frameworks/opt/setupwizard/navigationbar/common.mk
include frameworks/opt/setupwizard/library/common-full-support.mk
include frameworks/base/packages/SettingsLib/common.mk

include $(BUILD_PACKAGE)
include $(BUILD_PLF)

# Use the following include to make our test apk.
ifeq (,$(ONE_SHOT_MAKEFILE))
include $(call all-makefiles-under,$(LOCAL_PATH))
endif

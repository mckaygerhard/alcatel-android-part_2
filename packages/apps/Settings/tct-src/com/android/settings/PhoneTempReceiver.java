/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  wen.zhuang                                                      */
/*  Email  :  wen.zhuang@tcl.com                                              */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : Power save                                                     */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/04/2016|      xian.jiang      |     Task-2670032     |[GMS][Hidenmenu]  */
/*           |                      |                      |input "###666#"   */
/*           |                      |                      |do disabling GMS  */
/* ----------|----------------------|----------------------|----------------- */
/* 09/12/2016|      xian.jiang      |     Task-2855360     |Keep Gapp "package*/
/*           |                      |                      |installer"&"setupw*/
/*           |                      |                      |izard" when code  */
/*           |                      |                      |to disable Gapp   */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import java.util.List;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.util.Log;
import android.provider.Settings;

public class PhoneTempReceiver extends BroadcastReceiver {
    private static final String TAG = "PhoneTempReceiver";
    private static final String ACTION_GOOGLE_APP_CHANGED = "action.google.app.changed";
    private static final String PROPERTY_GOOGLEAPP_ENABLED = "persist.sys.google.enabled";
    private PackageManager mPm;
    private ActivityManager mAm;
    private static final int MSG_FORCE_STOP_GOOGLE_APPS = 0;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
            case MSG_FORCE_STOP_GOOGLE_APPS:
                List<PackageInfo> packageInfos = mPm.getInstalledPackages(0);
                Log.i(TAG, "Handler message, start");
                for (int i = 0; i < packageInfos.size(); i++) {
                    ApplicationInfo info = packageInfos.get(i).applicationInfo;
                    //[BUG]-Mod-BEGIN by TCTNB.Xian.Jiang,2016/09/12, defect-2855360, need to disable chrome and vending,to keep two google apk
                    if (((info.packageName).startsWith("com.google"))/* && ((info.flags & ApplicationInfo.FLAG_SYSTEM) != 0))*/ ||
                            "com.android.chrome".equalsIgnoreCase(info.packageName) ||  //disable Chrome apk
                            "com.android.vending".equalsIgnoreCase(info.packageName)) { //disable Google Play Store
                        // need to keep two google apk
                        if (!"com.google.android.setupwizard".equalsIgnoreCase(info.packageName) &&
                            !"com.google.android.packageinstaller".equalsIgnoreCase(info.packageName))
                             mAm.forceStopPackage(info.packageName);
                        //[BUG]-Mod-END by TCTNB.Xian.Jiang
                    }
                }
                Log.i(TAG, "Handler message, end");
                break;
            }
        }
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i(TAG, "Intent action = " + action);

        mPm = context.getPackageManager();
        mAm = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {

            //0=disable, 1=enable, 2=without google apk, 3=not initial
            String gEnabled = SystemProperties.get(PROPERTY_GOOGLEAPP_ENABLED, "3");
            if (gEnabled.equals("3")) {
                //Just do once
                List<PackageInfo> packageInfos = mPm.getInstalledPackages(0);
                SystemProperties.set(PROPERTY_GOOGLEAPP_ENABLED, "2");
                for (int i = 0; i < packageInfos.size(); i++) {
                    ApplicationInfo info = packageInfos.get(i).applicationInfo;
                    if (((info.packageName).startsWith("com.google")) /*&& (((info.flags & ApplicationInfo.FLAG_SYSTEM) != 0))*/) {
                        SystemProperties.set(PROPERTY_GOOGLEAPP_ENABLED, "1");
                        break;
                    }
                }
            }
            return;
        } if (ACTION_GOOGLE_APP_CHANGED.equals(action)) {

            //0=disable, 1=enable, 2=without google apk, 3=not initial
            String extra = intent.getStringExtra("googleConfig");
            Log.i(TAG, "start process google apps!!! gEnabled = " + extra);

            List<PackageInfo> packageInfos = mPm.getInstalledPackages(0);
            Log.i(TAG, "packageInfos.size() = " + packageInfos.size());
            if (extra.equals("0")) {  //From enable to disable
                for (int i = 0; i < packageInfos.size(); i++) {
                    ApplicationInfo info = packageInfos.get(i).applicationInfo;
                    //[BUG]-Mod-BEGIN by TCTNB.Xian.Jiang,2016/09/12, defect-2855360, need to disable chrome and vending,to keep two google apk
                    if (((info.packageName).startsWith("com.google"))/* && (((info.flags & ApplicationInfo.FLAG_SYSTEM) != 0))*/
                        || "com.android.chrome".equalsIgnoreCase(info.packageName)  //disable Chrome apk
                        ||"com.android.vending".equalsIgnoreCase(info.packageName)) {
                        if (info.enabled
                              &&!"com.google.android.setupwizard".equalsIgnoreCase(info.packageName)
                              &&!"com.google.android.packageinstaller".equalsIgnoreCase(info.packageName)) {
                    //[BUG]-Mod-END by TCTNB.Xian.Jiang
                            new DisableChanger(this, info,
                                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED_USER)
                            .execute((Object)null);
                            Log.i(TAG, "disable packageName = " + info.packageName + " , info.enabled = " + info.enabled);
                        }
                    }
                }
                Log.i(TAG, "send delayed msg to stop google apps!!!");
                mHandler.removeMessages(MSG_FORCE_STOP_GOOGLE_APPS);
                mHandler.sendEmptyMessageDelayed(MSG_FORCE_STOP_GOOGLE_APPS, 10 * 1000);
                SystemProperties.set(PROPERTY_GOOGLEAPP_ENABLED, "0");
                Settings.Global.putInt(context.getContentResolver(),"verifier_timeout", 0);//adb by TCTNB.cheng.liu merge Idol4 family patch to disable google app verrify 
            } else {  //From disable to enable
                for (int i = 0; i < packageInfos.size(); i++) {
                    ApplicationInfo info = packageInfos.get(i).applicationInfo;
                    //[BUG]-Mod-BEGIN by TCTNB.Xian.Jiang,2016/09/12, defect-2855360, need to disable chrome and vending,to keep two google apk
                    if (((info.packageName).startsWith("com.google")) /* && (((info.flags & ApplicationInfo.FLAG_SYSTEM) != 0))*/
                      ||"com.android.chrome".equalsIgnoreCase(info.packageName)  //disable Chrome apk
                      ||"com.android.vending".equalsIgnoreCase(info.packageName)) { //disable vending apk
                    //[BUG]-Mod-END by TCTNB.Xian.Jiang
                        if (!(info.enabled)) {
                            new DisableChanger(this, info,
                                    PackageManager.COMPONENT_ENABLED_STATE_DEFAULT)
                            .execute((Object)null);
                        }
                        Log.i(TAG, "enable packageName = " + info.packageName + " , info.enabled = " + info.enabled);
                    }
                }
                SystemProperties.set(PROPERTY_GOOGLEAPP_ENABLED, "1");
                Settings.Global.putInt(context.getContentResolver(), "verifier_timeout", 10*1000);//adb by TCTNB.cheng.liu merge Idol4 family patch to disable google app verrify 
            }
            return;
        }
    }

    static class DisableChanger extends AsyncTask<Object, Object, Object> {
        final PackageManager mPm;
        final ApplicationInfo mInfo;
        final int mState;

        DisableChanger(PhoneTempReceiver receiver, ApplicationInfo info, int state) {
            mPm = receiver.mPm;
            mInfo = info;
            mState = state;
        }

        @Override
        protected Object doInBackground(Object... params) {
            mPm.setApplicationEnabledSetting(mInfo.packageName, mState, 0);
            return null;
        }
    }
}

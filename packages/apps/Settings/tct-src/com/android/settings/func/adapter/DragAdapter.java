package com.android.settings.func.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.settings.R;

import java.util.List;

public class DragAdapter extends BaseAdapter {
    private final static String TAG = "DragAdapter";
    private Context context;
    private Resources res;
    public List<String> listData;
    private FuncEditItemListener funcEditItemListener;
    private final PackageManager pm;

    public DragAdapter(Context context, List<String> listData) {
        this.context = context;
        this.listData = listData;
        res = context.getResources();
        pm = context.getPackageManager();
    }

    @Override
    public int getCount() {
        return listData == null ? 0 : listData.size();
    }

    @Override
    public String getItem(int position) {
        if (listData != null && listData.size() != 0
                && position < listData.size()) {
            return listData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderNormal holderNormal = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.drag_listview_item, null);
            holderNormal = new ViewHolderNormal();
            holderNormal.funcIcon = (ImageView) convertView
                    .findViewById(R.id.funcicon);
            holderNormal.funcText = (TextView) convertView
                    .findViewById(R.id.functext);
            holderNormal.fun_editIcon = (ImageView) convertView
                    .findViewById(R.id.fun_edit);
            holderNormal.func_removeIcon = (ImageView) convertView
                    .findViewById(R.id.click_remove);
            convertView.setTag(holderNormal);
        } else {
            holderNormal = (ViewHolderNormal) convertView.getTag();
        }

        String item = listData.get(position);
        int id = res.getIdentifier("id/" + item, null, "android");

        int type = -1;
        if (id != 0 ) { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
            int descId = res.getIdentifier("string/" + item, null, context.getPackageName());
            holderNormal.funcText.setText(descId != 0 ? res.getString(descId) : item);
            int imgId = res.getIdentifier("drawable/" + item, null, context.getPackageName());
            if (imgId != 0) {
                holderNormal.funcIcon.setImageResource(imgId);
            }
            /* MODIFIED-BEGIN by song.huan, 2016-10-18,BUG-3088432*/
            if (id == com.android.internal.R.id.func_dial) {
                    /* MODIFIED-END by song.huan,BUG-3088432*/
                holderNormal.fun_editIcon.setVisibility(View.VISIBLE);
                type = id;
            } else {
                holderNormal.fun_editIcon.setVisibility(View.GONE);
            }
        } else {
            CharSequence tip;
            try {
                // MOD-BEGIN by zhiqianghu, 11/15/2016, for 3402581
                /*
                ApplicationInfo info = pm.getApplicationInfo(item, PackageManager.GET_META_DATA);
                tip = pm.getApplicationLabel(info);
                Drawable drawable = pm.getApplicationIcon(item);
                holderNormal.funcIcon.setImageDrawable(drawable);
                */
                String[] componentName = item.split("/");
                tip = item;
                if (componentName != null && componentName.length == 2) {
                    ComponentName com = new ComponentName(componentName[0], componentName[1]);
                    ActivityInfo activityInfo = pm.getActivityInfo(com, PackageManager.GET_META_DATA);

                    tip = activityInfo.loadLabel(pm);
                    Drawable drawable = activityInfo.loadIcon(pm);
                    holderNormal.funcIcon.setImageDrawable(drawable);
                }
                // MOD-END by zhiqianghu, 11/15/2016, for 3402581
            } catch (Exception e) {
                tip = item;
            }
            holderNormal.funcText.setText(tip);
            holderNormal.fun_editIcon.setVisibility(View.GONE);
        }
        final int goType = type;
        holderNormal.fun_editIcon.setOnClickListener(view -> {
            if (funcEditItemListener != null && goType != -1) {
                funcEditItemListener.editFuncItem(goType);
            }
        });
        return convertView;
    }

    public void setFuncEditItemListener(
            FuncEditItemListener funcEditItemListener) {
        this.funcEditItemListener = funcEditItemListener;
    }

    static class ViewHolderNormal {
        ImageView funcIcon;
        TextView funcText;
        ImageView fun_editIcon;
        ImageView func_removeIcon;
    }

    public interface FuncEditItemListener {
        void editFuncItem(int type);
    }
}

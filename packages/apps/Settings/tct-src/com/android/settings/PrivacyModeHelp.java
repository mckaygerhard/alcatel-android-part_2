/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/09/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import android.app.ActionBar;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class PrivacyModeHelp extends SettingsActivity {
    private PrivacyModeObserver mPrivacyModeObserver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mPrivacyModeObserver = new PrivacyModeObserver();
        this.getContentResolver().registerContentObserver(Settings.System.getUriFor(
                Settings.System.PRIVACY_MODE_ON), true, mPrivacyModeObserver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            this.getContentResolver().unregisterContentObserver(mPrivacyModeObserver);
        } catch(Exception e) {
        }
    }

    private class PrivacyModeObserver extends ContentObserver {
        public PrivacyModeObserver() {
            super(new Handler());
        }

        @Override
        public void onChange(boolean selfChange) {
            if (Settings.System.getInt(getContentResolver(), Settings.System.PRIVACY_MODE_ON, 0) == 1) {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

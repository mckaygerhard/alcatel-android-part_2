/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 13/10/2016|     zhengyu.hu       |   Task-2894950       |[LMN]Low memory   */
/*           |                      |                      |notification      */
/*           |                      |                      |policy            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
//SmartContainer modify begin
import android.os.UserHandle;
//SmartContainer modify end

//import android.util.TctLog;

public class LowMemoryReceiver extends BroadcastReceiver {
    public final static String TAG = "LowMemoryReceiver";
    public final static String ACTION_DISK_SPACE_LOWER_OFF = "storageforcemode.enabled.dataconnection";
    public final static String ACTION_DISK_SPACE_LOWER_ON = "storageforcemode.disable.dataconnection";
    public final static String WIFI_STATUE = "wifi_status";
    public final static String MOBILE_DATE_STATUE = "mobile_data_status";

    @Override
    public void onReceive(Context context, Intent intent) {
        //SmartContainer modify begin
        if(UserHandle.myUserId() != 0)
        {
            return;
        }
        //SmartContainer modify end
        String action = intent.getAction();
        // TctLog.i(TAG,"action="+action);
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        ConnectivityManager mConnService = ConnectivityManager.from(context);
        TelephonyManager telephonyManager = TelephonyManager.from(context);
        if (action.equals(ACTION_DISK_SPACE_LOWER_ON)) {
            SharedPreferences preferences = context.getSharedPreferences(
                    context.getPackageName(), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("WIFI_STATUE", wifiManager.isWifiEnabled());
            editor.putBoolean("MOBILE_DATE_STATUE",
                    mConnService.getMobileDataEnabled());
            editor.commit();
            wifiManager.setWifiEnabled(false);
            telephonyManager.setDataEnabled(false);
        } else if (action.equals(ACTION_DISK_SPACE_LOWER_OFF)) {
            SharedPreferences preferences = context.getSharedPreferences(
                    context.getPackageName(), Context.MODE_PRIVATE);
            boolean isMobileDataEnable = preferences.getBoolean(
                    "MOBILE_DATE_STATUE", false);
            boolean isWifiEnable = preferences.getBoolean("WIFI_STATUE", false);
            // TctLog.i(TAG,"isMobileDataEnable="+isMobileDataEnable
            // +" isWifiEnable="+isWifiEnable);
            wifiManager.setWifiEnabled(isWifiEnable);
            telephonyManager.setDataEnabled(isMobileDataEnable);
        }
    }
}


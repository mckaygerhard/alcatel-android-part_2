package com.android.settings;

/**
 *
 * [FEATURE]add by haifeng.zhai for defect 1618940 2016-2-25
 *
 *
 * **/
import android.content.Context;
import android.media.AudioManager;
import android.os.Vibrator;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.provider.Settings.SettingNotFoundException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.android.settings.R;
import android.provider.Settings.System;
import android.os.SystemVibrator;

public class HapticFeedbackPreference extends Preference implements
        SeekBar.OnSeekBarChangeListener {

    private static final String TAG = "HapticFeedbackPreference";
    private SeekBar mSeekBar;
    private TextView mTitleTextView;
    private final int mVibrateMaxmum = 250; // MODIFIED by haifeng.zhai, 2016-08-16,BUG-2743679
    private final int mVibrateMinmum = 1;
    private int mVibrateLastStrength = 0;
    private Vibrator mVibrator;
    private boolean mEnableHapticFeedback;
    private Context mContext;

    public HapticFeedbackPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setLayoutResource(R.layout.preference_hapticfeedback_slider);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);

        mSeekBar = (SeekBar) view.findViewById(R.id.haptic_feedback_seekbar);
        mTitleTextView = (TextView) view.findViewById(R.id.haptic_feedback_title);

        mSeekBar.setMax(mVibrateMaxmum - mVibrateMinmum);
        mSeekBar.setOnSeekBarChangeListener(this);
        mVibrator = new SystemVibrator();

        updateStatus();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
            boolean fromTouch) {
        boolean validate = (progress + mVibrateMinmum) != mVibrateLastStrength;
        if (validate) {
            mVibrator.vibrate(progress + mVibrateMinmum); // MODIFIED by haifeng.zhai, 2016-08-16,BUG-2743679
        } else {
            mVibrator.vibrate(0);
        }
        setVibrateStrength(progress + mVibrateMinmum);
        updateView();
    }

    private boolean setVibrateStrength(int vibrateStrengthValue) {
        boolean vibrateStrength = false;
        if (vibrateStrengthValue < mVibrateMinmum - 1
                || mVibrateMinmum > mVibrateMaxmum) {
            return false;
        }
        mVibrateLastStrength = vibrateStrengthValue;
        /* MODIFIED-BEGIN by haifeng.zhai, 2016-08-16,BUG-2743679*/
        vibrateStrength = System.putInt(getContext().getContentResolver(),
                    System.VIBRATION_FEEDBACK,vibrateStrengthValue);
                    /* MODIFIED-END by haifeng.zhai,BUG-2743679*/
        return vibrateStrength;
    }

    public boolean getEnableHapticFeedback() {
        int vibrateontouch = 0;
        try {
            vibrateontouch = System.getInt(getContext().getContentResolver(),
                    System.HAPTIC_FEEDBACK_ENABLED);
        } catch (SettingNotFoundException e) {
            Log.e(TAG, "SettingNotFoundException");
        }
        if (vibrateontouch == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void updateStatus() {
        int vibrateStrength = getSystemVibrateTime();
        mVibrateLastStrength = vibrateStrength;
        mEnableHapticFeedback = getEnableHapticFeedback();
        if (mEnableHapticFeedback) {
            mSeekBar.setEnabled(true);
            if (vibrateStrength != -1 && vibrateStrength <= mVibrateMaxmum
                    && vibrateStrength >= mVibrateMinmum) {
                setVibrateStrength(vibrateStrength);
                mSeekBar.setProgress(vibrateStrength - mVibrateMinmum);
            } else if (vibrateStrength == 0) {
                setVibrateStrength(1);
                mSeekBar.setProgress(0);
            } else {
                mSeekBar.setProgress(vibrateStrength - mVibrateMinmum);
                mSeekBar.setEnabled(false);
            }
            mTitleTextView.setText( mContext.getString(R.string.haptic_feedback_vibration_duration, vibrateStrength));
        } else {
            mSeekBar.setEnabled(false);
            mTitleTextView.setText(mContext.getString(R.string.haptic_feedback_vibration_duration, 0));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0) {

    }

    public int getSystemVibrateTime(){
        int vibrateTime = 0;
        try {
            vibrateTime = System.getInt(getContext().getContentResolver(),
                    System.VIBRATION_FEEDBACK);
        } catch (SettingNotFoundException e) {
            Log.i(TAG, "SettingNotFoundException");
        }
        return vibrateTime;
    }

    public void updateView(){
        mTitleTextView.setText(mContext.getString(R.string.haptic_feedback_vibration_duration,getSystemVibrateTime()));
    }
}

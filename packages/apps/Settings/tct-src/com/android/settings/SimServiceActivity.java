/**************************************************************************************************/
/*                                                                                 Date : 05/2014 */
/*                            PRESENTATION                                                        */
/*              Copyright (c) 2014 JRD Communications, Inc.                                       */
/**************************************************************************************************/
/*                                                                                                */
/*    This material is company confidential, cannot be reproduced in any                          */
/*    form without the written permission of JRD Communications, Inc.                             */
/*                                                                                                */
/*================================================================================================*/
/*   Author :  huiyuan.wang                                                                       */
/*   Role :                                                                                       */
/*   Reference documents :                                                                        */
/*================================================================================================*/
/* Comments :                                                                                     */
/*   file   : packages/apps/Setting/src/com/android/settings/SimServiceActivity                   */
/*   Labels :                                                                                     */
/*================================================================================================*/
/* Modifications   (month/day/year)                                                               */
/*================================================================================================*/
/* date    | author       |FeatureID                   |modification                              */
/*=========|==============|============================|==========================================*/
/*                                                                                                */
/*================================================================================================*/
/* Problems Report(PR/CR)                                                                         */
/*================================================================================================*/
/* date    | author       | FR #                       |                                          */
/*=========|==============|============================|==========================================*/
/* 30/06/14| huiyuan.wang | RR705643                   | add sim service selection                */
/*=========|==============|============================|==========================================*/
/* Task    528542                                                                                 */
/*================================================================================================*/
/* date    | author       | Task #                     |                                          */
/*=========|==============|============================|==========================================*/
/* 29/10/15| zhi-zhang    | 528542                     |   porting Task 528542                    */
/*===========|==============|============================|========================================*/
/* 01/19/2016| Ma Xiaoli    | Defect:1431056,1453250     |   SIM Provider not work                */
/*=========|==============|============================|==========================================*/

package com.android.settings;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemProperties;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.provider.Settings;
//[BUGFIX]-Add-BEGIN by TCTNB.xian.jiang,16/09/2014,780953
//update preferred apn to db
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.provider.Telephony;

//[BUGFIX]-Add-END by TCTNB.xian.jiang
public class SimServiceActivity extends ListActivity {
    private static final String TAG = "SimServiceActivity";
    private static final String MULTI_SIM_ID_KEY = "simid";
    private String[] mOperators = null;
    private int mSelectedOperator = -1;
    private IntentFilter mIntentFilter;
    // MODIFIED by fei.liu, 2016-05-28,defect-2202129
    private static final String[] MCCMNC23430 = {
            "EE", "Virgin Mobile", "BT", "BT OnePhone", "Asda Mobile"
    };
//[BUGFIX]-Mod-BEGIN by TSNJ.Tian haifeng,07/21/2015,PR-1042550, Remove APN "LIFE Mobile" from APN settings and "SIM provider" list
    private static final String[] MCCMNC23433 = {
            "EE"
    };
//[BUGFIX]-Mod-BEGIN by TSNJ.Tian haifeng,07/21/2015,PR-1042550, Remove APN "LIFE Mobile" from APN settings and "SIM provider" list
    // [BUGFIX]-Add- by jun-wang@jrdcom.com 05/27/2015,BUG1005404
    /* MODIFIED by fliu2,2016-05-28,BUG-2201163*/
    private static final String[] MCCMNC23410 = {
            "O2 Pay Monthly", "O2 Pay & Go", "TESCO Mobile","giffgaff","TalkTalk Mobile"
    };
    // [BUGFIX]-Add- by Tianhaifeng 05/27/2015,BUG1005404
    // MOD-BEGIN by jin-zhang, 10/22/2015, for 1102711
    //MODIFIED-BEGIN by fei.liu, 2016-05-28,defect-2202082
    private static final String[] MCCMNC23415 = {
            "Vodafone Contract", "Vodafone PAYG", "Talkmobile Contract", "Talkmobile PAYG",
            "Sainsbury's Contract", "Sainsbury's PAYG", "Lebara", "TalkTalk Mobile"
    };
    //MODIFIED-END by fei.liu,defect-2202082
    // MOD-END by jin-zhang, 10/22/2015, for 1102711
    // ADD-BEGIN by jin-zhang, 10/23/2015, for 1102787
    //Add-begin by qili.zhang for 1527070 fix H3G UK to 3
    //MODIFIED by fei.liu, 2016-05-28,defect-2202210
    private static final String[] MCCMNC23420 = {
            "iD", "3"
    };
    //Add-end by qili.zhang for 1527070
    // ADD-END by jin-zhang, 10/23/2015, for 1102787
    private static final String SIM_SERVICE = "sim_service";
    private static final String GPRS_CONNECTION_SIM_SETTING = "gprs_connection_sim_setting";
    private static final long DEFAULT_SIM_NOT_SET = -5;
    private static final long GPRS_CONNECTION_SIM_SETTING_NEVER = 0;
    private static final String ACTION_DATA_DEFAULT_SIM_CHANGED = "android.intent.action.DATA_DEFAULT_SIM";

    // [BUGFIX]-Add-BEGIN by TCTNB.xian.jiang,16/09/2014,780953 83
    // update preferred apn to db 84
    private static final Uri PREFERRED_APN_URI = Uri
            .parse("content://telephony/carriers/preferapn");
    private static final Uri CARRIER_URI = Telephony.Carriers.CONTENT_URI;
    private String[][] preferredApnTable = null;

    // MOD-BEGIN by jin-zhang, 10/19/2015, for 1100351
    //MODIFIED-BEGIN by fei.liu, 2016-05-28,defect-2202129
    private String PREFERRED_APN_TABLE_23430[][] = {
            {
                    "EE", "Internet", "everywhere"
            },
            {
                    "BT", "BT Internet", "btmobile.bt.com"
            },
            {
                    "BT OnePhone", "BT OnePhone Internet", "internet.btonephone.com"
            },
            {
                    "Virgin Mobile", "Virgin Media Mobile Internet", "goto.virginmobile.uk"
            },
            {
                    "Asda Mobile", "Asda Internet", "everywhere"
            },
    };
    //MODIFIED-END by fei.liu,defect-2202129
    // MOD-END by jin-zhang, 10/19/2015, for 1100351
//[BUGFIX]-Mod-BEGIN by TSNJ.Tian haifeng,07/21/2015,PR-1042550, Remove APN "LIFE Mobile" from APN settings and "SIM provider" list
    private String PREFERRED_APN_TABLE_23433[][] = {
            {
                    "EE", "Internet", "everywhere"
            },
    };
//[BUGFIX]-Mod-BEGIN by TSNJ.Tian haifeng,07/21/2015,PR-1042550, Remove APN "LIFE Mobile" from APN settings and "SIM provider" list
    // [BUGFIX]-Add- by jun-wang@jrdcom.com 05/27/2015,BUG1005404
    /* MODIFIED-BEGIN by fliu2,2016-05-28,BUG-2201163*/
    private String PREFERRED_APN_TABLE_23410[][] = {
            {
                    "O2 Pay Monthly", "O2 Mobile Web", "mobile.o2.co.uk"
            },
            {
                    "O2 Pay & Go", "O2 Pay & Go", "payandgo.o2.co.uk"
            },
            {
                    /* MODIFIED-BEGIN by fliu2, 2016-05-28,BUG-2201163*/
                    "TESCO Mobile", "Tesco WAP GPRS", "prepay.tesco-mobile.com"
            },
            {
                    "giffgaff", "giffgaff", "giffgaff.com"
            },
            {
                    "TalkTalk Mobile", "TalkTalk", "mobile.talktalk.co.uk"
            },
    };
    /* MODIFIED-END by fliu2,BUG-2201163*/
    /* MODIFIED-END by fliu2,BUG-2201163*/
    // [BUGFIX]-Add- by jun-wang@jrdcom.com 05/27/2015,BUG1005404
    //[BUGFIX]-Add-BEGIN by jinzhu.liu,06/23/2015,PR1019882
    // MOD-BEGIN by jin-zhang, 10/22/2015, for 1102711
    //MODIFIED-BEGIN by fei.liu, 2016-05-28,defect-2202082
    private String PREFERRED_APN_TABLE_23415[][] = {
            {
                    "Vodafone Contract", "Contract WAP", "wap.vodafone.co.uk"
            },
            {
                    "Vodafone PAYG", "PAYG WAP", "pp.vodafone.co.uk"
            },
            {
                    "Talkmobile Contract", "Talkmobile Internet", "talkmobile.co.uk"
            },
            {
                    "Talkmobile PAYG", "Talkmobile PAYG Internet", "payg.talkmobile.co.uk"
            },
            {
                    "TalkTalk Mobile", "TalkTalk", "mobile.talktalk.co.uk" // MODIFIED by fliu2, 2016-05-28,BUG-2202082
            },
            {
                    "BT Mobile", "BT Mobile Internet", "btmobile.bt.com"
            },
            {
                    "Lebara", "Lebara", "uk.lebara.mobi"
            },
            {
                    /* MODIFIED-BEGIN by fliu2, 2016-05-28,BUG-2202082*/
                    "Sainsbury's PAYG", "Sainsbury's PAYG", "payg.mobilebysainsburys.co.uk"
            },
            {
                    "Sainsbury's Contract", "Sainsburys Contract", "mobilebysainsburys.co.uk"
            },
    };
    //MODIFIED-END by fei.liu,defect-2202082
    /* MODIFIED-END by fliu2,BUG-2202082*/
    // MOD-END by jin-zhang, 10/22/2015, for 1102711
    //[BUGFIX]-Add-END by jinzhu.liu,06/23/2015,PR1019882

    // ADD-BEGIN by jin-zhang, 10/23/2015, for 1102787
    //Add-begin by qili.zhang for defect 1527070 porting 1102787
    // fix Carphone Warehouse to H3G UK to 3
    private String PREFERRED_APN_TABLE_23420[][] = {
            {
                    "3", "3", "three.co.uk"
            },
            {
                    "iD", "iD", "iD"
            },
    };
    //Add-end by qili.zhang for defect 1527070 porting 1102787
    // ADD-END by jin-zhang, 10/23/2015, for 1102787

    // [BUGFIX]-Add-END by TCTNB.xian.jiang

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String mccmnc = intent.getStringExtra("mccmnc");
        Log.d(TAG, "onCreate, mccmnc=" + mccmnc);
        if ("23430".equals(mccmnc)) {
            mOperators = MCCMNC23430;
            preferredApnTable = PREFERRED_APN_TABLE_23430; // [BUGFIX]-Add by
                                                           // TCTNB.xian.jiang,16/09/2014,780953
        } else if ("23433".equals(mccmnc)) {
            mOperators = MCCMNC23433;
            preferredApnTable = PREFERRED_APN_TABLE_23433; // [BUGFIX]-Add by
                                                           // TCTNB.xian.jiang,16/09/2014,780953
           //[BUGFIX]-ADD-Begin by qili.zhang 02/02/2016 defect 1539903
            Settings.Global.putString(getContentResolver(),SIM_SERVICE, "EE");
           //[BUGFIX]-ADD-End by qili.zhang 02/02/2016 defect 1539903
        } else if ("23410".equals(mccmnc)) {
            mOperators = MCCMNC23410;
            preferredApnTable = PREFERRED_APN_TABLE_23410; // [BUGFIX]-Add by
                                                           // TCTNB.xian.jiang,16/09/2014,780953
        } else if ("23415".equals(mccmnc)) {
            mOperators = MCCMNC23415;
            preferredApnTable = PREFERRED_APN_TABLE_23415; // [BUGFIX]-Add by
                                                           // TCTNB.xian.jiang,16/09/2014,780953
        // ADD-BEGIN by jin-zhang, 10/23/2015, for 1102787
        } else if ("23420".equals(mccmnc)) {
            mOperators = MCCMNC23420;
            preferredApnTable = PREFERRED_APN_TABLE_23420;
        // ADD-END by jin-zhang, 10/23/2015, for 1102787
        }
        if (mOperators == null) {
            Log.d(TAG, "onCreate, mccmnc=null, finish");
            finish();
        }
        // ADD-BEGIN by jin-zhang, 10/22/2015, for 1102633
        if (preferredApnTable == null || preferredApnTable.length <= 1) {
            Log.d(TAG, "onCreate, preferredApnTable no need to select, finish");
            finish();
        }
        // ADD-END by jin-zhang, 10/22/2015, for 1102633
        setFinishOnTouchOutside(false);
        addFooterView();
        setListAdapter(new ArrayAdapter<String>(this,
                R.layout.sim_service, mOperators));
        getListView().setBackgroundColor(android.graphics.Color.WHITE);
        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        String currentOperator = Settings.Global.getString( // MODIFIED by fliu2, 2016-07-18,BUG-2433933
                getContentResolver(), SIM_SERVICE);
        Log.d(TAG, "onCreate, currentOperator=" + currentOperator);
        if (currentOperator != null && mOperators.length > 0) {
            for (int index = 0; index < mOperators.length; index++) {
                if (currentOperator.equalsIgnoreCase(mOperators[index])) {
                    mSelectedOperator = index;
                    getListView().setItemChecked(index, true);
                    break;
                }
            }
        }
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onPause();
        Log.d(TAG, "onDestroy, unregisterReceiver");
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        mSelectedOperator = position;
        Log.d(TAG, "onListItemClick, mSelectedOperator=" + mSelectedOperator);
    }

    private void addFooterView() {
        View footer = LayoutInflater.from(this).inflate(R.layout.sim_service_footer, null);
        Button okButon = (Button) footer.findViewById(R.id.ok);
        okButon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mSelectedOperator > -1) {
                    setSimService();
                }
            }
        });
        Button cancelButon = (Button) footer.findViewById(R.id.cancel);
        cancelButon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                unsetSimService();
            }
        });
        getListView().addFooterView(footer);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Log.d(TAG, "KeyEvent.KEYCODE_BACK");
                return true;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void setSimService() {
        if (mSelectedOperator > -1) {
            Log.d(TAG, "setSimService, mSelectedOperator=" + mSelectedOperator);
            Settings.Global.putString(getContentResolver(),
                    SIM_SERVICE, mOperators[mSelectedOperator]);//[BUGFIX] Mod by TCT.NB.MaXiaoli @ 01/19/2016,Defect:1431056,1453250
            // [BUGFIX]-Add-BEGIN by TCTNJ.Jinxia.Dai,08/11/2014,754425
            Log.d(TAG, "setSimService, mOperators[mSelectedOperator]="
                    + mOperators[mSelectedOperator]);
            if ("EE".equals(mOperators[mSelectedOperator])) {
                Settings.Global.putInt(getContentResolver(), Settings.Global.DATA_ROAMING, 1);
            } else {
                Settings.Global.putInt(getContentResolver(), Settings.Global.DATA_ROAMING, 0);
            }
            // [BUGFIX]-Add-END by TCTNJ.Jinxia.Dai 754425
            long currentDataConnectionSimId = Settings.System.getLong(getContentResolver(),
                    GPRS_CONNECTION_SIM_SETTING, DEFAULT_SIM_NOT_SET);
            if (DEFAULT_SIM_NOT_SET != currentDataConnectionSimId
                    && GPRS_CONNECTION_SIM_SETTING_NEVER != currentDataConnectionSimId) {
                Intent dataIntent = new Intent();
                dataIntent.putExtra(MULTI_SIM_ID_KEY, GPRS_CONNECTION_SIM_SETTING_NEVER);
                dataIntent.setAction(ACTION_DATA_DEFAULT_SIM_CHANGED);
                sendBroadcast(dataIntent);
                dataIntent.removeExtra(MULTI_SIM_ID_KEY);
                dataIntent.putExtra(MULTI_SIM_ID_KEY, currentDataConnectionSimId);
                sendBroadcast(dataIntent);
            }
            // [BUGFIX]-Mod-BEGIN by TCTNB.xian.jiang,16/09/2014,780953
            // update preferred apn to db
            if (this.getCallingActivity() != null) {
                Log.v(TAG, "this.getCallingActivity()=" + this.getCallingActivity());
                Intent apnIntent = new Intent(this, ApnSettings.class);
                setResult(RESULT_OK, apnIntent);
            } else {
                Log.v(TAG,
                        "this.getCallingActivity() is null.Need to reset preferred_apn silencely");
                updatePreferredApn();
            }
            // startActivity(apnIntent); //[BUGFIX]-Add by
            // TCTNB.xian.jiang,04/09/2014,780953
            // [BUGFIX]-Mod-END by TCTNB.xian.jiang
            finish();
        }
    }

    private void unsetSimService() {
        Settings.Global.putString(getContentResolver(), // MODIFIED by fliu2, 2016-07-18,BUG-2433933
                SIM_SERVICE, "");
        Log.d(TAG, "unsetSimService, disable data");
        Intent dataIntent = new Intent();
        dataIntent.putExtra(MULTI_SIM_ID_KEY, GPRS_CONNECTION_SIM_SETTING_NEVER);
        dataIntent.setAction(ACTION_DATA_DEFAULT_SIM_CHANGED);
        sendBroadcast(dataIntent);
        Intent apnIntent = new Intent(this, ApnSettings.class);
        setResult(RESULT_CANCELED, apnIntent);
        finish();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        private final String SYSTEM_DIALOG_REASON_KEY = "reason";
        private final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
                Log.d(TAG, "onReceive, ACTION_CLOSE_SYSTEM_DIALOGS, reason=" + reason);
                if (reason != null && reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)) {
                    if (mSelectedOperator > -1) {
                        setSimService();
                    } else {
                        unsetSimService();
                    }
                }
            }
        }
    };

    // [BUGFIX]-Add-BEGIN by TCTNB.xian.jiang,16/09/2014,780953
    // update preferred apn to db
    private void updatePreferredApn() {
        String preferredApnName = preferredApnTable[mSelectedOperator][2];
        Log.v(TAG, "preferredApnName=" + preferredApnName);
        String apnKey = null;
        // select * from carriers where apn =preferredApnName
        String where = "apn=\"" + preferredApnName + "\"";
        Cursor cursor = getContentResolver().query(CARRIER_URI,
                new String[] {
                        "_id", "apn"
                }, where, null, null);
        if (null != cursor) {
            try {
                if (cursor.moveToFirst()) {
                    apnKey = cursor.getString(0);
                } else
                    Log.d(TAG, "Not find prefer apn's id.");
            } finally {
                cursor.close();
            }
        } else
            Log.v(TAG, "cursor is null!");
        if (apnKey != null) {
            Log.v(TAG, "apnKey=" + apnKey);
            ContentValues values = new ContentValues();
            values.put("apn_id", apnKey);
            getContentResolver().update(PREFERRED_APN_URI, values, null, null);
        } else
            Log.v(TAG, "apnKey is null!");
    }
    // [BUGFIX]-Add-END by TCTNB.xian.jiang
}

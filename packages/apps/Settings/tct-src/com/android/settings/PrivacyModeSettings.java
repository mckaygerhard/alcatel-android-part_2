/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/09/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import java.util.List;

import tct.util.privacymode.TctPrivacyModeHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Color;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.fingerprint.FingerprintEnrollEnrolling;
import com.android.settings.widget.SwitchBar;
import com.android.settings.widget.ToggleSwitch;
import com.android.settings.widget.ToggleSwitch.OnBeforeCheckedChangeListener;

public class PrivacyModeSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener, OnPreferenceClickListener, OnBeforeCheckedChangeListener {

    private static final String UNLOCK_METHOD = "unlock_method";
    private static final String AUTO_EXIT_PRIVATE_MODE = "auto_exit_private_mode";
    private static final String PRIVATE_FILES = "private_files";
    private static final String KEY_LAUNCHED_CONFIRM = "launched_confirm";
    private static final int MIN_PASSWORD_LENGTH = 4;

    private static final int UNLOCK_METHOD_PATTERN = 0;
    private static final int UNLOCK_METHOD_PIN = 1;
    private static final int UNLOCK_METHOD_PASSWORD = 2;

    private static final int CONFIRM_REQUEST = 601;
    private static final int CHOOSE_LOCK_GENERIC_REQUEST = 602;
    private static final int SET_PRIVACY_MODE_PASSWORD_REQUEST = 603;
    private static final int ADD_PRIVACY_FINGERPRINT_REQUEST = 604;
    private static final int CHANGE_PASSWORD_REQUEST = 605;
    private static final int ENTER_PRIVACY_MODE_REQUEST = 606;
    private static final int CONFIRM_PRIVACY_MODE_REQUEST = 607;
    private static final int ENABLE_FINGERPRINT_UNLOCK_SCREEEN_REQUEST = 608;

    private byte[] mToken;
    private boolean mLaunchedConfirm;
    private DevicePolicyManager mDPM;
    private FingerprintManager mFingerprintManager;
    private Preference mUnlockMethod;
    private long mChallenge;
    private int mUserId;
    private TctPrivacyModeHelper mTctPrivacyModeHelper;
    private int[] mUnlockMethodSummary = new int[] {
            R.string.unlock_set_unlock_pattern_title,
            R.string.unlock_set_unlock_pin_title,
            R.string.unlock_set_unlock_password_title};

    private SwitchBar mSwitchBar;
    protected ToggleSwitch mToggleSwitch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mToken = savedInstanceState.getByteArray(
                    ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
            mLaunchedConfirm = savedInstanceState.getBoolean(
                    KEY_LAUNCHED_CONFIRM, false);
        }

        mUserId = getIntent().getIntExtra(
                Intent.EXTRA_USER_ID, UserHandle.myUserId());

        mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mFingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
        mChallenge = mFingerprintManager.preEnroll();
        mTctPrivacyModeHelper = TctPrivacyModeHelper.createHelper(getActivity());

        if (mTctPrivacyModeHelper.isPrivacyModeEnable()) {
            launchConfirmPrivacyModeLock();
        } else {
            launchChooseOrConfirmLock();
        }

//        // Need to authenticate a session token if none
//        if (mToken == null && mLaunchedConfirm == false) {
//            mLaunchedConfirm = true;
//            launchChooseOrConfirmLock();
//        }

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.privacy_mode_settings);

        mUnlockMethod = (Preference) findPreference(UNLOCK_METHOD);
        mUnlockMethod.setOnPreferenceClickListener(this);

        SwitchPreference autoExit = (SwitchPreference) findPreference(AUTO_EXIT_PRIVATE_MODE);
        autoExit.setChecked(Settings.System.getInt(getContentResolver(), Settings.System.AUTO_EXIT_PRIVATE_MODE, 0) == 1);
        autoExit.setOnPreferenceChangeListener(this);

        Preference privateFilesPre = (Preference) findPreference(PRIVATE_FILES);
        privateFilesPre.setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, final Object newValue) {
        if (AUTO_EXIT_PRIVATE_MODE.equals(preference.getKey())) {
            Settings.System.putInt(getContentResolver(), Settings.System.AUTO_EXIT_PRIVATE_MODE, (Boolean) newValue ? 1 : 0);
        }
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (UNLOCK_METHOD.equals(preference.getKey())) {
            modifyPassword();
        } else if (PRIVATE_FILES.equals(preference.getKey())) {
            Intent intent = new Intent();
            intent.setAction("com.jrdcom.filemanager.action.PRIVATEMODE");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHOOSE_LOCK_GENERIC_REQUEST
                || requestCode == CONFIRM_REQUEST || requestCode == CONFIRM_PRIVACY_MODE_REQUEST) {
            if (resultCode == Activity.RESULT_FIRST_USER || resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    mToken = data.getByteArrayExtra(
                            ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
                }
                if (mToken == null) {
                    // Didn't get an authentication, finishing
                    finish();
                    return;
                }
                //setupPrivacyModePassword();
                if (Settings.System.getInt(getContentResolver(), Settings.System.PRIVACY_MODE_ON, 0) == 0) {
                    setupPrivacyModePassword();
                }
                if (requestCode == CONFIRM_PRIVACY_MODE_REQUEST) {
                    if (!mTctPrivacyModeHelper.isInPrivacyMode()) {
                        mTctPrivacyModeHelper.enterPrivacyMode(true);
                    }
                    updateUnlockMethodPreference();
                }
            } else {
                finish();
            }
        } else if (requestCode == SET_PRIVACY_MODE_PASSWORD_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("ccxccx","privacy password is set");
                addPrivateFingerprint();
                //Settings.System.putInt(getContentResolver(), Settings.System.PRIVACY_MODE_ON, 1);
            } else {
                finish();
            }
        } else if (requestCode == CHANGE_PASSWORD_REQUEST) {
            if (resultCode == Activity.RESULT_FIRST_USER || resultCode == Activity.RESULT_OK) {
//                if (data != null) {
//                    mToken = data.getByteArrayExtra(
//                            ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
//                }
            }
            updateUnlockMethodPreference();
        } else if (requestCode == ADD_PRIVACY_FINGERPRINT_REQUEST) {
            if (resultCode == Activity.RESULT_CANCELED) {
                finish();
            } else {
                launchFinshSetupPrivacyModeScreen();
            }
        } else if (requestCode == ENTER_PRIVACY_MODE_REQUEST) {
            if (resultCode == Activity.RESULT_CANCELED) {
                finish();
            } else {
                updateUnlockMethodPreference();
            }
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putByteArray(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN,
                mToken);
        outState.putBoolean(KEY_LAUNCHED_CONFIRM, mLaunchedConfirm);
    }

    private void launchChooseOrConfirmLock() {
        Intent intent = new Intent();
        //mChallenge = mFingerprintManager.preEnroll();
        Activity ac = getActivity();
        ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(ac, this);
        if (!helper.launchConfirmationActivity(CONFIRM_REQUEST,
                getString(R.string.private_mode),
                null, null, mChallenge, mUserId)) {
            intent.setClassName("com.android.settings", ChooseLockGeneric.class.getName());
            intent.putExtra(ChooseLockGeneric.ChooseLockGenericFragment.MINIMUM_QUALITY_KEY,
                    DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
            intent.putExtra(ChooseLockGeneric.ChooseLockGenericFragment.HIDE_DISABLED_PREFS,
                    true);
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_HAS_CHALLENGE, true);
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE, mChallenge);
            intent.putExtra(Intent.EXTRA_USER_ID, mUserId);
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_SET_NORMAL_PASSWORD, true);
            startActivityForResult(intent, CHOOSE_LOCK_GENERIC_REQUEST);
        }
    }

    private void modifyPassword() {
        //[BUGFIX]-Del-BEGIN by TCTNB.caixia.chen,03/22/2016,task 1427498
        //fix can not add fingerprint after change owner/privacy password failed
        //long challenge = mFingerprintManager.preEnroll();
        //[BUGFIX]-Del-END by TCTNB.caixia.chen
        Intent intent = new Intent(ChoosePrivacyModeLockGeneric.ACTION_SET_NEW_PRIVATE_PASSWORD);
        intent.putExtra(ChoosePrivacyModeLockGeneric.ChoosePrivacyModeLockGenericFragment.MINIMUM_QUALITY_KEY,
                DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
        intent.putExtra(ChoosePrivacyModeLockGeneric.ChoosePrivacyModeLockGenericFragment.HIDE_DISABLED_PREFS,
                true);
        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
        intent.putExtra(Intent.EXTRA_USER_ID, mUserId);
        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_HAS_CHALLENGE, true);
        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE, mChallenge);
        startActivityForResult(intent, CHANGE_PASSWORD_REQUEST);
    }

    private void setupPrivacyModePassword() {
        Intent intent = new Intent();
        String clazz = SetupPrivacyMode.class.getName();
        intent.setClassName("com.android.settings",clazz);
        startActivityForResult(intent, SET_PRIVACY_MODE_PASSWORD_REQUEST);
    }

    private void addPrivateFingerprint() {
        if (mFingerprintManager.isHardwareDetected()) {
            Intent intent = new Intent();
            intent.setClassName("com.android.settings",
                    FingerprintEnrollEnrolling.class.getName());
            intent.putExtra(Intent.EXTRA_USER_ID, mUserId);
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_SHOW_SKIP_BUTTON, true);
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG, FingerprintManager.FP_TAG_PRIVATE_MODE);
            startActivityForResult(intent, ADD_PRIVACY_FINGERPRINT_REQUEST);
        } else {
            launchFinshSetupPrivacyModeScreen();
        }
    }

    private void launchFinshSetupPrivacyModeScreen() {
        Intent intent = new Intent();
        String clazz = FinishSetupPrivacyMode.class.getName();
        intent.setClassName("com.android.settings",clazz);
        startActivityForResult(intent, ENTER_PRIVACY_MODE_REQUEST);
    }

    private void launchConfirmPrivacyModeLock() {
        Intent intent = new Intent();
        Activity ac = getActivity();
        ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(ac, this);
        helper.launchPrivacyModeConfirmationActivity(CONFIRM_PRIVACY_MODE_REQUEST,
                getString(R.string.private_mode),
                null, null, mChallenge, mUserId);
    }

    private int getCurrentUnlockMethod() {
        int unlockMethod = UNLOCK_METHOD_PATTERN;
        LockPatternUtils lpu = new LockPatternUtils(getActivity());
        switch (lpu.getKeyguardStoredPasswordQuality(UserHandle.myUserId())) {
            case DevicePolicyManager.PASSWORD_QUALITY_SOMETHING:
                unlockMethod = UNLOCK_METHOD_PATTERN;
                break;
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC_COMPLEX:
                unlockMethod = UNLOCK_METHOD_PIN;
                break;
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC:
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_COMPLEX:
                unlockMethod = UNLOCK_METHOD_PASSWORD;
                break;
        }
        return unlockMethod;
    }

    private void updateUnlockMethodPreference() {
        int resId = mUnlockMethodSummary[getCurrentUnlockMethod()];
        List<Fingerprint> items = mFingerprintManager.tctGetEnrolledFingerprints(FingerprintManager.FP_TAG_PRIVATE_MODE);
        int fingerprintCount = items != null ? items.size() : 0;
        if (fingerprintCount > 0) {
            mUnlockMethod.setSummary(getResources().getText(resId) + " & " + getResources().getText(R.string.fingerprint_unlock_screen));
        } else {
            mUnlockMethod.setSummary(resId);
        }
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TCT_PRIVATE_MODE;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SettingsActivity activity = (SettingsActivity) getActivity();

        mSwitchBar = activity.getSwitchBar();
        mToggleSwitch = mSwitchBar.getSwitch();
        mSwitchBar.setChecked(true);
        mToggleSwitch.setOnBeforeCheckedChangeListener(this);
        mSwitchBar.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mToggleSwitch.setOnBeforeCheckedChangeListener(null);
        mSwitchBar.hide();
    }

    @Override
    public boolean onBeforeCheckedChanged(ToggleSwitch toggleSwitch,
            boolean checked) {
        mSwitchBar.setCheckedInternal(true);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
        .setCancelable(false)
        .setTitle(R.string.diable_privacy_mode_title)
        .setMessage(R.string.diable_privacy_mode_msg)
        .setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Settings.System.putInt(getContentResolver(), Settings.System.PRIVACY_MODE_ON, 0);
                Settings.System.putString(getContentResolver(), Settings.System.PRIVACY_MODE_PASSWORD, "");
                Settings.System.putInt(getContentResolver(), Settings.System.CURRENT_PHONE_MODE, 0);
                List<Fingerprint> items = mFingerprintManager.tctGetEnrolledFingerprints(FingerprintManager.FP_TAG_PRIVATE_MODE);
                if (items != null && items.size() > 0) {
                    mFingerprintManager.remove(items.get(0), mUserId, null);
                    Settings.System.putInt(getContentResolver(), Settings.System.TCT_PRIVATE_FINGERPRINT_UNLOCK_SCREEN, 0);
                }
                List<Fingerprint> fps = mFingerprintManager.tctGetEnrolledFingerprints(FingerprintManager.FP_TAG_COMMON);
                if (fps != null && fps.size() == 0) {
                    Settings.System.putInt(getContentResolver(), Settings.System.TCT_UNLOCK_SCREEN, 0);
                }
                //send broadcat that private mode if off
                Intent intent = new Intent("android.intent.actions.CURRENT_MODE_CHANGED");
                intent.putExtra("current_mode", 2);//0:normal mode, 1:private mode, 2:turn off private mode
                getActivity().sendBroadcast(intent);
                PrivacyModeSettings.this.finish();
            }
        })
        .setNegativeButton(R.string.dlg_cancel, null)
        .show();
        return true;
    }
}

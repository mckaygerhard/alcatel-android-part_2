
package com.android.settings;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Telephony;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

public class ApnCheckService extends IntentService {

    private static final String TAG = "ApnCheckService";
    //[FEATURE]-ADD by Changwei.Chi,10/28/2015,Task-667610(porting from idol347)
    private static final String SIM_SERVICE = "sim_service";

    /**
     * A constructor is required, and must call the super IntentService(String) constructor with a
     * name for the worker thread.
     */
    public ApnCheckService() {
        super("ApnCheckService");
    }

    static boolean issimchanged = false;

    /**
     * The IntentService calls this method from the default worker thread with the intent that
     * started the service. When this method returns, IntentService stops the service, as
     * appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        // [BUGFIX]-Mod-BEGIN by TCTNB.changwei.chi,01/23/2016,defect-1275418,
        // Without select the data SIM Card while insert the dual SIM, the APN list can
        // be displayed
        Log.d(TAG, "onHandleIntent, received " + intent.getAction());
        int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        if (TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED.equals(intent.getAction())) {
            subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                    SubscriptionManager.getDefaultDataSubscriptionId());
        } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())) {
            // Handle the SIM changed event
            String stateExtra = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
            Log.d(TAG, "onHandleIntent begin stateExtra = " + stateExtra);

            if ((stateExtra == null)
                    || (!stateExtra.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED))) {
                Log.d(TAG, "onHandleIntent, return, because sim no ready");
                return;
            }

            subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                    SubscriptionManager.getDefaultDataSubscriptionId());
        }
        SharedPreferences sp = this.getSharedPreferences("apncheck", Context.MODE_PRIVATE);

        TelephonyManager teleponyservice = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);

        ContentResolver resolver = this.getContentResolver();

        // Changed by JRD_Zhanglei for idol4s task1210744, BEGIN
        int defaultData_subid = SubscriptionManager.getDefaultDataSubscriptionId();
        Log.d(TAG, "defaultData_subid is = " + defaultData_subid + ", onHandleIntent, sub id = "
                + subId);
        if (subId < 0) {
            Log.d(TAG, "return because the subId is a dummy value");
            return;
        }

        // Only show APN List for default Data subID.
        if (subId != defaultData_subid && defaultData_subid != -1) {
            Log.d(TAG, "return because this sub is not default data sub");
            return;
        }
        // Changed for Task1210744, END

        String numeric = teleponyservice.getSimOperator(subId);
        String imsiuri = ApnSettings.APN_CHECK_IMSI;
        String oldimsi = sp.getString(imsiuri, "");
        String newimsi = teleponyservice.getSubscriberId(subId);
        Uri carrieruri = Telephony.Carriers.CONTENT_URI;
        Uri perferuri = getUri(Uri.parse(ApnSettings.PREFERRED_APN_URI)); // Add by fei.liu for
                                                                          // defect-1275882

        Log.d(TAG, "onHandleIntent, newimsi:" + newimsi + ", oldimsi" + oldimsi + ", numeric:"
                + numeric);

        if (newimsi == null || newimsi.length() == 0) {
            Log.d(TAG, "onHandleIntent, return, because new imsi is null");
            /* MODIFIED-BEGIN by changwei.chi-nb, 2016-05-14,BUG-2009473*/
            ApnCheckReceiver.isBlockService = true;
            return;
        }
        ApnCheckReceiver.isBlockService = false;
        /* MODIFIED-END by changwei.chi-nb,BUG-2009473*/

        // check the APNs with the same are more than one
        // where, such as: select * from carriers where numeric="46000" and
        // (type="" or type<>"mms")
        String where = "numeric=\"" + numeric + "\""
                + "and (type IS NULL or type<>\"mms\")" + " and NOT(mcc=\"\" and mnc=\"\")"
                + " and NOT(mcc IS NULL and mnc IS NULL)";
        Cursor cursor = resolver.query(carrieruri,
                new String[] {
                        "_id", "name", "apn", "type"
                },
                where, null, Telephony.Carriers.DEFAULT_SORT_ORDER);
        int apncount = cursor.getCount();
        Log.d(TAG, "onHandleIntent, APN list count:" + cursor.getCount());
        Log.d(TAG, "onHandleIntent, Where:" + where);

        if (newimsi.equals(oldimsi) && (issimchanged == false)) {
            Log.d(TAG, "onHandleIntent, The sim is no changed");
            // Check is done or not
            boolean isfinishapnsetting = false;
            String currentapnkey = null;

            Cursor tempcursor = resolver.query(
                    perferuri, // Changed by fei.liu for defect-1275882
                    new String[] {
                        "_id"
                    },
                    null, null, Telephony.Carriers.DEFAULT_SORT_ORDER);
            if (tempcursor.getCount() > 0) {
                tempcursor.moveToFirst();
                currentapnkey = tempcursor.getString(0);
            }
            tempcursor.close();

            // find old key in current apn list
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String apnkey = cursor.getString(0);
                if ((currentapnkey != null) && currentapnkey.equals(apnkey)) {
                    isfinishapnsetting = true;
                    break;
                }
                cursor.moveToNext();
            }

            if (isfinishapnsetting == false) {
                Log.d(TAG, "onHandleIntent, The sim is no changed, but Apn setting no done");
            } else {
                Log.d(TAG, "onHandleIntent, return, because sim is no changed and " +
                        "Apn setting done, current apn key is " + currentapnkey);
                cursor.close();
                return;
            }
        } else {
            // Uri uri = Uri.parse(ApnSettings.PREFERRED_APN_URI); //Changed by fei.liu for
            // defect-1275882
            ContentValues values = new ContentValues();
            values.put("apn_id", 0);
            resolver.update(perferuri, values, null, null); // Changed by fei.liu for defect-1275882

            issimchanged = true;
            Log.d(TAG, "onHandleIntent, sim is changed");
            Editor editor = sp.edit();
            editor.putString(imsiuri, newimsi);
            Settings.System.putString(resolver, imsiuri, newimsi);
            editor.commit();
            cursor.close();
        }

        if (apncount < 2) {
            Log.d(TAG, "onHandleIntent, return, because apn list count is not more than one");
            // need save the sim info
            Editor editor = sp.edit();
            editor.putString(imsiuri, newimsi);
            Settings.System.putString(resolver, imsiuri, newimsi);
            editor.commit();
            cursor.close();
            return;
        }
        // [FEATURE]-ADD-BEGIN by Changwei.Chi,10/28/2015,Task-667610(porting from idol347)
        boolean isUKHomoSimserviceEnable = this.getResources().getBoolean(
                com.android.internal.R.bool.feature_tctfw_openmarketsimservice_on);
        if (isUKHomoSimserviceEnable) {
            if (issimchanged) {
                Log.d(TAG, "clear the previous APN selection when insert a different SIM");
                Settings.Global.putString(resolver, SIM_SERVICE, "");
            }
            String simNumeric = (newimsi.length() >= 5) ? newimsi.substring(0, 5) : "";
            if ("23430".equals(simNumeric) || "23433".equals(simNumeric)
                    //[BUGFIX]-Add-begin by TCTNB qili.zhang 01/27/2016 for 1527070
                    || "23420".equals(simNumeric)
                    //[BUGFIX]-Add-end by TCTNB qili.zhang 01/27/2016 for 1527070
                    || "23410".equals(simNumeric) || "23415".equals(simNumeric)) {
                Log.d(TAG,
                        "onReceive, start SimServiceActivity and return, simNumeric="
                                + simNumeric);
                Editor editor = sp.edit();
                editor.putString(imsiuri, newimsi);
                Settings.System.putString(resolver, imsiuri, newimsi);
                editor.commit();
                String currentOperator = Settings.Global.getString(resolver, SIM_SERVICE);
                Intent simServiceIntent = new Intent(this, SimServiceActivity.class);
                simServiceIntent.putExtra("mccmnc", simNumeric);
                simServiceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(simServiceIntent);
                // [BUGFIX]-Add-BEGIN by TCTNJ.Jinxia.Dai,09/11/2014,786909
                issimchanged = false;
                // [BUGFIX]-Add-END by TCTNJ.Jinxia.Dai 786909
                return;
            } else {
                Settings.Global.putString(resolver, SIM_SERVICE, "");
            }
        }
        // [FEATURE]-ADD-END by Changwei.Chi
        issimchanged = false;
        // do the APN select.
        Intent apnintent = new Intent(Settings.ACTION_APN_SETTINGS);
        apnintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Bundle bundle = new Bundle();
        bundle.putBoolean(ApnSettings.START_BY_POWER_ON, true);
        apnintent.putExtras(bundle);
        // [BUG]-ADD-BEGIN by Wenting.Cao, 01/06/2015, PR-879279.
        apnintent.putExtra(ApnSettings.SUB_ID, subId);
        // [BUG]-ADD-END by Wenting.Cao.

        // [TASK]-ADD-BEGIN by fei.liu,12/24/2015,task-1170651
        boolean inMMITest = android.provider.Settings.Global.getInt(resolver,
                android.provider.Settings.Global.IN_MMI_TEST_MODE, 0) == 1;
        Log.d(TAG, "inMMITest = " + inMMITest);
        // [TASK]-ADD-END by fei.liu

        boolean autoPopApn = this.getResources().getBoolean(
                R.bool.def_settings_apn_autopop_enable);
        if (autoPopApn && !inMMITest) { // changed by fei.liu for task-1170651
            Log.d(TAG, "onHandleIntent, start APN select Activity.");
            this.startActivity(apnintent);
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        Log.d(TAG, "onHandleIntent end");
        //[BUGFIX]-Mod-END by TCTNB.changwei.chi
    }

    //Add by fei.liu for defect-1275882, begin
    private Uri getUri(Uri uri) {
        return Uri.withAppendedPath(uri, "subId/" + SubscriptionManager.getDefaultDataSubscriptionId());
    }
    //Add by fei.liu for defect-1275882, end

    @Override
    public void onCreate() {
        Log.w(TAG, "<<<onCreate()!!!");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.w(TAG, "onDestroy()!!!>>>");
        super.onDestroy();
    }

}

/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/09/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import com.android.internal.widget.LockPatternUtils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SetupPrivacyMode extends Activity implements OnClickListener {
    private static final int SET_PRIVACY_PASSWORD_REQUEST = 700;
    private static final int MIN_PASSWORD_LENGTH = 4;
    private int mUserId;
    private DevicePolicyManager mDPM;
    private byte[] mToken;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.setup_privacy_mode);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        Button btn = (Button) findViewById(R.id.continueBtn);
        btn.setOnClickListener(this);

        mUserId = getIntent().getIntExtra(
                Intent.EXTRA_USER_ID, UserHandle.myUserId());
        mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mToken = getIntent().getByteArrayExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.continueBtn:
                launchSetPrivacyModeLock();
                break;
            default:
                break;
        }
    }

    private int getPasswordQualityForCurrent() {
        LockPatternUtils lpu = new LockPatternUtils(this);
        if (lpu.isLockScreenDisabled(UserHandle.myUserId())) {
            return DevicePolicyManager.PASSWORD_QUALITY_UNSPECIFIED;
        }
        switch (lpu.getKeyguardStoredPasswordQuality(UserHandle.myUserId())) {
            case DevicePolicyManager.PASSWORD_QUALITY_SOMETHING:
                return DevicePolicyManager.PASSWORD_QUALITY_SOMETHING;
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC_COMPLEX:
                return DevicePolicyManager.PASSWORD_QUALITY_NUMERIC;
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC:
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_COMPLEX:
                return DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC;
            default:
                return DevicePolicyManager.PASSWORD_QUALITY_UNSPECIFIED;
        }
    }

    private void launchSetPrivacyModeLock() {
        int quality = getPasswordQualityForCurrent();
        Intent intent = new Intent();
        switch (quality) {
            case DevicePolicyManager.PASSWORD_QUALITY_SOMETHING:
                intent = ChoosePrivacyModeLockPattern.createIntent(this, false, 0, mUserId);
                //intent.putExtra(ChooseLockSettingsHelper.OWNER_PASSWORD, ownerPassword);
                startActivityForResult(intent, SET_PRIVACY_PASSWORD_REQUEST);
                break;
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC:
                int minLength = mDPM.getPasswordMinimumLength(null);
                if (minLength < MIN_PASSWORD_LENGTH) {
                    minLength = MIN_PASSWORD_LENGTH;
                }
                final int maxLength = mDPM.getPasswordMaximumLength(quality);
                intent = ChoosePrivacyModeLockPassword.createIntent(this, quality, minLength,
                        maxLength, false, 0, mUserId);
                //intent.putExtra(ChooseLockSettingsHelper.OWNER_PASSWORD, ownerPassword);
                startActivityForResult(intent, SET_PRIVACY_PASSWORD_REQUEST);
                break;
            default:
                finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SET_PRIVACY_PASSWORD_REQUEST) {
            if (resultCode == RESULT_OK)
            setResult(RESULT_OK);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

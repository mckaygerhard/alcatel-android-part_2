/* Copyright (C) 2016 Tcl Corporation Limited */
//PR1239537 add for MCR by zhongbin.qian.hz

package com.android.settings;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemProperties;
import android.provider.Telephony;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.telephony.SubscriptionManager;

public class CarrierSettingActivity extends Activity {
	private static final String TAG = "CarrierSettingActivity";
	private ListView mListView;
	private ArrayList<CarrierItem> mCarrierItemList  = new ArrayList<CarrierItem>();
	private Cursor mCursor;
	private Uri mUri;
	private String valueProxy;
	private String valueID;
	private String valuePassword;
	private String valueSerName;
	private String valueSubID;
	private int mSubscription = 0;

	private static final String NAME_KEY = "name";
	private static final String VALUE_KEY = "value";
	private static final String HOMEPAGE = "Homepage";
	private static final String PROXY = "Proxies";
	private static final String PPP_ID = "PPP ID";
	private static final String PPP_PASSWORD = "PPP Password";
	private static final String SERVER_NAME = "Server Name";
	private static final String POST_URI = "POST URI";
	private static final String SUBSCRIBER_ID = "Subscriber ID";
	private static final String NOT_SET = "Not set";
	private static String HOMEPAGE_URI
		= SystemProperties.get("def_chrome_home_base_url", "http://www.android.com/");
	private boolean mUseNvOperatorForEhrpd = SystemProperties.getBoolean(
		"persist.radio.use_nv_for_ehrpd", false);

	private static final String[] APN_PROJECTION = {
		Telephony.Carriers.PROXY,
		Telephony.Carriers.USER,
		Telephony.Carriers.PASSWORD,
		Telephony.Carriers.SERVER,
	};

	private static final int PROXY_INDEX = 0;
	private static final int USER_INDEX = 1;
	private static final int PASSWORD_INDEX = 2;
	private static final int SERVER_INDEX = 3;

	private class CarrierItem {
		String mName;
		String mVaule;
	}


	private class MyAdapter extends ArrayAdapter<CarrierItem> {
		private Context mContext;
		private ArrayList<CarrierItem> mList;

		public MyAdapter(Context context, int textViewResourceId, ArrayList<CarrierItem> list) {
			super(context, textViewResourceId, list);
			// TODO Auto-generated constructor stub
			mContext = context;
			mList = list;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
					android.R.layout.simple_list_item_2, null);
			}
			((TextView) convertView.findViewById(android.R.id.text1))
				.setText(mList.get(position).mName);
			((TextView) convertView.findViewById(android.R.id.text2))
				.setText(mList.get(position).mVaule);
			return convertView;
		}
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
	  	// TODO Auto-generated method stub
	  	super.onCreate(savedInstanceState);

		setContentView(R.layout.nam_items);
		setTitle("Carrier Data Settings");

		mListView = (ListView) findViewById(R.id.nam_list);
		mListView.setAdapter(new MyAdapter(this, android.R.layout.simple_list_item_1, mCarrierItemList));
        //add by miao.guo@jrdcom.com PR853429 begin
        int pos = -1;
        try {
            pos = Integer.parseInt(getAPNKey());
        } catch(NumberFormatException e) {
        }
        //add by miao.guo@jrdcom.com PR853429 end
		mUri = ContentUris.withAppendedId(Telephony.Carriers.CONTENT_URI, pos);
Log.i(TAG,"mUri: " + mUri);
		mCursor = managedQuery(mUri,
				APN_PROJECTION, null, null);
		if(mCursor != null){
        		mCursor.moveToFirst();
			valueProxy = mCursor.getString(PROXY_INDEX);
			valueID = mCursor.getString(USER_INDEX);
			valuePassword = mCursor.getString(PASSWORD_INDEX);
			valueSerName = mCursor.getString(SERVER_INDEX);
Log.i(TAG,"valueProxy: " + valueProxy+ ", valueID: " + valueID);
Log.i(TAG,"valuePassword: " + valueSerName + ", valueSubID: " + valueSerName);
			updateItems(true);
		}else{
			updateItems(false);
		}

	}

	private void updateItems(boolean set){
		if(set){
			addCarrierData(HOMEPAGE, HOMEPAGE_URI);
			addCarrierData(PROXY, checkNull(valueProxy));  //Proxy
			addCarrierData(PPP_ID, checkNull(valueID));  //Username
			addCarrierData(PPP_PASSWORD, checkNull(valuePassword)); //password
			addCarrierData(SERVER_NAME, checkNull(valueSerName)); //Server
			addCarrierData(POST_URI, "/servlets/mms");
			addCarrierData(SUBSCRIBER_ID, NOT_SET);
		}else{
			addCarrierData(HOMEPAGE, NOT_SET);
			addCarrierData(PROXY, NOT_SET);  //Proxy
			addCarrierData(PPP_ID, NOT_SET);  //Username
			addCarrierData(PPP_PASSWORD, NOT_SET); //password
			addCarrierData(SERVER_NAME, NOT_SET); //Server
			addCarrierData(POST_URI, NOT_SET);
			addCarrierData(SUBSCRIBER_ID, NOT_SET);
		}
	}

	private void addCarrierData(String name, String value){
		CarrierItem item = new CarrierItem();
		item.mName = name;
		item.mVaule = value;
		mCarrierItemList.add(item);
	}

	private String checkNull(String value) {
		if (value == null || value.length() == 0) {
			return NOT_SET;
		} else {
			return value;
		}
	}

	private String getAPNKey(){
		String key = NOT_SET;
		String where = getOperatorNumericSelection();
		if (TextUtils.isEmpty(where)) {
			Log.d(TAG, "getOperatorNumericSelection is empty ");
			return key;
		}

		Cursor cursor = getContentResolver().query(Telephony.Carriers.CONTENT_URI, new String[] {
			"_id"}, where, null,
			Telephony.Carriers.DEFAULT_SORT_ORDER);

		if (cursor != null) {
			int count = cursor.getCount();

			cursor.moveToFirst();

			//while (!cursor.isAfterLast()) {
				key = cursor.getString(0);
			//	cursor.moveToNext();
			//}
            		cursor.close();
		}

		return key;
	}

	private String getOperatorNumericSelection() {
		String[] mccmncs = getOperatorNumeric();
		String where;
		where = (mccmncs[0] != null) ? "numeric=\"" + mccmncs[0] + "\"" : "";
		where += (mccmncs[1] != null) ? " or numeric=\"" + mccmncs[1] + "\"" : "";
		Log.d(TAG, "getOperatorNumericSelection: " + where);
		return where;
	}

	private String[] getOperatorNumeric() {
		ArrayList<String> result = new ArrayList<String>();
		Log.d(TAG, "mUseNvOperatorForEhrpd= " + mUseNvOperatorForEhrpd);
		if (mUseNvOperatorForEhrpd) {
			String mccMncForEhrpd = SystemProperties.get("ro.cdma.home.operator.numeric", null);

			Log.d(TAG, "mccMncForEhrpd= " + mccMncForEhrpd);

			if (mccMncForEhrpd != null && mccMncForEhrpd.length() > 0) {
				result.add(mccMncForEhrpd);
			}
		}
		String mccMncFromSim = null;
		/*if (MSimTelephonyManager.getDefault().isMultiSimEnabled()) {
			MSimTelephonyManager mSimTelephonyManager =
				(MSimTelephonyManager)getApplicationContext().getSystemService(Context.MSIM_TELEPHONY_SERVICE);
			mccMncFromSim = mSimTelephonyManager.getIccOperatorNumeric(mSubscription);
			Log.d(TAG, "getIccOperatorNumeric: sub= " + mSubscription +
				" mcc-mnc= " + mccMncFromSim);
		} else {*/
			TelephonyManager mTelephonyManager =
				(TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
			mccMncFromSim = mTelephonyManager.getNetworkOperator(SubscriptionManager.getDefaultSubscriptionId());
			Log.d(TAG, "getIccOperatorNumeric: mcc-mnc= " + mccMncFromSim);
		//}
		if (mccMncFromSim != null && mccMncFromSim.length() > 0) {
			result.add(mccMncFromSim);
		}
		return result.toArray(new String[2]);
	}

}
//PR1239537 add for zhongbin.qian.hz

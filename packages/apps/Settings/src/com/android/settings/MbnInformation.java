/* Copyright (C) 2016 Vertu Corporation Limited */

/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/30/2016|     Dandan.Fang      |     TASK2817986      |[Telecom][Intern- */
/*           |                      |                      |al] Develop a de- */
/*           |                      |                      |bug screen to di- */
/*           |                      |                      |splay the SIM and */
/*           |                      |                      | MBN information  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import android.app.Activity;
import android.os.SystemProperties;
import android.util.Log;
import android.widget.TextView;
import android.view.View;
import com.qualcomm.qcrilhook.QcRilHook;
import com.qualcomm.qcrilhook.IQcRilHook;
import com.qualcomm.qcrilhook.QcRilHookCallback;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;

/**
 * Activity to display mbn information.
 */
public class MbnInformation extends Activity {
    private static final String TAG = "MbnInformation";

    private Context mContext = null;
    private QcRilHook mQcRilHook;
    public static int MBN_TYPE_SW = 0;
    public static int MBN_TYPE_HW = 1;

    private boolean mQcRilHookReady = false;
    private MbnMetaInfo[] mConfigSwForAllSub;
    //private MbnMetaInfo[] mConfigHwForAllSub;

       // QcRilHook Callback
    private QcRilHookCallback mQcRilHookCallback = new QcRilHookCallback() {
        @Override
        public void onQcRilHookReady() {
            log("QcRilHook is ready");
            mQcRilHookReady = true;
            setActivityView();

        }
        public void onQcRilHookDisconnected() {
            // TODO: Handle onQcRilHookDisconnected
        }
    };

    /**
     * Initialization of the Activity after it is first created. Must at least
     * call {@link android.app.Activity#setContentView(int)} to describe what is
     * to be displayed in the screen.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mbn_information);
        mContext = this;

        int numPhones = TelephonyManager.from(mContext).getPhoneCount();
        mConfigSwForAllSub = new MbnMetaInfo[numPhones];
        //mConfigHwForAllSub = new MbnMetaInfo[numPhones];

        mQcRilHook = new QcRilHook(this, mQcRilHookCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mQcRilHook.dispose() ;
        super.onDestroy();
    }

    private String getMbnConfig(int sub, int mbnType) {
        log("getMbnConfig, Sub:" + sub + " mbnType:" + mbnType);
        if (!mQcRilHookReady) {
            return null;
        }
        return mQcRilHook.qcRilGetConfig(sub, mbnType);
    }

    public String getMetaInfoForConfig(String config, int mbnType) {
        log("getMetaInfoForConfig,  config:" + config);
        if (!mQcRilHookReady || config == null) {
            return null;
        }
        return mQcRilHook.qcRilGetMetaInfoForConfig(config, mbnType);
    }

    private void getMbnInfo() {
        int numPhones = TelephonyManager.from(mContext).getPhoneCount();
        for (int i = 0; i < numPhones; i++) {
            String mbn = getMbnConfig(i, MBN_TYPE_SW);
            log(" getMbnInfo mbn =" + mbn);
            String meta = getMetaInfoForConfig(mbn, MBN_TYPE_SW);
            log("getMbnInfo meta =" + meta);
            //String hwMbn = getMbnConfig(i, MBN_TYPE_HW);
            //String hwMeta = getMetaInfoForConfig(hwMbn, MBN_TYPE_HW);

            mConfigSwForAllSub[i] = new MbnMetaInfo(mContext, mbn, meta);
            //mConfigHwForAllSub[i] = new MbnMetaInfo(mContext, hwMbn, hwMeta);
            log("sub = " + i + "Current Sw Mbn:= " + mConfigSwForAllSub[i]);
            //log("sub = " + i + "Current Hw Mbn:" + mConfigHwForAllSub[i]);
        }
    }

    private void setActivityView() {
        getMbnInfo();

        int numPhones = TelephonyManager.from(mContext).getPhoneCount();
        for (int i = 0; i < numPhones; i++) {
            TextView mbnIdTextView = null;
            TextView iccidIdTextView = null;
            TextView simOperatorTextView = null;
            if (i == 0) {
                mbnIdTextView = (TextView) findViewById(R.id.mbn_id_1); //slot1

            } else if (i == 1) {
                mbnIdTextView = (TextView) findViewById(R.id.mbn_id_2);//slot2
                mbnIdTextView.setVisibility(View.VISIBLE);
            }

            //iccid
             String iccid = TelephonyManager.getDefault().getSimSerialNumber(
                                             SubscriptionManager.getSubId(i)[0]);

             //sim operator
             String simOperator = TelephonyManager.getDefault().getSimOperatorNumeric(
                     SubscriptionManager.getSubId(i)[0]);

            //mbn information
            /* MODIFIED-BEGIN by Dandan.FANG@tcl.com, 2016-09-02,BUG-2817986*/
            //when cross-mapping happen,UE can not get correct MBN inform from *#*#4636#*#*
            int stackid = SystemProperties.getInt("persist.radio.msim.stackid_"+i, -1);
            int mbnindex = i;
            log("phoneId["+i+"]"+" on stack[" + stackid+"]");
            if(stackid != i && (stackid == 0 || stackid == 1))
            {
               mbnindex = 1-i;
            }
            if (mConfigSwForAllSub[mbnindex] == null) {
                mbnIdTextView.setText("Mbn information of Slot " + i + "is null");
            }else if (mConfigSwForAllSub[mbnindex].getMetaInfo() == null) {
                mbnIdTextView.setText(getString(R.string.sim_operator) + simOperator + "\n"
                        + getString(R.string.sim_iccid) + iccid + "\n"
                        + getString(R.string.mbn_name)
                        + mConfigSwForAllSub[mbnindex].getMetaInfo());
            } else {
                mbnIdTextView.setText(getString(R.string.sim_operator) + simOperator + "\n"
                        + getString(R.string.sim_iccid) + iccid + "\n"
                        + getString(R.string.mbn_name)
                        + mConfigSwForAllSub[mbnindex].getMetaInfo() + "\n"
                        + getString(R.string.carrier_name)
                        + mConfigSwForAllSub[mbnindex].getCarrier() + "\n"
                        + getString(R.string.device_type)
                        + mConfigSwForAllSub[mbnindex].getDeviceType() + "\n"
                        + getString(R.string.source)
                        + mConfigSwForAllSub[mbnindex].getSourceString());
                        /* MODIFIED-END by Dandan.FANG@tcl.com,BUG-2817986*/
            }
        }
    }

    private void log(String msg) {
        Log.d(TAG, "MbnInformation_" + msg);
    }

    private class MbnMetaInfo {
        // Expected like xxx-3CSFB-DSDS-CMCC
        private final int META_MIN_DASH_SIZE = 3;
        private Context mContext;
        private String mMbnId;
        private String mMetaInfo;
        private String mDeviceType = "Unknown";
        private String mCarrier = "Unknown";
        private String mMultiMode = "Unknown";
        private static final int MBN_FROM_TAR = 0;
        private static final int MBN_FROM_APP = 1;
        private static final int MBN_FROM_GOLDEN = 2;
        private static final int MBN_FROM_PC = 3;
        private static final String APP_MBN_ID_PREFIX = "MbnApp_";
        private static final String GOLDEN_MBN_ID_PREFIX = "GOLDEN_";
        private static final String MBN_FILE_SUFFIX = ".mbn";

        // default 0->can't get multisim configure, 1->ssss, 2->dsdx
        private int mMultiModeNumber = 0;
        private int mSource = MBN_FROM_TAR;

        public MbnMetaInfo(Context context, String mbnId, String metaInfo) {
            this.mContext = context;
            this.mMbnId = mbnId;
            this.mMetaInfo = metaInfo;
            setSource(mbnId);
            // get all info from meta instead of MbnID
            setDeviceCarrierMultiMode(metaInfo);
        }

        private void setMultiModeNumber(String mode) {
            if (mode == null) {
                return;
            }
            if (mode.toLowerCase().contains("ss")
                    || mode.toLowerCase().contains("singlesim")) {
                this.mMultiModeNumber = 1;
                this.mMultiMode = "ssss";
            } else if (mode.toLowerCase().contains("da")) {
                this.mMultiModeNumber = 2;
                this.mMultiMode = "dsda";
            } else if (mode.toLowerCase().contains("ds")) {
                this.mMultiModeNumber = 2;
                this.mMultiMode = "dsds";
            }
        }

        public void setDeviceCarrierMultiMode(String meta) {
            if (meta == null) {
                return;
            }

            String[] tempName;
            tempName = meta.split("-");
            int len = tempName.length;
            this.mCarrier = tempName[len - 1];
            if (len >= META_MIN_DASH_SIZE) {
                this.mDeviceType = tempName[len - 3];
                setMultiModeNumber(tempName[len - 2].toLowerCase());
            }
            return;
        }

        private void setSource(String meta) {
            if (meta == null) {
                return;
            }
            if (meta.startsWith(GOLDEN_MBN_ID_PREFIX)) {
                this.mSource = MBN_FROM_GOLDEN;
            } else if (meta.startsWith(APP_MBN_ID_PREFIX)) {
                this.mSource = MBN_FROM_APP;
            } else if (meta.equalsIgnoreCase("CMCC")||
                    meta.equalsIgnoreCase("CU") ||
                    meta.equalsIgnoreCase("CT") ){
                this.mSource = MBN_FROM_TAR;
            }else {
                this.mSource = MBN_FROM_PC;
            }
        }

        public String getCarrier() {
            return mCarrier;
        }

        public int getMultiModeNumber() {
            return mMultiModeNumber;
        }

        public String getDeviceType() {
            return mDeviceType;
        }

        public String getMultiMode() {
            return mMultiMode;
        }

        public String getMbnId() {
            return mMbnId;
        }

        public String getMetaInfo() {
            return mMetaInfo;
        }

        public int getSource() {
            return mSource;
        }

        public String getSourceString() {
            switch (mSource) {
            case MBN_FROM_GOLDEN:
                return mContext.getString(R.string.source_golden);
            case MBN_FROM_APP:
                return mContext.getString(R.string.source_application);
            case MBN_FROM_PC:
                return mContext.getString(R.string.source_pc);
            case MBN_FROM_TAR:
                return mContext.getString(R.string.source_tar);
            default:
                return "Unknown";
            }
        }

        @Override
        public String toString() {
            return "Meta Info:" + mMetaInfo + " Mbn ID:" + mMbnId + " Source:"
                    + getSourceString();
        }
    }
}

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.android.settings.datausage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.NetworkTemplate;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemProperties;
import android.provider.Settings.Global;
import android.support.v7.preference.PreferenceViewHolder;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Checkable;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.CustomDialogPreference;
import com.android.settings.R;
import com.android.settings.Utils;
//[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430
//[ForTest][FDN] Mobile data connection and USSD should be disabled when FDN is enabled
import android.provider.Settings;
import android.util.TctLog;
//[SOLUTION]-Add-END by TCTNB.(Chuanjun Chen)

import java.util.List;

public class CellDataPreference extends CustomDialogPreference implements TemplatePreference {

    private static final String TAG = "CellDataPreference";
    private static final String CARRIER_MODE_CT_CLASS_A = "ct_class_a";

    public int mSubId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
    public boolean mChecked;
    private TelephonyManager mTelephonyManager;
    private SubscriptionManager mSubscriptionManager;
    private String mCarrierMode = SystemProperties.get("persist.radio.carrier_mode", "default");
    private boolean mIsCTClassA = mCarrierMode.equals(CARRIER_MODE_CT_CLASS_A);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
    public boolean mNeedWarningDialog;

    private void showWarningDialog(AlertDialog.Builder builder,
            DialogInterface.OnClickListener listener) {
        String title = null;
        String message = getContext().getString(R.string.data_usage_enable_warning);

        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.okay, listener);
        builder.setNegativeButton(R.string.cancel, null);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    public CellDataPreference(Context context, AttributeSet attrs) {
        super(context, attrs, android.R.attr.switchPreferenceStyle);
    }

    @Override
    protected void onRestoreInstanceState(Parcelable s) {
        CellDataState state = (CellDataState) s;
        super.onRestoreInstanceState(state.getSuperState());
        mTelephonyManager = TelephonyManager.from(getContext());
        mChecked = state.mChecked;
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
        mNeedWarningDialog = state.mNeedWarningDialog;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        if (mSubId == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            mSubId = state.mSubId;
            setKey(getKey() + mSubId);
        }
        notifyChanged();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        CellDataState state = new CellDataState(super.onSaveInstanceState());
        state.mChecked = mChecked;
        state.mSubId = mSubId;
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
        state.mNeedWarningDialog = mNeedWarningDialog;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        return state;
    }

    @Override
    public void onAttached() {
        super.onAttached();
        mListener.setListener(true, mSubId, getContext());
    }

    @Override
    public void onDetached() {
        mListener.setListener(false, mSubId, getContext());
        super.onDetached();
    }

    @Override
    public void setTemplate(NetworkTemplate template, int subId, NetworkServices services) {
        if (subId == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            throw new IllegalArgumentException("CellDataPreference needs a SubscriptionInfo");
        }
        mSubscriptionManager = SubscriptionManager.from(getContext());
        mTelephonyManager = TelephonyManager.from(getContext());
        if (mSubId == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            mSubId = subId;
            setKey(getKey() + subId);
        }
        updateChecked();
    }

    private void updateChecked() {
        setChecked(mTelephonyManager.getDataEnabled(mSubId));
    }

    @Override
    protected void performClick(View view) {
        MetricsLogger.action(getContext(), MetricsEvent.ACTION_CELL_DATA_TOGGLE, !mChecked);
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
        if (mChecked) {
            mNeedWarningDialog = false;
            super.performClick(view);
        } else {
            if (getContext().getResources().getBoolean(R.bool.feature_settings_data_enalbewarning_on)) {
                mNeedWarningDialog = true;
                super.performClick(view);
            }
            setMobileDataEnabled(true);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    private void setMobileDataEnabled(boolean enabled) {
        if (DataUsageSummary.LOGD) Log.d(TAG, "setMobileDataEnabled(" + enabled + ","
                + mSubId + ")");
        //[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430
        //[ForTest][FDN] Mobile data connection and USSD should be disabled when FDN is enabled
        if (checkDataEnableByFDN(mSubId)) {
            mTelephonyManager.setDataEnabled(mSubId, false);
            setChecked(false);
        } else {
        //[SOLUTION]-Add-END by TCTNB.(Chuanjun Chen)
            mTelephonyManager.setDataEnabled(mSubId, enabled);
            setChecked(enabled);
        }
    }

    //[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430
    //[ForTest][FDN] Mobile data connection and USSD should be disabled when FDN is enabled
    private boolean checkDataEnableByFDN(int subId){
        int fdnFlag = 0;
        if (-1 == subId) {
            fdnFlag = Settings.System.getInt(getContext().getContentResolver(), "FDN_DATA_CONNECTION_DISABLE_FLAG", -1);
        } else {
            fdnFlag = Settings.System.getInt(getContext().getContentResolver(), "FDN_DATA_CONNECTION_DISABLE_FLAG" + subId, -1);
        }
        return mTelephonyManager.getIccFdnEnabled(mSubscriptionManager.getPhoneId(subId))
                   && fdnFlag != -1;
    }
    //[SOLUTION]-Add-END by TCTNB.(Chuanjun Chen)

    private void setChecked(boolean checked) {
        if (mChecked == checked) return;
        mChecked = checked;
        notifyChanged();
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        View switchView = holder.findViewById(android.R.id.switch_widget);
        switchView.setClickable(false);
        ((Checkable) switchView).setChecked(mChecked);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder,
            DialogInterface.OnClickListener listener) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
        if (mNeedWarningDialog) {
            showWarningDialog(builder, listener);
        } else {
            showDisableDialog(builder, listener);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    private void showDisableDialog(AlertDialog.Builder builder,
            DialogInterface.OnClickListener listener) {
        builder.setTitle(null)
                .setMessage(R.string.data_usage_disable_mobile)
                .setPositiveButton(android.R.string.ok, listener)
                .setNegativeButton(android.R.string.cancel, null);
    }

    @Override
    protected void onClick(DialogInterface dialog, int which) {
        if (which != DialogInterface.BUTTON_POSITIVE) {
            return;
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
        if (mNeedWarningDialog) {
            setMobileDataEnabled(true);
        } else {
            // TODO: extend to modify policy enabled flag.
            setMobileDataEnabled(false);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    private final DataStateListener mListener = new DataStateListener() {
        @Override
        public void onChange(boolean selfChange) {
            updateChecked();
        }
    };

    public abstract static class DataStateListener extends ContentObserver {
        public DataStateListener() {
            super(new Handler(Looper.getMainLooper()));
        }

        public void setListener(boolean listening, int subId, Context context) {
            if (listening) {
                Uri uri = Global.getUriFor(Global.MOBILE_DATA);
                if (TelephonyManager.getDefault().getSimCount() != 1) {
                    uri = Global.getUriFor(Global.MOBILE_DATA + subId);
                }
                context.getContentResolver().registerContentObserver(uri, false, this);
            } else {
                context.getContentResolver().unregisterContentObserver(this);
            }
        }
    }

    public static class CellDataState extends BaseSavedState {
        public int mSubId;
        public boolean mChecked;
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 09/21/2016, SOLUTION-2521568
//[TMO][UI]A Connection Properties dialog box should be displayed in the system
        public boolean mNeedWarningDialog;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        public CellDataState(Parcelable base) {
            super(base);
        }

        public CellDataState(Parcel source) {
            super(source);
            mChecked = source.readByte() != 0;
            mSubId = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeByte((byte) (mChecked ? 1 : 0));
            dest.writeInt(mSubId);
        }

        public static final Creator<CellDataState> CREATOR = new Creator<CellDataState>() {
            @Override
            public CellDataState createFromParcel(Parcel source) {
                return new CellDataState(source);
            }

            @Override
            public CellDataState[] newArray(int size) {
                return new CellDataState[size];
            }
        };
    }
}

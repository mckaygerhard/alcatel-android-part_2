package com.android.settings.fuelgauge;

import android.R.integer;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.ArrayMap;
import android.util.Log;
import android.app.AlarmManager;
import android.os.SystemClock;
import android.content.Context;
import android.app.PendingIntent;
import com.android.internal.os.BatteryStatsHelper;
import android.os.UserManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import android.os.Bundle;
import android.os.BatteryStats;
import com.android.internal.os.BatterySipper;
import com.android.internal.os.BatterySipper.DrainType;
import android.os.UserHandle;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Process;
import android.util.SparseArray;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.app.AppGlobals;
import android.os.RemoteException;
import android.content.SharedPreferences;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;

public class TctPowerDetectService extends Service {

    private static String TAG = "TctPowerDetectService";

    private static final long DETECTED_DURATION = 20 * 60 * 1000;  //20 minutes

    private static final String ACTION_POWER_DETECT_SERVICE = "com.android.settings.TCT_POWER_DETECT_SERVICE";
    private static final String POWER_DETECT_CHANGE_KEY = "power_consumption_notification_state";

    private static final String BROADCAST_HIGH_POWER_DETECT = "android.intent.action.tct.HIGH_POWER_APP_DETECTED";
    private static final String BROADCAST_HIGH_POWER_APP_LIST = "high_power_app_list";

    private static final String BROADCAST_ABNORMAL_POWER_DETECT = "android.intent.action.tct.ABNORMAL_POWER_APP_DETECTED";
    private static final String BROADCAST_ABNORMAL_POWER_APP_LIST = "abnormal_power_app_list";

    private static final String ACTION_POWER_DETECT_SERVICE_SELF = "com.android.settings.TCT_POWER_DETECT_SERVICE_SELF";
    private static final String ACTION_POWER_ABNORMAL_DETECT_SELF_COMPLETE = "com.android.settings.TCT_POWER_ABNORMAL_DETECT_SELF_COMPLETE";

    private static final String ACTION_HIGH_POWER_DETECT_SYNC = "com.android.settings.TCT_HIGH_POWER_DETECT_SYNC";
    private static final String BROADCAST_HIGH_POWER_DETECT_SYNC = "android.intent.action.tct.HIGH_POWER_APP_DETECTED_SYNC";

    public static final String PREF_NAME = "com.android.settings.tcthighpowerdetect_preferences";
    public static final String KEY_SWITCH_POWER_DETECT = "switch_power_detect";

    private BatteryStatsHelper mStatsHelper;
    private UserManager mUm;
    private PowerManager mPowerManager;
    private double HIGH_POWER_LEVEL = 250;//250mA
    private long HIGH_POWER_TIME_LEVEL = 2*60*1000;//two minutes
    private ArrayList<String> currentWhiteList = new ArrayList<String>();

    private static final String[] TOP_CMD = {"/system/bin/top", "-n", "1", "-d", "1", "-m", "10"};
    //private static final String[] DUMPSYS_POWER_CMD = {"/system/bin/dumpsys", "power"};
    private String powerRegex = "(.*?)\\_WAKE\\_LOCK(.*?)\\(uid\\=(.*?)\\, pid\\=(.*?), ws\\=(.*?)";
    private String topRegex = "[0-9]{1,}\\s+(.*?)\\s.*\\s+(.*?)\\%\\s.*\\s+(.*?)\\s+([\\s\\S]*)";
    private String topFileName = "top";

    private static final int FLAG_HIGH_POWER_NOTIFICATION = 1<<0;
    private static final int FLAG_ABNORMAL_POWER_NOTIFICATION = 1<<1;
    private static final int MSG_ABNORMAL_POWER_APPS = 0;
    private static final long ABNORMAL_POWER_DELAY = 15 * 1000;  //15 senconds
    private static final int ABNORMAL_CPU = 8;  //default is >8%

    private int mCheckWakelockCount = 0;
    private int mCheckTopCount = 0;
    private String mWakelocksUid = "";
    private String mTopApps = "";
    private File mCurrentFile;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
            case MSG_ABNORMAL_POWER_APPS:
                Log.i(TAG, "Handler msg abnormal power, start");
                if ((mWakelocksUid != null) && (!("".equals(mWakelocksUid)))) {
                    getAbnormalWakeLockApps();
                }
                if ((mTopApps != null) && (!("".equals(mTopApps)))) {
                    getAbnormalTopApps();
                }
                if ((("".equals(mWakelocksUid)) && ("".equals(mTopApps))) || (mCheckWakelockCount > 2) || (mCheckTopCount > 2)) {
                    //Have try three times to get abnormal power apps
                    Log.i(TAG, "Handler msg Wakelock or top get complete 3 times or get null, mWakelocksUid="+mWakelocksUid + " ,mCheckWakelockCount="+mCheckWakelockCount);
                    mHandler.removeMessages(MSG_ABNORMAL_POWER_APPS);
                    Intent complete = new Intent(ACTION_POWER_ABNORMAL_DETECT_SELF_COMPLETE);
                    sendBroadcast(complete);
                } else {
                    Log.i(TAG, "Handler msg Wakelock or top, delay msg 15s");
                    mHandler.removeMessages(MSG_ABNORMAL_POWER_APPS);
                    mHandler.sendEmptyMessageDelayed(MSG_ABNORMAL_POWER_APPS, ABNORMAL_POWER_DELAY);
                }
                break;
            default:
                super.handleMessage(msg);
                break;
            }
        }
    };

    BroadcastReceiver mPowerInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String action = intent.getAction();
            Log.i(TAG, "mPowerInfoReceiver action = " + action);

            if(action.equals(ACTION_POWER_ABNORMAL_DETECT_SELF_COMPLETE)) {
                ArrayList<String> result = new ArrayList<String>();
                Log.i(TAG, "mPowerInfoReceiver mWakelocksUid="+mWakelocksUid + " ,mTopApps="+mTopApps + " ,mCheckWakelockCount="+mCheckWakelockCount + " ,mCheckTopCount="+mCheckTopCount);

                if ((mWakelocksUid != null) && (!("".equals(mWakelocksUid)))) {
                    String[] uids = mWakelocksUid.split(",");
                    for (String uid : uids) {
                        PackageManager pm = getPackageManager();
                        String name = pm.getNameForUid(Integer.parseInt(uid));
                        if (!(result.contains(name))) {
                            result.add(name);
                        }
                    }
                }
                if ((mTopApps != null) && (!("".equals(mTopApps)))) {
                    String[] names = mTopApps.split(",");
                    for (String name : names) {
                        if (!(result.contains(name))) {
                            result.add(name);
                        }
                    }
                    //Save the abnormal top file
                    renameFile(topFileName);
                }

                Log.i(TAG, "Abnormal power app result:" + result);
                Intent report = new Intent(BROADCAST_ABNORMAL_POWER_DETECT);
                report.putStringArrayListExtra(BROADCAST_ABNORMAL_POWER_APP_LIST, result);
                sendBroadcast(report);

                startAlarm();
                stopSelf();
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mUm = (UserManager) getSystemService(Context.USER_SERVICE);
        mStatsHelper = new BatteryStatsHelper(this, true);
        mStatsHelper.create((Bundle)null);
        currentWhiteList = new ParsingWhiteList(this.getResources()).parsingXml();

        IntentFilter mInterFiler = new IntentFilter();
        mInterFiler.addAction(ACTION_POWER_ABNORMAL_DETECT_SELF_COMPLETE);

        registerReceiver(mPowerInfoReceiver, mInterFiler);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (mPowerInfoReceiver != null)
            unregisterReceiver(mPowerInfoReceiver);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            handleIntent(intent);
        }
        //stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    private void saveSwitchState(int state) {
        SharedPreferences spref = getSharedPreferences(
            PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = spref.edit();
        editor.putInt(KEY_SWITCH_POWER_DETECT, state).commit();
    }

    private int getSwitchState() {
        int result = 0;
        SharedPreferences spref = getSharedPreferences(
                PREF_NAME, Activity.MODE_PRIVATE);
        result = spref.getInt(KEY_SWITCH_POWER_DETECT, 0);
        return result;
     }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        Log.i(TAG, "handleIntent action = " + action);
        if (action.equals(ACTION_POWER_DETECT_SERVICE)) {
            int switStatus = intent.getIntExtra(POWER_DETECT_CHANGE_KEY,0);
            Log.i(TAG, "handleIntent switch state = " + switStatus);
            saveSwitchState(switStatus);
            if (switStatus == 0) {
                //Both high power and abnormal power defect off
                cancelAlarm();
            } else {
                //State means just turn on high power or abnormal power defect or both on
                startAlarm();
            }
            stopSelf();
        } else if (action.equals(ACTION_POWER_DETECT_SERVICE_SELF)) {
            int state = getSwitchState();

            if ((state & FLAG_HIGH_POWER_NOTIFICATION) > 0) {
                ArrayList<String> appList = getHighPowerAppList();
                Log.i(TAG, "handleIntent get high power apps, appList ="+appList);

                Intent report = new Intent(BROADCAST_HIGH_POWER_DETECT);
                report.putStringArrayListExtra(BROADCAST_HIGH_POWER_APP_LIST,appList);
                sendBroadcast(report);
            }
            if ((state & FLAG_ABNORMAL_POWER_NOTIFICATION) > 0) {
                Log.i(TAG, "handleIntent get abnormal power apps");
                getAbnormalWakeLockApps();
                getAbnormalTopApps();
            }

            if (("".equals(mWakelocksUid)) && ("".equals(mTopApps))) {
                Intent complete = new Intent(ACTION_POWER_ABNORMAL_DETECT_SELF_COMPLETE);
                sendBroadcast(complete);
            } else {
                Log.i(TAG, "Wakelocks or top not null, delay msg, mWakelocksUid="+mWakelocksUid+" ,mTopApps="+mTopApps);
                mHandler.removeMessages(MSG_ABNORMAL_POWER_APPS);
                mHandler.sendEmptyMessageDelayed(MSG_ABNORMAL_POWER_APPS, ABNORMAL_POWER_DELAY);
            }
        } else if (action.equals(ACTION_HIGH_POWER_DETECT_SYNC)) {
                final ArrayList<String> appList = getHighPowerAppList();
                Intent report = new Intent(BROADCAST_HIGH_POWER_DETECT_SYNC);
                report.putStringArrayListExtra(BROADCAST_HIGH_POWER_APP_LIST,appList);
                sendBroadcast(report);
                stopSelf();
                /*if (getSwitchState() > 0) {
                    startAlarm();
                }*/
        }
    }

    private boolean isHighPowerAppFilter(BatterySipper app) {
        if (app != null) {
            if (app.getUid() < Process.FIRST_APPLICATION_UID) {
            // System app will not be a high power app
            return false;
            }

            if (isAppInWhitelist(getPackageName(app))) {
                //except the whitelist app in high power app
                return false;
            }

            if (makeCurrentForApp(app) < HIGH_POWER_LEVEL) {
                return false;
            } else {
                Log.i(TAG, app.usagePowerMah + " ,"+app.wifiPowerMah +" ,"+ app.gpsPowerMah + " ,"+app.cpuPowerMah + " ,"+app.
                        sensorPowerMah + " ,"+app.mobileRadioPowerMah + " ,"+app.wakeLockPowerMah + " ,"+app.cameraPowerMah +" ,"+app.flashlightPowerMah);
            }

            /*if (app.wakeLockTimeMs > WAKELOCK_TIME_LIMIT) {
                //note the according to BatteryStatsImpl, the WakeLock timer will paused count when the screen is on
                //if over the wakelock time limit, we treat it high power app
                return true;
            }*/
            return true;
        }
        return false;

    }

    private double makeCurrentForApp(BatterySipper app) {
        double current = 0;
        if (app != null) {
            PackageManager pm = getPackageManager();
            String pkgName = pm.getNameForUid(app.getUid());
            Log.i(TAG, "pkgName = " + pkgName + " ,totalPowerMah = " + app.totalPowerMah + " ,cpuTimeMs = " + app.cpuTimeMs+" ,usageTimeMs="+app.usageTimeMs+" ,cpuFgTimeMs="+app.cpuFgTimeMs);
            if (app.cpuTimeMs > 1000 * 60 * 60) {//More than 1 hour
                current = (app.totalPowerMah / app.cpuTimeMs) * 1000 * 60 * 60;
            } else if (app.cpuTimeMs > HIGH_POWER_TIME_LEVEL) {
                current = app.totalPowerMah;
            }
        }
        Log.i(TAG, "makeCurrentForApp uid = " + app.getUid() + " power = " + current);
        return current;
    }

        /**
     * check whether the app is on the whitelist
     * @param packageName
     * @return
     */
    private boolean isAppInWhitelist(String packageName) {
        if (packageName != null && currentWhiteList != null) {
            for (String pckName : currentWhiteList) {
                if (packageName.equals(pckName)) {
                    return true;
                }
            }
        }

        return false;
    }


    private ArrayList<String> getHighPowerAppList(){
        Log.i(TAG, "getHighPowerAppList ");
        ArrayList<String> result = new ArrayList<String>();
        mStatsHelper.clearStats();
        mStatsHelper.refreshStats(BatteryStats.STATS_SINCE_CHARGED, mUm.getUserProfiles());
        List<BatterySipper> usageList = getCoalescedUsageList(mStatsHelper.getUsageList());
        Log.i(TAG, "usageList size " + usageList.size());

        /*if (usageList.size() == 0) {
            return result;
        }*/

        ArrayList<BatterySipper> appList = new ArrayList<BatterySipper>();
        for(int i = 0; i < usageList.size(); i++){
            BatterySipper sipper = usageList.get(i);
            Log.i(TAG, "sipper uid = " + sipper.getUid() + " sipper type = " + sipper.drainType);
            if(sipper.drainType == BatterySipper.DrainType.APP && isHighPowerAppFilter(sipper)){
                appList.add(sipper);
            }
        }

        ArrayList<BatterySipper> appListResult = new ArrayList<BatterySipper>();
        if(appList.size() != 0){
            ActivityManager activityManager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> processList = activityManager.getRunningAppProcesses();
            for(int i = 0; i < appList.size(); i++){
                final BatterySipper sipper = appList.get(i);
                for(int j = 0; j < processList.size(); j++){
                    final RunningAppProcessInfo app = processList.get(j);
                    if(app.uid == sipper.getUid()){
                        appListResult.add(sipper);
                        break;
                    }
                }
            }
        }

        PackageManager pm = getPackageManager();
        for(int i = 0; i < appListResult.size(); i++){
            final BatterySipper sipper = appListResult.get(i);
            String packageName = getPackageName(sipper);
            if(packageName != null){
                try {
                    ApplicationInfo info = pm.getApplicationInfo(packageName,0);
                    if ((info != null) && ((info.flags & ApplicationInfo.FLAG_STOPPED) == 0) && ((info.flags & ApplicationInfo.FLAG_SYSTEM) == 0)){
                        Log.i(TAG, "getHighPowerAppList packageName = " + packageName);
                        result.add(packageName);
                    }
                }catch(Exception e){
                    Log.e(TAG, "find package name error");
                }
            }
        }
        return result;
    }

    private String getPackageName(BatterySipper sipper) {
        // Bail out if the current sipper is not an App sipper.
        String defaultPackageName = null;
        if (sipper.getUid() == 0) {
            return defaultPackageName;
        }

        PackageManager pm = getPackageManager();
        final int uid = sipper.uidObj.getUid();
        sipper.mPackages = pm.getPackagesForUid(uid);
        if (sipper.mPackages != null) {
            String[] packageLabels = new String[sipper.mPackages.length];
            System.arraycopy(sipper.mPackages, 0, packageLabels, 0, sipper.mPackages.length);

            // Convert package names to user-facing labels where possible
            IPackageManager ipm = AppGlobals.getPackageManager();
            final int userId = UserHandle.getUserId(uid);
            for (int i = 0; i < packageLabels.length; i++) {
                try {
                    final ApplicationInfo ai = ipm.getApplicationInfo(packageLabels[i],
                            0 /* no flags */, userId);
                    if (ai == null) {
                        Log.d(TAG, "Retrieving null app info for package "
                                + packageLabels[i] + ", user " + userId);
                        continue;
                    }
                    CharSequence label = ai.loadLabel(pm);
                    if (label != null) {
                        packageLabels[i] = label.toString();
                    }
                    if (ai.icon != 0) {
                        defaultPackageName = sipper.mPackages[i];
                        break;
                    }
                } catch (RemoteException e) {
                    Log.d(TAG, "Error while retrieving app info for package "
                            + packageLabels[i] + ", user " + userId, e);
                }
            }

            if (packageLabels.length > 1) {
                // Look for an official name for this UID.
                for (String pkgName : sipper.mPackages) {
                    try {
                        final PackageInfo pi = ipm.getPackageInfo(pkgName, 0 /* no flags */, userId);
                        if (pi == null) {
                            Log.d(TAG, "Retrieving null package info for package "
                                    + pkgName + ", user " + userId);
                            continue;
                        }
                        if (pi.sharedUserLabel != 0) {
                            final CharSequence nm = pm.getText(pkgName,
                                    pi.sharedUserLabel, pi.applicationInfo);
                            if (nm != null){
                                if (pi.applicationInfo.icon != 0) {
                                    defaultPackageName = pkgName;
                                }
                                break;
                            }
                        }
                    } catch (RemoteException e) {
                        Log.d(TAG, "Error while retrieving package info for package "
                                + pkgName + ", user " + userId, e);
                    }
                }
            }
        }
        return defaultPackageName;
    }

    private static boolean isSharedGid(int uid) {
        return UserHandle.getAppIdFromSharedAppGid(uid) > 0;
    }

    private static boolean isSystemUid(int uid) {
        return uid >= Process.SYSTEM_UID && uid < Process.FIRST_APPLICATION_UID;
    }

    /**
     * We want to coalesce some UIDs. For example, dex2oat runs under a shared gid that
     * exists for all users of the same app. We detect this case and merge the power use
     * for dex2oat to the device OWNER's use of the app.
     * @return A sorted list of apps using power.
     */
    private static List<BatterySipper> getCoalescedUsageList(final List<BatterySipper> sippers) {
        final SparseArray<BatterySipper> uidList = new SparseArray<>();

        final ArrayList<BatterySipper> results = new ArrayList<>();
        final int numSippers = sippers.size();
        for (int i = 0; i < numSippers; i++) {
            BatterySipper sipper = sippers.get(i);
            if (sipper.getUid() > 0) {
                int realUid = sipper.getUid();

                // Check if this UID is a shared GID. If so, we combine it with the OWNER's
                // actual app UID.
                if (isSharedGid(sipper.getUid())) {
                    realUid = UserHandle.getUid(UserHandle.USER_OWNER,
                            UserHandle.getAppIdFromSharedAppGid(sipper.getUid()));
                }

                // Check if this UID is a system UID (mediaserver, logd, nfc, drm, etc).
                if (isSystemUid(realUid)
                        && !"mediaserver".equals(sipper.packageWithHighestDrain)) {
                    // Use the system UID for all UIDs running in their own sandbox that
                    // are not apps. We exclude mediaserver because we already are expected to
                    // report that as a separate item.
                    realUid = Process.SYSTEM_UID;
                }

                if (realUid != sipper.getUid()) {
                    // Replace the BatterySipper with a new one with the real UID set.
                    BatterySipper newSipper = new BatterySipper(sipper.drainType,
                            new FakeUid(realUid), 0.0);
                    newSipper.add(sipper);
                    newSipper.packageWithHighestDrain = sipper.packageWithHighestDrain;
                    newSipper.mPackages = sipper.mPackages;
                    sipper = newSipper;
                }

                int index = uidList.indexOfKey(realUid);
                if (index < 0) {
                    // New entry.
                    uidList.put(realUid, sipper);
                } else {
                    // Combine BatterySippers if we already have one with this UID.
                    final BatterySipper existingSipper = uidList.valueAt(index);
                    existingSipper.add(sipper);
                    if (existingSipper.packageWithHighestDrain == null
                            && sipper.packageWithHighestDrain != null) {
                        existingSipper.packageWithHighestDrain = sipper.packageWithHighestDrain;
                    }

                    final int existingPackageLen = existingSipper.mPackages != null ?
                            existingSipper.mPackages.length : 0;
                    final int newPackageLen = sipper.mPackages != null ?
                            sipper.mPackages.length : 0;
                    if (newPackageLen > 0) {
                        String[] newPackages = new String[existingPackageLen + newPackageLen];
                        if (existingPackageLen > 0) {
                            System.arraycopy(existingSipper.mPackages, 0, newPackages, 0,
                                    existingPackageLen);
                        }
                        System.arraycopy(sipper.mPackages, 0, newPackages, existingPackageLen,
                                newPackageLen);
                        existingSipper.mPackages = newPackages;
                    }
                }
            } else {
                results.add(sipper);
            }
        }

        final int numUidSippers = uidList.size();
        for (int i = 0; i < numUidSippers; i++) {
            results.add(uidList.valueAt(i));
        }

        // The sort order must have changed, so re-sort based on total power use.
        Collections.sort(results, new Comparator<BatterySipper>() {
            @Override
            public int compare(BatterySipper a, BatterySipper b) {
                return Double.compare(b.totalPowerMah, a.totalPowerMah);
            }
        });
        return results;
    }


    private void startAlarm() {
        Log.i(TAG, "startAlarm()");
        Intent i = new Intent(this, TctPowerDetectService.class);
        i.setAction(ACTION_POWER_DETECT_SERVICE_SELF);
        final PendingIntent pi = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager =
            (AlarmManager) getSystemService (Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + DETECTED_DURATION, pi);
    }

    private void cancelAlarm() {
    	Log.i(TAG, "cancelAlarm()");
        Intent i = new Intent(this, TctPowerDetectService.class);
        i.setAction(ACTION_POWER_DETECT_SERVICE_SELF);
        final PendingIntent pi = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager =
            (AlarmManager) getSystemService (Context.ALARM_SERVICE);
        alarmManager.cancel(pi);
    }

    private boolean isBackgroundApp(int uid) {
        boolean res = true;
        PackageManager pm = getPackageManager();
        String pkgName = pm.getNameForUid(uid);
        ActivityManager activityManager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        res = activityManager.isBackgroundProcesses(pkgName);
        Log.i(TAG, "isBackgroundProcesses pkg name =" +pkgName + " ,res="+res );
        return res;
    }

    /**
     *  process abnormal wake locks which not released all time
     */
    private void getAbnormalWakeLockApps() {
        Pattern mPattern = Pattern.compile(powerRegex);
        Matcher matcher;
        String tempUid = "";

        String wakelocks = "";
        mPowerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakelocks = mPowerManager.getWakelocks();
        if (wakelocks == null) {
            Log.i(TAG, "get null wakelocks : " + wakelocks);
        } else if (!("".equals(wakelocks))) {
            String[] str = wakelocks.split("\n");
            for (String wakelock : str) {
                Log.i(TAG, "get wakelock : " + wakelock);
                matcher = mPattern.matcher(wakelock);
                if (matcher.find()) {
                    if (!("DOZE".equalsIgnoreCase(matcher.group(1)))) {
                        String uidString = matcher.group(3);
                        int uid = Integer.parseInt(matcher.group(3));
                        if (isSystemUid(uid)) {
                            if (uid == Process.SYSTEM_UID) {
                                //Process system uid 1000
                            }
                        } else if ((uid >= Process.FIRST_APPLICATION_UID) && isBackgroundApp(uid))  {
                            if ((mCheckWakelockCount == 0) && (tempUid.indexOf(uidString) < 0)) {
                                //First get wake locks, ensure not include repeat uid
                                tempUid = tempUid + uid + ",";
                            }
                            else if ((mCheckWakelockCount > 0) && (mWakelocksUid.indexOf(uidString) >= 0) && (tempUid.indexOf(uidString) < 0)) {
                                //Try get wake locks more time, pick the wake locks which acquire all the time
                                tempUid = tempUid + uidString + ",";
                            }
                        }
                    }
                }
            }
        }
        Log.i(TAG, "get wakelock mCheckWakelockCount: " + mCheckWakelockCount+" ,mWakelocksUid="+mWakelocksUid+" ,tempUid="+tempUid);
        mWakelocksUid = tempUid;
        mCheckWakelockCount++;
    }

    /**
     * process high CPU used apps from top command
     */
    private void getAbnormalTopApps() {
        if (initFile(topFileName)) {
            execCommand(TOP_CMD, mCurrentFile);
            parseTopData(mCurrentFile, topRegex);
        }
    }

    /**
     * Filter uid make not included system app or other certain
     * @param uid to filter
     * @return
     */
    private boolean isFilterTopApp(String uid) {
        boolean res = true;
        if (  ("system".equalsIgnoreCase(uid))
           || ("root".equalsIgnoreCase(uid))
           || ("logd".equalsIgnoreCase(uid))
           || ("shell".equalsIgnoreCase(uid))) {
            res = false;
        }
        int i;
        if ((i = (uid.indexOf("u0_a"))) >= 0) { //Uid format in top : u0_a***
            Integer uId = Integer.parseInt("10" + uid.substring(i+4));
            if (isSystemUid(uId)) {  //System uid should not more than 10000
                res = false;
            }
        } else {
            res = false;
        }
        Log.i(TAG, "isFilterTopApp result : " + res + " for uid : " + uid);
        return res;
    }

    /**
     * parse file according to certain regular
     * @param file file to parse
     * @param regex regular to parse file
     */
    private void parseTopData(File file, String regex) {
        String tempApp = "";
        try {
            Log.i(TAG, "parse top data do");
            Pattern mPattern = Pattern.compile(regex);
            Matcher matcher;
            if (file != null) {
                InputStream instream = new FileInputStream(file);
                if (instream != null) {
                    InputStreamReader inputreader = new InputStreamReader(instream);
                    BufferedReader buffreader = new BufferedReader(inputreader);
                    String line;
                    while ((line = buffreader.readLine()) != null) {
                        //Log.i(TAG, "parse data do readline : " + line.toString());
                        matcher = mPattern.matcher(line);
                        if (matcher.find()) {
                            int percent = Integer.parseInt(matcher.group(2));
                            if ((percent > ABNORMAL_CPU) && ("bg".equalsIgnoreCase(matcher.group(3))) && isFilterTopApp(matcher.group(1))) {  //CPU high than 8%
                                Log.i(TAG, "parseTopData matcher found:" + line.toString());
                                String pkgName = matcher.group(4);
                                int i;
                                if ((i = (pkgName.indexOf(":"))) >= 0) {
                                    pkgName = pkgName.split(":")[0];
                                }
                                if ((mCheckTopCount == 0) && (tempApp.indexOf(pkgName) < 0)) {
                                    //First get top apps, ensure not include repeat package name
                                    tempApp = tempApp + pkgName + ",";
                                }
                                else if ((mCheckTopCount > 0) && (mTopApps.indexOf(pkgName) >= 0) && (tempApp.indexOf(pkgName) < 0)) {
                                    //Try get top apps more time, pick the top apps which use high CPU all the time
                                    tempApp = tempApp + pkgName + ",";
                                }
                            }
                        }
                    }
                    instream.close();
                }
            }
            Log.i(TAG, "get top apps mCheckTopCount: " + mCheckTopCount+" ,mTopApps="+mTopApps+" ,tempApp="+tempApp);
            mTopApps = tempApp;
            mCheckTopCount++;
        } catch (java.io.FileNotFoundException e) {
            Log.e(TAG, "The File doesn't not exist.", e);
        } catch (IOException e) {
             Log.e(TAG, e.getMessage());
        }
    }

    /**
     * init file, if exist, delete first, can be optimize
     * @param fileName eg:top.txt
     * @return
     */
    private boolean initFile(String fileName) {
        if (Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            mCurrentFile = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/" + fileName + ".txt");
            if (mCurrentFile.exists() && mCurrentFile.isFile()) {
                mCurrentFile.delete();
            }
            try {
                mCurrentFile.createNewFile();
            } catch (IOException e) {
                Log.e(TAG, "create file have an issue", e);
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * rename file
     * @param fileName eg:top.txt
     * @return
     */
    private boolean renameFile(String fileName) {
        if (Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String currentData = sdf.format(new Date());
            File dir = Environment.getExternalStorageDirectory().getAbsoluteFile();
            File newFile = new File(dir + "/" + fileName + "_" + currentData + ".txt");
            if ((mCurrentFile != null) && mCurrentFile.exists() && mCurrentFile.isFile() && (newFile != null)) {
                FilenameFilter filenameFilter = new FilenameFilter() {
                    @Override
                    public boolean accept(final File dir, final String filename) {
                        return filename.startsWith("top_");
                    }
                };
                if (!dir.isDirectory()) {
                    return false;
                }
                File[] files = dir.listFiles(filenameFilter);
                if (files == null) {
                    return false;
                }
                for (File file : files) {
                    if (file.isFile()) {
                        Log.i(TAG, "delete old abnormal top file : " + file.toString());
                        file.delete();
                    }
                }
                mCurrentFile.renameTo(newFile);
                Log.i(TAG, "rename new top file : " + newFile.toString());
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * the method of exe command
     * @param command
     * @param file
     * @return
     */
    public boolean execCommand(String[] command, File file) {
        try {
            return _runCommand(command, file);
        } catch (IOException e) {
            // TODO: handle exception
        }
        return true;
    }

    /**
     * run command
     */
    private boolean _runCommand(String[] command, File file)  throws IOException {
        InputStream in = null;
        OutputStream out = null;
        java.lang.Process process = null;
        try {
            process = new ProcessBuilder(command)
                                    .redirectErrorStream(false)
                                    .directory(new File(file.getParent()))
                                    .start();

            // get the output from the process
            in = new BufferedInputStream(process.getInputStream());
            out = new BufferedOutputStream(new FileOutputStream(file.getAbsolutePath()));

            int cnt;
            byte[] buffer = new byte[1024];
            // read from this until EOF, and write the output to a file
            while ((cnt = in.read(buffer)) != -1) {
                out.write(buffer, 0, cnt);
            }

            //There should really be a timeout here.
            if (0 != process.waitFor()) {
                Log.e(TAG, "cmd time out!");
                return false;
            }
            Log.i(TAG, "_runCommand: finished");
            return true;
        } catch (Exception e) {
            final String msg = e.getMessage();
            Log.e(TAG, "\n\n\t\tCOMMAND FAILED: " + msg, e);
            throw new IOException(msg);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (process != null) {
                    process.destroy();
                }
            } catch (Exception ignored) {}
        }
    }
}

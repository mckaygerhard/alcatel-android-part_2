/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Telephony;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
import java.util.ArrayList;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

public class ApnPreference extends Preference implements
        CompoundButton.OnCheckedChangeListener, OnClickListener {
    final static String TAG = "ApnPreference";

    public ApnPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ApnPreference(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.apnPreferenceStyle);
    }

    public ApnPreference(Context context) {
        this(context, null);
    }

    private static String mSelectedKey = null;
    private static CompoundButton mCurrentChecked = null;
    private boolean mProtectFromCheckedChange = false;
    private boolean mSelectable = true;
    private boolean mApnReadOnly = false;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
    private int mSubId;
    public static final String OPERATOR_NUMERIC_EXTRA = "operator";
    private boolean mUseNvOperatorForEhrpd = SystemProperties.getBoolean("persist.radio.use_nv_for_ehrpd", false);

    private static final String APN_SIM_OPERATOR_BACKUP_KEY = "gsm.apn.sim.numeric.backup";

    public void setSubId(int subid) {
        mSubId = subid;
    }

    private Uri getUri(Uri uri) {
        return Uri.withAppendedPath(uri, "/subId/" + mSubId);
    }

    private String[] getOperatorNumeric() {
        ArrayList<String> result = new ArrayList<String>();
        if (mUseNvOperatorForEhrpd) {
            String mccMncForEhrpd = SystemProperties.get("ro.cdma.home.operator.numeric", null);
            if (mccMncForEhrpd != null && mccMncForEhrpd.length() > 0) {
                result.add(mccMncForEhrpd);
            }
        }

        String mccMncFromSim = TelephonyManager.getDefault().getSimOperator(mSubId);
        Log.d(TAG, "getOperatorNumeric: sub= " + mSubId +
                    " mcc-mnc= " + mccMncFromSim);

        if (null == mccMncFromSim || TextUtils.isEmpty(mccMncFromSim)) {
            mccMncFromSim = TelephonyManager.getTelephonyProperty(
                    mSubId, APN_SIM_OPERATOR_BACKUP_KEY, null);
            Log.d(TAG, " getMccMncFromSim is null,then get from backup operator with value " + mccMncFromSim);
        }

        if (mccMncFromSim != null && mccMncFromSim.length() > 0) {
            result.add(mccMncFromSim);
        }
        return result.toArray(new String[2]);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);

        View widget = view.findViewById(R.id.apn_radiobutton);
        if ((widget != null) && widget instanceof RadioButton) {
            RadioButton rb = (RadioButton) widget;
            if (mSelectable) {
                rb.setOnCheckedChangeListener(this);

                boolean isChecked = getKey().equals(mSelectedKey);
                if (isChecked) {
                    mCurrentChecked = rb;
                    mSelectedKey = getKey();
                }

                mProtectFromCheckedChange = true;
                rb.setChecked(isChecked);
                mProtectFromCheckedChange = false;
                rb.setVisibility(View.VISIBLE);
            } else {
                rb.setVisibility(View.GONE);
            }
        }

        View textLayout = view.findViewById(R.id.text_layout);
        if ((textLayout != null) && textLayout instanceof RelativeLayout) {
            textLayout.setOnClickListener(this);
        }
    }

    public boolean isChecked() {
        return getKey().equals(mSelectedKey);
    }

    public void setChecked() {
        mSelectedKey = getKey();
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.i(TAG, "ID: " + getKey() + " :" + isChecked);
        if (mProtectFromCheckedChange) {
            return;
        }

        if (isChecked) {
            if (mCurrentChecked != null) {
                mCurrentChecked.setChecked(false);
            }
            mCurrentChecked = buttonView;
            mSelectedKey = getKey();
            callChangeListener(mSelectedKey);
        } else {
            mCurrentChecked = null;
            mSelectedKey = null;
        }
    }

    public void onClick(android.view.View v) {
        if ((v != null) && (R.id.text_layout == v.getId())) {
            Context context = getContext();
            if (context != null) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
                if (getKey().equals("Dummy")) {
                    //mSubId = mActivity.getIntent().getIntExtra(ApnSettings.SUB_ID, SubscriptionManager.getDefaultDataSubscriptionId());
                    Intent intent = new Intent(Intent.ACTION_INSERT, getUri(Telephony.Carriers.CONTENT_URI));
                    intent.putExtra(OPERATOR_NUMERIC_EXTRA, getOperatorNumeric()[0]);
                    intent.putExtra(ApnSettings.SUB_ID, mSubId);
                    context.startActivity(intent);
                } else {
                    int pos = Integer.parseInt(getKey());
                    Uri url = ContentUris.withAppendedId(Telephony.Carriers.CONTENT_URI, pos);
                    Intent intent = new Intent(Intent.ACTION_EDIT, url);
                    intent.putExtra("DISABLE_EDITOR", mApnReadOnly);
                    context.startActivity(intent);
                }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
            }
        }
    }

    public void setSelectable(boolean selectable) {
        mSelectable = selectable;
    }

    public boolean getSelectable() {
        return mSelectable;
    }

    public void setApnReadOnly(boolean apnReadOnly) {
        mApnReadOnly = apnReadOnly;
    }

    public boolean getApnReadOnly() {
        return mApnReadOnly;
    }
}

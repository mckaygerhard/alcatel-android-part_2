/************************************************************************************************************/
/*                                                                           Date : 26/01/2013 */
/*                                      PRESENTATION                                           */
/*                        Copyright (c) 2013 JRD Communications, Inc.                          */
/************************************************************************************************************/
/*                                                                                                          */
/*              This material is company confidential, cannot be reproduced in any             */
/*              form without the written permission of JRD Communications, Inc.                */
/*                                                                                                          */
/*==========================================================================================================*/
/*   Author :                                                                                 */
/*   Role :    Setting                                                                        */
/*   Reference documents : None                                                               */
/*==========================================================================================================*/
/* Comments :                                                                                 */
/*     file    :                                                                              */
/*     Labels  :                                                                              */
/*==========================================================================================================*/
/* Modifications   (month/day/year)                                                           */
/*==========================================================================================================*/
/* date    | author       |FeatureID                                 |modification            */
/*=========|==============|==========================================|======================================*/

/*==========================================================================================================*/
/* Problems Report(PR/CR)                                                                     */
/*==========================================================================================================*/
/* date    | author    | PR #                               |                                 */
/*==========|==============|=========================================|======================================*/
/* ----------|----------------------|----------------------|----------------- */
/* 10/20/2015|      HongZhang       |    Task-609273-ALM   | SDM Control      */
/* ----------|----------------------|----------------------|----------------- */
/* 12/16/2015|      HongZhang       |  Defect-1170235-ALM  | Change SDM name  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.search.Indexable.SearchIndexProvider;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.SearchIndexableResource;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.preference.PreferenceCategory;
import com.android.internal.logging.MetricsLogger;
//import com.android.settings.wakeup.WakeUpSettings;
import android.os.SystemProperties;

import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import com.android.internal.logging.MetricsProto.MetricsEvent;

public class GesturesSettings extends SettingsPreferenceFragment implements
Indexable,OnPreferenceChangeListener,OnPreferenceClickListener {

	private static final String TAG = "GesturesSettings";

    private static final String KEY_GLOVE_MODE_PREFERENCE = "glove_mode_preference";
    private static final String KEY_CALLS_PREFERENCE = "calls_preference";
//    private static final String KEY_ALARM_PREFERENCE = "alarm_preference";//DELETE BY DINGYI 2016/10/25 FOR DEFECT 3213594
    private static final String KEY_WAKE_UP_PREFERENCE = "wake_up_preference";
//	private final String PRE_BLACKGESTURE_KEY = "black_gesture";

    private static final String KEY_SENSOR_AND_TOUCH_CATEGORY = "sensor_and_touch_category";
    private static final String KEY_INCOMING_CALL_CATEGORY = "incoming_call_category";
//    private static final String KEY_ALARM_CATEGORY = "alarm_category";//DELETE BY DINGYI 2016/10/25 FOR DEFECT 3213594
    private static final String KEY_SCREEN_GESTURES_CATEGORY="screen_gestures_category";

    // [FEATURE]-Add-Begin by TCTNB.junqiang.shi,10/20/2016,task-3066103-ALM
    private static final String KEY_THREE_FINGER_SCREENSHOT = "three_finger_screenshot";
    private final String THREE_FINGER_GESTURES_ENABLE = "persist.sys.ssshot.threePointer";
    // [FEATURE]-Add-End by TCTNB.junqiang.shi
//    private static final String KEY_TOUCHLESS_UI = "touchless_ui";
//    private Preference mTouchlessPreferenceScreen;

//    private static final String KEY_FINGER_SCREENSHOT_PREFERENCE = "finger_screenshot";
//    private static final String KEY_SCREENSHOT_GESTURES_CATEGORY = "screenshot_gestures";

    private SwitchPreference mGloveModePreference;
    private SwitchPreference mCallsPreference;
//    private SwitchPreference mAlarmPreference;//DELETE BY DINGYI 2016/10/25 FOR DEFECT 3213594
    // private ListPreference mAlarmPreference;
    private SwitchPreference mWakeUpPreference;
    private SwitchPreference mThreeFingerSsshot;// [FEATURE] TCTNB.junqiang.shi,10/20/2016,task-3066103-ALM

//    private static final String KEY_VIRTUAL_REALITY_PREFERENCE = "virtual_reality_perference";
//    private SwitchPreference mVirtualRealityPreference;

    private PreferenceGroup mSensorAndTouchCategory;
    private PreferenceGroup mIncomingCallCategory;
//    private PreferenceGroup mAlarmCategory;//DELETE BY DINGYI 2016/10/25 FOR DEFECT 3213594
    private PreferenceGroup mScreenGesturesCategory;
//    private PreferenceScreen blackGesture;
//    private PreferenceScreen mFingerScreenshot;
//    private PreferenceCategory mScreenshotGestureCategory;

    private Boolean glovemodemenu = false;
    private boolean screenshotEditEnable = false;
//    private Boolean fingerScreenshotMenu = false;

    private final Configuration mCurConfig = new Configuration();

    private Context mContext;
    public static boolean blackGestureFeatureOn = false;

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TCT_GESTURES;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();

        addPreferencesFromResource(R.xml.gestures_settings);
        mSensorAndTouchCategory = (PreferenceGroup) findPreference(KEY_SENSOR_AND_TOUCH_CATEGORY);
        mIncomingCallCategory = (PreferenceGroup) findPreference(KEY_INCOMING_CALL_CATEGORY);
//        mAlarmCategory = (PreferenceGroup) findPreference(KEY_ALARM_CATEGORY);//DELETE BY DINGYI 2016/10/25 FOR DEFECT 3213594
        mScreenGesturesCategory = (PreferenceGroup) findPreference(KEY_SCREEN_GESTURES_CATEGORY);
        mGloveModePreference = (SwitchPreference) findPreference(KEY_GLOVE_MODE_PREFERENCE);
//        mVirtualRealityPreference = (SwitchPreference) findPreference(KEY_VIRTUAL_REALITY_PREFERENCE);
//
//        if(!SystemProperties.getBoolean("ro.vrglass.enabled", false)){
//            getPreferenceScreen().removePreference(mVirtualRealityPreference);
//        }
        mGloveModePreference.setPersistent(false);

//        mTouchlessPreferenceScreen = (Preference)findPreference(KEY_TOUCHLESS_UI);
//        if (!mContext.getResources().getBoolean(
//                com.android.internal.R.bool.feature_tctfw_touchlessui_on)) {
//            mSensorAndTouchCategory.removePreference(mTouchlessPreferenceScreen);
//            mTouchlessPreferenceScreen = null;
//        }
        glovemodemenu=this.getResources().getBoolean(com.android.internal.R.bool.feature_tctsetting_glovemode_on);

        if (!glovemodemenu) {
            //MODIFY BEGIN BY DINGYI 2016/10/25 FOR DEFECT 3213594
            mSensorAndTouchCategory.removePreference(mGloveModePreference);
            getPreferenceScreen().removePreference(findPreference(KEY_SENSOR_AND_TOUCH_CATEGORY));
            //MODIFY END BY DINGYI 2016/10/25 FOR DEFECT 3213594
        }
        mCallsPreference = (SwitchPreference) findPreference(KEY_CALLS_PREFERENCE);
        mCallsPreference.setPersistent(false);

        //DELETE BEGIN BY DINGYI 2016/10/25 FOR DEFECT 3213594
//        mAlarmPreference = (SwitchPreference) findPreference(KEY_ALARM_PREFERENCE);
//        mAlarmPreference.setPersistent(false);
        //DELETE END BY DINGYI 2016/10/25 FOR DEFECT 3213594
        // mAlarmPreference = (ListPreference) findPreference(KEY_ALARM_PREFERENCE);
        // mAlarmPreference.setPersistent(false);

        mWakeUpPreference = (SwitchPreference) findPreference(KEY_WAKE_UP_PREFERENCE);
        mWakeUpPreference.setPersistent(false);

        // [FEATURE] Begin TCTNB.junqiang.shi,10/20/2016,task-3066103-ALM
        screenshotEditEnable = mContext.getResources().getBoolean(com.android.internal.R.bool.feature_screenshot_edit_enable);
        if (screenshotEditEnable) {
            mThreeFingerSsshot = (SwitchPreference) findPreference(KEY_THREE_FINGER_SCREENSHOT);
            mThreeFingerSsshot.setPersistent(false);
        }else {
            mScreenGesturesCategory.removePreference(findPreference(KEY_THREE_FINGER_SCREENSHOT));
        }
        // [FEATURE] End TCTNB.junqiang.shi,10/20/2016,task-3066103-ALM

//        if (!com.tct.feature.Global.TCT_TARGET_DBUNLOCK) {
//            getPreferenceScreen().removePreference(findPreference(KEY_WAKE_UP_PREFERENCE));
//        }


//        blackGesture = (PreferenceScreen) findPreference(PRE_BLACKGESTURE_KEY);
//        blackGestureFeatureOn = getResources().getBoolean(R.bool.feature_settings_blackscreengesture_on);

//        if (!blackGestureFeatureOn)
//            mScreenGesturesCategory.removePreference(findPreference(PRE_BLACKGESTURE_KEY));
//
//        boolean blackGestureSwitch =
//                Settings.System.getInt(getContentResolver(), WakeUpSettings.BLACK_GESTURE_ENABLE, 0) == 1;
//        blackGesture.setSummary(blackGestureSwitch ? R.string.gesture_switch_on : R.string.gesture_switch_off);
//
//        mFingerScreenshot = (PreferenceScreen) findPreference(KEY_FINGER_SCREENSHOT_PREFERENCE);
//        mFingerScreenshot.setPersistent(false);
//        fingerScreenshotMenu = this.getResources().getBoolean(com.android.internal.R.bool.feature_tctsetting_finger_screenshot_on);
//
//        mScreenshotGestureCategory = (PreferenceCategory) findPreference(KEY_SCREENSHOT_GESTURES_CATEGORY);
//        if (!fingerScreenshotMenu){
//            mScreenshotGestureCategory.removeAll();
//            getPreferenceScreen().removePreference(findPreference(KEY_SCREENSHOT_GESTURES_CATEGORY));
//        }

    }

    /**
     * use to update the state of gestures
     */
    private void update(){
       int mDefTurnOverToMute = Settings.System.getInt(getContentResolver(), Settings.System.TCT_TURN_OVER_TO_MUTE, 0);
       mCallsPreference.setChecked(mDefTurnOverToMute == 1);

       //DELETE BEGIN BY DINGYI 2016/10/25 FOR DEFECT 3213594
//       int mAlarmTurnOver = Settings.System.getInt(getContentResolver(), Settings.System.TCT_ALARM_TURNOVER_TO_ACTIVATE, 1);
//       mAlarmPreference.setChecked(mAlarmTurnOver == 1);
       //DELETE END BY DINGYI 2016/10/25 FOR DEFECT 3213594
//       mVirtualRealityPreference.setChecked(Settings.System.getInt(getContentResolver(), Settings.System.TCT_VIRTUAL_REALITY,0)==1);

       if (glovemodemenu) {
           mGloveModePreference.setChecked(
                   Settings.System.getInt(getContentResolver(), Settings.System.TCT_GLOVE_MODE_ENABLE, 0) == 1);
       }

       mWakeUpPreference.setChecked(Settings.System.getInt(getContentResolver(),
               Settings.System.TCT_DOUBLE_CLICK, 1) == 1);

       if (screenshotEditEnable) {
           mThreeFingerSsshot.setChecked(SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, false));// [FEATURE] TCTNB.junqiang.shi,10/20/2016,task-3066103-ALM
       }
       /*
       final int tct_alarm_turnover_to_activate=Settings.System.getInt(getContentResolver(),
            Settings.System.TCT_ALARM_TURNOVER_TO_ACTIVATE, 1);

       mAlarmPreference.setValue(String.valueOf(tct_alarm_turnover_to_activate));
       updateAlarmPreferenceDescription(tct_alarm_turnover_to_activate);
       mAlarmPreference.setOnPreferenceChangeListener(this);
       mAlarmPreference.setOnPreferenceClickListener(this);
       */

//        if(blackGestureFeatureOn) {
//            boolean defGesture = this.getResources().getBoolean(com.android.internal.R.bool.def_feature_settings_gesture);
//            int on = Settings.System.getInt(getContentResolver(), WakeUpSettings.BLACK_GESTURE_ENABLE,
//                    defGesture ? 1 : 0);
//            int summary = R.string.switch_on_text;
//            if (on == 1)
//                summary = R.string.switch_on_text;
//            else
//                summary = R.string.switch_off_text;
//            blackGesture.setSummary(summary);
//        }

//        mFingerScreenshot.setOnPreferenceClickListener(this);
//        if (fingerScreenshotMenu) {
//            Boolean fingerScreenshotOn = Settings.System.getInt(getContentResolver(),
//                    "screenshot_gesture_enable", 0) == 1;
//            mFingerScreenshot.setSummary(fingerScreenshotOn? R.string.switch_on_text:R.string.switch_off_text);
//        }

    }
    /**
     * use to update the summary of alarm
     */
    /* AVG not support this funtion,so remove it temporatily
    private void updateAlarmPreferenceDescription(long currentValue) {
        ListPreference preference = mAlarmPreference;
        String summary;
        if (currentValue < 0) {
            // Unsupported value
            summary = "";
        } else {
            final CharSequence[] entries = preference.getEntries();
            final CharSequence[] values = preference.getEntryValues();
            if (entries == null || entries.length == 0) {
                summary = "";
            } else {
                int index = 0;
                for (int i = 0; i < values.length; i++) {
                	int alarm_value = Integer.parseInt(values[i].toString());
                    if (alarm_value == currentValue) {
                    	index = i;
                    	break;
                    }
                }
                summary = entries[index].toString();
            }
        }
        preference.setSummary(summary);
    }
    */

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mCurConfig.updateFrom(newConfig);
    }
    @Override
    public void onResume() {
        super.onResume();
        update();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference == mCallsPreference) {
            boolean value = mCallsPreference.isChecked();
            Settings.System.putInt(getContentResolver(),
                    Settings.System.TCT_TURN_OVER_TO_MUTE, value ? 1 : 0);

            return true;
        }
        //DELETE BEGIN BY DINGYI 2016/10/25 FOR DEFECT 3213594
//        else if (preference == mAlarmPreference) {
//            boolean value = mAlarmPreference.isChecked();
//            Settings.System.putInt(getContentResolver(),
//                    Settings.System.TCT_ALARM_TURNOVER_TO_ACTIVATE, value ? 1 : 0);
//
//            return true;
//        }
        //DELETE END BY DINGYI 2016/10/25 FOR DEFECT 3213594
        else if(preference == mWakeUpPreference){
            String file_path=this.getResources().getString(com.android.internal.R.string.tp_device_doubleclick_enable);
            boolean value = mWakeUpPreference.isChecked();
            Settings.System.putInt(getContentResolver(),
                    Settings.System.TCT_DOUBLE_CLICK, value ? 1 : 0);
            SetDoubleClickValue(value,file_path);
        } else if(preference == mThreeFingerSsshot){
            // [FEATURE]-Add-Begin by TCTNB.junqiang.shi,10/20/2016,task-3066103-ALM
            if (screenshotEditEnable){
                boolean value = mThreeFingerSsshot.isChecked();
                SystemProperties.set(THREE_FINGER_GESTURES_ENABLE, String.valueOf(value));
                return true;
            }
            // [FEATURE]-Add-End by TCTNB.junqiang.shi
        }
//        else if(preference == mVirtualRealityPreference){
//            boolean value = mVirtualRealityPreference.isChecked();
//            Settings.System.putInt(getContentResolver(),Settings.System.TCT_VIRTUAL_REALITY, value ? 1 : 0);
//            return true;
//        }

        if (glovemodemenu) {
            if (preference == mGloveModePreference) {
                String file_path=this.getResources().getString(com.android.internal.R.string.tp_device_glove_enable);
                boolean value = mGloveModePreference.isChecked();
                Settings.System.putInt(getContentResolver(), Settings.System.TCT_GLOVE_MODE_ENABLE, value ? 1 : 0);
                SetGloveModeValue(value,file_path);
            }
        }

        return super.onPreferenceTreeClick(preference);
    }

    private void SetDoubleClickValue(boolean status,String path) {
        char temp = status ? '1' : '0';
        FileWriter fileOutStream = null;
        try {
            File file = new File(path);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(temp);
            writer.flush();
            writer.close();
            Log.i(TAG, "DoubleClickValue temp=:" + temp);
        } catch (FileNotFoundException ex) {
            Log.w(TAG, "file  not found: " + ex);
        } catch (IOException ex) {
            Log.w(TAG, "IOException trying to  : " + ex);
        } catch (RuntimeException ex) {
            Log.w(TAG, "exception while syncing file: ", ex);
        } finally {
            if (fileOutStream != null) {
                try {
                    fileOutStream.close();
                } catch (IOException ex) {
                    Log.w(TAG, "IOException while closing synced file: ", ex);
                } catch (RuntimeException ex) {
                    Log.w(TAG, "exception while closing file: ", ex);
                }
            }
        }
    }

    private void SetGloveModeValue(boolean status,String path) {
        char temp = status ? '1' : '0';
        FileWriter fileOutStream = null;
        try {
            File file = new File(path);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(temp);
            writer.flush();
            writer.close();
            Log.i(TAG, "GloveModeValue temp=:" + temp);
        } catch (FileNotFoundException ex) {
            Log.w(TAG, "file  not found: " + ex);
        } catch (IOException ex) {
            Log.w(TAG, "IOException trying to  : " + ex);
        } catch (RuntimeException ex) {
            Log.w(TAG, "exception while syncing file: ", ex);
        } finally {
            if (fileOutStream != null) {
                try {
                    fileOutStream.close();
                } catch (IOException ex) {
                    Log.w(TAG, "IOException while closing synced file: ", ex);
                } catch (RuntimeException ex) {
                    Log.w(TAG, "exception while closing file: ", ex);
                }
            }
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        String key = preference.getKey();
        /*
        if (KEY_ALARM_PREFERENCE.equals(key)) {
            try {
                int value = Integer.parseInt((String) objValue);
                Settings.System.putInt(getContentResolver(), Settings.System.TCT_ALARM_TURNOVER_TO_ACTIVATE, value);
                updateAlarmPreferenceDescription(value);
            } catch (NumberFormatException e) {
                Log.e(TAG, "could not persist screen timeout setting", e);
            }
        }
        */
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
//        if (fingerScreenshotMenu) {
//            if (preference == mFingerScreenshot) {
//                Intent intent = new Intent(mContext, FingerScreenshotActivity.class);
//                startActivity(intent);
//            }
//        }
        return false;
    }
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
        @Override
        public List<SearchIndexableResource> getXmlResourcesToIndex(Context context, boolean enabled) {
            List<SearchIndexableResource> result = new ArrayList<SearchIndexableResource>();

            final SearchIndexableResource sir = new SearchIndexableResource(context);
            sir.xmlResId = R.xml.gestures_settings;
            result.add(sir);

            return result;
        }
    };
}


package com.android.settings.sim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import android.content.ComponentName;
import android.net.wifi.WifiManager;
import android.content.Context;

import com.android.settings.R;
import com.android.setupwizard.navigationbar.SetupWizardNavBar;
import com.android.setupwizard.navigationbar.SetupWizardNavBar.NavigationBarListener;

public class SetupWizardMultiSimSettings extends Activity implements NavigationBarListener {
    SimSettings fragment;
    private SetupWizardNavBar mNavigationBar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        Log.i("com.tct.setupwizard", "SetupWizardMultiSimSettings->onCreate");

        setContentView(R.layout.setup_wizard_qct);

        // Set the title selected (marquee)
        findViewById(R.id.sim_management_title).setSelected(true);
        fragment=(SimSettings)getFragmentManager().findFragmentById(
                R.id.MultiSimSettingsFragment);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);


    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            this.onNavigateBack();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onNavigationBarCreated(final SetupWizardNavBar bar) {
        mNavigationBar = bar;
        if (mNavigationBar != null) {
            //mNavigationBar.setUseImmersiveMode(false);
            mNavigationBar.getNextButton().setText(R.string.next_label);
            mNavigationBar.getNextButton().setEnabled(true);
            //mNavigationBar.setUseImmersiveMode(true);
        }
    }

    @Override
    public void onNavigateBack() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onNavigateNext() {
        setResult(RESULT_OK);
        finish();
    }
}


/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.nfc;

import android.app.ActionBar;
/* MODIFIED-BEGIN by Ji.Chen, 2016-09-29,BUG-3004922*/
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
/* MODIFIED-END by Ji.Chen,BUG-3004922*/
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.UserManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast; // MODIFIED by Ji.Chen, 2016-09-29,BUG-3004922

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settingslib.HelpUtils;
import com.android.settings.InstrumentedFragment;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.ShowAdminSupportDetailsDialog;
import com.android.settings.widget.SwitchBar;
import com.android.settingslib.RestrictedLockUtils;

import static com.android.settingslib.RestrictedLockUtils.EnforcedAdmin;

public class AndroidBeam extends InstrumentedFragment
        implements SwitchBar.OnSwitchChangeListener {
    private View mView;
    private NfcAdapter mNfcAdapter;
    private SwitchBar mSwitchBar;
    private CharSequence mOldActivityTitle;
    private boolean mBeamDisallowedByBase;
    private boolean mBeamDisallowedByOnlyAdmin;

    /* MODIFIED-BEGIN by Ji.Chen, 2016-09-29,BUG-3004922*/
    private IntentFilter mFilter;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (NfcAdapter.ACTION_ADAPTER_STATE_CHANGED.equals(action)) {
                handleNfcStateChanged(intent.getIntExtra(NfcAdapter.EXTRA_ADAPTER_STATE,
                        NfcAdapter.STATE_OFF));
            }
        }
    };
    /* MODIFIED-END by Ji.Chen,BUG-3004922*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        HelpUtils.prepareHelpMenuItem(getActivity(), menu, R.string.help_uri_beam,
                getClass().getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final EnforcedAdmin admin = RestrictedLockUtils.checkIfRestrictionEnforced(
                getActivity(), UserManager.DISALLOW_OUTGOING_BEAM, UserHandle.myUserId());
        final UserManager um = UserManager.get(getActivity());
        mBeamDisallowedByBase = RestrictedLockUtils.hasBaseUserRestriction(getActivity(),
                UserManager.DISALLOW_OUTGOING_BEAM, UserHandle.myUserId());
        if (!mBeamDisallowedByBase && admin != null) {
            View view = inflater.inflate(R.layout.admin_support_details_empty_view, null);
            ShowAdminSupportDetailsDialog.setAdminSupportDetails(getActivity(), view, admin, false);
            view.setVisibility(View.VISIBLE);
            mBeamDisallowedByOnlyAdmin = true;
            return view;
        }
        mView = inflater.inflate(R.layout.android_beam, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /* MODIFIED-BEGIN by Ji.Chen, 2016-09-29,BUG-3004922*/
        mFilter = new IntentFilter();
        mFilter.addAction(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED);
        /* MODIFIED-END by Ji.Chen,BUG-3004922*/

        SettingsActivity activity = (SettingsActivity) getActivity();

        mOldActivityTitle = activity.getActionBar().getTitle();

        mSwitchBar = activity.getSwitchBar();
        if (mBeamDisallowedByOnlyAdmin) {
            mSwitchBar.hide();
        } else {
            mSwitchBar.setChecked(!mBeamDisallowedByBase && mNfcAdapter.isNdefPushEnabled());
            mSwitchBar.addOnSwitchChangeListener(this);
            mSwitchBar.setEnabled(!mBeamDisallowedByBase);
            mSwitchBar.show();
        }
    }

    @Override
    /* MODIFIED-BEGIN by Ji.Chen, 2016-09-29,BUG-3004922*/
    public void onResume() {
        super.onResume();
        if (!mBeamDisallowedByOnlyAdmin) {
            getActivity().registerReceiver(mReceiver, mFilter);
        }
    }

    @Override
    /* MODIFIED-END by Ji.Chen,BUG-3004922*/
    public void onDestroyView() {
        super.onDestroyView();
        if (mOldActivityTitle != null) {
            getActivity().getActionBar().setTitle(mOldActivityTitle);
        }
        if (!mBeamDisallowedByOnlyAdmin) {
            mSwitchBar.removeOnSwitchChangeListener(this);
            mSwitchBar.hide();
            /* MODIFIED-BEGIN by Ji.Chen, 2016-09-29,BUG-3004922*/
            getActivity().unregisterReceiver(mReceiver);
        }
    }

    @Override
    public void onSwitchChanged(Switch switchView, boolean desiredState) {
        if(desiredState && ! mNfcAdapter.isEnabled()) {
            Toast.makeText(getActivity(), R.string.android_beam_disabled_summary, Toast.LENGTH_SHORT).show();
            android.util.Log.d("AndroidBeam", "onSwitchChanged ignore change for nfc is off");
            mSwitchBar.setChecked(false);
            return;
        }
        /* MODIFIED-END by Ji.Chen,BUG-3004922*/
        boolean success = false;
        mSwitchBar.setEnabled(false);
        if (desiredState) {
            success = mNfcAdapter.enableNdefPush();
        } else {
            success = mNfcAdapter.disableNdefPush();
        }
        if (success) {
            mSwitchBar.setChecked(desiredState);
        }
        mSwitchBar.setEnabled(true);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.NFC_BEAM;
    }

    /* MODIFIED-BEGIN by Ji.Chen, 2016-09-29,BUG-3004922*/
    private void handleNfcStateChanged(int newState) {
        switch (newState) {
        case NfcAdapter.STATE_OFF:
            mSwitchBar.setEnabled(!mBeamDisallowedByBase);
            mSwitchBar.removeOnSwitchChangeListener(this);
            mSwitchBar.setChecked(false);
            mSwitchBar.addOnSwitchChangeListener(this);
            break;
        case NfcAdapter.STATE_ON:
            mSwitchBar.setEnabled(!mBeamDisallowedByBase);
            mSwitchBar.removeOnSwitchChangeListener(this);
            mSwitchBar.setChecked(!mBeamDisallowedByBase && mNfcAdapter.isNdefPushEnabled());
            mSwitchBar.addOnSwitchChangeListener(this);
            break;
        case NfcAdapter.STATE_TURNING_ON:
            mSwitchBar.setEnabled(false);
            break;
        case NfcAdapter.STATE_TURNING_OFF:
            mSwitchBar.setEnabled(false);
            break;
        }
    }
    /* MODIFIED-END by Ji.Chen,BUG-3004922*/
}

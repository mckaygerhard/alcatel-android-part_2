/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
/* MODIFIED-BEGIN by beibei.yang, 2016-11-02,BUG-3304204*/
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.ContactsContract;
/* MODIFIED-END by beibei.yang,BUG-3304204*/
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
/* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
import com.android.settings.deviceinfo.boomkey.adapter.BoomAlternativeFuncShortcutsAdapter;
import com.android.settings.deviceinfo.boomkey.adapter.BoomAlternativeFuncShortcutsAdapter.FuncAddListener;
import com.android.settings.deviceinfo.boomkey.adapter.BoomDragAdapter;
import com.android.settings.deviceinfo.boomkey.adapter.BoomDragAdapter.FuncEditItemListener;
import com.android.settings.deviceinfo.boomkey.view.BoomAlternativeShortcutslistView;
import com.android.settings.deviceinfo.boomkey.view.BoomDragSortListView;
import com.android.settings.deviceinfo.boomkey.view.BoomDragSortListView.RemoveListener;
import com.android.settings.deviceinfo.boomkey.BoomFuncUtil;
/* MODIFIED-BEGIN by chao.hu, 2016-11-18,BUG-3472599*/
import android.database.Cursor;
/* MODIFIED-END by chao.hu,BUG-3472599*/

import java.util.ArrayList;

public class BoomFuncSettings extends SettingsPreferenceFragment  {

    private static final String TAG = "BoomFuncSettings";
    private Context mContext;
    private ContentResolver cr;
    private ArrayList<String> choosedLists;
    private ArrayList<String> alternativeList;
    private ScrollView func_scrollview;
    private TextView func_illustrationtv;
    private BoomDragSortListView dragSortlistView;
    private TextView zeroshowingtv;
    private BoomDragAdapter dragAdapter;
    private BoomAlternativeShortcutslistView alternativeShortcutslistview;
    private BoomAlternativeFuncShortcutsAdapter alternativeShortcutsAdapter;
    /* MODIFIED-END by beibei.yang,BUG-2830318*/
    private static final String SP_NAME = "funcSettings_preferences";
    private static final String SHOWREMOVEFUNCDIALOG = "showRemoveFuncDialog";
    private boolean mChecked;
    private SharedPreferences mSharedPreferences;
    private boolean isShowRemoveFuncDialog;

    private static int MIDDLE_ITEM_NUM = 3;
    private Toast showToast;
    private PackageIntentReceiver mPackageIntentReceiver = null;
    private Handler mHandler = new Handler();
    private boolean mIsSaved = false;

    private class PackageIntentReceiver extends BroadcastReceiver {

        void registerReceiver() {
            IntentFilter filter = new IntentFilter(
                    BoomFuncUtil.UNINSTALLACTION); // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
            mContext.registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String actionStr = intent.getAction();
            Log.i(TAG, "onReceive actionStr=" + actionStr);
            if (dragSortlistView == null) {
                Log.i(TAG, "onReceive: dragSortlistView has not been initialized yet, just return!");
                return;
            }
            if (null == actionStr) {
                return;
            }
            if (BoomFuncUtil.UNINSTALLACTION.equals(actionStr)) { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
                initData();
                setAdapterAndListenners();
            }
        }

        void unregisterReceiver() {
            mContext.unregisterReceiver(this);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK
                && requestCode == BoomFuncUtil.GOFuncAppsListActivity) { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String pkgName = bundle.getString("packageName");
                if (!TextUtils.isEmpty(pkgName)) {
                    choosedLists.add(pkgName);
                    dragAdapter.notifyDataSetChanged();
                    isChoosedListsEmpty();
                    func_scrollview.fullScroll(ScrollView.FOCUS_UP);
                }
            }

        }
        //Mod-Begain by chao.hu for Defect:3472599
        else if(resultCode == Activity.RESULT_OK
                && requestCode == BoomFuncUtil.PICK_CONTACT_REQUEST) {
            Uri contactUri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(contactUri, projection, null, null, null);
                if(cursor != null && cursor.moveToFirst()) {
                    int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    String number = cursor.getString(column);
                    Settings.System.putString(cr, "contactNumber", number);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (null != cursor) {
                    cursor.close();
                }
            }
        }
        //Mod-End by chao.hu for Defect:3472599
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        cr = getContentResolver();
        mPackageIntentReceiver = new PackageIntentReceiver();
        mPackageIntentReceiver.registerReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.boom_funcsettings, container, false);
        initView(result);
        return result;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        final SettingsActivity activity = (SettingsActivity) getActivity();
        initData();
        setAdapterAndListenners();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!mIsSaved) {
            saveFuncSettingData();
        }
        mIsSaved = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mPackageIntentReceiver) {
            mPackageIntentReceiver.unregisterReceiver();
            mPackageIntentReceiver = null;
        }
    }

    private void initView(View hostView) {
        func_scrollview = (ScrollView) hostView.findViewById(R.id.func_scrollview);
        func_illustrationtv = (TextView) hostView.findViewById(R.id.func_illustrationtv);
        mSharedPreferences = mContext.getSharedPreferences(SP_NAME,
                Context.MODE_PRIVATE);
        isShowRemoveFuncDialog = mSharedPreferences.getBoolean(
                SHOWREMOVEFUNCDIALOG, true);
        mChecked = true;//MOD by chao.hu for bug 3647375
        if (mChecked) {
            func_scrollview.setVisibility(View.VISIBLE);
            func_illustrationtv.setVisibility(View.GONE);
        } else {
            func_scrollview.setVisibility(View.GONE);
            func_illustrationtv.setVisibility(View.VISIBLE);
        }
        /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
        dragSortlistView = (BoomDragSortListView) hostView.findViewById(android.R.id.list);
        zeroshowingtv = (TextView) hostView.findViewById(R.id.zeroshowingtv);
        alternativeShortcutslistview = (BoomAlternativeShortcutslistView) hostView
                .findViewById(R.id.alternativeShortcutslistview);
    }

    private void setAdapterAndListenners() {
        dragAdapter = new BoomDragAdapter(mContext, choosedLists);
        dragSortlistView.setAdapter(dragAdapter);
        dragSortlistView.setDropListener(onDrop);
        dragSortlistView.setRemoveListener(onRemove);
        dragAdapter.setFuncEditItemListener(funcEditItemListener);
        alternativeShortcutsAdapter = new BoomAlternativeFuncShortcutsAdapter(mContext,
                alternativeList);
        alternativeShortcutslistview.setAdapter(alternativeShortcutsAdapter);
        alternativeShortcutsAdapter.setFuncAddListener(onAddFuncItemListener);
        isChoosedListsEmpty();
    }

    private BoomDragSortListView.DropListener onDrop = new BoomDragSortListView.DropListener() {
    /* MODIFIED-END by beibei.yang,BUG-2830318*/
        @Override
        public void drop(int from, int to) {
            if (from != to) {
                String item = dragAdapter.getItem(from);
                choosedLists.remove(item);
                choosedLists.add(to, item);
                dragAdapter.notifyDataSetChanged();
                dragSortlistView.moveCheckState(from, to);
            }
        }
    };

    private FuncEditItemListener funcEditItemListener = new FuncEditItemListener() {

        @Override
        public void editFuncItem(int goType) {
            if (goType == com.android.internal.R.id.func_dial) { // MODIFIED by beibei.yang, 2016-11-03,BUG-3304204
                Intent mIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                mIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(mIntent, BoomFuncUtil.PICK_CONTACT_REQUEST);
                /* MODIFIED-END by beibei.yang,BUG-3304204*/
            }
        }

    };

    private RemoveListener onRemove = new BoomDragSortListView.RemoveListener() { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
        @Override
        public void remove(int which, boolean flag) {

            String item = dragAdapter.getItem(which);
            if (item != null) {
                int id = getResources().getIdentifier("id/" + item, null, "android");
                if (id != 0 || item.equals("func_screenshot")) { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
                    if (id == com.android.internal.R.id.func_settings && flag) {
                        showRemoveFuncSettingDialog(which);
                    } else {
                        choosedLists.remove(item);
                        dragAdapter.notifyDataSetChanged();
                        alternativeList.add(0, item);
                        alternativeShortcutsAdapter.notifyDataSetChanged();
                    }
                } else {
                    choosedLists.remove(item);
                    dragAdapter.notifyDataSetChanged();
                }
            }
            isChoosedListsEmpty();
        }
    };

    private FuncAddListener onAddFuncItemListener = new FuncAddListener() {

        @Override
        public void addFuncItem(int position) {
            if (choosedLists.size() < 3) {
                String item = alternativeShortcutsAdapter.getItem(position);
                choosedLists.add(item);
                dragAdapter.notifyDataSetChanged();
                alternativeList.remove(item);
                alternativeShortcutsAdapter.notifyDataSetChanged();
                isChoosedListsEmpty();
            } else {
                if (showToast != null) {
                    showToast.cancel();
                }
                showToast = Toast.makeText(mContext, R.string.func_AlternativeShortcuts_warning_boom,
                        Toast.LENGTH_SHORT);
                showToast.show();
            }

        }
    };

    private void initData() {
        String choosed = Settings.System.getStringForUser(mContext.getContentResolver(),
                Settings.System.BOOM_KEY_FUNC_INFO, UserHandle.USER_CURRENT);
        if (TextUtils.isEmpty(choosed)) {
            choosed = getResources().getString(R.string.def_boom_func_list_default);
        }

        /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
        choosedLists = BoomFuncUtil.stringToList(choosed);

        String alter = mSharedPreferences.getString("boom_alternative_list", null); // MODIFIED by beibei.yang, 2016-11-02,BUG-3294209
        if (TextUtils.isEmpty(alter)) {
            alternativeList = new ArrayList<>();
            String funcList = getResources().getString(R.string.def_boom_func_list_total); // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
            String[] tmp = funcList.split(BoomFuncUtil.DELIMITER);
            for (String item : tmp) {
                if (choosedLists.contains(item)) continue;
                alternativeList.add(item);
            }
        } else {
            alternativeList = BoomFuncUtil.stringToList(alter);
            /* MODIFIED-END by beibei.yang,BUG-2830318*/
        }
    }

    public void isChoosedListsEmpty() {
        if (choosedLists != null && choosedLists.size() <= 0) {
            zeroshowingtv.setVisibility(View.VISIBLE);
            dragSortlistView.setVisibility(View.GONE);
        } else {
            zeroshowingtv.setVisibility(View.GONE);
            dragSortlistView.setVisibility(View.VISIBLE);
        }
        mHandler.post(() -> {
            if (func_scrollview != null)
                func_scrollview.scrollTo(0, 0);
        });
    }

    private void saveFuncSettingData() {
         if (choosedLists != null && !choosedLists.isEmpty()) {
            /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
            String choosed_list = BoomFuncUtil.listToString(choosedLists);
            Settings.System.putString(cr, Settings.System.BOOM_KEY_FUNC_INFO, choosed_list);
         } else {
            Settings.System.putString(cr, Settings.System.BOOM_KEY_FUNC_INFO, BoomFuncUtil.DELIMITER);//store delimiter for empty
         }
         if (alternativeList != null && !alternativeList.isEmpty()) {
             SharedPreferences.Editor editor = mSharedPreferences.edit();
             editor.putString("boom_alternative_list", BoomFuncUtil.listToString(alternativeList)); // MODIFIED by beibei.yang, 2016-11-02,BUG-3294209
             /* MODIFIED-END by beibei.yang,BUG-2830318*/
             editor.apply();
         }

    }

    public void showRemoveFuncSettingDialog(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true /* cancelable */);
        builder.setMessage(R.string.func_showremovedialogmsg);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            isShowRemoveFuncDialog = false;
            dragSortlistView.removeItem(position);
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putBoolean(SHOWREMOVEFUNCDIALOG, false);
            editor.commit();
        });
        builder.create().show();
    }


    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.LOCKSCREEN;
    }

}

/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey;

import android.app.Activity;
/* MODIFIED-BEGIN by xiang.miao-nb, 2016-11-07,BUG-3311756*/
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.ComponentName; // MODIFIED by beibei.yang, 2016-11-03,BUG-3311545
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Contacts;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.IWindowManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.internal.app.AssistUtils;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.R;

import java.util.ArrayList;
import java.util.Arrays;
/* MODIFIED-END by xiang.miao-nb,BUG-3311756*/


public class BoomUiActivity extends Activity {
    ImageView twoUp ;
    ImageView twoDown ;
    ImageView threeUp ;
    ImageView threeDown ;
    ImageView threeCenter ;
    ImageView close ;
    RelativeLayout uiArea ;
    String appInfo = null ;
    ArrayList<String> appList = null ;
    String packageName1;
    String packageName2;
    String packageName3;
    PackageManager pm ;
    boolean isAppMode = false ;
    private Resources res;
    /* MODIFIED-BEGIN by xiang.miao-nb, 2016-11-07,BUG-3311756*/
    private LockPatternUtils mLockPatternUtils;
    private AssistUtils mAssistUtils;
    protected IWindowManager mWindowManagerService;
    /* MODIFIED-END by xiang.miao-nb,BUG-3311756*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("boomui", "onCreate ");
        // TODO Auto-generated method stub
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER); // MODIFIED by beibei.yang, 2016-11-07,BUG-3312000
        setContentView(R.layout.layout_boom_ui);
        appInfo = getIntent().getStringExtra("app_info");
        isAppMode = getIntent().getBooleanExtra("boom_is_app_mode", false);
        if (appInfo != null)  appList = new ArrayList<>(Arrays.asList(appInfo.split(";")));
        pm = getPackageManager();
        res = getResources();
        Log.d("boomui", "appList = "+appList +" size ="+appList.size());
        /* MODIFIED-BEGIN by xiang.miao-nb, 2016-11-07,BUG-3311756*/
        mLockPatternUtils = new LockPatternUtils(getApplicationContext());
        mAssistUtils = new AssistUtils(getApplicationContext());
        mWindowManagerService = WindowManagerGlobal.getWindowManagerService();
        /* MODIFIED-END by xiang.miao-nb,BUG-3311756*/
        initView();
        /* MODIFIED-BEGIN by beibei.yang, 2016-11-09,BUG-3403631*/
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenOffReceiver,filter);

    }
    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                Log.d("screen","finish");
                finish();
            }
        }
    };
    /* MODIFIED-END by beibei.yang,BUG-3403631*/
    private void initView() {
        uiArea = (RelativeLayout)findViewById(R.id.ui_area);
        twoUp = (ImageView)findViewById(R.id.two_up);
        twoDown = (ImageView)findViewById(R.id.two_down);
        threeUp = (ImageView)findViewById(R.id.three_up);
        threeDown = (ImageView)findViewById(R.id.three_down);
        threeCenter = (ImageView)findViewById(R.id.three_center);
        close = (ImageView)findViewById(R.id.close);
        close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        uiArea.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        if (appList != null ) {
            if (appList.size() == 2) {
                twoUp.setVisibility(View.VISIBLE);
                twoDown.setVisibility(View.VISIBLE);
                threeUp.setVisibility(View.GONE);
                threeDown.setVisibility(View.GONE);
                threeCenter.setVisibility(View.GONE);
                packageName1 = appList.get(0);
                packageName2 = appList.get(1);
                if (isAppMode) {
                    try {
                        twoUp.setBackground(pm.getApplicationIcon(packageName1));
                        twoDown.setBackground(pm.getApplicationIcon(packageName2));
                    } catch (NameNotFoundException e) {
                        // TODO: handle exception
                    }
                } else {
                    twoUp.setImageResource(getImageResId(packageName1));
                    twoDown.setImageResource(getImageResId(packageName2));
                    twoUp.setTag(packageName1);
                    twoDown.setTag(packageName2);
                }
                setClickListener(twoUp, packageName1);
                setClickListener(twoDown, packageName2);
            }else if (appList.size() == 3) {
                threeUp.setVisibility(View.VISIBLE);
                threeDown.setVisibility(View.VISIBLE);
                threeCenter.setVisibility(View.VISIBLE);
                twoUp.setVisibility(View.GONE);
                twoDown.setVisibility(View.GONE);
                packageName1 = appList.get(0);
                packageName2 = appList.get(1);
                packageName3 = appList.get(2);
                if (isAppMode) {
                    try {
                        threeUp.setBackground(pm.getApplicationIcon(packageName1));
                        threeDown.setBackground(pm.getApplicationIcon(packageName2));
                        threeCenter.setBackground(pm.getApplicationIcon(packageName3));
                    } catch (NameNotFoundException e) {
                        // TODO: handle exception
                    }
                } else {
                    threeUp.setImageResource(getImageResId(packageName1));
                    threeDown.setImageResource(getImageResId(packageName2));
                    threeCenter.setImageResource(getImageResId(packageName3));
                    threeUp.setTag(packageName1);
                    threeDown.setTag(packageName2);
                    threeCenter.setTag(packageName3);
                }
                setClickListener(threeUp, packageName1);
                setClickListener(threeDown, packageName2);
                setClickListener(threeCenter, packageName3);
            }
        }
    }
    private void setClickListener (ImageView view ,String appPackageName) {
        final String packageName = appPackageName ;
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (isAppMode) {
                    Intent targetIntent = pm.getLaunchIntentForPackage(packageName);
                    startActivityAsUser(targetIntent,UserHandle.CURRENT);
                    finish(); // MODIFIED by beibei.yang, 2016-11-09,BUG-3403631
                }else {
                    String tag = (String)v.getTag();
                    handleFunEvent(tag);
                }
            }
        });
    }

    private int getImageResId(String item) {
        return res.getIdentifier("drawable/" + item, null, getPackageName());
    }
    private void handleFunEvent (String tag) {
        boolean secure = mLockPatternUtils.isSecure(UserHandle.myUserId()); // MODIFIED by xiang.miao-nb, 2016-11-07,BUG-3311756
        Intent targetIntent = new Intent() ;
        if (tag.equals("func_torch")) {
        	sendBroadcast(new Intent("Torch_func"));
        	return ;
        }
        /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
        if (tag.equals("func_screenshot")) {
            Intent intent = new Intent("action_boom_screenshot");
            sendBroadcast(intent);
            /*Add-Begain by chao.hu for defect:3418642*/
            finish();
            /*Add-End by chao.hu for defect:3418642*/
            return ;
        }
        /* MODIFIED-END by beibei.yang,BUG-2830318*/
        switch (tag) {
        case "func_voice_search":
            /* MODIFIED-BEGIN by xiang.miao-nb, 2016-11-07,BUG-3311756*/
            if (secure) {
                AsyncTask.execute(() -> {
                    mAssistUtils.launchVoiceAssistFromKeyguard();
                    try {
                        mWindowManagerService.overridePendingAppTransition(null, 0, 0, null);
                    } catch (RemoteException e) {
                        Log.w("boomui", "Error overriding app transition: " + e);
                    }
                });
                //return; //Mod by chao.hu for Defect:3445700
            }
            /* MODIFIED-END by xiang.miao-nb,BUG-3311756*/
            targetIntent.setAction(Intent.ACTION_VOICE_ASSIST);
            break;

        case "func_call":
            targetIntent.setAction(Intent.ACTION_DIAL);
            break;

        case "func_camera":
            /* MODIFIED-BEGIN by xiang.miao-nb, 2016-11-07,BUG-3311756*/
            if (secure) {
                targetIntent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE)
                        .addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            } else {
                targetIntent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
            }
            /* MODIFIED-END by xiang.miao-nb,BUG-3311756*/
            break ;

        case "func_dial":
            String number = Settings.System.getString(
                    this.getContentResolver(), "contactNumber");
            if(number == null || number.equals("")) {
                targetIntent.putExtra("isNullNumber", true);
            } else {
                targetIntent.putExtra("isNullNumber", false);
                targetIntent.putExtra("number", number);
            }
            targetIntent.setClassName("com.android.systemui",
                    "com.android.systemui.statusbar.phone.CallContactActivity");
            break ;

        case "func_recognise_song":
            targetIntent.setClassName("com.shazam.android",
                    "com.shazam.android.activities.MainActivity");
                 targetIntent.setAction("com.shazam.android.intent.actions.START_TAGGING");
            break ;

        case "func_timer":
            targetIntent.setAction("android.intent.action.SET_TIMER");
            break;

        case "func_selfie":
            /* MODIFIED-BEGIN by xiang.miao-nb, 2016-11-07,BUG-3311756*/
            if (secure) {
                targetIntent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE)
                        .addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                targetIntent.setClassName("com.tct.camera", "com.android.camera.SecureCameraActivity");
                targetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                targetIntent.setAction(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE);
                targetIntent.putExtra("func_selfie",true);
            } else {
                targetIntent.setAction("com.tct.camera.STARTFRONTCAMERA");
            }
            /* MODIFIED-END by xiang.miao-nb,BUG-3311756*/
            break;
        case "func_music":
            /* MODIFIED-BEGIN by beibei.yang, 2016-11-03,BUG-3311545*/
            ComponentName componet = ComponentName.unflattenFromString(
                    "com.google.android.music/com.google.android.music.VoiceActionsActivity");
             targetIntent.setAction("android.media.action.MEDIA_PLAY_FROM_SEARCH");
             targetIntent.setComponent(componet);
             /* MODIFIED-END by beibei.yang,BUG-3311545*/
            break;
        case "func_message_new":
            Uri smsToUri = Uri.parse("smsto:");
            targetIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
            targetIntent.putExtra("sms_body", "");
            break ;

        case "func_email_new":
            Uri uri = Uri.parse("mailto:");
            targetIntent = new Intent(Intent.ACTION_SENDTO, uri);
            break ;

        case "func_contact_new":
            targetIntent.setAction(Contacts.Intents.Insert.ACTION);
            targetIntent.setType(Contacts.People.CONTENT_TYPE);
            break ;

        case "func_event_new":
            targetIntent.setAction("com.google.android.calendar.EVENT_EDIT");
            targetIntent.setClassName("com.google.android.calendar", "com.android.calendar.AllInOneActivity"); // MODIFIED by beibei.yang, 2016-11-03,BUG-3311512
            targetIntent.putExtra("createEditSource", "fab");
            targetIntent.putExtra("is_find_time_promo", false);
            break;

        case "func_sound_record":
            targetIntent.setClassName("com.tct.soundrecorder", "com.tct.soundrecorder.SoundRecorder");
            targetIntent.putExtra("dowhat", "record");
            targetIntent.setAction(Intent.ACTION_MAIN);
            break;

        case "func_navigate":
            targetIntent.setClassName("com.google.android.apps.maps",
                    "com.google.android.maps.driveabout.app.DestinationActivity");
            break;

        case "func_calculator" :
            targetIntent.setClassName("com.tct.calculator",
                    "com.tct.calculator.Calculator");
            targetIntent.putExtra("showByLocked", true);
            targetIntent.putExtra("IsSecure", true); // MODIFIED by xiang.miao-nb, 2016-11-07,BUG-3311756
            break ;
        default:
            break;
        }
        /* MODIFIED-BEGIN by beibei.yang, 2016-11-09,BUG-3403631*/
        if (targetIntent != null) {
            startActivityAsUser(targetIntent, UserHandle.CURRENT);
            finish();
        }
        /* MODIFIED-END by beibei.yang,BUG-3403631*/
    }

    /* MODIFIED-BEGIN by beibei.yang, 2016-11-03,BUG-2830318*/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }
    /* MODIFIED-BEGIN by beibei.yang, 2016-11-09,BUG-3403631*/
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        if (mScreenOffReceiver != null) unregisterReceiver(mScreenOffReceiver);
        super.onDestroy();
    }
    /* MODIFIED-END by beibei.yang,BUG-3403631*/
}

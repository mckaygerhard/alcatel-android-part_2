/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.aod;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Switch;
import android.support.v7.preference.Preference;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.widget.SwitchBar;

import static android.provider.Settings.System.AOD_BG_ENABLED;


public class AlwaysOnDisplaySetBackgroundPreference extends
        SettingsPreferenceFragment implements
        Preference.OnPreferenceChangeListener {

    private SwitchBar mSwitchBar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.always_on_display_set_background);
    }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.QS_BLUETOOTH;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initSwitchBar();
    }

    private void initSwitchBar() {
        final SettingsActivity sa = (SettingsActivity) getActivity();
        mSwitchBar = sa.getSwitchBar();
        mSwitchBar
                .addOnSwitchChangeListener(new SwitchBar.OnSwitchChangeListener() {

                    @Override
                    public void onSwitchChanged(Switch switchView,
                            boolean isChecked) {
                        // TODO Auto-generated method stub
                        Settings.System.putInt(getContentResolver(),
                                AOD_BG_ENABLED, isChecked ? 1 : 0);
                    }

                });
        mSwitchBar.show();
        // init switchbar checked
        if (mSwitchBar != null) {
            int value = Settings.System.getInt(getContentResolver(),
                    AOD_BG_ENABLED, 0); // MODIFIED by song.huan, 2016-10-11,BUG-3000498
            mSwitchBar.setChecked(value == 1);
            Log.d("edward", "AOD_BG_ENABLED value--->" + value);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        // TODO Auto-generated method stub
        return false;
    }

}

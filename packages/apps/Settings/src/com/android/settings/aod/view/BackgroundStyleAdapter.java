/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.aod.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.AbsListView; // MODIFIED by song.huan, 2016-10-13,BUG-3000498
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.android.settings.R;

public class BackgroundStyleAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    private int[] mBackgroundStyles;
    private int mFocusPosition = 0;

    public BackgroundStyleAdapter(Context context, int[] backgroundStyles){
        mContext = context;
        mBackgroundStyles = backgroundStyles;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mBackgroundStyles.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        /* MODIFIED-BEGIN by song.huan, 2016-10-13,BUG-3000498*/
        final View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.background_select_item, parent, false);
        } else {
            view = convertView;
        }
        //get the width of the view and make the height is equal to the width
        view.post(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                int width = view.getWidth();
                int height = width;
                view.setLayoutParams(new AbsListView.LayoutParams(width, height));
            }

        });
        /* MODIFIED-END by song.huan,BUG-3000498*/

        bindView(position, view, mBackgroundStyles[position]);
        return view;
    }

    private void bindView(int position, View view, int drawable) {
        // TODO Auto-generated method stub
        LinearLayout mBackgroundStyle = (LinearLayout) view
                .findViewById(R.id.background_style);
        mBackgroundStyle.setBackground(mContext.getDrawable(drawable));
        LinearLayout mFocusedBackground = (LinearLayout) view
                .findViewById(R.id.bg_focused_background);

        if (position == mFocusPosition) {
            mFocusedBackground.setVisibility(View.VISIBLE);
        } else {
            mFocusedBackground.setVisibility(View.GONE);
        }
    }

    public void setFocusPosition(int i) {
        mFocusPosition = i;
        notifyDataSetChanged();
    }
}

/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.aod;

import android.R.color;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static android.provider.Settings.System.AOD_BG_STYLE;
import static android.provider.Settings.System.AOD_COLCK_STYLE;
import static android.provider.Settings.System.AOD_SIGNATURE_STYLE;
import static android.provider.Settings.System.AOD_CLOCK_ENABLED;
import static android.provider.Settings.System.AOD_BG_ENABLED;
import static android.provider.Settings.System.AOD_SIGNATURE_ENABLED;

public class AlwaysDisplayPreference extends Preference {

    private Context mContext;
    private TextView signature;
    private ImageView clock;
    private LinearLayout aodConfigPreview;

    private static final int INVISIBLE = 0;

    private int[] mBackgroundStyle = { R.drawable.always_on_a,
            R.drawable.always_on_b, R.drawable.always_on_c,
            R.drawable.always_on_d,R.drawable.always_on_e,R.drawable.always_on_f };

    private int[] mClockStyle = { R.drawable.always_on_1,
            R.drawable.always_on_2, R.drawable.always_on_3,
            R.drawable.always_on_4, R.drawable.always_on_5,
            R.drawable.always_on_6, R.drawable.always_on_7 };

    public AlwaysDisplayPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutResource(R.layout.aod_configuration);
        mContext = context;

    }


    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);
        // TODO Auto-generated method stub

        aodConfigPreview = (LinearLayout) view.findViewById(R.id.aod_configuration_preview);
        signature = (TextView) view.findViewById(R.id.pre_signature);
        clock = (ImageView) view.findViewById(R.id.pre_clock);

        initView();
    }

    //init preview Aod style based on the style which use chose
    public void initView(){

        int clockStyle = Settings.System.getInt(mContext.getContentResolver(), AOD_COLCK_STYLE, 0);

        int backgroundStyle = Settings.System.getInt(mContext.getContentResolver(), AOD_BG_STYLE, 0);

        String signatureStyle = Settings.System.getString(mContext.getContentResolver(), AOD_SIGNATURE_STYLE);


        //set clock style
        /* MODIFIED-BEGIN by song.huan, 2016-11-02,BUG-3284288*/
        if(clock != null) {
            clock.setImageResource(mClockStyle[clockStyle]);
        }

        //set background style
        if(aodConfigPreview != null) {
            aodConfigPreview.setBackgroundResource(mBackgroundStyle[backgroundStyle]);
        }
        /* MODIFIED-END by song.huan,BUG-3284288*/

        //set signature
        if(signatureStyle != null){
            signature.setText(signatureStyle);
        }


        setWidgetsVisibility();

    }

    public void setWidgetsVisibility(){

        /* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
        int signatureVisibility = Settings.System.getInt(mContext.getContentResolver(), AOD_SIGNATURE_ENABLED, 0);

        int clockVisibility = Settings.System.getInt(mContext.getContentResolver(), AOD_CLOCK_ENABLED, 1);

        int backgroundVisibility = Settings.System.getInt(mContext.getContentResolver(), AOD_BG_ENABLED, 0);
        /* MODIFIED-END by song.huan,BUG-3000498*/

        /* MODIFIED-BEGIN by song.huan, 2016-11-02,BUG-3284288*/
        if(signature != null) {
            signature.setVisibility(signatureVisibility==INVISIBLE ? View.INVISIBLE : View.VISIBLE);
        }
        if(clock != null) {
            clock.setVisibility(clockVisibility==INVISIBLE ? View.INVISIBLE : View.VISIBLE);
        }
        if(backgroundVisibility == INVISIBLE && aodConfigPreview != null){
        /* MODIFIED-END by song.huan,BUG-3284288*/
            aodConfigPreview.setBackgroundColor(color.black);
     }


    }


}

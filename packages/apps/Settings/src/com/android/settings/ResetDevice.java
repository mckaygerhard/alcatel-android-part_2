/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.backup.BackupManager;
import android.app.IActivityManager;
import android.app.ActivityManagerNative;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Locale;
import com.android.internal.logging.MetricsProto.MetricsEvent;

/**
 * Created by miaoxiang on 16-10-9.
 */

public class ResetDevice extends OptionsMenuFragment implements DialogInterface.OnClickListener{

    private static final String TAG = "ResetDevice";
    private static final String INTENT_RESET = "android.intent.action.LAUNCH_DEVICE_RESET";
    private static final int KEYGUARD_REQUEST = 55;

    private View mContentView;
    private Button mInitiateButton;

    private boolean runKeyguardConfirmation(int request) {
        Resources res = getActivity().getResources();
        return new ChooseLockSettingsHelper(getActivity(), this).launchConfirmationActivity(
                request, res.getText(R.string.reset_device_title));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != KEYGUARD_REQUEST) {
            return;
        }

        // If the user entered a valid keyguard trace, present the final
        // confirmation prompt; otherwise, go back to the initial state.
        if (resultCode == Activity.RESULT_OK) {
            showFinalConfirmation();
        } else {
            establishInitialState();
        }
    }

    private final Button.OnClickListener mInitiateListener = new Button.OnClickListener() {

        public void onClick(View v) {
            if (!runKeyguardConfirmation(KEYGUARD_REQUEST)) {
                showFinalConfirmation();
            }
        }
    };

    BroadcastReceiver resultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            // TODO Auto-generated method stub
            int count = this.getResultCode();
            String resultData = this.getResultData();

            Log.i(TAG, "total " + count + "app done device reset!");

            if (resultData.contains("language")) {
                //Reset language at here after disappear progress dialog
                //In order to avoid exception(view not attached to window manager)
                try {
                    String language = SystemProperties.get("ro.product.locale.language", "en");
                    String country = SystemProperties.get("ro.product.locale.region", "US");
                    IActivityManager am = ActivityManagerNative.getDefault();
                    Configuration config = am.getConfiguration();
                    config.locale = new Locale(language, country);
                    config.userSetLocale = true;
                    am.updateConfiguration(config);
                    // Trigger the dirty bit for the Settings Provider.
                    BackupManager.dataChanged("com.android.providers.settings");
                } catch (RemoteException e) {
                    Log.i(TAG, "Reset language exception: "+e.toString());
                }
            }

            // The reboot call is blocking, so we need to do it on another thread.
            Thread thr = new Thread("Reboot") {
                @Override
                public void run() {
                    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    pm.reboot(null);
                }
            };
            thr.start();
        }
    };

    private void showFinalConfirmation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.device_reset_confirm_dialog_message);
        builder.setPositiveButton(R.string.reset_confirm_dialog_positive_button, ResetDevice.this); // MODIFIED by xiang.miao-nb, 2016-10-14,BUG-3043749
        builder.setNegativeButton(android.R.string.cancel, ResetDevice.this);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            Log.d(TAG, "OK");
            sendBroadcast();
        }
    }

    private void sendBroadcast() {
        Intent intent = new Intent();
        intent.setAction(INTENT_RESET);
        this.getActivity().sendOrderedBroadcast(intent, null, resultReceiver,
                null, 0, null, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContentView = inflater.inflate(R.layout.reset_device, null);

        establishInitialState();
        return mContentView;
    }

    private void establishInitialState() {
        mInitiateButton = (Button) mContentView.findViewById(R.id.initiate_reset_device);
        mInitiateButton.setOnClickListener(mInitiateListener);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.RESET_DEVICE;
    }
}

/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.fingerprint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settings.search.SearchIndexableRaw;
import com.android.settings.search.Indexable.SearchIndexProvider;

import android.provider.SearchIndexableResource;

import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Utils;
import com.android.settings.search.Indexable;
import com.android.settingslib.wifi.AccessPoint;
import com.android.settingslib.wifi.WifiTracker;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Settings;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceGroup;
import android.util.Log;

import com.android.internal.logging.MetricsProto.MetricsEvent;

public class FingerprintSensorSettings extends SettingsPreferenceFragment implements Preference.OnPreferenceChangeListener, Indexable{

    private UserManager mUm;
    private static final int MY_USER_ID = UserHandle.myUserId();
    private static final String KEY_FINGERPRINT_PREFERENCE = "fingerprint_preference_screen";
    private static final String KEY_FINGERPRINT_BACK_TO_HOME = "fingerprint_use_as_home";
    private static final String KEY_FINGERPRINT_ZOOM_CAMERA = "fingerprint_zoom";

    /* MODIFIED-BEGIN by jianguang.sun, 2016-10-19,BUG-2740734*/
    private static final String KEY_FINGERPRINT_TAKE_PHOTO = "fingerprint_take_photo";
    private static final String KEY_FINGERPRINT_NOTICATION_PANEL = "fingerprint_notification_panel";
    private static final String KEY_FINGERPRINT_SWITCH_PHOTO = "fingerprint_switch_photo";
    private SwitchPreference mTakePhoto;
    private SwitchPreference mNoticationPanel;
    private SwitchPreference mSwitchPhoto;
    /* MODIFIED-END by jianguang.sun,BUG-2740734*/
    private SwitchPreference mBackToHome;
    private SwitchPreference mZoomCamera;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mUm = UserManager.get(getActivity());
        createPreferenceHierarchy();

    }

    @Override
    public void onResume() {
        super.onResume();
        createPreferenceHierarchy();
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.FINGERPRINT;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean result = true;
        final String key = preference.getKey();
        if(KEY_FINGERPRINT_BACK_TO_HOME.equals(key)){
            if ((Boolean) (newValue)) {
                mBackToHome.setChecked(true);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_BACK_TO_HOME, 1);
            }else {
                mBackToHome.setChecked(false);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_BACK_TO_HOME, 0);
            }

        /* MODIFIED-BEGIN by jianguang.sun, 2016-10-19,BUG-2740734*/
        }else if (KEY_FINGERPRINT_TAKE_PHOTO.equals(key)) {
            if ((Boolean) (newValue)) {
                mTakePhoto.setChecked(true);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_TAKE_PHOTO, 1);
            }else {
                mTakePhoto.setChecked(false);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_TAKE_PHOTO, 0);
            }
        }else if (KEY_FINGERPRINT_NOTICATION_PANEL.equals(key)) {
            if ((Boolean) (newValue)) {
                mNoticationPanel.setChecked(true);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_NOTICATION_PANEL, 1);
            }else {
                mNoticationPanel.setChecked(false);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_NOTICATION_PANEL, 0);
            }
        }else if (KEY_FINGERPRINT_SWITCH_PHOTO.equals(key)) {
            if ((Boolean) (newValue)) {
                mSwitchPhoto.setChecked(true);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_SWITCH_PHOTO, 1);
            }else {
                mSwitchPhoto.setChecked(false);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_SWITCH_PHOTO, 0);
                                /* MODIFIED-END by jianguang.sun,BUG-2740734*/
            }
        }else if (KEY_FINGERPRINT_ZOOM_CAMERA.equals(key)) {
            if ((Boolean) (newValue)) {
                mZoomCamera.setChecked(true);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_ZOOM_CAMERA, 1);
            }else {
                mZoomCamera.setChecked(false);
                Settings.System.putInt(getContentResolver(),
                                Settings.System.TCT_FINGERPRINT_ZOOM_CAMERA, 0);
            }
        }
        return false;
    }

    private void createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceScreen();
        if (root != null) {
            root.removeAll();
        }
        addPreferencesFromResource(R.xml.fingerprint_sensor_settings);
        root = getPreferenceScreen();
        maybeAddFingerprintPreference(root, MY_USER_ID);
        mBackToHome = (SwitchPreference) findPreference(KEY_FINGERPRINT_BACK_TO_HOME);
        /* MODIFIED-BEGIN by jianguang.sun, 2016-10-19,BUG-2740734*/
        mTakePhoto = (SwitchPreference) findPreference(KEY_FINGERPRINT_TAKE_PHOTO);
        mNoticationPanel = (SwitchPreference) findPreference(KEY_FINGERPRINT_NOTICATION_PANEL);
        mSwitchPhoto = (SwitchPreference) findPreference(KEY_FINGERPRINT_SWITCH_PHOTO);
        mZoomCamera = (SwitchPreference) findPreference(KEY_FINGERPRINT_ZOOM_CAMERA);
        mBackToHome.setOnPreferenceChangeListener(this);
        mTakePhoto.setOnPreferenceChangeListener(this);
        mNoticationPanel.setOnPreferenceChangeListener(this);
        mSwitchPhoto.setOnPreferenceChangeListener(this);
        mZoomCamera.setOnPreferenceChangeListener(this);
        mBackToHome.setChecked(Settings.System.getInt(getContentResolver(),
                    Settings.System.TCT_FINGERPRINT_BACK_TO_HOME, 1) == 1? true:false);
        mTakePhoto.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_TAKE_PHOTO, 0) == 1? true:false);
        mNoticationPanel.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_NOTICATION_PANEL, 1) == 1? true:false);
        mSwitchPhoto.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_SWITCH_PHOTO, 0) == 1? true:false);
                /* MODIFIED-END by jianguang.sun,BUG-2740734*/
        mZoomCamera.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_ZOOM_CAMERA, 0) == 1? true:false);
    }

    private void maybeAddFingerprintPreference(PreferenceGroup securityCategory, int userId) {
        Preference fingerprintPreference =
                FingerprintSettings.getFingerprintPreferenceForUser(
                        securityCategory.getContext(), userId);
        if (fingerprintPreference != null) {
            securityCategory.removeAll();
            securityCategory.addPreference(fingerprintPreference);
            addPreferencesFromResource(R.xml.fingerprint_sensor_settings);
        }
    }
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
        @Override
        public List<SearchIndexableRaw> getRawDataToIndex(Context context, boolean enabled) {
            final List<SearchIndexableRaw> result = new ArrayList<>();
            final Resources res = context.getResources();

            // Add fragment title
            SearchIndexableRaw data = new SearchIndexableRaw(context);
            data.title = res.getString(R.string.fingerprint_sensor_settings);
            data.screenTitle = res.getString(R.string.fingerprint_sensor_settings);
            result.add(data);
            FingerprintManager fpm =
                    (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            if (fpm != null && fpm.isHardwareDetected()) {
                // This catches the title which can be overloaded in an overlay
                data = new SearchIndexableRaw(context);
                data.title = res.getString(R.string.security_settings_fingerprint_preference_title);
                data.screenTitle = res.getString(R.string.fingerprint_sensor_settings);
                result.add(data);
                // Fallback for when the above doesn't contain "fingerprint"
                data = new SearchIndexableRaw(context);
                data.title = res.getString(R.string.fingerprint_manage_category_title);
                data.screenTitle = res.getString(R.string.fingerprint_sensor_settings);
                result.add(data);
            }
            return result;
        }
    };
}


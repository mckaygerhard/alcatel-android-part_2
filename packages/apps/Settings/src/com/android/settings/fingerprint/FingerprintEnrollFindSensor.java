/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.settings.fingerprint;

import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.UserHandle;
import android.util.Log; // MODIFIED by jianguang.sun, 2016-09-19,BUG-2740734

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.ChooseLockSettingsHelper;
import com.android.settings.R;
import com.android.settings.fingerprint.FingerprintEnrollSidecar.Listener;

/**
 * Activity explaining the fingerprint sensor location for fingerprint enrollment.
 */
public class FingerprintEnrollFindSensor extends FingerprintEnrollBase {

    private static final int CONFIRM_REQUEST = 1;
    private static final int ENROLLING = 2;
    public static final String EXTRA_KEY_LAUNCHED_CONFIRM = "launched_confirm_lock";

    private FingerprintFindSensorAnimation mAnimation;
    private boolean mLaunchedConfirmLock;
    private FingerprintEnrollSidecar mSidecar;
    private boolean mNextClicked;

    private GifView mFindSerserGif; // MODIFIED by jianguang.sun, 2016-09-27,BUG-2740734


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        setHeaderText(R.string.security_settings_fingerprint_enroll_find_sensor_title);
        if (savedInstanceState != null) {
            mLaunchedConfirmLock = savedInstanceState.getBoolean(EXTRA_KEY_LAUNCHED_CONFIRM);
            mToken = savedInstanceState.getByteArray(
                    ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
        }
        if (mToken == null && !mLaunchedConfirmLock) {
            launchConfirmLock();
        } else if (mToken != null) {
            startLookingForFingerprint(); // already confirmed, so start looking for fingerprint
        }
        /* MODIFIED-BEGIN by jianguang.sun, 2016-09-27,BUG-2740734*/
        //mAnimation = (FingerprintFindSensorAnimation) findViewById(
         //       R.id.fingerprint_sensor_location_animation);

        mFindSerserGif = (GifView) findViewById(R.id.fp_gif);
        mFindSerserGif.setMovieResource(R.raw.find_fingerprint_sensor);
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/

    }

    protected int getContentView() {
        return R.layout.fingerprint_enroll_find_sensor;
    }

    @Override
    protected void onStart() {
        super.onStart();
        /* MODIFIED-BEGIN by jianguang.sun, 2016-09-27,BUG-2740734*/
        //mAnimation.startAnimation();
        mFindSerserGif.setPaused(false);
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/
    }

    private void startLookingForFingerprint() {
        mSidecar = (FingerprintEnrollSidecar) getFragmentManager().findFragmentByTag(
                FingerprintEnrollEnrolling.TAG_SIDECAR);
        if (mSidecar == null) {
            mSidecar = new FingerprintEnrollSidecar();
            getFragmentManager().beginTransaction()
                    .add(mSidecar, FingerprintEnrollEnrolling.TAG_SIDECAR).commit();
        }
        mSidecar.setListener(new Listener() {
            @Override
            public void onEnrollmentProgressChange(int steps, int remaining) {
                mNextClicked = true;
                if (!mSidecar.cancelEnrollment()) {
                    proceedToEnrolling();
                }
            }

            @Override
            public void onEnrollmentHelp(CharSequence helpString) {
            }

            @Override
            public void onEnrollmentError(int errMsgId, CharSequence errString) {
                /* MODIFIED-BEGIN by jianguang.sun, 2016-09-19,BUG-2740734*/
                Log.d("FingerprintFindsensor", "onEnrollmentError mNextClicked:" + mNextClicked + errMsgId);
                /* MODIFIED-BEGIN by jianguang.sun, 2016-10-09,BUG-2740734*/
                if (!isFinishing()) {
                /* MODIFIED-END by jianguang.sun,BUG-2740734*/
                    mNextClicked = false;
                    if (mSidecar != null) {
                        mSidecar.setListener(null);
                    }
                   finish();
                   /* MODIFIED-END by jianguang.sun,BUG-2740734*/
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        /* MODIFIED-BEGIN by jianguang.sun, 2016-10-09,BUG-2740734*/
        if (mSidecar != null) {
            mSidecar.setListener(null);
        }
        /* MODIFIED-BEGIN by jianguang.sun, 2016-09-27,BUG-2740734*/
        mFindSerserGif.setPaused(true);//jianguangadd
       // mAnimation.pauseAnimation();
    }

    @Override
    public void onBackPressed() {
        Log.d("FingerprintFindsensor","onBackPressed");
        if (mSidecar != null) {
            mSidecar.setListener(null);
            mSidecar.cancelEnrollment();
            getFragmentManager().beginTransaction().remove(mSidecar).commitAllowingStateLoss();
            mSidecar = null;
        }
        super.onBackPressed();
    }

    @Override
    /* MODIFIED-END by jianguang.sun,BUG-2740734*/
    protected void onDestroy() {
        super.onDestroy();
        //mAnimation.stopAnimation();
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        /* MODIFIED-BEGIN by jianguang.sun, 2016-10-09,BUG-2740734*/
        if (!isChangingConfigurations()) {
            Log.d("FingerprintFindsensor","onSaveInstanceState");
            if (mSidecar != null) {
                Log.d("fingerprintfindsensor"," enroll onSaveInstanceState ");
                mSidecar.cancelEnrollment();
                getFragmentManager().beginTransaction().remove(mSidecar).commit();
            }
            finish();
        }
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_KEY_LAUNCHED_CONFIRM, mLaunchedConfirmLock);
        outState.putByteArray(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
    }

    @Override
    protected void onNextButtonClick() {
        mNextClicked = true;
        if (mSidecar == null || (mSidecar != null && !mSidecar.cancelEnrollment())) {
            proceedToEnrolling();
        }
    }

    private void proceedToEnrolling() {
        getFragmentManager().beginTransaction().remove(mSidecar).commit();
        mSidecar = null;
        /* MODIFIED-BEGIN by jianguang.sun, 2016-09-19,BUG-2740734*/
        finish();
        //startActivityForResult(getEnrollingIntent(), ENROLLING);
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONFIRM_REQUEST) {
            if (resultCode == RESULT_OK) {
                mToken = data.getByteArrayExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
                overridePendingTransition(R.anim.suw_slide_next_in, R.anim.suw_slide_next_out);
                getIntent().putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
                startLookingForFingerprint();
            } else {
                finish();
            }
        } else if (requestCode == ENROLLING) {
            if (resultCode == RESULT_FINISHED) {
                setResult(RESULT_FINISHED);
                finish();
            } else if (resultCode == RESULT_SKIP) {
                setResult(RESULT_SKIP);
                finish();
            } else if (resultCode == RESULT_TIMEOUT) {
                setResult(RESULT_TIMEOUT);
                finish();
            } else {
                FingerprintManager fpm = getSystemService(FingerprintManager.class);

                int enrolled = fpm.tctGetEnrolledFingerprints(FingerprintManager.FP_TAG_COMMON).size(); // MODIFIED by jianguang.sun, 2016-09-19,BUG-2740734
                int max = getResources().getInteger(
                        com.android.internal.R.integer.config_fingerprintMaxTemplatesPerUser);
                if (enrolled >= max) {
                    finish();
                } else {
                    // We came back from enrolling but it wasn't completed, start again.
                    startLookingForFingerprint();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void launchConfirmLock() {
        long challenge = getSystemService(FingerprintManager.class).preEnroll();
        ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(this);
        boolean launchedConfirmationActivity = false;
        if (mUserId == UserHandle.USER_NULL) {
            launchedConfirmationActivity = helper.launchConfirmationActivity(CONFIRM_REQUEST,
                getString(R.string.security_settings_fingerprint_preference_title),
                null, null, challenge);
        } else {
            launchedConfirmationActivity = helper.launchConfirmationActivity(CONFIRM_REQUEST,
                    getString(R.string.security_settings_fingerprint_preference_title),
                    null, null, challenge, mUserId);
        }
        if (!launchedConfirmationActivity) {
            // This shouldn't happen, as we should only end up at this step if a lock thingy is
            // already set.
            finish();
        } else {
            mLaunchedConfirmLock = true;
        }
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.FINGERPRINT_FIND_SENSOR;
    }
}

package com.android.settings.bluetooth;

/******************************************************************************/
/*                                                               Date:10/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  dongdong.gong                                                   */
/*  Email  :  dongdong.gong@tcl.com                                           */
/*  Role   :  engineer                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/10/2016|    dongdong.gong     |       3055667        |For Dual Bluetoo- */
/*           |                      |                      |th Dev            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.android.settings.R;
import com.android.settingslib.bluetooth.BluetoothDeviceFilter;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.LocalBluetoothAdapter;
import com.android.settingslib.bluetooth.LocalBluetoothManager;

import android.R.integer;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class BluetoothSelectSecondDevice extends DialogFragment {
    private AlertDialog mAlertDialog;
    final LocalBluetoothAdapter mLocalAdapter;
    final LocalBluetoothManager localManager;
    private RadioGroup mBtSectionSecond;
    private CheckBox mCheckBox;
    private Map<Integer, CachedBluetoothDevice> mCachedArray;
    private int mCachedDevCount;
    private static final String SHARED_PREFERENCES_NAME = "bluetooth_settings";
    private SharedPreferences mSharedPreferences;
    public static final String KEY_NOT_PROMT = "is_select_second";
    public BluetoothSelectSecondDevice() {
        localManager = Utils.getLocalBtManager(getActivity());
        mLocalAdapter = localManager.getBluetoothAdapter();
        mCachedArray = new HashMap<Integer, CachedBluetoothDevice>();
    }
    private int mChecktId = 1;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.bluetooth_connect_to_second_dual)
                .setView(createDialogView())
                .setPositiveButton(R.string.bluetooth_connect_dual,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            	CachedBluetoothDevice mGoingConnDev = mCachedArray.get(new Integer(mChecktId));
                            	if(mGoingConnDev != null){
                            	    mGoingConnDev.connect(false);
                            	}
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        return mAlertDialog;
    }

    private View createDialogView() {
        final LayoutInflater layoutInflater = (LayoutInflater)getActivity()
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bt_select_second_device, null);
        Collection<CachedBluetoothDevice> cachedDevices = localManager.getCachedDeviceManager().getCachedDevicesCopy();
        mBtSectionSecond = (RadioGroup)view.findViewById(R.id.bt_profile_section);
        mCheckBox = (CheckBox)view.findViewById(R.id.bt_not_show_checkbox);
        mCheckBox.setAlpha(0.54f);
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean mCheckBox) {
				if(mCheckBox){
					mSharedPreferences.edit().putString(KEY_NOT_PROMT, "true").apply();
				}
				else{
					mSharedPreferences.edit().putString(KEY_NOT_PROMT, "false").apply();
				}
			}
		});
        // Prevent updates while the list shows one of the state messages
        if (mLocalAdapter.getBluetoothState() != BluetoothAdapter.STATE_ON) return view;
        mCachedDevCount = 0;
        BluetoothDeviceFilter.Filter audioFilter = BluetoothDeviceFilter.getFilter(1);
        for(CachedBluetoothDevice cacheBTdevice : cachedDevices){
        	if(audioFilter.matches(cacheBTdevice.getDevice()) 
        			&& BluetoothDeviceFilter.UNBONDED_DEVICE_FILTER.matches(cacheBTdevice.getDevice())){
        		createRadioButton(cacheBTdevice);
        	}
        }
        if(mCachedArray.isEmpty())
        	dismiss();
        mBtSectionSecond.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				mChecktId = group.getCheckedRadioButtonId();
			}
		});
        mBtSectionSecond.check(mChecktId);
        return view;
    }

    

    @Override
	public void onResume() {
    	mSharedPreferences = getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
    public void onDestroy() {
        super.onDestroy();
        mAlertDialog = null;
        mCachedArray = null;
        mBtSectionSecond = null;
        mCheckBox = null;
    }

	private void createRadioButton(CachedBluetoothDevice cacheBTdevice) {
		float aaa = getActivity().getResources().getDisplayMetrics().density;
		int pixxxx = (int)((48 * aaa + 0.5f) / 2);
		if(mCachedArray.isEmpty()){
            RadioButton radioOne = new RadioButton(getActivity());
            radioOne.setText(cacheBTdevice.getDevice().getName());
            radioOne.setPadding( pixxxx, 0, 0, 0);
            radioOne.setHeight(pixxxx * 2);
            radioOne.setGravity(Gravity.CENTER_VERTICAL);
            mBtSectionSecond.addView(radioOne);
            mCachedArray.put(new Integer(radioOne.getId()), cacheBTdevice);
            mChecktId = radioOne.getId();
		}
		else{
            RadioButton radioOne = new RadioButton(getActivity());
            radioOne.setText(cacheBTdevice.getDevice().getName());
            radioOne.setPadding( pixxxx, 0, 0, 0);
            radioOne.setHeight(pixxxx * 2);
            radioOne.setGravity(Gravity.CENTER_VERTICAL);
            mBtSectionSecond.addView(radioOne);
            mCachedArray.put(new Integer(radioOne.getId()), cacheBTdevice);
		}
	}
}

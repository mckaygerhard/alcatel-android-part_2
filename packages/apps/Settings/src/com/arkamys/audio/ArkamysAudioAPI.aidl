package com.arkamys.audio;

interface ArkamysAudioAPI {
	void setEffectEnable(boolean activate);
	boolean getEffectEnable();
	void setRingtoneMode(boolean RingMode);
	void setPreset(int NewPresetID);
	int getPreset();
	void setMusicMode(boolean NewMusicModeID);
	void setTuningMode(boolean TuningMode);
	void setSensorSpeakerMode(int SensorSPKMode);
	String getArkamysVersion();
	float getAttenuation();
	int setParameter(in byte[] data);
	int getParameter(int CMD_ID,int ParamID,out byte[] Read_data);
	void setBroadcastLogEnable(boolean enable);
	void setCustomTrebleLevel(float gainTrebleLvl);
	void setCustomBassLevel(float gainBassLvl);
	void setCustomEQ(float gaindB, int bandId);
	void setCustomWidening(float gainWideningLvl);
}
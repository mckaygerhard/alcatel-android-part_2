/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

/* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
import mst.app.MstActivity;
import android.content.ContentValues; // MODIFIED by wanshun.ni, 2016-10-10,BUG-2827925
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; // MODIFIED by wanshun.ni, 2016-10-09,BUG-2827925
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.Intents;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
/* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button; // MODIFIED by wanshun.ni, 2016-10-09,BUG-2827925
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ex.chips.RecipientEditTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import com.android.settings.R;
import java.util.ArrayList;

/* Create by TCTNB(Guoqiang.Qiu)
 * This Activity is to save emergency message of One Finger Alarm
 */
public class AlarmMessage extends MstActivity { // MODIFIED by wanshun.ni, 2016-10-20,BUG-2827925

    private RecipientEditTextView contact;
    private EditText message;
    private Set<String> contactSet = null;
    private ListView alarmContacts;
    private CheckBox mLocation, mCallRecord, mElectricity, mNetworkCar;
    private View btnChoose; // MODIFIED by yiwei.zhu, 2016-11-17,BUG-3082231
    Button addContacts; // MODIFIED by wanshun.ni, 2016-10-09,BUG-2827925
    SharedPreferences file;
    List<ContactsContent> persons = null;
    AlarmContactsAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
        setMstContentView(R.layout.activity_alarm_message);
        showBackIcon(true);
        setTitle(R.string.title_emergency_message);
        inflateToolbarMenu(R.menu.alarm_message_menu);
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        alarmContacts = (ListView) findViewById(R.id.alarm_contacts);

        persons = new ArrayList<ContactsContent>();

        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-09,BUG-2827925*/
        AlarmDateBaseHelper dbHelper = new AlarmDateBaseHelper(this,"AlarmContact.db",null,1);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query("alarmContact",new String[]{"name","number"},null,null,null,null,null);
        while (cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String number = cursor.getString(cursor.getColumnIndex("number"));
            persons.add(new ContactsContent(name, number));
        }
        db.close();
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

        adapter = new AlarmContactsAdapter(this, persons);
        alarmContacts.setAdapter(adapter);
        UiHelper.HeightBasedOnChilder(alarmContacts);

        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
        alarmContacts.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(AlarmMessage.this,AddContacts.class);
                intent.putExtra("isclickListView", true);
                intent.putExtra("position", position);
                intent.putExtra("name", ((TextView)alarmContacts.getChildAt(position).findViewById(R.id.tv)).getText().toString());
                intent.putExtra("number", ((TextView)alarmContacts.getChildAt(position).findViewById(R.id.con_num)).getText().toString());
                startActivityForResult(intent, 1);
            }
        });
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

        btnChoose = findViewById(R.id.one_finger_choose);//add contacts // MODIFIED by yiwei.zhu, 2016-11-17,BUG-3082231
        contact = (RecipientEditTextView) findViewById(R.id.one_finger_contact);
        message = (EditText) findViewById(R.id.one_finger_message);

        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-09,BUG-2827925*/
        addContacts = (Button)findViewById(R.id.add_contacts);
        addContacts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(AlarmMessage.this,AddContacts.class);
                intent.putExtra("isclickListView", false); // MODIFIED by wanshun.ni, 2016-10-10,BUG-2827925
                startActivityForResult(intent, 0);
            }
        });
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

        file = getSharedPreferences(Config.PREF_NAME, MODE_PRIVATE);
        contactSet = file.getStringSet(Config.KEY_CONTACTS, null);
        message.setText(file.getString(Config.KEY_MESSAGE,
                getString(R.string.default_message)));

        mLocation = (CheckBox) findViewById(R.id.current_location);
        mCallRecord = (CheckBox) findViewById(R.id.call_record);
        mElectricity = (CheckBox) findViewById(R.id.electricity);
        mNetworkCar = (CheckBox) findViewById(R.id.network_car);
        mLocation.setChecked(file.getBoolean(Config.KEY_LOCATION, true));
        mCallRecord.setChecked(file.getBoolean(Config.KEY_CALLRECORD,true));
        mElectricity.setChecked(file.getBoolean(Config.KEY_ELECTRITY,true));
        mNetworkCar.setChecked(file.getBoolean(Config.KEY_NETWORKCAR,true));

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	/* MODIFIED-BEGIN by yiwei.zhu, 2016-11-17,BUG-3082231*/
            	Intent intent = new Intent(AlarmMessage.this,AddContacts.class);
            	               intent.putExtra("isclickListView", false);
                               startActivityForResult(intent, 0);
                               /* MODIFIED-END by yiwei.zhu,BUG-3082231*/
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (contactSet == null || contactSet.size() == 0) {
                    contact.setText("");
                    return;
                }
                for (String number : contactSet) {
                    /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
                    contact.append(contactToToken(number) + Config.DELIMITER
                            + " ");
                            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
                }
            }
        }).start();
        contact.setTokenizer(new RecipientsEditorTokenizer());
        contact.setAdapter(new ChipsRecipientAdapter(this));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-09,BUG-2827925*/
        if (data == null)
            return;
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
        switch (requestCode){
            case 1:
                persons.remove(Integer.parseInt(data.getExtra("position").toString()));
                if(resultCode == 0){
                    String nameString = data.getExtras().getString("name");
                    String num = data.getExtras().getString("number");
                    persons.add(new ContactsContent(nameString, num));
                }
                break;
            case 0:
                String nameString = data.getExtras().getString("name");
                String num = data.getExtras().getString("number");
                persons.add(new ContactsContent(nameString, num));
                break;
        }
        adapter.notifyDataSetChanged();
        UiHelper.HeightBasedOnChilder(alarmContacts);
    }
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if(alarmContacts.getCount() >= 5){
            /* MODIFIED-BEGIN by yiwei.zhu, 2016-11-17,BUG-3082231*/
            //addContacts.setVisibility(View.GONE);
        	btnChoose.setVisibility(View.GONE);
        	contact.setVisibility(View.GONE);
        } else {
            //addContacts.setVisibility(View.VISIBLE);
        	btnChoose.setVisibility(View.VISIBLE);
        	contact.setVisibility(View.VISIBLE);
        	/* MODIFIED-END by yiwei.zhu,BUG-3082231*/
        }
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }

    @Override
    /* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
    public boolean onMenuItemClick(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
        case R.id.one_finger_save:
            saveMessage();
            return true;
        }
        return false;
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }

    private void saveMessage() {
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-09,BUG-2827925*/
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-12,BUG-2827925*/
        if(alarmContacts.getCount() < 1){
            Toast.makeText(AlarmMessage.this, R.string.contact_null, Toast.LENGTH_SHORT).show(); // MODIFIED by wanshun.ni, 2016-10-28,BUG-2827925
            return;
        }
        if(message.getText().toString().trim().isEmpty()){
            Toast.makeText(AlarmMessage.this, R.string.add_contact_message, Toast.LENGTH_SHORT).show();
            return;
        }
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        Set<String> contacts = new HashSet<String>();
        AlarmDateBaseHelper dbHelper = new AlarmDateBaseHelper(this,
                "AlarmContact.db", null, 1);
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("alarmContact", null, null);

        for(int i = 0; i < alarmContacts.getCount(); i++){
            String mtv = ((TextView)alarmContacts.getChildAt(i).findViewById(R.id.tv)).getText().toString();
            String mnum= ((TextView)alarmContacts.getChildAt(i).findViewById(R.id.con_num)).getText().toString();
            contacts.add(mnum);
            ContentValues cv = new ContentValues();
            cv.put("name",mtv);
            cv.put("number",mnum);
            db.insert("alarmContact",null,cv);
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        }
        db.close();
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

        String messageText = message.getText().toString();
        if (messageText.trim().isEmpty()) {
            messageText = getString(R.string.default_message);
        }// If there is no emergency message or it is empty, save default
            // message

        SharedPreferences.Editor editor = file.edit();
        editor.putStringSet(Config.KEY_CONTACTS, contacts);
        editor.putString(Config.KEY_MESSAGE, messageText);

        editor.putBoolean(Config.KEY_LOCATION, mLocation.isChecked() ? true
                : false);
        editor.putBoolean(Config.KEY_CALLRECORD, mCallRecord.isChecked() ? true
                : false);
        editor.putBoolean(Config.KEY_ELECTRITY, mElectricity.isChecked() ? true
                : false);
        editor.putBoolean(Config.KEY_NETWORKCAR, mNetworkCar.isChecked() ? true
                : false);
                /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        editor.putBoolean(Config.KEY_HASCONTACTS, alarmContacts.getCount() > 0 ? true
                : false);

        editor.apply();
        setResult(RESULT_OK);
        finish();
    }

    private CharSequence contactToToken(String c) {
        return new SpannableString(c);
    }

    private Set<String> stringToSet(String ori) {
        if (ori == null) {
            return null;
        }
        String tmp = ori.replaceAll(" ", "").replaceAll("-", ""); // MODIFIED by wanshun.ni, 2016-09-27,BUG-2827925
        if (tmp.isEmpty()) {
            return null;
        }
        return new HashSet<>(Arrays.asList(tmp.split(Config.DELIMITER)));
    }

    /* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
    /* MODIFIED-END by wanshun.ni,BUG-2827925*/
}

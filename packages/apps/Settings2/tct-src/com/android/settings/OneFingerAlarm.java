/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import mst.app.dialog.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter; // MODIFIED by wanshun.ni, 2016-10-10,BUG-2827925
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Bundle;
import mst.preference.SwitchPreference;
import mst.preference.Preference;
import mst.preference.PreferenceScreen;
import android.provider.Settings;

import com.android.internal.logging.MetricsProto.MetricsEvent;
/* MODIFIED-BEGIN by wanshun.ni, 2016-10-11,BUG-2827925*/
import com.android.settings.fingerprint.FingerprintEnrollEnrolling2;
import com.android.settings.fingerprint.RenameDeleteActivity;
import android.os.UserHandle;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/

import java.util.List;

/*
 * Setting UI of OneFingerAlarm
 * [FEATURE]-Add by TCTNB(Guoqiang.Qiu),2016-02-25,Task-1690228
 */

public class OneFingerAlarm extends SettingsPreferenceFragment implements Preference.OnPreferenceChangeListener {
    private static final String KEY_ONE_FINGER = "one_finger_enable";
    private static final String KEY_FINGER_ALARM_EDIT = "finger_alarm_edit";
    private static final String TCT_ONE_KEY_ALARM = "tct_one_key_alarm"; // MODIFIED by wanshun.ni, 2016-09-08,BUG-2827925
    private static final int RESULT_OK = -1;
    private static final int RESULT_FINISHED = 1;
    private static final int REQUEST_ON = 1;
    private static final int REQUEST_EDIT = 2;
    private static final int REQUEST_CONFIRM = 601;

    private static final Uri URI_MSG = Uri.parse("content://fingerprint.information/emergency_contact");

    private SwitchPreference mToggleAlarm;
    private FingerprintManager fpm;
    private byte[] mToken;
    private boolean hasFingerprint = false;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.finger_alarm);
        fpm = (FingerprintManager) getPrefContext().getSystemService( // MODIFIED by wanshun.ni, 2016-10-14,BUG-2827925
                Context.FINGERPRINT_SERVICE);
        mToggleAlarm = (SwitchPreference) findPreference(KEY_ONE_FINGER);
        mToggleAlarm.setOnPreferenceChangeListener(this);
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-11,BUG-2827925*/
        Intent alarmIntent = new Intent("com.tct.fingerprint.alarm.cancel");
        getPrefContext().sendBroadcast(alarmIntent);
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        Bundle args = getArguments();
        if(args != null) {
            mToken = args.getByteArray(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Fingerprint fp = getFingerprint();
        if (mToken == null && hasFingerprint) {
            launchChooseOrConfirmLock();
        }
        editOrNewFingerprint(fp);
        mToggleAlarm.setChecked(Settings.System.getInt(
            getContentResolver(), TCT_ONE_KEY_ALARM, 0) == 1); // MODIFIED by wanshun.ni, 2016-09-08,BUG-2827925
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.SECURITY;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ON && resultCode == RESULT_FINISHED) {
            if (data != null) {
                mToken = data.getByteArrayExtra(
                        ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
            }
            if (hasEmergencyContact()) {
                toggleOneKeyAlarm(true);
            } else {
                Intent intent = new Intent();
                intent.setAction("com.tct.settings.fingeralarm");
                startActivityForResult(intent, REQUEST_EDIT);
            }
        } else if (requestCode == REQUEST_EDIT) {
            if (resultCode == RESULT_OK)
                toggleOneKeyAlarm(true);
        } else if (data != null && (requestCode == REQUEST_CONFIRM && resultCode == RESULT_OK)) {
            mToken = data.getByteArrayExtra(
                    ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
        } else {
            super.finishFragment();
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        if (preference == mToggleAlarm) {
            if ((Boolean)value && !hasFingerprint) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getPrefContext()); // MODIFIED by wanshun.ni, 2016-10-10,BUG-2827925
                builder.setMessage(R.string.dialog_finger_alarm_content);
                builder.setPositiveButton(R.string.dialog_finger_alarm_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG,
                                FingerprintManager.FP_TAG_ALARM);
                        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
                        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-11,BUG-2827925*/
                        final String entryName = FingerprintEnrollEnrolling2.class.getName();
                        intent.setClassName("com.android.settings", entryName);
                        intent.putExtra(Intent.EXTRA_USER_ID, UserHandle.myUserId());
                        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
                        startActivityForResult(intent, REQUEST_ON);
                    }
                });
                builder.setNegativeButton(R.string.dialog_finger_alarm_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
                //[BUGFIX]-ADD by TSNJ.heng.zhang1,2016-06-06,Defect-2252136
                return false;
            } else if ((Boolean)value && hasFingerprint && !hasEmergencyContact()) {
                Intent intent = new Intent();
                intent.setAction("com.tct.settings.fingeralarm");
                startActivityForResult(intent, REQUEST_EDIT);
                return false;
            } else {
                Settings.System.putInt(getContentResolver(),
                    TCT_ONE_KEY_ALARM, // MODIFIED by wanshun.ni, 2016-09-08,BUG-2827925
                    (Boolean)value ? 1 : 0);
            }
        }
        return true;
    }

    private void editOrNewFingerprint(Fingerprint fp) {
        Preference fingerprintPreference = (Preference)findPreference(KEY_FINGER_ALARM_EDIT);
        Intent intent = new Intent();
        String entryName;
        if (hasFingerprint) {
            fingerprintPreference.setTitle(R.string.finger_alarm_title);
            Bundle args = new Bundle();
            args.putParcelable("fingerprint", fp);
            intent.putExtras(args);
            /* MODIFIED-BEGIN by wanshun.ni, 2016-10-11,BUG-2827925*/
            entryName = RenameDeleteActivity.class.getName();
        } else {
            intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG, FingerprintManager.FP_TAG_ALARM);
            fingerprintPreference.setTitle(R.string.finger_alarm_new_title);
            intent.putExtra(Intent.EXTRA_USER_ID, UserHandle.myUserId()); // MODIFIED by wanshun.ni, 2016-10-17,BUG-2827925
            entryName = FingerprintEnrollEnrolling2.class.getName();
            toggleOneKeyAlarm(false);
        }
        intent.setClassName("com.android.settings", entryName);
        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
        fingerprintPreference.setIntent(intent);
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }

    private void launchChooseOrConfirmLock() {
        long challenge = fpm.preEnroll();
        ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(getActivity(), this);
        helper.launchConfirmationActivity(REQUEST_CONFIRM,
                getString(R.string.input_password),
                null, null, challenge);
    }

    private Fingerprint getFingerprint() {
        final List<Fingerprint> items = fpm.tctGetEnrolledFingerprints(FingerprintManager.FP_TAG_ALARM);
        hasFingerprint = items != null && items.size() != 0;
        if (hasFingerprint) {
            return items.get(0);
        }
        return null;
    }

    private boolean hasEmergencyContact() {
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-17,BUG-2827925*/
        Boolean hasContact;
        SharedPreferences file = getPrefContext().getSharedPreferences(
                Config.PREF_NAME, Context.MODE_PRIVATE);
        hasContact = file.getBoolean(Config.KEY_HASCONTACTS, false);
        return hasContact;
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }

    private void toggleOneKeyAlarm(boolean on) {
        mToggleAlarm.setChecked(on);
        Settings.System.putInt(getContentResolver(), TCT_ONE_KEY_ALARM, on ? 1 : 0); // MODIFIED by wanshun.ni, 2016-09-08,BUG-2827925
    }
}

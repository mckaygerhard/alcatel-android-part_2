/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.MultiAutoCompleteTextView;

public class RecipientsEditorTokenizer implements MultiAutoCompleteTextView.Tokenizer {

    @Override
    public int findTokenStart(CharSequence text, int cursor) {
        int i = cursor;
        char c;

        // If we're sitting at a delimiter, back up so we find the previous token
        //[BUGFIX]-Add Begin by TSNJ,wei.li,7/21/2014,Bug-737121
        while (i > 0 && text.charAt(i - 1) == ' ') {
            i--;
        }
        //[BUGFIX]-Add End by TSNJ,wei.li
        if (i > 0 && ((c = text.charAt(i - 1)) == ',' || c == ';')) {
            --i;
        }
        // Now back up until the start or until we find the separator of the previous token
        while (i > 0 && (c = text.charAt(i - 1)) != ',' && c != ';') {
            i--;
        }
        while (i < cursor && text.charAt(i) == ' ') {
            i++;
        }
        //filter Full width space
        while (i < cursor && text.charAt(i) == '\u3000') {
            i++;
        }

        return i;
    }

    @Override
    public int findTokenEnd(CharSequence text, int cursor) {
        int i = cursor;
        int len = text.length();
        char c;

        if(i < 0) i = 0;// MODIFIED by ailin.yang, 2016-05-19,BUG-2193537
        while (i < len) {
            if ((c = text.charAt(i)) == ',' || c == ';') {
                return i;
            } else {
                i++;
            }
        }

        return len;
    }

    @Override
    public CharSequence terminateToken(CharSequence text) {
        int i = text.length();

        while (i > 0 && text.charAt(i - 1) == ' ') {
            i--;
        }

        char c;
        if (i > 0 && ((c = text.charAt(i - 1)) == ',' || c == ';')) {
            return text;
        } else {
            // Use the same delimiter the user just typed.
            // This lets them have a mixture of commas and semicolons in their list.
            String separator = Config.DELIMITER + " ";
            if (text instanceof Spanned) {
                SpannableString sp = new SpannableString(text + separator);
                TextUtils.copySpansFrom((Spanned) text, 0, text.length(), Object.class, sp, 0);
                return sp;
            } else {
                return text + separator;
            }
        }
    }
}

/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Set;

/*
 * This is config for One-key Alarm, all the attritubes are package-private
 */

public final class Config {
    static final String PREF_NAME = "emergency_message";
    static final String KEY_CONTACTS = "number";
    static final String KEY_MESSAGE = "message";
    /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
    static final String KEY_LOCATION = "location";
    static final String KEY_CALLRECORD = "callrecord";
    static final String KEY_ELECTRITY = "electrity";
    static final String KEY_NETWORKCAR = "networkcar";
    static final String KEY_HASCONTACTS = "hascontacts"; // MODIFIED by wanshun.ni, 2016-10-17,BUG-2827925
    /* MODIFIED-END by wanshun.ni,BUG-2827925*/

    static final String DELIMITER = ",";
    static final String URL_FORMAT = " http://m.amap.com/?q=%f,%f&dev=1";

    static final String ACTION_ONE_KEY_ALARM = "com.tct.fingerprint.alarm";
    static final String ACTION_ONE_KEY_MMS = "com.tct.fingerprint.mms";

    static final String ACTION_MULTI_PHONE = "com.android.contacts.action.GET_MULTI_PHONES";
    static final String PICK_PHONES_RESULT = "MULTI_PICK_PHONE_RESULT";
    static final String SELECTED_PHONES = "SELECTED_PHONE_NUMBERS";

    public static int hasContact(Context context) {
        SharedPreferences file = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        final Set<String> contactSet = file.getStringSet(KEY_CONTACTS, null);
        if (contactSet == null || contactSet.isEmpty()) {
            return 0;
        }
        return 1;
    }
}

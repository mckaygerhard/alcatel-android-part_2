/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.settings.R;
import mst.preference.Preference;

public class TctV7WidgetPreference extends Preference {
    private TextView mTextView;
    private String mDetail;
    private ImageView mImageView;
    private boolean mShow = false;
    private boolean needsetcolor = false;
    private Context mContext = null;

    public TctV7WidgetPreference(Context context) {
        this(context, null);
        mContext=context;
        setWidgetLayoutResource(R.layout.tct_pref_widget);
    }

    public TctV7WidgetPreference(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.preferenceStyle);
    }

    public TctV7WidgetPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public TctV7WidgetPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext=context;
        setWidgetLayoutResource(R.layout.tct_pref_widget);
    }

    @Override
    public void onBindView(View holder){
        super.onBindView(holder);

        View widgetFrame = holder.findViewById(com.android.internal.R.id.widget_frame);

        if (widgetFrame != null) {
            mTextView = (TextView) widgetFrame.findViewById(R.id.pref_tv_detail);
            if (!TextUtils.isEmpty(mDetail)) {
                mTextView.setText(mDetail);
            } else {
                mTextView.setText("");
            }
            mImageView = (ImageView) widgetFrame.findViewById(R.id.pref_image_detail);
            if (mShow) {
                mImageView.setVisibility(View.GONE);
            }
            if(needsetcolor){
                mImageView.setColorFilter(mContext.getResources().getColor(R.color.text_disable));
            }
        }
    }

    public void setDetail(String detail) {
        this.mDetail = detail;
        notifyChanged();
    }

    public void setGone(boolean visible) {
        this.mShow = visible;
        notifyChanged();
    }

    public String getDetail() {
        return mDetail;
    }

    public void setDsiabledImageColorFilter(boolean needset) {
        this.needsetcolor=needset;
        notifyChanged();
    }
}

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.UserHandle;
import android.preference.SeekBarVolumizer;
import android.provider.*;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Objects;

import static android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE;
import static android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL;

/** A slider preference that directly controls an audio stream volume (no dialog) **/
public class BrightnessSeekBarPreference extends SeekBarPreference {
    private static final String TAG = "BrightnessSeekBarPreference";
    private boolean mAutoBrightnessMode;


    public BrightnessSeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr,
                                       int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public BrightnessSeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public BrightnessSeekBarPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public void setBrightnessMode(boolean mode) {
        mAutoBrightnessMode = mode;
    }

    // during initialization, this preference is the SeekBar listener
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
        super.onProgressChanged(seekBar, progress, fromTouch);
        Log.d("zhouyong", "progess =" + progress);
        int brightnessMode = Settings.System.getInt(getContext().getContentResolver(),
                SCREEN_BRIGHTNESS_MODE, SCREEN_BRIGHTNESS_MODE_MANUAL);
        mAutoBrightnessMode = brightnessMode != SCREEN_BRIGHTNESS_MODE_MANUAL;
        if(!mAutoBrightnessMode) {
            int val = progress * 255/100;
            Log.d("zhouyong","val = "+val);
            android.provider.Settings.System.putIntForUser(getContext().getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS, val,
                    UserHandle.USER_CURRENT);
        } else {
            float adj = progress * 2.0f/100 -1;
            Log.d("zhouyong","adj = "+adj);
            android.provider.Settings.System.putFloatForUser(getContext().getContentResolver(),
                    android.provider.Settings.System.SCREEN_AUTO_BRIGHTNESS_ADJ, adj,
                    UserHandle.USER_CURRENT);
        }
    }
}

/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/10/11|     jianhong.yang    |     task 3046476     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.wifi;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Iterator;

import android.content.Context;
import android.net.IpConfiguration;
import android.net.LinkAddress;
import android.net.NetworkUtils;
import android.net.ProxyInfo;
import android.net.StaticIpConfiguration;
import android.net.Uri;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiEnterpriseConfig;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration.AuthAlgorithm;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiEnterpriseConfig.Eap;
import android.net.wifi.WifiEnterpriseConfig.Phase2;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.MstSpinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import mst.widget.toolbar.Toolbar;
import mst.app.MstActivity;

import com.android.settings.ProxySelector;
import com.android.settings.R;
import com.android.settingslib.wifi.AccessPoint;

public class ApAdvancedSettings extends MstActivity implements TextWatcher,
        AdapterView.OnItemSelectedListener, OnCheckedChangeListener,View.OnClickListener{
    public static final String TAG = "ApAdvancedSettings";
    private Context mContext;

    /* This value comes from "wifi_ip_settings" resource array */
    private static final int DHCP = 0;
    private static final int STATIC_IP = 1;

    /* These values come from "wifi_proxy_settings" resource array */
    public static final int PROXY_NONE = 0;
    public static final int PROXY_STATIC = 1;
    public static final int PROXY_PAC = 2;

    private Toolbar mToolbar;
    private RelativeLayout mProxySettingsFields;
    private RelativeLayout mIpFields;

    private MstSpinner mIpSettingsSpinner;
    private TextView mIpAddressView;
    private TextView mGatewayView;
    private TextView mNetworkPrefixLengthView;
    private TextView mDns1View;
    private TextView mDns2View;

    private TextView mIpSettingsSummary;

    private MstSpinner mProxySettingsSpinner;
    private TextView mProxyHostView;
    private TextView mProxyPortView;
    private TextView mProxyExclusionListView;
    private TextView mProxyPacView;
    private TextView mProxySummary;

    private Bundle mApInfo;
    private String mSsid;
    private String mBssid;
    private String mSecurity;
    private DetailedState mState;
    private WifiConfiguration mApConfig;
    private WifiInfo mInfo;
    private int mLevel;
    private int mSpeed;
    private int mFrequencyInt;
    private int mIpStyle;
    private String mIpAddr;
    private int mNetworkId;
    private int mSecurityInt;
    private String mSecurityString;
    private boolean mIsActive;
    private boolean mIsEphemeral;

    private WifiManager.ActionListener mSaveListener;
    private WifiManager mWifiManager;
    private IpAssignment mIpAssignment = IpAssignment.UNASSIGNED;
    private ProxySettings mProxySettings = ProxySettings.UNASSIGNED;
    private ProxyInfo mHttpProxy = null;
    private StaticIpConfiguration mStaticIpConfiguration = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setMstContentView(R.layout.ap_advanced_settings);

        mToolbar = getToolbar();
        setTitle(getString(R.string.advanced_settings));
        showBackIcon(true);
        setActionBar(mToolbar);
        //inflateToolbarMenu(R.menu.wifi_save);

        initView();
    }

    private void initView() {
        mContext = getApplicationContext();
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        mApInfo = getIntent().getBundleExtra("SelectedAccessPoint");
        if(mApInfo != null) {
            mIsActive = mApInfo.getBoolean("IS_ACTIVE");
            mIsEphemeral = mApInfo.getBoolean("IS_EPHEMERAL");
            mSsid = mApInfo.getString("SSID");
            mSecurityString = mApInfo.getString("SECURITY_STR");
            mSecurityInt = mApInfo.getInt("SECURITY_INT");
            mLevel = mApInfo.getInt("LEVEL");
            mApConfig = mApInfo.getParcelable("CONFIG");
            mInfo = mApInfo.getParcelable("WIFI_INFO");
            mState = "".equals(mApInfo.getString("STATE")) ? null
                    : DetailedState.valueOf(mApInfo.getString("STATE"));
            mSpeed = mApInfo.getInt("SPEED");
            mFrequencyInt = mApInfo.getInt("FREQUENCY_INT");
            mIpStyle = mApInfo.getInt("IPSTYLE");
            mIpAddr = mApInfo.getString("IPADRR");
            mNetworkId = mApInfo.getInt("NETWORK_ID");
        }

        mProxySettingsFields = (RelativeLayout) findViewById(R.id.proxy_settings_fields);
        mIpFields = (RelativeLayout) findViewById(R.id.ip_fields);

        mProxySettingsFields.setOnClickListener(this);
        mIpFields.setOnClickListener(this);

        mIpSettingsSpinner = (MstSpinner) findViewById(R.id.ip_settings);
        mIpSettingsSpinner.setOnItemSelectedListener(this);
        mProxySettingsSpinner = (MstSpinner) findViewById(R.id.proxy_settings);
        mProxySettingsSpinner.setOnItemSelectedListener(this);

        mProxySummary = (TextView) findViewById(R.id.proxy_summary);
        mIpSettingsSummary = (TextView) findViewById(R.id.ip_settings_summary);

        getAdvancedSettings();

        mSaveListener = new WifiManager.ActionListener() {
            @Override
            public void onSuccess() {
            }
            @Override
            public void onFailure(int reason) {
                if (mContext != null) {
                    Toast.makeText(mContext,
                        R.string.wifi_failed_save_message,
                        Toast.LENGTH_SHORT).show();
                }
            }
        };

        showIpConfigFields();
        showProxyFields();
        invalidateOptionsMenu();
    }

    private void showIpConfigFields() {
        WifiConfiguration config = null;

        findViewById(R.id.ip_fields).setVisibility(View.VISIBLE);

        if (mApInfo != null && mNetworkId != WifiConfiguration.INVALID_NETWORK_ID) {
            config = mApConfig;
        }

        int ipSelection = mIpSettingsSpinner.getSelectedItemPosition();
        mIpSettingsSummary.setText((String)mIpSettingsSpinner.getSelectedItem());

        if (ipSelection == STATIC_IP) {
            findViewById(R.id.staticip).setVisibility(View.VISIBLE);
            if (mIpAddressView == null) {
                mIpAddressView = (TextView) findViewById(R.id.ipaddress);
                mIpAddressView.addTextChangedListener(this);
                mGatewayView = (TextView) findViewById(R.id.gateway);
                mGatewayView.addTextChangedListener(this);
                mNetworkPrefixLengthView = (TextView)findViewById(R.id.network_prefix_length);
                mNetworkPrefixLengthView.addTextChangedListener(this);
                mDns1View = (TextView) findViewById(R.id.dns1);
                mDns1View.addTextChangedListener(this);
                mDns2View = (TextView) findViewById(R.id.dns2);
                mDns2View.addTextChangedListener(this);
            }
            if (config != null) {
                StaticIpConfiguration staticConfig = config.getStaticIpConfiguration();
                if (staticConfig != null) {
                    if (staticConfig.ipAddress != null) {
                        mIpAddressView.setText(
                                staticConfig.ipAddress.getAddress().getHostAddress());
                        mNetworkPrefixLengthView.setText(Integer.toString(staticConfig.ipAddress
                                .getNetworkPrefixLength()));
                    }

                    if (staticConfig.gateway != null) {
                        mGatewayView.setText(staticConfig.gateway.getHostAddress());
                    }

                    Iterator<InetAddress> dnsIterator = staticConfig.dnsServers.iterator();
                    if (dnsIterator.hasNext()) {
                        mDns1View.setText(dnsIterator.next().getHostAddress());
                    }
                    if (dnsIterator.hasNext()) {
                        mDns2View.setText(dnsIterator.next().getHostAddress());
                    }
                }
            }
        } else {
            findViewById(R.id.staticip).setVisibility(View.GONE);
        }
    }

    private void showProxyFields() {
        WifiConfiguration config = null;

        findViewById(R.id.proxy_settings_fields).setVisibility(View.VISIBLE);

        if (mApInfo != null && mNetworkId != WifiConfiguration.INVALID_NETWORK_ID) {
            config = mApConfig;
        }

        int proxySelection = mProxySettingsSpinner.getSelectedItemPosition();
        mProxySummary.setText((String)mProxySettingsSpinner.getSelectedItem());

        if (proxySelection == PROXY_STATIC) {
            setVisibility(R.id.proxy_warning_limited_support, View.VISIBLE);
            setVisibility(R.id.proxy_fields, View.VISIBLE);
            setVisibility(R.id.proxy_pac_field, View.GONE);
            if (mProxyHostView == null) {
                mProxyHostView = (TextView) findViewById(R.id.proxy_hostname);
                mProxyHostView.addTextChangedListener(this);
                mProxyPortView = (TextView) findViewById(R.id.proxy_port);
                mProxyPortView.addTextChangedListener(this);
                mProxyExclusionListView = (TextView) findViewById(R.id.proxy_exclusionlist);
                mProxyExclusionListView.addTextChangedListener(this);
            }
            if (config != null) {
                ProxyInfo proxyProperties = config.getHttpProxy();
                if (proxyProperties != null) {
                    mProxyHostView.setText(proxyProperties.getHost());
                    mProxyPortView.setText(Integer.toString(proxyProperties.getPort()));
                    mProxyExclusionListView.setText(proxyProperties.getExclusionListAsString());
                }
            }
        } else if (proxySelection == PROXY_PAC) {
            setVisibility(R.id.proxy_warning_limited_support, View.GONE);
            setVisibility(R.id.proxy_fields, View.GONE);
            setVisibility(R.id.proxy_pac_field, View.VISIBLE);

            if (mProxyPacView == null) {
                mProxyPacView = (TextView) findViewById(R.id.proxy_pac);
                mProxyPacView.addTextChangedListener(this);
            }
            if (config != null) {
                ProxyInfo proxyInfo = config.getHttpProxy();
                if (proxyInfo != null) {
                    mProxyPacView.setText(proxyInfo.getPacFileUrl().toString());
                }
            }
        } else {
            setVisibility(R.id.proxy_warning_limited_support, View.GONE);
            setVisibility(R.id.proxy_fields, View.GONE);
            setVisibility(R.id.proxy_pac_field, View.GONE);
        }
    }

    private void setVisibility(int id, int visibility) {
        final View v = findViewById(id);
        if (v != null) {
            v.setVisibility(visibility);
        }
    }

    private void getAdvancedSettings() {
        if (mNetworkId != WifiConfiguration.INVALID_NETWORK_ID) {
            WifiConfiguration config = mApConfig;
            if (config != null && config.getIpAssignment() == IpAssignment.STATIC) {
                mIpSettingsSpinner.setSelection(STATIC_IP);
            } else {
                mIpSettingsSpinner.setSelection(DHCP);
            }

            if (config != null && config.getProxySettings() == ProxySettings.STATIC) {
                mProxySettingsSpinner.setSelection(PROXY_STATIC);
            } else if (config != null && config.getProxySettings() == ProxySettings.PAC) {
                mProxySettingsSpinner.setSelection(PROXY_PAC);
            } else {
                mProxySettingsSpinner.setSelection(PROXY_NONE);
            }

            showIpConfigFields();
            showProxyFields();
            invalidateOptionsMenu();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
        case 1:
            mWifiManager.save(getConfig(), mSaveListener);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        menu.add(Menu.NONE, 1, 0, R.string.wifi_save)
        .setEnabled(ipAndProxyFieldsAreValid())
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        //getMenuInflater().inflate(R.menu.wifi_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                mWifiManager.save(getConfig(), mSaveListener);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch(v.getId()) {
        case R.id.proxy_settings_fields :
            if(mProxySettingsSpinner.performClick()) {
            }
        break;

        case R.id.ip_fields :
            if(mIpSettingsSpinner.performClick()) {
            }
            break;

        default :
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
            long id) {
        // TODO Auto-generated method stub
        if(parent == mProxySettingsSpinner) {
            showProxyFields();
        } else if(parent == mIpSettingsSpinner){
            showIpConfigFields();
        }

        invalidateOptionsMenu();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub
        invalidateOptionsMenu();
    }

    private boolean ipAndProxyFieldsAreValid() {
        mIpAssignment =
                (mIpSettingsSpinner != null
                    && mIpSettingsSpinner.getSelectedItemPosition() == STATIC_IP)
                ? IpAssignment.STATIC
                : IpAssignment.DHCP;

        if (mIpAssignment == IpAssignment.STATIC) {
            mStaticIpConfiguration = new StaticIpConfiguration();
            int result = validateIpConfigFields(mStaticIpConfiguration);
            if (result != 0) {
                return false;
            }
        }

        final int selectedPosition = mProxySettingsSpinner.getSelectedItemPosition();
        mProxySettings = ProxySettings.NONE;
        mHttpProxy = null;
        if (selectedPosition == PROXY_STATIC && mProxyHostView != null) {
            mProxySettings = ProxySettings.STATIC;
            String host = mProxyHostView.getText().toString();
            String portStr = mProxyPortView.getText().toString();
            String exclusionList = mProxyExclusionListView.getText().toString();
            int port = 0;
            int result = 0;
            try {
                port = Integer.parseInt(portStr);
                result = ProxySelector.validate(host, portStr, exclusionList);
            } catch (NumberFormatException e) {
                result = R.string.proxy_error_invalid_port;
            }
            if (result == 0) {
                mHttpProxy = new ProxyInfo(host, port, exclusionList);
            } else {
                return false;
            }
        } else if (selectedPosition == PROXY_PAC && mProxyPacView != null) {
            mProxySettings = ProxySettings.PAC;
            CharSequence uriSequence = mProxyPacView.getText();
            if (TextUtils.isEmpty(uriSequence)) {
                return false;
            }
            Uri uri = Uri.parse(uriSequence.toString());
            if (uri == null) {
                return false;
            }
            mHttpProxy = new ProxyInfo(uri);
        }
        return true;
    }

    private int validateIpConfigFields(StaticIpConfiguration staticIpConfiguration) {
        if (mIpAddressView == null) return 0;

        String ipAddr = mIpAddressView.getText().toString();
        if (TextUtils.isEmpty(ipAddr)) return R.string.wifi_ip_settings_invalid_ip_address;

        Inet4Address inetAddr = getIPv4Address(ipAddr);
        if (inetAddr == null || inetAddr.equals(Inet4Address.ANY)) {
            return R.string.wifi_ip_settings_invalid_ip_address;
        }

        int networkPrefixLength = -1;
        try {
            networkPrefixLength = Integer.parseInt(mNetworkPrefixLengthView.getText().toString());
            if (networkPrefixLength < 0 || networkPrefixLength > 32) {
                return R.string.wifi_ip_settings_invalid_network_prefix_length;
            }
            staticIpConfiguration.ipAddress = new LinkAddress(inetAddr, networkPrefixLength);
        } catch (NumberFormatException e) {
            // Set the hint as default after user types in ip address
            mNetworkPrefixLengthView.setText(getResources().getString(
                    R.string.wifi_network_prefix_length_hint));
        } catch (IllegalArgumentException e) {
            return R.string.wifi_ip_settings_invalid_ip_address;
        }

        String gateway = mGatewayView.getText().toString();
        if (TextUtils.isEmpty(gateway)) {
            try {
                //Extract a default gateway from IP address
                InetAddress netPart = NetworkUtils.getNetworkPart(inetAddr, networkPrefixLength);
                byte[] addr = netPart.getAddress();
                addr[addr.length - 1] = 1;
                mGatewayView.setText(InetAddress.getByAddress(addr).getHostAddress());
            } catch (RuntimeException ee) {
            } catch (java.net.UnknownHostException u) {
            }
        } else {
            InetAddress gatewayAddr = getIPv4Address(gateway);
            if (gatewayAddr == null) {
                return R.string.wifi_ip_settings_invalid_gateway;
            }
            if (gatewayAddr.isMulticastAddress()) {
                return R.string.wifi_ip_settings_invalid_gateway;
            }
            staticIpConfiguration.gateway = gatewayAddr;
        }

        String dns = mDns1View.getText().toString();
        InetAddress dnsAddr = null;

        if (TextUtils.isEmpty(dns)) {
            //If everything else is valid, provide hint as a default option
            mDns1View.setText(getResources().getString(R.string.wifi_dns1_hint));
        } else {
            dnsAddr = getIPv4Address(dns);
            if (dnsAddr == null) {
                return R.string.wifi_ip_settings_invalid_dns;
            }
            staticIpConfiguration.dnsServers.add(dnsAddr);
        }

        if (mDns2View.length() > 0) {
            dns = mDns2View.getText().toString();
            dnsAddr = getIPv4Address(dns);
            if (dnsAddr == null) {
                return R.string.wifi_ip_settings_invalid_dns;
            }
            staticIpConfiguration.dnsServers.add(dnsAddr);
        }
        return 0;
    }

    private Inet4Address getIPv4Address(String text) {
        try {
            return (Inet4Address) NetworkUtils.numericToInetAddress(text);
        } catch (IllegalArgumentException | ClassCastException e) {
            return null;
        }
    }

    WifiConfiguration getConfig() {

        WifiConfiguration config = new WifiConfiguration();

        if (mNetworkId == WifiConfiguration.INVALID_NETWORK_ID) {
            //config.SSID = AccessPoint.convertToQuotedString(mSsid);
        } else {
            config.networkId = mNetworkId;
        }

        config.setIpConfiguration(
                new IpConfiguration(mIpAssignment, mProxySettings,
                                    mStaticIpConfiguration, mHttpProxy));

        return config;
    }
}
/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/10/11|     jianhong.yang    |     task 3046476     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.wifi;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.android.settings.R;

public class PasswordEditText extends EditText {
    private Context mContext;
    private Drawable mShowPassWord;
    private boolean isPasswordshown = false;

    public PasswordEditText(Context context) {
        // TODO Auto-generated constructor stub
        super(context);
        mContext = context;
        initView();
    }

    public PasswordEditText(Context context, AttributeSet attrs) {
        // TODO Auto-generated constructor stub
        super(context, attrs);
        mContext = context;
        initView();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        if(mShowPassWord != null && event.getAction() == MotionEvent.ACTION_UP) {
            int x = (int) event.getX();
            boolean isInnerWidth = (x > (getWidth() - getTotalPaddingRight())) &&
                    (x < (getWidth() - getPaddingRight()));
                Rect rect = mShowPassWord.getBounds();
                int height = rect.height();
                int y = (int) event.getY();
                int distance = (getHeight() - height) / 2;
                boolean isInnerHeight = (y > distance) && (y < (distance + height));
                if (isInnerHeight && isInnerWidth) {
                    isPasswordshown = !isPasswordshown;
                    setPasswordVisible(isPasswordshown);
                }
        }

        return super.onTouchEvent(event);
    }

    private void setPasswordVisible(boolean visible) {
        // TODO Auto-generated method stub
        int pos = getSelectionEnd();
        setInputType(
                InputType.TYPE_CLASS_TEXT | (visible ?
                        InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD :
                            InputType.TYPE_TEXT_VARIATION_PASSWORD));
        if(pos > 0) setSelection(pos);
        if(visible) {
            Drawable shown = mContext.getResources().getDrawable(R.drawable.password_show);
            shown.setBounds(0, 0, shown.getIntrinsicWidth(),
                    shown.getIntrinsicHeight());
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1],
                    shown, getCompoundDrawables()[3]);
        } else {
            Drawable hide = mContext.getResources().getDrawable(R.drawable.password_hide);
            hide.setBounds(0, 0, hide.getIntrinsicWidth(),
                    hide.getIntrinsicHeight());
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1],
                    hide, getCompoundDrawables()[3]);
        }
    }

    private void initView() {
        mShowPassWord = getCompoundDrawables()[2];

        if (mShowPassWord == null) {
            mShowPassWord = mContext.getResources().getDrawable(R.drawable.password_hide);
        }
        mShowPassWord.setBounds(0, 0, mShowPassWord.getIntrinsicWidth(),
                mShowPassWord.getIntrinsicHeight());
    }

}

package com.android.settings.lockapp.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import mst.app.dialog.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Color;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;

import com.android.settings.InstrumentedActivity;
import com.android.settings.lockapp.adapter.CategoryAdapter;
import com.android.settings.lockapp.adapter.CategoryAdapter.AppInfoViewHolder;
import com.android.settings.lockapp.ui.AppInfoParams;
import com.android.settings.lockapp.ui.ApplicationLockSaveGuardActivity;
import com.android.settings.lockapp.ui.ApplicationLockSettingActivity;
import com.android.settings.lockapp.ui.ChooseLockPatternActivity;
import com.android.settings.lockapp.ui.LetterView;
import com.android.settings.lockapp.ui.AppSectionIndexer;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.utils.CharacterParser;
import com.android.settings.lockapp.utils.PinyinComparator;
import com.android.settings.R;

import com.android.settings.lockapp.ui.LetterView.OnLetterClickListener;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.lockapp.adapter.Category;

public class ApplicationLockActivity extends InstrumentedActivity {
    private final static String TAG = "ApplicationLockActivity";
    private PackageManager localPackageManager;
    private ListView mAppListView;
    private ProgressBar mProgressBar;
    private CategoryAdapter mCategoryAdapter;
    private RelativeLayout mLockSettings;
    private TextView mLockedAppNum;
    private FingerprintManager mFpm;
    ArrayList<AppInfoParams> localArrayList;
    private CharacterParser characterParser;
    private PinyinComparator pinyinComparator;
    private LetterView letterView;
    private AppSectionIndexer mIndexer;
    private int[] mCounts;
    private static final String[] SECTIONS = {"*","#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
            "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V","W", "X", "Y", "Z"};
    private static final String LETTERS = "*#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setMstContentView(R.layout.activity_application_lock);
        showBackIcon(true);
        initView();
        loadData();
    }

    private void initView() {
        mLockSettings = (RelativeLayout) findViewById(R.id.btn_settings);
        mAppListView = (ListView) findViewById(R.id.list);
        mAppListView.setDivider(null);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mLockedAppNum = (TextView) findViewById(R.id.locked_num);
        localPackageManager = this.getPackageManager();
        characterParser = CharacterParser.getInstance();
        setTitle(R.string.app_lock);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        pinyinComparator = new PinyinComparator();
        letterView = (LetterView) findViewById(R.id.letter_view);

        letterView.setOnLetterClickListener(new OnLetterClickListener() {
            @Override
            public void onLetterClick(String s) {
                int section = LETTERS.indexOf(s);
                int position = mIndexer.getPositionForSection(section);
                if(position != -1){
                    mAppListView.setSelection(position);
                }
            }
        });


        mLockSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApplicationLockActivity.this, ApplicationLockSettingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
/* MODIFIED-BEGIN by zheng.ding, 2016-10-10,BUG-2825800*/
//        if (shoudShowDialog()) {
//            showFingerPrintDialog();
//        }
/* MODIFIED-END by zheng.ding,BUG-2825800*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        finish();
    }

    private void loadData() {
        mAppListView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        LocalInfoAsync tTask = new LocalInfoAsync(this);
        tTask.execute();
    }

    private class LocalInfoAsync extends AsyncTask<Long, Void, ArrayList<AppInfoParams>> {
        private Context mContext;

        public LocalInfoAsync(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<AppInfoParams> doInBackground(final Long... params) {
            Intent localIntent = new Intent("android.intent.action.MAIN", null);
            localIntent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> mAllAppList = localPackageManager.queryIntentActivities(localIntent, 0);
            Collections.sort(mAllAppList, new ResolveInfo.DisplayNameComparator(localPackageManager));
            Log.d(TAG, "size:" + mAllAppList.size());
            localArrayList = new ArrayList<AppInfoParams>();
            for (int i = 0; i < mAllAppList.size(); ++i) {
                 ResolveInfo appInfo = (ResolveInfo) mAllAppList.get(i);

//                 if ("com.tct.securitycenter".equals(appInfo.activityInfo.packageName)
//                     || "com.android.contacts".equals(appInfo.activityInfo.packageName)
//                     || "com.android.dialer".equals(appInfo.activityInfo.packageName)) {
//                    continue;
//                }

                 AppInfoParams mAppInfoParams = new AppInfoParams();
                Log.d("yshcnm","packageName: "+appInfo.activityInfo.packageName +" appName: "+appInfo.loadLabel(getPackageManager()).toString());
                 mAppInfoParams.packageName = appInfo.activityInfo.packageName;
                 mAppInfoParams.appName = appInfo.loadLabel(getPackageManager())
                          .toString();
                 mAppInfoParams.appIcon = appInfo.loadIcon(getPackageManager());
                int locaState =
                        localPackageManager.getApplicationLockStatus(appInfo.activityInfo.packageName);
                if (locaState == 2 || locaState == 1) {
                    mAppInfoParams.letter = "*";
                } else {
                    if ("com.tencent.mm".equals(appInfo.activityInfo.packageName)
                            || "com.tencent.mobileqq".equals(appInfo.activityInfo.packageName)
                            || "com.tct.gallery3d".equals(appInfo.activityInfo.packageName)
                            ||"com.android.mms".equals(appInfo.activityInfo.packageName)){
                        mAppInfoParams.letter = "#";
                    }else{
                        String pinyin = characterParser.getSelling(mAppInfoParams.appName);
                        String sortString = pinyin.substring(0,1).toUpperCase();
                        if (sortString.matches("[A-Z]")){
                            mAppInfoParams.letter = sortString;
                        } else {
                            mAppInfoParams.letter = ".";
                        }
                    }
                }
                localArrayList.add(mAppInfoParams);
            }
            Collections.sort(localArrayList,pinyinComparator);
            mCounts = new int[SECTIONS.length];
            //mCounts[0] = 1;
            for (AppInfoParams Params : localArrayList) {
                int index = LETTERS.indexOf(Params.letter);
                mCounts[index]++;
            }
            return localArrayList;
        }

        @Override
        protected void onPostExecute(final ArrayList<AppInfoParams> result) {
        	int num = getLockedAppNum(result);
            mIndexer = new AppSectionIndexer(SECTIONS, mCounts);
         //   mAdapter = new SelectCityAdapter(this, mList, mIndexer);
            ArrayList<Category> listData = getData(result);
            mCategoryAdapter = new CategoryAdapter(mContext, listData, localPackageManager,mIndexer);
            mAppListView.setAdapter(mCategoryAdapter);
            mLockedAppNum.setText(num + getResources().getString(R.string.number_of_locked_application));

            mProgressBar.setVisibility(View.GONE);
            mAppListView.setVisibility(View.VISIBLE);

            mAppListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int pos, long paramLong) {
                       AppInfoViewHolder localAppInfoViewHolder = (AppInfoViewHolder) paramView.getTag();
                       final AppInfoParams localAppInfoParams = (AppInfoParams) mCategoryAdapter.getItem(pos);
                       if (localAppInfoViewHolder != null){
                           if (localAppInfoViewHolder.checkBox.isChecked()) {
                               localAppInfoViewHolder.checkBox.setChecked(false);
                               localPackageManager.setApplicationLockStatus(localAppInfoParams.packageName, 0);
                               updateLockedNum();
                           } else {
                               localAppInfoViewHolder.checkBox.setChecked(true);
                               localPackageManager.setApplicationLockStatus(localAppInfoParams.packageName, 1);
                               updateLockedNum();
                           }
                       }
                    }
                }
            );

            Log.d(TAG, "onPostExecute");
        }
    }

    private ArrayList<Category> getData( ArrayList<AppInfoParams> result) {
        ArrayList<Category> listData = new ArrayList<Category>();
        Category categoryName ;
        int index = 0;
        for(int i =0;i<mCounts.length;i++){
            if (mCounts[i]>0){//mCounts[0] =3-->A 含有3个
                String letterName =SECTIONS[i];
                categoryName = new Category(letterName);
                for(int j =0 ;j<mCounts[i];j++){
                    Log.d("yshcate",""+letterName+"  "+result.get(j+index).packageName);
                    categoryName.addItem(result.get(j+index));//分类完成
                }
                index += mCounts[i];
                listData.add(categoryName);
            }
        }

        for (int k = 0;k<listData.size();k++){
            Category a = listData.get(k);
            for (int j=0;j<a.getItemCount();j++){
                AppInfoParams app = a.getItem(j);
                Log.d("yshcndy","letterName"+app.letter + " packageName: "+app.packageName);
            }
        }
        return listData;
    }

    public void updateLockedNum() {
        int i = getLockedAppNum(localArrayList);
        mLockedAppNum.setText(i + getResources().getString(R.string.number_of_locked_application));
        //save num to settings db
        Settings.System.putInt(getContentResolver(), "tct_lock_app_num",
                i);
    }

    private int getLockedAppNum(List<AppInfoParams> paramList) {
        int i = 0;
        for (int j = 0; j < paramList.size(); ++j) {
            if (localPackageManager.getApplicationLockStatus(paramList.get(j).packageName) != 0) {
                i++;
            }
        }
        return i;
    }

    void showFingerPrintDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                this);
        alert.setMessage(R.string.fingerprint_message)
                .setPositiveButton(
                        R.string.link_fingerprint,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog,
                                    int which) {
                                mFpm = (FingerprintManager) ApplicationLockActivity.this.getSystemService(Context.FINGERPRINT_SERVICE);
                                if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, ApplicationLockActivity.this)) {
                                    Toast.makeText(ApplicationLockActivity.this,
                                            getResources().getString(R.string.already_linked),
                                            Toast.LENGTH_SHORT).show();
                                    //Intent intent = new Intent();
                                    Settings.System.putInt(getContentResolver(),
                                            "tct_fingerprint_applicationlock", 1);
                                    dialog.dismiss();
                                    SharedPreferenceUtil
                                            .editFingerPrintDialogShowed(ApplicationLockActivity.this, false);
                                } else {
                                    //Intent intent = new Intent("com.tct.ACTION_APP_FINGERPRINTSETTINGS");
                                    Intent intent = new Intent();
//                                    intent.setClassName("com.android.settings", "com.android.settings.fingerprint.FingerprintEnrollEnrolling");
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//                                    intent.putExtra("enroll_fingerprint_tag", FingerprintManager.FP_TAG_COMMON);
//                                    intent.putExtra("enroll_feature",1);
                                    intent.setClassName("com.android.settings", "com.android.settings.fingerprint.FingerprintSettings");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                    startActivity(intent);
//                                    intent.putExtra("enroll_fingerprint_tag", FingerprintManager.FP_TAG_COMMON);
//                                    intent.putExtra("enroll_feature",1);


                                    SharedPreferenceUtil
                                            .editFingerPrintDialogShowed(ApplicationLockActivity.this, false);
                                }
                            }
                        })
                .setNegativeButton(
                        R.string.password_dialog_setlater,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog,
                                    int which) {
                                dialog.dismiss();
                                SharedPreferenceUtil
                                .editFingerPrintDialogShowed(ApplicationLockActivity.this, false);
                            }
                        });
        AlertDialog mDialog = alert.create();
        mDialog.show();
    }

    private boolean shoudShowDialog() {
        boolean shouldShow = SharedPreferenceUtil.readFingerPrintDialogShowed(this);
        //int mSettingSwith = Settings.System.getInt(getContentResolver(), "tct_fingerprint_applicationlock", 0);

        FingerprintManager mFingerprintManager = (FingerprintManager) this.getSystemService(
                Context.FINGERPRINT_SERVICE);
//        final List<Fingerprint> items = mFingerprintManager
//                .tctGetEnrolledFingerprints(FingerprintManager.FP_TAG_COMMON);
//        final int fingerprintCount = items.size();
        if (shouldShow && (!mFingerprintManager.hasEnrolledFingerprints())) {//first time enter AND no fingerprint
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.applicationlockmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if(R.id.action_settings == item.getItemId()) {
//            Intent intent = new Intent(ApplicationLockActivity.this, ApplicationLockSettingActivity.class);
//            startActivity(intent);
//        }
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.FINGERPRINT;
    }

    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
}

/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.lockapp.activity;

import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.R;

import android.os.Bundle;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.view.View;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.app.ProgressDialog;

import com.android.settings.lockapp.ui.AppInfoParams;

import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.widget.TextView;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;

import mst.preference.SwitchPreference;
import mst.preference.Preference;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.PreferenceScreen;
import mst.preference.PreferenceCategory;
import android.content.pm.ActivityInfo;
import android.view.ViewGroup.LayoutParams;

public class AppLockFragment extends SettingsPreferenceFragment {
    private final static String TAG = "AppLockFragment";
    ArrayList<AppInfoParams> localArrayList;
    private View mView;
    private PackageManager localPackageManager;
    private ProgressBar mProgressBar;
    private static final float DEFAULT_FONT_SIZE = 15f;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.lock_applist);
        localPackageManager = getActivity().getPackageManager();
        if (Configuration.ORIENTATION_LANDSCAPE == getActivity().getResources().getConfiguration().orientation ){
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

    }


    @Override
    protected int getMetricsCategory() {
        return 1;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadList();
    }

    private void reloadList() {
        LocalInfoAsync tTask = new LocalInfoAsync(getPrefContext());
        tTask.execute();
    }


    private class LocalInfoAsync extends AsyncTask<Long, Void, ArrayList<AppInfoParams>> {
        private Context mContext;
        int has_locked_num = 0;
        int suggest_lock_num = 0;
        int no_lock_num = 0;
        ProgressDialog mProgressDialog;

        public LocalInfoAsync(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<AppInfoParams> doInBackground(final Long... params) {
            Intent localIntent = new Intent("android.intent.action.MAIN", null);
            localIntent.addCategory("android.intent.category.LAUNCHER");
            if (localPackageManager == null){// MODIFIED by zheng.ding, 2016-11-14,defect-2825800
                localPackageManager = getActivity().getPackageManager();
            }
            List<ResolveInfo> mAllAppList = localPackageManager.queryIntentActivities(localIntent, 0);
            Collections.sort(mAllAppList, new ResolveInfo.DisplayNameComparator(localPackageManager));
            localArrayList = new ArrayList<AppInfoParams>();
            for (int i = 0; i < mAllAppList.size(); ++i) {
                ResolveInfo appInfo = (ResolveInfo) mAllAppList.get(i);
                AppInfoParams mAppInfoParams = new AppInfoParams();
                mAppInfoParams.packageName = appInfo.activityInfo.packageName;
                mAppInfoParams.appName = appInfo.loadLabel(getPackageManager())
                        .toString();
                mAppInfoParams.appIcon = appInfo.loadIcon(getPackageManager());
                localArrayList.add(mAppInfoParams);
            }
            return localArrayList;
        }

        @Override
        protected void onPostExecute(final ArrayList<AppInfoParams> result) {
            mProgressDialog.hide();
            final PreferenceScreen screen = getPreferenceScreen();
            screen.removeAll();
            final AppCustomCategory has_locked_Category = new AppCustomCategory(getActivity());
            has_locked_Category.setTitle(R.string.has_locked);
            final AppCustomCategory suggest_locked_Category = new AppCustomCategory(getActivity());
            suggest_locked_Category.setTitle(R.string.suggest_lock);
            final AppCustomCategory not_locked_Category = new AppCustomCategory(getActivity());
            not_locked_Category.setTitle(R.string.no_lock);

            screen.addPreference(has_locked_Category);
            screen.addPreference(suggest_locked_Category);
            screen.addPreference(not_locked_Category);

            AppCustomPreference pref;
            for (AppInfoParams app : result) {

                if (isPackageLocked(app.packageName)) {
                    has_locked_num++;
                    pref = addHasLockPre(app);
                    has_locked_Category.addPreference(pref);
                } else if ("com.tencent.mm".equals(app.packageName)
                        || "com.tencent.mobileqq".equals(app.packageName)
                        || "com.tct.gallery3d".equals(app.packageName)
                        || "com.android.mms".equals(app.packageName)) {
                    suggest_lock_num++;
                    pref = addSuggestLockPre(app);
                    suggest_locked_Category.addPreference(pref);
                } else {
                    no_lock_num++;
                    pref = addNoLockPre(app);
                    not_locked_Category.addPreference(pref);
                }
            }

            if (has_locked_num == 0) {
                screen.removePreference(has_locked_Category);
            }
            if (suggest_lock_num == 0) {
                screen.removePreference(suggest_locked_Category);
            }
            if (no_lock_num == 0) {
                screen.removePreference(not_locked_Category);
            }
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = getProgressDialog();
            mProgressDialog.show();
        }
    }

    private AppCustomPreference addHasLockPre(AppInfoParams app) {
        AppCustomPreference pref = new AppCustomPreference(getPrefContext());
        pref.setPersistent(false);
        pref.setIcon(app.appIcon);
        pref.setTitle(app.appName);
        pref.setChecked(true);
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean access = (Boolean) newValue;
                if (access) {
                    Log.d("yshsw", "access: " + access);
                    localPackageManager.setApplicationLockStatus(app.packageName, 1);
                    updateLockedNum();
                } else {
                    localPackageManager.setApplicationLockStatus(app.packageName, 0);
                    updateLockedNum();
                }
                return true;
            }
        });
        return pref;
    }

    private AppCustomPreference addSuggestLockPre(AppInfoParams app) {
        AppCustomPreference pref = new AppCustomPreference(getPrefContext());
        pref.setPersistent(false);
        pref.setIcon(app.appIcon);
        pref.setTitle(app.appName);
        pref.setChecked(false);
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean access = (Boolean) newValue;
                if (access) {
                    Log.d("yshsw", "access: " + access);
                    localPackageManager.setApplicationLockStatus(app.packageName, 1);
                    updateLockedNum();
                } else {
                    localPackageManager.setApplicationLockStatus(app.packageName, 0);
                    updateLockedNum();
                }
                return true;
            }
        });
        return pref;
    }

    private AppCustomPreference addNoLockPre(AppInfoParams app) {
        AppCustomPreference pref = new AppCustomPreference(getPrefContext());
        pref.setPersistent(false);
        pref.setIcon(app.appIcon);
        pref.setTitle(app.appName);
        pref.setChecked(false);
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean access = (Boolean) newValue;
                if (access) {
                    Log.d("yshsw", "access: " + access);
                    localPackageManager.setApplicationLockStatus(app.packageName, 1);
                    updateLockedNum();
                } else {
                    localPackageManager.setApplicationLockStatus(app.packageName, 0);
                    updateLockedNum();
                }
                return true;
            }
        });
        return pref;
    }

    private boolean isPackageLocked(String paramString) {
        try {
            if (localPackageManager != null) {
                if (localPackageManager.getApplicationLockStatus(paramString) != 0) {
                    Log.d(TAG, "package name " + paramString + " true");
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception localException) {
        }
        return false;
    }

    public void updateLockedNum() {
        int i = getLockedAppNum(localArrayList);
        Settings.System.putInt(getContentResolver(), "tct_lock_app_num",
                i);
    }

    private int getLockedAppNum(List<AppInfoParams> paramList) {
        int i = 0;
        for (int j = 0; j < paramList.size(); ++j) {
            if (localPackageManager.getApplicationLockStatus(paramList.get(j).packageName) != 0) {
                i++;
            }
        }
        return i;
    }

    private ProgressDialog getProgressDialog() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(
                getActivity().getString(R.string.lock_load_title));
        progressDialog.setMessage(
                getActivity().getString(R.string.lock_load_message));
        return progressDialog;
    }

    static class AppCustomPreference extends SwitchPreference {
        LayoutParams para;

        AppCustomPreference(Context context) {
            super(context);
        }

        @Override
        public void onBindView(View view) {
            super.onBindView(view);
            ImageView icon = (ImageView) view.findViewById(android.R.id.icon);
            para = icon.getLayoutParams();
            para.height = 102;
            para.width = 102;
            icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
            icon.setLayoutParams(para);

            LinearLayout icon_frame = (LinearLayout) view.findViewById(android.R.id.icon_frame);
            icon_frame.setLayoutParams(new LinearLayout.LayoutParams(162, LinearLayout.LayoutParams.WRAP_CONTENT));

            TextView title = (TextView) view.findViewById(android.R.id.title);
            title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            title.setTextSize(16);
            title.setTextColor(0xFF000000);

        }
    }


    static class AppCustomCategory extends PreferenceCategory {
        LayoutParams para;

        AppCustomCategory(Context context) {
            super(context);
        }

        @Override
        public void onBindView(View view) {
            super.onBindView(view);
            TextView textView = (TextView) view.findViewById(com.mst.internal.R.id.preference_category_space);
            textView.setVisibility(View.GONE);

            TextView title = (TextView) view.findViewById(android.R.id.title);
            title.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            title.setTextSize(15);
            title.setTextColor(0xFFB2B5B8);

        }
    }

}

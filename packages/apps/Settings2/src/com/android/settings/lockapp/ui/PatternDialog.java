package com.android.settings.lockapp.ui;

import java.util.List;

import android.app.Dialog;
import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.SystemProperties; //MODIFIED by chunhua.liu-nb, 2016-04-12,BUG-1440608
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;
import android.app.KeyguardManager;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.ui.LockPatternView.Cell;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

public class PatternDialog extends Dialog {
    private final static String TAG = "PatternDialog";
    private String mPackageName = null;
    private PackageManager localPackageManager;
    private Context mContext;
    private LockPatternView mLockPatternView;
    private RelativeLayout mLinearLayout;
    private FingerprintManager mFpm;
    private CancellationSignal mFingerprintCancelSignal;
    private ImageView mFingerprintView;
    private KeyguardManager km;
    private TextView mForgotPassword;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private ForgotPasswordDialog pd;

    private enum Stage {
        NeedToUnlock,
        NeedToUnlockWrong,
        LockedOut
    }

    public PatternDialog(Context context) {
        super(context);
        mContext = context;
    }

    public PatternDialog(Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
    }

    public PatternDialog(Context context, int themeResId, String packageName) {
        super(context, themeResId);
        mContext = context;
        this.mPackageName = packageName;
        localPackageManager = context.getPackageManager();
    }

    private Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();
        }
    };

    protected LockPatternView.OnPatternListener mChooseNewLockPatternListener =
            new LockPatternView.OnPatternListener() {

            public void onPatternStart() {
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
                patternInProgress();
            }

            public void onPatternCleared() {
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
            }

            public void onPatternDetected(List<LockPatternView.Cell> pattern) {
                mLockPatternView.setEnabled(false);
                startCheckPattern(pattern);
                return;
            }

            private void patternInProgress() {

            }

            @Override
            public void onPatternCellAdded(List<Cell> pattern) {

            }
     };

     @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            init();
            final IntentFilter filter = new IntentFilter(
                    Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            filter.addAction(Intent.ACTION_USER_PRESENT);
            filter.addAction("android.intent.actions.CURRENT_MODE_CHANGED"); // MODIFIED by caixia.chen, 2016-05-31,BUG-2114657
            mContext.getApplicationContext().registerReceiver(mReceiver, filter);
            km = (KeyguardManager) mContext.getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
            mFpm = (FingerprintManager) mContext.getApplicationContext().getSystemService(Context.FINGERPRINT_SERVICE);
            if (!km.inKeyguardRestrictedInputMode()) {
                Log.d(TAG, "need authenticate");
                authenticateFingerPrint();
            }
        }

    private void init() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.pattern_dialog, null);
        setContentView(view);
        mLinearLayout = (RelativeLayout) findViewById(R.id.keyguard_pattern_view);
        mLockPatternView = (LockPatternView) findViewById(R.id.lockPatternView);
        mFingerprintView = (ImageView) findViewById(R.id.lock_icon);
        mForgotPassword = (TextView) findViewById(R.id.forgot);
        if (!ApplicationLockUtils.shouldForgotPasswordShowed(mContext)) {
            mForgotPassword.setVisibility(View.INVISIBLE);
        }
        mLockPatternView.setOnPatternListener(mChooseNewLockPatternListener);
        mLockPatternView.setTactileFeedbackEnabled(true);
        WallpaperManager wallpaperManager = WallpaperManager
                .getInstance(mContext);
        updateStage(Stage.NeedToUnlock);
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ApplicationLockSaveGuardActivity.class);
                intent.putExtra("from_app_forgot", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                dismiss();
            }
        });
    }

    private FingerprintManager.AuthenticationCallback mAuthenticationCallback = new AuthenticationCallback() {
        @Override
        public void onAuthenticationFailed() {
            Log.d(TAG, "onAuthenticationFailed");
        };

        @Override
        public void onAuthenticationSucceeded(AuthenticationResult result) {
            Log.d(TAG, "onAuthenticationSucceeded");
            handleFingerprintAuthenticated(result);
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Log.d(TAG, "onAuthenticationHelp");
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.d(TAG, "onAuthenticationError");
        }

        @Override
        public void onAuthenticationAcquired(int acquireInfo) {
            Log.d(TAG, "onAuthenticationAcquired");
        }
    };

    public void Dismiss() {
        Log.d(TAG, "onDismiss");
        mContext.getApplicationContext().unregisterReceiver(mReceiver);
        if (mFingerprintCancelSignal != null) {
            mFingerprintCancelSignal.cancel();
            mFingerprintCancelSignal = null;
        }
        if (pd != null) {
            pd.dismiss();
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(intent.getAction())) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        cancel();
                    }
                }, 500);
            } else if (Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
                Log.d(TAG, "Intent.ACTION_USER_PRESENT");
                authenticateFingerPrint();
            /* MODIFIED-BEGIN by caixia.chen, 2016-05-31,BUG-2114657*/
            } else if ("android.intent.actions.CURRENT_MODE_CHANGED".equals(intent.getAction())) {
                if (intent.getIntExtra("current_mode", 0) == 1) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cancel();
                        }
                    }, 500);
                }
             }
             /* MODIFIED-END by caixia.chen,BUG-2114657*/
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goHome();
            //super.onKeyDown(keyCode, event);
            return false;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return true;
    }

    public void goHome() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        mContext.startActivity(homeIntent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, 500);
    }

    private void startCheckPattern(final List<LockPatternView.Cell> pattern) {
        String patternStr = LockPatternUtilsSelf.patternToString(pattern);
        if (patternStr != null && patternStr.equals(LockPatternUtilsSelf.loadFromPreferences(mContext))) {
            updateStage(Stage.LockedOut);
            if (mPackageName != null) {
                localPackageManager.setApplicationLockStatus(mPackageName, 2);
            }
            ApplicationLockUtils.showNavibar();
          //MODIFIED by qiang.he, 2016-05-17,BUG-2024030
            dismiss();
        } else {
            updateStage(Stage.NeedToUnlockWrong);
        }
    }

    private void updateStage(Stage stage) {
        switch (stage) {
            case NeedToUnlock:
                mLockPatternView.setEnabled(true);
                mLockPatternView.enableInput();
                mLockPatternView.clearPattern();
                break;
            case NeedToUnlockWrong:
                //Toast.makeText(mContext, "WRONG", Toast.LENGTH_SHORT).show();
                mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                mLockPatternView.setEnabled(true);
                mLockPatternView.enableInput();
                break;
            case LockedOut:
                mLockPatternView.clearPattern();
                mLockPatternView.setEnabled(false); // appearance of being disabled
                break;
        }
    }

    private boolean isFingerprintDisabled(int userId) {
        final DevicePolicyManager dpm =
                (DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
        return dpm != null && (dpm.getKeyguardDisabledFeatures(null, userId)
                    & DevicePolicyManager.KEYGUARD_DISABLE_FINGERPRINT) != 0;
    }

    private void handleFingerprintAuthenticated(AuthenticationResult result) {
        int fpid = 0;
        int tag = 0;
        if (result.getFingerprint() == null) {
            Log.e(TAG, "fingerprint == null");
        } else {
            fpid = result.getFingerprint().getFingerId();
            tag = mFpm.tctGetTagForFingerprintId(fpid);
        }
        if (isFingerprintDisabled(0)) {
            Log.d(TAG, "Fingerprint disabled by DPM for userId: ");
            return;
        }
        if (tag == FingerprintManager.FP_TAG_COMMON) {
            //to be done
            Log.d(TAG, "AuthenticationSucceeded");
            if (mPackageName != null) {
                localPackageManager.setApplicationLockStatus(mPackageName, 2);
            }
            ApplicationLockUtils.showNavibar();
          //MODIFIED by qiang.he, 2016-05-17,BUG-2024030
            dismiss();
        } else {
            //Toast.makeText(mContext, "验证失败", Toast.LENGTH_SHORT).show();
            authenticateFingerPrint();
        }
    }

    private void authenticateFingerPrint() {
        if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, mContext)
                && ApplicationLockUtils.isFingerPrint4ApplicationLockOn(mContext)) {
            if (mFingerprintCancelSignal != null) {
                mFingerprintCancelSignal.cancel();
            }
            mFingerprintView.setVisibility(View.VISIBLE);
            mFingerprintCancelSignal = new CancellationSignal();
            mFpm.authenticate(null, mFingerprintCancelSignal, 0, mAuthenticationCallback, null, 0);
            //setFingerprintRunningState(FINGERPRINT_STATE_RUNNING);
        } else {
            mFingerprintView.setVisibility(View.INVISIBLE);
        }
    }
}

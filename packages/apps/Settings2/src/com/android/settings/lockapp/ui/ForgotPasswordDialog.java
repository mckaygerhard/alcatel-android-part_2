package com.android.settings.lockapp.ui;

import mst.app.dialog.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.settings.R;
import com.android.settings.lockapp.adapter.ApplicationLockArrayAdapter;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

public class ForgotPasswordDialog extends AlertDialog implements View.OnClickListener{
    private final static String TAG = "ForgotPasswordDialog";
    private Context mContext;
    private Button mConfirmButton;
    private Button mCancelButton;
    private EditText mPasswordEntry;
    private TextView mQuesTitle;
    private static int mChoose;
    private String[] mStringArray;
    private TextView mNote;

    public ForgotPasswordDialog(Context context) {
        super(context);
        mContext = context;
    }

    public ForgotPasswordDialog(Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.forgot_password_dialog, null);
        setContentView(view);
        final IntentFilter filter = new IntentFilter(
                Intent.ACTION_SCREEN_OFF);
        mContext.getApplicationContext().registerReceiver(mReceiver, filter);
        mQuesTitle = ((TextView) findViewById(R.id.spinner));
        mPasswordEntry = ((EditText) findViewById(R.id.password_entry));
        mNote = ((TextView) findViewById(R.id.note_area));
        mConfirmButton = ((Button) findViewById(R.id.confirm_button));
        mCancelButton = ((Button) findViewById(R.id.cancel_button));
        mConfirmButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);

        mNote.setVisibility(View.GONE);

        mPasswordEntry.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                      int after) {
                            Log.d(TAG, "beforeTextChanged");
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                           Log.d(TAG, "onTextChanged");
                           mNote.setVisibility(View.GONE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                           Log.d(TAG, "afterTextChanged");
                        }

        });

        mChoose = SharedPreferenceUtil.readQuestion(mContext);
        mStringArray = mContext.getResources().getStringArray(R.array.password_questions);
        mQuesTitle.setText(mStringArray[mChoose]);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button :
                dismiss();
                break;
            case R.id.confirm_button :
                if (mChoose == SharedPreferenceUtil.readQuestion(mContext)) {
                    String password = mPasswordEntry.getText().toString();
                    if (password != null) {
                        if (password.equals(SharedPreferenceUtil.readQuestionAnswer(mContext)))  {
                            Intent intent = new Intent(mContext, ChooseLockPatternActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                            SharedPreferenceUtil.editShouldReturn(mContext, true);
                            dismiss();
                        } else {
                            //Toast.makeText(mContext, "密保问题错误", Toast.LENGTH_SHORT).show();
                            Log.i(TAG, "Incorrect answer");
                            mNote.setText(mContext.getResources().getString(R.string.wrong_saveguard));
                            mNote.setVisibility(View.VISIBLE);
                            //Toast.makeText(mContext,
                            //        mContext.getResources().
                            //        getString(R.string.wrong_saveguard),Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    //Toast.makeText(mContext, "密保问题错误", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    public void goHome() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        mContext.startActivity(homeIntent);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                dismiss();
            }
        }
    };

    public void Dismiss() {
        Log.d(TAG, "onDismiss");
        mContext.getApplicationContext().unregisterReceiver(mReceiver);
    }
}

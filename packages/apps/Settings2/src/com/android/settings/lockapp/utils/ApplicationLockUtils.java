package com.android.settings.lockapp.utils;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;
//[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,06/12/2016,2114657
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.provider.Settings;
//[BUGFIX]-Add-END by TCTNB.caixia.chen

public class ApplicationLockUtils {
    private final static String TAG = "ApplicationLockUtils";

    public static boolean isUnlockWithFingerprintPossible(int userId, FingerprintManager mFpm, Context context) {
        return mFpm != null && mFpm.isHardwareDetected() && !isFingerprintDisabled(userId, context)
                && mFpm.tctGetEnrolledFingerprints(userId).size() > 0; // MODIFIED by kai.wang1, 2016-07-20,BUG-2545780
    }

    public static boolean isFingerprintDisabled(int userId, Context context) {
        final DevicePolicyManager dpm =
                (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        return dpm != null && (dpm.getKeyguardDisabledFeatures(null, userId)
                & DevicePolicyManager.KEYGUARD_DISABLE_FINGERPRINT) != 0;
    }

    public static boolean isFingerPrint4ApplicationLockOn(Context context) {
        int isTurnOn = Settings.System.getInt(context.getContentResolver(), "tct_fingerprint_applicationlock", 0);
        return isTurnOn == 1 ? true : false;
    }

    public static boolean shouldForgotPasswordShowed(Context context) {
        String mAnswer = SharedPreferenceUtil.readQuestionAnswer(context);
        if (mAnswer != null && !"".equals(mAnswer)) {
            return true;
        } else {
            return false;
        }
    }

    //MODIFIED-BEGIN by qiang.he, 2016-05-17,BUG-2024030
    public static void showNavibar() {
        /*MODIFIED-BEGIN by chunhua.liu-nb, 2016-04-12,BUG-1440608*/
        String hideNotibar = SystemProperties.get("persist.sys.hide", "0");
        Log.d(TAG, "hideNotibar = " + hideNotibar);
        if (hideNotibar.equals("0")) {
            Log.d(TAG, "NavibarNOtHide");
            SystemProperties.set("persist.sys.show.navi", "1");
        }
        /*MODIFIED-END by chunhua.liu-nb,BUG-1440608*/
    }
    //MODIFIED-END by qiang.he, 2016-05-17,BUG-2024030

    //[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,06/12/2016,2114657
    public static boolean isApplicationKilled(Context context, String packageName) {
        boolean killed = false;
        boolean isPrivacyMode = (Settings.System.getInt(context.getContentResolver(), "current_phone_mode", 0) == 1);

        if (isPrivacyMode) {
            long switchTime = Settings.System.getLong(context.getContentResolver(), "switch_to_privacy_mode_timestamp", 0);
            long timestamp = System.currentTimeMillis() - switchTime;
            if (timestamp < 10000) {
                ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                List<RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(5);
                if (runningTaskInfos != null) {
                    boolean running = false;
                    for (int i = 0; i < runningTaskInfos.size(); i++) {
                        RunningTaskInfo info = runningTaskInfos.get(i);
                        if (packageName != null
                                && (packageName.equals(info.topActivity.getPackageName())
                                || packageName.equals(info.baseActivity.getPackageName()))) {
                            running = true;
                            break;
                        }
                    }
                    killed = !running;
                }
            }
            //Log.w("ccxccx","isApplicationKilled = " + killed + ", timestamp = " + timestamp + ", isPrivacyMode = " + isPrivacyMode);
        }
        return killed;
    }
    //[BUGFIX]-Add-END by TCTNB.caixia.chen

    public static boolean isLockedPackageName(PackageManager mPM, String packageName) {//判断对应的应用是否属于需要加锁应用
        int lockState =
                mPM.getApplicationLockStatus(packageName);
        if (lockState == 1) {
            return true;//需显示解锁界面的应用
        }
        return false;
    }

    public static boolean hasUnLockedPackageName(PackageManager mPM, String packageName) {//判断对应apk lockstate ==2
        int lockState =
                mPM.getApplicationLockStatus(packageName);
        if (lockState == 2) {
            return true;//已解锁应用
        }
        return false;//未加锁应用
    }
}

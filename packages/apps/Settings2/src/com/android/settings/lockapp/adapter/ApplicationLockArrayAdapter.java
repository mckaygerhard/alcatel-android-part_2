package com.android.settings.lockapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ApplicationLockArrayAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private String[] mStringArray;

    public ApplicationLockArrayAdapter(Context context, String[] objects) {
        super(context,android.R.layout.simple_spinner_item, objects);
        mContext = context;
        mStringArray = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }
        
        TextView mTV = (TextView) convertView.findViewById(android.R.id.text1);
        mTV.setText(mStringArray[position]);
        String str = mTV.getText().toString();
        mTV.setTextColor(0xff333333);
        mTV.setBackgroundColor(0xfff8f8f8);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        
        TextView mTV = (TextView) convertView.findViewById(android.R.id.text1);
        mTV.setText(mStringArray[position]);
        String str1 = mTV.getText().toString();
        mTV.setTextColor(0xff333333);
        mTV.setBackgroundColor(0xfff8f8f8);
        return convertView;
    }
}

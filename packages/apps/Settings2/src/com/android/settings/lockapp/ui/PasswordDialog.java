package com.android.settings.lockapp.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;

import com.android.settings.R;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.utils.StringUtils;

public class PasswordDialog extends Dialog {
    public static final String TAG = "PasswordDialog";
    private static final int SET_TEXTVIEW = 1;
    private static final int SET_EDITTEXT = 2;
    private String mPackageName = null;
    private PackageManager localPackageManager;
    private RelativeLayout mLinearLayout;
    private Context mContext;
    private Button mButton0;
    private Button mButton1;
    private Button mButton2;
    private Button mButton3;
    private Button mButton4;
    private Button mButton5;
    private Button mButton6;
    private Button mButton7;
    private Button mButton8;
    private Button mButton9;
    private TextView textView,title_area;
    private EditText editText;
    private ImageButton mOkButton;
    private Button mDeleteButton;
    private TextView mForgotText;
    private ArrayList<Button> buttonArray = new ArrayList<Button>();
    private FingerprintManager mFpm;
    private KeyguardManager km;
    private CancellationSignal mFingerprintCancelSignal;
    private ImageView mFingerprintView;
    private LinearLayout mDialpadLinearLayout;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private ForgotPasswordDialog pd;
    private String mPasswordConfirmStr = "";
    private ArrayList<TextView> mPasswordEntryArray = new ArrayList<TextView>();
    private FingerprintManager.AuthenticationCallback mAuthenticationCallback = new AuthenticationCallback() {
        @Override
        public void onAuthenticationFailed() {
            Log.d(TAG, "onAuthenticationFailed");
        };

        @Override
        public void onAuthenticationSucceeded(AuthenticationResult result) {
            Log.d(TAG, "onAuthenticationSucceeded");
            handleFingerprintAuthenticated(result);
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Log.d(TAG, "onAuthenticationHelp");
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.d(TAG, "onAuthenticationError");
        }

        @Override
        public void onAuthenticationAcquired(int acquireInfo) {
            Log.d(TAG, "onAuthenticationAcquired");
        }
    };

    public PasswordDialog(Context context) {
        super(context);
        mContext = context;
    }

    public PasswordDialog(Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
    }

    public PasswordDialog(Context context, int themeResId, String packageName) {
        super(context, themeResId);
        mContext = context;
        this.mPackageName = packageName;
        localPackageManager = context.getPackageManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        final IntentFilter filter = new IntentFilter(
                Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction("android.intent.actions.CURRENT_MODE_CHANGED"); // MODIFIED by caixia.chen, 2016-05-31,BUG-2114657
        mContext.getApplicationContext().registerReceiver(mReceiver, filter);
        km = (KeyguardManager) mContext.getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
        mFpm = (FingerprintManager) mContext.getApplicationContext().getSystemService(Context.FINGERPRINT_SERVICE);
        if (!km.inKeyguardRestrictedInputMode()) {
            Log.d(TAG, "need authenticate");
            authenticateFingerPrint();
        }
        mForgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ApplicationLockSaveGuardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("from_app_forgot", true);
                mContext.startActivity(intent);
                dismiss();
            }
        });
    }

    public void init() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.password_dialog, null);
        setContentView(view);
        mLinearLayout = (RelativeLayout) findViewById(R.id.keyguard_password_view);
        mDialpadLinearLayout = (LinearLayout) findViewById(R.id.number_dialpad_linearlayout);
        mForgotText = (TextView)findViewById(R.id.forgot);
        title_area = (TextView)findViewById(R.id.title_area);
        if (!ApplicationLockUtils.shouldForgotPasswordShowed(mContext)) {
            mForgotText.setVisibility(View.INVISIBLE);
        }
        mFingerprintView = (ImageView) findViewById(R.id.lock_icon);

        mDeleteButton = (Button)findViewById(R.id.mDeleteButton);
        mOkButton = (ImageButton) findViewById(R.id.mOkButton);
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry1));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry2));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry3));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry4));
        mDeleteButton = (Button)findViewById(R.id.mDeleteButton);
        mOkButton = (ImageButton) findViewById(R.id.mOkButton);
        buttonArray.add((Button)findViewById(R.id.mButton0));
        buttonArray.add((Button)findViewById(R.id.mButton1));
        buttonArray.add((Button)findViewById(R.id.mButton2));
        buttonArray.add((Button)findViewById(R.id.mButton3));
        buttonArray.add((Button)findViewById(R.id.mButton4));
        buttonArray.add((Button)findViewById(R.id.mButton5));
        buttonArray.add((Button)findViewById(R.id.mButton6));
        buttonArray.add((Button)findViewById(R.id.mButton7));
        buttonArray.add((Button)findViewById(R.id.mButton8));
        buttonArray.add((Button)findViewById(R.id.mButton9));
        changeButton();
        WallpaperManager wallpaperManager = WallpaperManager
                .getInstance(mContext);
    }

    public void changeButton(){
        for(int i=0;i<10;i++){
            Button button = buttonArray.get(i);
            button.setText(String.valueOf(i));
            final int j = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPasswordConfirmStr.length() < 4) {
                        mPasswordConfirmStr = mPasswordConfirmStr+j;
                        updateEditView();
                    }
                }
            });
        }
        mDeleteButton.getBackground().setAlpha(0);
//        mOkButton.getBackground().setAlpha(0); // MODIFIED by kai.wang1, 2016-07-22,BUG-2570987
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePassword();
            }
        });
    }

    private void updateEditView() {
        if (mPasswordConfirmStr.length() > 0) {
            for (int n=0;n<mPasswordConfirmStr.length();n++) {
                TextView textView = mPasswordEntryArray.get(n);
                textView.setText("*");
            }
            if (mPasswordConfirmStr.length() == 4) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        handleNext();
                    }
                }, 300);
            }
        }
    }

    private void clearEditView(){
        for (int n=0;n<4;n++) {
            TextView textView = mPasswordEntryArray.get(n);
            mPasswordConfirmStr = "";
            textView.setText("");
        }
    }

    private void deletePassword() {
        int n = mPasswordConfirmStr.length();
        if (n > 0) {
            mPasswordEntryArray.get(n-1).setText("");
            mPasswordConfirmStr = mPasswordConfirmStr.substring(0, n-1);
        }
    }

    public void handleNext() {
        final String pin = mPasswordConfirmStr;
        String md5 = StringUtils.toMD5(pin);
        if (md5 != null && md5.equals(SharedPreferenceUtil.readNumPassword(mContext))) {
            if (mPackageName != null) {
                localPackageManager.setApplicationLockStatus(mPackageName, 2);
            }
            ApplicationLockUtils.showNavibar();
          //MODIFIED by qiang.he, 2016-05-17,BUG-2024030
            dismiss();
        } else {
            clearEditView();
//            Toast.makeText(mContext,mContext.getResources().
//                    getString(R.string.wrong_password_input_again),
//                    Toast.LENGTH_SHORT).show();
            title_area.setText(R.string.wrong_password);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goHome();
            //super.onKeyDown(keyCode, event);
            return false;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return true;
    }

    public void goHome() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        mContext.startActivity(homeIntent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                dismiss();
            }
        }, 300);
    }

    public void Dismiss() {
        mContext.getApplicationContext().unregisterReceiver(mReceiver);
        if (mFingerprintCancelSignal != null) {
            mFingerprintCancelSignal.cancel();
            mFingerprintCancelSignal = null;
        }
        if (pd != null) {
            pd.dismiss();
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(intent.getAction())) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        cancel();
                    }
                }, 300);
            } else if (Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
                Log.d(TAG, "Intent.ACTION_USER_PRESENT");
                authenticateFingerPrint();
            /* MODIFIED-BEGIN by caixia.chen, 2016-05-31,BUG-2114657*/
            } else if ("android.intent.actions.CURRENT_MODE_CHANGED".equals(intent.getAction())) {
                if (intent.getIntExtra("current_mode", 0) == 1) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cancel();
                        }
                    }, 300);
                }
             }
             /* MODIFIED-END by caixia.chen,BUG-2114657*/
        }
    };

    private boolean isFingerprintDisabled(int userId) {
        final DevicePolicyManager dpm =
                (DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
        return dpm != null && (dpm.getKeyguardDisabledFeatures(null, userId)
                    & DevicePolicyManager.KEYGUARD_DISABLE_FINGERPRINT) != 0;
    }

    private void handleFingerprintAuthenticated(AuthenticationResult result) {
        int fpid = 0;
        int tag = 0;
        if (result.getFingerprint() == null) {
            Log.e(TAG, "fingerprint == null");
        } else {
            fpid = result.getFingerprint().getFingerId();
            tag = mFpm.tctGetTagForFingerprintId(fpid);
        }
        if (isFingerprintDisabled(0)) {
            Log.d(TAG, "Fingerprint disabled by DPM for userId: ");
            return;
        }
        if (tag == FingerprintManager.FP_TAG_COMMON) {
            //to be done
            Log.d(TAG, "AuthenticationSucceeded");
            if (mPackageName != null) {
                localPackageManager.setApplicationLockStatus(mPackageName, 2);
            }
            ApplicationLockUtils.showNavibar();
          //MODIFIED by qiang.he, 2016-05-17,BUG-2024030
            dismiss();
        } else {
            //Toast.makeText(mContext, "验证失败", Toast.LENGTH_SHORT).show();
            authenticateFingerPrint();
        }
    }

    private void authenticateFingerPrint() {
        if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, mContext)
                && ApplicationLockUtils.isFingerPrint4ApplicationLockOn(mContext)) {
            if (mFingerprintCancelSignal != null) {
                mFingerprintCancelSignal.cancel();
            }
            mFingerprintView.setVisibility(View.VISIBLE);
            mFingerprintCancelSignal = new CancellationSignal();
            mFpm.authenticate(null, mFingerprintCancelSignal, 0, mAuthenticationCallback, null, 0);
            //setFingerprintRunningState(FINGERPRINT_STATE_RUNNING);
        } else {
            mFingerprintView.setVisibility(View.INVISIBLE);
        }
    }
}

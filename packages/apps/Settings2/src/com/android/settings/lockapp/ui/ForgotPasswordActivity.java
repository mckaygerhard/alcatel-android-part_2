/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.lockapp.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

public class ForgotPasswordActivity extends Activity implements
	OnEditorActionListener, TextWatcher {

	    private Button mCompleteButton;
	    private Button mSetLaterButton;
	    private EditText mPasswordEntry;
	    private Spinner mSpinner;
	    private static int mChoose;
	    private TextView mspinner_text;

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
	        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
	        setContentView(R.layout.activity_application_lock_savegurad);
	        getActionBar().setTitle(R.string.saveguard);
	        mSpinner = ((Spinner) findViewById(R.id.spinner));
	        mspinner_text = ((TextView) findViewById(R.id.spinner_text));
	        mPasswordEntry = ((EditText) findViewById(R.id.password_entry));
	        mPasswordEntry.setOnEditorActionListener(this);
	        mPasswordEntry.addTextChangedListener(this);
	        mSetLaterButton = ((Button) findViewById(R.id.setlater_button));
	        mCompleteButton = ((Button) findViewById(R.id.next_button));
	        mCompleteButton.setEnabled(false);

	        mSetLaterButton.setVisibility(View.GONE);
            mSpinner.setVisibility(View.GONE);
            mspinner_text.setVisibility(View.VISIBLE);
            mspinner_text.setText(getResources()
                    .getStringArray(R.array.password_questions)[SharedPreferenceUtil.readQuestion(this)]);

	        mCompleteButton.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View paramView) {
	                final String answer = mPasswordEntry.getText().toString();

                    if (answer.equals(SharedPreferenceUtil.readQuestionAnswer(ForgotPasswordActivity.this)))  {
                        Intent intent = new Intent(ForgotPasswordActivity.this, ChooseLockPatternActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        SharedPreferenceUtil.editShouldReturn(ForgotPasswordActivity.this, true);
                        ForgotPasswordActivity.this.finish();
                    } else {
                        //Toast.makeText(ForgotPasswordActivity.this, "密保问题错误", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ForgotPasswordActivity.this,
                                ForgotPasswordActivity.this.getResources().
                                getString(R.string.wrong_saveguard),Toast.LENGTH_SHORT).show();
                    }
	            }
	        });

	        new Timer().schedule(new TimerTask() {
	            public void run() {
	                ((InputMethodManager) ForgotPasswordActivity.this
	                        .getSystemService("input_method")).showSoftInput(
	                        mPasswordEntry, 0);
	            }
	        }, 500);
	    }
	    
		private void savePasswordQuestion(int position, String str) {
	        SharedPreferenceUtil.editQuestion(this, position);
	        SharedPreferenceUtil.editQuestionAnswer(this, str);
	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count,
	            int after) {
	    }

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count) {
	    }

	    @Override
	    public void afterTextChanged(Editable s) {
	        String password = mPasswordEntry.getText().toString();
	        if (password != null && password != "") {
	            mCompleteButton.setEnabled(true);
	        }
	    }

	    @Override
	    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	        return false;
	    }

	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK ) {
	            finish();
	            return true;
	        }
	        return super.onKeyDown(keyCode, event);
	    }

}

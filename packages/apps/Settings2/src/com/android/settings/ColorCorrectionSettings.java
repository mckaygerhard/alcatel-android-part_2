/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.os.SystemProperties;
import mst.preference.Preference;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.PreferenceActivity;
import mst.preference.SwitchPreference;

import com.android.settings.AppWidgetLoader.ItemConstructor;
import com.android.settings.R;
import com.android.settings.location.RadioButtonPreference;
import com.android.settings.location.RadioButtonPreference.OnClickListener;
import com.android.settings.accessibility.DaltonizerListViewAdapter;
import android.provider.Settings;
import android.view.accessibility.AccessibilityManager;
import mst.widget.toolbar.Toolbar;

public class ColorCorrectionSettings extends PreferenceActivity implements OnItemClickListener, OnPreferenceChangeListener{
    private static final String TAG="ColorCorrectionSettings";
    private int mCurrentSelected;
    private ListView mDaltonizerTypeListView;
    private DaltonizerListViewAdapter mAdapter;
    private SwitchPreference mDaltonizerToggleBar;
    private String[] mDaltonizerTypes;
    private String[] mDaltonizerTypesValue;
    private String[] mDaltonizerTypesSummary;

    private static final String ENABLED = Settings.Secure.ACCESSIBILITY_DISPLAY_DALTONIZER_ENABLED;
    private static final String TYPE = Settings.Secure.ACCESSIBILITY_DISPLAY_DALTONIZER;
    private static final int DEFAULT_TYPE = AccessibilityManager.DALTONIZER_CORRECT_DEUTERANOMALY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setTitle(R.string.accessibility_display_daltonizer_preference_title);
        addPreferencesFromResource(R.xml.daltonizer_toggle_preference);
        setContentView(R.layout.daltonizer_mode_settings);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle(R.string.accessibility_display_daltonizer_preference_title);
        setActionBar(myToolbar);
        //getActionBar().setHomeAsUpIndicator(com.mst.R.drawable.ic_set);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mDaltonizerToggleBar = (SwitchPreference) findPreference("daltonizer_toggle_bar");
        mDaltonizerToggleBar.setOnPreferenceChangeListener(this);

        mDaltonizerToggleBar.setChecked(
                Settings.Secure.getInt(getContentResolver(), ENABLED, 0) == 1);
        mDaltonizerTypes=getResources().getStringArray(R.array.daltonizer_type);
        mDaltonizerTypesSummary=getResources().getStringArray(R.array.daltonizer_type_summary);
        mDaltonizerTypesValue=getResources().getStringArray(R.array.daltonizer_type_values);

        mDaltonizerTypeListView = (ListView) findViewById(R.id.daltonizer_mode_list);
        mDaltonizerTypeListView.setOnItemClickListener(this);
        mAdapter = new DaltonizerListViewAdapter(this, getListItems());
        mDaltonizerTypeListView.setAdapter(mAdapter);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                saveSettings();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private List<Map<String, Object>> getListItems() {
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        mCurrentSelected=getSelectIndex();
        int index = 0;
        for (index=0;index<3;index++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", mDaltonizerTypes[index]);
            map.put("summary",mDaltonizerTypesSummary[index]);
            if (index == mCurrentSelected) {
                map.put("checked", true);
            } else {
                map.put("checked", false);
            }
            listItems.add(map);
        }
        return listItems;
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == mDaltonizerTypes.length) {
             return;
        }
        mCurrentSelected = position;
        saveSettings();
        int firstPos = mDaltonizerTypeListView.getFirstVisiblePosition();
        mAdapter = new DaltonizerListViewAdapter(this, getListItems());
        mDaltonizerTypeListView.setAdapter(mAdapter);
        mDaltonizerTypeListView.setSelection(firstPos);
    }

    private boolean saveSettings() {
        if (indexIsLegal(mCurrentSelected)) {
            Settings.Secure.putInt(getContentResolver(), TYPE, Integer.parseInt((String)mDaltonizerTypesValue[mCurrentSelected]));
        }
        return true;
    }
    public boolean indexIsLegal(int index){
        if ((index >= 0)&&(index<3)) return true;
        else return false;
    }
    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if(preference == mDaltonizerToggleBar){
            Settings.Secure.putInt(getContentResolver(), ENABLED, (boolean)newValue ? 1 : 0);
        }
        return true;
    }

    public int getSelectIndex(){
        final String value = Integer.toString(
                Settings.Secure.getInt(getContentResolver(), TYPE, DEFAULT_TYPE));
        if(value.equals(mDaltonizerTypesValue[0])) return 0;
        else if (value.equals(mDaltonizerTypesValue[1])) return 1;
        else if (value.equals(mDaltonizerTypesValue[2])) return 2;
        else {
            android.util.Log.d(TAG, "getSelectIndex: illegal value= "+value);
            return 0;
        }
    }
}

/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/11/17|     jianhong.yang    |     task 3476360     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.util.Log;

import android.os.Binder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.net.INetworkStatsService;

public class HotSpotTrafficReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = HotSpotTrafficService.DEBUG;
    private static final String TAG = "HotSpotTrafficReceiver";

    private static int apState = WifiManager.WIFI_AP_STATE_FAILED;
    private INetworkStatsService mStatsService = INetworkStatsService.Stub.asInterface(ServiceManager.getService(Context.NETWORK_STATS_SERVICE));

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Binder.getCallingUid() > 1000) return; //Only system application can start this
        String action = intent.getAction();
        if (WifiManager.WIFI_AP_STATE_CHANGED_ACTION.equals(action)) {
            int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);
            if (DEBUG) Log.d(TAG,"wifi ap state is " + state);
            if (WifiManager.WIFI_AP_STATE_ENABLED == state) {
                if (apState != state) {
                    apState = state;
                   /* try {
                        long startPoint = mStatsService.getTetherStatsBytes();
                        Settings.Global.putLong(context.getContentResolver(),
                                HotSpotTrafficService.HOTSPOT_START_POINT_DATA_USAGE_NUM,
                                startPoint);
                    } catch (RemoteException e) {
                        throw new RuntimeException(e);
                    }*/
                    if(Settings.Global.getInt(context.getContentResolver(),
                            TetherSettings.KEY_RESTRICT_DATA_TRAFFIC,0) == 1) {
                        if (DEBUG) Log.d(TAG,"wifi ap is enabled, start hotspotTrafficService");
                        startHotspotTrafficStats(context);
                    }
                }
            } else if (WifiManager.WIFI_AP_STATE_DISABLING == state || WifiManager.WIFI_AP_STATE_DISABLED == state) {
                apState = state;
            }
        }
    }

    private void startHotspotTrafficStats(Context context) {
        Intent intent = new Intent("com.android.settings.START_HOTSPOT_TRAFFIC_SERVICE");
        intent.setPackage("com.android.settings");
        context.startService(intent);
    }
}

/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/11/17|     jianhong.yang    |     task 3476360     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings;

public interface HotSpotTrafficListener {
    public void onTrafficChange(long totalBytes);
}

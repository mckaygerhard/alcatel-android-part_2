package com.android.settings;

import android.os.UserHandle;
import android.content.Context;
import android.content.Intent;
import android.os.SystemProperties;
import com.android.internal.telephony.TelephonyProperties;
import android.provider.Settings;
import com.android.settingslib.WirelessUtils;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.Message;
import com.android.internal.telephony.PhoneStateIntentReceiver;

import android.widget.Switch;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.util.Log;

public class FlyModeEnabler {
    private final Context mContext;
    private Switch mSwitch;
    private PhoneStateIntentReceiver mPhoneStateReceiver;
    private static final int EVENT_SERVICE_STATE_CHANGED = 3;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_SERVICE_STATE_CHANGED:
                    onAirplaneModeChanged();
                    break;
            }
        }
    };

    public FlyModeEnabler(Context context, Switch sw) {
        mContext = context;
        mSwitch = sw;
        mPhoneStateReceiver = new PhoneStateIntentReceiver(mContext, mHandler);
        mPhoneStateReceiver.notifyServiceState(EVENT_SERVICE_STATE_CHANGED);
    }

    public void setSwitch(Switch sw) {
        if (mSwitch == sw) return;
        mSwitch = sw;
        mSwitch.setOnCheckedChangeListener(FlyModeListener);
        setSwitchStatus();
    }

    private OnCheckedChangeListener FlyModeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
            if (Boolean.parseBoolean(SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
            } else {
                setAirplaneModeOn(arg1);
            }
        }
    };

    private void setAirplaneModeOn(boolean enabling) {
        Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, enabling ? 1 : 0);
        mSwitch.setChecked(enabling);

        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", enabling);
        mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
    }

    private void onAirplaneModeChanged() {
        if (mSwitch.isChecked() !=  WirelessUtils.isAirplaneModeOn(mContext)) {
            mSwitch.setChecked(WirelessUtils.isAirplaneModeOn(mContext));
        }
    }

    private ContentObserver mAirplaneModeObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            onAirplaneModeChanged();
        }
    };

    public void resume() {
        setSwitchStatus();
        mSwitch.setChecked(WirelessUtils.isAirplaneModeOn(mContext));
        mSwitch.setOnCheckedChangeListener(FlyModeListener);
        mPhoneStateReceiver.registerIntent();
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(Settings.Global.AIRPLANE_MODE_ON), true,
                mAirplaneModeObserver);
    }

    public void setSwitchStatus() {
        if (mSwitch.isChecked() !=  WirelessUtils.isAirplaneModeOn(mContext)) {
            mSwitch.setChecked(WirelessUtils.isAirplaneModeOn(mContext));
        }
    }

    public void pause() {
        mSwitch.setOnCheckedChangeListener(null);
        mPhoneStateReceiver.unregisterIntent();
        mContext.getContentResolver().unregisterContentObserver(mAirplaneModeObserver);
    }

}

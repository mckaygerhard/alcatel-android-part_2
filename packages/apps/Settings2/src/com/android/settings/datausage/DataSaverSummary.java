/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.android.settings.datausage;

import android.app.Application;
import android.os.Bundle;
import mst.preference.Preference;
import mst.preference.SwitchPreference;
import mst.preference.Preference.OnPreferenceChangeListener;
import android.widget.Switch;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.applications.AppStateBaseBridge.Callback;
import com.android.settings.datausage.DataSaverBackend.Listener;
import com.android.settings.widget.SwitchBar;
import com.android.settings.widget.SwitchBar.OnSwitchChangeListener;
import com.android.settingslib.applications.ApplicationsState;
import com.android.settingslib.applications.ApplicationsState.AppEntry;
import com.android.settingslib.applications.ApplicationsState.Callbacks;
import com.android.settingslib.applications.ApplicationsState.Session;
import com.android.settings.TctV7WidgetPreference;
import android.util.Log;

import java.util.ArrayList;

public class DataSaverSummary extends SettingsPreferenceFragment
        implements OnPreferenceChangeListener, Listener, Callback, Callbacks {//Modify by Dingyi 2016/10/25 for task 2827906

    private static final String KEY_DATA_SAVER_ENABLE = "data_saver_enable";//Add by Dingyi 2016/10/25 for task 2827906
    private static final String KEY_UNRESTRICTED_ACCESS = "unrestricted_access";

//    private SwitchBar mSwitchBar;//Delete by Dingyi 2016/10/25 for task 2827906
    private DataSaverBackend mDataSaverBackend;
    private SwitchPreference mDataSaverEnablePreference;
    private TctV7WidgetPreference mUnrestrictedAccess;//Modify by Dingyi 2016/10/25 for task 2827906
    private ApplicationsState mApplicationsState;
    private AppStateDataUsageBridge mDataUsageBridge;
    private Session mSession;

    // Flag used to avoid infinite loop due if user switch it on/off too quicky.
    private boolean mSwitching;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.data_saver);
        //Modify begin by Dingyi 2016/10/25 for task 2827906
        mDataSaverEnablePreference = (SwitchPreference)findPreference(KEY_DATA_SAVER_ENABLE);
        mDataSaverEnablePreference.setOnPreferenceChangeListener(this);
        mUnrestrictedAccess = (TctV7WidgetPreference)findPreference(KEY_UNRESTRICTED_ACCESS);
        //Modify end by Dingyi 2016/10/25 for task 2827906
        mApplicationsState = ApplicationsState.getInstance(
                (Application) getContext().getApplicationContext());
        mDataSaverBackend = new DataSaverBackend(getContext());
        mDataUsageBridge = new AppStateDataUsageBridge(mApplicationsState, this, mDataSaverBackend);
        mSession = mApplicationsState.newSession(this);
    }
    //Add begin by Dingyi 2016/10/25 for task 2827906
    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        if (preference == mDataSaverEnablePreference){
            synchronized(this){
                if (mSwitching) {
                    return true;
                }
                mSwitching = true;
                boolean dataSaverEnable = !mDataSaverEnablePreference.isChecked();
                mDataSaverBackend.setDataSaverEnabled(dataSaverEnable);
            }
        }
        return true;
    }
    //Add end by Dingyi 2016/10/25 for task 2827906
    //Delete begin by Dingyi 2016/10/25 for task 2827906
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        mSwitchBar = ((SettingsActivity) getActivity()).getSwitchBar();
//        mSwitchBar.show();
//        mSwitchBar.addOnSwitchChangeListener(this);
//    }
    //Delete end by Dingyi 2016/10/25 for task 2827906
    @Override
    public void onResume() {
        super.onResume();
        mDataSaverBackend.refreshWhitelist();
        mDataSaverBackend.refreshBlacklist();
        mDataSaverBackend.addListener(this);
        mSession.resume();
        mDataUsageBridge.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mDataSaverBackend.remListener(this);
        mDataUsageBridge.pause();
        mSession.pause();
    }

    //Delete begin by Dingyi 2016/10/25 for task 2827906
//    @Override
//    public void onSwitchChanged(Switch switchView, boolean isChecked) {
//        synchronized(this) {
//            if (mSwitching) {
//               return;
//            }
//            mSwitching = true;
//            mDataSaverBackend.setDataSaverEnabled(isChecked);
//        }
//    }
    //Delete end by Dingyi 2016/10/25 for task 2827906

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.DATA_SAVER_SUMMARY;
    }

    @Override
    protected int getHelpResource() {
        return R.string.help_url_data_saver;
    }

    @Override
    public void onDataSaverChanged(boolean isDataSaving) {
        synchronized(this) {
            //Modify begin by Dingyi 2016/10/25 for task 2827906
            mDataSaverEnablePreference.setChecked(isDataSaving);
//            mSwitchBar.setChecked(isDataSaving);
            //Modify end by Dingyi 2016/10/25 for task 2827906
            mSwitching = false;
        }
    }

    @Override
    public void onWhitelistStatusChanged(int uid, boolean isWhitelisted) {
    }

    @Override
    public void onBlacklistStatusChanged(int uid, boolean isBlacklisted) {
    }

    @Override
    public void onExtraInfoUpdated() {
        if (!isAdded()) {
            return;
        }
        int count = 0;
        final ArrayList<AppEntry> allApps = mSession.getAllApps();
        final int N = allApps.size();
        for (int i = 0; i < N; i++) {
            final AppEntry entry = allApps.get(i);
            if (!ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER.filterApp(entry)) {
                continue;
            }
            if (entry.extraInfo != null && ((AppStateDataUsageBridge.DataUsageState)
                    entry.extraInfo).isDataSaverWhitelisted) {
                count++;
            }
        }
        //Delete by Dingyi 2016/10/25 for task 2827906
//        mUnrestrictedAccess.setSummary(getResources().getQuantityString(
//                R.plurals.data_saver_unrestricted_summary, count, count));
        //Delete by Dingyi 2016/10/25 for task 2827906
    }

    @Override
    public void onRunningStateChanged(boolean running) {

    }

    @Override
    public void onPackageListChanged() {

    }

    @Override
    public void onRebuildComplete(ArrayList<AppEntry> apps) {

    }

    @Override
    public void onPackageIconChanged() {

    }

    @Override
    public void onPackageSizeChanged(String packageName) {

    }

    @Override
    public void onAllSizesComputed() {

    }

    @Override
    public void onLauncherInfoChanged() {

    }

    @Override
    public void onLoadEntriesCompleted() {

    }
}

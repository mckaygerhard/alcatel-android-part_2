/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.notification;


import android.os.Bundle;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.RingtonePreference;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Utils;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedPreference;
import mst.preference.SwitchPreference;
import mst.preference.Preference;
import mst.preference.PreferenceCategory;
import mst.preference.PreferenceScreen;
import android.provider.Settings;
import android.content.Intent;
import mst.preference.Preference.OnPreferenceClickListener;

public class NotificationStatusSettings extends SettingsPreferenceFragment implements Indexable, Preference.OnPreferenceChangeListener,OnPreferenceClickListener{
    private static final String TAG = "NotificationStatusSettings";
    private static final String KEY_NOTIFICATION_SCREEN_ON = "notificaion_screenon";
    private static final String KEY_SHOW_BATTERY_PERCENT = "battery_percent";
    private static final String KEY_NOTIFICATION_STATUS = "app_notification_manager";
    private static final String KEY_QUICK_SWITCH = "quick_switch_manager";
    private SwitchPreference mNotificationScreenOnPreference;
    private SwitchPreference mShowBatteryPercentPreference;
    private Preference mNotificationStatus;
    private Preference mQuickSwitchManagerPreference;

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.SOUND;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.notification_status_settings);
        mNotificationScreenOnPreference = (SwitchPreference) findPreference(KEY_NOTIFICATION_SCREEN_ON);
        mShowBatteryPercentPreference = (SwitchPreference) findPreference(KEY_SHOW_BATTERY_PERCENT);
        mNotificationStatus = (Preference) findPreference(KEY_NOTIFICATION_STATUS);
        mQuickSwitchManagerPreference = (Preference) findPreference(KEY_QUICK_SWITCH);
        if (mNotificationStatus != null){
            mNotificationStatus.setOnPreferenceClickListener(this);
        } else {
            removePreference(KEY_NOTIFICATION_STATUS);
        }

        if (mQuickSwitchManagerPreference != null){
            mQuickSwitchManagerPreference.setOnPreferenceClickListener(this);
        } else {
            removePreference(KEY_QUICK_SWITCH);
        }

        if (mNotificationScreenOnPreference != null) {
            mNotificationScreenOnPreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_NOTIFICATION_SCREEN_ON);
        }
        if (mShowBatteryPercentPreference != null) {
            mShowBatteryPercentPreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_SHOW_BATTERY_PERCENT);
        }
        updateState();
    }

    public void updateState() {
        if (mNotificationScreenOnPreference != null) {
            mNotificationScreenOnPreference.setChecked(
                    Settings.System.getInt(getContentResolver(), Settings.System.COMING_NOTIFICATION_SCREEN_ON, 0) == 1);
        }
        if (mShowBatteryPercentPreference != null) {
            mShowBatteryPercentPreference.setChecked(
                    Settings.System.getInt(getContentResolver(), Settings.System.SHOW_BATTERY_PERCENT, 0) == 1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {

        if (preference == mNotificationScreenOnPreference) {
            Settings.System.putInt(getContentResolver(),
                    Settings.System.COMING_NOTIFICATION_SCREEN_ON, (Boolean) newValue ? 1 : 0);

        }
        if (preference == mShowBatteryPercentPreference) {
            Settings.System.putInt(getContentResolver(),
                    Settings.System.SHOW_BATTERY_PERCENT, (Boolean) newValue ? 1 : 0);
        }
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        Intent intent = null;
        if (preference == mNotificationStatus) {
            intent = new Intent("android.intent.action.SHOW_NOTIFY_MANAGE_ACTIVITY");
            getActivity().startActivity(intent);
        }

        if (preference == mQuickSwitchManagerPreference){
            intent = new Intent("com.android.settings.action.CUSTOMIZE_EDIT");
            getActivity().startActivity(intent);
        }
        return true;
    }

}

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;


import mst.app.dialog.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
//import android.preference.Preference;
//import android.preference.SwitchPreference;
import mst.preference.Preference;
import mst.preference.SwitchPreference;
import android.provider.Settings;
import android.os.SystemProperties;

import android.text.TextUtils;
import android.util.Log;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.search.Indexable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream; // MODIFIED by Wang Xiongke, 2016-10-28,BUG-2827929
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class QuickOperateSettings extends SettingsPreferenceFragment implements Indexable, Preference.OnPreferenceChangeListener {
    private static final String TAG = "QuickOperateSettings";

    private static final String KEY_GLOVE_MODE_PREFERENCE = "glove_mode";
    private static final String KEY_DOZE = "doze";
    /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-28,BUG-2827929*/
    private static final String KEY_DOUBLE_TAP_WAKE_PREFERENCE = "double_tap_wake";
    /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-31,BUG-3253025*/
    private static final String KEY_POCKET_MODE_PREFERENCE = "pocket_mode";
    private static final String KEY_THREE_FINGER_GESTURES = "three_finger_screenshort";
    private final String THREE_FINGER_GESTURES_ENABLE = "persist.sys.ssshot.threePointer";
    private SwitchPreference mThreeFingerScreenshortEnable;
    private SwitchPreference mDozePreference;
    private SwitchPreference mGloveModePreference;
    private SwitchPreference mDoubleTapWakePreference;
    private SwitchPreference mPocketModePreference;
    /* MODIFIED-END by Wang Xiongke,BUG-3253025*/
    /* MODIFIED-END by Wang Xiongke,BUG-2827929*/

    private Boolean glovemodemenu = false;

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.SOUND;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.quick_operate_settings);
        if (isDozeAvailable(getActivity())) {
            mDozePreference = (SwitchPreference) findPreference(KEY_DOZE);
            mDozePreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_DOZE);
        }

        mGloveModePreference = (SwitchPreference) findPreference(KEY_GLOVE_MODE_PREFERENCE);
        if (mGloveModePreference != null) {
            mGloveModePreference.setPersistent(false);
            mGloveModePreference.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_GLOVE_MODE_PREFERENCE);
        }
        glovemodemenu = this.getResources().getBoolean(com.android.internal.R.bool.feature_tctsetting_glovemode_on);
        glovemodemenu = true;
        if (!glovemodemenu) {
            getPreferenceScreen().removePreference(findPreference(KEY_GLOVE_MODE_PREFERENCE));
        }

        /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-28,BUG-2827929*/
        mDoubleTapWakePreference = (SwitchPreference) findPreference(KEY_DOUBLE_TAP_WAKE_PREFERENCE);
        mDoubleTapWakePreference.setOnPreferenceChangeListener(this);
        /* MODIFIED-END by Wang Xiongke,BUG-2827929*/

        /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-31,BUG-3253025*/
        mPocketModePreference = (SwitchPreference) findPreference(KEY_POCKET_MODE_PREFERENCE);
        mPocketModePreference.setOnPreferenceChangeListener(this);
        /* MODIFIED-END by Wang Xiongke,BUG-3253025*/
        updateState();
        mThreeFingerScreenshortEnable = (SwitchPreference)findPreference(KEY_THREE_FINGER_GESTURES);
        android.util.Log.d("TAG","THREE_FINGER_GESTURES_ENABLE= "+SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, true));
        android.util.Log.d("TAG","THREE_FINGER_GESTURES_ENABLE= "+SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, false));
        mThreeFingerScreenshortEnable.setChecked(SystemProperties.getBoolean(THREE_FINGER_GESTURES_ENABLE, true));
        mThreeFingerScreenshortEnable.setOnPreferenceChangeListener(this);
    }

    private void updateState() {
        if (glovemodemenu) {
            if (mGloveModePreference != null) {
                mGloveModePreference.setChecked(
                        Settings.System.getInt(getContentResolver(), Settings.System.TCT_GLOVE_MODE_ENABLE, 0) == 1);
            }
        }
        // Update doze if it is available.
        if (mDozePreference != null) {
            int value = Settings.Secure.getInt(getContentResolver(), Settings.Secure.DOZE_ENABLED, 1);
            mDozePreference.setChecked(value != 0);
        }

        /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-28,BUG-2827929*/
        if (mDoubleTapWakePreference != null) {
            int value = Settings.System.getInt(getContentResolver(),Settings.System.TCT_DOUBLE_CLICK, 1);
            mDoubleTapWakePreference.setChecked(value != 0);
        }
        /* MODIFIED-END by Wang Xiongke,BUG-2827929*/

        /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-31,BUG-3253025*/
        if (mPocketModePreference != null) {
            int value = Settings.System.getInt(getContentResolver(), Settings.System.TCT_POCKET_MODE, 0);
            mPocketModePreference.setChecked(value != 0);
        }
        /* MODIFIED-END by Wang Xiongke,BUG-3253025*/
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {


        if (preference == mGloveModePreference) {
            boolean isGloveModeON = (boolean) newValue;//mGloveModePreference.isChecked();
            if (isGloveModeON) {
                final AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.dialog_GloveMode_content);
                // builder.setCancelable(false);
                builder.setPositiveButton
                        (R.string.dialog_enable, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mGloveModePreference.setChecked(true);
                                SetGloveMode();
                            }
                        });
                builder.setNegativeButton(R.string.dialog_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mGloveModePreference.setChecked(false);
                                SetGloveMode();
                                dialog.cancel();
                            }
                        });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        mGloveModePreference.setChecked(false);
                        SetGloveMode();
                        dialog.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                mGloveModePreference.setChecked(false);
                SetGloveMode();
            }
            return false;
        }
        /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-28,BUG-2827929*/
        if (preference == mDoubleTapWakePreference) {
            String doubleTapNode = this.getResources()
                                        .getString(com.android.internal.R.string.tp_device_doubleclick_enable);
            boolean doubleTapOn = (boolean) newValue;
            Settings.System.putInt(getContentResolver(),Settings.System.TCT_DOUBLE_CLICK, doubleTapOn ? 1 : 0);
            byte[] bytes = new byte[1];
            bytes[0] = (byte) (doubleTapOn ? '1' : '0');
            try (FileOutputStream fos = new FileOutputStream(doubleTapNode)) {
                fos.write(bytes);
                Log.i(TAG, "double tap wakeup:" + bytes[0]);
            } catch (Exception e) {
                Log.w(TAG, "error to write to node", e);
            }
            return true;
        }
        /* MODIFIED-END by Wang Xiongke,BUG-2827929*/
        /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-31,BUG-3253025*/
        if (preference == mPocketModePreference) {
            boolean value = (Boolean) newValue;
            Settings.System.putInt(getContentResolver(), Settings.System.TCT_POCKET_MODE, value ? 1 : 0);
            return true;
        }
        /* MODIFIED-END by Wang Xiongke,BUG-3253025*/
        if (preference == mDozePreference) {
            boolean value = (Boolean) newValue;
            Settings.Secure.putInt(getContentResolver(), Settings.Secure.DOZE_ENABLED, value ? 1 : 0);
            return true;
        }
        if (preference == mThreeFingerScreenshortEnable) {
            boolean value = (Boolean) newValue;
            SystemProperties.set(THREE_FINGER_GESTURES_ENABLE, String.valueOf(value));
            return true;
        }
        return false;
    }

    private void SetGloveMode() {
        String file_path = this.getResources().getString(com.android.internal.R.string.tp_device_glove_enable);
        boolean value = mGloveModePreference.isChecked();
        Settings.System.putInt(getContentResolver(), Settings.System.TCT_GLOVE_MODE_ENABLE, value ? 1 : 0);
        SetGloveModeValue(value, file_path);
    }

    private void SetGloveModeValue(boolean status, String path) {
        char temp = status ? '1' : '0';
        FileWriter fileOutStream = null;
        try {
            File file = new File(path);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(temp);
            writer.flush();
            writer.close();
            Log.i(TAG, "GloveModeValue temp=:" + temp);
        } catch (FileNotFoundException ex) {
            Log.w(TAG, "file  not found: " + ex);
        } catch (IOException ex) {
            Log.w(TAG, "IOException trying to  : " + ex);
        } catch (RuntimeException ex) {
            Log.w(TAG, "exception while syncing file: ", ex);
        } finally {
            if (fileOutStream != null) {
                try {
                    fileOutStream.close();
                } catch (IOException ex) {
                    Log.w(TAG, "IOException while closing synced file: ", ex);
                } catch (RuntimeException ex) {
                    Log.w(TAG, "exception while closing file: ", ex);
                }
            }
        }
    }

    private static boolean isDozeAvailable(Context context) {
        String name = Build.IS_DEBUGGABLE ? SystemProperties.get("debug.doze.component") : null;
        if (TextUtils.isEmpty(name)) {
            name = context.getResources().getString(
                    com.android.internal.R.string.config_dozeComponent);
        }
        return !TextUtils.isEmpty(name);
    }
}

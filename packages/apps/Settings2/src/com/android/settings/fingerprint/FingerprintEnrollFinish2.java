/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.settings.fingerprint;

import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.view.View;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.ChooseLockSettingsHelper; // MODIFIED by jianguang.sun, 2016-10-06,BUG-2987595
import android.provider.Settings;

/**
 * Activity which concludes fingerprint enrollment.
 */
public class FingerprintEnrollFinish2 extends FingerprintEnrollBase2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMstContentView(R.layout.fingerprint_enroll_finish_base2);
        setHeaderText(R.string.security_settings_fingerprint_enroll_start_title);
        //Button addButton = (Button) findViewById(R.id.add_another_button);
        showBackIcon(true);
        FingerprintManager fpm = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
        /* MODIFIED-BEGIN by jianguang.sun, 2016-10-06,BUG-2987595*/
        int enrolled = fpm.tctGetEnrolledFingerprints(mUserId, FingerprintManager.FP_TAG_COMMON).size();
        int max = getResources().getInteger(
                com.android.internal.R.integer.config_fingerprintMaxTemplatesPerUser);
        Intent intent = getEnrollingIntent();
        mEnrollTag = intent.getIntExtra(
                ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG, 0);
        if (enrolled >= max || mEnrollTag != FingerprintManager.FP_TAG_COMMON) {
        /* MODIFIED-END by jianguang.sun,BUG-2987595*/
            /* Don't show "Add" button if too many fingerprints already added */
            //addButton.setVisibility(View.INVISIBLE);
        } else {
            //addButton.setOnClickListener(this);
        }

        /* MODIFIED-BEGIN by jianguang.sun, 2016-10-11,BUG-2987595*/
        if (enrolled == 1) {
            Settings.System.putInt(getContentResolver(),
                    Settings.System.TCT_UNLOCK_SCREEN, 1);
        }
        /* MODIFIED-END by jianguang.sun,BUG-2987595*/

    }

    @Override
    protected void onNextButtonClick() {
        setResult(RESULT_FINISHED);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_another_button) {
            final Intent intent = getEnrollingIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            startActivity(intent);
            finish();
        }
        super.onClick(v);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.FINGERPRINT_ENROLL_FINISH;
    }

    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
}

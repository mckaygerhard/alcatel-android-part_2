package com.android.settings.vpn2;

import android.os.Bundle;
import android.os.UserManager;
import mst.preference.Preference;
import mst.preference.PreferenceGroup;
import mst.preference.PreferenceScreen;
import android.util.Log;

import com.android.settingslib.RestrictedPreference;

import com.android.settings.R;
import com.android.settings.RestrictedSettingsFragment;
import com.android.internal.logging.MetricsProto.MetricsEvent;

import java.util.Map;
import android.util.ArrayMap;

/**
 * Created by liang.zhang on 9/13/16.
 */
public class VpnCreateFragment extends RestrictedSettingsFragment implements Preference.OnPreferenceClickListener {
    public VpnCreateFragment() {
        super(UserManager.DISALLOW_CONFIG_VPN);
    }

    private Map<Preference, Integer> mPrefMap = new ArrayMap<>();
    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.VPN;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addPreferencesFromResource(R.xml.vpn_create);
        PreferenceGroup prefGroup = getPreferenceScreen();
        prefGroup.removeAll();
        String[] vpnTypes = getPrefContext().getResources().getStringArray(R.array.vpn_types);
        for(int i = 1;i<= vpnTypes.length;i++) {
            Preference pref = new RestrictedPreference(getPrefContext());
            //pref.setFragment("com.android.settings.vpn2.VpnEditFragment");
            pref.setKey("vpntype_" + i);
            pref.setTitle(vpnTypes[i - 1]);
            mPrefMap.put(pref, i);
            prefGroup.addPreference(pref);
            pref.setOnPreferenceClickListener(this);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        int i = mPrefMap.get(preference);
        Bundle args = new Bundle();
        args.putInt("vpn_type", i);
        startFragment(VpnCreateFragment.this, VpnEditFragment.class.getCanonicalName(), R.string.vpn_title, 0, args);
        return true;
    }
}

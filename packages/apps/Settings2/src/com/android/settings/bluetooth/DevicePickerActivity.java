/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.bluetooth;
//[BUGFIX]-Add-BEGIN by TCTNB.dongdong.gong,10/28/2016,3082231,bt ergo
import mst.app.MstActivity;
import mst.widget.toolbar.Toolbar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
//[BUGFIX]-Add-END by TCTNB.dongdong.gong
import com.android.settings.R;

/**
 * Activity for Bluetooth device picker dialog. The device picker logic
 * is implemented in the {@link BluetoothSettings} fragment.
 */
//[BUGFIX]-Add-BEGIN by TCTNB.dongdong.gong,10/28/2016,3082231,bt ergo
public final class DevicePickerActivity extends MstActivity {
//[BUGFIX]-Add-END by TCTNB.dongdong.gong
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //[BUGFIX]-Add-BEGIN by TCTNB.dongdong.gong,10/28/2016,3082231,bt ergo
        setMstContentView(R.layout.bluetooth_device_picker);
        setTitle(getString(R.string.device_picker));
        showBackIcon(true);
        inflateToolbarMenu(R.menu.bt_refresh);
      //[BUGFIX]-Add-END by TCTNB.dongdong.gong
    }
  //[BUGFIX]-Add-BEGIN by TCTNB.dongdong.gong,10/28/2016,3082231,bt ergo
    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
    
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        // TODO Auto-generated method stub
        BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if(mBluetoothManager == null) return true;
        BluetoothAdapter adapter =  mBluetoothManager.getAdapter();
        if(adapter == null) return true;
        adapter.startDiscovery();
        return true;
    }
    //[BUGFIX]-Add-END by TCTNB.dongdong.gong
}

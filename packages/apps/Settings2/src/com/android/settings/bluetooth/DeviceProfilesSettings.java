/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.bluetooth;

import mst.app.dialog.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import mst.preference.CheckBoxPreference;
import mst.preference.EditTextPreference;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
//[FEATURE]-Add-END by TCTNB.dongdong.gong

import com.android.settings.R;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import com.android.settingslib.bluetooth.MapProfile;
import com.android.settingslib.bluetooth.PanProfile;
import com.android.settingslib.bluetooth.PbapServerProfile;

import java.util.HashMap;
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
public final class DeviceProfilesSettings extends DialogFragment implements
        CachedBluetoothDevice.Callback, DialogInterface.OnClickListener, OnClickListener, OnCheckedChangeListener {
//[FEATURE]-Add-END by TCTNB.dongdong.gong
    private static final String TAG = "DeviceProfilesSettings";

    public static final String ARG_DEVICE_ADDRESS = "device_address";

    private static final String KEY_PROFILE_CONTAINER = "profile_container";
    private static final String KEY_UNPAIR = "unpair";
    private static final String KEY_PBAP_SERVER = "PBAP Server";

    private CachedBluetoothDevice mCachedDevice;
    private LocalBluetoothManager mManager;
    private LocalBluetoothProfileManager mProfileManager;

    private ViewGroup mProfileContainer;
    private TextView mProfileLabel;
    private EditTextPreference mDeviceNamePref;
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
    private View relativeView;
    private ImageView imageView;
    private Button mforgetDevice;
//[FEATURE]-Add-END by TCTNB.dongdong.gong

    private final HashMap<LocalBluetoothProfile, CheckBoxPreference> mAutoConnectPrefs
            = new HashMap<LocalBluetoothProfile, CheckBoxPreference>();

    private AlertDialog mDisconnectDialog;
    private boolean mProfileGroupIsRemoved;

    private View mRootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mManager = Utils.getLocalBtManager(getActivity());
        CachedBluetoothDeviceManager deviceManager = mManager.getCachedDeviceManager();

        String address = getArguments().getString(ARG_DEVICE_ADDRESS);
        BluetoothDevice remoteDevice = mManager.getBluetoothAdapter().getRemoteDevice(address);

        mCachedDevice = deviceManager.findDevice(remoteDevice);
        if (mCachedDevice == null) {
            mCachedDevice = deviceManager.addDevice(mManager.getBluetoothAdapter(),
                    mManager.getProfileManager(), remoteDevice);
        }
        mProfileManager = mManager.getProfileManager();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
        Dialog dialog = null;
        mRootView = LayoutInflater.from(getContext()).inflate(R.layout.device_profiles_settings,
                null);
        mProfileContainer = (ViewGroup) mRootView.findViewById(R.id.profiles_section);
//        mProfileLabel = (TextView) mRootView.findViewById(R.id.profiles_label);
        imageView = (ImageView)mRootView.findViewById(R.id.renameDevice);
        mforgetDevice = (Button)mRootView.findViewById(R.id.btn_forget);
//        final EditText deviceName = (EditText) mRootView.findViewById(R.id.name);
        final TextView deviceName = (TextView) mRootView.findViewById(R.id.name);
//        deviceName.setText(mCachedDevice.getName(), TextView.BufferType.EDITABLE);
        deviceName.setText(mCachedDevice.getName());
        Log.d("gdd", "setStyle android.R.style.Theme_Material_Light");
        dialog = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Light)
                .setView(mRootView)
//                .setNegativeButton(R.string.forget, this)
//                .setPositiveButton(R.string.okay, this)
                .setTitle(R.string.bluetooth_preference_paired_devices)
                .create();
        mforgetDevice.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {
                mCachedDevice.unpair();
                com.android.settings.bluetooth.Utils.updateSearchIndex(getContext(),
                        BluetoothSettings.class.getName(), mCachedDevice.getName(),
                        getString(R.string.bluetooth_settings),
                        R.drawable.ic_settings_bluetooth, false);
                Log.d("GDD", "FORGET BUTTON IS CLICKED");
                DeviceProfilesSettings.this.dismiss();
            }
            
        });
        
        imageView.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                View mChangeNameView = LayoutInflater.from(DeviceProfilesSettings.this.getContext()).inflate(R.layout.dialog_edittext,
                        null);
                final EditText mDeviceNameView = (EditText) mChangeNameView.findViewById(R.id.edittext);
                mDeviceNameView.setText(mCachedDevice.getName(), TextView.BufferType.EDITABLE);
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCachedDevice.setName(mDeviceNameView.getText().toString());
                    }
                };
                Dialog dialog = new AlertDialog.Builder(DeviceProfilesSettings.this.getContext(), android.R.style.Theme_Material_Light_Dialog_Alert)
                .setView(mChangeNameView)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(R.string.bluetooth_rename_button, listener)
                .setTitle(R.string.bluetooth_preference_paired_dialog_name_label)
                .create();
                dialog.show();
            }
        });
        return dialog;
//[FEATURE]-Add-END by TCTNB.dongdong.gong
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
                TextView deviceName = (TextView) mRootView.findViewById(R.id.name);
                mCachedDevice.setName(deviceName.getText().toString());
//[FEATURE]-Add-END by TCTNB.dongdong.gong
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                mCachedDevice.unpair();
                com.android.settings.bluetooth.Utils.updateSearchIndex(getContext(),
                        BluetoothSettings.class.getName(), mCachedDevice.getName(),
                        getString(R.string.bluetooth_settings),
                        R.drawable.ic_settings_bluetooth, false);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDisconnectDialog != null) {
            mDisconnectDialog.dismiss();
            mDisconnectDialog = null;
        }
        if (mCachedDevice != null) {
            mCachedDevice.unregisterCallback(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        mManager.setForegroundActivity(getActivity());
        if (mCachedDevice != null) {
            mCachedDevice.registerCallback(this);
            if (mCachedDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                dismiss();
                return;
            }
            addPreferencesForProfiles();
            refresh();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mCachedDevice != null) {
            mCachedDevice.unregisterCallback(this);
        }

        mManager.setForegroundActivity(null);
    }

    private void addPreferencesForProfiles() {
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
        mProfileContainer.removeAllViews();
        MarginLayoutParams params = new MarginLayoutParams(MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.WRAP_CONTENT);
        params.setMargins(0, 35, 0, 0);
        for (LocalBluetoothProfile profile : mCachedDevice.getConnectableProfiles()) {
            /* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/
            // MAP and PBAP profiles would be added based on permission access
            if (!((profile instanceof PbapServerProfile) ||
                (profile instanceof MapProfile))) {
            /* MODIFIED-END by qianbo.pan, 2016-10-21,BUG-3175107*/
//            CheckBox pref = createProfilePreference(profile);
            Switch switchPref = createProfilePreferenceSwitch(profile);
//                    new Switch(getActivity());
            switchPref.setText(profile.getNameResource(mCachedDevice.getDevice()));

            mProfileContainer.addView(switchPref, params);
//            mProfileContainer.addView(pref);
            }
        }

        final int pbapPermission = mCachedDevice.getPhonebookPermissionChoice();
        // Only provide PBAP cabability if the client device has requested PBAP.
        if (pbapPermission == CachedBluetoothDevice.PBAP_CONNECT_RECEIVED) { // MODIFIED by qianbo.pan, 2016-10-21,BUG-3175107
            final PbapServerProfile psp = mManager.getProfileManager().getPbapProfile();
 //           CheckBox pbapPref = createProfilePreference(psp);
            Switch pbapPref = createProfilePreferenceSwitch(psp);
            mProfileContainer.addView(pbapPref, params);
        }

        final MapProfile mapProfile = mManager.getProfileManager().getMapProfile();
        final int mapPermission = mCachedDevice.getMessagePermissionChoice();
        if (mapPermission != CachedBluetoothDevice.ACCESS_UNKNOWN) {
//            CheckBox mapPreference = createProfilePreference(mapProfile);
            Switch mapPreference = createProfilePreferenceSwitch(mapProfile);
            mProfileContainer.addView(mapPreference, params);
        }
//[FEATURE]-Add-END by TCTNB.dongdong.gong
        showOrHideProfileGroup();
    }

    private void showOrHideProfileGroup() {
        int numProfiles = mProfileContainer.getChildCount();
        if (!mProfileGroupIsRemoved && numProfiles == 0) {
            mProfileContainer.setVisibility(View.GONE);
//            mProfileLabel.setVisibility(View.GONE);
            mProfileGroupIsRemoved = true;
        } else if (mProfileGroupIsRemoved && numProfiles != 0) {
            mProfileContainer.setVisibility(View.VISIBLE);
//            mProfileLabel.setVisibility(View.VISIBLE);
            mProfileGroupIsRemoved = false;
        }
    }
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
    /**
     * Creates a checkbox preference for the particular profile. The key will be
     * the profile's name.
     *
     * @param profile The profile for which the preference controls.
     * @return A preference that allows the user to choose whether this profile
     *         will be connected to.
     */
/*    private CheckBox createProfilePreference(LocalBluetoothProfile profile) {
        CheckBox pref = new CheckBox(getActivity());
        pref.setTag(profile.toString());
        pref.setText(profile.getNameResource(mCachedDevice.getDevice()));
        pref.setOnClickListener(this);

        refreshProfilePreference(pref, profile);

        return pref;
    }*/

    /**
     * Creates a checkbox preference for the particular profile. The key will be
     * the profile's name.
     *
     * @param profile The profile for which the preference controls.
     * @return A preference that allows the user to choose whether this profile
     *         will be connected to.
     */
    private Switch createProfilePreferenceSwitch(LocalBluetoothProfile profile) {
        Switch pref = new Switch(getActivity());
        pref.setTag(profile.toString());
        pref.setText(profile.getNameResource(mCachedDevice.getDevice()));
        pref.setOnClickListener(this);
        pref.setOnCheckedChangeListener(this);
        refreshProfilePreference(pref, profile);

        return pref;
    }
//[FEATURE]-Add-END by TCTNB.dongdong.gong

    @Override
    public void onClick(View v) {
        if (v instanceof CheckBox) {
            LocalBluetoothProfile prof = getProfileOf(v);
            if (prof != null)
                onProfileClicked(prof, (CheckBox) v);
            else
                Log.e(TAG, "Error: Can't get the profile for the preference");
        }
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
        if (v instanceof Switch) {
            LocalBluetoothProfile prof = getProfileOf(v);
            if (prof != null)
                onProfileClicked(prof, (Switch) v);
            else
                Log.e(TAG, "Error: Can't get the profile for the preference");
            Log.e("gdd", "The Switch is clicked");
        }
//[FEATURE]-Add-END by TCTNB.dongdong.gong
    }
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
    private void onProfileClicked(LocalBluetoothProfile profile, Switch profilePref) {
        BluetoothDevice device = mCachedDevice.getDevice();
        /* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/
        /*Log.e("gdd", "The Switch is performing onProfileClicked");
        if (KEY_PBAP_SERVER.equals(profilePref.getTag())) {
            final int newPermission = mCachedDevice.getPhonebookPermissionChoice()
                == CachedBluetoothDevice.ACCESS_ALLOWED ? CachedBluetoothDevice.ACCESS_REJECTED
                : CachedBluetoothDevice.ACCESS_ALLOWED;
            mCachedDevice.setPhonebookPermissionChoice(newPermission);
            profilePref.setChecked(newPermission == CachedBluetoothDevice.ACCESS_ALLOWED);
            return;
        }*/
        /* MODIFIED-END by qianbo.pan, 2016-10-21,BUG-3175107*/
        if (!profilePref.isChecked()) {
            // Recheck it, until the dialog is done.
            profilePref.setChecked(true);
            askDisconnect(mManager.getForegroundActivity(), profile);
        } else {
            if (profile instanceof MapProfile) {
                mCachedDevice.setMessagePermissionChoice(BluetoothDevice.ACCESS_ALLOWED);
            }
            /* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/
            else if (profile instanceof PbapServerProfile) {
                // Special handling for pbap server, as server cant connect to client
                mCachedDevice.setPhonebookPermissionChoice(
                    CachedBluetoothDevice.PBAP_CONNECT_RECEIVED);
                refreshProfilePreference(profilePref, profile);
                return;
            }
            /* MODIFIED-END by qianbo.pan, 2016-10-21,BUG-3175107*/
            if (profile.isPreferred(device)) {
                // profile is preferred but not connected: disable auto-connect
                if (profile instanceof PanProfile) {
                    mCachedDevice.connectProfile(profile);
                } else {
                    profile.setPreferred(device, false);
                }
            } else {
                profile.setPreferred(device, true);
                mCachedDevice.connectProfile(profile);
            }
            refreshProfilePreference(profilePref, profile);
        }
    }
//[FEATURE]-Add-END by TCTNB.dongdong.gong
    private void onProfileClicked(LocalBluetoothProfile profile, CheckBox profilePref) {
        BluetoothDevice device = mCachedDevice.getDevice();

        if (KEY_PBAP_SERVER.equals(profilePref.getTag())) {
            final int newPermission = mCachedDevice.getPhonebookPermissionChoice()
                == CachedBluetoothDevice.ACCESS_ALLOWED ? CachedBluetoothDevice.ACCESS_REJECTED
                : CachedBluetoothDevice.ACCESS_ALLOWED;
            mCachedDevice.setPhonebookPermissionChoice(newPermission);
            profilePref.setChecked(newPermission == CachedBluetoothDevice.ACCESS_ALLOWED);
            return;
        }

        if (!profilePref.isChecked()) {
            // Recheck it, until the dialog is done.
            profilePref.setChecked(true);
            askDisconnect(mManager.getForegroundActivity(), profile);
        } else {
            if (profile instanceof MapProfile) {
                mCachedDevice.setMessagePermissionChoice(BluetoothDevice.ACCESS_ALLOWED);
            }
            if (profile.isPreferred(device)) {
                // profile is preferred but not connected: disable auto-connect
                if (profile instanceof PanProfile) {
                    mCachedDevice.connectProfile(profile);
                } else {
                    profile.setPreferred(device, false);
                }
            } else {
                profile.setPreferred(device, true);
                mCachedDevice.connectProfile(profile);
            }
            refreshProfilePreference(profilePref, profile);
        }
    }

    private void askDisconnect(Context context,
            final LocalBluetoothProfile profile) {
        // local reference for callback
        final CachedBluetoothDevice device = mCachedDevice;
        String name = device.getName();
        if (TextUtils.isEmpty(name)) {
            name = context.getString(R.string.bluetooth_device);
        }

        String profileName = context.getString(profile.getNameResource(device.getDevice()));

        String title = context.getString(R.string.bluetooth_disable_profile_title);
        String message = context.getString(R.string.bluetooth_disable_profile_message,
                profileName, name);

        DialogInterface.OnClickListener disconnectListener =
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                device.disconnect(profile);
                profile.setPreferred(device.getDevice(), false);
                if (profile instanceof MapProfile) {
                    device.setMessagePermissionChoice(BluetoothDevice.ACCESS_REJECTED);
                }
                /* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/
                if (profile instanceof PbapServerProfile) {
                    device.setPhonebookPermissionChoice(BluetoothDevice.ACCESS_REJECTED);
                }
                /* MODIFIED-END by qianbo.pan, 2016-10-21,BUG-3175107*/
                refreshProfilePreference(findProfile(profile.toString()), profile);
            }
        };

        mDisconnectDialog = Utils.showDisconnectDialog(context,
                mDisconnectDialog, disconnectListener, title, Html.fromHtml(message));
    }

    @Override
    public void onDeviceAttributesChanged() {
        refresh();
    }

    private void refresh() {
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
//        final EditText deviceNameField = (EditText) mRootView.findViewById(R.id.name);
        final TextView deviceNameField = (TextView) mRootView.findViewById(R.id.name);
        if (deviceNameField != null) {
            deviceNameField.setText(mCachedDevice.getName());
        }
        refreshProfiles();
    }

    private void refreshProfiles() {
        for (LocalBluetoothProfile profile : mCachedDevice.getConnectableProfiles()) {
//            CheckBox profilePref = findProfile(profile.toString());
            Switch profilePref = findProfile(profile.toString());
            if (profilePref == null) {
                profilePref = createProfilePreferenceSwitch(profile);
                mProfileContainer.addView(profilePref);
            } else {
                refreshProfilePreference(profilePref, profile);
            }
        }
        for (LocalBluetoothProfile profile : mCachedDevice.getRemovedProfiles()) {
            Switch profilePref = findProfile(profile.toString());
            if (profilePref != null) {
                /* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/
                if (profile instanceof PbapServerProfile) {
                    final int pbapPermission = mCachedDevice.getPhonebookPermissionChoice();
                    Log.d(TAG, "refreshProfiles: pbapPermission = " + pbapPermission);
                    if (pbapPermission != CachedBluetoothDevice.ACCESS_UNKNOWN)
                        continue;
                }
                /* MODIFIED-END by qianbo.pan, 2016-10-21,BUG-3175107*/
                Log.d(TAG, "Removing " + profile.toString() + " from profile list");
                mProfileContainer.removeView(profilePref);
            }
        }

        showOrHideProfileGroup();
    }

/*    private CheckBox findProfile(String profile) {
        return (CheckBox) mProfileContainer.findViewWithTag(profile);
    }*/

    private Switch findProfile(String profile) {
        return (Switch) mProfileContainer.findViewWithTag(profile);
    }
//[FEATURE]-Add-END by TCTNB.dongdong.gong
    private void refreshProfilePreference(CheckBox profilePref,
            LocalBluetoothProfile profile) {
        BluetoothDevice device = mCachedDevice.getDevice();

        // Gray out checkbox while connecting and disconnecting.
        profilePref.setEnabled(!mCachedDevice.isBusy());

        if (profile instanceof MapProfile) {
            profilePref.setChecked(mCachedDevice.getMessagePermissionChoice()
                    == CachedBluetoothDevice.ACCESS_ALLOWED);

        } else if (profile instanceof PbapServerProfile) {
            profilePref.setChecked(mCachedDevice.getPhonebookPermissionChoice()
                    == CachedBluetoothDevice.ACCESS_ALLOWED);

        } else if (profile instanceof PanProfile) {
            profilePref.setChecked(profile.getConnectionStatus(device) ==
                    BluetoothProfile.STATE_CONNECTED);

        } else {
            profilePref.setChecked(profile.isPreferred(device));
        }
    }

//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
    private void refreshProfilePreference(Switch profilePref,
            LocalBluetoothProfile profile) {
        BluetoothDevice device = mCachedDevice.getDevice();

        // Gray out Switch while connecting and disconnecting.
        profilePref.setEnabled(!mCachedDevice.isBusy());

        if (profile instanceof MapProfile) {
            profilePref.setChecked(mCachedDevice.getMessagePermissionChoice()
                    == CachedBluetoothDevice.ACCESS_ALLOWED);

        } else if (profile instanceof PbapServerProfile) {
            profilePref.setChecked(mCachedDevice.getPhonebookPermissionChoice()
                    == CachedBluetoothDevice.PBAP_CONNECT_RECEIVED); // MODIFIED by qianbo.pan, 2016-10-21,BUG-3175107

        } else if (profile instanceof PanProfile) {
            profilePref.setChecked(profile.getConnectionStatus(device) ==
                    BluetoothProfile.STATE_CONNECTED);

        } else {
            profilePref.setChecked(profile.isPreferred(device));
        }
    }

    private LocalBluetoothProfile getProfileOf(View v) {
//        if (!(v instanceof CheckBox)) {
//            return null;
//        }
        String key = (String) v.getTag();
        if (TextUtils.isEmpty(key)) return null;

        try {
            return mProfileManager.getProfileByName(key);
        } catch (IllegalArgumentException ignored) {
            return null;
        }
    }/* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    }
//[FEATURE]-Add-END by TCTNB.dongdong.gong
}

/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import com.android.settings.RestrictedListPreference.RestrictedItem;

import mst.app.dialog.AlertDialog;
import mst.app.dialog.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import mst.preference.ListPreference;
import mst.preference.ListPreferenceDialogFragment;
import mst.preference.PreferenceFragment;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ListView;
import android.widget.SpinnerPopupDialog;

public class CustomListPreference extends ListPreference {

	private static final java.lang.String KEY_CLICKED_ENTRY_INDEX = "settings.CustomListPrefDialog.KEY_CLICKED_ENTRY_INDEX";

	private int mClickedDialogEntryIndex;
	public CustomListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

    public CustomListPreference(Context context, AttributeSet attrs, int defStyleAttr,
                                int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    
    
    
    @Override
    protected void onPrepareListDialog(SpinnerPopupDialog dialog) {
    	// TODO Auto-generated method stub
    	super.onPrepareListDialog(dialog);
    	
    }
    
    
    protected DialogInterface.OnClickListener getOnItemClickListener() {
        return new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                setClickedDialogEntryIndex(which);


                    /*
                     * Clicking on an item simulates the positive button
                     * click, and dismisses the dialog.
                     */
                    onClick(dialog,
                            DialogInterface.BUTTON_POSITIVE);
                    dialog.dismiss();
            }
        };
    }

    protected void onDialogCreated(Dialog dialog){
    	
    }
    
    @Override
    protected void showDialog(Bundle state) {
    	// TODO Auto-generated method stub
    	super.showDialog(state);
    	onDialogCreated(getDialog());
    }
    
    
    protected void setClickedDialogEntryIndex(int which) {
        mClickedDialogEntryIndex = which;
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
       super.onDialogClosed(positiveResult);
        if (mClickedDialogEntryIndex >= 0 &&
                getEntryValues() != null) {
            String value = getEntryValues()[mClickedDialogEntryIndex].toString();
            if (callChangeListener(value)) {
                setValue(value);
            }
        }
    }
    

}

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.settings;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;

import com.android.internal.net.VpnProfile;
import com.android.settings.vpn2.AppPreference;
import mst.preference.Preference;
import com.android.settingslib.RestrictedPreference;

/**
 * A preference with a Gear on the side
 */
public class VpnItemPreference extends RestrictedPreference implements View.OnClickListener {
    private VpnProfile mProfile;
    public static int STATE_NONE = -1;

    boolean mIsAlwaysOn = false;
    int mState = STATE_NONE;
    private boolean mSelected;
    private OnIconClickListener mIconClickListener;

    public VpnItemPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSelected = false;
        setLayoutResource(R.layout.preference_widget_vpn);
    }

    public VpnItemPreference(Context context) {
        super(context, null);
        mSelected = false;
        setLayoutResource(R.layout.preference_widget_vpn);
    }

    public VpnProfile getProfile() {
        return mProfile;
    }

    public void setProfile(VpnProfile profile) {
        final String oldLabel = (mProfile != null ? mProfile.name : null);
        final String newLabel = (profile != null ? profile.name : null);
        if (!TextUtils.equals(oldLabel, newLabel)) {
            setTitle(newLabel);
            notifyHierarchyChanged();
        }
        mProfile = profile;
    }

    @Override
    public void onBindView(final View holder) {
        super.onBindView(holder);
        View select_icon = holder.findViewById(R.id.select_icon);
        View summary_view = holder.findViewById(com.android.internal.R.id.summary);
        summary_view.setVisibility(View.VISIBLE);
        ((RadioButton)holder.findViewById(R.id.select_button)).setChecked(mSelected);
        select_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.select_icon) {
            Log.d("zhouyong","select icon ...");
            if (mIconClickListener != null) {
                mIconClickListener.onClick(this);
            }
        }
    }

    @Override
    public int compareTo(Preference preference) {
        if (preference instanceof VpnItemPreference) {
            VpnItemPreference another = (VpnItemPreference) preference;
            int result;
            if ((result = another.mState - mState) == 0 &&
                    (result = mProfile.name.compareToIgnoreCase(another.mProfile.name)) == 0 &&
                    (result = mProfile.type - another.mProfile.type) == 0) {
                result = mProfile.key.compareTo(another.mProfile.key);
            }
            return result;
        } else {
            return super.compareTo(preference);
        }
    }

    public boolean isAlwaysOn() {
        return mIsAlwaysOn;
    }

    public int getState() {
        return mState;
    }

    public void setState(int state) {
        if (mState != state) {
            mState = state;
            updateSummary();
            notifyHierarchyChanged();
        }
    }

    public void setAlwaysOn(boolean isEnabled) {
        if (mIsAlwaysOn != isEnabled) {
            mIsAlwaysOn = isEnabled;
            updateSummary();
        }
    }

    public void setIconClickListener(OnIconClickListener l) {
        mIconClickListener = l;
    }
    public interface OnIconClickListener {
        void onClick(Preference p);
    }

    /**
     * Update the preference summary string (see {@see Preference#setSummary}) with a string
     * reflecting connection status and always-on setting.
     *
     * State is not shown for {@code STATE_NONE}.
     */
    protected void updateSummary() {
        final Resources res = getContext().getResources();
        final String[] states = res.getStringArray(R.array.vpn_states);
        String summary = (mState == STATE_NONE ? "" : states[mState]);
        if (mIsAlwaysOn) {
            final String alwaysOnString = res.getString(R.string.vpn_always_on_active);
            summary = TextUtils.isEmpty(summary) ? alwaysOnString : res.getString(
                    R.string.join_two_unrelated_items, summary, alwaysOnString);
        }
        setSummary(summary);
    }
    public void setVpnChecked(boolean selected){
        mSelected=selected;
        notifyChanged();
    }
}

/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_EFFECT_AC_H_
#define ANDROID_EFFECT_AC_H_

#include <hardware/audio_effect.h>

#if __cplusplus
extern "C" {
#endif

// this effect is not defined in OpenSL ES as one of the standard effects
static const effect_uuid_t FX_IID_AC_ =
        {0x951a8ac1, 0xf771, 0x4166, 0xb8bd, {0xf4, 0x5e, 0x6e, 0xb2, 0xb0, 0x4c}};
const effect_uuid_t * const FX_IID_AC = &FX_IID_AC_;

#if __cplusplus
}  // extern "C"
#endif


#endif /*ANDROID_EFFECT_AC_H_*/

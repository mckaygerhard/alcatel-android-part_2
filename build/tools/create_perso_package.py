#!/usr/bin/python

import sys
import os
import xml.dom.minidom as minidom
import zipfile
import re

def create_perso_zip(currentpath,jrdresout):
    os.chdir(jrdresout)
    targetZip = os.path.join(currentpath,'perso_build.zip')
    if os.path.isfile(targetZip):
        os.remove(targetZip)
    zipFile = zipfile.ZipFile(targetZip,'w',zipfile.ZIP_DEFLATED)
    for dirpath,dirname,filenames in os.walk('.'):
        m = re.search('device',dirpath)
        if m:
            continue
        else:
            copydir = os.path.split(dirpath)[1]
            m = re.search('values',copydir)
            for filename in filenames:
                if m or copydir == 'xml':
                    zipFile.write(os.path.join(dirpath,filename))
    os.chdir(currentpath)
    if os.path.isfile('app-info.xml'):
        zipFile.write('app-info.xml')
        os.remove('app-info.xml')
    try:
        zipFile.write(os.path.join('development','tcttools','mergeplf','mergeplf'))
        zipFile.write(os.path.join('development','tcttools','mergeplf','mergeplf.jar'))
    except OSError:
        print "ERROR: No such file or directory: development/tcttools/mergeplf"
        sys.exit(2)
    except:
        print "ERROR: when zip files in development/tcttools/mergeplf"
        sys.exit(-1)
    zipFile.close()

def main(productout,jrdresout):
    impl = minidom.getDOMImplementation()
    dom = impl.createDocument(None,'packages',None)
    root = dom.documentElement

    packageFile = open(os.path.join(productout,'apps-info.txt'), 'r')
    for line in packageFile:
        m = re.search(r'NAME="([^"]*)" PATH="([^"]*)" INSTALLED="([^"]*)"', line)
        moduleN = m.group(1)
        srcpath = m.group(2)
        apkpath = m.group(3).split(' ')[0]

        if os.path.exists(apkpath):
            apkInfo = dom.createElement('package')
            apkInfo.setAttribute('name',moduleN)
            apkInfo.setAttribute('path',srcpath)
            apkInfo.setAttribute('installed',apkpath)
            root.appendChild(apkInfo)
        else:
            continue

    packageFile.close()
    f = open('app-info.xml','w')
    dom.writexml(f, indent="", addindent="  ", newl="\n",encoding = 'utf-8')
    f.close()

    currentpath = os.getcwd()
    create_perso_zip(currentpath,jrdresout)


if __name__ == "__main__":
    main(sys.argv[1],sys.argv[2])

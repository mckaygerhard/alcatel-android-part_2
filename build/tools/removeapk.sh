#!/bin/bash

#    apk_dirname=""
#    apk_fullpath=""
    currentdir=$(pwd)
    echo $currentdir
    JRD_OUT_SYSTEM=$currentdir/out/target/product/$TARGET_PRODUCT/system
    echo $JRD_OUT_SYSTEM

    #get apk list which isdm "JRD_PRODUCT_PACKAGES" value is set to "0" in isdm_sys_properties.plf
    JRD_PRODUCT_PACKAGES=($(CALLED_FROM_SETUP=true \
                       BUILD_SYSTEM=build/core \
                       command make --no-print-directory \
                       -f build/core/config.mk \
                       dumpvar-JRD_PRODUCT_PACKAGES))
    echo ${JRD_PRODUCT_PACKAGES}
    export ${JRD_PRODUCT_PACKAGES}
    if [ -f "remove_apks.log" ] ; then
        rm remove_apks.log
    fi
    # remove apks in /system/app or /system/priv-app, when apk isdm value is "0" in isdm_sys_properties.plf
    for apk in ${JRD_PRODUCT_PACKAGES[@]}
    do 
         echo $apk
        if [ -d $JRD_OUT_SYSTEM/app/$apk ] ; then
            apk_dirname=$JRD_OUT_SYSTEM/app/$apk
            apk_fullpath=$JRD_OUT_SYSTEM/app/$apk/$apk.apk
        elif [ -d $JRD_OUT_SYSTEM/priv-app/$apk ] ; then
            apk_dirname=$JRD_OUT_SYSTEM/priv-app/$apk
            apk_fullpath=$JRD_OUT_SYSTEM/priv-app/$apk/$apk.apk
        elif [ -f $JRD_OUT_SYSTEM/app/$apk.apk ] ; then
            apk_dirname=$JRD_OUT_SYSTEM/app/$apk.apk
            apk_fullpath=$JRD_OUT_SYSTEM/app/$apk.apk
        elif [ -f $JRD_OUT_SYSTEM/priv-app/$apk.apk ] ; then
            apk_dirname=$JRD_OUT_SYSTEM/priv-app/$apk.apk
            apk_fullpath=$JRD_OUT_SYSTEM/priv-app/$apk.apk
        else
            echo "WARNING:CANNOT find $apk in /system"
            apk_dirname=""
            apk_fullpath=""
            continue
        fi
        if [ -n "$apk_dirname" ] ; then
            rm -rf $apk_dirname
            echo "apkfile_name: $apk_fullpath" >> remove_apks.log
        else
            echo "do nothing"
        fi
    done

package com.tct.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.tct.android.tools.feature.app.Main;

public class ConfigFile {

	public static boolean readLines(File f, Read read) {
		if (!f.exists()) return false;
	    BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(f));
            String line = null;
            while ((line = br.readLine()) != null) {
            	if(!read.onLine(line)){
            		Main.LOG.e("Config File May Has Error Format! LINE:" + line);
            		return false;
            	}
            }
        } catch (Exception e) {
            Main.LOG.e("Read Config File ERROR!");
        } finally {
            try {
            	read.onFinish();
                br.close();
            } catch (IOException e) {
            }
        }
        return true;
	}
	
	public interface Read {
		public boolean onLine(String line);
		public void onFinish();
	}
}

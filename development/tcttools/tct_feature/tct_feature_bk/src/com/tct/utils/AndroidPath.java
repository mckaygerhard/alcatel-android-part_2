package com.tct.utils;

public class AndroidPath {

    public static String getRelativeDir(String dir) {
    	String relDir = "";
    	dir = dir.trim();
    	int num = dir.split("/").length;
        if (dir.startsWith("/")) {
            num--;
        }
        for (int i=0; i<num; i++) {
        	relDir += "../";
        } 
        return relDir;
    }
    
    //CHANGE "./aa/./bb/././cc/" TO "aa/bb/cc"
    public static String plainPath(String pathString) {
    	return clearHeadTail(pathString).replaceAll("\\/\\.\\/", "\\/");
    }

    private static String clearHeadTail(String pathString) {
        String ret = pathString.trim();
        if (ret.startsWith("./")) {
            ret = ret.substring(2).trim();
        }
        if (ret.endsWith("/")) {
            ret = ret.substring(0, ret.length()-1).trim();
        }
        return ret;
    }
}

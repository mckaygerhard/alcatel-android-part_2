package com.tct.android.tools.feature.plf2fq;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tct.android.tools.feature.app.Main;

public class SplfParser {
    // Format:
    // <VAR>
    // <SDMID>def_version_info_dialog_hardware_version_content</SDMID>
    // <VALUE>"MSM7227B"</VALUE>
    // </VAR>
    private static final String KEY_VAR   = "VAR";
    private static final String KEY_SDMID = "SDMID";
    private static final String KEY_VALUE = "VALUE";

    public synchronized static void parse(String file,
            final HashMap<String, FqJava.Field> fields) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            InputStream is = new FileInputStream(file);
            SAXParser parser = factory.newSAXParser();
            parser.parse(is, new DefaultHandler() {
                private boolean inVAR;
                private String tag;
                private String sdmid;
                private String sdmval;

                @Override
                public void startElement(String uri, String localName,
                        String name, Attributes atrbts) throws SAXException {
                    super.startElement(uri, localName, name, atrbts);
                    if (name.equals(KEY_VAR)) {
                        inVAR = true;
                    }
                    tag = name;
                }

                @Override
                public void characters(char[] chars, int i, int i1)
                        throws SAXException {
                    super.characters(chars, i, i1);
                    String value = new String(chars, i, i1);
                    if (inVAR) {
                        if (tag.equals(KEY_SDMID)) {
                            sdmid = value;
                            return;
                        } 
                        if (tag.equals(KEY_VALUE)) {
                            sdmval = value;
                            return;
                        }
                    }
                }

                @Override
                public void endElement(String uri, String localName, String name)
                        throws SAXException {
                    super.endElement(uri, localName, name);
                    if (name.equals(KEY_VAR)) {
                        inVAR = false;
                        if (sdmid != null) {
                            if (fields.containsKey(sdmid)) {
                                FqJava.Field field = fields.get(sdmid);
                                field.value = Perso.convertValue(field.type, sdmval);
                            } else {
                                Main.LOG.w("No SDMID define in plf or xplf. SDMID=" + sdmid);
                            }
                            sdmid = null;
                            sdmval = null;
                        }
                    }
                    tag = "";
                }
            });
        } catch (Exception ex) {
            Main.LOG.e("PlfParser Error:" + ex);
        }
    }
}

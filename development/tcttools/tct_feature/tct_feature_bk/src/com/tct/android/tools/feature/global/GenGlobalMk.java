package com.tct.android.tools.feature.global;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.tct.android.tools.feature.global.GlobalConf.Section;

public class GenGlobalMk {
	private final static String BUILD_MK_FILE = "global_build.mk";
	private FileWriter fw;
	
	public GenGlobalMk(String outDir) {
		File file  = new File(outDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String outputJavaFile = file.getPath() + "/" + BUILD_MK_FILE;
        try {
            fw = new FileWriter(outputJavaFile);
        }catch (IOException e) {
        }
	}
	
	
	public void write(Section section) {
		try {
			fw.write(section.finalName() + " := " + section.value() + "\n");
		} catch (IOException e) {
		}
	}
	
	public void close(){
		try {
			fw.close();
		} catch (IOException e) {
		}
	}
}

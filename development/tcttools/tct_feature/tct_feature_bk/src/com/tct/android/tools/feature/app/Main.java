package com.tct.android.tools.feature.app;

import java.util.HashMap;
import java.util.Map;

import com.tct.utils.Log;


public class Main {
    public static final Log LOG = Log.instance("Feature")
    		                         .setLogLevel(Log.DEBUG)
    		                         .setOutputs(Log.FILE);

    private static final String CMD_PLF2FQ = "plf2fq";
    private static final String CMD_GLOBAL = "global";
    private static final String CMD_SDM2H  = "sdm2h";
    private static final String CMD_SDM2J  = "sdm2j";
    private static final Map<String, Command> sCommands;
    
    static {
        sCommands = new HashMap<String, Command>();
        sCommands.put(CMD_PLF2FQ, new Plf2fq());
        sCommands.put(CMD_GLOBAL, new Global());
        sCommands.put(CMD_SDM2H, new SDM2H());
        sCommands.put(CMD_SDM2J, new SDM2J());
    }
    
    public static void main(String[] args) {
        LOG.i("Start App Feature:" + getArgs(args));
        int status = Command.ACT_STATUS_NOCMD;
        if (args.length > 0) {
            Command cmd = sCommands.get(args[0]);
            if (cmd != null) {
                status = cmd.action(args);
            }
        }
        LOG.i("System.exit(" + status + ")\n");
        System.exit(status);
    }

    private static String getArgs(String[] args) {
        if (args == null | args.length == 0) {
            return "args.len=0";
        } else {
            String ret = "args.len=" + args.length;
            for (String s : args) {
                ret = ret + " " + s;
            }
            return ret;
        }
    }   
    
    static interface Command {
        public static final int ACT_STATUS_OK = 0;
        public static final int ACT_STATUS_NOCMD = -1;
        public static final int ACT_STATUS_INVALID_PARAM = -2;
        public static final int ACT_STATUS_RUNTIME_ERR = -8;
        public static final int ACT_STATUS_ERR = -9;

        public int action(String[] args);
    }
}

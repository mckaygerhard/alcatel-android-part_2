package com.tct.android.tools.feature.sdm2;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tct.android.tools.feature.app.Main;


public class SPLF_File {
    private static final String KEY_VAR   = "VAR";
    private static final String KEY_SDMID = "SDMID";
    private static final String KEY_VALUE = "VALUE";
    private static final String KEY_DESC = "DESC";
	private File mFile;
	private OnSdmListener mListener;
	public SPLF_File(File splf, OnSdmListener listener){
//		if (splf.getName().endsWith(".splf")) {
			mFile = splf;
			mListener = listener;
//		}
	}
	
	public boolean parse() {
		if (mFile == null) return false;
		SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            InputStream is = new FileInputStream(mFile);
            SAXParser parser = factory.newSAXParser();
            parser.parse(is, new DefaultHandler() {
                private boolean inVAR;
                private String tag;
                private String sdmid;
                private String sdmval;
                private String sdmdesc;

                @Override
                public void startElement(String uri, String localName,
                        String name, Attributes atrbts) throws SAXException {
                    super.startElement(uri, localName, name, atrbts);
                    if (name.equals(KEY_VAR)) {
                        inVAR = true;
                    }
                    tag = name;
                }

                @Override
                public void characters(char[] chars, int i, int i1)
                        throws SAXException {
                    super.characters(chars, i, i1);
                    String value = new String(chars, i, i1);
                    if (inVAR) {
                        if (tag.equals(KEY_SDMID)) {
                            sdmid = value;
                            return;
                        } 
                        if (tag.equals(KEY_VALUE)) {
                            value = escapeXML(value);
                            if (sdmval == null) {
                                sdmval = value;
                            }else {
                                sdmval = sdmval + value;
                            }
                            return;
                        }
                        if(tag.equals(KEY_DESC)) {
                            value = escapeXML(value);
                            if (sdmdesc == null) {
                                sdmdesc = value;
                            }else {
                                sdmdesc = sdmdesc + value;
                            }
                            return;
                        }
                    }
                }

                @Override
                public void endElement(String uri, String localName, String name)
                        throws SAXException {
                    super.endElement(uri, localName, name);
                    if (name.equals(KEY_VAR)) {
                        inVAR = false;
                        if (sdmid != null) {
                        	mListener.onSdmUpdate(sdmid, sdmval);
                            sdmid = null;
                            sdmval = null;
                            sdmdesc = null;
                        }
                    }
                    tag = "";
                }
            });
        } catch (Exception ex) {
            Main.LOG.e("PlfParser Error:" + ex);
        }
		return true;
	}
	
	static String escapeXML(String value) {
        if (value == null) {
            return null;
        }
        char content[] = new char[value.length()];
        value.getChars(0, value.length(), content, 0);
        StringBuffer result = new StringBuffer(content.length+50);
        for(int i=0; i<content.length; i++) {
            switch(content[i]) {
            case 60: // '<'
                result.append("&lt;");
                break;
            case 62: // '>'
                result.append("&gt;");
                break;
            case 38: // '&'
                result.append("&amp;");
                break;
            default:
                result.append(content[i]);
                break;
            }
        }
        return result.toString();
    }
}

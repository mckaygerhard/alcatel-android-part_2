package com.tct.android.tools.feature.sdm2;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.tct.android.tools.feature.app.Main;

public class SDM2 {
	private static final String DEVICE_TCT = "device/tct";
	private static final String PERSO_DIR = "perso/plf";
	//此变量为了区别sdmid是定义在plf还是xplf中的， 定义在plf中的变量会放在这里
	private List<String> definedInPlfSdmidList = new ArrayList<String>(); 
	private boolean isMakePerso = false;
	
	private List<String> strSdmidList = new ArrayList<String>();
	private List<String> numSdmidList = new ArrayList<String>();
	private List<String> mList;
	//此构造是SDM2Java
	public SDM2() {
		mList = SdmMapList.LIST;
		fetchCommon();
		sort(numSdmidList);
		sort(strSdmidList);	
	}
	//此构造是SDM2H
	public SDM2(boolean ismp, String product) {
		isMakePerso = ismp;
		mList = SdmMapList.LIST;
		fetchCommon();
		fetchProduct(product);
		sort(numSdmidList);
		sort(strSdmidList);		
//		System.out.println(numSdmidList);//		System.out.println(strSdmidList);//		SdmTable.print();
	}
	
	public List<String> getStrSdmidList(){
		return strSdmidList;
	}
	
	public List<String> getNumSdmidList(){
		return numSdmidList;
	}
	
	public boolean isDefinedInPLF(String sdmid) {
		return definedInPlfSdmidList.contains(sdmid);
	}
	
	private void fetchCommon() {
		String dirString = DEVICE_TCT + "/common/" + PERSO_DIR;
		File dirFile = new File(dirString);
		if (dirFile.exists()) {
			fetch(dirFile);
		}
	}
	
	private void fetchProduct(String product) {
		String dirString = DEVICE_TCT + "/" + product.trim() + "/" + PERSO_DIR;
		File dirFile = new File(dirString);
		if (dirFile.exists()) {
			fetch(dirFile);
		}
	}
	
	private void fetch(File dirFile) {
		File[] fs = dirFile.listFiles();
		for (File f : fs) {
			if (f.isDirectory()) {
				fetch(f);
			} else {
				String name = f.getName();
				if (name.endsWith(".plf") || name.endsWith(".xplf")) {
					if (!new PLF_File(f, mListener).parse()) {
						Main.LOG.e("PLF_File parse Error: File:" + f.getPath());
					}
				} else if (name.endsWith(".splf")) {
					if (!new SPLF_File(f, mListener).parse()) {
						Main.LOG.e("SPLF_File parse Error: File:" + f.getPath());
					}
				}
			}
		}
	}
	
	private void sort(List<String> l) {
		if (!l.isEmpty()) {
			Collections.sort(l, new Comparator<Object>(){
			      @Override
			      public int compare(Object arg0, Object arg1) {
			      	String v1 = (String)arg0;
			      	String v2 = (String)arg1;
			          return v1.compareTo(v2);
			      }
			  });
		}
	}
	
	private OnSdmListener mListener = new OnSdmListener() {
		@Override
		public void onSdmUpdate(String sdmid, String value) {
			Main.LOG.d(sdmid +  "   ---   "  + value);
			if (mList.contains(sdmid.trim())) {
				if (isMakePerso && isDefinedInPLF(sdmid.trim())) return; 
				SdmTable.updateVar(sdmid, value);
			}
		}
		
		private void  addToListIfNotExist(List<String> l, String s) {
			if (!l.contains(s)) {
				l.add(s);
			}
		}
		@Override
		public void onNewSdm(Boolean definedInPlf, String sdmid, String type,
				String value) {
			Main.LOG.d(sdmid +  "   ---   "  + type + "   ---   "  + value);
			if (mList.contains(sdmid.trim())) {
				SdmTable.addOrUpdateVar(sdmid, type, value);
				SdmTable.VarInfo varInfo = SdmTable.getVar(sdmid);
				if (definedInPlf) {
					addToListIfNotExist(definedInPlfSdmidList, varInfo.sdmid());
				}
				if (varInfo.isNum()) {
					addToListIfNotExist(numSdmidList,varInfo.sdmid());
				} else {
					addToListIfNotExist(strSdmidList,varInfo.sdmid());
				}
			}
		}
	};

}

package com.tct.android.tools.feature.global;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tct.android.tools.feature.app.Main;
import com.tct.utils.ConfigFile;

//File Format: tct_feature.global
//[TCT_TARGET_UI] | [TCT_TARGET_UI_#TCT_TARGET_UI.value#]
//type = [boolean, string, integer]
//value = 
//target = [java|make|native|kernel|all]
//
public class GlobalConf {
	public final static String COMMON_CONF_PATH = "device/tct/common";
	public final static String PRODUCT_CONF_PATH = "device/tct";
	public final static String CONF_FILE_NAME = "tct_feature.global";
	
	private final static String SUPPORT_KEY_TYPE = "type";
	private final static String SUPPORT_KEY_VALUE = "value";
	private final static String SUPPORT_KEY_TARGET = "target";
	private final static String SUPPORT_KEY_MP_VALUE = "mp_branch_value";

	private final static String MP_KEY_VALUE = "TCT_FEATURE_MP_BRANCH";
	private static boolean isMP = false;
	private static boolean mpChecked = false;
	
	private String mProduct = null;
	private Map<String, Section> mSections= new HashMap<String, Section>();
	public GlobalConf(String product) {
		mProduct = product;
	}
	
	public boolean parse() {
		File commonFile = new File(COMMON_CONF_PATH + "/" + CONF_FILE_NAME);
		if (!commonFile.exists()) return false;
		ConfigFile.readLines(commonFile, rgc);
		
		File productFile = new File(PRODUCT_CONF_PATH + "/" + mProduct + "/" + CONF_FILE_NAME);
		if (productFile.exists()) {
			ConfigFile.readLines(productFile, rgc);
		}		
		return true;
	}
	
	public Collection<Section> getSections() {
		return mSections.values();
	}
	
	ReadGlobalConfig rgc = new ReadGlobalConfig();
	class ReadGlobalConfig implements ConfigFile.Read {
		private Section section = null;
		public boolean onLine(String line) {
			line = line.trim();
			if (line.length() == 0) return true;
			if (line.startsWith("#") || line.startsWith(";")) return true; //# or ; is comment
			if (line.startsWith("[") && line.endsWith("]")) {
				if (section != null) {
					if (mSections.get(section.name) == null) {
					    mSections.put(section.name, section);
					}
				}
				String name = line.substring(1, line.length()-1).trim();
				section = mSections.get(name);
				if (section == null) {
					section = new Section();
					section.name = name;
				}
			} else {
				int splitPos = line.indexOf("=");
				if (splitPos == -1) return false;
				String key = line.substring(0, splitPos).trim();
				//Main.LOG.e("Key = |" + key + "|");
				String val = "";
				if (splitPos < line.length()) {
					val = line.substring(splitPos + 1).trim();
				}
				if (SUPPORT_KEY_TYPE.equals(key)) {
					section.type = val;
				} else if(SUPPORT_KEY_VALUE.equals(key)) {
					if (!mpChecked && MP_KEY_VALUE.equals(section.name)) {
						mpChecked = true;
						isMP = "true".equals(val) ? true :false;
					}
					if (!section.mpValue) {
						section.value = val;
					}
				} else if(SUPPORT_KEY_TARGET.equals(key)) {
					section.target = val;
				} else if (SUPPORT_KEY_MP_VALUE.equals(key)) {
					if (isMP) {
						section.value = val;
						section.mpValue = true;
					}
				} else {
					Main.LOG.e("KEY " + key + " is NOT support.");
					return false;
				}
			}
			return true;
		}	
		public void onFinish(){
			if (section != null) {
				if (mSections.get(section.name) == null) {
				    mSections.put(section.name, section);
				}
			}
		}
	}
	
	
	public class Section {
		String fname = null;
		String name="";
		String type="";
		String value="";
		String target="";
		boolean mpValue = false;
		private Section(){}
		
		public String finalName() {
			if (!name.contains("#")) {
				return name;
			} else {
				if (fname != null) {
					return fname;
				}
				List<String> list = new ArrayList<String>();
				int pos = 0;
				while((pos = name.indexOf("#", pos+1)) != -1){
					list.add(name.substring(pos+1, (pos = name.indexOf("#", pos+1))));
				}
				Map<String, String> map = new HashMap<String, String>();
				for(String key : list){
					if (key.contains(".")) {
						String[] ss = key.split("\\.");
						Section sec = mSections.get(ss[0]);
						if (sec == null) {
							return null;
						}
						String valString = sec.getOwnValue(ss[1]);
						if (valString == null) {
							return null;
						}
						map.put(key, valString);
					} else {
						String valString = getOwnValue(key);
						if (valString == null) {
							return null;
						}
						map.put(key, valString);
					}
				}
				Set<String> kSet = map.keySet();
				String finalNameString = name;
				for(String k : kSet){
					String v = map.get(k);
					finalNameString = finalNameString.replaceAll("#"+k+"#", v.toUpperCase());
				}
				fname = finalNameString;
				return fname;
			}
		}
		public String type() {
			return type;
		}
		public String value() {
			return value;
		}
		public String target(){
			return target;
		}
		
		private String getOwnValue(String key){
			if (SUPPORT_KEY_TYPE.equals(key)) {
				return type;
			} else if(SUPPORT_KEY_VALUE.equals(key)) {
				return value;
			} else if(SUPPORT_KEY_TARGET.equals(key)) {
				return target;
			}
			return null;
		}
	}
}

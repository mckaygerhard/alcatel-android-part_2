package com.tct.android.tools.feature.global;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GenGlobalJava {
	FileWriter fw = null; 
	public GenGlobalJava(String outDir) {
		File file  = new File(outDir + "/src/com/tct/feature");
        if (!file.exists()) {
            file.mkdirs();
        }
        String outputJavaFile = file.getPath() + "/Global.java";
        try {
            fw = new FileWriter(outputJavaFile);
            fw.write("package com.tct.feature;\n");
            fw.write("public final class Global {\n");
        }catch (IOException e) {
        }
	}
	
	public void write(String name, String type, String value) {
		type = getType(type);
		if (type != null) {
			if (type.equals("String")) {
				value = "\"" + value + "\"";
			}
			String line = "    public final static " + type + " " + name + " = " + value + ";\n";
			try {
				fw.write(line);
			} catch (Exception e) {
			}
		}
	}
	
	public void close(){
		try {
			fw.write("}\n");
			fw.close();
		} catch (IOException e) {
		}
	}
	
	private String getType(String type){
		if ("boolean".equalsIgnoreCase(type)) {
			return "boolean";
		}
		if ("string".equalsIgnoreCase(type)) {
			return "String";
		}
		if ("integer".equalsIgnoreCase(type)) {
			return "int";
		}
		return null;
	}
}

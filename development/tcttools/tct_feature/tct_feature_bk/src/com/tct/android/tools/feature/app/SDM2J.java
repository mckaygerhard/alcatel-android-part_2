package com.tct.android.tools.feature.app;

import static com.tct.android.tools.feature.app.Main.LOG;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.tct.android.tools.feature.sdm2.SDM2;
import com.tct.android.tools.feature.sdm2.SdmTable;
import com.tct.utils.AndroidPath;

/**
 * CommandLine: tct_feature sdm2j ${OUT_DIR}
 * 例如 tct_feature sdm2j out/target/product/riol_tf/tct_intermediates/feature/tct.feature_query
 * 但是最后会在out/target/product/riol_tf/tct_intermediates/feature/tct.feature_query/src目录下生成java文件
 * “src”这子目录是本工具自己生成。
 * 最后的文件为：
 * out/target/product/riol_tf/tct_intermediates/feature/tct.feature_query/src/com/tct/feature/PLF.java 和
 * out/target/product/riol_tf/tct_intermediates/feature/tct.feature_query/sdm2j.mk
 */
public class SDM2J implements Main.Command {
	// tct.feature_query的目录必须是vendor/tct/source/feature_query， 如若不是则必须修改下面RELATIVE_FQ_DIR的值。
	private static final String RELATIVE_FQ_DIR = "../../../../";
	private static final String MK_FILE = "sdm2j.mk";
	private static final String JAVA_OUTPUT_PATH = "/src/com/tct/feature/PLF.java";
	private String mOutDir;
	
	private List<String> mStrSdmidList;
	private List<String> mNumSdmidList;	
	private List<String> mStringList;
	private List<String> mBooleanList;
	private List<String> mIntList;
	
	@Override
	public int action(String[] args) {
		if (args.length == 2) {
			mOutDir = AndroidPath.plainPath(args[1]);
			SDM2 sdm2j = new SDM2();
			mStrSdmidList = sdm2j.getStrSdmidList();
			mNumSdmidList = sdm2j.getNumSdmidList();
			
			mStringList = new ArrayList<String>();
			mBooleanList = new ArrayList<String>();
			mIntList = new ArrayList<String>();
			
			if (!mStrSdmidList.isEmpty()) {
				for(String sdm : mStrSdmidList){
					mStringList.add(sdm);
				}
				sort(mStringList);
			}
			
			if (!mNumSdmidList.isEmpty()) {
				for(String sdm : mNumSdmidList){
					if (SdmTable.getVar(sdm).type().equals(SdmTable.VarInfo.BOOLEAN)) {
						mBooleanList.add(sdm);
					} else {
						mIntList.add(sdm);
					}
				}
				sort(mBooleanList);
				sort(mIntList);
			}

			//write to PLF.java
			genPLFJava();
			//write to sdm2j.mk
			writeMkFile(); 			
		} else {
			LOG.e("COMMAND ERROR: args.LEN=" + args.length);
		}
		return 0;
	}
	
	
	//#########################################################//
	//START GEN PLF.java CODE AREA
	private static class SPACE {
		private static String getSpaces() {
			return SPACES[mSpaceNum];
		}
        private static void increase() {
            mSpaceNum++;
            if (mSpaceNum >= 10) {
                mSpaceNum = 10;
            }
        }
        private static void decrease() {
            mSpaceNum--;
            if (mSpaceNum < 0) {
                mSpaceNum = 0;
            }
        }
        private static int mSpaceNum = 0;
        private final static String[] SPACES = {
          "",
          "  ",
          "    ",
          "      ",
          "        ",
          "          ",
          "            ",
          "              ",
          "                ",
          "                  ",
          "                    ",
        };
    }
	FileWriter fw = null;
	private static final String RID_STR_PREFIX = "RS_"; //Rid_Str
	private static final String RID_NUM_PREFIX = "RN_"; //Rid_Num
    private void write(String str) throws IOException {
        if (str.contains("{")) {
            fw.write(SPACE.getSpaces() + str +"\n");
            SPACE.increase();
        } else if (str.contains("}")) {
            SPACE.decrease();
            fw.write(SPACE.getSpaces() + str +"\n");
        } else {
            fw.write(SPACE.getSpaces() + str +"\n");
        }
    }
    private void genPLFJava() {
        File file  = new File(mOutDir + "/src/com/tct/feature");
        if (!file.exists()) {
            file.mkdirs();
        }
        String oPLFJavaFile = file.getPath() + "/PLF.java";
        try {
            fw = new FileWriter(oPLFJavaFile);
            write("//----This file is auto-generated.  DO NOT MODIFY.----//\n");
            write(SPACE.getSpaces() + "package com.tct.feature;");
            write(SPACE.getSpaces() + "public final class PLF {");

            //native method
            write("private native static void init(String[] strs, int sl, int[] nums, int nl);");
            
            write("private static final String[] STR_SDMIDS;");
            write("private static final int[] NUM_SDMIDS;");
            write("static {");
            int str_sdmids_len = mStrSdmidList.size();
            int num_sdmids_len = mNumSdmidList.size();
            write("STR_SDMIDS = new String[" + (str_sdmids_len+1) + "];");
            write("NUM_SDMIDS = new int[" + (num_sdmids_len+1) + "];");
            write("System.loadLibrary(\"android_tctfeature\");");
            write("init(STR_SDMIDS, " + str_sdmids_len + ", NUM_SDMIDS, " + num_sdmids_len+ ");");
            write("}"); //static {
            
            if (!mStringList.isEmpty() || !mBooleanList.isEmpty() || !mIntList.isEmpty()) {
            	write("public final static class R {");
            	int str_sdmids_size=0;
                if (!mStrSdmidList.isEmpty()) {
                	for (String sdm : mStrSdmidList) {
                		write("private static final int " + RID_STR_PREFIX + sdm + " = " + str_sdmids_size++ + ";");
                	}
                	fw.write("\n");
                }
                int num_sdmids_size=0;
                if (!mNumSdmidList.isEmpty()) {
                	for (String sdm : mNumSdmidList) {
                		write("private static final int " + RID_NUM_PREFIX + sdm + " = " + num_sdmids_size++ + ";");
                	}
                	fw.write("\n");
                }
            	// public final static class string
            	if (!mStringList.isEmpty()) {
            		write("public final static class string {");
            		for (String sdmid : mStringList) {
            			write(SdmTable.getVar(sdmid).toString());
            		}
            		//write static block to init value
            		write("static {");
            		for (String sdmid : mStringList) {
            			write(sdmid + " = " + "STR_SDMIDS[" + RID_STR_PREFIX + sdmid + "];");
            		}
                    write("}"); //static {
            		write("}"); //public final static class string
            	}
            	
            	// public final static class bool
            	if (!mBooleanList.isEmpty()) {
            		write("public final static class bool {");
            		for (String sdmid : mBooleanList) {
            			write(SdmTable.getVar(sdmid).toString());
            		}
            		//write static block to init value
            		write("static {");
            		for (String sdmid : mBooleanList) {
            			write(sdmid + " = " + "NUM_SDMIDS[" + RID_NUM_PREFIX + sdmid + "] == 0 ? false : true;");
            		}
                    write("}"); //static {
            		write("}"); //public final static class bool
            	}
            	
            	// public final static class integer
            	if (!mIntList.isEmpty()) {
            		write("public final static class integer {");
            		for (String sdmid : mIntList) {
            			write(SdmTable.getVar(sdmid).toString());
            		}
            		//write static block to init value
            		write("static {");
            		for (String sdmid : mIntList) {
            			write(sdmid + " = " + "NUM_SDMIDS[" + RID_NUM_PREFIX + sdmid + "];");
            		}
            		write("}"); //static {
            		write("}"); //public final static class integer
            	}
            	
            	write("}"); //public final static class R
            }
            write("}"); //public final class PLF 
        } catch (Exception e) {
            Main.LOG.e("PLF.java Error:" + e.toString());
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
            }
        }
        
    }
    //END GEN PLF.java CODE AREA
    //#########################################################//

	private void writeMkFile() {
		FileWriter fw = null;
		try {
			fw = new FileWriter(new File(mOutDir + "/" + MK_FILE));
			fw.write("LOCAL_SRC_FILES += " + RELATIVE_FQ_DIR + mOutDir + JAVA_OUTPUT_PATH); 
		} catch (IOException e) {
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
			}
		}
	}

	private void sort(List<String> l) {
		if (!l.isEmpty()) {
			Collections.sort(l, new Comparator<Object>(){
			      @Override
			      public int compare(Object arg0, Object arg1) {
			      	String v1 = (String)arg0;
			      	String v2 = (String)arg1;
			          return v1.compareTo(v2);
			      }
			  });
		}
	}
}

package com.android.mmi;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.display.DisplayManager;
import android.hardware.display.WifiDisplay;
import android.hardware.display.WifiDisplayStatus;
import android.media.MediaRouter;
import android.media.MediaRouter.RouteInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.internal.app.MediaRouteDialogPresenter;
import com.android.mmi.util.MMILog;

public class TvLink extends TestBase {
    private static final int CHANGE_ALL = -1;
    private static final int CHANGE_ROUTES = 1 << 1;
    private static final int CHANGE_WIFI_DISPLAY_STATUS = 1 << 2;
    private static final int MESSAGE_WIFI_DEFAULT = 1 << 3;
    private static final int MESSAGE_WIFI_ENABLE_REQUEST = MESSAGE_WIFI_DEFAULT + 1;
    private static final int LENGTH_WIFI_CHECK_TIME = 100;
    private int mPendingChanges;
    private WifiDisplayStatus mWifiDisplayStatus;
    private DisplayManager mDisplayManager;
    private WifiManager wifiManager;
    private Handler mHandler;
    private Handler handler;
    private ArrayList<HashMap<String, Object>> remotedisplays;
    private SimpleAdapter simpleAdapter;
    private ListView remoteDisplayListView;
//  private TextView messageTextView;
    private TextView emptyView;
    private MediaRouter mRouter;
    private boolean mWifiDisplayOn;
    private boolean mWifiOldState;
    private boolean mTvLinkOldState;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_tvlink,
                LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mDisplayManager = (DisplayManager) mContext
                .getSystemService(Context.DISPLAY_SERVICE);
        mRouter = (MediaRouter) mContext
                .getSystemService(Context.MEDIA_ROUTER_SERVICE);
        wifiManager = (WifiManager) mContext
                .getSystemService(mContext.WIFI_SERVICE);
        saveWifiDisplayOldState();
        wifiManager.setWifiEnabled(true);
        mWifiDisplayOn = Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.WIFI_DISPLAY_ON, 0) != 0;
        mHandler = new Handler();
        handler = new MemberHandler();
        remotedisplays = new ArrayList<HashMap<String, Object>>();
        remoteDisplayListView = (ListView) findViewById(R.id.list);
//      messageTextView = (TextView) findViewById(R.id.tvlink_message);
        emptyView = (TextView) findViewById(R.id.empty);
        remoteDisplayListView.setEmptyView(emptyView);
        simpleAdapter = new SimpleAdapter(mContext, remotedisplays,
                R.layout.manu_item_tv_link, new String[] {
                        "frendlyDisplayname", "status" }, new int[] {
                        R.id.manu_text, R.id.manu_text_status });
        remoteDisplayListView.setAdapter(simpleAdapter);
        remoteDisplayListView.setOnItemClickListener(new ItemClickListener());
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DisplayManager.ACTION_WIFI_DISPLAY_STATUS_CHANGED);
        mContext.registerReceiver(mReceiver, filter);
        mRouter.addCallback(MediaRouter.ROUTE_TYPE_REMOTE_DISPLAY,
                mRouterCallback, MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);
        // update(CHANGE_ALL);
        sendWifiEnableMessage();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(mReceiver);
        mRouter.removeCallback(mRouterCallback);
        unscheduleUpdate();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        removeAllhandlerMessages();
        restoreWifiDisplayOldState();
    }

    private boolean isWifiEnbled() {
        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            return true;
        } else {
            return false;
        }
    }

    private void saveWifiDisplayOldState() {
        mWifiOldState = wifiManager.isWifiEnabled();
        mTvLinkOldState = Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.WIFI_DISPLAY_ON, 0) != 0;
    }

    private void restoreWifiDisplayOldState() {
        wifiManager.setWifiEnabled(mWifiOldState);
        Settings.Global.putInt(mContext.getContentResolver(),
                Settings.Global.WIFI_DISPLAY_ON, mTvLinkOldState ? 1 : 0);
    }

    private void sendWifiEnableMessage() {
        if (isWifiEnbled()) {
            if (mWifiDisplayOn) {
                update(CHANGE_ALL);
            } else {
                Settings.Global.putInt(mContext.getContentResolver(),
                        Settings.Global.WIFI_DISPLAY_ON, 1);
                mWifiDisplayOn = Settings.Global.getInt(
                        mContext.getContentResolver(),
                        Settings.Global.WIFI_DISPLAY_ON, 0) != 0;
                update(CHANGE_ALL);
            }
        } else {
            wifiManager.setWifiEnabled(true);
            handler.sendEmptyMessageDelayed(MESSAGE_WIFI_ENABLE_REQUEST,
                    LENGTH_WIFI_CHECK_TIME);
        }
    }

    private void removeAllhandlerMessages() {
        handler.removeMessages(MESSAGE_WIFI_ENABLE_REQUEST);
    }

    private void update(int changes) {

        // Update wifi display state.
        if ((changes & CHANGE_WIFI_DISPLAY_STATUS) != 0) {
            mWifiDisplayStatus = mDisplayManager.getWifiDisplayStatus();
        }
        remotedisplays.removeAll(remotedisplays);
        boolean isConnected = false;
        // Add all known remote display routes.
        final int routeCount = mRouter.getRouteCount();
        for (int i = 0; i < routeCount; i++) {
            MediaRouter.RouteInfo route = mRouter.getRouteAt(i);
            if (route.matchesTypes(MediaRouter.ROUTE_TYPE_REMOTE_DISPLAY)) {
                WifiDisplay display = findWifiDisplay(route.getDeviceAddress());
                if (display != null) {
                    HashMap<String, Object> displayitem = new HashMap<String, Object>();
                    displayitem.put("frendlyDisplayname", route.getName());
                    displayitem.put("dispaly", display);
                    displayitem.put("route", route);
                    displayitem.put("status", route.getDescription());
                    if (route.isSelected()) {
                        if (route.isConnecting()) {
                            displayitem.put("status", "connecting");
                        } else {
                            displayitem.put("status", "connected");
                            isConnected = true;
                        }
                    } else {
                        if (!route.isEnabled()) {
                            if (route.getStatusCode() == MediaRouter.RouteInfo.STATUS_IN_USE) {
                                displayitem.put("status", "In use");
                            } else {
                                displayitem.put("status", "Unavailable");
                            }
                        }
                    }
                    remotedisplays.add(displayitem);
                }
            }
        }
        // Additional features for wifi display routes.
        if (mWifiDisplayStatus != null
                && mWifiDisplayStatus.getFeatureState() == WifiDisplayStatus.FEATURE_STATE_ON) {
            // Add all unpaired wifi displays.
            for (WifiDisplay display : mWifiDisplayStatus.getDisplays()) {
                if (!display.isRemembered()
                        && display.isAvailable()
                        && !display.equals(mWifiDisplayStatus
                                .getActiveDisplay())) {
                    HashMap<String, Object> displayitem = new HashMap<String, Object>();
                    displayitem.put("frendlyDisplayname",
                            display.getFriendlyDisplayName());
                    displayitem.put("dispaly", display);
                    displayitem.put("status", "Wireless display");
                    if (!display.canConnect()) {
                        displayitem.put("status", "In use");
                    }
                    remotedisplays.add(displayitem);
                }

            }
        }

        MMILog.i(TAG, "remote displays num: " + remotedisplays.size());
        if (isConnected) {
            setPassButtonEnable(true);
        }

        simpleAdapter.notifyDataSetChanged();
    }

    private WifiDisplay findWifiDisplay(String deviceAddress) {
        if (mWifiDisplayStatus != null && deviceAddress != null) {
            for (WifiDisplay display : mWifiDisplayStatus.getDisplays()) {
                if (display.getDeviceAddress().equals(deviceAddress)) {
                    return display;
                }
            }
        }
        return null;
    }

    private void scheduleUpdate(int changes) {
        if (mPendingChanges == 0) {

            mHandler.post(mUpdateRunnable);
        }
        mPendingChanges |= changes;
    }

    private void unscheduleUpdate() {

        if (mPendingChanges != 0) {
            mPendingChanges = 0;
            mHandler.removeCallbacks(mUpdateRunnable);
        }

    }

    private void toggleRoute(MediaRouter.RouteInfo route) {
        if (route != null) {
            if (route.isSelected()) {
                MediaRouteDialogPresenter.showDialogFragment(mContext,
                        MediaRouter.ROUTE_TYPE_REMOTE_DISPLAY, null);
            } else {
                route.select();
            }
        }
    }

    private void pairWifiDisplay(WifiDisplay display) {
        if (display.canConnect()) {
            mDisplayManager.connectWifiDisplay(display.getDeviceAddress());
        }
    }

    private final Runnable mUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            final int changes = mPendingChanges;
            mPendingChanges = 0;
            update(changes);
        }
    };

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action
                    .equals(DisplayManager.ACTION_WIFI_DISPLAY_STATUS_CHANGED)) {
                scheduleUpdate(CHANGE_WIFI_DISPLAY_STATUS);
            }
        }
    };

    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_WIFI_ENABLE_REQUEST:
                sendWifiEnableMessage();
                break;
            }
        }
    }

    private final MediaRouter.Callback mRouterCallback = new MediaRouter.SimpleCallback() {
        @Override
        public void onRouteAdded(MediaRouter router, RouteInfo info) {
            scheduleUpdate(CHANGE_ROUTES);
        }

        @Override
        public void onRouteChanged(MediaRouter router, RouteInfo info) {
            scheduleUpdate(CHANGE_ROUTES);
        }

        @Override
        public void onRouteRemoved(MediaRouter router, RouteInfo info) {
            scheduleUpdate(CHANGE_ROUTES);
        }

        @Override
        public void onRouteSelected(MediaRouter router, int type, RouteInfo info) {
            scheduleUpdate(CHANGE_ROUTES);
        }

        @Override
        public void onRouteUnselected(MediaRouter router, int type,
                RouteInfo info) {
            scheduleUpdate(CHANGE_ROUTES);
        }
    };

    private final class ItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            // TODO Auto-generated method stub
//          Intent intent;
            ListView listView = (ListView) parent;
            HashMap<String, Object> item = (HashMap<String, Object>) listView
                    .getItemAtPosition(position);
            WifiDisplay mDisplay = (WifiDisplay) item.get("dispaly");
            MediaRouter.RouteInfo mRoute = (MediaRouter.RouteInfo) item
                    .get("route");
            if (!mDisplay.isRemembered() && mDisplay.isAvailable()
                    && !mDisplay.equals(mWifiDisplayStatus.getActiveDisplay())) {
                pairWifiDisplay(mDisplay);
            }
            if (mRoute != null) {
                toggleRoute(mRoute);
            }
        }
    }
}

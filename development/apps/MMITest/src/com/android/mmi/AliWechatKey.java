/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/AliWechatKey.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/21/16| Shishun.Liu    |                    | Create for Key check       */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.os.AsyncTask;
import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;

public class AliWechatKey extends TestBase {
    private String mKeyStatus;
    private boolean bKeyOk = false;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setDefTextMessage("read...");
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {
                getKeyStatus();
                return "";
            }

            @Override
            protected void onPostExecute(String result) {
                setDefTextMessage(mKeyStatus);
                if(bKeyOk) {
                    setPassButtonEnable(true);
                }
            }
        }.execute();
    }

    private void getKeyStatus(){

        JRDClient jrdClient = new JRDClient();
        long iAliKeyStatus = 1;
        long iWechatKeyStatus = 1;
        byte[] byteStatus = jrdClient.readAliWechatKeyStatus();
        if (byteStatus.length == 8) {
            iAliKeyStatus = (byteStatus[0]);
            iAliKeyStatus += (byteStatus[1] << 8);
            iAliKeyStatus += (byteStatus[2] << 16);
            iAliKeyStatus += (byteStatus[3] << 24);
            iWechatKeyStatus = (byteStatus[4]);
            iWechatKeyStatus += (byteStatus[5] << 8);
            iWechatKeyStatus += (byteStatus[6] << 16);
            iWechatKeyStatus += (byteStatus[7] << 24);
            MMILog.d(TAG, "ali key status = "+iAliKeyStatus+", wechat key status = "+iWechatKeyStatus);
        }

        mKeyStatus = "Ali Key: "+( (iAliKeyStatus == 0x0)?"OK":"NOK" );
        mKeyStatus += "\nWechat Key: "+( (iWechatKeyStatus == 0x0)?"OK":"NOK" );
        if(iAliKeyStatus==0&&iWechatKeyStatus==0) {
            bKeyOk = true;
        }
    }
}

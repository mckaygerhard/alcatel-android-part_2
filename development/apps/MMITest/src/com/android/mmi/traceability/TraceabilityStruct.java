/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/traceability/      */
/*                                                    TraceabilityStruct.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.traceability;

import java.security.InvalidParameterException;

import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;

public class TraceabilityStruct {

    /*private int[] TracabilityInfoItems = { QcNvItemsJRD.NV_TRACABILITY_I,
            QcNvItemsJRD.NV_TRACABILITY_1_I, QcNvItemsJRD.NV_TRACABILITY_2_I,
            QcNvItemsJRD.NV_TRACABILITY_3_I };*/

    /*private FactoryInfo mTracabilityData = new FactoryInfo(512,
            TracabilityInfoItems);*/

    /*private FactoryInfo mMmitestInfo = new FactoryInfo(4,
            QcNvItemsJRD.NV_MMITEST_INFO_I);*/

    private String TAG = "TracabilityStruct";

    JRDClient mClient = new JRDClient();

    /*
     * The main components of this class are declared static so their value is
     * shared between all users One client sync cache with NV when writing
     * values
     */

    public TraceabilityStruct() throws Exception {
        // check the tables are valid
        for (int i = 0; i < ID.TRACABILITY_ITEM_TYPE_MAX.ordinal(); i++) {
            if (map[i].id.ordinal() != i) {
                throw new Exception("Invalid tracability map");
            }
        }

        /*if (mTracabilityData == null)
            mTracabilityData = new FactoryInfo(512, TracabilityInfoItems);

        if (mMmitestInfo == null)
            mMmitestInfo = new FactoryInfo(4, QcNvItemsJRD.NV_MMITEST_INFO_I);*/

    }

    public static enum ID {
        DEVICE_IMEI,
        DEVICE_BD_ADDR,
        DEVICE_WIFI_ADDR,
        MMI_TEST_RESULT,
        REF_PCBA_I,
        SHORT_CODE_I,
        ICS_I,
        SITE_FAC_PCBA_I,
        LINE_FAC_PCBA_I,
        DATE_PROD_PCBA_I,
        SN_PCBA_I,
        INDUS_REF_HANDSET_I,
        INFO_PTM_I,
        SITE_FAC_HANDSET_I,
        LINE_FAC_HANDSET_I,
        DATE_PROD_HANDSET_I,
        SN_HANDSET_I,
        INFO_PTS_MINI_I,
        INFO_NAME_MINI_I,
        INFO_TECH_MINI_I,
        INFO_GOLDEN_FLAG_I,
        INFO_GOLDEN_DATE_I,
        INFO_ID_BAIE_HDTB_I,
        INFO_DATE_PASS_HDTB_I,
        INFO_PROD_BAIE_PARA_SYS_I,
        INFO_STATUS_PARA_SYS_I,
        INFO_NBRE_PASS_PARA_SYS_I,
        INFO_DATE_PASS_PARA_SYS_I,
        INFO_PROD_BAIE_PARA_SYS_2_I,
        INFO_STATUS_PARA_SYS_2_I,
        INFO_NBRE_PASS_PARA_SYS_2_I,
        INFO_DATE_PASS_PARA_SYS_2_I,
        INFO_PROD_BAIE_PARA_SYS_3_I,
        INFO_STATUS_PARA_SYS_3_I,
        INFO_NBRE_PASS_PARA_SYS_3_I,
        INFO_DATE_PASS_PARA_SYS_3_I,
        INFO_PROD_BAIE_BW_I,
        INFO_STATUS_BW_I,
        INFO_NBRE_PASS_BW_I,
        INFO_DATE_BAIE_BW_I,
        INFO_PROD_BAIE_GPS_I,
        INFO_STATUS_GPS_I,
        INFO_NBRE_PASS_GPS_I,
        INFO_DATE_BAIE_GPS_I,
        INFO_STATUS_MMI_TEST_I,
        INFO_PROD_BAIE_FINAL_I,
        INFO_STATUS_FINAL_I,
        INFO_NBRE_PASS_FINAL_I,
        INFO_DATE_BAIE_FINAL_I,
        INFO_PROD_BAIE_FINAL_2_I,
        INFO_STATUS_FINAL_2_I,
        INFO_NBRE_PASS_FINAL_2_I,
        INFO_DATE_BAIE_FINAL_2_I,
        INFO_ID_BAIE_HDT_I,
        INFO_DATE_PASS_HDT_I,
        INFO_COMM_REF_I,
        INFO_PTS_APPLI_I,
        INFO_NAME_APPLI_I,
        INFO_NAME_PERSO1_I,
        INFO_NAME_PERSO2_I,
        INFO_NAME_PERSO3_I,
        INFO_NAME_PERSO4_I,
        INFO_PROD_BAIE_PARA_SYS_4,
        INFO_STATUS_PARA_SYS_4,
        INFO_NBRE_PASS_PARA_SYS_4,
        INFO_DATE_PASS_PARA_SYS_4,
        SKT_SERIAL_NUMBER,
        SKT_STATUS,
        INFO_SPARE_REGION,
        INFO_TEMP_CALI_VALUE, //MODIFIED by Liu Shishun, 2016-03-30,BUG-1868091
        PSENSOR,
        CURRENT,
        SMART_PA,
        F0,
        INFO_SSID,
        INFO_PASSWORD,
        INFO_TABLET_SN,
        PPE_FLAG,
        KPH_FLAG,
        INFO_SPEAKER,
        INFO_SPARE_REGION_I,
        TRACABILITY_ITEM_TYPE_MAX
    };

    static class map_item_type {
        ID id;
        int size;
        String name;

        map_item_type(ID i, int s) {
            id = i;
            size = s;
        }
    }

    static map_item_type[] map = {
            new map_item_type(ID.DEVICE_IMEI, 15),
            new map_item_type(ID.DEVICE_BD_ADDR, 6),
            new map_item_type(ID.DEVICE_WIFI_ADDR, 6),
            new map_item_type(ID.MMI_TEST_RESULT, 4),
            /* PCBA Info */
            new map_item_type(ID.REF_PCBA_I, 12),
            new map_item_type(ID.SHORT_CODE_I, 4),
            new map_item_type(ID.ICS_I, 2),
            new map_item_type(ID.SITE_FAC_PCBA_I, 1),
            new map_item_type(ID.LINE_FAC_PCBA_I, 1),
            new map_item_type(ID.DATE_PROD_PCBA_I, 3),
            new map_item_type(ID.SN_PCBA_I, 4),
            /* Handset Info */
            new map_item_type(ID.INDUS_REF_HANDSET_I, 12),
            new map_item_type(ID.INFO_PTM_I, 2),
            new map_item_type(ID.SITE_FAC_HANDSET_I, 1),
            new map_item_type(ID.LINE_FAC_HANDSET_I, 1),
            new map_item_type(ID.DATE_PROD_HANDSET_I, 3),
            new map_item_type(ID.SN_HANDSET_I, 4),
            /* Mini Info */
            new map_item_type(ID.INFO_PTS_MINI_I, 3),
            new map_item_type(ID.INFO_NAME_MINI_I, 20),
            new map_item_type(ID.INFO_TECH_MINI_I, 20),
            /* Golden Sample */
            new map_item_type(ID.INFO_GOLDEN_FLAG_I, 1),
            new map_item_type(ID.INFO_GOLDEN_DATE_I, 3),
            /* HDTB (Rework) */
            new map_item_type(ID.INFO_ID_BAIE_HDTB_I, 3),
            new map_item_type(ID.INFO_DATE_PASS_HDTB_I, 3),
            /* PT1 Test */
            new map_item_type(ID.INFO_PROD_BAIE_PARA_SYS_I, 3),
            new map_item_type(ID.INFO_STATUS_PARA_SYS_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_PARA_SYS_I, 1),
            new map_item_type(ID.INFO_DATE_PASS_PARA_SYS_I, 3),
            /* PT2 Test */
            new map_item_type(ID.INFO_PROD_BAIE_PARA_SYS_2_I, 3),
            new map_item_type(ID.INFO_STATUS_PARA_SYS_2_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_PARA_SYS_2_I, 1),
            new map_item_type(ID.INFO_DATE_PASS_PARA_SYS_2_I, 3),
            /* PT3 Test */
            new map_item_type(ID.INFO_PROD_BAIE_PARA_SYS_3_I, 3),
            new map_item_type(ID.INFO_STATUS_PARA_SYS_3_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_PARA_SYS_3_I, 1),
            new map_item_type(ID.INFO_DATE_PASS_PARA_SYS_3_I, 3),
            /* Bluetooth/WI-FI Test */
            new map_item_type(ID.INFO_PROD_BAIE_BW_I, 3),
            new map_item_type(ID.INFO_STATUS_BW_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_BW_I, 1),
            new map_item_type(ID.INFO_DATE_BAIE_BW_I, 3),
            /* GPS Test */
            new map_item_type(ID.INFO_PROD_BAIE_GPS_I, 3),
            new map_item_type(ID.INFO_STATUS_GPS_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_GPS_I, 1),
            new map_item_type(ID.INFO_DATE_BAIE_GPS_I, 3),
            /* MMI Test */
            new map_item_type(ID.INFO_STATUS_MMI_TEST_I, 1),
            /* Final Test (2G Antenna Test) */
            new map_item_type(ID.INFO_PROD_BAIE_FINAL_I, 3),
            new map_item_type(ID.INFO_STATUS_FINAL_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_FINAL_I, 1),
            new map_item_type(ID.INFO_DATE_BAIE_FINAL_I, 3),
            /* Final Test 2 (3G Antenna Test) */
            new map_item_type(ID.INFO_PROD_BAIE_FINAL_2_I, 3),
            new map_item_type(ID.INFO_STATUS_FINAL_2_I, 1),
            new map_item_type(ID.INFO_NBRE_PASS_FINAL_2_I, 1),
            new map_item_type(ID.INFO_DATE_BAIE_FINAL_2_I, 3),
            /* HDT (CU perso downloading) , */
            new map_item_type(ID.INFO_ID_BAIE_HDT_I, 3),
            new map_item_type(ID.INFO_DATE_PASS_HDT_I, 3),
            /* CU SW info , */
            new map_item_type(ID.INFO_COMM_REF_I, 20),
            new map_item_type(ID.INFO_PTS_APPLI_I, 3),
            new map_item_type(ID.INFO_NAME_APPLI_I, 20),
            new map_item_type(ID.INFO_NAME_PERSO1_I, 20),
            new map_item_type(ID.INFO_NAME_PERSO2_I, 20),
            new map_item_type(ID.INFO_NAME_PERSO3_I, 20),
            new map_item_type(ID.INFO_NAME_PERSO4_I, 20),
            /* PT4 , */
            new map_item_type(ID.INFO_PROD_BAIE_PARA_SYS_4, 3),
            new map_item_type(ID.INFO_STATUS_PARA_SYS_4, 1),
            new map_item_type(ID.INFO_NBRE_PASS_PARA_SYS_4, 1),
            new map_item_type(ID.INFO_DATE_PASS_PARA_SYS_4, 3),
            /* SKT PARE , */
            new map_item_type(ID.SKT_SERIAL_NUMBER, 7),
            new map_item_type(ID.SKT_STATUS, 1),
             /*MODIFIED-BEGIN by Liu Shishun, 2016-03-30,BUG-1868091*/
            new map_item_type(ID.INFO_SPARE_REGION, 64),
            /* temp for g-sensor cali value*/
            new map_item_type(ID.INFO_TEMP_CALI_VALUE, 12),
             /*MODIFIED-END by Liu Shishun,BUG-1868091*/
            /* P Sensor Calibration Param , */
            new map_item_type(ID.PSENSOR, 18),
            /* Charge Current , */
            new map_item_type(ID.CURRENT, 6),
            /* Smart PA , */
            new map_item_type(ID.SMART_PA, 8),
            /* F0 , */
            new map_item_type(ID.F0, 8),
            /* wifi ssid , */
            new map_item_type(ID.INFO_SSID, 32),
            /* password , */
            new map_item_type(ID.INFO_PASSWORD, 15),
            /* tablet SN , */
            new map_item_type(ID.INFO_TABLET_SN, 15),
            /* PPE flag , */
            new map_item_type(ID.PPE_FLAG, 1),
            /* KPH flag , */
            new map_item_type(ID.KPH_FLAG, 1),
            /* speaker info , */
            new map_item_type(ID.INFO_SPEAKER, 4),
            new map_item_type(ID.INFO_SPARE_REGION_I, 23) };

    /*
     * getItem
     *
     * reads an item from a tracability table returns the item datas
     */

    public byte[] getItem(ID id) {
        byte[] result = new byte[map[id.ordinal()].size];
        int offset = 0;

        mClient.readTraceability();

        // get the offset of the desired item
        for (int i = 0; i < map.length; i++) {
            if (map[i].id.equals(id)) {
                break;
            } else {
                offset += map[i].size;
            }
        }
        // extract the item datas from the tracability buffer
        try {
            System.arraycopy(JRDClient.deviceinfo_data, offset, result, 0,
                    result.length);
        } catch (Exception e) {
            MMILog.d(TAG, "Can't read Tracability item " + id.toString());
        }
        return result;
    }

    /*
     * putItem
     *
     * puts an item to a tracability table
     */

     /*MODIFIED-BEGIN by Liu Shishun, 2016-03-30,BUG-1868091*/
    public boolean putItem(ID id, byte[] data) {
        int offset = 0;

        if( mClient.forceSyncTraceability() == false ) {
            MMILog.e(TAG, "Can't syncronize traceability");
            return false;
             /*MODIFIED-END by Liu Shishun,BUG-1868091*/
        }
        // get the offset of the desired item
        for (int i = 0; i < map.length; i++) {
            if (map[i].id.equals(id)) {
                break;
            } else {
                offset += map[i].size;
            }
        }

        if (data.length != map[id.ordinal()].size) {
            throw new InvalidParameterException("Item size is wrong");
        }

        return mClient.writeTraceability(offset, data); //MODIFIED by Liu Shishun, 2016-03-30,BUG-1868091

    }

    /*public int getMmitestInfo() {
        mMmitestInfo.read();

        int result = ((int) mMmitestInfo.data[0] << 0) & 0x000000FF
                | ((int) mMmitestInfo.data[1] << 8) & 0x0000FF00
                | ((int) mMmitestInfo.data[2] << 16) & 0x00FF0000
                | ((int) mMmitestInfo.data[3] << 24) & 0xFF000000;

        return result;
    }*/

    /*public void setMmitestInfo(int mStatusBits) {

        byte[] data = new byte[4];

        data[0] = (byte) (mStatusBits >> 0);
        data[1] = (byte) (mStatusBits >> 8);
        data[2] = (byte) (mStatusBits >> 16);
        data[3] = (byte) (mStatusBits >> 24);

        mMmitestInfo.write(data, 0);
    }*/

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < map.length; i++) {
            result += map[i].id.toString();
            result += "\n";
        }

        return result;
    }

}

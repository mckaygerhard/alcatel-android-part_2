/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/MiniTestMenu.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.mmi.util.TestItemManager;

public class MiniTestMenu extends TestBase {
    private ListView mListView;

    @Override
    public void create(CommonActivity a) {
        super.create(a);

        setContentView(R.layout.manu, LAYOUTTYPE.LAYOUT_CUST_WITHOUT_PASSFAIL);
        mListView = (ListView) findViewById(R.id.manu_listView);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        int citType;
        List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
        for (int position = 0; position < TestItemManager.getTestItemCount(); position++) {
            if (TestItemManager.IsCIT()) {
                citType = TestItemManager.GetCITCustomizationInfoInt(
                        TestItemManager
                                .GetTestItemInfoString(position, "title"),
                        "deftype");
                if (citType < 0) {
                    if ((TestItemManager
                            .GetTestItemInfoInt(position, "deftype") & TestItemManager.DEF_TYPE_MANU) == TestItemManager.DEF_TYPE_MANU) {
                        data.add(TestItemManager.getTestItem(position));
                    }
                } else if ((citType & TestItemManager.DEF_TYPE_MANU) == TestItemManager.DEF_TYPE_MANU) {
                    data.add(TestItemManager.getTestItem(position));
                }
            } else {
                if ((TestItemManager.GetTestItemInfoInt(position, "deftype") & TestItemManager.DEF_TYPE_MANU) == TestItemManager.DEF_TYPE_MANU) {
                    data.add(TestItemManager.getTestItem(position));
                }
            }
        }

        SimpleAdapter simpleAdapter = null;
        simpleAdapter = new SimpleAdapter(mContext, data,
                R.layout.manu_item_common, new String[] { "title" },
                new int[] { R.id.manu_text });

        mListView.setAdapter(simpleAdapter);
        mListView.setOnItemClickListener(new ItemClickListener());
    }

    @Override
    public void onBackPressed() {
        mContext.finish();
    }

    private final class ItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            // TODO Auto-generated method stub
            Intent intent;
            ListView listView = (ListView) parent;
            HashMap<String, Object> item = (HashMap<String, Object>) listView
                    .getItemAtPosition(position);
            TestItemManager.setCurrentItem((String) item.get("defclass"),
                    (String) item.get("title"));
            intent = new Intent(mContext, CommonActivity.class);
            intent.putExtra(Value.USER_SELECT, Value.USER_SELECT_MODE_MANU);
            mContext.startActivity(intent);
        }
    }

}

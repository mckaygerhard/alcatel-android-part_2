/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/UsbCable.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.FileOutputStream;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.SysClassManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Message;

public class USBCable extends TestBase {
    public final static int MESSAGE_USERDEFINE = 1024;
//    public final static int MESSAGE_TIMEOUT = MESSAGE_USERDEFINE + 1;
//    public final static int MESSAGE_TIMECOUNT = MESSAGE_TIMEOUT + 1;
//    public final static int LENGTH_TIMEOUT = 15000;
//    public final static int LENGTH_SECOND = 1000;
//    public final static int LENGTH_SECOND_TIMECOUNT = LENGTH_TIMEOUT
//            / LENGTH_SECOND;

    private final PlugStateReceiever USBRcv = new PlugStateReceiever();;
    private IntentFilter inf;
    private Handler handler;
//    private int timeCount;
    private String ins;
    private boolean mRegistered = false;
    private boolean mUSBConnected = false;
//    private boolean mACChargerConnected = false;
    private boolean mUSBCharging = false;
    private boolean IsChargingOK = false;
//    private boolean IsReCheck = false;
    private boolean mIsCurrentOk = false;

    private SysClassManager mPlugTypeManager;
    private SysClassManager mCurrentManager;
    private SysClassManager mCurrentWriteManager;
    private SysClassManager mChargeStatusManager;
    private Handler mObjHandler = new Handler();
    private Handler mHandler = new Handler();
    private Handler ReCheckHandler = new Handler();

    private boolean saved = false;
    private TraceabilityStruct traceabilityStruct;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mPlugTypeManager = new SysClassManager();
        mCurrentManager = new SysClassManager();
        mCurrentWriteManager = new SysClassManager();
        mChargeStatusManager = new SysClassManager();

        try {
            traceabilityStruct = new TraceabilityStruct();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        timeCount = LENGTH_SECOND_TIMECOUNT;

        inf = new IntentFilter();
        inf.addAction(Intent.ACTION_BATTERY_CHANGED);

        handler = new MemberHandler();
        mHandler.postDelayed(updateCurrent, 1000);
        startUsbPlugCheck();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mContext.registerReceiver(USBRcv, inf);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(USBRcv);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        stopUsbPlugCheck();
        mHandler.removeCallbacks(updateCurrent);
        if (mPlugTypeManager != null) {
            mPlugTypeManager = null;
        }
        if (mCurrentManager != null) {
            mCurrentManager = null;
        }
        if(mCurrentWriteManager != null){
            mCurrentWriteManager = null;
        }
        if (mChargeStatusManager != null) {
             mChargeStatusManager = null;
        }
        if(mHandler != null){
            mHandler = null;
        }
    }

    private void startUsbPlugCheck() {
//        handler.sendEmptyMessageDelayed(MESSAGE_TIMEOUT, LENGTH_TIMEOUT);
//        handler.sendEmptyMessageDelayed(MESSAGE_TIMECOUNT, LENGTH_SECOND);
    }

    private void stopUsbPlugCheck() {
//        handler.removeMessages(MESSAGE_TIMEOUT);
//        handler.removeMessages(MESSAGE_TIMECOUNT);
//        timeCount = LENGTH_SECOND_TIMECOUNT;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

//            switch (msg.what) {
//            case MESSAGE_TIMEOUT:
//                setPassButtonEnable(false);
//                setDefTextMessage(R.string.usb_fail);
//                break;
//            case MESSAGE_TIMECOUNT:
//                timeCount--;
//                handler.sendEmptyMessageDelayed(MESSAGE_TIMECOUNT,
//                        LENGTH_SECOND);
//                break;
//            }
        }
    }

    public class PlugStateReceiever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {

                int mBatteryPlug = intent.getIntExtra(
                        BatteryManager.EXTRA_PLUGGED, -1);
//                mUSBConnected = (mBatteryPlug == BatteryManager.BATTERY_PLUGGED_USB);
//                mACChargerConnected = (mBatteryPlug == BatteryManager.BATTERY_PLUGGED_AC);

                if (mBatteryPlug == BatteryManager.BATTERY_PLUGGED_USB||
                        mBatteryPlug == BatteryManager.BATTERY_PLUGGED_AC) {
                    mUSBConnected = true;
                } else {
                    mUSBConnected = false;
                }
            }

            mUSBCharging = Judge_Charge_Status();
            if(!IsChargingOK)
            {
                if(mUSBCharging){
//                    if(mUSBConnected) {
                        ins = "USB : OK\n" + "Charging : OK\n" + "Current : NOK\n";
                        stopUsbPlugCheck();
                        setDefTextMessage("Please remove USB cable\n\n" + ins);
                        mObjHandler.postDelayed(mSetResult, 0);
                        IsChargingOK = true;
//                        setPassButtonEnable(true);
//                    }
//                    else {    // AC charge
//                        ins = "USB : NOK\n" + "Charging : OK\n";
//                        setDefTextMessage("Please connect USB cable\n\n" + ins);
//                        setPassButtonEnable(false);
//                    }
                }
                else{
                    if(mUSBConnected)
                    {
                        ins = "USB : OK\n" + "Charging : NOK\n" + "Current : NOK\n";
                        setDefTextMessage("Please insert USB cable.\n\n" + ins);
//                        if(!IsReCheck)
//                        ReCheckHandler.postDelayed(ReCheckCharger,1000);
                    }
                    else{
                        ins = "USB : unknown\n" + "Charging : unknown\n" + "Current : unknown\n";
                        setDefTextMessage("Please insert USB cable.\n\n" + ins);
                    }
                    setPassButtonEnable(false);
                }
            }
            else
            {
                if(!mUSBConnected)
                {
                   if(Value.isAutoTest){
                       boolean enable = mContext.mPassButton.isEnabled();
                       if(enable){
                           onPassClick();
                       }else{
                           setDefActionMessage(" ");
                           setDefTextMessage("USB cable checked.\n\n" + ins);
                       }
                   }else{
                       /* MODIFIED-BEGIN by Liu Shishun, 2016-11-15,BUG-3469299*/
                       //setPassButtonEnable(true);
                       //setDefActionMessage("");
                       /* MODIFIED-END by Liu Shishun,BUG-3469299*/
                       setDefTextMessage("USB cable checked.\n\n" + ins);
                   }
                }
            }
        }
    }

    private boolean isPluged(){
        int usb = -1;
        if (mPlugTypeManager != null) {
            mPlugTypeManager.setFileInputStream(mPlugTypeManager
                    .fileInputOpen(SysClassManager.BATTERY_USB));
            usb = mPlugTypeManager.readFileInputStream();
            mPlugTypeManager.fileInputClose();
        }
        if (usb == 49) {
            return true;
        }
        return false;
    }

    private boolean Judge_Charge_Status(){
        String status = "";

        if (mChargeStatusManager != null) {
            mChargeStatusManager.setFileInputStream(mChargeStatusManager
                    .fileInputOpen(SysClassManager.CHARGE_STATUS));
            byte curr_sta[]= mChargeStatusManager.readFileInputByte();
            mChargeStatusManager.fileInputClose();
            if (curr_sta == null) {
                return false;
            }
            status = (new String(curr_sta)).trim();
            if(status.equals("Full")|| status.equals("Charging")) {

                return true;
            }

            if(status.equals("Discharging")){
                int currentElec = GetElec();

                if ((currentElec < -500) && (currentElec > -120000) ) {

                    return true;
                }
            }

        }
        return false;
    }

    private String getCurrent() {

        int current = GetElec();
        // if(current<0) current = -current;

        String strCurrent = new String("Current: = ");
        if (current > 1000 || current < -1000) {
            strCurrent = strCurrent + (float) current / 1000f + "mA";
        } else {
            strCurrent += current + "uA";
        }

        if (IsChargingOK /*&& !mIsTimeout*/ && !mIsCurrentOk) {
            if (current <= -2500000) {
                ins = "USB : OK\n" + "Charging : OK\n" + "Current : OK\n";
                setDefTextMessage("Please remove USB cable.\n\n" + ins);
                setPassButtonEnable(true);
                mIsCurrentOk = true;

                if(!saved){
                    saveCurrent(Math.abs(current));
                    saved = true;
                }

            } else {
                ins = "USB : OK\n" + "Charging : OK\n" + "Current : NOK\n";
                setDefTextMessage("Please insert USB cable.\n\n" + ins);
                setPassButtonEnable(false);
            }
        }

        return strCurrent;
    }

    private int GetElec()
    {
        String result = null;
        int index;
        int current = 0;
        if (mCurrentManager != null) {

            FileOutputStream fos = mCurrentWriteManager.fileOutputOpen(SysClassManager.UPDATE_CHARGE_NOW);
            if(fos != null){
                mCurrentWriteManager.setFileOutputStream(fos);
                mCurrentWriteManager.WriteFileOutputChar("1");
                mCurrentWriteManager.fileOutputClose();
            }

            mCurrentManager.setFileInputStream(mCurrentManager
                    .fileInputOpen(SysClassManager.CHARGE_NOW));
            byte currentByte[] = mCurrentManager.readFileInputByte();
            mCurrentManager.fileInputClose();

            result = new String(currentByte);

            if (currentByte != null){
                for (index = 1; index < result.length(); index++) {
                     char c = result.charAt(index);
                     if (c < '0' || c > '9') {
                         break;
                     }
                }

               result = result.substring(0, index);
               current = Integer.parseInt(result);
            }
        }
        return current;

    }

    private Runnable mSetResult = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            startUsbPlugCheck();
        }
    };
    private Runnable updateCurrent = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            /* MODIFIED-BEGIN by Liu Shishun, 2016-11-15,BUG-3469299*/
            //if(mUSBConnected /*|| mACChargerConnected*/){
                setDefActionMessage(getCurrent());
            //}
            mHandler.postDelayed(updateCurrent, 2000);
            /* MODIFIED-END by Liu Shishun,BUG-3469299*/
        }
    };

    private void saveCurrent(int current){
        byte[] result = new byte[6];
        result[0] = (byte) (current & 0xFF);
        result[1] = (byte) ((current >> 8) & 0xFF);
        result[2] = (byte) ((current >> 16) & 0xFF);
        result[3] = (byte) ((current >> 24) & 0xFF);

        if (traceabilityStruct != null) {
            traceabilityStruct.putItem(TraceabilityStruct.ID.CURRENT, result);
        }
    }
//    private Runnable ReCheckCharger = new Runnable() {
//
//        @Override
//        public void run() {
//            // TODO Auto-generated method stub
//            IsReCheck = true;
//            if((timeCount>0)&&(!IsChargingOK)&&mUSBConnected)
//            {
//                Intent mIntent = new Intent(Intent.ACTION_BATTERY_CHANGED);
//                mIntent.putExtra(BatteryManager.EXTRA_PLUGGED,
//                BatteryManager.BATTERY_PLUGGED_USB);
//                mContext.sendBroadcast(mIntent);
//                ReCheckHandler.postDelayed(ReCheckCharger,1000);
//            }
//        }
//    };
}

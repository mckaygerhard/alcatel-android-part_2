package com.android.mmi.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.android.mmi.util.MMILog;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class PSensorRaw {
    private String TAG = "MMITest.Service_PSensorRaw";

    private Handler mHandler = new Handler();
    private boolean isUpdatingRaw = false;
    private boolean isSavingRaw = false;

    private Messenger mClientMessenger;

    public PSensorRaw(Context context) {
    }

    public void setMessenger(Messenger toClientMsg) {
        mClientMessenger = toClientMsg;
    }

    public void run() {
        // TODO Auto-generated method stub
    }

    public void resume() {
        // TODO Auto-generated method stub
        mHandler.postDelayed(updateRaw, 500);
        isUpdatingRaw = false;
    }

    public void pause() {
        // TODO Auto-generated method stub
        mHandler.removeCallbacks(updateRaw);
    }

    private Runnable updateRaw = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            getRawdata(1, false);
        }
    };

    public void save() {
        sendStatus(true, "", true, false);
        isSavingRaw = true;
        getRawdata(5, true);
    }

    private void getRawdata(int times, boolean save) {
        int value = 0;
        try {
            int count = 0;

            while (true) {
                if (isUpdatingRaw) {
                    if (count < 10) {
                        MMILog.d(TAG, "sleep 1s to wait previous cmd finish");
                        count++;
                        Thread.sleep(1000);
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }

            String cmd = "sns_proximity_test -s 40 -c " + String.valueOf(times);
            isUpdatingRaw = true;
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()), 4096);
            String line = "";
            int real_times = 0;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("ID=")) {
                    int start = line.indexOf("Val=");
                    int end = line.indexOf("Quality");
                    String[] val_range = line.substring(start, end).split(",");
                    int val = Integer.valueOf(val_range[1]);
                    value += val;
                    real_times++;
                }
            }
            value /= real_times;
            MMILog.d(TAG, "value: " + value + " real_times: " + real_times);
        } catch (Exception e) {
            MMILog.e(TAG, "getRawdata fail! " + e.getMessage());
            isUpdatingRaw = false;
            sendStatus(false, "0", false, false);
            return;
        }

        isUpdatingRaw = false;

        if (save) {
            sendStatus(true, String.valueOf(value), false, true);
        } else {
            if (!isSavingRaw) {
                mHandler.postDelayed(updateRaw, 500);
                sendStatus(true, String.valueOf(value), false, false);
            }
        }
    }

    private void sendStatus(boolean psOK, String value, boolean saving, boolean saved) {
        if (mClientMessenger != null) {

            Message toClientMsg = new Message();
            Bundle bundle = new Bundle();

            if (!psOK) {
                bundle.putBoolean("psOK", false);
            } else {
                if (!"0".equals(value)) {
                    if (saving) {
                        bundle.putBoolean("ispass", false);
                    } else if (saved) {
                        bundle.putBoolean("ispass", true);
                        bundle.putString("save_ps_raw", value);
                    } else {
                        bundle.putBoolean("ispass", true);
                        bundle.putString("update_ps_raw", value);
                    }
                }
            }

            toClientMsg.setData(bundle);

            try {
                MMILog.d(TAG, "service begin send msg to client");
                mClientMessenger.send(toClientMsg);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
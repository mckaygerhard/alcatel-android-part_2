package com.android.mmi.service;

import java.io.FileOutputStream;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings;
import com.android.mmi.aidl.*;

public class mmiService extends Service {
    // import com.android.mmi.aidl.*;
    private static final String TAG = "MMITest.mmiService";

    private static final String TCT_MMITEST = "tct_mmitest";
    private static final String CONFIRMED = "confirmed";

    private Messenger mServiceMessenger;

    private LcdBrightness mLcdBrightness;
    private LcdBacklight mLcdBacklight;
    private Rawdata mRawdata;
    private Fm mFm;
    private NfcSim mNfcSim;
    private MicroSD mMicroSD;
    private USBTypeC mUSBTypeC;
    private PSensorRaw mPSensorRaw;
//    private SmartPATest mSmartPATest;
//    private SpeakBOXTest mSpeakBOXTest;
    private NfcAdapter mNfcAdapter;
    private AudioManager audioManager;
    private SysClassManager mCurrentWriteManager;
//    private SysClassManager mUsgDcPriorityManager;
//    private SensorI2C mSensorI2C;

    private ContentResolver mContentResolver = null;
    private int tctMMITest = 0;
    private boolean mSysProperity_MMITestPower;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mContentResolver = getContentResolver();
        mServiceMessenger = new Messenger(new serviceHandler());

        mLcdBrightness = new LcdBrightness(this);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        MMILog.d(TAG, "onBind");
        return new mmiAidl.Stub() {
            @Override
            public IBinder getMessenger() throws RemoteException {
                // TODO Auto-generated method stub
                IBinder mIBinder = mServiceMessenger.getBinder();
                MMILog.d(TAG, "mIBinder: " + mIBinder);
                return mIBinder;
            }
        };
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        MMILog.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    private class serviceHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            ProcessAutoMMIKey(msg);
            super.handleMessage(msg);
        }
    };

    private void ProcessAutoMMIKey(Message msg) {

        MMILog.i(TAG, "ProcessAutoMMIKey, keycode: " + msg.what);

        Messenger clientMsg = msg.replyTo;
        if (clientMsg == null) {
            MMILog.i(TAG, "clientMsg is null!");
            return;
        }

        Bundle bundle = msg.getData();

        switch (msg.what) {
        case ServiceKey.KEY_CODE_PROPERTIES:// properties
            setProperties(bundle);
            break;
        case ServiceKey.KEY_CODE_SET_LCD_BRIGHTNESS:// lcd brightness
            setLcdBrightness(bundle);
            break;
        case ServiceKey.KEY_CODE_RESET_LCD_BRIGHTNESS:
            resetLcdBrightness(bundle);
            break;
        case ServiceKey.KEY_CODE_GPS:// gps
            setGPS(bundle);
            break;
        case ServiceKey.KEY_CODE_NFC:// nfc
            setNfc(bundle);
            break;
        case ServiceKey.KEY_CODE_NFC_SIM:// nfc sim
            getNfcSim(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_SENSOR_I2C:// sensor i2c
            getSensorI2C(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_RCV_SWAP:// receiver swap
            setRcvSwp(bundle);
            break;
        case ServiceKey.KEY_WIRELESS_CHARGE:// wireless charge
            setWireCharge(bundle);
            break;
        case ServiceKey.KEY_CODE_WRITE_CHARGE_NODE:// charge current update
            setChargeNode(bundle);
            break;
        case ServiceKey.KEY_CODE_AUDIO_MODE:// audio mode
            setAudioMode(bundle);
            break;
        case ServiceKey.KEY_CODE_MICRO_SD:// micro sd
            getMicroSD(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_TYPEC:// type c
            getTypeC(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_PS_RAW:// psensor raw
            getPsensorRaw(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_IMMERSIVE_MODE_CONFIRMATIONS:// NavigationBar
                                                                // config
            setImmersive();
            break;
        case ServiceKey.KEY_CODE_TP_RAWDATA:// TP rawdata
            getTpRaw(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_LCD_BACKLIGHT:// lcd backlight
            setLcdBacklight(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_FM:// Fm
            setFM(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_SMARTPA:// smartpa
            smartPA(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_SPEAKBOX:// speakbox
            speakBOX(bundle, clientMsg);
            break;
        case ServiceKey.KEY_CODE_SHUT_DOWN:// shut down
            shutDown();
            break;
        default:
            MMILog.d(TAG, "mmi service can't handle this key");
            break;
        }
    }

    private void setProperties(Bundle bundle) {
        MMILog.i(TAG, "=========setProperties===========");
        String status = bundle.getString("atuommi_status", null);
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client status: " + status);
        MMILog.i(TAG, "from client method: " + method);

        if ("setMMITest_true".equals(method)) {
            if ("run".equals(status)) {
                tctMMITest = Settings.Global.getInt(mContentResolver, TCT_MMITEST, 0);
                return;
            }
            if (tctMMITest == 0) {
                Settings.Global.putInt(mContentResolver, TCT_MMITEST, 1);
            }
        } else if ("setMMITestPower_true".equals(method)) {
            if ("run".equals(status)) {
                mSysProperity_MMITestPower = SystemProperties.getBoolean("dev.tct.MMITestPower", false);
                return;
            }
            if (mSysProperity_MMITestPower == false) {
                SystemProperties.set("dev.tct.MMITestPower", "true");
            }
        } else if ("resetMMITest".equals(method)) {
            if (tctMMITest == 0) {
                Settings.Global.putInt(mContentResolver, TCT_MMITEST, 0);
            }
        } else if ("resetMMITestPower".equals(method)) {
            if (mSysProperity_MMITestPower == false) {
                SystemProperties.set("dev.tct.MMITestPower", "false");
            }
        } else if ("enterPDAFmode".equals(method)) {
            SystemProperties.set("persist.camera.stats.test", "5");
            SystemProperties.set("persist.camera.stats.disablehaf", "8");
        } else if ("exitPDAFmode".equals(method)) {
            SystemProperties.set("persist.camera.stats.disablehaf", "0");
        } else {
            MMILog.e(TAG, "no allow this method to handle system properties!");
        }
    }

    private void setLcdBrightness(Bundle bundle) {
        MMILog.i(TAG, "=========setLcdBrightness===========");
        int brightness = bundle.getInt("atuommi_lcd_brightness", -1);
        if (brightness != -1) {
            mLcdBrightness.setLcdBrightness(brightness);
        }
    }

    private void resetLcdBrightness(Bundle bundle) {
        MMILog.i(TAG, "=========resetLcdBrightness===========");
        mLcdBrightness.resetLcdBrightness();
    }

    private void setGPS(Bundle bundle) {
        MMILog.i(TAG, "=========setGPS===========");
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client method: " + method);

        if ("on".equals(method)) {
            Settings.Secure.setLocationProviderEnabled(mContentResolver, LocationManager.GPS_PROVIDER, true);
        } else if ("off".equals(method)) {
            Settings.Secure.setLocationProviderEnabled(mContentResolver, LocationManager.GPS_PROVIDER, false);
        }
    }

    private void setNfc(Bundle bundle) {
        MMILog.i(TAG, "=========setNfc===========");
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client method: " + method);

        if ("on".equals(method)) {
            mNfcAdapter.enable();
        } else if ("off".equals(method)) {
            mNfcAdapter.disable();
        }
    }

    private void getNfcSim(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========getNfcSim===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mNfcSim == null) {
            mNfcSim = new NfcSim(this);
        }

        if ("run".equals(status)) {
            mNfcSim.setMessenger(clientMsg);
            mNfcSim.run();
        } else if ("destroy".equals(status)) {
            mNfcSim.destroy();
            mNfcSim = null;
        }
    }

    private void getSensorI2C(Bundle bundle, Messenger clientMsg) {/*
        MMILog.i(TAG, "=========getSensorI2C===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mSensorI2C == null) {
            mSensorI2C = new SensorI2C(this);
        }

        if ("run".equals(status)) {
            mSensorI2C.setMessenger(clientMsg);
            mSensorI2C.run();
        }
    */}

    private void setRcvSwp(Bundle bundle) {
        MMILog.i(TAG, "=========setRcvSwp===========");
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client method: " + method);

        if ("on".equals(method)) {
            new Thread(new Runnable() {
                public void run() {
                    AudioSystem.setParameters("ftm_rcv_swap=on");
                    MMILog.d(TAG, "ftm_rcv_swap=on");
                }
            }).start();
        } else if ("off".equals(method)) {
            new Thread(new Runnable() {
                public void run() {
                    AudioSystem.setParameters("ftm_rcv_swap=off");
                    MMILog.d(TAG, "ftm_rcv_swap=off");
                }
            }).start();
        }
    }

    private void setWireCharge(Bundle bundle) {/*
        MMILog.i(TAG, "=========setWireCharge===========");
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client method: " + method);

        if (mUsgDcPriorityManager == null) {
            mUsgDcPriorityManager = new SysClassManager();
        }
        if ("on".equals(method)) {
            FileOutputStream fos = mUsgDcPriorityManager.fileOutputOpen(SysClassManager.USB_DC_PRIORITY);
            if (fos != null) {
                mUsgDcPriorityManager.setFileOutputStream(fos);
                mUsgDcPriorityManager.WriteFileOutputChar("1");
                mUsgDcPriorityManager.fileOutputClose();
            }
        } else if ("off".equals(method)) {
            FileOutputStream fos = mUsgDcPriorityManager.fileOutputOpen(SysClassManager.USB_DC_PRIORITY);
            if (fos != null) {
                mUsgDcPriorityManager.setFileOutputStream(fos);
                mUsgDcPriorityManager.WriteFileOutputChar("0");
                mUsgDcPriorityManager.fileOutputClose();
            }
        }
    */}

    private void setChargeNode(Bundle bundle) {
        MMILog.i(TAG, "=========setChargeNode===========");
        if (mCurrentWriteManager == null) {
            mCurrentWriteManager = new SysClassManager();
        }
        FileOutputStream fos = mCurrentWriteManager.fileOutputOpen(SysClassManager.UPDATE_CHARGE_NOW);
        if (fos != null) {
            mCurrentWriteManager.setFileOutputStream(fos);
            mCurrentWriteManager.WriteFileOutputChar("1");
            mCurrentWriteManager.fileOutputClose();
        }
    }

    private void setAudioMode(Bundle bundle) {
        MMILog.i(TAG, "=========setAudioMode===========");
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client method: " + method);

        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        if ("in_call".equals(method)) {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
        } else if ("normal".equals(method)) {
            audioManager.setMode(AudioManager.MODE_NORMAL);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume - 2, 0);
        }
        MMILog.i(TAG, "audio mode: " + audioManager.getMode());
    }

    private void getMicroSD(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========getMicroSD===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mMicroSD == null) {
            mMicroSD = new MicroSD(this);
        }

        if ("run".equals(status)) {
            mMicroSD.setMessenger(clientMsg);
            mMicroSD.run();
        } else if ("destroy".equals(status)) {
            mMicroSD.destroy();
            mMicroSD = null;
        }
    }

    private void getTypeC(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========USBTypeC===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mUSBTypeC == null) {
            mUSBTypeC = new USBTypeC(this);
        }

        if ("run".equals(status)) {
            mUSBTypeC.setMessenger(clientMsg);
            mUSBTypeC.run();
        } else if ("destroy".equals(status)) {
            mUSBTypeC.destroy();
            mUSBTypeC = null;
        }
    }

    private void getPsensorRaw(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========getPsensorRaw===========");
        String status = bundle.getString("atuommi_status", null);
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client status: " + status);
        MMILog.i(TAG, "from client method: " + method);

        if (mPSensorRaw == null) {
            mPSensorRaw = new PSensorRaw(this);
        }

            if ("run".equals(status)) {
                mPSensorRaw.setMessenger(clientMsg);
                mPSensorRaw.run();
            } else if ("pause".equals(status)) {
                mPSensorRaw.pause();
            } else if ("resume".equals(status)) {
                mPSensorRaw.resume();
            } else if ("destroy".equals(status)) {
                mPSensorRaw = null;
            }

        if ("save".equals(method)) {
            mPSensorRaw.save();
        }
    }

    private void setImmersive() {
        MMILog.i(TAG, "=========setImmersive===========");
        String value = Settings.Secure.getStringForUser(mContentResolver, Settings.Secure.IMMERSIVE_MODE_CONFIRMATIONS,
                UserHandle.USER_CURRENT);
        if (!CONFIRMED.equals(value)) {
            Settings.Secure.putStringForUser(mContentResolver, Settings.Secure.IMMERSIVE_MODE_CONFIRMATIONS, CONFIRMED,
                    UserHandle.USER_CURRENT);
        }
    }

    private void getTpRaw(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========getTpRaw===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mRawdata == null) {
            mRawdata = new Rawdata(this);
        }
        if ("run".equals(status)) {
            mRawdata.setMessenger(clientMsg);
            mRawdata.run();
        } else if ("destroy".equals(status)) {
            mRawdata = null;
        }
    }

    private void setLcdBacklight(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========setLcdBacklight===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mLcdBacklight == null) {
            mLcdBacklight = new LcdBacklight(this);
        }
        if ("run".equals(status)) {
            mLcdBacklight.setMessenger(clientMsg);
            mLcdBacklight.run();
        } else if ("pause".equals(status)) {
            mLcdBacklight.pause();
        } else if ("resume".equals(status)) {
            mLcdBacklight.resume();
        } else if ("destroy".equals(status)) {
            mLcdBacklight = null;
        }
    }

    private void setFM(Bundle bundle, Messenger clientMsg) {
        MMILog.i(TAG, "=========setFM===========");
        String status = bundle.getString("atuommi_status", null);
        String method = bundle.getString("atuommi_method", null);
        MMILog.i(TAG, "from client status: " + status);
        MMILog.i(TAG, "from client method: " + method);

        if (mFm == null) {
            mFm = new Fm(this);
        }
        if ("run".equals(status)) {
            mFm.setMessenger(clientMsg);
            mFm.run();
        } else if ("destroy".equals(status)) {
            mFm.setFmOff();
            mFm = null;
        }

            int frequency = bundle.getInt("atuommi_fm_frequency");

        if ("on".equals(method)) {
            mFm.initFm(frequency);
        } else if ("off".equals(method)) {
            mFm.setFmOff();
        } else if ("set_frequency".equals(method)) {
            mFm.setFrequency(frequency);
        }
    }

    private void smartPA(Bundle bundle, Messenger clientMsg) {/*
        MMILog.i(TAG, "=========smartPA===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mSmartPATest == null) {
            mSmartPATest = new SmartPATest();
        }

        if ("run".equals(status)) {
            mSmartPATest.setMessenger(clientMsg);
            mSmartPATest.run();
        }
    */}

    private void speakBOX(Bundle bundle, Messenger clientMsg) {/*
        MMILog.i(TAG, "=========speakBOX===========");
        String status = bundle.getString("atuommi_status", null);
        MMILog.i(TAG, "from client status: " + status);

        if (mSpeakBOXTest == null) {
            mSpeakBOXTest = new SpeakBOXTest();
        }

        if ("run".equals(status)) {
            mSpeakBOXTest.setMessenger(clientMsg);
            mSpeakBOXTest.run();
        }
    */}

    private void shutDown() {
        MMILog.i(TAG, "=========shutDown===========");
        Intent shutdown = new Intent(Intent.ACTION_REQUEST_SHUTDOWN);
        shutdown.putExtra(Intent.EXTRA_KEY_CONFIRM, false);
        shutdown.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(shutdown);
    }

}

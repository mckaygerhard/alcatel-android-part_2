package com.android.mmi.service;

import java.io.IOException;

import com.android.mmi.util.MMILog;
import com.nxp.nfc.NxpNfcAdapter;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class NfcSim {
    private String TAG = "MMITest.Service_NfcSim";

    private Messenger mClientMessenger;
    private boolean mNFCEnable;
    private NfcAdapter mNfcAdapter = null;
    private AsyncTask asynTask = null;
    private Context mContext;

    public NfcSim(Context context) {
        mContext = context;
        mNFCEnable = isNFCEnable();
    }

    public void setMessenger(Messenger toClientMsg) {
        mClientMessenger = toClientMsg;
    }

    public void run() {
        // TODO Auto-generated method stub
        getNfcSim();
    }

    public void destroy() {
        if (mNFCEnable) {
            if (!isNFCEnable()) {
                enableNFC(true);
            }
        }
        if (asynTask != null) {
            asynTask.cancel(true);
            asynTask = null;
        }
    }

    private boolean isNFCEnable() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        if (mNfcAdapter != null) {
            return mNfcAdapter.isEnabled();
        }
        return false;
    }

    private void enableNFC(boolean enable) {
        if (mNfcAdapter != null) {
            if (enable) {
                MMILog.d(TAG, "enable NFC Adatper");
                mNfcAdapter.enable();
            } else {
                MMILog.d(TAG, "disable NFC Adatper");
                mNfcAdapter.disable();
            }
        }
    }

    private void getNfcSim() {
        try {
            asynTask = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... unused) {
                    int count = 0;

                    if (mNFCEnable) {
                        enableNFC(false);
                    }

                    while (isNFCEnable()) {
                        if (count < 10) {
                            MMILog.d(TAG, "NFC adapter is enable, sleep(500)");
                            count += 1;
                            android.os.SystemClock.sleep(500);
                        } else {
                            MMILog.d(TAG, "NFC adapter is enable, getNfcSim return -1");
                            return "FAIL";
                        }
                    }

                    NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
                    NxpNfcAdapter mNxpNfcAdapter = NxpNfcAdapter.getNxpNfcAdapter(mNfcAdapter);
                    int result = -1;
                    try {
                        MMILog.d(TAG, "testNfcSwp() start");
                        result = mNxpNfcAdapter.testNfcSwp();
                        MMILog.d(TAG, "testNfcSwp() end");
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        MMILog.d(TAG, "testNfcSwp() error: " + e.getMessage());
                    }
                    MMILog.d(TAG, "testNfcSwp(), result: " + result);

                    if (result == 0) {
                        return "PASS";
                    } else {
                        return "FAIL";
                    }
                }

                @Override
                protected void onPostExecute(String result) {
                    if (!isCancelled()) {
                        sendStatus(result);
                    }
                }
            }.execute();
        } catch (Exception e) {
            sendStatus("FAIL");
        }
    }

    private void sendStatus(String nfc_sim) {
        if (mClientMessenger != null) {

            Message toClientMsg = new Message();
            Bundle bundle = new Bundle();

            bundle.putString("nfc_sim", nfc_sim);

            toClientMsg.setData(bundle);

            try {
                MMILog.d(TAG, "service begin send msg to client");
                mClientMessenger.send(toClientMsg);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}

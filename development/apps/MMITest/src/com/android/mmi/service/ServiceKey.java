package com.android.mmi.service;

public class ServiceKey {
    /**
     * define adb key form auto mmi
     *
     * intent key: atuommi_status atuommi_method
     */

    // ============status like: run, resume, pause, destroy======

    // no method
    public static final int KEY_CODE_TP_RAWDATA = 55;
    public static final int KEY_CODE_LCD_BACKLIGHT = 2000;
    public static final int KEY_CODE_PS_RAW = 1508;
    public static final int KEY_CODE_MICRO_SD = 1510;
    public static final int KEY_CODE_TYPEC = 1519;
    public static final int KEY_CODE_NFC_SIM = 1512;
    public static final int KEY_CODE_SENSOR_I2C = 1517;
    // method: on, off, set_frequency
    public static final int KEY_CODE_FM = 1505;
    // method: setMMITest_true, setMMITestPower_true,
    // resetMMITest, resetMMITestPower, enterPDAFmode, exitPDAFmode
    public static final int KEY_CODE_PROPERTIES = 1500;


    // =============status: no status====================

    public static final int KEY_CODE_SHUT_DOWN = 1509;
    public static final int KEY_CODE_IMMERSIVE_MODE_CONFIRMATIONS = 1513;
    public static final int KEY_CODE_WRITE_CHARGE_NODE = 1514;
    public static final int KEY_CODE_SMARTPA = 1515;
    public static final int KEY_CODE_SPEAKBOX = 1516;
    // method: on off
    public static final int KEY_CODE_RCV_SWAP = 1507;
    public static final int KEY_CODE_NFC = 1501;
    public static final int KEY_CODE_GPS = 1502;
    public static final int KEY_WIRELESS_CHARGE = 1518;

    public static final int KEY_CODE_AUDIO_MODE = 1511;

    // get brightness value form "atuommi_lcd_brightness"
    public static final int KEY_CODE_SET_LCD_BRIGHTNESS = 9001;
    public static final int KEY_CODE_RESET_LCD_BRIGHTNESS = 9002;

}

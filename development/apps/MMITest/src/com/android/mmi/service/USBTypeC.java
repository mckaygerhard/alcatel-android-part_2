
package com.android.mmi.service;

import java.nio.charset.Charset;

import com.android.mmi.util.ExternStorageTest;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;

public class USBTypeC {
    private String TAG = "MMITest.Service_USBTypeC";

    private String mUSBPlugStatusFile = "/sys/devices/soc/7575000.i2c/i2c-1/1-0050/detect";
    private Handler mHandler = new Handler();
    private boolean mCheckTypeCInfo = false;
    private boolean mCheckPlug1 = false;
    private boolean mCheckPlug2 = false;
    private boolean mCheckPlug1_RW = false;
    private boolean mCheckPlug2_RW = false;
    private StorageManager storageManager;
    private StorageEventListener storageEventListener;
    private Thread mStorageTestThread = null;
    private String mStoragePath = "";
    private String mTypeCInfo = "";

    private Context mContext;
    private Messenger mClientMessenger;

    public USBTypeC(Context context) {
        mContext = context;
    }

    public void setMessenger(Messenger toClientMsg) {
        mClientMessenger = toClientMsg;
    }

    public void run() {
        // TODO Auto-generated method stub
        storageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        storageEventListener = new MemberStorageEventListener();
        storageManager.registerListener(storageEventListener);
    }

    public void destroy() {
        if (mStorageTestThread != null && mStorageTestThread.isAlive()) {
            mStorageTestThread.interrupt();
        }
        storageManager.unregisterListener(storageEventListener);
        mHandler.removeCallbacksAndMessages(null);
    }

    private Runnable mStorageTestRunnable = new Runnable() {
        @Override
        public void run() {

            if (mCheckTypeCInfo == false) {
                mTypeCInfo = ReadFileString(SysClassManager.FILE_USBTYPE_C);
                if (mTypeCInfo.equalsIgnoreCase("7418")) {
                    mCheckTypeCInfo = true;
                }
            }
            if (mCheckPlug1 == false || mCheckPlug2 == false) {
                boolean result = new ExternStorageTest(mStoragePath).startTest();
                String plugStatus = ReadFileString(mUSBPlugStatusFile);

                if (plugStatus.compareToIgnoreCase("f1") == 0) {
                    mCheckPlug1 = true;
                    mCheckPlug1_RW = result;
                } else if (plugStatus.compareToIgnoreCase("f4") == 0) {
                    mCheckPlug2 = true;
                    mCheckPlug2_RW = result;
                }

                sendStatus();
            }
        }
    };

    private String ReadFileString(String path) {
        SysClassManager fileManager = new SysClassManager();
        byte result[];
        String resultStri;

        fileManager.setFileInputStream(fileManager.fileInputOpen(path));

        result = fileManager.readFileInputByte();
        if (result != null) {
            fileManager.fileInputClose();
            MMILog.d(TAG, "read len=" + result.length);
            resultStri = new String(result, Charset.forName("US-ASCII"));
            resultStri = resultStri.trim();
            MMILog.d(TAG, "read string = " + resultStri);
        } else {
            resultStri = "ERROR";
            MMILog.e(TAG, "Failed to read: " + path);
        }

        return resultStri;
    }

    private class MemberStorageEventListener extends StorageEventListener {
        @Override
        public void onStorageStateChanged(String path, String oldState, String newState) {
            super.onStorageStateChanged(path, oldState, newState);
            MMILog.i(TAG, "path(" + newState + ") = " + path);
            if (isUsbStorage(path)) {
                if (Environment.MEDIA_MOUNTED.equals(newState)) {
                    mStoragePath = path;
                    // start storage test
                    try {
                        if (mStorageTestThread != null && mStorageTestThread.isAlive()) {
                            MMILog.i(TAG, "StorageTestThread is still running, return");
                            // mStorageTestThread.interrupt();
                            return;
                        }
                        mStorageTestThread = new Thread(mStorageTestRunnable);
                        mStorageTestThread.start();
                    } catch (IllegalThreadStateException e) {
                        MMILog.e(TAG, e.toString());
                    }
                } else {
                    mStoragePath = "";
                }
            }
        }
    }

    private boolean isUsbStorage(String path) {
        return !path.contains("emulated") && !path.contains("self");
    }

    private void sendStatus() {
        if (mClientMessenger != null) {

            Message toClientMsg = new Message();
            Bundle bundle = new Bundle();

            bundle.putString("mTypeCInfo", mTypeCInfo);
            bundle.putBoolean("mCheckPlug1", mCheckPlug1);
            bundle.putBoolean("mCheckPlug2", mCheckPlug2);
            bundle.putBoolean("mCheckPlug1_RW", mCheckPlug1_RW);
            bundle.putBoolean("mCheckPlug2_RW", mCheckPlug2_RW);

            toClientMsg.setData(bundle);

            try {
                MMILog.d(TAG, "service begin send msg to client");
                mClientMessenger.send(toClientMsg);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/CommonActivity.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.TestItemManager;

public class CommonActivity extends Activity implements OnClickListener {
    private TestBase mCurrentTest;
    public Button mPassButton = null;
    public Button mFailButton = null;
    public Button mAuto2Button = null;
    public TextView mTextTitle = null;
    public TextView mTextMessage = null;
    public TextView mTextAction = null;
    private String TAG = "MMITest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //StatusBarManager mStatusBarManager = (StatusBarManager)getSystemService(Context.STATUS_BAR_SERVICE);
        //mStatusBarManager.disable(StatusBarManager.DISABLE_MASK);

        TestItemManager.InitStaticInfo(this);

        // create test class
        if( !CreateProductTestInstance(TestItemManager.getProduct(),
                TestItemManager.getCurrentItemClass(),
                TestItemManager.getCurrentITemTitle()) ) {
            AlertDialog alertDlg = new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Faile to find class("+TestItemManager.getCurrentItemClass()+"), who's base class is TestBase.")
                    .setPositiveButton("Close",
                        new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                int whichButton) {
                            Intent intent = CommonActivity.this.getIntent();
                            CommonActivity.this.setResult(Value.RESULT_CODE_FAIL, intent);
                            CommonActivity.this.finish();
                        }
                    }).create();
            alertDlg.show();
            return;
        }

        mCurrentTest.create(this);
        if( mCurrentTest.mLayoutType == TestBase.LAYOUTTYPE.LAYOUT_DEF) {
            if(TestItemManager.getCurrentItemClass().equals("MainScreen")){
                this.setContentView(R.layout.main_screen);
            }else{
                this.setContentView(R.layout.common_screen);
            }
        }
         /*MODIFIED-BEGIN by Liu Shishun, 2016-04-01,BUG-1804050*/
        mTextMessage = (TextView)this.findViewById(R.id.textview_common_message);
        mTextAction = (TextView)this.findViewById(R.id.textview_common_action);
         /*MODIFIED-END by Liu Shishun,BUG-1804050*/
        if( mCurrentTest.mLayoutType == TestBase.LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL ||
                mCurrentTest.mLayoutType == TestBase.LAYOUTTYPE.LAYOUT_DEF) {
            mPassButton = (Button)this.findViewById(R.id.button_common_pass);
            mFailButton = (Button)this.findViewById(R.id.button_common_fail);
            mPassButton.setOnClickListener(this);
            mFailButton.setOnClickListener(this);
            mPassButton.setText(R.string.common_button_test_left);
            mFailButton.setText(R.string.common_button_test_right);
            mPassButton.setEnabled(false);
            mFailButton.setEnabled(true);

            if(TestItemManager.getCurrentItemClass().equals("MainScreen")){
                mAuto2Button = (Button)this.findViewById(R.id.button_auto2);
                mAuto2Button.setOnClickListener(this);
                mAuto2Button.setText(R.string.com_btn_auto2);
                mAuto2Button.setEnabled(false);
            }
        }
        mTextTitle = (TextView)this.findViewById(R.id.textview_common_title);
        if(mTextTitle!=null){
            MMILog.d(TAG, "title = " + mCurrentTest.mTitle);
            mTextTitle.setText(mCurrentTest.mTitle.toUpperCase());
        }

        mCurrentTest.run();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if( mCurrentTest!=null ){
            mCurrentTest.destroy();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if( mCurrentTest!=null ){
            mCurrentTest.resume();
        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if( mCurrentTest!=null ){
            mCurrentTest.pause();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if( mCurrentTest!=null ){
            mCurrentTest.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if( v == mPassButton ) {
            mCurrentTest.onPassClick();
        } else if(v == mAuto2Button){
            mCurrentTest.onAuto2Click();
        } else {
            mCurrentTest.onFailClick();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        mCurrentTest.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        //return super.onKeyDown(keyCode, event);
        if(mCurrentTest!=null) {
            return mCurrentTest.onKeyDown(keyCode, event);
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public boolean super_onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        //return super.onKeyDown(keyCode, event);
        if(mCurrentTest!=null) {
            return mCurrentTest.onKeyUp(keyCode, event);
        }
        else {
            return super.onKeyUp(keyCode, event);
        }
    }

    public boolean super_onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // TODO Auto-generated method stub
        //return super.onKeyDown(keyCode, event);
        if(mCurrentTest!=null) {
            return mCurrentTest.dispatchKeyEvent(event);
        }
        else {
            return super.dispatchKeyEvent(event);
        }
    }

    public boolean super_dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        if( mCurrentTest!=null ){
            mCurrentTest.onNewIntent(intent);
        }
    }

    boolean CreateProductTestInstance(String product, String className, String title) {
        if(CreateTestInstance(className+"_"+product, title)==false) {
            return CreateTestInstance(className, title);
        }
        return true;
    }

    boolean CreateTestInstance(String className, String title) {
        try {
            Class c = Class.forName("com.android.mmi."+className);
            try {
                Object ctemp = (TestBase)c.newInstance();
                if(ctemp instanceof TestBase) {
                    mCurrentTest = (TestBase)ctemp;
                    mCurrentTest.mTitle = title;
                    return true;
                }
                else {
                    MMILog.d(TAG, "class com.android.mmi."+ className+", whose base class is not TestBase");
                }
            }
            catch(Exception e){
                //MMILog.d(TAG, "failed to newInstance class com.android.mmi."+ className);
                //e.printStackTrace();
            }
        }
        catch(ClassNotFoundException e) {
            //MMILog.d(TAG, "failed to find class com.android.mmi."+ className);
            //e.printStackTrace();
        }
        catch (Error e) {
            e.printStackTrace();
        }

        return false;
    }
}

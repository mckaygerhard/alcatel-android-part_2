/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Gsensor.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.TestItemManager;

public class Gsensor extends TestBase implements SensorEventListener {
    private final int INIT_SCREEN = 0;
    private final int INIT_FAIL_SCREEN = INIT_SCREEN + 1;
    private final int TEST_SCREEN = INIT_FAIL_SCREEN + 1;
    private final int LEFT_SCREEN = TEST_SCREEN + 1;
    private final int RIGHT_SCREEN = LEFT_SCREEN + 1;
    private final int DOWN_SCREEN = RIGHT_SCREEN + 1;
    private final int BACK_SCREEN = DOWN_SCREEN + 1;
    private final int FINISH_SCREEN = BACK_SCREEN + 1;
    private int mCount = 0;

    private Sensor mOriSensor = null;
    private Sensor mAccSensor = null;
    private SensorManager mSensorManager = null;
    private TextView tv_fail = null;
    private ImageView iv_init = null;
    private ImageView iv_init_bottom = null;
    private FrameLayout fl_test_screen = null;
    private ImageView iv_faceback = null;
    private ImageView iv_left = null;
    private ImageView iv_right = null;
    private ImageView iv_down = null;
    private ImageView iv_up = null;
    //private TextView mBtnRight = null;
    private boolean isTestScreen = false;
    private boolean isTestRScreen = false;
    private boolean isTestLScreen = false;
    private boolean isTestDScreen = false;
    private boolean isTestBScreen = false;
    private boolean isTestUScreen = false;
    //private String mCurMode;
    private AnimationDrawable mAniDraw;
    private TextView mAngleXy;

    private static String mBSN = "";

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        this.setContentView(R.layout.test_gsensor, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        findView();
        setScreen(INIT_SCREEN);

        mBSN = TestItemManager.getBSN();

        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mAccSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void findView() {
        tv_fail = (TextView) findViewById(R.id.tv_gsensor_init_fail);
        iv_init = (ImageView) findViewById(R.id.iv_gsensor_init);
        iv_init_bottom = (ImageView) findViewById(R.id.iv_gsensor_init_botton);
        fl_test_screen = (FrameLayout) findViewById(R.id.fl_gsensor_test_screen);
        iv_faceback = (ImageView) findViewById(R.id.iv_gsensor_faceback);
        iv_left = (ImageView) findViewById(R.id.iv_gsensor_left);
        iv_right = (ImageView) findViewById(R.id.iv_gsensor_right);
        iv_down = (ImageView) findViewById(R.id.iv_gsensor_down);
        iv_up = (ImageView) findViewById(R.id.iv_gsensor_faceup);
        mAngleXy = (TextView) findViewById(R.id.angle_xy);
    }

    private void setScreen(int screen) {
        switch (screen) {
        case INIT_SCREEN: {
            if (fl_test_screen != null) {
                fl_test_screen.setVisibility(View.INVISIBLE);
            }
            if (tv_fail != null) {
                tv_fail.setVisibility(View.GONE);
            }
            if (iv_init != null) {
                iv_init.setVisibility(View.VISIBLE);
            }
            if (iv_init_bottom != null) {
                iv_init_bottom.setVisibility(View.VISIBLE);
            }
            break;
        }
        case INIT_FAIL_SCREEN: {
            if (fl_test_screen != null) {
                fl_test_screen.setVisibility(View.INVISIBLE);
            }
            if (iv_init != null) {
                iv_init.setVisibility(View.INVISIBLE);
            }
            if (iv_init != null) {
                iv_init_bottom.setVisibility(View.INVISIBLE);
            }
            if (tv_fail != null) {
                tv_fail.setVisibility(View.VISIBLE);
                mAngleXy.setVisibility(View.GONE);
            }
            break;
        }
        case TEST_SCREEN: {
            if (iv_init_bottom != null) {
                iv_init_bottom.setVisibility(View.GONE);
            }
            if (tv_fail != null) {
                tv_fail.setVisibility(View.GONE);
            }
            iv_init.setVisibility(View.GONE);
            if (fl_test_screen != null) {
                fl_test_screen.setVisibility(View.VISIBLE);
                // iv_init.setImageResource(R.drawable.cell_right);
                // startAnimation(iv_right, R.anim.right);
            }
            break;
        }
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mSensorManager.registerListener(this, mAccSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub
        int x = (int) event.values[0];
        int y = (int) event.values[1];
        int z = (int) event.values[2];
        MMILog.e(TAG, "isInit = " + isTestScreen);
        MMILog.e(TAG, "x = " + x);
        MMILog.e(TAG, "y = " + y);
        MMILog.e(TAG, "z = " + z);

        DDDVect mDir = new DDDVect(event.values[0], event.values[1],
                event.values[2]);

        float mAngleToYaxis = mDir.getYAngle();
        float mAngleToXaxis = mDir.getXAngle();
        float mAngleToZaxis = mDir.getZAngle();

        String info = String.format("V: %.2f", mAngleToYaxis)
                 /*MODIFIED-BEGIN by Liu Shishun, 2016-03-30,BUG-1868091*/
                + String.format(", H: %.2f", mAngleToXaxis)
                + String.format("\nx: %.2f", event.values[0])
                + String.format(" y: %.2f", event.values[1])
                + String.format(" z: %.2f", event.values[2]);
                 /*MODIFIED-END by Liu Shishun,BUG-1868091*/
        mAngleXy.setText(info);

        String msg = mBSN + "  " + info + String.format(", Z: %.2f", mAngleToZaxis);
        MMILog.i(TAG , msg);

        // stand up
        if (!isTestScreen && mAngleToYaxis < 15) {
            setScreen(TEST_SCREEN);
            isTestScreen = true;
            mCount++;
            // remove right arrow
        } else if (isTestScreen && !isTestRScreen && mAngleToXaxis > 165) {
            iv_right.setVisibility(View.INVISIBLE);
            isTestRScreen = true;
            mCount++;
            // remove down arrow
        } else if (isTestScreen && !isTestDScreen && mAngleToYaxis > 165) {
            MMILog.e(TAG, "x1 = " + x);
            iv_down.setVisibility(View.INVISIBLE);
            isTestDScreen = true;
            mCount++;
            // remove left arrow
        } else if (isTestScreen && !isTestLScreen && mAngleToXaxis < 15) {
            iv_left.setVisibility(View.INVISIBLE);
            isTestLScreen = true;
            mCount++;
        } else if (isTestScreen && !isTestBScreen && mAngleToZaxis > 165) {
            iv_faceback.setVisibility(View.INVISIBLE);
            isTestBScreen = true;
            mCount++;
        } else if (isTestScreen && isTestBScreen && !isTestUScreen
                && mAngleToZaxis < 15) {
            iv_up.setVisibility(View.INVISIBLE);
            isTestUScreen = true;
            mCount++;
        }
        if (6 == mCount) {
            onPassClick();
        }
    }

    class DDDVect {
        float Vx;
        float Vy;
        float Vz;

        DDDVect(float x, float y, float z) {
            Vx = x;
            Vy = y;
            Vz = z;
        }

        public float getYAngle() {
            return getAngle(Vy);
        }

        public float getXAngle() {
            return getAngle(Vx);
        }

        public float getZAngle() {
            return getAngle(Vz);
        }

        private float getAngle(float ref) {
            return (float) Math.toDegrees(Math.acos(ref
                    / Math.sqrt(Vx * Vx + Vy * Vy + Vz * Vz)));
        }

    }
}

/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/SmartCover.java    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;

import android.content.ContentResolver;
import android.graphics.Color;
import android.provider.Settings;
import android.view.KeyEvent;
import android.widget.TextView;

public class SmartCover extends TestBase {
    private TextView mOPEN;
    private TextView mCLOSE;
    private TextView mOPEN_HALL2;
    private TextView mCLOSE_HALL2;
    private TextView mHallStatus;

    private boolean mIsCLOSED = false;
    private boolean mIsCLOSED_HALL2 = false;
    private boolean mIsOPEN = false;
    private boolean mIsOPEN_HALL2 = false;

//    private ContentResolver mContentResolver = null;
//    private int tctMMITest = 0;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_hall, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mOPEN = (TextView) findViewById(R.id.prox_far);
        mCLOSE = (TextView) findViewById(R.id.prox_near);
        mOPEN_HALL2 = (TextView) findViewById(R.id.prox_far_hall2);
        mCLOSE_HALL2 = (TextView) findViewById(R.id.prox_near_hall2);
        mHallStatus = (TextView) findViewById(R.id.prox_status);

//        mContentResolver = mContext.getContentResolver();
//        tctMMITest = Settings.Global.getInt(mContentResolver, TCT_MMITEST, 0);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
//        if (tctMMITest == 0) {
//            Settings.Global.putInt(mContentResolver, TCT_MMITEST, 1);
//        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
//        if (tctMMITest == 0) {
//            Settings.Global.putInt(mContentResolver, TCT_MMITEST, 0);
//        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        MMILog.e(TAG, "keyCode :" + keyCode);
        switch (keyCode) {
        case 250:
            mIsOPEN = true;
            mHallStatus.setTextColor(Color.WHITE);
            mHallStatus.setText("HALL1 OPEN");
            break;
        case 251:
            mIsCLOSED = true;
            mHallStatus.setTextColor(Color.WHITE);
            mHallStatus.setText("HALL1 CLOSED");
            break;
        case 252:
            mIsOPEN_HALL2 = true;
            mHallStatus.setTextColor(Color.WHITE);
            mHallStatus.setText("HALL2 OPEN");
            break;
        case 253:
            mIsCLOSED_HALL2 = true;
            mHallStatus.setTextColor(Color.WHITE);
            mHallStatus.setText("HALL2 CLOSED");
            break;
        }

        if (mIsOPEN) {
            mCLOSE.setText("hall1 open : OK");
        } else {
            mCLOSE.setText("hall1 open : not tested");
        }

        if (mIsOPEN_HALL2) {
            mCLOSE_HALL2.setText("hall2 open : OK");
        } else {
            mCLOSE_HALL2.setText("hall2 open : not tested");
        }

        if (mIsCLOSED) {
            mOPEN.setText("hall1 close : OK");
        } else {
            mOPEN.setText("hall1 close : not tested");
        }

        if (mIsCLOSED_HALL2) {
            mOPEN_HALL2.setText("hall2 close : OK");
        } else {
            mOPEN_HALL2.setText("hall2 close : not tested");
        }

        if (mIsOPEN && mIsCLOSED && mIsOPEN_HALL2 && mIsCLOSED_HALL2) {
            setPassButtonEnable(true);
        }

        return super.dispatchKeyEvent(event);
    }
}

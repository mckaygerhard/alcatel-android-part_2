package com.android.mmi;

import java.io.FileOutputStream;
import java.io.IOException;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.widget.TextView;

public class SpeakBOXTest extends TestBase {
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer = null;
    private TextView m_TextView1;
    private TextView m_TextView2;
    private TextView m_TextView3;
    private TraceabilityStruct traceabilityStruct;
    private int LRES_value = 0;
    private int RRES_value = 0;
    private boolean isEnd = false;

    private static final String LRES = "/sys/class/ftm_spr_res/LRES";
    private static final String RRES = "/sys/class/ftm_spr_res/RRES";
    private static final String TOP_RES_MAX = "/sys/class/ftm_spr_res/top_res_max";
    private static final String TOP_RES_MIN = "/sys/class/ftm_spr_res/top_res_min";
    private static final String BTM_RES_MAX = "/sys/class/ftm_spr_res/btm_res_max";
    private static final String BTM_RES_MIN = "/sys/class/ftm_spr_res/btm_res_min";
    private static final String STATUS = "/sys/class/ftm_spr_res/status";

    @Override
    public void create(CommonActivity a) {
        super.create(a);
        setContentView(R.layout.test_smartpa,
                LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
        m_TextView1 = (TextView) this.findViewById(R.id.smartpa_message_1);
        m_TextView2 = (TextView) this.findViewById(R.id.smartpa_message_2);
        m_TextView3 = (TextView) this.findViewById(R.id.smartpa_message_3);
    }

    @Override
    public void run() {
        try {
            traceabilityStruct = new TraceabilityStruct();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setFailButtonEnable(false);
        mAudioManager = (AudioManager) mContext
                .getSystemService(Context.AUDIO_SERVICE);
        MMILog.i(TAG, "max value="+mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 12, 0);
        mMediaPlayer = new MediaPlayer();
        try {
            setDataSourceFromResource(mContext.getResources(), mMediaPlayer,
                    R.raw.pinknoise);
            startMelody();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Teststart();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        release();
    }

    @Override
    public void onBackPressed() {
        if (!isEnd) {
            return;
        }
        super.onBackPressed();
    }

    private void startMelody() throws java.io.IOException,
            IllegalArgumentException, IllegalStateException {
        mMediaPlayer.setLooping(true);
        mMediaPlayer.setVolume(1.0f, 1.0f);
        mMediaPlayer.prepare();
        mMediaPlayer.start();
    }

    private void setDataSourceFromResource(Resources resources,
            MediaPlayer player, int res) throws java.io.IOException {
        AssetFileDescriptor afd = resources.openRawResourceFd(res);
        if (afd != null) {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            afd.close();
        }
    }

    private void Teststart() {
        m_TextView3.setText("Do SpeakBOX Test,Please wait...");

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {

                long start_time = System.currentTimeMillis();
                MMILog.i(TAG, "start... " + start_time);

                SysClassManager sy = new SysClassManager();
                FileOutputStream fos = sy.fileOutputOpen(STATUS);
                if (fos != null) {
                    sy.setFileOutputStream(fos);
                    sy.WriteFileOutputChar("start");
                    sy.fileOutputClose();

                    long end_time = System.currentTimeMillis();
                    MMILog.i(TAG, "end... " + end_time);
                    MMILog.i(TAG, "total used time: " + (end_time - start_time));

                    return "PASS";
                } else {
                    MMILog.i(TAG, "output status node fail");
                    return "FAIL";
                }
            }

            @Override
            protected void onPostExecute(String result) {
                if (result.equals("PASS")) {
                    show();
                    saveResult(LRES_value, RRES_value);
                } else {
                    m_TextView3.setText("SpeakBOX Test FAIL");
                    setFailButtonEnable(true);
                }
                release();
                isEnd = true;
            }
        }.execute();
    }

    private void release() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private float read(String node) {
        SysClassManager sy = new SysClassManager();
        sy.setFileInputStream(sy.fileInputOpen(node));
        byte value[] = sy.readFileInputByte();
        sy.fileInputClose();
        if (value == null) {
            return -1;
        }
        String result = (new String(value)).trim();
        if ("".equals(result)) {
            return -1;
        }
        return Float.parseFloat(result);
    }

    private void show() {
        float mLRES = 0;
        float mRRES = 0;
        float c = 0;
        float d = 0;
        float e = 0;
        float f = 0;

        c = read(TOP_RES_MAX);
        d = read(TOP_RES_MIN);
        e = read(BTM_RES_MAX);
        f = read(BTM_RES_MIN);

        MMILog.i(TAG, "audio.smartpa.top_res_max :" + c);
        MMILog.i(TAG, "audio.smartpa.top_res_min :" + d);
        MMILog.i(TAG, "audio.smartpa.btm_res_max :" + e);
        MMILog.i(TAG, "audio.smartpa.btm_res_min :" + f);

        mLRES = read(LRES);
        mRRES = read(RRES);

        MMILog.i(TAG, "audio.smartpa.LRES :" + mLRES);
        MMILog.i(TAG, "audio.smartpa.RRES :" + mRRES);

        if (mLRES == -1) {
            m_TextView3.setText("SpeakBOX Test FAIL");
            m_TextView1.setTextColor(Color.RED);
            m_TextView1.setText("TOP NOK");
        } else {
            if (mLRES > d && mLRES < c) {
                m_TextView1.setTextColor(Color.GREEN);
                m_TextView1.setText("TOP OK " + mLRES + "\n" + d + "~" + c);
            } else {
                m_TextView1.setTextColor(Color.RED);
                m_TextView1.setText("TOP NOK " + mLRES + "\n" + d + "~" + c);
            }
        }

        if (mRRES == -1) {
            m_TextView3.setText("SpeakBOX Test FAIL");
            m_TextView2.setTextColor(Color.RED);
            m_TextView2.setText("BOTTOM NOK");
        } else {
            if (mRRES > f && mRRES < e) {
                m_TextView2.setTextColor(Color.GREEN);
                m_TextView2.setText("BOTTOM OK " + mRRES + "\n" + f + "~" + e);
            } else {
                m_TextView2.setTextColor(Color.RED);
                m_TextView2.setText("BOTTOM NOK " + mRRES + "\n" + f + "~" + e);
            }
        }

        LRES_value = (int) mLRES;
        RRES_value = (int) mRRES;

        if (mLRES > d && mLRES < c && mRRES > f && mRRES < e) {
            setPassButtonEnable(true);
            setFailButtonEnable(true);
            m_TextView3.setText("SpeakBOX Test PASS");
        } else {
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            m_TextView3.setText("SpeakBOX Test FAIL");
        }
    }

    private void saveResult(int L_value, int R_value) {
        if (traceabilityStruct != null) {
            byte[] result = new byte[8];
            if (L_value >= Integer.MAX_VALUE) {
                result[0] = (byte) 0xFF;
                result[1] = (byte) 0xFF;
                result[2] = (byte) 0xFF;
                result[3] = (byte) 0xFF;
            } else {
                result[0] = (byte) (L_value & 0xFF);
                result[1] = (byte) ((L_value >> 8) & 0xFF);
                result[2] = (byte) ((L_value >> 16) & 0xFF);
                result[3] = (byte) ((L_value >> 24) & 0xFF);
            }

            if (R_value >= Integer.MAX_VALUE) {
                result[4] = (byte) 0xFF;
                result[5] = (byte) 0xFF;
                result[6] = (byte) 0xFF;
                result[7] = (byte) 0xFF;
            } else {
                result[4] = (byte) (R_value & 0xFF);
                result[5] = (byte) ((R_value >> 8) & 0xFF);
                result[6] = (byte) ((R_value >> 16) & 0xFF);
                result[7] = (byte) ((R_value >> 24) & 0xFF);
            }
            for (int i = 0; i < result.length; i++) {
                MMILog.i(TAG, "result[" + i + "] = " + result[i]);
            }
            traceabilityStruct.putItem(TraceabilityStruct.ID.F0, result);
        }
    }

}

/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/activity/          */
/*                                                             Rawdata.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 15/11/24| zhengyang.ma   |                    | Add samsung tp rawdata test*/
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.android.mmi.util.MMILog;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.Time;
import au.com.bytecode.opencsv.CSVWriter;

public class Rawdata extends TestBase {
    private static final String NODE_INIT = "/sys/devices/soc/75ba000.i2c/i2c-12/12-0049/init_test";
    private static final String NODE_ITO = "/sys/devices/soc/75ba000.i2c/i2c-12/12-0049/ito_test";
    private static final String NODE_READ_MUTUAL_TUNE = "/sys/devices/soc/75ba000.i2c/i2c-12/12-0049/read_mutual_cx";
    private static final String NODE_READ_MUTUAL_RAW = "/sys/devices/soc/75ba000.i2c/i2c-12/12-0049/read_mutual_raw";
    private static final String NODE_READ_SELF_TUNE = "/sys/devices/soc/75ba000.i2c/i2c-12/12-0049/read_self_cx";
    private static final String NODE_READ_SELF_RAW = "/sys/devices/soc/75ba000.i2c/i2c-12/12-0049/read_self_raw";

    private final String mThresholdFile = "threshold_file_idol4s.csv";

    // node data
    private String[] init_head, ito_head;
    private String[] init_data, ito_data;
    private String[] init_end, ito_end;
    private String[] mutual_tune_head, mutual_raw_head;
    private String[][] mutual_tune_data, mutual_raw_data;
    private String[] mutual_tune_end, mutual_raw_end;
    private String[] self_tune_head, s_ix2_force, s_cx2_force, s_ix2_sense, s_cx2_sense, self_tune_end;
    private String[] self_raw_head, s_raw_force, s_raw_sense, self_raw_end;

    // threshold data
    private final int MAX = 50;
    private String[][] CX2_Hor_Thr = new String[MAX][MAX];
    private String[][] CX2_Ver_Thr = new String[MAX][MAX];
    private String[][] CX2_Min_Thr = new String[MAX][MAX];
    private String[][] CX2_Max_Thr = new String[MAX][MAX];
    private String[] s_ix2_force_min, s_ix2_sense_min, s_ix2_force_max, s_ix2_sense_max/*, s_cx2_force_min,
            s_cx2_sense_min, s_cx2_force_max, s_cx2_sense_max*/;
    private String m_raw_min_thres, m_raw_max_thres, m_raw_gap_thres, s_raw_force_min_thres, s_raw_force_max_thres,
            s_raw_sense_min_thres, s_raw_sense_max_thres;

    // error record
    private String mutual_tune_error = "";
    private String mutual_raw_error = "";
    private String self_tune_error = "";
    private String self_raw_error = "";

    @Override
    public void run() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {

                setDefTextMessage("During Test, Do not touch the screen.");

                boolean init = init_threshold();
                if (!init) {
                    return "INIT THRESHOULD FAIL";
                }

                String message = "";
                boolean init_test = start_init_test();
                boolean ito_test = start_ito_test();
                boolean mutual_tune_test = start_mutual_tune_test();
                boolean mutual_rawdata_test = start_mutual_rawdata_test();
                boolean self_tune_test = start_self_tune_test();
//              boolean self_rawdata_test = start_self_rawdata_test();

                if (!init_test) {
                    message += "INIT TEST FAIL\n";
                }
                if (!ito_test) {
                    message += "ITO TEST FAIL\n";
                }
                if (!mutual_tune_test) {
                    message += "MUTUAL TUNE TEST FAIL\n";
                }
                if (!mutual_rawdata_test) {
                    message += "MUTUAL RAWDATA TEST FAIL\n";
                }
                if (!self_tune_test) {
                    message += "SELF TUNE TEST FAIL\n";
                }
//              if (!self_rawdata_test) {
//                  message += "SELF RAWDATA TEST FAIL\n";
//              }
                if ("".equals(message)) {
                    message = "PASS";
                }

                saveData();

                return message;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result.equals("PASS")) {
                    setDefTextMessage("Pass.");
                    setPassButtonEnable(true);
                } else {
                    setDefTextMessage("Failed.\n" + result);
                    setPassButtonEnable(false);
                }

                Intent intent = mContext.getIntent();
                if(intent != null) {
                    boolean isRobust = intent.getBooleanExtra("robust_tp_raw", false);
                    if(isRobust) {
                        if (result.equals("PASS")) {
                            mContext.setResult(100, intent);
                        } else {
                            mContext.setResult(101, intent);
                        }
                        mContext.finish();
                    }
                }

            }
        }.execute();
    }

    // INIT
    // 0 pass
    // 1 fail
    private boolean start_init_test() {
        try {
            final int HEAD = 1;
            final int END = 1;
            FileInputStream fis = new FileInputStream(NODE_INIT);

            byte[] buffer_head = new byte[HEAD * 2];
            fis.read(buffer_head, 0, buffer_head.length);

            init_head = new String[HEAD];
            init_end = new String[END];

            String start = String.valueOf((char) buffer_head[0]) + String.valueOf((char) buffer_head[1]);

            init_head[0] = start;

            byte[] buffer_data = new byte[2];
            fis.read(buffer_data);

            String data = String.valueOf((char) buffer_data[0]) + String.valueOf((char) buffer_data[1]);

            init_data = new String[1];
            init_data[0] = data;

            byte[] buffer_end = new byte[END * 2];
            fis.read(buffer_end);

            String end = String.valueOf((char) buffer_end[0]) + String.valueOf((char) buffer_end[1]);
            init_end[0] = end;

            fis.close();
            return Integer.parseInt(new String(data).trim()) == 0;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "not exist node: " + NODE_INIT);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    // ITO
    // 0 pass
    // 1 fail
    private boolean start_ito_test() {
        try {
            final int HEAD = 1;
            final int END = 1;
            FileInputStream fis = new FileInputStream(NODE_ITO);

            byte[] buffer_head = new byte[HEAD * 2];
            fis.read(buffer_head, 0, buffer_head.length);

            ito_head = new String[HEAD];
            ito_end = new String[END];

            String start = String.valueOf((char) buffer_head[0]) + String.valueOf((char) buffer_head[1]);

            ito_head[0] = start;

            byte[] buffer_data = new byte[2];
            fis.read(buffer_data);

            String data = String.valueOf((char) buffer_data[0]) + String.valueOf((char) buffer_data[1]);

            ito_data = new String[1];
            ito_data[0] = data;

            byte[] buffer_end = new byte[END * 2];
            fis.read(buffer_end);

            String end = String.valueOf((char) buffer_end[0]) + String.valueOf((char) buffer_end[1]);
            ito_end[0] = end;

            fis.close();
            return Integer.parseInt(new String(data).trim()) == 0;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "not exist node: " + NODE_ITO);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    // MUTUAL_TUNE
    // 1 byte start
    // 1 byte f_cnt
    // 1 byte s_cnt
    // 1 byte m_cx1
    // m * n byte f_cnt*s_cnt
    // 1 byte end
    private boolean start_mutual_tune_test() {
        FileInputStream fis = null;
        try {
            final int HEAD = 4;
            final int END = 1;
            fis = new FileInputStream(NODE_READ_MUTUAL_TUNE);

            byte[] buffer_head = new byte[HEAD * 2];
            fis.read(buffer_head, 0, buffer_head.length);

            mutual_tune_head = new String[HEAD];
            mutual_tune_end = new String[END];

            String start = String.valueOf((char) buffer_head[0]) + String.valueOf((char) buffer_head[1]);
            String f_cnt = String.valueOf((char) buffer_head[2]) + String.valueOf((char) buffer_head[3]);
            String s_cnt = String.valueOf((char) buffer_head[4]) + String.valueOf((char) buffer_head[5]);
            String m_cx1 = String.valueOf((char) buffer_head[6]) + String.valueOf((char) buffer_head[7]);

            mutual_tune_head[0] = start;
            mutual_tune_head[1] = f_cnt;
            mutual_tune_head[2] = s_cnt;
            mutual_tune_head[3] = m_cx1;

            int row = Integer.valueOf(f_cnt, 16);
            int col = Integer.valueOf(s_cnt, 16);

            MMILog.d(TAG, "start_mutual_tune_test: " + "row:" + row + " col:" + col);

            byte[] buffer_data = new byte[row * col * 2];
            fis.read(buffer_data);

            byte[] buffer_end = new byte[END * 2];
            fis.read(buffer_end);

            String end = String.valueOf((char) buffer_end[0]) + String.valueOf((char) buffer_end[1]);
            mutual_tune_end[0] = end;

            fis.close();

            printBuffer("mutual_tune", "head", buffer_head);
            printBuffer("mutual_tune", "data", buffer_data);
            printBuffer("mutual_tune", "end", buffer_end);

            int[][] m_cx2 = new int[row][col];
            mutual_tune_data = new String[row][col];

            for (int i = 0; i < row * col * 2; i += 2) {
                int r = i / 2 / col;
                int c = i / 2 % col;

                String val = String.valueOf((char) buffer_data[i]) + String.valueOf((char) buffer_data[i + 1]);

                mutual_tune_data[r][c] = val;
                m_cx2[r][c] = Integer.valueOf(val, 16);
            }

            int P = 4;
            int Q = 1;
            int[][] m_cx = new int[row][col];
            for (int i = 0; i < row; i++) {
                // all rows
                for (int j = 0; j < col; j++)
                // all columns
                {
                    m_cx[i][j] = P * Integer.valueOf(m_cx1, 16) + Q * m_cx2[i][j];
                }
            }
            /*--- Check if all CX2 values equal to 0 ---*/
            int sum = 0;
            for (int i = 0; i < row; i++) {
                // all rows
                for (int j = 0; j < col; j++)
                // all columns
                {
                    sum += m_cx2[i][j];
                }
            }

            if (sum == 0) {
                MMILog.d(TAG, "mutual_tune, all zero error");// all zero error
                mutual_tune_error += ("all zero error");
                return false;
            }

            /*--- Check adjacent CX2 nodes against threshold maps ---*/
            byte cx2_err[][] = new byte[row][col]; // for recording error
                                                    // nodes
            int hrzErrCnt = 0; // horizontal error counter
            int vrtErrCnt = 0; // vertical error counter
            String hrz_error = "";
            String vrt_error = "";

            // sweep for horizontal delta
            for (int i = 0; i < row; i++) {// all rows
                for (int j = 0; j < (col - 1); j++) {// all columns,
                                                        // except
                                                        // Right-most
                                                        // column
                    int node1 = m_cx2[i][j]; // node under comparison
                    int node2 = m_cx2[i][j + 1]; // RIGHT neighbour

                    int threshold = Integer.parseInt(CX2_Hor_Thr[i][j].trim()); // horizontal
                                                                                // node
                                                                                // threshold
                    int delta = Math.abs(node1 - node2); // absolute difference
                    if (delta > threshold) {
                        String error = "hrz Err! delta:" + delta + " threshold:" + threshold + " row:" + i + " col:"
                                + j;
                        MMILog.e(TAG, "mutual_tune " + error);

                        hrz_error += "(" + i + "," + j + ") ";

                        cx2_err[i][j] += 0x01;
                        hrzErrCnt++;
                    }
                }
            }

            // sweep for vertical delta
            for (int i = 0; i < (row - 1); i++){ // all rows, except
                                                // bottom-most row
                for (int j = 0; j < col; j++)
                // all columns
                {
                    int node1 = m_cx2[i][j];
                    int node2 = m_cx2[i + 1][j]; // Bottom neighbour
                    int threshold = Integer.parseInt(CX2_Ver_Thr[i][j].trim()); // vertical
                                                                                // node
                                                                                // threshold
                    int delta = Math.abs(node1 - node2); // absolute difference
                    if (delta > threshold) {
                        String error = "vrt Err! delta:" + delta + " threshold:" + threshold + " row:" + i + " col:"
                                + j;
                        MMILog.e(TAG, "mutual_tune " + error);
                        vrt_error += "(" + i + "," + j + ") ";

                        cx2_err[i][j] += 0x02;
                        vrtErrCnt++;
                    }
                    // bit 1 to indicate horizontal delta error
                }
            }

            if(hrzErrCnt != 0) {
                mutual_tune_error += ("hrz Err: " + hrz_error + "\n");
            }

            if(vrtErrCnt != 0) {
                mutual_tune_error += ("vrt Err: " + vrt_error);
            }
            if ((hrzErrCnt + vrtErrCnt) != 0) {
                return false; // adjacent node error
            }

            String min_error = "";
            String max_error = "";
            /*--- Check CX2 against min and max theshold ---*/
            byte[][] cx2_min_err = new byte[row][col]; // for recording min
                                                        // error nodes
            byte[][] cx2_max_err = new byte[row][col]; // for recording max
                                                        // error nodes
            int minErrCnt = 0; // min error counter
            int maxErrCnt = 0; // max error counter
            for (int i = 0; i < row; i++){
                // all rows
                for (int j = 0; j < col; j++)
                // all columns
                {
                    int threshold_min = Integer.parseInt(CX2_Min_Thr[i][j].trim());
                    if (/* m_cx2[i][j] */m_cx[i][j] < threshold_min) {
                        String error = "min Err! threshold:" + threshold_min + " row:" + i + " col:" + j;
                        MMILog.e(TAG, "mutual_tune " + error);

                        min_error += "(" + i + "," + j + ") ";

                        minErrCnt++;
                        cx2_min_err[i][j] = 1;
                    }

                    int threshold_max = Integer.parseInt(CX2_Max_Thr[i][j].trim());
                    if (/* m_cx2[i][j] */m_cx[i][j] > threshold_max) {
                        String error = "max Err! threshold:" + threshold_max + " row:" + i + " col:" + j;
                        MMILog.e(TAG, "mutual_tune " + error);

                        max_error += "(" + i + "," + j + ") ";

                        maxErrCnt++;
                        cx2_max_err[i][j] = 1;
                    }
                }
            }

            if(minErrCnt != 0) {
                mutual_tune_error += ("min Err: " + min_error + "\n");
            }

            if(maxErrCnt != 0) {
                mutual_tune_error += ("max Err: " + max_error);
            }

            if ((minErrCnt + maxErrCnt) != 0) {
                return false; // CX2 min/max error
            }

            return true;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "not exist node: " + NODE_READ_MUTUAL_TUNE);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return false;
    }

    // MUTUAL_RAW_DATA
    // 1 byte start
    // 1 byte f_cnt
    // 1 byte s_cnt
    // m * n * 2 byte f_cnt * s_cnt *2
    // 1 byte end
    private boolean start_mutual_rawdata_test() {
        try {
            final int HEAD = 3;
            final int END = 1;
            FileInputStream fis = new FileInputStream(NODE_READ_MUTUAL_RAW);

            byte[] buffer_head = new byte[HEAD * 2];
            fis.read(buffer_head, 0, buffer_head.length);

            mutual_raw_head = new String[HEAD];
            mutual_raw_end = new String[END];

            String start = String.valueOf((char) buffer_head[0]) + String.valueOf((char) buffer_head[1]);
            String f_cnt = String.valueOf((char) buffer_head[2]) + String.valueOf((char) buffer_head[3]);
            String s_cnt = String.valueOf((char) buffer_head[4]) + String.valueOf((char) buffer_head[5]);

            mutual_raw_head[0] = start;
            mutual_raw_head[1] = f_cnt;
            mutual_raw_head[2] = s_cnt;

            int row = Integer.valueOf(f_cnt, 16);
            int col = Integer.valueOf(s_cnt, 16);

            MMILog.d(TAG, "start_mutual_rawdata_test: " + "row:" + row + " col:" + col);

            byte[] buffer_data = new byte[row * col * 2 * 2];
            fis.read(buffer_data);

            byte[] buffer_end = new byte[END * 2];
            fis.read(buffer_end);

            String end = String.valueOf((char) buffer_end[0]) + String.valueOf((char) buffer_end[1]);
            mutual_raw_end[0] = end;

            fis.close();

            printBuffer("mutual_rawdata", "head", buffer_head);
            printBuffer("mutual_rawdata", "data", buffer_data);
            printBuffer("mutual_rawdata", "end", buffer_end);

            int[][] m_raw = new int[row][col];
            mutual_raw_data = new String[row][col];

            for (int i = 0; i < (row * col) * 4; i += 4) {
                int r = i / 4 / col;
                int c = i / 4 % col;

                String first = String.valueOf((char) buffer_data[i]);
                String second = String.valueOf((char) buffer_data[i + 1]);
                String third = String.valueOf((char) buffer_data[i + 2]);
                String fourth = String.valueOf((char) buffer_data[i + 3]);

                String val = first + second + third + fourth;

                String convert = third + fourth + first + second;

                mutual_raw_data[r][c] = val;
                m_raw[r][c] = Integer.valueOf(convert, 16);
            }

            String min_error = "";
            String max_error = "";
            /*--- check Screen mutual raw data ---*/
            byte[][] mutual_raw_err = new byte[row][col]; // for recording
                                                            // min/max error
            int minErrCnt = 0; // min error counter
            int maxErrCnt = 0; // max error counter
            int m_raw_min = m_raw[0][0]; // initialize with first raw data
            int m_raw_max = m_raw[0][0]; // initialize with first raw data
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    // min check

                    int threshold_min = Integer.parseInt(m_raw_min_thres.trim());
                    if (m_raw[i][j] < threshold_min) {
                        String error = "min Err! threshold:" + threshold_min + " row:" + i + " col:" + j;
                        MMILog.e(TAG, "mutual_rawdata " + error);

                        min_error += "(" + i + "," + j + ") ";

                        minErrCnt++;
                        mutual_raw_err[i][j] = 1;
                        // 1 indicates min error
                    }
                    // max check

                    int threshold_max = Integer.parseInt(m_raw_max_thres.trim());
                    if (m_raw[i][j] > threshold_max) {
                        String error = "max Err! threshold:" + threshold_max + " row:" + i + " col:" + j;
                        MMILog.e(TAG, "mutual_rawdata " + error);

                        max_error += "(" + i + "," + j + ") ";

                        maxErrCnt++;
                        mutual_raw_err[i][j] = 2;
                        // 2 indicates max error
                    }
                    // find min
                    if (m_raw[i][j] < m_raw_min) {
                        m_raw_min = m_raw[i][j];
                    }
                    // find max
                    if (m_raw[i][j] > m_raw_max) {
                        m_raw_max = m_raw[i][j];
                    }
                }
            }
            if(minErrCnt != 0) {
                mutual_raw_error += ("min Err: " + min_error + "\n");
            }

            if(maxErrCnt != 0) {
                mutual_raw_error += ("max Err: " + max_error);
            }
            if ((minErrCnt + maxErrCnt) != 0) {
                return false; // mutual raw min/max error
            }

            int raw_gap = Integer.parseInt(m_raw_gap_thres);

            if ((m_raw_max - m_raw_min) > raw_gap) {
                String error = "raw_gap Err! threshold:" + raw_gap + " raw max:" + m_raw_max + " raw min:" + m_raw_min;
                MMILog.e(TAG, "mutual_rawdata " + error);

                mutual_raw_error += ("gap error, threshould:" + raw_gap + ", raw max:" + m_raw_max + ", raw min=" + m_raw_min);

                return false; // mutual raw gap error
            }

            return true;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "not exist node: " + NODE_READ_MUTUAL_RAW);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    // SELF_TUNE
    // 1 byte start
    // 1 byte f_cnt
    // 1 byte s_cnt
    // 1 byte f_ix1_f
    // 1 byte f_cx1_s
    // 1 byte s_ix1_f
    // 1 byte s_cx1_s
    // m + n byte f_cnt + s_cnt
    // m + n byte f_cnt + s_cnt
    // 1 byte end
    private boolean start_self_tune_test() {
        try {
            final int HEAD = 7;
            final int END = 1;
            FileInputStream fis = new FileInputStream(NODE_READ_SELF_TUNE);

            byte[] buffer_head = new byte[HEAD * 2];
            fis.read(buffer_head, 0, buffer_head.length);

            self_tune_head = new String[HEAD];
            self_tune_end = new String[END];

            String start = String.valueOf((char) buffer_head[0]) + String.valueOf((char) buffer_head[1]);
            String f_cnt = String.valueOf((char) buffer_head[2]) + String.valueOf((char) buffer_head[3]);
            String s_cnt = String.valueOf((char) buffer_head[4]) + String.valueOf((char) buffer_head[5]);
            String s_ix1_f = String.valueOf((char) buffer_head[6]) + String.valueOf((char) buffer_head[7]);
            String s_ix1_s = String.valueOf((char) buffer_head[8]) + String.valueOf((char) buffer_head[9]);
            String s_cx1_f = String.valueOf((char) buffer_head[10]) + String.valueOf((char) buffer_head[11]);
            String s_cx1_s = String.valueOf((char) buffer_head[12]) + String.valueOf((char) buffer_head[13]);

            self_tune_head[0] = start;
            self_tune_head[1] = f_cnt;
            self_tune_head[2] = s_cnt;
            self_tune_head[3] = s_ix1_f;
            self_tune_head[4] = s_ix1_s;
            self_tune_head[5] = s_cx1_f;
            self_tune_head[6] = s_cx1_s;

            int row = Integer.valueOf(f_cnt, 16);
            int col = Integer.valueOf(s_cnt, 16);

            MMILog.d(TAG, "start_self_tune_test: " + "row:" + row + " col:" + col);

            byte[] buffer_data = new byte[(row + col) * 2 * 2];
            fis.read(buffer_data);

            byte[] buffer_end = new byte[END * 2];
            fis.read(buffer_end);

            String end = String.valueOf((char) buffer_end[0]) + String.valueOf((char) buffer_end[1]);
            self_tune_end[0] = end;

            fis.close();

            printBuffer("self_tune", "head", buffer_head);
            printBuffer("self_tune", "data", buffer_data);
            printBuffer("self_tune", "end", buffer_end);

            ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < buffer_data.length; i += 2) {
                String val = String.valueOf((char) buffer_data[i]) + String.valueOf((char) buffer_data[i + 1]);
                list.add(val);
            }

            s_ix2_force = new String[row];
            s_cx2_force = new String[row];
            s_ix2_sense = new String[col];
            s_cx2_sense = new String[col];

            for (int i = 0; i < row; i++) {
                s_ix2_force[i] = list.get(0);
                list.remove(0);
            }

            for (int i = 0; i < row; i++) {
                s_cx2_force[i] = list.get(0);
                list.remove(0);
            }

            for (int i = 0; i < col; i++) {
                s_ix2_sense[i] = list.get(0);
                list.remove(0);
            }

            for (int i = 0; i < col; i++) {
                s_cx2_sense[i] = list.get(0);
                list.remove(0);
            }

            /*--- Calculate Self IX ---*/
            int[] s_ix_force = new int[row];
            int[] s_ix_sense = new int[col];
            int K = 2;
            int L = 1;
            int M = 2;
            int N = 1;
            for (int i = 0; i < row; i++) {
                s_ix_force[i] = K * Integer.parseInt(s_ix1_f, 16) + L * Integer.parseInt(s_ix2_force[i].trim(), 16);
            }
            for (int i = 0; i < col; i++) {
                s_ix_sense[i] = M * Integer.parseInt(s_ix1_s, 16) + N * Integer.parseInt(s_ix2_sense[i].trim(), 16);
            }

            String force_min_error = "";
            String force_max_error = "";
            String sense_min_error = "";
            String sense_max_error = "";

            /*--- check Self IX against min & max thresholds ---*/
            byte[] s_ix_force_err = new byte[row];
            byte[] s_ix_sense_err = new byte[col];
            int force_minErrCnt = 0; // force min error counter
            int force_maxErrCnt = 0; // force max error counter
            int sense_minErrCnt = 0; // sense min error counter
            int sense_maxErrCnt = 0; // sense max error counter

            for (int i = 0; i < row; i++) {
                int min = Integer.parseInt(s_ix2_force_min[i].trim());
                int max = Integer.parseInt(s_ix2_force_max[i].trim());

                if (s_ix_force[i] < min) {
                    String error = "ix2 force min Err! threshold:" + min + " col:" + i;
                    MMILog.e(TAG, "self_tune " + error);

                    force_min_error += "(" + 0 + "," + i + ") ";

                    force_minErrCnt++;
                    s_ix_force_err[i] += 1; // record min error
                }
                if (s_ix_force[i] > max) {
                    String error = "ix2 force max Err! threshold:" + max + " col:" + i;
                    MMILog.e(TAG, "self_tune " + error);

                    force_max_error += "(" + 0 + "," + i + ") ";

                    force_maxErrCnt++;
                    s_ix_force_err[i] += 2; // record max error
                }
            }

            if(force_minErrCnt != 0) {
                mutual_raw_error += ("force min Err: " + force_min_error + "\n");
            }
            if(force_maxErrCnt != 0) {
                mutual_raw_error += ("force max Err: " + force_max_error + "\n");
            }

            for (int i = 0; i < col; i++) {
                int min = Integer.parseInt(s_ix2_sense_min[i].trim());
                int max = Integer.parseInt(s_ix2_sense_max[i].trim());

                if (s_ix_sense[i] < min) {
                    String error = "ix2 sense min Err! threshold:" + min + " col:" + i;
                    MMILog.e(TAG, "self_tune " + error);

                    sense_min_error += "(" + 2 + "," + i + ") ";

                    sense_minErrCnt++;
                    s_ix_sense_err[i] += 1; // record min error
                }
                if (s_ix_sense[i] > max) {
                    String error = "ix2 sense max Err! threshold:" + max + " col:" + i;
                    MMILog.e(TAG, "self_tune " + error);

                    sense_min_error += "(" + 2 + "," + i + ") ";

                    sense_maxErrCnt++;
                    s_ix_sense_err[i] += 2; // record max error
                }
            }

            if(force_minErrCnt != 0) {
                mutual_raw_error += ("sense_min Err: " + sense_min_error + "\n");
            }
            if(force_maxErrCnt != 0) {
                mutual_raw_error += ("sense_max Err: " + sense_max_error + "\n");
            }

            if ((force_minErrCnt + force_maxErrCnt + sense_minErrCnt + sense_maxErrCnt) != 0) {
                return false; // self IX2 min/max error
            }

            int m_force_min = s_ix_force[0];
            int m_force_max = s_ix_force[0];

            for (int i = 0; i < row; i++) {
                // find min
                if (s_ix_force[i] < m_force_min) {
                    m_force_min = s_ix_force[i];
                }
                // find max
                if (s_ix_force[i] > m_force_max) {
                    m_force_max = s_ix_force[i];
                }
            }

            int force_gap = 100;

            if ((m_force_max - m_force_min) > force_gap) {
                String error = "force_gap Err! threshold:" + force_gap + " force max:" + m_force_max + " force min:" + m_force_min;
                MMILog.e(TAG, "self tune " + error);

                mutual_raw_error += ("force gap Err, threshold:" + force_gap + ", force max:"+ m_force_max + ", force min:" + m_force_min + "\n");

                return false; // self tune gap error
            }

            int m_sense_min = s_ix_sense[0];
            int m_sense_max = s_ix_sense[0];

            for (int i = 0; i < col; i++) {
                // find min
                if (s_ix_sense[i] < m_sense_min) {
                    m_sense_min = s_ix_sense[i];
                }
                // find max
                if (s_ix_sense[i] > m_sense_max) {
                    m_sense_max = s_ix_sense[i];
                }
            }

            int sense_gap = 100;

            if ((m_sense_max - m_sense_min) > sense_gap) {
                String error = "sense_gap Err! threshold:" + sense_gap + " sense max:" + m_sense_max + " sense min:" + m_sense_min;
                MMILog.e(TAG, "self tune " + error);

                mutual_raw_error += ("sense gap Err, threshold:" + sense_gap + ", sense max:"+ m_sense_max + ", sense min:" + m_sense_min + "\n");

                return false; // self tune gap error
            }

            return true;

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "not exist node: " + NODE_READ_SELF_TUNE);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    // READ_SELF_RAW
    // 1 byte start
    // 1 byte f_cnt
    // 1 byte s_cnt
    // m * n * 2 byte f_cnt * s_cnt *2
    // 1 byte end
    private boolean start_self_rawdata_test() {
        try {
            final int HEAD = 3;
            final int END = 1;
            FileInputStream fis = new FileInputStream(NODE_READ_SELF_RAW);

            byte[] buffer_head = new byte[HEAD * 2];
            fis.read(buffer_head, 0, buffer_head.length);

            self_raw_head = new String[HEAD];
            self_raw_end = new String[END];

            String start = String.valueOf((char) buffer_head[0]) + String.valueOf((char) buffer_head[1]);
            String f_cnt = String.valueOf((char) buffer_head[2]) + String.valueOf((char) buffer_head[3]);
            String s_cnt = String.valueOf((char) buffer_head[4]) + String.valueOf((char) buffer_head[5]);

            self_raw_head[0] = start;
            self_raw_head[1] = f_cnt;
            self_raw_head[2] = s_cnt;

            int row = Integer.valueOf(f_cnt, 16);
            int col = Integer.valueOf(s_cnt, 16);
            MMILog.d(TAG, "start_self_rawdata_test: " + "row:" + row + " col:" + col);

            byte[] buffer_data = new byte[(row + col) * 2 * 2];
            fis.read(buffer_data);

            byte[] buffer_end = new byte[END * 2];
            fis.read(buffer_end);

            String end = String.valueOf((char) buffer_end[0]) + String.valueOf((char) buffer_end[1]);
            self_raw_end[0] = end;

            fis.close();

            printBuffer("self_raw", "head", buffer_head);
            printBuffer("self_raw", "data", buffer_data);
            printBuffer("self_raw", "end", buffer_end);

            ArrayList<String> list = new ArrayList<String>();
            ArrayList<String> convert_list = new ArrayList<String>();
            for (int i = 0; i < (row + col) * 4; i += 4) {

                String first = String.valueOf((char) buffer_data[i]);
                String second = String.valueOf((char) buffer_data[i + 1]);
                String third = String.valueOf((char) buffer_data[i + 2]);
                String fourth = String.valueOf((char) buffer_data[i + 3]);

                String val = first + second + third + fourth;
                String convert = third + fourth + first + second;

                list.add(val);
                convert_list.add(convert);
            }

            s_raw_force = new String[row];
            s_raw_sense = new String[col];

            String[] convert_s_raw_force = new String[row];
            String[] convert_s_raw_sense = new String[col];

            for (int i = 0; i < row; i++) {
                s_raw_force[i] = list.get(0);
                list.remove(0);
            }

            for (int i = 0; i < col; i++) {
                s_raw_sense[i] = list.get(0);
                list.remove(0);
            }

            for (int i = 0; i < row; i++) {
                convert_s_raw_force[i] = convert_list.get(0);
                convert_list.remove(0);
            }

            for (int i = 0; i < col; i++) {
                convert_s_raw_sense[i] = convert_list.get(0);
                convert_list.remove(0);
            }

            String force_min_error = "";
            String force_max_error = "";
            String sense_min_error = "";
            String sense_max_error = "";

            /*--- check Self Raw data ---*/
            byte[] s_raw_force_err = new byte[row];
            byte[] s_raw_sense_err = new byte[col];
            int force_minErrCnt = 0; // force min error counter
            int force_maxErrCnt = 0; // force max error counter
            int sense_minErrCnt = 0; // sense min error counter
            int sense_maxErrCnt = 0; // sense max error counter

            int force_min = Integer.parseInt(s_raw_force_min_thres);
            int force_max = Integer.parseInt(s_raw_force_max_thres);

            for (int i = 0; i < row; i++) {

                if (Integer.valueOf(convert_s_raw_force[i], 16) < force_min) {
                    String error = "raw force min Err! threshold:" + force_min + " col:" + i;
                    MMILog.e(TAG, "self_raw " + error);

                    force_min_error += "(" + 0 + "," + i + ") ";

                    force_minErrCnt++;
                    s_raw_force_err[i] += 1; // record min error
                }
                if (Integer.valueOf(convert_s_raw_force[i], 16) > force_max) {
                    String error = "raw force max Err! threshold:" + force_max + " col:" + i;
                    MMILog.e(TAG, "self_raw " + error);

                    force_max_error += "(" + 0 + "," + i + ") ";

                    force_maxErrCnt++;
                    s_raw_force_err[i] += 2; // record max error
                }
            }

            if(force_minErrCnt != 0) {
                self_raw_error = "force min Err, threshold: " + force_min + ", " + force_min_error + "\n";
            }

            if(force_maxErrCnt != 0) {
                self_raw_error += ("force max Err, threshold: " + force_max + ", " + force_max_error + "\n");
            }

            int sense_min = Integer.parseInt(s_raw_sense_min_thres);
            int sense_max = Integer.parseInt(s_raw_sense_max_thres);

            for (int i = 0; i < col; i++) {

                if (Integer.valueOf(convert_s_raw_sense[i], 16) < sense_min) {
                    String error = "raw sense min Err! threshold:" + sense_min + " col:" + i;
                    MMILog.e(TAG, "self_raw " + error);

                    sense_min_error += "(" + 1 + "," + i + ") ";

                    sense_minErrCnt++;
                    s_raw_sense_err[i] += 1; // record min error
                }
                if (Integer.valueOf(convert_s_raw_sense[i], 16) > sense_max) {
                    String error = "raw sense max Err! threshold:" + sense_max + " col:" + i;
                    MMILog.e(TAG, "self_raw " + error);

                    sense_max_error += "(" + 1 + "," + i + ") ";

                    sense_maxErrCnt++;
                    s_raw_sense_err[i] += 2; // record max error
                }
            }

            if(sense_minErrCnt != 0) {
                self_raw_error += ("sense min Err: threshold: " + sense_min + ", " + sense_min_error + "\n");
            }

            if(sense_maxErrCnt != 0) {
                self_raw_error += ("sense max Err: threshold: " + sense_max + ", " + sense_max_error);
            }

            if ((sense_minErrCnt + sense_maxErrCnt + force_minErrCnt + force_maxErrCnt) != 0) {
                return false; // self raw min/max error
            }

            return true;

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "not exist node: " + NODE_READ_SELF_RAW);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    private boolean init_threshold() {
        ArrayList<String> mTags = new ArrayList<String>();
        mTags.add(0, "CX2_HORIZONTAL_THRESHOLD");
        mTags.add(1, "CX2_VERTICAL_THRESHOLD");
        mTags.add(2, "CX2_MIN_THRESHOLD");
        mTags.add(3, "CX2_MAX_THRESHOLD");
        mTags.add(4, "SELF_IX2_MIN");
        mTags.add(5, "SELF_IX2_MAX");
        mTags.add(6, "SELF_CX2_MIN");
        mTags.add(7, "SELF_CX2_MAX");
        mTags.add(8, "MUTUAL_RAW_MIN");
        mTags.add(9, "MUTUAL_RAW_MAX");
        mTags.add(10, "MUTUAL_RAW_GAP");
        mTags.add(11, "SELF_RAW_FORCE_MIN");
        mTags.add(12, "SELF_RAW_FORCE_MAX");
        mTags.add(13, "SELF_RAW_SENSE_MIN");
        mTags.add(14, "SELF_RAW_SENSE_MAX");

        try {
            InputStream instream = mContext.getResources().getAssets().open(mThresholdFile);
            if (instream != null) {
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                String line = "";
                int current_tag_index = 0;
                int row = 0;
                boolean read_rx_row = false;

                while ((line = buffreader.readLine()) != null) {
                    if (line.equals("") || line.startsWith("//") || line.startsWith(",,,")) {
                        continue;
                    }

                    boolean find_new_tag = false;
                    for (int i = current_tag_index; i < mTags.size(); i++) {
                        if (line.startsWith(mTags.get(i))) {
                            find_new_tag = true;
                            read_rx_row = false;
                            current_tag_index = i;
                            row = 0;
                            break;
                        }
                    }

                    if (find_new_tag) {
                        continue;
                    }

                    if (current_tag_index == 0) {
                        if (!line.startsWith("TX")) {
                            continue;
                        }
                        int start = line.indexOf(',') + 1;
                        int end = line.lastIndexOf(',');
                        String[] item = line.substring(start, end).split(",");
                        for (int i = 0; i < item.length; i++) {
                            CX2_Hor_Thr[row][i] = item[i].trim();
                        }
                        row++;
                    } else if (current_tag_index == 1) {
                        if (!line.startsWith("TX")) {
                            continue;
                        }
                        int start = line.indexOf(',') + 1;
                        int end = line.lastIndexOf(',');
                        String[] item = line.substring(start, end).split(",");
                        for (int i = 0; i < item.length; i++) {
                            CX2_Ver_Thr[row][i] = item[i].trim();
                        }
                        row++;
                    } else if (current_tag_index == 2) {
                        if (!line.startsWith("TX")) {
                            continue;
                        }
                        int start = line.indexOf(',') + 1;
                        int end = line.lastIndexOf(',');
                        String[] item = line.substring(start, end).split(",");
                        for (int i = 0; i < item.length; i++) {
                            CX2_Min_Thr[row][i] = item[i].trim();
                        }
                        row++;
                    } else if (current_tag_index == 3) {
                        if (!line.startsWith("TX")) {
                            continue;
                        }
                        int start = line.indexOf(',') + 1;
                        int end = line.lastIndexOf(',');
                        String[] item = line.substring(start, end).split(",");
                        for (int i = 0; i < item.length; i++) {
                            CX2_Max_Thr[row][i] = item[i].trim();
                        }
                        row++;
                    } else if (current_tag_index == 4) {
                        if (line.startsWith(", TX")) {
                            continue;
                        } else if (line.startsWith(", RX")) {
                            read_rx_row = true;
                            continue;
                        }
                        int start = line.indexOf(',') + 1;
                        int end = line.lastIndexOf(',');
                        String[] item = line.substring(start, end).split(",");
                        if (read_rx_row) {
                            s_ix2_sense_min = item;
                        } else {
                            s_ix2_force_min = item;
                        }
                    } else if (current_tag_index == 5) {
                        if (line.startsWith(", TX")) {
                            continue;
                        } else if (line.startsWith(", RX")) {
                            read_rx_row = true;
                            continue;
                        }
                        int start = line.indexOf(',') + 1;
                        int end = line.lastIndexOf(',');
                        String[] item = line.substring(start, end).split(",");
                        if (read_rx_row) {
                            s_ix2_sense_max = item;
                        } else {
                            s_ix2_force_max = item;
                        }
                    } else if (current_tag_index == 6) {
//                      if (line.startsWith(", TX")) {
//                          continue;
//                      } else if (line.startsWith(", RX")) {
//                          read_rx_row = true;
//                          continue;
//                      }
//                      int start = line.indexOf(',') + 1;
//                      int end = line.lastIndexOf(',');
//                      String[] item = line.substring(start, end).split(",");
//                      if (read_rx_row) {
//                          s_cx2_sense_min = item;
//                      } else {
//                          s_cx2_force_min = item;
//                      }
                    } else if (current_tag_index == 7) {
//                      if (line.startsWith(", TX")) {
//                          continue;
//                      } else if (line.startsWith(", RX")) {
//                          read_rx_row = true;
//                          continue;
//                      }
//                      int start = line.indexOf(',') + 1;
//                      int end = line.lastIndexOf(',');
//                      String[] item = line.substring(start, end).split(",");
//                      if (read_rx_row) {
//                          s_cx2_sense_max = item;
//                      } else {
//                          s_cx2_force_max = item;
//                      }
                    } else if (current_tag_index == 8) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            m_raw_min_thres = thres[0].trim();
                        }
                    } else if (current_tag_index == 9) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            m_raw_max_thres = thres[0].trim();
                        }
                    } else if (current_tag_index == 10) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            m_raw_gap_thres = thres[0].trim();
                        }
                    } else if (current_tag_index == 11) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            s_raw_force_min_thres = thres[0].trim();
                        }
                    } else if (current_tag_index == 12) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            s_raw_force_max_thres = thres[0].trim();
                        }
                    } else if (current_tag_index == 13) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            s_raw_sense_min_thres = thres[0].trim();
                        }
                    } else if (current_tag_index == 14) {
                        String[] thres = line.split(",");
                        if (thres.length > 0) {
                            s_raw_sense_max_thres = thres[0].trim();
                        }
                    } else {
                        // do nothing
                    }
                }

                instream.close();
                return true;
            }
        } catch (IOException e) {
            MMILog.e(TAG, "init_threshold error: " + e.getMessage());
            return false;
        }

        return false;
    }

    private void saveData() {
        try {
            String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();

            Time time = new Time();
            time.setToNow();

            String csvFileName = new String("tp_raw_test_");

            csvFileName = csvFileName.concat(time.format("%Y%m%d_%H%M%S"));
            csvFileName = csvFileName.concat(".csv");

            String csvFilePath = sdCardPath.concat("/");
            csvFilePath = csvFilePath.concat(csvFileName);

            MMILog.d(TAG, "sdCardPath = " + sdCardPath);
            MMILog.d(TAG, "filePath = " + csvFilePath);

            CSVWriter writer = new CSVWriter(new FileWriter(csvFilePath), CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER);

            // ======================INIT============================
            writer.writeNext(new String[] { ("init,,,,,,,,") });

            writer.writeNext(new String[] { init_head[0] });
            writer.writeNext(new String[] { init_data[0] });
            writer.writeNext(new String[] { init_end[0] });

            writer.writeNext(new String[] { (",,,,,,,,") });

            // ======================ITO============================

            writer.writeNext(new String[] { ("ito,,,,,,,,") });

            writer.writeNext(new String[] { ito_head[0] });
            writer.writeNext(new String[] { ito_data[0] });
            writer.writeNext(new String[] { ito_end[0] });

            writer.writeNext(new String[] { (",,,,,,,,") });

            // ===================MUTUAL TUNE============================
            int mutual_tune_row = mutual_tune_data.length;
            int mutual_tune_col = mutual_tune_data[0].length;

            writer.writeNext(new String[] { ("read_mutual_tune,,,,,,,,") });

            for (int i = 0; i < mutual_tune_head.length; i++) {
                writer.writeNext(new String[] { mutual_tune_head[i] });
            }

            String[] mutual_tune_logData_DEC = new String[mutual_tune_col];
            for (int i = 0; i < mutual_tune_row; i++) {
                for (int j = 0; j < mutual_tune_col; j++) {
                    mutual_tune_logData_DEC[j] = String.valueOf(Integer.valueOf(mutual_tune_data[i][j], 16));
                }
                writer.writeNext(mutual_tune_logData_DEC);
            }

            writer.writeNext(new String[] { mutual_tune_end[0] });

            writer.writeNext(new String[] { (mutual_tune_error) });

            writer.writeNext(new String[] { (",,,,,,,,") });

            // ===================SELF TUNE============================
            writer.writeNext(new String[] { ("read_self_tune,,,,,,,,") });

            for (int i = 0; i < self_tune_head.length; i++) {
                writer.writeNext(new String[] { self_tune_head[i] });
            }

            String[] s_ix2_force_logData_DEC = new String[s_ix2_force.length];
            for (int i = 0; i < s_ix2_force.length; i++) {
                s_ix2_force_logData_DEC[i] = String.valueOf(Integer.valueOf(s_ix2_force[i], 16));
            }
            writer.writeNext(s_ix2_force_logData_DEC);

            String[] s_cx2_force_logData_DEC = new String[s_cx2_force.length];
            for (int i = 0; i < s_cx2_force.length; i++) {
                s_cx2_force_logData_DEC[i] = String.valueOf(Integer.valueOf(s_cx2_force[i], 16));
            }
            writer.writeNext(s_cx2_force_logData_DEC);

            String[] s_ix2_sense_logData_DEC = new String[s_ix2_sense.length];
            for (int i = 0; i < s_ix2_sense.length; i++) {
                s_ix2_sense_logData_DEC[i] = String.valueOf(Integer.valueOf(s_ix2_sense[i], 16));
            }
            writer.writeNext(s_ix2_sense_logData_DEC);

            String[] s_cx2_sense_logData_DEC = new String[s_cx2_sense.length];
            for (int i = 0; i < s_cx2_sense.length; i++) {
                s_cx2_sense_logData_DEC[i] = String.valueOf(Integer.valueOf(s_cx2_sense[i], 16));
            }
            writer.writeNext(s_cx2_sense_logData_DEC);

            writer.writeNext(new String[] { self_tune_end[0] });

            writer.writeNext(new String[] { (self_tune_error) });

            writer.writeNext(new String[] { (",,,,,,,,") });


            int row = mutual_raw_data.length;
            int col = mutual_raw_data[0].length;

            // ===================MUTUAL RAW============================
            writer.writeNext(new String[] { ("read_mutual_raw,,,,,,,,") });

            for (int i = 0; i < mutual_raw_head.length; i++) {
                writer.writeNext(new String[] { mutual_raw_head[i] });
            }

            String[] mutual_raw_logData_DEC = new String[col];
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    String data1 = mutual_raw_data[i][j].substring(0, 2);
                    String data2 = mutual_raw_data[i][j].substring(2, 4);
                    mutual_raw_logData_DEC[j] = String.valueOf(Integer.valueOf((data2 + data1), 16));
                }
                writer.writeNext(mutual_raw_logData_DEC);
            }
            writer.writeNext(new String[] { mutual_raw_end[0] });

            writer.writeNext(new String[] { (mutual_raw_error) });

            writer.writeNext(new String[] { (",,,,,,,,") });

//          // ===================SELF RAW============================
//          writer.writeNext(new String[] { ("read_self_raw,,,,,,,,") });
//
//          for (int i = 0; i < self_raw_head.length; i++) {
//              writer.writeNext(new String[] { self_raw_head[i] });
//          }
//
//          String[] s_raw_force_logData = new String[s_raw_force.length];
//          for (int i = 0; i < s_raw_force.length; i++) {
//              s_raw_force_logData[i] = s_raw_force[i];
//          }
//          writer.writeNext(s_raw_force_logData);
//
//          String[] s_raw_sense_logData = new String[s_raw_sense.length];
//          for (int i = 0; i < s_raw_sense.length; i++) {
//              s_raw_sense_logData[i] = s_raw_sense[i];
//          }
//          writer.writeNext(s_raw_sense_logData);
//          writer.writeNext(new String[] { self_raw_end[0] });
//
//          for (int i = 0; i < self_raw_error.size(); i++) {
//              writer.writeNext(new String[] { (self_raw_error.get(i)) });
//          }
//          writer.writeNext(new String[] { (",,,,,,,,") });

            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void printBuffer(String fun, String tag, byte[] buffer) {
        String data = "";
        for (int i = 0; i < buffer.length; i++) {
            data += (" " + (char) buffer[i]);
        }
        MMILog.d(TAG, fun + "  " + tag + "  size:" + data.length() + "\n" + data);
    }
}
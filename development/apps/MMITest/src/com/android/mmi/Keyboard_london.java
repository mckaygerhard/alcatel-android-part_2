/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Keyboard.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;
import android.content.ContentResolver;
import android.os.Handler;
import android.os.SystemProperties;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;


public class Keyboard_london extends Keyboard {

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_keypress_london, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
        visibleButtonLength = 0;
        pressedButtonLength = 0;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        buttonStringArray = mContext.getResources().getStringArray(R.array.key_text);
        final int allButtonLength = buttonStringArray.length;

        buttonArray = new Button[allButtonLength];
        buttonResourceArray = new int[allButtonLength];

        // index 0 is CALL
        for (int i = 1; i < allButtonLength; i++) {
            buttonResourceArray[i] = mContext.getResources().getIdentifier(
                    "key_btn_" + i, "id", mContext.getPackageName());
            if(buttonResourceArray[i]==0) {
                buttonArray[i] = null;
            }
            else {
                buttonArray[i] = (Button) findViewById(buttonResourceArray[i]);
                if(buttonArray[i]==null) {
                    continue;
                }
                buttonArray[i].setText(buttonStringArray[i]);
                buttonArray[i].setFocusable(false);
                if(i == 3 || i == 1 || i == 2){// 3 --> Back, 1 --> Home, 2 --> Menu
                    View parent = (View) buttonArray[i].getParent();
                    parent.setVisibility(View.INVISIBLE);
                    buttonArray[i] = null;
                }
                else {
                    visibleButtonLength++;
                }
            }
        }
    }
}

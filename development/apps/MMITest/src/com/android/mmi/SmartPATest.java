package com.android.mmi;

import java.io.FileOutputStream;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.TextView;

public class SmartPATest extends TestBase {
    private TextView m_TextView1;
    private TextView m_TextView2;
    private TextView m_TextView3;
    private TraceabilityStruct traceabilityStruct;
    private int LOHM_value = 0;
    private int ROHM_value = 0;
    private boolean isEnd = false;

    private static final String LOHM = "/sys/class/ftm_cal/LOHM";
    private static final String ROHM = "/sys/class/ftm_cal/ROHM";
    private static final String TOP_IMP_MAX = "/sys/class/ftm_cal/top_imp_max";
    private static final String TOP_IMP_MIN = "/sys/class/ftm_cal/top_imp_min";
    private static final String BTM_IMP_MAX = "/sys/class/ftm_cal/btm_imp_max";
    private static final String BTM_IMP_MIN = "/sys/class/ftm_cal/btm_imp_min";
    private static final String STATUS = "/sys/class/ftm_cal/status";

    @Override
    public void create(CommonActivity a) {
        super.create(a);
        setContentView(R.layout.test_smartpa, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
        m_TextView1 = (TextView) this.findViewById(R.id.smartpa_message_1);
        m_TextView3 = (TextView) this.findViewById(R.id.smartpa_message_3);
        m_TextView2 = (TextView) this.findViewById(R.id.smartpa_message_2);
    }

    @Override
    public void run() {
        try {
            traceabilityStruct = new TraceabilityStruct();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setFailButtonEnable(false);
        Teststart();
    }

    private float read(String node) {
        SysClassManager sy = new SysClassManager();
        sy.setFileInputStream(sy.fileInputOpen(node));
        byte value[] = sy.readFileInputByte();
        sy.fileInputClose();
        if (value == null) {
            return -1;
        }
        String result = (new String(value)).trim();
        if ("".equals(result)) {
            return -1;
        }
        return Float.parseFloat(result);
    }

    private void Teststart() {
        m_TextView3.setText("Do SmartPA Test,Please wait...");

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {

                long start_time = System.currentTimeMillis();
                MMILog.i(TAG, "start... " + start_time);

                SysClassManager sy = new SysClassManager();
                FileOutputStream fos = sy.fileOutputOpen(STATUS);
                if (fos != null) {
                    sy.setFileOutputStream(fos);
                    sy.WriteFileOutputChar("start");
                    sy.fileOutputClose();

                    long end_time = System.currentTimeMillis();
                    MMILog.i(TAG, "end... " + end_time);
                    MMILog.i(TAG, "total used time: " + (end_time - start_time));

                    return "PASS";
                } else {
                    MMILog.i(TAG, "output status node fail");
                    return "FAIL";
                }

            }

            @Override
            protected void onPostExecute(String result) {
                if (result.equals("PASS")) {
                    show();
                    saveResult(LOHM_value, ROHM_value);
                } else {
                    m_TextView3.setText("SmartPA Test FAIL");
                    setFailButtonEnable(true);
                }
                isEnd = true;
            }
        }.execute();
    }

    @Override
    public void onBackPressed() {
        if (!isEnd) {
            return;
        }
        super.onBackPressed();
    }

    private void show() {
        float mLOHM = 0;
        float mROHM = 0;
        float c = 0;
        float d = 0;
        float e = 0;
        float f = 0;

        c = read(TOP_IMP_MAX);
        d = read(TOP_IMP_MIN);
        e = read(BTM_IMP_MAX);
        f = read(BTM_IMP_MIN);

        MMILog.i(TAG, "audio.smartpa.top_imp_max :" + c);
        MMILog.i(TAG, "audio.smartpa.top_imp_min :" + d);
        MMILog.i(TAG, "audio.smartpa.btm_imp_max :" + e);
        MMILog.i(TAG, "audio.smartpa.btm_imp_min :" + f);

        mLOHM = read(LOHM);
        mROHM = read(ROHM);

        MMILog.i(TAG, "audio.smartpa.LOHM :" + mLOHM);
        MMILog.i(TAG, "audio.smartpa.ROHM :" + mROHM);

        if (mLOHM == -1) {
            m_TextView3.setText("SmartPA Test FAIL");
            m_TextView1.setTextColor(Color.RED);
            m_TextView1.setText("TOP NOK");
        } else {
            if (mLOHM > d && mLOHM < c) {
                m_TextView1.setTextColor(Color.GREEN);
                m_TextView1.setText("TOP OK " + mLOHM + "\n" + d + "~" + c);
            } else {
                m_TextView1.setTextColor(Color.RED);
                m_TextView1.setText("TOP NOK " + mLOHM + "\n" + d + "~" + c);
            }
        }

        if (mROHM == -1) {
            m_TextView3.setText("SmartPA Test FAIL");
            m_TextView2.setTextColor(Color.RED);
            m_TextView2.setText("BOTTOM NOK");
        } else {
            if (mROHM > f && mROHM < e) {
                m_TextView2.setTextColor(Color.GREEN);
                m_TextView2.setText("BOTTOM OK " + mROHM + "\n" + f + "~" + e);
            } else {
                m_TextView2.setTextColor(Color.RED);
                m_TextView2.setText("BOTTOM NOK " + mROHM + "\n" + f + "~" + e);
            }
        }

        LOHM_value = (int) (mLOHM * 100);
        ROHM_value = (int) (mROHM * 100);

        if (mLOHM > d && mLOHM < c && mROHM > f && mROHM < e) {
            setPassButtonEnable(true);
            setFailButtonEnable(true);
            m_TextView3.setText("SmartPA Test PASS");
        } else {
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            m_TextView3.setText("SmartPA Test FAIL");
        }
    }

    private void saveResult(int L_value, int R_value) {
        if (traceabilityStruct != null) {
            byte[] result = new byte[8];
            if (L_value >= Integer.MAX_VALUE) {
                result[0] = (byte) 0xFF;
                result[1] = (byte) 0xFF;
                result[2] = (byte) 0xFF;
                result[3] = (byte) 0xFF;
            } else {
                result[0] = (byte) (L_value & 0xFF);
                result[1] = (byte) ((L_value >> 8) & 0xFF);
                result[2] = (byte) ((L_value >> 16) & 0xFF);
                result[3] = (byte) ((L_value >> 24) & 0xFF);
            }

            if (R_value >= Integer.MAX_VALUE) {
                result[4] = (byte) 0xFF;
                result[5] = (byte) 0xFF;
                result[6] = (byte) 0xFF;
                result[7] = (byte) 0xFF;
            } else {
                result[4] = (byte) (R_value & 0xFF);
                result[5] = (byte) ((R_value >> 8) & 0xFF);
                result[6] = (byte) ((R_value >> 16) & 0xFF);
                result[7] = (byte) ((R_value >> 24) & 0xFF);
            }
            for (int i = 0; i < result.length; i++) {
                MMILog.i(TAG, "result[" + i + "] = " + result[i]);
            }
            traceabilityStruct.putItem(TraceabilityStruct.ID.SMART_PA, result);
        }
    }
}

/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/BatteryLevel.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;
import com.android.mmi.BuildConfig;

public class BatteryLevel extends TestBase {
    private final static int VALUE_VOLTAGE_DEFALUT = 0;
    private final static int POWEROFF_TIMEOUT = 10000;

    private String stateString;

    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;

    private int levelMax;
    private int levelMin;
    private int resIdNeedCharge;
    private int resIdNeedDischarge;

    private View rootView;
    private TextView messageTextView;
    private TextView tipTextView;
    private Button shutDownBtn;

    private boolean isBatteryLevel = false;
    private boolean chargeStoped = false;
    private boolean isCharging = false;
    private JRDClient jrdClient = null;

    private int mSystemScreenBrightMode;
    private int mSystemScreenBrigntness;
    private NotificationManager mNotificationManager;

    @Override
    public void create(CommonActivity a) {
        super.create(a);
        this.setContentView(R.layout.test_batterylevel, LAYOUTTYPE.LAYOUT_CUST_WITHOUT_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub

        stateString = mContext.getString(R.string.battery_level_message);
        messageTextView = (TextView) findViewById(R.id.textview_common_message);
        messageTextView.setText(String.format(stateString,
                VALUE_VOLTAGE_DEFALUT));

        tipTextView = (TextView) findViewById(R.id.textview_tip);
        rootView = findViewById(R.id.root_view);
        shutDownBtn = (Button) findViewById(R.id.botton_shutdown);

        shutDownBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent shutdown = new Intent(Intent.ACTION_REQUEST_SHUTDOWN);
                shutdown.putExtra(Intent.EXTRA_KEY_CONFIRM, false);
                shutdown.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(shutdown);
            }
        });

        jrdClient = new JRDClient();

        levelMax = 60;
        levelMin = 42;

        resIdNeedCharge = R.string.battery_level_tip_needcharge;
        resIdNeedDischarge = R.string.battery_level_tip_needdischarge;

        broadcastReceiver = new MemberReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        mNotificationManager = (NotificationManager) mContext
                .getSystemService(mContext.NOTIFICATION_SERVICE);
    }

    private String getStateString(int level) {
        return String.format(stateString, level) + "%";
    }

    private class MemberReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,
                    VALUE_VOLTAGE_DEFALUT);
            int state = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
                    BatteryManager.BATTERY_STATUS_UNKNOWN);
            messageTextView.setText(getStateString(level));
            if (state == BatteryManager.BATTERY_STATUS_CHARGING) {
                isCharging = true;
            } else {
                isBatteryLevel = false;
                isCharging = false;
            }

            if (isBatteryLevel && level == levelMax) {
                isBatteryLevel = false;
                rootView.setBackgroundColor(Color.GREEN);
            }
            if (level > levelMax) {
                stopCharger();
                setLcdBacklight(SysClassManager.NORMAL);
                rootView.setBackgroundColor(Color.RED);
                tipTextView.setText(resIdNeedDischarge);
                shutDownBtn.setVisibility(View.GONE);
            } else if (level > levelMin && level <= levelMax) {
                if (!isBatteryLevel) {
                        stopCharger();
                        setLcdBacklight(SysClassManager.NORMAL);
                        rootView.setBackgroundColor(Color.GREEN);
                        tipTextView.setText(R.string.battery_level_tip_ok);
                        shutDownBtn.setVisibility(View.VISIBLE);
                } else {
                        if (!BuildConfig.isSW) {
                            shutDownBtn.setVisibility(View.VISIBLE);
                        }
                        setLcdBacklight(SysClassManager.NORMAL);
                        if (state == BatteryManager.BATTERY_STATUS_CHARGING) {
                            tipTextView.setText(R.string.battery_level_tip_charging);
                        } else {
                            tipTextView.setText(R.string.battery_level_tip_ok);
                        }
                        rootView.setBackgroundColor(Color.GREEN);
                }
            } else if (level <= levelMin) {
                isBatteryLevel = true;
                restartCharger();
                setLcdBacklight(SysClassManager.MIN);

                rootView.setBackgroundColor(Color.RED);
                shutDownBtn.setVisibility(View.GONE);
                if (state == BatteryManager.BATTERY_STATUS_CHARGING) {
                    tipTextView.setText(R.string.battery_level_tip_charging);
                } else {
                    tipTextView.setText(resIdNeedCharge);
                }
            }
        }
    }

    private void stopCharger() {
        if (isCharging && !chargeStoped && jrdClient !=null) {
            System.out.println("level  stopCharger");
            jrdClient.commandCharge(JRDClient.CMD_STOPCHARGE);
            chargeStoped = true;
        }
        setChargeLed(false);
    }

    private void restartCharger() {
        if (!isCharging && chargeStoped && jrdClient !=null) {
            System.out.println("level  restartCharger");
            jrdClient.commandCharge(JRDClient.CMD_RESTARTCHARGE);
            chargeStoped = false;
            setChargeLed(true);
        }
    }

    private void saveSystemParams() {
        mSystemScreenBrightMode = Settings.System.getInt(mContext.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE, -1);
        if(mSystemScreenBrightMode!=Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            mSystemScreenBrigntness = Settings.System.getInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS, 100);
            MMILog.d(TAG, "system brightness is "+ mSystemScreenBrigntness);
        }
    }

    private void restoreSystemParams() {
        if(mSystemScreenBrightMode==Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, mSystemScreenBrightMode);
        }
        else {
            setLcdBacklight(mSystemScreenBrigntness);
        }
    }

    private void setLcdBacklight(int iBrightness) {
        Settings.System.putInt(mContext.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS, iBrightness);
    }

    private void setChargeLed(boolean on) {
        /*if(mNotificationManager!=null) {
            int id = on ? 1 : 2;
            MMILog.d(TAG, "set notification: id="+id);
            Notification notice = new Notification();
            notice.ledARGB = 0xffffffff;
            notice.flags |= Notification.FLAG_SHOW_LIGHTS;
            notice.ledOnMS = on ? 1000 : 0;
            notice.ledOffMS = on ? 0 : 1000;
            mNotificationManager.notify(id, notice);
        }*/
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        saveSystemParams();
        mContext.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(broadcastReceiver);
        restoreSystemParams();
        restartCharger();
    }
}
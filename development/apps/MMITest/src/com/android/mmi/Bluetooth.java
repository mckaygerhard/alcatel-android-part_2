/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Bluetoth.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.util.ArrayList;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Bluetooth extends TestBase {
    public static final int MESSAGE_BLUETOOTH_DEFAULT = 1024;
    public static final int MESSAGE_BLUETOOTH_RESTART = MESSAGE_BLUETOOTH_DEFAULT + 1;
    public static final int LENGHT_BLUETOOTH_CHECK_TIME = 1000;

    public static enum BluetoothState {
        INIT_WAIT, INIT_FAIL, DETECT_WAIT, DETECT_FAIL, DETECT_SUCCESS;
    }

    private boolean isBluetoothOldState;
    private ArrayList<String> deviceList;
    private ArrayList<String> deviceListWithName;

    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothStateReceiver;
    private BroadcastReceiver bluetoothFindReceiver;
    private IntentFilter bluetoothStateIntentFilter;
    private IntentFilter bluetoothFindIntentFilter;
    private ArrayAdapter<String> deviceAdapter;
    private Handler handler;
    private View.OnClickListener onClickListener;

    private ListView bluetoothListView;
    private TextView messageTextView;
    private Button retryButton;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_bluetooth, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        deviceList = new ArrayList<String>();
        deviceListWithName = new ArrayList<String>();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothStateReceiver = new BluetoothStateReceiver();
        bluetoothFindReceiver = new BluetoothFindReceiver();
        bluetoothStateIntentFilter = new IntentFilter(
                BluetoothAdapter.ACTION_STATE_CHANGED);
        bluetoothFindIntentFilter = new IntentFilter(
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        bluetoothFindIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        deviceAdapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_list_item_1, deviceList);
        handler = new MemberHandler();
        onClickListener = new MemberOnClickListener();

        bluetoothListView = (ListView) findViewById(R.id.bluetooth_main_list);
        bluetoothListView.setAdapter(deviceAdapter);
        messageTextView = (TextView) findViewById(R.id.bluetooth_message);
        retryButton = (Button) findViewById(R.id.bluetooth_btn_retry);
        // retryButton.setOnClickListener(onClickListener);

        saveBluetoothOldState();
        requestBluetoothRestart();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        registerReceivers();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        unregisterReceivers();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        loadBluetoothOldState();
    }

    private void setViewState(BluetoothState state) {
        switch (state) {
        case INIT_WAIT:
            messageTextView.setText(R.string.bluetooth_init_wait);
            retryButton.setEnabled(false);
            setPassButtonEnable(false);
            break;
        case INIT_FAIL:
            messageTextView.setText(R.string.bluetooth_init_fail);
            retryButton.setEnabled(true);
            setPassButtonEnable(false);
            break;
        case DETECT_WAIT:
            messageTextView.setText(R.string.bluetooth_detect_wait);
            retryButton.setEnabled(false);
            setPassButtonEnable(true);
            break;
        case DETECT_FAIL:
            messageTextView.setText(R.string.bluetooth_detect_fail);
            retryButton.setEnabled(true);
            setPassButtonEnable(false);
            break;
        case DETECT_SUCCESS:
            messageTextView.setText(R.string.bluetooth_detect_success);
            retryButton.setEnabled(true);
            setPassButtonEnable(true);
            break;
        }
    }

    private void updateListView(String listItem) {
        deviceList.add(listItem);
        deviceAdapter.notifyDataSetChanged();
    }

    /* *****************************************************************************
     * METHOD restart bluetooth
     * *************************************************
     * **************************
     */
    private void requestBluetoothRestart() {
        setViewState(BluetoothState.INIT_WAIT);
        bluetoothAdapter.disable();
        handler.sendEmptyMessageDelayed(MESSAGE_BLUETOOTH_RESTART,
                LENGHT_BLUETOOTH_CHECK_TIME);
    }

    private void restartBluetooth() {
        deviceList.clear();
        deviceListWithName.clear();
        deviceAdapter.notifyDataSetChanged();
        bluetoothAdapter.enable();
    }

    /* *****************************************************************************
     * METHOD get device state
     * **************************************************
     * *************************
     */
    private boolean isDeviceNothing() {
        return deviceList.size() == 0 ? true : false;
    }

    /* *****************************************************************************
     * METHOD save bluetooth old state
     * ******************************************
     * *********************************
     */
    private void saveBluetoothOldState() {
        isBluetoothOldState = bluetoothAdapter.isEnabled();
    }

    private void loadBluetoothOldState() {
        if (isBluetoothOldState) {
            bluetoothAdapter.enable();
        } else {
            bluetoothAdapter.cancelDiscovery();
            bluetoothAdapter.disable();
        }
    }

    /* *****************************************************************************
     * METHOD register bluetooth receiver
     * ***************************************
     * ************************************
     */
    private void registerReceivers() {
        mContext.registerReceiver(bluetoothStateReceiver, bluetoothStateIntentFilter);
        mContext.registerReceiver(bluetoothFindReceiver, bluetoothFindIntentFilter);
    }

    private void unregisterReceivers() {
        mContext.unregisterReceiver(bluetoothStateReceiver);
        mContext.unregisterReceiver(bluetoothFindReceiver);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_BLUETOOTH_RESTART:
                if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    restartBluetooth();
                } else {
                    handler.sendEmptyMessageDelayed(MESSAGE_BLUETOOTH_RESTART,
                            LENGHT_BLUETOOTH_CHECK_TIME);
                }
                break;
            }
        }
    }

    private class MemberOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
            case R.id.bluetooth_btn_retry:
                requestBluetoothRestart();
                break;
            }
        }
    }

    class BluetoothStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(
                        BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.STATE_OFF);

                switch (state) {
                case BluetoothAdapter.STATE_ON:
                    setViewState(BluetoothState.INIT_WAIT);
                    bluetoothAdapter.startDiscovery();
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                case BluetoothAdapter.STATE_TURNING_OFF:
                case BluetoothAdapter.STATE_OFF:
                    setViewState(BluetoothState.INIT_WAIT);
                    break;
                default:
                    setViewState(BluetoothState.INIT_FAIL);
                    break;
                }
            }
        }
    }

    class BluetoothFindReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                setViewState(BluetoothState.DETECT_WAIT);
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String strDevice = device.getAddress();
                short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,
                        Short.MIN_VALUE);
                if (!deviceListWithName.contains(strDevice)) {
                    updateListView(strDevice + "&" + rssi);
                    deviceListWithName.add(strDevice);
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                    .equals(action)) {
                if (isDeviceNothing()) {
                    setViewState(BluetoothState.DETECT_FAIL);
                } else {
                    setViewState(BluetoothState.DETECT_SUCCESS);
                }
            }
        }
    }

}

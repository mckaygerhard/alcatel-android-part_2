/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Traceability.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.MMILog;
import android.os.SystemProperties;//jeffzhang add

public class Traceability extends TestBase {
    private TraceabilityStruct mTraceabilityStruct;
    private boolean mIsShowedPart_1 = false;

    @Override
    public void run() {
        try {
            mTraceabilityStruct = new TraceabilityStruct();
        } catch (Exception e) {
            MMILog.e(TAG, e.toString());
        }

        if (mTraceabilityStruct != null) {
            showTracabilityPart_1();
            mIsShowedPart_1 = true;
            setPassButtonEnable(true);
        } else {
            mContext.mTextMessage.setText("null");
        }
    }

    @Override
    public void onPassClick() {
        if (mIsShowedPart_1) {
            mIsShowedPart_1 = false;
            showTracabilityPart_2();
        } else {
            super.onPassClick();
        }
    }

    private void showTracabilityPart_2() {
       String displayString = "PT: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_PARA_SYS_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_PARA_SYS_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_PROD_BAIE_PARA_SYS_I))
            + "\n"

            + "PFT1: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_PARA_SYS_2_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_PARA_SYS_2_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_PROD_BAIE_PARA_SYS_2_I))
            + "\n"
            + "PFT4: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_PARA_SYS_3_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_PARA_SYS_3_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_PROD_BAIE_PARA_SYS_3_I))
            + "\n"

            + "WIFI: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_BW_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_BW_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_PROD_BAIE_BW_I))
            + "\n"

            + "BDMMI: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_GPS_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_GPS_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_DATE_BAIE_GPS_I))
            + "\n"

            + "MMI: "
            + Integer
                    .toHexString(mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_STATUS_MMI_TEST_I)[0] & 0x0F)
            + "\n"
            + "FT: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_FINAL_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_FINAL_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_PROD_BAIE_FINAL_I))
          /*+ "\n"
            + "FT2: "
            + Byte.toString(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_STATUS_FINAL_2_I)[0])
            + "-"
            + getHex(mTraceabilityStruct
                    .getItem(TraceabilityStruct.ID.INFO_NBRE_PASS_FINAL_2_I))
            + "-"
            + new String(
                    mTraceabilityStruct
                            .getItem(TraceabilityStruct.ID.INFO_PROD_BAIE_FINAL_2_I))*/

            + "\n"
            + "Date Code: "
            + formatDateCode(mTraceabilityStruct
                .getItem(TraceabilityStruct.ID.INFO_DATE_PASS_HDT_I));

        mContext.mTextMessage.setText(displayString);

    }

    private void showTracabilityPart_1() {
        String displayString = new String();

        displayString = new String(
                mTraceabilityStruct
                .getItem(TraceabilityStruct.ID.DEVICE_IMEI)) + "\n";

        displayString = displayString
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.INDUS_REF_HANDSET_I))
                + "\n"
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.SHORT_CODE_I))
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.ICS_I))
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.SITE_FAC_PCBA_I))
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.LINE_FAC_PCBA_I))
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.DATE_PROD_PCBA_I))
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.SN_PCBA_I))
                + "\n"

                + "PTM: "
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.INFO_PTM_I))
                + "\n"

                + "PTS: "
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.INFO_PTS_MINI_I))
                + "\n"

                + "CU Ref: "
                + new String(
                        mTraceabilityStruct
                                .getItem(TraceabilityStruct.ID.INFO_COMM_REF_I));
        mContext.mTextMessage.setText(displayString);
    }

    private String getHex(byte[] raw) {

        final String HEXES = "0123456789ABCDEF";

        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(
                    HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }

    private String formatDateCode(byte[] raw) {
        String dayString = "**";
        String monthString = "**";
        String yearString = "****";
        int i;

        if (raw.length != 3) {
            MMILog.e(TAG, "date code raw byte array is not 3 length");
            return null;
        }

        String dayRule  = "123456789ABCDEFGHIJKLMNOPQRSTUV";
        String monthRule= "EFGHIJKLMNOP";
        String yearRule = "UVWXYZ6ABCDEFGHIJKLMNOPQ";//"KLMNOPQRSTUVWXYZ";
        // get day value
        i   = dayRule.indexOf(raw[0]);
        if (i>=0) {
            dayString   = String.format("%02d", i+1);
        }
        // get month value
        i   = monthRule.indexOf(raw[1]);
        if (i>=0) {
            monthString = String.format("%02d", i+1);
        }
        // get year value
        i   = yearRule.indexOf(raw[2]);
        if (i>=0) {
            yearString  = String.format("20%02d", i+10);
        }

        return yearString + monthString + dayString;
    }
}

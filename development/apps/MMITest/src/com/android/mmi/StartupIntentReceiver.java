/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/StartupIntentReceiver.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

package com.android.mmi;

import java.io.File;

import com.android.mmi.util.MMILog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemProperties;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.Settings;

public class StartupIntentReceiver extends BroadcastReceiver {

//    private static final String TCT_MMITEST = "tct_mmitest";

    @Override
    public void onReceive(Context context, Intent intent) {

        String runintest_path = getEmmcDir(context) + "/runintest.txt";
        try {
            File flag = new File(runintest_path);
            if (flag.exists()) {
                return;
            }
        } catch (Exception e) {
            MMILog.e("MMITEST Start Up Receiver",
                    "get runintest status file error: " + e.getMessage());
        }

        MMILog.i("MMITEST Start Up Receiver", "Launching MYMMITEST");
        SystemProperties.set("dev.tct.MMITest", "true");
//        Settings.Global.putInt(context.getContentResolver(), TCT_MMITEST, 1);

        Intent starterIntent = new Intent(context, MMITest.class);
        // Set launch flags pertaining to M6 release
        starterIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        starterIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(starterIntent);

    };

    private String getEmmcDir(Context context) {
        StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] storageVolumeList = storageManager.getVolumeList();
        if (storageVolumeList != null) {
            for (StorageVolume volume : storageVolumeList) {
                String path = volume.getPath();
                boolean removeable = volume.isRemovable();
                if (!removeable) {
                    return path;
                }
            }
        }
        return null;
    }
}

/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/TouchPanel.java    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class TouchPanel extends TestBase {
    private final String VOLUME_GESTURE_ENABLE = "volume_gesture_enable";
    private static /*final*/ int MINXYP = 150;
    private final int TOUCH_PANEL_VERTICAL = 0;
    private final int TOUCH_PANEL_HORIZONTAL = 1;
    private final int TOUCH_PANEL_OTHER = 2;
    private int displayWidth;
    private int displayHeight;
    private int mState = TOUCH_PANEL_VERTICAL;
    private int mWidth = 170;
    private int mWidth_other = 170;
    private float mAverageX = 0;
    private float mAverageY = 0;
    private float mLastY;
    private float mLastx;
    public final static int NOT_TESTED = 0;
    public final static int PASSED = 1;
    public final static int FAILED = 2;
    private int Result;
    private MyView mMyView;

    private Point[] hRect1;
    private Point[] hRect2;
    private Point[] hRect3;
    private Point[] vRect1;
    private Point[] vRect2;
    private Point[] vRect3;
    private Point[] vRect_other1;
    private Point[] vRect_other_2;

//    private ContentResolver mContentResolver = null;
//    private int tctMMITest = 0;

    private int mSysProperity_VOLUME_GESTURE;

    @Override
    public void create(CommonActivity a) {
        super.create(a);

        hideNavigationBar();
        DisplayMetrics dm = getRealMetrics();
        displayWidth = dm.widthPixels;
        displayHeight = dm.heightPixels;
        MMILog.d(TAG, "width = "+displayWidth+" height = "+displayHeight);

        mWidth = mWidth_other = displayWidth/7;

        Result = NOT_TESTED;
        hRect1 = new Point[]{ new Point(0, 0),
                new Point(displayWidth, 0),
                new Point(displayWidth, mWidth),
                new Point(0, mWidth), };

        hRect2 = new Point[]{
                new Point(0, (displayHeight - 3 * mWidth) / 4 * 2 + mWidth),
                new Point(displayWidth, (displayHeight - 3 * mWidth) / 4 * 2
                        + mWidth),
                new Point(displayWidth, (displayHeight - 3 * mWidth) / 4 * 2
                        + mWidth * 2),
                new Point(0, (displayHeight - 3 * mWidth) / 4 * 2 + mWidth * 2), };

        hRect3 = new Point[]{
                new Point(0, (displayHeight - mWidth)),
                new Point(displayWidth, (displayHeight - mWidth)),
                new Point(displayWidth, displayHeight),
                new Point(0,displayHeight), };

        vRect1 = new Point[]{ new Point((displayWidth - 3 * mWidth) / 4, 0),
                new Point((displayWidth - 3 * mWidth) / 4, displayHeight),
                new Point((displayWidth - 3 * mWidth) / 4 + mWidth, displayHeight),
                new Point((displayWidth - 3 * mWidth) / 4 + mWidth, 0), };

        vRect2 = new Point[]{
                new Point((displayWidth - 3 * mWidth) / 4 * 2 + mWidth, 0),
                new Point((displayWidth - 3 * mWidth) / 4 * 2 + mWidth,
                        displayHeight),
                new Point((displayWidth - 3 * mWidth) / 4 * 2 + mWidth * 2,
                        displayHeight),
                new Point((displayWidth - 3 * mWidth) / 4 * 2 + mWidth * 2, 0), };

        vRect3 = new Point[]{
                new Point((displayWidth - 3 * mWidth) / 4 * 3 + mWidth * 2, 0),
                new Point((displayWidth - 3 * mWidth) / 4 * 3 + mWidth * 2,
                        displayHeight),
                new Point((displayWidth - 3 * mWidth) / 4 * 3 + mWidth * 3,
                        displayHeight),
                new Point((displayWidth - 3 * mWidth) / 4 * 3 + mWidth * 3, 0), };

        vRect_other1 = new Point[]{ new Point(0, 0),
                new Point(displayWidth - mWidth_other, displayHeight),
                new Point(displayWidth, displayHeight),
                new Point(mWidth_other, 0), };

        vRect_other_2 = new Point[]{ new Point(displayWidth - mWidth_other, 0),
                new Point(0, displayHeight),
                new Point(mWidth_other, displayHeight),
                new Point(displayWidth, 0), };

        mMyView = new MyView(mContext);
        setContentView(mMyView, LAYOUTTYPE.LAYOUT_CUST_WITHOUT_PASSFAIL);

    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
//        mContentResolver = mContext.getContentResolver();
//        tctMMITest = Settings.Global.getInt(mContentResolver, TCT_MMITEST, 0);
        mSysProperity_VOLUME_GESTURE = Settings.System.getInt(mContext.getContentResolver(), VOLUME_GESTURE_ENABLE, 0);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
//        if(tctMMITest == 0) {
//            Settings.Global.putInt(mContentResolver, TCT_MMITEST, 1);
//        }
        if(mSysProperity_VOLUME_GESTURE==1) {
            Settings.System.putInt(mContext.getContentResolver(), VOLUME_GESTURE_ENABLE, 0);
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
//        if(tctMMITest == 0) {
//            Settings.Global.putInt(mContentResolver, TCT_MMITEST, 0);
//        }
        if(mSysProperity_VOLUME_GESTURE==1) {
            Settings.System.putInt(mContext.getContentResolver(), VOLUME_GESTURE_ENABLE, 1);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        if (mMyView != null) {
            mMyView.recycleBitmap();
        }

        resetConfirmed();
    }

    @Override
    public void onPassClick() {
        switch(mState) {
        case TOUCH_PANEL_VERTICAL:
            mState = TOUCH_PANEL_HORIZONTAL;
            reSet();
            break;
        case TOUCH_PANEL_HORIZONTAL:
            super.onPassClick();
            break;
//            mState = TOUCH_PANEL_OTHER;
//            reSet();
//            break;
        default:
            super.onPassClick();
            break;
        }
    }

    public void reSet() {
        Result = NOT_TESTED;
        if (mMyView != null) {
            mMyView.recycleBitmap();
        }
        mMyView = new MyView(mContext);
        setContentView(mMyView, LAYOUTTYPE.LAYOUT_CUST_WITHOUT_PASSFAIL);
    }

    public class MyView extends View {

        private Bitmap mBitmap;
        private Path mPath;
        private Paint mBitmapPaint;
        private Paint mPaint;
        private AlertDialog mAlertDialog, mAlertDialogMsg, mAlertDialogEnd;

        private Parallelepipede pl1;
        private Parallelepipede pl2;
        private Parallelepipede pl3;

        private Parallelepipede pl4;
        private Parallelepipede pl5;

        private int mMinLen = 0;
        private int mLineUnderTest = 0;
        private int mLine1Pass = 0;
        private int mLine2Pass = 0;
        private int mLine3Pass = 0;
        private int mLine4Pass = 0;
        private int mLine5Pass = 0;
        private int mGoodLinesCount = 0;

        public MyView(Context c) {
            super(c);
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setColor(0xFFFF0000);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.BEVEL);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(1);
            if((displayHeight <= 0) || (displayWidth <= 0)){
                displayHeight = 960;
                displayWidth = 540;
            }
            mBitmap = Bitmap.createBitmap(displayWidth, displayHeight,
                    Bitmap.Config.ARGB_8888);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            switch (mState) {
            case TOUCH_PANEL_VERTICAL:
                pl1 = new Parallelepipede(vRect1);
                pl2 = new Parallelepipede(vRect2);
                pl3 = new Parallelepipede(vRect3);
                mMinLen = displayHeight-60;
                break;
            case TOUCH_PANEL_HORIZONTAL:
                pl1 = new Parallelepipede(hRect1);
                pl2 = new Parallelepipede(hRect2);
                pl3 = new Parallelepipede(hRect3);
                mMinLen = displayWidth - 60;
                break;
            case TOUCH_PANEL_OTHER:
                pl4 = new Parallelepipede(vRect_other1);
                pl5 = new Parallelepipede(vRect_other_2);
                mMinLen = (int) Math.sqrt(displayHeight
                        *displayHeight + (displayWidth-mWidth_other)*(displayWidth-mWidth_other));
                mMinLen = mMinLen-30;
                break;

            default:
                break;
            }

            mAlertDialogEnd = new AlertDialog.Builder(mContext)
                    .setCancelable(false)
                    .setTitle("TEST RESULT")
                    .setMessage("Pen out of bounds!")
                    .setPositiveButton("FAIL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    onFailClick();
                                }
                            })
                    .setNegativeButton("RETEST",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    reSet();
                                }
                            }).create();

            mAlertDialogMsg = new AlertDialog.Builder(mContext)
                    .setCancelable(false)
                    .setTitle("TEST RESULT")
                    .setMessage("line is short!")
                    .setNeutralButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    mPath.reset();
                                    invalidate();
                                }
                            }).create();

        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(Color.BLACK);
            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

            /* draw 2 parallelepipede on the screen */
            switch (mState) {
            case TOUCH_PANEL_VERTICAL:
                pl1.draw(canvas);
                pl2.draw(canvas);
                pl3.draw(canvas);
                break;
            case TOUCH_PANEL_HORIZONTAL:
                pl1.draw(canvas);
                pl2.draw(canvas);
                pl3.draw(canvas);
                break;
            case TOUCH_PANEL_OTHER:
                pl4.draw(canvas);
                pl5.draw(canvas);
                break;

            default:
                break;
            }

            /* draw references lines on the screen */
            Paint p = new Paint();
            p.setStyle(Paint.Style.STROKE);
            p.setTextAlign(Paint.Align.CENTER);
            p.setColor(Color.LTGRAY);
            p.setTextSize(40);
            // header text
            canvas.drawText("Please draw in the yellow area", displayWidth / 2,
                    80, p);

            /* draw the current pen position */
            canvas.drawPath(mPath, mPaint);

        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 1;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            if (y < 0 ) {
                y = 0;
            } else if (y > displayHeight) {
                y = displayHeight;
            } else if (x < 0) {
                x = 0;
            } else if (x > displayWidth) {
                x = displayWidth;
            }

            // check if the point is inside the bounds drawn on the screen
            if (x>0 && x<displayWidth && y>0 && y<displayHeight) {
                switch (mState) {
                case TOUCH_PANEL_VERTICAL:
                case TOUCH_PANEL_HORIZONTAL:
                    if (pl1.includePoint(x, y)) {
                        mLineUnderTest = 1;
                    } else if (pl2.includePoint(x, y)) {
                        mLineUnderTest = 2;
                    } else if (pl3.includePoint(x, y)) {
                        mLineUnderTest = 3;
                    } else {
                        Result = FAILED;
                    }
                    break;
                case TOUCH_PANEL_OTHER:
                    if (pl4 != null && pl5 != null) {
                        if (pl4.includePoint(x, y)) {
                            mLineUnderTest = 4;
                        } else if (pl5.includePoint(x, y)) {
                            mLineUnderTest = 5;
                        } else {
                            Result = FAILED;
                        }
                    }
                    break;
                default:
                    break;
                }
            }

            MMILog.d(TAG, "x = " + x + " y = " + y);

            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                mAverageX = x;
                mAverageY = y;
                mLastY = y;
                mLastx = x;
                break;
            case MotionEvent.ACTION_MOVE:
                if (x>0 && x<displayWidth && y>0 && y<displayHeight) {
                    if ((Math.abs(y-mLastY) > MINXYP || Math.abs(x-mLastx) > MINXYP)
                            && mAlertDialogMsg != null) {
                        if (mAlertDialog != null && mAlertDialog.isShowing()) {
                            break;
                        }
                        MMILog.d(TAG, "ERR: dx = "+Math.abs(x-mLastx)+" limit="+MINXYP);
                        MMILog.d(TAG, "ERR: dy = "+Math.abs(y-mLastY)+" limit="+MINXYP);
                        mAlertDialog = mAlertDialogMsg;
                        mAlertDialog.show();
                        break;
                    }
                    touch_move(x, y);
                    invalidate();
                    mAverageX = (x + mAverageX) / 2;
                    mAverageY = (y + mAverageY) / 2;
                    mLastY = y;
                    mLastx = x;
                }
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();

                MMILog.d(TAG, "AVERAGES : x = " + mAverageX + " y = " + mAverageY);

                /* check the length of the path */
                RectF rect = new RectF(0, 0, 0, 0);
                mPath.computeBounds(rect, true);
                float mPathLength = (float) Math.sqrt(rect.height()
                        * rect.height() + rect.width() * rect.width());
                MMILog.d(TAG, "path length is " + mPathLength);
                if (Result == FAILED) {
                    mAlertDialog = mAlertDialogEnd;
                } else if (mPathLength < mMinLen) {
                    mAlertDialog = mAlertDialogMsg;
                } else {
                    switch (mState) {
                    case TOUCH_PANEL_VERTICAL:
                    case TOUCH_PANEL_HORIZONTAL:
                        if (1 == mLineUnderTest) {
                            mLine1Pass = 1;
                        } else if (2 == mLineUnderTest) {
                            mLine2Pass = 1;
                        } else if (3 == mLineUnderTest) {
                            mLine3Pass = 1;
                        }
                        mGoodLinesCount = mLine1Pass + mLine2Pass + mLine3Pass;

                        if (3 == mGoodLinesCount) {
                            mAlertDialog=null;
                            onPassClick();
                        } else {
                            mAlertDialog = null;
                        }
                        break;
                    case TOUCH_PANEL_OTHER:
                        if (4 == mLineUnderTest) {
                            mLine4Pass = 1;
                        } else if (5 == mLineUnderTest) {
                            mLine5Pass = 1;
                        }
                        mGoodLinesCount = mLine4Pass + mLine5Pass;

                        if (2 == mGoodLinesCount) {
                            mAlertDialog = null;
                            onPassClick();
                        } else {
                            mAlertDialog = null;
                        }
                        break;

                    default:
                        break;
                    }
                }

                if (mAlertDialog == null) {
                    // do nothing
                } else if (!mAlertDialog.isShowing()) {
                    mAlertDialog.show();
                }

                break;
            }
            return true;
        }

        public void recycleBitmap() {
          if (mBitmap != null) {
              mBitmap.recycle();
              mBitmap = null;
            }
        }
    }

    class Parallelepipede {
        private Path mPath;
        private Paint mPaint;
        private Point[] points;

        Parallelepipede(Point[] p) {
            points = p.clone();
            mPath = new Path();
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setColor(Color.YELLOW);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setStrokeJoin(Paint.Join.BEVEL);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(1);
        }

        void draw(Canvas c) {
            mPath.reset();
            mPath.moveTo(points[0].x, points[0].y);
            for (int i = 1; i < points.length; i++) {
                mPath.lineTo(points[i].x, points[i].y);
            }
            mPath.close();
            c.drawPath(mPath, mPaint);
        }

        /*
         * checks if the point (x,y) is included in the Parallelepipede
         */

        public boolean includePoint(float x, float y) {
            Point p = new Point((int) x, (int) y);
            double d1 = distLineToPoint(points[0], points[1], p);
            double d2 = distLineToPoint(points[2], points[3], p);
            double range = distLineToPoint(points[0], points[1], points[2]);
            MMILog.d(TAG, "includePoint: " + d1 + " " + d2 + " " + range);
            /*
             * to be included in the shape, the distance from (x,y) to the
             * bottom or top line should not exceed the distance between the
             * bottom to top line
             */
            if (Math.max(d1, d2) < range) {
                return true;
            }
            return false;
        }

        /* computes the shortest distance form a point to a line */

        private double distLineToPoint(Point A, Point B, Point p) {

            /*
             * let [AB] be the segment and C the projection of C on (AB) AC * AB
             * (Cx-Ax)(Bx-Ax) + (Cy-Ay)(By-Ay) u = ------- =
             * ------------------------------- ||AB||^2 ||AB||^2
             */
            double det = Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2);
            if (det == 0) {
                return 0;
            }

            double u = ((p.x - A.x) * (B.x - A.x) + (p.y - A.y) * (B.y - A.y))
                    / det;

            /*
             * The projection point P can then be found: Px = Ax + r(Bx-Ax) Py =
             * Ay + r(By-Ay)
             */
            double Px = A.x + u * (B.x - A.x);
            double Py = A.y + u * (B.y - A.y);

            // Log.d(TAG,"distLineToPoint : u="+u+" Px=" +Px +" Py=" +Py);

            /* the distance to (AB) is the the [Pp] segment length */

            double distance = Math.sqrt(Math.pow(Px - p.x, 2)
                    + Math.pow(Py - p.y, 2));

            return distance;
        }

    }/* Parallelepipede */
}

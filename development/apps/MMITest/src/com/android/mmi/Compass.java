
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Compass.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/15/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.android.mmi.util.CompassView;
import com.android.mmi.util.CompassDialog;

public class Compass extends TestBase implements SensorEventListener {
    private SensorManager mSensorManager;
    private CompassView mSensorView;
    private Sensor aSensor=null;
    private Sensor mSensor=null;
    private Sensor oSensor=null;
    public static final int TYPE_ORIENTATION = 3;
    float[] accelerometerValues=new float[3];
    float[] magneticFieldValues=new float[3];
    public float[] values=new float[3];
    float[] rotate=new float[9];
    private TextView mAngleTextView;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        this.setContentView(R.layout.test_compass, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mAngleTextView = (TextView) findViewById(R.id.angleText);
        mSensorView = (CompassView) findViewById(R.id.sensorview);
        mSensorView.setCompass(this);
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        aSensor=mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        oSensor=mSensorManager.getDefaultSensor(TYPE_ORIENTATION);
        mSensor=mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Dialog dialog = new CompassDialog(mContext);
        dialog.show();

        if(aSensor != null && mSensor != null && oSensor != null){
            setPassButtonEnable(true);
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mSensorManager.registerListener(this, aSensor, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, oSensor, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        mSensorManager.unregisterListener(this, aSensor);
        mSensorManager.unregisterListener(this, oSensor);
        mSensorManager.unregisterListener(this, mSensor);
        super.destroy();
    }

    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub

        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            accelerometerValues=event.values;
        }
        else if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD){
            magneticFieldValues=event.values;
        }
        SensorManager.getRotationMatrix(rotate, null, accelerometerValues, magneticFieldValues);
        SensorManager.getOrientation(rotate, values);
        //经过SensorManager.getOrientation(rotate, values);得到的values值为弧度
        //转换为角度
        values[0]=(float)Math.toDegrees(values[0]);
        String angle = "";
        if (values[0] < 0) {
            angle = String.format("Angle: %.1f", -values[0]);
        } else {
            angle = String.format("Angle: -%.1f", values[0]);
        }

        mAngleTextView.setText(angle);
        if (mSensorView != null) {
            mSensorView.invalidate();
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }
}

/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/activity/compass/  */
/*                                                          CameraDialog.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for MMITest        */
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

package com.android.mmi.camera;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.R;

// NOTICE #Raw_CameraDialog
public class CameraDialog extends Dialog {

    private String waringTitle;
    private String cameraType;
    private int nv;

    public CameraDialog(Context context, String waringTitle, String cameraType,
            int nv) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.test_camera_dialog);
        this.waringTitle = waringTitle;
        this.cameraType = cameraType;
        this.nv = nv;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView txtVwTitle = (TextView) findViewById(R.id.txtVwTitle);
        TextView msgTxtVw = (TextView) findViewById(R.id.txtViewMsg);
        Button btn = (Button) findViewById(R.id.camera_dialog_btn);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dismiss();
            }
        });
        txtVwTitle.setText(waringTitle);
        msgTxtVw.setText(cameraType + "\n" + "NV95: " + nv);
    }
}

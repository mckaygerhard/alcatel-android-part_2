/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/camera/ui/         */
/*                                            FocusIndicatorRotateLayout.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.camera.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.android.mmi.R;

public class FocusIndicatorRotateLayout extends RotateLayout {

    private int mState;
    private static final int STATE_IDLE = 0;
    private static final int STATE_FOCUSING = 1;
    private static final int STATE_FINISHING = 2;

    private Runnable mDisappear = new Disappear();
    private Runnable mEndAction = new EndAction();
    private static final int SCALING_UP_TIME = 1000;
    private static final int SCALING_DOWN_TIME = 200;
    private static final int DISAPPEAR_TIMEOUT = 200;

    public FocusIndicatorRotateLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void setDrawable(int resid) {
        mChild.setBackgroundDrawable(getResources().getDrawable(resid));
    }

    public void showStart() {
        // TODO Auto-generated method stub
        System.out.println("=========  showStart  mState = "+mState);
        if (mState == STATE_IDLE) {
            setDrawable(R.drawable.ic_focus_focusing);
            animate().withLayer().setDuration(SCALING_UP_TIME)
                    .scaleX(1.5f).scaleY(1.5f);
            mState = STATE_FOCUSING;
        }
    }

    public void showSuccess(boolean timeout) {
        // TODO Auto-generated method stub
        System.out.println("=========  showSuccess  mState = "+mState);
        if (mState == STATE_FOCUSING) {
            setDrawable(R.drawable.ic_focus_focused);
            animate().withLayer().setDuration(SCALING_DOWN_TIME).scaleX(1f)
                    .scaleY(1f).withEndAction(timeout ? mEndAction : null);
            mState = STATE_FINISHING;
        }
    }

    public void showFail(boolean timeout) {
        // TODO Auto-generated method stub
        if (mState == STATE_FOCUSING) {
            setDrawable(R.drawable.ic_focus_failed);
            animate().withLayer().setDuration(SCALING_DOWN_TIME).scaleX(1f)
                    .scaleY(1f).withEndAction(timeout ? mEndAction : null);
            mState = STATE_FINISHING;
        }
    }

    public void clear() {
        // TODO Auto-generated method stub
        System.out.println("=========  clear  mState = "+mState);
        animate().cancel();
        removeCallbacks(mDisappear);
        mDisappear.run();
        setScaleX(1f);
        setScaleY(1f);
    }
    private class EndAction implements Runnable {
        @Override
        public void run() {
            // Keep the focus indicator for some time.
            postDelayed(mDisappear, DISAPPEAR_TIMEOUT);
        }
    }

    private class Disappear implements Runnable {
        @Override
        public void run() {
            mChild.setBackgroundDrawable(null);
            mState = STATE_IDLE;
        }
    }
}

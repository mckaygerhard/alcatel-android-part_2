package com.android.mmi.camera;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import com.android.mmi.R;
import com.android.mmi.util.MMILog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnLayoutChangeListener;

public class CameraTextureView implements TextureView.SurfaceTextureListener {
    private static final String TAG = "MMITest.CameraTextureView";
    private int mPreviewWidth = 0;
    private int mPreviewHeight = 0;
    private Matrix mMatrix = null;
    private float mAspectRatio = 4f / 3f;
    private boolean mAspectRatioResize;
    private SurfaceTexture mSurfaceTexture;
    private TextureView mTextureView;
    private Context mContext;

    private final static int MESSAGE_FAIL = 1000;
    private final static int LENGTH_TIMEOUT = 5 * 1000; // 5 seconds
    private boolean previewSuccess = false;
    private int try_count = 0;
    private Handler mHandler = new MemberHandler();

    private boolean mOrientationResize;
    private boolean mPrevOrientationResize;
    private final Object mSurfaceTextureLock = new Object();

    private TextureViewListener mTextureViewListener;
    private Camera mCamera;
    private Camera.Parameters mParams;
    private int mCameraId;
    private CameraInfo mCameraInfo;
    private android.hardware.Camera.AutoFocusMoveCallback mAutoFocusMoveCallback;

    public CameraTextureView(Context context, TextureView textureView,
            int cameraId, CameraInfo info) {
        mContext = context;
        mTextureView = textureView;
        mCameraId = cameraId;
        mCameraInfo = info;
        textureView.setSurfaceTextureListener(this);
        textureView.addOnLayoutChangeListener(mLayoutListener);

        mOrientationResize = false;
        mPrevOrientationResize = false;
    }

    public interface TextureViewListener {
        public void setButton(int timeout);

        public void setText(int resId);
    }

    public void setTextureViewListener(TextureViewListener listener) {
        mTextureViewListener = listener;
    }

    private OnLayoutChangeListener mLayoutListener = new OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right,
                int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            int width = right - left;
            int height = bottom - top;
            if (mPreviewWidth != width || mPreviewHeight != height
                    || (mOrientationResize != mPrevOrientationResize)
                    || mAspectRatioResize) {
                mPreviewWidth = width;
                mPreviewHeight = height;
                setTransformMatrix(width, height);
                mAspectRatioResize = false;
            }
        }
    };

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
            int height) {
        synchronized (mSurfaceTextureLock) {
            MMILog.i(TAG, "SurfaceTexture ready.");
            mSurfaceTexture = surface;
            try {
                doOpenCamera();
                doStartPreview();
            } catch (Exception e) {
                // TODO: handle exception
                if (mTextureViewListener != null) {
                    mTextureViewListener.setText(R.string.cam_fail);
                }
            }

            if (mPreviewWidth != 0 && mPreviewHeight != 0) {
                // Re-apply transform matrix for new surface texture
                setTransformMatrix(mPreviewWidth, mPreviewHeight);
            }
        }
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        synchronized (mSurfaceTextureLock) {
            MMILog.i(TAG, "SurfaceTexture destroyed");
            mSurfaceTexture = null;
            return true;
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
            int height) {
        // Ignored, Camera does all the work for us
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        // TODO Auto-generated method stub
        MMILog.i(TAG, "SurfaceTexture updated");
        if(!previewSuccess){
            mHandler.removeMessages(MESSAGE_FAIL);
            previewSuccess = true;
        }
    }

    private void setTransformMatrix(int width, int height) {
        mMatrix = mTextureView.getTransform(mMatrix);
        float scaleX = 1f, scaleY = 1f;
        float scaledTextureWidth, scaledTextureHeight;
        if (mOrientationResize) {
            scaledTextureWidth = height * mAspectRatio;
            if (scaledTextureWidth > width) {
                scaledTextureWidth = width;
                scaledTextureHeight = scaledTextureWidth / mAspectRatio;
            } else {
                scaledTextureHeight = height;
            }
        } else {
            if (width > height) {
                scaledTextureWidth = Math.max(width, (height * mAspectRatio));
                scaledTextureHeight = Math.max(height, (width / mAspectRatio));
            } else {
                scaledTextureWidth = Math.max(width, (height / mAspectRatio));
                scaledTextureHeight = Math.max(height, (width * mAspectRatio));
            }
        }

        scaleX = scaledTextureWidth / width;
        scaleY = scaledTextureHeight / height;
        mMatrix.setScale(scaleX, scaleY, (float) width / 2, (float) height / 2);
        mTextureView.setTransform(mMatrix);

        // Calculate the new preview rectangle.
        RectF previewRect = new RectF(0, 0, width, height);
        mMatrix.mapRect(previewRect);
    }

    //HAL1 version code
    private static final int CAMERA_HAL_API_VERSION_1_0 = 0x100;

    private void doOpenCamera() {
        int timeout = 100;

        try {
            Method openMethod = Class.forName("android.hardware.Camera").getMethod(
                    "openLegacy", int.class, int.class);
            mCamera = (android.hardware.Camera) openMethod.invoke(
                    null, mCameraId, CAMERA_HAL_API_VERSION_1_0);

            timeout = 3000;

        } catch (Exception e) {
            /* Retry with open if openLegacy doesn't exist/fails */
            MMILog.i(TAG, "openLegacy failed due to " + e.getMessage()
                    + ", using open instead");
            try {
                mCamera = android.hardware.Camera.open(mCameraId);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                if (mCamera != null) {
                    mCamera.release();
                }
                mCamera = null;
                e1.printStackTrace();
            }
        }

        if (mTextureViewListener != null) {
            mTextureViewListener.setButton(timeout);
        }
    }

    private void doStartPreview() {
        if (mCamera == null) {
            return;
        }

        String rtp = SystemProperties.get("ro.tct.product").trim();
        mParams = mCamera.getParameters();
        setDisplayOrientation();

        List <String> zslModeList = mParams.getSupportedZSLModes();
        for(String zslMode:zslModeList) {
            MMILog.i(TAG, "zsl mode: "+zslMode);
            if(zslMode.equals(Parameters.ZSL_ON)) {
                MMILog.i(TAG, "set ZSL mode on");
                mParams.setZSLMode(Parameters.ZSL_ON);
            }
        }

        Size optimalSize = null;
        List<Size> list = mCamera.getParameters().getSupportedPreviewSizes();
        if (list != null) {
            optimalSize = getOptimalPreviewSize(list, mPreviewWidth, mPreviewHeight);
        }

        MMILog.i(TAG, "====>" + optimalSize.width + " " + optimalSize.height + " "
                + mPreviewWidth + " " + mPreviewHeight);

        try {
            mCamera.setPreviewTexture(mSurfaceTexture);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "Could not set preview texture", e);
        }
        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            if ((rtp != null)
                    && ((rtp.equals("rio4g")) || (rtp.equals("rio4g_orange")) || (rtp
                            .equals("rio4g_tf")))) {
                mParams.set("tintless", "disable");
            }
        } else if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mParams.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            if (mAutoFocusMoveCallback != null) {
                mCamera.setAutoFocusMoveCallback(mAutoFocusMoveCallback);
            }
            // mCamera.setAutoFocusMoveCallback(null);
        }
        // set noise mode
        List<String> denoiseModes = mParams.getSupportedDenoiseModes();
        if(denoiseModes!=null) {
            if(denoiseModes.contains(Parameters.DENOISE_ON)) {
                MMILog.d(TAG, "set DENOISE_ON");
                mParams.setDenoise(Parameters.DENOISE_ON);
            }
        }
        mParams.setPreviewSize(optimalSize.width, optimalSize.height);
        mParams.setAntibanding(Parameters.ANTIBANDING_50HZ);
        mCamera.setParameters(mParams);
        resizeForPreviewAspectRatio(mParams);
        mCamera.startPreview();

        if(try_count == 0){
            mHandler.sendEmptyMessageDelayed(MESSAGE_FAIL, LENGTH_TIMEOUT);
        }
    }

    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    private void setAspectRatio(float ratio) {
        if (ratio <= 0.0)
            throw new IllegalArgumentException();

        if (mOrientationResize
                && ((Activity) mContext).getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
            ratio = 1 / ratio;
        }

        MMILog.d(TAG, "setAspectRatio() ratio[" + ratio + "] mAspectRatio["
                + mAspectRatio + "]");
        mAspectRatio = ratio;
        mAspectRatioResize = true;
        mTextureView.requestLayout();
    }

    private void cameraOrientationPreviewResize(boolean orientation) {
        mPrevOrientationResize = mOrientationResize;
        mOrientationResize = orientation;
    }

    private void setPreviewFrameLayoutCameraOrientation() {
        // if camera mount angle is 0 or 180, we want to resize preview
        if (mCameraInfo.orientation % 180 == 0) {
            cameraOrientationPreviewResize(true);
        } else {
            cameraOrientationPreviewResize(false);
        }
    }

    private void resizeForPreviewAspectRatio(Camera.Parameters params) {
        setPreviewFrameLayoutCameraOrientation();
        Size size = params.getPreviewSize();
        MMILog.d(TAG, "Width = " + size.width + "Height = " + size.height);
        setAspectRatio((float) size.width / size.height);
    }

    private static int getDisplayRotation(Context context) {
        int rotation = ((Activity) context).getWindowManager()
                .getDefaultDisplay().getRotation();
        switch (rotation) {
        case Surface.ROTATION_0:
            return 0;
        case Surface.ROTATION_90:
            return 90;
        case Surface.ROTATION_180:
            return 180;
        case Surface.ROTATION_270:
            return 270;
        }
        return 0;
    }

    private static int getDisplayOrientation(int degrees, int cameraId) {
        // See android.hardware.Camera.setDisplayOrientation for
        // documentation.
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    private void setDisplayOrientation() {
        int mDisplayRotation = getDisplayRotation(mContext);
        int mDisplayOrientation = getDisplayOrientation(mDisplayRotation,
                mCameraId);
        int mCameraDisplayOrientation = mDisplayOrientation;
        // Change the camera display orientation
        mCamera.setDisplayOrientation(mCameraDisplayOrientation);
    }

    public void setPause() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public void setDestroy() {
        if (mCamera != null) {
            // mCamera.setPreviewCallback(null);
            if (mCamera.previewEnabled()) {
                mCamera.stopPreview();
                mCamera.release();
            }
            // mHolder.removeCallback(this);
            // mHolder=null;
            // mParams=null;
            mCamera = null;
            MMILog.i(TAG, "camera has been destroyed");
        } else {
            MMILog.i(TAG, "camera is null");
            return;
        }
    }

    public void setAutoFocusMoveCallback(
            android.hardware.Camera.AutoFocusMoveCallback autoFocusMoveCallback) {
        mAutoFocusMoveCallback = autoFocusMoveCallback;
    }

    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
            case MESSAGE_FAIL: //restart
                if(try_count >= 1){
                    return;
                }

                MMILog.e(TAG, "preview fail, restart one time...");

                try_count ++;

                if (mCamera != null) {
                    mCamera.stopPreview();
                    mCamera.release();
                    mCamera = null;
                }

                try {
                    doOpenCamera();
                    doStartPreview();
                } catch (Exception e) {
                    // TODO: handle exception
                    if (mTextureViewListener != null) {
                        mTextureViewListener.setText(R.string.cam_fail);
                    }
                }

                break;
            default:
                break;
            }
        }
    }
}

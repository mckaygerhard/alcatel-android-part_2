/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.mmi.util;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import libcore.io.IoUtils;

public class ExternStorageTest {
    private String TAG = "MMITest.ExternStorageTest";
    private static final byte VALUE_TEST_BYTE = 9;
    private boolean isReadDataSame;
    private String mPath;

    public ExternStorageTest(String path) {
        mPath = path;
        MMILog.i(TAG, "extern storage path = "+path);
    }

    public boolean startTest() {
        isReadDataSame = false;
        //if(checkStorageState()) {
            return startFileWriteReadTest();
        //}
        //return false;
    }

    private boolean checkStorageState() {
        String state = Environment.getExternalStorageState(new File(mPath));
        MMILog.i(TAG, "storage state is " + state);
        if( state.equals(Environment.MEDIA_MOUNTED) ) {
            return true;
        }
        return false;
    }

    private boolean startFileWriteReadTest() {
        String filePath = mPath+"/test.txt";
        File externalFile = null;
        FileInputStream externalFileInputStream = null;
        FileOutputStream externalFileOutputStream = null;

        try {
            externalFile = setExternalFileCreate(filePath);
            externalFileOutputStream = setExternalFileWrite(externalFile);
            MMILog.d(TAG, "write");
            externalFileInputStream = setExternalFileRead(externalFile);
            MMILog.d(TAG, "read");
            externalFile = setExternalFileDelete(externalFile);
        } catch (FileNotFoundException e) {
            MMILog.e(TAG, e + ", externalFile: " + externalFile);
            return false;
        } catch (IOException e) {
            MMILog.e(TAG, e.toString());
            return false;
        } catch (Exception e) {
            MMILog.e(TAG, e.toString());
            return false;
        } finally {
            IoUtils.closeQuietly(externalFileInputStream);
            IoUtils.closeQuietly(externalFileOutputStream);
        }

        return isReadDataSame;
    }

    private File setExternalFileCreate(String path) throws IOException {
        File externalFile = new File(path);

        if (!externalFile.exists()) {
            MMILog.d(TAG, "before create!");
            externalFile.createNewFile();
            MMILog.d(TAG, "end create!");
        }
        if (externalFile.exists()) {
            MMILog.d(TAG, "create sicess!");
        }else
            MMILog.d(TAG, "create fail!");
        return externalFile;
    }

    private File setExternalFileDelete(File externalFile) throws IOException {
        if (externalFile.exists()) {
            externalFile.delete();
        }
        return externalFile;
    }

    private FileOutputStream setExternalFileWrite(File externalFile)
            throws FileNotFoundException, IOException {
        FileOutputStream externalFileOutputStream;
        externalFileOutputStream = new FileOutputStream(externalFile);
        externalFileOutputStream.write(VALUE_TEST_BYTE);
        externalFileOutputStream.close();
        return externalFileOutputStream;
    }

    private FileInputStream setExternalFileRead(File externalFile)
            throws FileNotFoundException, IOException {
        FileInputStream externalFileInputStream;
        externalFileInputStream = new FileInputStream(externalFile);
        isReadDataSame = (externalFileInputStream.read() == VALUE_TEST_BYTE);
        externalFileInputStream.close();
        return externalFileInputStream;
    }
}

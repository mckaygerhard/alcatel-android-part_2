/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/TestItemManager.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.os.SystemProperties;

import com.android.mmi.R;

public class TestItemManager {
    private static String TAG = "MMITest.TestItemManager";
    private static int ITEM_INFO_NUM = 3;
    private static List<HashMap<String, Object>> mMaxTestItemsList = null;
    private static String mCurrentTestClassName = "MainScreen";
    private static String mCurrentTestTitle = "ALCATEL MOBILE PHONES";
    private static String mProduct = null;
    public static final int DEF_TYPE_AUTO1 = 0x1;
    public static final int DEF_TYPE_MANU = 0x2;
    public static final int DEF_TYPE_AUTO2 = 0x4;

    private static String mBSN = "";
    private static boolean mIsCIT = false;
    private static List<HashMap<String, Object>> mCITCustomizedList = null;

    public static void setCurrentItem(String className, String title) {
        mCurrentTestClassName = className;
        mCurrentTestTitle = title;
    }

    public static String getCurrentItemClass() {
        return mCurrentTestClassName;
    }

    public static String getCurrentITemTitle() {
        return mCurrentTestTitle;
    }

    public static String setBSN(String bsn) {
        return mBSN = bsn;
    }

    public static String getBSN() {
        return mBSN;
    }

    public static void setCIT(boolean bCITTest) {
        mIsCIT = bCITTest;
    }

    public static boolean IsCIT() {
        return mIsCIT;
    }

    public static String getProduct() {
        String defName = "unknown";
        if(mProduct==null) {
            mProduct = com.tct.feature.Global.TCT_TARGET_MMITEST_PRODUCT;

            if("notdef".equals(mProduct)){
                mProduct = SystemProperties.get("ro.tct.product", defName).trim();
                if(mProduct.equals(defName)) {
                    mProduct = SystemProperties.get("ro.product.name", defName);
                }
                if(mProduct.equals("msm8953_64")) {
                    mProduct = "london";
                }
            }
            MMILog.d(TAG, "Product Name: "+mProduct);
        }
        return mProduct;
    }

    public static boolean InitStaticInfo(Context context) {
        getProduct();
        return LoadStaticTestItems(context, mProduct);
    }

    public static void resetStaticVariables() {
        mCurrentTestClassName = "MainScreen";
        mCurrentTestTitle = "ALCATEL MOBILE PHONES";
    }

    private static boolean LoadStaticTestItems(Context context, String product) {
        if(mMaxTestItemsList==null) {
            MMILog.d(TAG, "Load Static TestItems");
            mMaxTestItemsList = new ArrayList<HashMap<String, Object>>();
            String []itemInfo;
            String []maxItemsArray;

            maxItemsArray = context.getResources().getStringArray(R.array.max_test_items);

            for(String item : maxItemsArray) {
                itemInfo = item.split(",");
                if(itemInfo.length != ITEM_INFO_NUM){
                    MMILog.d(TAG, "LoadStaticTestItems:Invalid fromat string: "+item);
                    mMaxTestItemsList = null;
                    return false;
                }

                HashMap<String, Object> itemHash = new HashMap<String, Object>();
                itemHash.put("title", itemInfo[0].trim());
                itemHash.put("defclass", itemInfo[1].trim());
                itemHash.put("deftype", itemInfo[2].trim());

                mMaxTestItemsList.add(itemHash);
            }

            for(int i=0; i<mMaxTestItemsList.size(); i++) {
                MMILog.d(TAG, "item "+i+": "+
                        GetTestItemInfoString(i, "title") + "," +
                        GetTestItemInfoString(i, "defclass") + "," +
                        GetTestItemInfoString(i, "deftype"));
            }

            if(CustomizeProductTestItems(context, product)) {
                CustomizeNVTestItems(95);
//                CustomizeNVTestItems(3);
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }

    private static boolean CustomizeProductTestItems(Context context, String product) {
        int productItemId = context.getResources().getIdentifier(product, "array", context.getPackageName());
        if(productItemId!=0) {
            String []itemInfo;
            String title, defclass, deftype;
            String []itemsArray = context.getResources().getStringArray(productItemId);
            for(String item : itemsArray) {
                itemInfo = item.split(",");
                if(itemInfo.length!= ITEM_INFO_NUM) {
                    MMILog.d(TAG, "CustomizeProductTestItems:Invalid format string: "+item);
                    return false;
                }
                title = itemInfo[0].trim();
                defclass = itemInfo[1].trim();
                deftype = itemInfo[2].trim();
                UpdateStaticTestItem(title, defclass, deftype);
                MMILog.d(TAG, "Product Customization item : "+
                        title + "," +
                        defclass + "," +
                        deftype);
            }
        }
        else{
            MMILog.d(TAG, "no product("+product+") customized info found.");
        }

        return true;
    }

    private static boolean CustomizeNVTestItems(int nv) {
        JRDClient jrdClient = new JRDClient();
        byte[] result = jrdClient.readNv(nv);

        if(result != null && result.length>0) {
            int idx;
            String title;
            String deftype = "0";
            MMILog.d(TAG, "NV"+nv+" = "+result[0]);
            for(idx =0; idx<mMaxTestItemsList.size(); idx++) {
                title = (String)mMaxTestItemsList.get(idx).get("title");
                // customize NFC/HALL/Compass
                if(nv==95) {
                    if(title.equals("Hall") && (result[0] & 0x1) == 0x1
                        || title.equals("NFC") && (result[0] & 0x2) == 0x2
                        || title.equals("E-Compass") && (result[0] & 0x4) == 0x4){
                       MMILog.d(TAG, "NV95 Customization item : remove "+title);
                       UpdateStaticTestItem(title, null, deftype);
                    }
                }
                // customize area
                else if(nv==3) {
                    if( getProduct().equalsIgnoreCase("alto4") ) {
                        //                  emea    latam
                        // light sensor     no      yes
                        // p-sensor         no      yes
                        // sub-mic          yes     no
                        if((result[0]==1 || result[0]==23) // emea
                                && (title.equalsIgnoreCase("Light Sensor")
                                || title.equals("Proximity Sensor")) ) {
                            MMILog.d(TAG, "NV3 Customization item : remove "+title);
                            UpdateStaticTestItem(title, null, deftype);
                        }
                        else if((result[0]==6 || result[0]==24)   // latam
                                && title.equals("Audio") ) {
                            MMILog.d(TAG, "NV3 Customization item : change Audio_with_sub_mic to Audio");
                            UpdateStaticTestItem(title, "Audio", null);
                        }
                    }
                }
            }
            return true;
        }
        else {
            return false;
        }
    }

    private static void UpdateStaticTestItem(String title, String defclass, String deftype) {
        int idx;
        HashMap<String, Object> itemHash;
        for(idx =0; idx<mMaxTestItemsList.size(); idx++) {
            itemHash = mMaxTestItemsList.get(idx);
            if( itemHash.get("title").equals(title)){
                if(defclass!=null) {
                    itemHash.put("defclass", defclass);
                }
                if(deftype!=null) {
                    itemHash.put("deftype", deftype);
                }
                break;
            }
        }
        if(idx>=mMaxTestItemsList.size()){
            MMILog.d(TAG, "UpdateStaticTestItem:Fail to update static test items: title="+title);
        }
    }

    public static int GetTestItemInfoIntFromClass(String className) {
        HashMap<String, Object> itemHash;
        for(int idx =0; idx<mMaxTestItemsList.size(); idx++) {
            itemHash = mMaxTestItemsList.get(idx);
            if( itemHash.get("defclass").equals(className)){
                try{
                    return Integer.parseInt((String)itemHash.get("deftype"));
                }
                catch(NumberFormatException e){
                    MMILog.d(TAG, "Invalid integer value, class="+className+" pos="+idx);
                }
            }
        }
        return 0;
    }


    public static HashMap<String, Object> getTestItem(int position) {
        return mMaxTestItemsList.get(position);
    }

    public static String GetTestItemInfoString(int position, String key) {
        if(position<mMaxTestItemsList.size()) {
            return (String)mMaxTestItemsList.get(position).get(key);
        }
        else{
            return "null";
        }
    }

    public static int GetTestItemInfoInt(int position, String key) {
        int value = 0;
        if(position<mMaxTestItemsList.size()) {
            try{
                value = Integer.parseInt((String)mMaxTestItemsList.get(position).get(key));
            }
            catch(NumberFormatException e){
                MMILog.d(TAG, "Invalid integer value, key="+key+" pos="+position);
            }
        }

        return value;
    }

    public static HashMap<String, Object> GetTestItemInfoFromTitle(String deftitle) {
        HashMap<String, Object> itemHash;
        for(int idx =0; idx<mMaxTestItemsList.size(); idx++) {
            itemHash = mMaxTestItemsList.get(idx);

            String title = (String) itemHash.get("title");
            if(title.equals(deftitle)){
                return itemHash;
            }
        }

        return null;
    }

    public static boolean setCurrentTestItem(int currentIdx) {
        if(currentIdx<getTestItemCount()) {
            mCurrentTestClassName = GetTestItemInfoString(currentIdx,"defclass");
            mCurrentTestTitle = GetTestItemInfoString(currentIdx,"title");
            return true;
        }
        else {
            return false;
        }
    }

    public static int getTestItemCount() {
        return mMaxTestItemsList.size();
    }

    public static int getPhoneMode() {
        JRDClient jrdClient = new JRDClient();
        byte[] nv = jrdClient.readNv(453);

        int result = -1;
        if(nv != null && nv.length>0) {
            result = nv[0];
        }
        return result;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    // CIT
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public static int GetCITCustomizationInfoInt(String title, String key) {
        int idx;
        HashMap<String, Object> itemHash;
        if(mCITCustomizedList==null) {
            return -1;
        }
        for(idx =0; idx<mCITCustomizedList.size(); idx++) {
            itemHash = mCITCustomizedList.get(idx);
            if( itemHash.get("title").equals(title)){
                try{
                    return Integer.parseInt((String)itemHash.get(key));
                }
                catch(NumberFormatException e){
                    MMILog.d(TAG, "Invalid integer value, "+"title="+title+"key="+key);
                }
            }
        }
        return -1;
    }

    public static boolean LoadStaticCITCustomization(Context context) {
        if(mCITCustomizedList==null) {
            MMILog.d(TAG, "Load Static CIT Customized TestItems");
            mCITCustomizedList = new ArrayList<HashMap<String, Object>>();
            String []itemInfo;
            String []maxItemsArray = context.getResources().getStringArray(R.array.cit);
            for(String item : maxItemsArray) {
                itemInfo = item.split(",");
                if(itemInfo.length != ITEM_INFO_NUM){
                    MMILog.d(TAG, "LoadStaticCITCustomization fromat string: "+item);
                    mCITCustomizedList = null;
                    return false;
                }
                HashMap<String, Object> itemHash = new HashMap<String, Object>();
                itemHash.put("title", itemInfo[0].trim());
                itemHash.put("defclass", itemInfo[1].trim());
                itemHash.put("deftype", itemInfo[2].trim());
                mCITCustomizedList.add(itemHash);
                MMILog.d(TAG, "CIT Customization item : "+
                        itemInfo[0].trim() + "," +
                        itemInfo[1].trim() + "," +
                        itemInfo[2].trim());
            }
        }

        return true;
    }
}

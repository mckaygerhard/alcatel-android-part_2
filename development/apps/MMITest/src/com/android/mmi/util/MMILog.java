package com.android.mmi.util;

import android.util.Log;

/**
 * @hide
 */
public final class MMILog {

    private MMILog() {
    }

    public static int d(String tag, String msg) {
        return Log.d(tag, msg);
    }

    public static int i(String tag, String msg) {
        return Log.i(tag, msg);
    }

    public static int w(String tag, String msg) {
        return Log.w(tag, msg);
    }

    public static int e(String tag, String msg) {
        return Log.e(tag, msg);
    }

    public static int d(String tag, String msg, Throwable tr) {
        return d(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int i(String tag, String msg, Throwable tr) {
        return i(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int w(String tag, String msg, Throwable tr) {
        return w(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int w(String tag, Throwable tr) {
        return w(tag, Log.getStackTraceString(tr));
    }

    public static int e(String tag, String msg, Throwable tr) {
        return e(tag, msg + '\n' + Log.getStackTraceString(tr));
    }
}

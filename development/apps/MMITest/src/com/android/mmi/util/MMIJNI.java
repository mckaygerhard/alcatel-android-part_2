package com.android.mmi.util;

public class MMIJNI {

    static {
        // The runtime will add "lib" on the front and ".o" on the end of
        // the name supplied to loadLibrary.
        System.loadLibrary("mmijni");
    }

    public static native int getOemfuseStatus();
}

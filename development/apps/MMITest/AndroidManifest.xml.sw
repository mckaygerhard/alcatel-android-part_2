<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.android.mmi"
    android:sharedUserId="android.uid.system"
    android:versionCode="1"
    android:versionName="1.0" >

    <uses-sdk
        android:minSdkVersion="18"
        android:targetSdkVersion="23" />
    <!-- ================================================================================ -->
    <!-- uses-permisson -->
    <!-- ================================================================================ -->
    <uses-permission android:name="android.permission.VIBRATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_OWNER_DATA" />
    <uses-permission android:name="android.permission.READ_OWNER_DATA" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.DEVICE_POWER" />
    <uses-permission android:name="android.permission.WRITE_SETTINGS" />
    <uses-permission android:name="android.permission.READ_SETTINGS" />
    <uses-permission android:name="android.permission.READ_CONTACTS" />
    <uses-permission android:name="android.permission.WRITE_CONTACTS" />
    <uses-permission android:name="android.permission.HARDWARE_TEST" />
    <uses-permission android:name="android.permission.WRITE_APN_SETTINGS" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
    <uses-permission android:name="android.permission.FLASHLIGHT" />
    <uses-permission android:name="android.permission.READ_ERS" />
    <uses-permission android:name="android.permission.WRITE_ERS" />
    <uses-permission android:name="android.permission.MASTER_CLEAR" />
    <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_LOCATION_EXTRA_COMMANDS" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS" />
    <uses-permission android:name="com.android.providers.syncml.permission.READ_SYNCML_PROFILE" />
    <uses-permission android:name="com.android.providers.syncml.permission.WRITE_SYNCML_PROFILE" />
    <uses-permission android:name="android.permission.DISABLE_KEYGUARD" />
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.NFC" />

    <uses-feature android:name="android.hardware.camera" />
    <uses-feature android:name="android.hardware.camera.autofocus" />

    <uses-permission android:name="android.permission.GET_TASKS" />
    <uses-permission android:name="android.permission.CALL_PHONE" />
    <uses-permission android:name="android.permission.WRITE_SECURE_SETTINGS" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.FORCE_STOP_PACKAGES" />
    <uses-permission android:name="android.permission.SHUTDOWN" />

    <permission android:name="android.permission.mmi.service" android:protectionLevel="normal" ></permission>

    <!-- ================================================================================ -->
    <!-- uses-permisson -->
    <!-- ================================================================================ -->

    <application
        android:allowBackup="true"
        android:icon="@drawable/ic_launcher"
        android:label="@string/app_name"
        android:theme="@style/Theme.Holo.NoTitleBar.Fullscreen"
        android:screenOrientation="portrait" >

        <activity
            android:name=".MMITest"
            android:configChanges="keyboardHidden|mcc|mnc|locale|layoutDirection"
            android:label="@string/app_name"
            android:screenOrientation="portrait" >
            <intent-filter >
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>
        <activity
            android:name=".CIT"
            android:configChanges="keyboardHidden|mcc|mnc|locale|layoutDirection"
            android:screenOrientation="portrait" >
        </activity>
        <activity
            android:name=".CommonActivity"
            android:configChanges="keyboardHidden|mcc|mnc|locale|layoutDirection"
            android:screenOrientation="portrait" >
            <intent-filter>
                <action android:name="com.tct.intent.action.HOVER" />
            </intent-filter>
        </activity>
        <receiver android:name=".PhoneCallReceiver">
            <intent-filter>
                <action android:name="android.provider.Telephony.SECRET_CODE" />
                <data android:scheme="android_secret_code" android:host="20111028" />
            </intent-filter>
        </receiver>
        <service
            android:name="com.android.mmi.service.mmiService"
            android:permission="android.permission.mmi.service"
            android:exported="true" >
                <intent-filter>
                        <action android:name="android.intent.action.mmiservice" />
                </intent-filter>
        </service>
    </application>

</manifest>

/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                        GPSSensorTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.util.TctLog;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class GPSSensorTest extends Test implements GpsStatus.Listener/* LocationListener */{

    TestLayout1 tl;
    String mDisplayString;

    private LocationManager mLocationManager;
    private LocationProvider mLocationProvider;
    private String TAG = "GPS";
    private int mStatusCount = 0, mLocationCount = 0;

    private double mLatitude = 0;
    private double mLongitude = 0;
    // private Location mLocation=null;
    private GpsStatus mGpsStatus = null;

    private boolean mPRN1Found = false;

    private boolean debug = false;

    Iterator<GpsSatellite> mSatArrayIterator;

    List<GpsSatellite> mSatArray = new ArrayList<GpsSatellite>();

    private LocationListener mLocationListener = new LocationListener() {

        public void onLocationChanged(Location location) {
            TctLog.i(TAG,
                    "LocationListener: location update: "
                            + location.getLatitude() + ","
                            + location.getLongitude());
            mLocationCount++;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            TctLog.i(TAG, "LocationListener: status changed to " + status);
        }

        public void onProviderDisabled(String provider) {
            TctLog.i(TAG, "LocationListener : provider disabled: " + provider);
            Toast.makeText(mContext, "GPS provider disabled",
                    Toast.LENGTH_SHORT).show();
        }

        public void onProviderEnabled(String provider) {
            TctLog.i(TAG, "LocationListener : provider re-enabled: " + provider);
            Toast.makeText(mContext, "GPS provider enabled", Toast.LENGTH_SHORT)
                    .show();
        }
    };

    GPSSensorTest(ID pid, String s) {
        super(pid, s);
        TAG = Test.TAG + TAG;

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_D) {
                    debug = true;
                    return true;
                }
                return false;
            }
        };
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT: // init the test, shows the first screen
            mStatusCount = 0;
            mLocationCount = 0;

            if (!Settings.Secure
                    .isLocationProviderEnabled(mContext.getContentResolver(),
                            LocationManager.GPS_PROVIDER)) {
                Settings.Secure.setLocationProviderEnabled(
                        mContext.getContentResolver(),
                        LocationManager.GPS_PROVIDER, true);
            }

            mLocationManager = (LocationManager) mContext
                    .getSystemService(Context.LOCATION_SERVICE);

            if (mLocationManager != null) {
                TctLog.d(TAG,
                        "enabled providers : "
                                + mLocationManager.getProviders(true));

                mLocationProvider = mLocationManager
                        .getProvider(LocationManager.GPS_PROVIDER);
                if (mLocationProvider != null) {
                    TctLog.i(TAG, "provider name: " + mLocationProvider.getName());

                    // NormalLocationManager.addGpsStatusListener
                    // (NormalGpsStatusListener);
                    mLocationManager.addGpsStatusListener(this);
                } else {
                    TctLog.w(TAG, "gps provider doesn't exist");
                }
            }

            tl = new TestLayout1(mContext, mName, "GPS Initializing...");
            tl.hideButtons();
            mContext.setContentView(tl.ll);

            mLocationManager.requestLocationUpdates(
                    mLocationProvider.getName(), 0, 0, mLocationListener);
            /* now register to GPS updates */

            StopTimer();
            SetTimer(500, new CallBack() {
                public void c() {
                    mState++;
                    Run();
                }
            });

            break;

        case TIMEOUT:

            tl = new TestLayout1(mContext, mName,
                    "GPS Initialize failed:\n no satellite found");
            // tl.setAutoTestButtons(true,false);
            mContext.setContentView(tl.ll);

            mState = END;

        case END://
            StopTimer();
            mLocationManager.removeGpsStatusListener(this);
            mLocationManager.removeUpdates(mLocationListener);
            mLocationManager = null;

            break;

        default:
            String sDisplay;
            int numSatellites = 0;
            String SatellitesPRNs = "";
            String SatelliteSNRs = "";

            /* first pass : set TIMEOUT */
            // if( MMITest.mode == MMITest.AUTO_MODE && mState == INIT+1) {
            // StopTimer();
            // SetTimer(60000, new CallBack() {
            // public void c(){
            // mState = TIMEOUT;
            // Run();
            // }
            // });
            //
            // }

            if (mGpsStatus != null) {
                while (mSatArrayIterator.hasNext()) {
                    numSatellites++;
                    GpsSatellite mGpsSatellite = mSatArrayIterator.next();
                    int PRN = mGpsSatellite.getPrn();
                    float SNR = mGpsSatellite.getSnr();
                    if (PRN == 1) {
                        mPRN1Found = true;
                    }
                    SatellitesPRNs += Integer.toString(PRN)
                            + (mSatArrayIterator.hasNext() ? "/" : " ");
                    SatelliteSNRs += Float.toString(SNR)
                            + (mSatArrayIterator.hasNext() ? "/" : " ");
                }

            }

            sDisplay = numSatellites
                    + " satellite(s) "
                    + "\n"
                    + (numSatellites > 0 ? "PRN=" + SatellitesPRNs
                            + "tracked\n" + "SNR=" + SatelliteSNRs
                            + "tracked\n" : "\n") + "status updates = "
                    + mStatusCount;
            TctLog.i(TAG, sDisplay);

            tl = new TestLayout1(mContext, mName, sDisplay);
            // if(numSatellites > 0){
            // tl.setAutoTestButtons(false, true); //pass only.
            // }else{
            // tl.setAutoTestButtons(true, false);
            // }

            mContext.setContentView(tl.ll);
            mState++;

            // if ( (numSatellites>0) && MMITest.mode == MMITest.AUTO_MODE ) {
            // mState = END;
            // }

            break;
        }

    }

    public void onGpsStatusChanged(int event) {

        switch (event) {
        case GpsStatus.GPS_EVENT_STARTED:
            if (debug)
                Toast.makeText(mContext, "GPS_EVENT_STARTED",
                        Toast.LENGTH_SHORT).show();
            break;
        case GpsStatus.GPS_EVENT_STOPPED:
            if (debug)
                Toast.makeText(mContext, "GPS_EVENT_STOPPED",
                        Toast.LENGTH_SHORT).show();
            break;
        case GpsStatus.GPS_EVENT_FIRST_FIX:
            if (debug)
                Toast.makeText(mContext, "GPS_EVENT_FIRST_FIX",
                        Toast.LENGTH_SHORT).show();
            break;
        case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
            if (debug)
                Toast.makeText(
                        mContext,
                        "gps event sat status " + mStatusCount + "\n"
                                + "maxsat  =" + mGpsStatus.getMaxSatellites()
                                + "\n" + "timefix ="
                                + mGpsStatus.getTimeToFirstFix() + "\n",
                        Toast.LENGTH_SHORT).show();
            mStatusCount++;
            if (mGpsStatus == null)
                mGpsStatus = mLocationManager.getGpsStatus(null);
            else
                mLocationManager.getGpsStatus(mGpsStatus);
            mSatArrayIterator = mGpsStatus.getSatellites().iterator();
            mStatusCount++;
            break;
        }
        Run();

    }
}

/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                             WIFITest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.util.List;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.text.TextUtils;

/*
 * WIFI Test use as a default when no test was defined
 *
 */

class WIFITest extends Test {

    private WifiManager mWifiManager;

    private IntentFilter mIntentFilter;

    private String networkList;

    /** String present in capabilities if the scan result is ad-hoc */
    private static final String ADHOC_CAPABILITY = "[IBSS]";

    private TestLayout1 tl;

    WIFITest(ID pid, String s) {
        super(pid, s);
    }

    WIFITest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
    }

    private boolean WIFIinit() {

        mWifiManager = (WifiManager) mContext
                .getSystemService(Context.WIFI_SERVICE);
        if (null == mWifiManager) {
            return false;
        }
        if (mWifiManager.isWifiEnabled() == false) {
            mWifiManager.setWifiEnabled(true);
            /* Fix venus bug 141706 by Jiang Dong */
            // SystemClock.sleep(5000);
        }
        return true;
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:
            tl = new TestLayout1(mContext, mName, "WIFI initializing.....");
            // tl.setAutoTestButtons(false);

            mContext.setContentView(tl.ll);
            SetTimer(500, new CallBack() {
                public void c() {
                    mState++;
                    Run();
                }
            });

            break;
        case INIT + 1: // init wifi and show status.
            if (!WIFIinit()) {
                TestLayout1 tl_failed = new TestLayout1(mContext, mName,
                        "WIFI initialize fail.");
                // tl.setAutoTestButtons(true,false);
                mContext.setContentView(tl_failed.ll);
                Result = Test.FAILED;
            } else {
                TestLayout1 tl_succeed = new TestLayout1(mContext, mName,
                        "Detecting WIFI network...");
                // tl.setAutoTestButtons(true,false);
                mContext.setContentView(tl_succeed.ll);
                mIntentFilter = new IntentFilter();
                mIntentFilter
                        .addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
                mContext.registerReceiver(mReceiver, mIntentFilter);
                mWifiManager.startScan();
                StartTimeout();
            }
            break;
        case END://
            mContext.unregisterReceiver(mReceiver);
            mWifiManager.setWifiEnabled(false);
            StopTimer();
            break;
        }

    }

    private void StartTimeout() {
        SetTimer(15000/* miliseconds */, TimeoutCb);
    }

    CallBack TimeoutCb = new CallBack() {
        public void c() {
            TestLayout1 tl_succeed = new TestLayout1(mContext, mName,
                    "No network detected.");
            // tl.setAutoTestButtons(true,false);
            mContext.setContentView(tl_succeed.ll);
            Stop();
        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                handleScanResultsAvailable();
            }
        }
    };

    private void handleScanResultsAvailable() {
        List<ScanResult> list = mWifiManager.getScanResults();
        if (list != null) {
            for (int i = list.size() - 1; i >= 0; i--) {
                final ScanResult scanResult = list.get(i);

                if (scanResult == null) {
                    continue;
                }
                /*
                 * Ignore adhoc, enterprise-secured, or hidden networks. Hidden
                 * networks show up with empty SSID.
                 */
                if (WIFITest.isAdhoc(scanResult)
                        || TextUtils.isEmpty(scanResult.SSID)) {
                    continue;
                }

                final String ssid = WIFITest
                        .convertToQuotedString(scanResult.SSID); /*
                                                                  * get the
                                                                  * network name
                                                                  * of Wifi hot
                                                                  * point
                                                                  */
                final int sigLevel = scanResult.level; /*
                                                        * get the detected
                                                        * signal of network in
                                                        * dbm
                                                        */

                /*
                 * add SSID and signal level into network list and display on
                 * screen
                 */
                if (null == networkList) {
                    StopTimer();/* succeed to find a network */
                    networkList = ssid;
                    networkList = networkList + "," + sigLevel;
                    networkList += "\n";
                } else {
                    if ((networkList.indexOf(ssid)) < 0) {
                        networkList += ssid;
                        networkList = networkList + "," + sigLevel;
                        networkList += "\n";
                    }
                }
                TestLayout1 tl_networkFound = new TestLayout1(mContext, mName,
                        networkList);
                mContext.setContentView(tl_networkFound.ll);
                Result = Test.PASSED;
            }
            if (Result == Test.PASSED) {
                StopTimer();
            }
        }
    }

    public static String convertToQuotedString(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }

        final int lastPos = string.length() - 1;
        if (lastPos < 0
                || (string.charAt(0) == '"' && string.charAt(lastPos) == '"')) {
            return string;
        }

        return "\"" + string + "\"";
    }

    public static boolean isAdhoc(ScanResult scanResult) {
        return scanResult.capabilities.contains(ADHOC_CAPABILITY);
    }
}

package com.EngineeringMode;
import android.provider.Settings;
import android.provider.Settings.System.*;
import android.app.Activity;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.View;
import android.view.KeyEvent;
import android.os.SystemProperties;
import android.util.Log;
public class UaConfirm extends Activity implements RadioGroup.OnCheckedChangeListener {
    private RadioButton rdo0;
    private RadioButton rdo1;
    private RadioButton rdo2;
    private RadioButton rdo3;
    private RadioButton rdo4;
    private RadioButton rdo5;

    private RadioGroup rdogroup;


    private String ua_name;
    private String ua_name1;
    private String ua_name2;
    private String ua_name3;
    private String ua_name4;
    private String ua_name5;

    private String ua_value;
    private String ua_value1;
    private String ua_value2;
    private String ua_value3;
    private String ua_value4;
    private String ua_value5;

    private int checked_id;


    private void initResourceRefs() {
        rdo0 = (RadioButton) findViewById(R.id.ua_provider0);
        rdo1 = (RadioButton) findViewById(R.id.ua_provider1);
        rdo2 = (RadioButton) findViewById(R.id.ua_provider2);
        rdo3 = (RadioButton) findViewById(R.id.ua_provider3);
        rdo4 = (RadioButton) findViewById(R.id.ua_provider4);
        rdo5 = (RadioButton) findViewById(R.id.ua_provider5);
        rdogroup = (RadioGroup) findViewById(R.id.ua_selects_radiogroup);

    }

    private void updateUi() {
//        String ua = Settings.System.getString(getContentResolver(),Settings.System.TCT_UA_CONFIRM);
//        ua_name= getResources().getString(tct.R.string.def_tctfw_webkit_useragent_name);
//        ua_value = getResources().getString(tct.R.string.def_tctfw_webkit_useragent_value);
//        if (!"".equals(ua_value)) {
//            rdo0.setText(ua_name);
//            rdo0.setVisibility(View.VISIBLE);
//        } else {
            rdo0.setVisibility(View.GONE);
//        }
//
//        ua_name1 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent1_name);
//        ua_value1 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent1_value);
//        if (!"".equals(ua_value1)) {
//            rdo1.setText(ua_name1);
//            rdo1.setVisibility(View.VISIBLE);
//        } else {
            rdo1.setVisibility(View.GONE);
//        }
//        ua_name2 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent2_name);
//        ua_value2 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent2_value);
//        if (!"".equals(ua_value2)) {
//            rdo2.setText(ua_name2);
//            rdo2.setVisibility(View.VISIBLE);
//        } else {
            rdo2.setVisibility(View.GONE);
//        }
//        ua_name3 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent3_name);
//        ua_value3 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent3_value);
//        if (!"".equals(ua_value3)) {
//            rdo3.setText(ua_name3);
//            rdo3.setVisibility(View.VISIBLE);
//        } else {
            rdo3.setVisibility(View.GONE);
//        }
//        ua_name4 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent4_name);
//        ua_value4 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent4_value);
//        if (!"".equals(ua_value4)) {
//            rdo4.setText(ua_name4);
//            rdo4.setVisibility(View.VISIBLE);
//        } else {
            rdo4.setVisibility(View.GONE);
//        }
//        ua_name5 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent5_name);
//        ua_value5 = getResources().getString(tct.R.string.def_tctfw_webkit_useragent5_value);
//        if (!"".equals(ua_value5)) {
//            rdo5.setText(ua_name5);
//            rdo5.setVisibility(View.VISIBLE);
//        } else {
            rdo5.setVisibility(View.GONE);
//        }
//
//        if(null != ua)
//        {
//            if(getResources().getBoolean(tct.R.bool.feature_tctfw_webkit_useragent_on)){
//                Log.d("ENGINEERINGMODE","useragent is ON");
//                SystemProperties.set("persist.sys.mmiua","true");
//            }
//            if (ua.equals(ua_value)) {
//                rdo0.setChecked(true);
//            }
//            if (ua.equals(ua_value1)) {
//                rdo1.setChecked(true);
//            }
//            if (ua.equals(ua_value2)) {
//                rdo2.setChecked(true);
//            }
//            if (ua.equals(ua_value3)) {
//                rdo3.setChecked(true);
//            }
//            if (ua.equals(ua_value4)) {
//                rdo4.setChecked(true);
//            }
//            if (ua.equals(ua_value5)) {
//                rdo5.setChecked(true);
//            }
//        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pre_ua_provider);
        initResourceRefs();
        updateUi();
        rdogroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void onCheckedChanged(RadioGroup group, int checkedId) {/*
       if(checkedId==R.id.ua_provider0){
              Settings.System.putString(getContentResolver(), Settings.System.TCT_UA_CONFIRM,ua_value);
              SystemProperties.set("persist.sys.uaname",ua_value);
       }else if(checkedId==R.id.ua_provider1){
              Settings.System.putString(getContentResolver(), Settings.System.TCT_UA_CONFIRM,ua_value1);
              SystemProperties.set("persist.sys.uaname",ua_value1);
       }else if(checkedId==R.id.ua_provider2){
              Settings.System.putString(getContentResolver(), Settings.System.TCT_UA_CONFIRM,ua_value2);
              SystemProperties.set("persist.sys.uaname",ua_value2);
       }else if(checkedId==R.id.ua_provider3){
              Settings.System.putString(getContentResolver(), Settings.System.TCT_UA_CONFIRM,ua_value3);
              SystemProperties.set("persist.sys.uaname",ua_value3);
       }else if(checkedId==R.id.ua_provider4){
              Settings.System.putString(getContentResolver(), Settings.System.TCT_UA_CONFIRM,ua_value4);
              SystemProperties.set("persist.sys.uaname",ua_value4);
       }else if(checkedId==R.id.ua_provider5){
             Settings.System.putString(getContentResolver(), Settings.System.TCT_UA_CONFIRM,ua_value5);
             SystemProperties.set("persist.sys.uaname",ua_value5);
       }
    */}
}


/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                          ExecuteTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.app.Activity;
//import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.TctLog;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
//import android.widget.TableLayout;
import android.widget.TextView;
import android.content.res.Configuration;

public class ExecuteTest extends Activity {

    static Test currentTest = null; // points to the test currently executed

    String TAG = "ExecuteTest";

    private final boolean DEBUG = false;

    private static int lockCount = 0;

    public static void lock() {
        if (lockCount == 0)
            lockCount++;
        else
            while (lockCount != 0)
                ;
        /*
         * try { wait(); }catch ( InterruptedException e ) { }
         */

    }

    public static void unlock() {
        if (lockCount > 0)
            lockCount--;
        if (lockCount == 0)
            ;// notify();
    }

    private void LOGD(String s) {
        if (DEBUG) {
            TctLog.d(TAG, s);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set screen appearance
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        int mask = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        mask |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        getWindow().setFlags(mask, mask);

        currentTest.Create(this);

    }

    /* fix venus_bug [148624],jiangdong ,start */
    // workaround, by wang.jian.
    // CallBack Cb = new CallBack() {
    // public void c() {
    // //NOTE: may interrupt some test-item, such as camera.
    // //let Activity TEST can capture HOME key.
    // currentTest.mContext.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);

    // execute the current test
    // currentTest.Start();
    // }
    // };

    /* fix venus_bug [148624],jiangdong ,end */

    @Override
    protected void onStart() {
        super.onStart();
        if (currentTest.mName == "KEYPAD")
            currentTest.Start();
        LOGD("onStart");
    }

    @Override
    public void onResume() {
        super.onResume();

        /* fix venus_bug [148624],jiangdong ,start */
        // currentTest.Resume();
        // if(currentTest.mName!="KEYPAD")currentTest.SetTimer(100, Cb);
        // //miliseconds
        currentTest.Start();
        /* fix venus_bug [148624],jiangdong ,end */
        // LOGD("onResume");
    }

    @Override
    public void onPause() {
        super.onPause();

        currentTest.Pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        // currentTest.Stop(); no idle timeout!

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TctLog.d(TAG, "onDestroy() currentTest = " + currentTest);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Retrieve Data from Event
        boolean result = true;
        final int eventAction = event.getAction();
        if (eventAction == MotionEvent.ACTION_UP) {
        } else if (eventAction == MotionEvent.ACTION_DOWN) {
        }
        // if there is a handler for the event in the test
        if (currentTest.hTouch != null) {
            if (currentTest.hTouch.handleTouch(event))
                return true;
        }
        return result;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        currentTest.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // in case we want to filter keys, return true to indicate the event was
        // consumed
        int keycode = event.getKeyCode();
        // if (MMITest.mgMode == MMITest.MANU_MODE){
        // MANUAL MODE DEFAULT KEY BEHAVIOR
        if (event.getEventTime() - event.getDownTime() > 500) {// is it a long
                                                               // press?
            TctLog.i(currentTest.toString(),
                    Long.toString(event.getEventTime() - event.getDownTime())
                            + " repeat:"
                            + Integer.toString(event.getRepeatCount()));
            // long press just manages some exit keys
            switch (keycode) {
            case KeyEvent.KEYCODE_BACK:
                break;

            case KeyEvent.KEYCODE_ENDCALL:
                break;
            case KeyEvent.KEYCODE_HOME:
                finish();// always end the test if long press on HOME
                break;
            default:
                break;
            }
            return true;// always handle long press on these events
        } else {
            // if there is a handler for the event in the test
            if (currentTest.hKey != null)
                if (currentTest.hKey.handleKey(event))
                    return true;

            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                return super.onKeyDown(keycode, event);
            } else if (keycode == KeyEvent.KEYCODE_HOME) {
                currentTest.Exit();
                return true;// after all we don't want any other app to handle
                            // it!
            }
        }
        // //manual mode end.
        //
        // }else if (MMITest.mgMode == MMITest.AUTO_MODE && KeyEvent.ACTION_UP
        // == event.getAction() ){
        // // AUTO MODE DEFAULT KEY BEHAVIOR
        // if (event.getEventTime() - event.getDownTime() > 500){// is it a long
        // press?
        // TctLog.i(currentTest.toString(), Long.toString(event.getEventTime() -
        // event.getDownTime()) +
        // " repeat:"+Integer.toString(event.getRepeatCount()));
        // // long press just manages some exit keys
        // switch (keycode) {
        // case KeyEvent.KEYCODE_BACK:
        // break;
        // case KeyEvent.KEYCODE_ENDCALL:
        // break;
        // case KeyEvent.KEYCODE_HOME:
        // onTestInterrupted();
        // break;
        // default:
        // break;
        // }
        // return true;// always handle long press on these events
        // }else{
        // // if there is a handler for the event in the test
        // if (currentTest.hKey != null)
        // if (currentTest.hKey.handleKey(event))
        // return true;
        //
        // // in auto mode we can set some default key behaviors
        // switch (keycode) {
        // case KeyEvent.KEYCODE_BACK:
        // //currentTest.Start();// retest
        // break;
        // case KeyEvent.KEYCODE_MENU:
        // //currentTest.Exit();// operator can't skip test
        // break;
        // case KeyEvent.KEYCODE_ENDCALL:
        // break;
        // case KeyEvent.KEYCODE_HOME:
        // //currentTest.Exit();// operator can't skip test
        // break;
        // default:
        // break;
        // }
        // return true;
        // }
        // }// AUTO MODE
        return true;
    }

    private void onTestInterrupted() {

        TestLayout1 tl;

        TctLog.i(currentTest.toString(), "test " + currentTest.toString()
                + "interrupted!!\n");

        // first stop the test here to stop all timers
        currentTest.Stop();

        View.OnClickListener BuFail = new View.OnClickListener() {
            public void onClick(View v) {
                currentTest.Result = Test.FAILED;
                currentTest.Exit();
            }
        };

        View.OnClickListener BuRestart = new View.OnClickListener() {
            public void onClick(View v) {
                currentTest.Start();
            }
        };

        View.OnClickListener BuPass = new View.OnClickListener() {
            public void onClick(View v) {
                currentTest.Result = Test.PASSED;
                currentTest.Exit();
            }
        };

        // if(MMITest.debugmode)
        // tl = new TestLayout1(this, AutoTest.TAG,"Test interrupted!\n",
        // "PASS", "RESTART", "FAIL", BuPass, BuRestart, BuFail);
        // else
        // tl = new TestLayout1(this, AutoTest.TAG,"Test interrupted!\n",
        // "RESTART", "FAIL", BuRestart, BuFail);

        // setContentView(tl.ll);
    }

}

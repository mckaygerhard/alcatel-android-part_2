/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                      BatteryTempTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.util.TctLog;

class BackLight {

    private final String TAG = "MMITest:BackLight";
    // static private FileWriter LcdBacklightFile, KbdBacklightFile;
    private FileOutputStream LcdBacklightFile, KbdBacklightFile;

    @SuppressWarnings("hiding")
    BackLight() {

        try {
            LcdBacklightFile = new FileOutputStream(
                    "/sys/class/leds/lcd-backlight/brightness");
            KbdBacklightFile = new FileOutputStream(
                    "/sys/class/leds/keyboard-backlight/brightness");
        } catch (FileNotFoundException fnfe) {
            TctLog.d(TAG, "Specified file not found" + fnfe);
        } catch (IOException ioe) {
            TctLog.d(TAG, "cannot write file " + ioe);
        }

    }

    private void setBacklight(int value, FileOutputStream file) {
        try {
            file.write(Integer.toString(value).getBytes());
            TctLog.d(TAG, "write " + value + " to " + file.toString());
        } catch (IOException ioe) {
            TctLog.d(TAG, "Error while writing file" + ioe);
        }

    }

    public void setKbdBacklight(int value) {
        if (KbdBacklightFile != null) {
            setBacklight(value, KbdBacklightFile);
        } else {
            TctLog.d(TAG, "KbdBacklightFile is null");
        }
    }

    public void setLcdBacklight(int value) {
        if (LcdBacklightFile != null) {
            setBacklight(value, LcdBacklightFile);
        } else {
            TctLog.d(TAG, "LcdBacklightFile is null");
        }
    }

}

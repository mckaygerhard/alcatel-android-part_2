/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                         EngineerMode.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/* 08/10/12| Qianbo.Pan     |                    | change antenna test        */
/* 30/09/13| xinjian.fang   |                    |  Add UA function           */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

package com.EngineeringMode;

import static android.provider.Settings.System.*;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TctLog;

import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ListView;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class EngineerMode extends ListActivity {

    String TAG = "EngineerMode";

    int MINI_AUTOTEST_MAX_ITEMS = 32;

    private Test[] EMList;

    /**
     * avoid static init, as traceability test link to lib qcNVItems and
     * qcNVHook, which are not included in user-release.
     */
    private void initListEM() {
        final Test[] List = {
                new Version(Test.ID.VERSION, "Version"),
                new EmptyTest(Test.ID.AUTOANSWER, "Auto Answer"),
                new LcdTest(Test.ID.LCD_MIRERGB, "LCD"),
                new LightTest(Test.ID.BACKLIGHT, "LCD BACKLIGHT"),
                new KeyBLTest(Test.ID.KBD_BACKLIGHT, "KEY BACKLIGHT"),
                new EmptyTest(Test.ID.TOUCHWINDOW_TEST, "Touch Window Test"),
                new TouchWindowLineTest(Test.ID.TOUCH_LINE, "Touch Panel"),
                new EmptyTest(Test.ID.ANTENNA, "Antenna Test"),
                new EmptyTest(Test.ID.CAMERA_PREVIEW, "Camera Preview"),
                new EmptyTest(Test.ID.VIDEO_CAMERA, "Video Camera Preview"),
                new EmptyTest(Test.ID.LEDTORCH, "FlashLED Test"),
                new KeypadTest(Test.ID.KEYPAD, "KEYPAD"),
                new EmptyTest(Test.ID.BATTERY_INFO, "Battery Info"),
                new EmptyTest(Test.ID.EXTREMEVIBRATE, "Vibrate Test"),
                new GSensorTest(Test.ID.GSENSOR, "GSENSOR"),
                new CompassTest(Test.ID.COMPASS, "COMPASS"),
                new LightSensorTest(Test.ID.LIGHTSENSOR, "Light_SENSOR"),
                new ProximitySensorTest(Test.ID.PSENSOR, "Proximity_SENSOR"),
//                new FmTest(Test.ID.FM, "FM Radio"),
                new EmptyTest(Test.ID.NMEAGPS, "AGPS Test"),
                new BluetoothTest(Test.ID.BT, "Bluetooth"),
                new WIFITest(Test.ID.WIFI, "WIFI"),
                new MasterClearConfirm(Test.ID.FACTORY_RESET,
                        "Factory data reset") ,
                new EmptyTest(Test.ID.UACONFIRM, "UA Confirm"),
                new EmptyTest(Test.ID.SETTINGS,"Settings")//add by dongdong.wang at 2015-12-11 to fix  FR 1061001
        };  //[BUGFIX]-Add-BEGIN by TCTNB.(xinjian.fang),09/30/2013, PR-514176,add UA function

        EMList = List;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Lcd.setSize(getWindowManager().getDefaultDisplay().getWidth(),
                getWindowManager().getDefaultDisplay().getHeight());

        initListEM();

        // set screen appearance
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        int mask = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        mask |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        getWindow().setFlags(mask, mask);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Use an existing ListAdapter that will map an array
        // of strings to TextViews
        setListAdapter(new ArrayAdapter<Test>(this,
                android.R.layout.simple_list_item_1, EMList));
        getListView().setTextFilterEnabled(true);

    }

    @Override
    protected void onListItemClick(ListView lv, View v, int position, long id) {

        Test currentTest = EMList[position];
        if (currentTest.getId() == Test.ID.TS_CALIBRATION.ordinal()) {
            Intent Execution = new Intent(Intent.ACTION_MAIN, null);
            Execution.setClassName("touchscreen.test",
                    "touchscreen.test.CalibrationTest");
            startActivity(Execution);
        //[BUGFIX]-Add-BEGIN by TCTNB.(xinjian.fang),09/30/2013, PR-514176,add UA function
        }else if (currentTest.getId() == Test.ID.UACONFIRM.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.UaConfirm");
            startActivity(intent);
        }//[BUGFIX]-Add-END  by TCTNJ.(xinjian.fang)
        //add by dongdong.wang  at 2015-12-11 to fix  task  1133932 begin
        else if (currentTest.getId()==Test.ID.SETTINGS.ordinal()){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this,"com.EngineeringMode.HardwareSettings");
            startActivity(intent);
        }//add by dongdong.wang at 2015-12-11 to fix task  1133932 end
        else if (currentTest.getId() == Test.ID.TOUCH_CALIBRATION.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.TSCalibration");
            startActivity(intent);
        }

        else if (currentTest.getId() == Test.ID.LEDTORCH.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.LEDTorch");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.EXTREMEVIBRATE.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.EmVibratorTest");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.EMERGENCY_CALL.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_CALL_EMERGENCY);
            intent.setData(android.net.Uri.fromParts("tel", "112", null));
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.AUTOANSWER.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this,
                    "com.EngineeringMode.AutoAnswerPreferenceActivity");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.TOUCHWINDOW_TEST.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.TouchWindowTest");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.CAMERA_PREVIEW.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.gallery3d",
                    "com.android.camera.CameraLauncher");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.VIDEO_CAMERA.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.gallery3d",
                    "com.android.camera.VideoCamera");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.BATTERY_INFO.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.settings",
                    "com.android.settings.BatteryInfo");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.NMEAGPS.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.NmeaGps");
            startActivity(intent);
        } else if (currentTest.getId() == Test.ID.ANTENNA.ordinal()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(this, "com.EngineeringMode.AntennaTest");
            startActivity(intent);
        } else if (currentTest.getId() < Test.ID.MAX_ITEMS.ordinal()) {
            // Normal case
            TctLog.i(TAG, "start test " + EMList[position].toString());
            ExecuteTest.currentTest = currentTest;

            Intent Execution = new Intent(Intent.ACTION_MAIN, null);
            Execution.setClassName(this, "com.EngineeringMode.ExecuteTest");
            startActivity(Execution);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }

}

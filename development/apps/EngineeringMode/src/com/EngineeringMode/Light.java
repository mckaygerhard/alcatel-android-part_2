/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                            LightTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.os.IPowerManager;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.TctLog;

class LightTest extends Test {
    // TODO : values are copied from Hardware Service, check they are in sync!
    static final int LIGHT_ID_BACKLIGHT = 0;
    static final int LIGHT_ID_KEYBOARD = 1;

    static final int BRIGHTNESS_OFF = 0;
    static final int BRIGHTNESS_ON = 255;

    final int version = 2;
    private int ii;

    private BackLight bl = new BackLight();
    // private BackLightJNI bl = new BackLightJNI();
    private String mBlName;
    private TestLayout1 tl;

    LightTest(ID pid, String s) {
        this(pid, s, 0);
    }

    LightTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);

        if (mId == Test.ID.BACKLIGHT.ordinal()) {
            mBlName = "screen";
        } else {
            mBlName = "keyboard";
        }

    }

    private void setBacklight(int brightness) {
        //try {
            IPowerManager power = IPowerManager.Stub.asInterface(ServiceManager
                    .getService("power"));
            if (power != null) {
                //power.setBacklightBrightness(brightness);
                TctLog.i(TAG,
                        "set brightness to :"
                                + new Integer(brightness).toString());
            }
        //}
         /*catch (RemoteException doe) {
            TctLog.e(mName, "can't set backlights");
        }*/
    }

    private void setBacklightNormalState() {
        bl.setLcdBacklight(255);// workaround deng
        bl.setKbdBacklight(0);
    }

    private void ToggleBacklight() {
        setBacklight((mState & 1) == 0 ? BRIGHTNESS_OFF : BRIGHTNESS_ON);
    }

    private void ToggleBacklight2() {
        if (mId == Test.ID.BACKLIGHT.ordinal()) {
            bl.setLcdBacklight((mState & 1) == 0 ? BRIGHTNESS_OFF
                    : BRIGHTNESS_ON);
        } else {
            bl.setKbdBacklight((mState & 1) == 0 ? BRIGHTNESS_OFF
                    : BRIGHTNESS_ON);
        }
    }

    private void ToggleBacklight3() {
        PowerManager pm = (PowerManager) mContext
                .getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
        /* ..screen will stay on during this section.. */

        if (mState % 1 == 0) {
            wl.acquire();
        } else {
            wl.release();
        }
    }

    @Override
    protected void Run() {

        // stop timers if any
        StopTimer();
        // this function executes the test
        switch (mState) {
        case END:
            setBacklight(BRIGHTNESS_ON);
            setBacklightNormalState();
            break;

        case INIT: // init the test, shows the first screen
            mTimeIn.start();
            ii = 0;

        default:
            ii++;
            if (ii > 5) { // deng
                setBacklight(BRIGHTNESS_ON);
                ToggleBacklight3();
                break;
            }

            ToggleBacklight();

            // go to next step
            SetTimer(1000 /* mili */, new CallBack() {
                public void c() {
                    Run();
                }
            });

            tl = new TestLayout1(mContext, mName, "Is " + mBlName
                    + " Backlight flashing?");
            // if(!mTimeIn.isFinished())
            // tl.setAutoTestButtons(false);
            // tl.setAutoTestButtons(true);

            mState++;

            mContext.setContentView(tl.ll);
            break;
        }

    }

    @Override
    protected void onTimeInFinished() {
        // if(tl!=null)
        // tl.setAutoTestButtons(true);
    }
}

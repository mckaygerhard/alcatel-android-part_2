/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                           CameraTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.util.List;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.SystemClock;
import android.util.TctLog;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * common landscape camera test.
 * */
class CameraTest extends Test {

    private FrameLayout fl;
    private TestLayout1 tl;
    private Preview mPreview;

    int mZoomValue = 0;

    CameraTest(ID pid, String s) {
        this(pid, s, 0);
    }

    CameraTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
        setKey();
    }

    private void setKey() {

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                int KeyCode = event.getKeyCode();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (KeyCode) {
                    case KeyEvent.KEYCODE_VOLUME_UP:
                        mZoomValue = mZoomValue >= 60 ? 60 : ++mZoomValue;
                        break;
                    case KeyEvent.KEYCODE_VOLUME_DOWN:
                        mZoomValue = mZoomValue == 0 ? 0 : --mZoomValue;

                        break;

                    }
                    mPreview.setZoom(mZoomValue);
                }

                return true;
            }

        };

    }

    private void setFrontCamera(boolean en) {
        final String prop = "ril.camera.name";
        if (!en) {
            android.os.SystemProperties.set(prop, "ov5647");
        } else {
            android.os.SystemProperties.set(prop, "ov7690");
        }
    }

    @Override
    protected void Run() {
        try {
            mContext.getWindow().setType(
                    WindowManager.LayoutParams.TYPE_APPLICATION);
        } catch (Exception e) {
            // TODO: handle exception
        }

        TctLog.e(TAG, "deng+++++ct run");
        // this function executes the test
        switch (mState) {
        case INIT:
            mZoomValue = 0;
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); // deng.

            fl = new FrameLayout(mContext);
            mPreview = new Preview(mContext);

            if (mId == Test.ID.CAMERA_FRONT.ordinal()) {
                setFrontCamera(true);
            }
            // tl = new TestLayout1(mContext, mName, "    ", "PASS", "FAIL",
            // 4000);
            tl = new TestLayout1(mContext, mName, "    ", "PASS", "FAIL", 0);

            fl.addView(mPreview);
            fl.addView(tl.ll);

            mContext.setContentView(fl);
            break;

        case INIT + 1:
            mZoomValue = 30;
            mPreview.setZoom(mZoomValue);

            mState = MULTI_PAGE_LAST_PAGE;
            break;

        case END:
            // Exit() always pass here.
            setFrontCamera(false);
            break;
        default:
            break;
        }
    }

}

/**
 * for camera full-screen preview.
 * */
class Preview extends SurfaceView implements SurfaceHolder.Callback {
    SurfaceHolder mHolder;
    private android.hardware.Camera mCamera;
    int mZoom = 0;

    int mViewFinderWidth = Lcd.height();
    int mViewFinderHeight = Lcd.width();

    private String TAG = "MMITEST:CamPreview";

    Preview(Context context) {
        super(context);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    Preview(Context context, int scale) {
        this(context);
        mZoom = scale;
    }

    Preview(Context context, boolean isPortrait) {
        this(context);
    }

    /**
     * wait for surfaceCreated before calling setPreviewDisplay() or starting
     * preview.
     */
    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where to
        // draw.
        try {
            mCamera = Camera.open();
        } catch (Exception e) {
            TctLog.e(TAG, "can't open camera ");
            Toast.makeText(mContext, "open camera error!!", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        // add some delay. deng.
        SystemClock.sleep(500);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        // Because the CameraDevice object is not a shared resource,
        // it's very
        // important to release it when the activity is paused.
        if (mCamera == null) {
            TctLog.e(TAG, "destroy: no camera ");
            return;
        }

        if (mCamera.previewEnabled()) {
            try {
                mCamera.stopPreview();
                mCamera.release();

                // add some delay. deng.
                SystemClock.sleep(500);

            } catch (Exception e) {
                TctLog.e(TAG, "can't stop preview ");
            }
            mCamera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and
        // begin the preview.
        if (mCamera != null) {
            TctLog.d(TAG, "setPreviewDisplay");
            try {
                mCamera.setPreviewDisplay(holder);
            } catch (Exception ex) {
                TctLog.e(TAG, "setPreviewDisplay failed");
            }

            if (getPreviewSize(w, h)) {
                // if w+h is not supported, go to default(full screen).
                mViewFinderWidth = w;
                mViewFinderHeight = h;
            }
            setCameraParameters();

            // setFlash(1); //torch on, deng.

            TctLog.d(TAG, "startPreview");
            try {
                mCamera.startPreview();
            } catch (Throwable ex) {
                Toast.makeText(mContext, "start preview error!!",
                        Toast.LENGTH_LONG).show();
                //TctLog.e(TAG, "startPreview failed", ex);
            }
        }
    }

    public void setFlash(int z) {
        if (mCamera == null) {
            TctLog.e(TAG, "setFlash: no camera ");
            return;
        }

        Camera.Parameters mParameters = mCamera.getParameters();
        String mode;
        if (z == 0) {
            mode = "off";
        } else {
            mode = "torch";
        }
        TctLog.d(TAG, "set flash to " + mode);
        mParameters.set("flash-mode", mode);
        mCamera.setParameters(mParameters);
    }

    public void setZoom(int z) {
        if (mCamera == null) {
            TctLog.e(TAG, "setZoom: no camera ");
            return;
        }

        Camera.Parameters mParameters = mCamera.getParameters();
        if (z < 0) {
            z = 0;
        } else if (z > 60) {
            z = 60;
        }
        TctLog.d(TAG, "set zoom value to " + z);
        mParameters.set("zoom", String.valueOf(z));
        mCamera.setParameters(mParameters);
    }

    private boolean getPreviewSize(int w, int h) {
        List<Size> sps = mCamera.getParameters().getSupportedPreviewSizes();
        for (Size ps : sps) {
            if (ps.width == w && ps.height == h) {
                TctLog.d(TAG, "the preview size " + w + "x" + h + " is valid.");
                return true;
            }
        }

        TctLog.d(TAG, "the preview size " + w + "x" + h + " is not supported.");
        return false;
    }

    private void setCameraParameters() {
        TctLog.e(TAG, "setCameraParameters");

        final String ANDROID_QUALITY = "jpeg-quality";
        final String THUNDERST_TIMESTAMP = "thunderst_timestamp";
        final String THUNDERST_NIGHTMODE = "thunderst_nightmode";
        final String ANDROID_EFFECT = "effect";
        final String ANDROID_FLICKER_ADJ = "antibanding";
        final String PARM_PICTURE_SIZE = "picture-size";

        final String BRIGHTNESS = "luma-adaptation";
        final String WHITEBALANCE = "whitebalance";

        Camera.Parameters mParameters = mCamera.getParameters();

        mParameters.setPreviewSize(mViewFinderWidth, mViewFinderHeight);
        TctLog.e(TAG, "setCameraParameter: mViewFinderWidth: " + mViewFinderWidth
                + " mViewFinderHeight: " + mViewFinderHeight);

        // to prevent auto clockwise rotation of 90 degree
        // FIXME: However, we must set "orientation" as "protrait",
        // for front camera YUV OV7690.
        mParameters.set("orientation", "portrait");

        try {
            mCamera.setParameters(mParameters);
        } catch (IllegalArgumentException e) {
            TctLog.e(TAG, "set Parameters error!!!");
            e.printStackTrace();
        }

    }
}

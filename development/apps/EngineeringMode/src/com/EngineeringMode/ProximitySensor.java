/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                  ProximitySensorTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.TctLog;
import android.view.Gravity;
import android.widget.TextView;

/*
 * Empty Test use as a default when no test was defined
 */
class ProximitySensorTest extends Test implements SensorEventListener {

    private int far = -1;
    private int near = -1;

    TestLayout1 tl;
    private TextView tvbody;

    private SensorManager mSensorManager;
    private String TAG = "ProximitySensor";
    // private int mCount=0;

    private Sensor lsensor;

    ProximitySensorTest(ID pid, String s) {
        super(pid, s);
        TAG = Test.TAG + TAG;

    }

    ProximitySensorTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
        TAG = Test.TAG + TAG;

    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT: // init the test, shows the first screen

            far = -1;
            near = -1;

            if (mSensorManager == null) {
                mSensorManager = (SensorManager) mContext
                        .getSystemService(Context.SENSOR_SERVICE);
            }

            lsensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
            if (lsensor != null) {
                TctLog.i(TAG, "Sensor opened : " + lsensor.getName());
                if (!mSensorManager.registerListener(this, lsensor,
                        SensorManager.SENSOR_DELAY_NORMAL)) {
                    TctLog.e(TAG,
                            "register listener for sensor " + lsensor.getName()
                                    + " failed");
                }
            } else {
                tl = new TestLayout1(mContext, mName, "No Sensor found");
                // tl.setAutoTestButtons(true, false);
                mContext.setContentView(tl.ll);
            }

            tvbody = new TextView(mContext);
            tvbody.setGravity(Gravity.CENTER);
            // tvbody.setTypeface(Typeface.MONOSPACE, 1);
            tvbody.setTextAppearance(mContext,
                    android.R.style.TextAppearance_Large);
            tvbody.setText("opening ...");

            tl = new TestLayout1(mContext, mName, tvbody, "PASS", "FAIL");
            // tl.setAutoTestButtons(true, false);
            mContext.setContentView(tl.ll);

            TctLog.e(TAG, "deng  for sensor " + mState);
            mState++;
            // Run();
            break;

        case TIMEOUT:
            // tl.setAutoTestButtons(true);
            break;

        case END://
        default:
            mSensorManager.unregisterListener(this, lsensor);
            TctLog.d(TAG, "sensor listener unregistered");
            StopTimer();
            break;
        }
    }

    public void onSensorChanged(SensorEvent event) {

        TctLog.d(TAG, "onSensorChanged: (" + event.values[0] + ", "
                + event.values[1] + ", " + event.values[2] + ")");

        int value = (int) event.values[0];
        if (value == 0) {
            near = 1;
        } else {
            far = 1;
        }

        String s = "please shadow the sonsor." + "\n\n";
        if (far > 0) {
            s = s + "far: OK\n";
        } else {
            s = s + "far: not tested\n";
        }

        if (near > 0) {
            s = s + "near: OK\n";
        } else {
            s = s + "near: not tested\n";
        }

        tvbody.setText(s);

        if ((far > 0) && (near > 0)) {
            // tl.setAutoTestButtons(true);
        }

    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
        // TctLog.i(TAG,"sensor"+sensor.getName()+" accuracy changed :"+accuracy);
    }

    @Override
    protected void onTimeInFinished() {
        // if(tl!=null)
        // tl.setAutoTestButtons(true);
    }

}

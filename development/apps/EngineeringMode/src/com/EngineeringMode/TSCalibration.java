/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                        TSCalibration.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;

import android.view.Window;
import android.view.WindowManager;

public class TSCalibration extends Activity {

    // final private static String TAG = "TSCalibration";

    public TSCalibrationView mTSCalibrationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        mTSCalibrationView = new TSCalibrationView(this, display.getHeight(),
                display.getWidth());
        // set screen appearance
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        int mask = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        mask |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        getWindow().setFlags(mask, mask);
        setContentView(mTSCalibrationView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTSCalibrationView.reset();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mTSCalibrationView.isFinished()) {
            setResult(0);
            finish();
        } else {
            setContentView(mTSCalibrationView);
        }
        return true;
    }

    public void onCalTouchEvent(MotionEvent ev) {

        mTSCalibrationView.invalidate();
        if (mTSCalibrationView.isFinished()) {
            finish();
        }
    }
}

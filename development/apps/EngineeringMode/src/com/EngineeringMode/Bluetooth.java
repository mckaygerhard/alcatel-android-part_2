/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                        BluetoothTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.TctLog;
import android.view.View;

/**
 * Bluetooth test. {turn on -- search -- found -- turn off}.
 */
class BluetoothTest extends Test {

    private BluetoothAdapter mBtAdapter;
    ArrayList<String> mDevicesList = new ArrayList<String>();
    private int mBluetoothState = 0, mBluetoothOldState = 0;

    private final String TAG = super.TAG + "-BT";

    private IntentFilter filter;
    private TestLayout1 tl;

    private final int STATE_TURNING_ON = INIT + 1;
    private final int STATE_ON = INIT + 2;
    private final int STATE_SEARCHING = INIT + 3;
    private final int STATE_SEARCH_FINISHED = INIT + 4;
    private final int STATE_FOUND = INIT + 5;
    private final int STATE_TURNING_OFF = INIT + 6;
    private final int STATE_OFF = INIT + 7;
    private final int STATE_SHOW_RESULT = INIT + 8;

    private final int mPolicyNum = 1; // get [policy_num] remote devices, then
                                      // stop.
    private boolean mPolicyDone;
    private boolean mTestResult = false;

    BluetoothTest(ID pid, String s) {
        super(pid, s);
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        TctLog.d(TAG, "doDiscovery()");

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    @Override
    protected void Run() {
        switch (mState) {
        case INIT:
            mDevicesList.clear();
            mPolicyDone = false;

            // Register for broadcasts when a device is discovered
            filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            mContext.registerReceiver(mReceiver, filter);

            // Get the local Bluetooth adapter
            mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            mBluetoothState = mBtAdapter.getState();

            if (!mBtAdapter.isEnabled()) {
                mBtAdapter.enable();
                tl = new TestLayout1(mContext, mName, "turn on Bluetooth");

            } else {
                TctLog.d(TAG, "BT already ON!");
                tl = new TestLayout1(mContext, mName,
                        "BT already ON , starting discovery");

                mBluetoothOldState = BluetoothAdapter.STATE_ON;
                doDiscovery();
            }

            // wait for searching start.
            SetTimer(15000, new CallBack() {
                public void c() {
                    mState = TIMEOUT;
                    Run();
                }
            });

            mContext.setContentView(tl.ll);
            break;

        case BluetoothAdapter.STATE_TURNING_ON:
            tl = new TestLayout1(mContext, mName, "enabling Bluetooth...");
            tl.setButtonsEnabled(false);
            mContext.setContentView(tl.ll);
            break;

        case BluetoothAdapter.STATE_ON:
            tl = new TestLayout1(mContext, mName, "Starting discovery");
            tl.setButtonsEnabled(false);
            mContext.setContentView(tl.ll);
            doDiscovery();
            break;

        case BluetoothAdapter.STATE_TURNING_OFF:
            tl = new TestLayout1(mContext, mName, "disabling Bluetooth...");
            tl.setButtonsEnabled(false);
            mContext.setContentView(tl.ll);
            break;

        case BluetoothAdapter.STATE_OFF:
            if (!mPolicyDone) // for re-test.
                return;

            if (mBluetoothOldState == BluetoothAdapter.STATE_TURNING_OFF) {

                // regular exit.
                goExit();
            }
            break;

        case STATE_SEARCHING:
            StopTimer();
            // at most 5 seconds.
            SetTimer(5000, new CallBack() {
                public void c() {
                    mState = STATE_SHOW_RESULT;
                    Run();
                }
            });

            tl = new TestLayout1(mContext, mName, "searching...");
            tl.setButtonsEnabled(false);
            mContext.setContentView(tl.ll);
            break;

        case STATE_FOUND:
        case STATE_SEARCH_FINISHED:
        case STATE_SHOW_RESULT:
            updateDeviceList("Devices :\n\n", mPolicyNum);
            break;

        case TIMEOUT:
            StopTimer();
            String error;
            if (!mBtAdapter.isEnabled()) {
                error = "enable failed";
            } else {
                error = "no device found";
            }

            tl = new TestLayout1(mContext, mName, error);
            // tl.setAutoTestButtons(true,false);
            mContext.setContentView(tl.ll);

            // If we're discovering, stop it
            if (mBtAdapter.isDiscovering()) {
                mBtAdapter.cancelDiscovery();
            }
            break;

        case END:
            // DO cleanup here to ensure re-test is okay.
            TctLog.e(TAG, "end 1");
            StopTimer();
            unregister();
            disableBT();
            TctLog.e(TAG, "end 2");
            break;

        default:
            break;
        }
    }

    private void StopUpdate() {
        mPolicyDone = true;
    }

    private void updateDeviceList(String title, int show) {

        if (mPolicyDone) { // avoid bothering.
            return;
        }

        String list = title;

        int s = mDevicesList.size();
        if (show <= s) {
            mPolicyDone = true;
            s = show;
            list = "OK\n\n";
        }

        for (int i = 0; i < s; i++) {
            list = list + mDevicesList.get(i) + "\n";
        }

        for (int i = 0; i < (show - s); i++) {
            list = list + "====\n";
        }

        tl = new TestLayout1(mContext, mName, list, "PASS", "FAIL",
                mLeftButton, mRightButton);
        // tl.setAutoTestButtons(true, false);

        // if(mPolicyDone){
        // StopTimer();
        // disableBT();
        // tl.setAutoTestButtons(false, true); //pass only
        // }

        mContext.setContentView(tl.ll);
    }

    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                /* STATE_OFF, STATE_TURNING_ON, STATE_ON, STATE_TURNING_OFF */
                mBluetoothState = intent.getIntExtra(
                        BluetoothAdapter.EXTRA_STATE, 0);
                mBluetoothOldState = intent.getIntExtra(
                        BluetoothAdapter.EXTRA_PREVIOUS_STATE, 0);

                Hashtable<Integer, String> h = new Hashtable<Integer, String>();
                h.put(BluetoothAdapter.STATE_OFF, "STATE_OFF");
                h.put(BluetoothAdapter.STATE_ON, "STATE_ON");
                h.put(BluetoothAdapter.STATE_TURNING_OFF, "STATE_TURNING_OFF");
                h.put(BluetoothAdapter.STATE_TURNING_ON, "STATE_TURNING_ON");
                TctLog.d(TAG, "state changed " + h.get(mBluetoothOldState) + "=>"
                        + h.get(mBluetoothState));

                mState = mBluetoothState;

            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                TctLog.d(TAG, "found >>");
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!mDevicesList.contains(device.getAddress())) {
                    mDevicesList.add(device.getAddress());
                    TctLog.d(TAG, Arrays.toString(mDevicesList.toArray()));
                } else {
                    return;
                }
                mState = STATE_FOUND;

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                    .equals(action)) {
                TctLog.d(TAG, "discovery finished <<");
                if (mDevicesList.size() == 0) {
                    // mDevicesList.add("No device found");
                    TctLog.d(TAG, "scan result: 0 remote device.");
                }
                mState = STATE_SEARCH_FINISHED;

            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                TctLog.d(TAG, "searching...");
                mState = STATE_SEARCHING;

            } else {
                TctLog.d(TAG, "unknown action.");
                return; /* don't change test state */
            }

            Run();
        }
    };// recv

    protected void unregister() {
        try {
            mContext.unregisterReceiver(mReceiver);
        } catch (Exception e) {
            TctLog.e(TAG, "already unregistered");
        }
    }

    private boolean disableBT() {
        if (mBtAdapter.isEnabled()) {
            if (mBtAdapter.isDiscovering()) {
                mBtAdapter.cancelDiscovery();
            }
            TctLog.i(TAG, "disable BT");
            mBtAdapter.disable();

            return true;
        }

        return false;
    }

    private void goExit() {
        StopTimer(); // stop other timer, as this page also use timer.

        tl = new TestLayout1(mContext, mName, "  ", 100, mTestResult);
        mState = MULTI_PAGE_LAST_PAGE;
        mContext.setContentView(tl.ll);
    }

    private View.OnClickListener mRightButton = new View.OnClickListener() {
        public void onClick(View v) {
            mTestResult = false;
            StopUpdate();
            boolean dis = disableBT();
            if (!dis) {
                goExit();
            }
        }
    };

    private View.OnClickListener mLeftButton = new View.OnClickListener() {
        public void onClick(View v) {
            mTestResult = true;
            StopUpdate();
            boolean dis = disableBT();
            if (!dis) {
                goExit();
            }
        }
    };

}

/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                              Version.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/* 13/01/10| Qianbo.Pan     |     FR-378099      | Firmware version display   */
/* 13/06/10| Qianbo.Pan     |     PR-466129      | Firmware version display   */
/* 13/12/23| Yongliang.wang |     CR-552166      | BoardInfo display          */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.io.FileInputStream;

public class Version extends Test {

    Version(ID pid, String s) {
        super(pid, s);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void Run() {
        // TODO Auto-generated method stub
        switch (mState) {
        case INIT:
            /*String firmware = getDeviceInfo(CommandManager.FIRMWARE_FILE)==null?null:getDeviceInfo(CommandManager.FIRMWARE_FILE).trim();
            String board_info=getDeviceInfo(CommandManager.BOARD_INFO)==null?"unknown":shift_boardinfo(getDeviceInfo(CommandManager.BOARD_INFO).trim());
            String str = String.format("SW = %s\nIMEI = %s\nBOARDINFO:\n%s\n", getBuildNumber(),
                    getIMEI(),board_info);
            if (firmware == null) {
                str += "Firmware version read error!";
            } else {
                str += String.format("Firmware: \n %s", firmware);
            }*/
            String str = String.format("SW = %s\nIMEI = %s\n", getBuildNumber(), getIMEI());
            TestLayout1 tl = new TestLayout1(mContext, mName, str);
            mContext.setContentView(tl.ll);
            break;

        default:
            break;
        }
    }

    private String getBuildNumber() {
        return Build.VERSION.INCREMENTAL;
    }

    private String getIMEI() {
        String imeiStr = ((TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        return imeiStr;
    }

   // private String getFirmware() {
   //     CommandManager cmd = new CommandManager();
   //     byte[] range = null;

   //     FileInputStream fis = cmd.fileInputOpen(CommandManager.FIRMWARE_FILE);
   //     if(fis != null){
   //         cmd.setFileInputStream(fis);
   //         range = cmd.readFileInputStream();

   //     }
   //     cmd.fileInputClose();
   //     if (range == null) {
   //         return null;
   //     }
   //     return new String(range);
   // }

     protected String shift_boardinfo(String  board_info){
      String board_id = "";
      String board_region="";
      String result="";
       double voltage=0.0;
        int region=0;
       boolean unit_mV=false;
       int ind1,ind2,ind3;
       ind1=ind2=ind3=0;

       if(board_info.contains("MPP7:")){
           ind1=board_info.indexOf("MPP7:");
         }else{
           ind1=-1;
          }

       if(board_info.contains("mV")){
          ind2=board_info.indexOf("mV");
          unit_mV=true;
        }else if(board_info.contains("V")){
          ind2=board_info.indexOf("V");
          unit_mV=false;
        }else{
           ind2=-1;
         }

        if(board_info.contains("GPIO56:")){
           ind3=board_info.indexOf("GPIO56:");
         }else{
           ind3=-1;
          }
        //System.out.println("board_info=" + board_info);
        //System.out.println("ind1="+ind1+",ind2="+ind2+",ind3="+ ind3+",unit_mV="+unit_mV);
         try{
        if(ind1!=-1 && (ind2 > ind1)){
         board_id=board_info.substring((ind1 +5), ind2);
         if(!unit_mV){
            voltage=Double.parseDouble(board_id);
            voltage= voltage*(1000.0);
          }else{
            voltage=Double.parseDouble(board_id);
           }
        System.out.println("voltage="+voltage);
          if((voltage>=0) && (voltage <300.0)){
               result+="V00";
            }else if((voltage>=300.0) && (voltage <800.0)){
               result+="V01";
            }else if((voltage>=800.0) && (voltage <1100.0)){
               result+="V02&8GB&NFC";
            }else if((voltage>=1100.0) && (voltage <1300.0)){
               result+="V02&8GB&NO NFC";
            }else if((voltage>=1400.0) && (voltage <1500.0)){
               result+="V02&4GB&NO NFC";
            }else{
                 //do nothing
              }
         }
         if(ind3!=-1){
             board_region=board_info.substring((ind3 +7), board_info.length());
             region=Integer.parseInt(board_region);
             if(region==0){
                 result+="&EMEA";
             }else if(region==1){
                 result+="&LATAM";
             }else{
               //do nothing
              }
              //System.out.println("region="+region);
          }
         }catch(Exception e){
              e.printStackTrace();
          }
           //System.out.println("result="+result);
           return result;

     }



    private String getDeviceInfo(String device) {
        CommandManager cmd = new CommandManager();
        byte[] range = null;

        try{
            FileInputStream fis = cmd.fileInputOpen(device);
            if(fis != null){
   //             Log.e("WYL","fis not null");
                cmd.setFileInputStream(fis);
                range = cmd.readFileInputStream();
            }
            cmd.fileInputClose();
            if (range == null) {
                return null;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return new String(range);
    }
}

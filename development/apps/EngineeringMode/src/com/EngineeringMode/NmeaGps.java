/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                              NmeaGps.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.TctLog;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class NmeaGps extends Activity {
    private LocationManager mLocationManager;
    private GpsStatus.Listener statusListener;
    private LocationListener locationListener;
    private GpsStatus.NmeaListener mygpsnmealistener;
    private GpsSatellite s;
    private TextView tvLatitude;
    private TextView tvLongitude;
    private TextView tvDateTime;
    private TextView tvAltitude;
    private TextView txtSatellites;
    private TextView txtSnr;
    private TextView txtLoggingTo;
    private TextView txtFrequency;
    private TextView txtDistance;
    private TextView txtFilename;
    private TextView txtsateID;
    private TextView nmeastatus;
    private ToggleButton buttonOnOff;
    private GpsStatus statusnew;
    private TextView tvStatus;
    public Iterator<GpsSatellite> mIt;
    public static boolean newFileOnceADay = true;
    public static boolean startFlag = false;
    public static int i = 0;
    public static int j = 0;

    public static String currentFileName;
    public static String mycurrentFileName;
    public static int numoflog = 0;
    public static int num = 0;
    public int maxSatellites;
    public int[] sateID;
    public float[] satesnr;
    public int satenumber;
    public int satenext;
    /*
     * String sGGA1 = "$GPGGA,"; String sGGA2 = ",,,,0,00,999.9,,M,,M,,*66";
     * String sPGLOR1 = "$PGLOR,FTS,27,"; String sPGLOR2 =
     * ",dBHz,,,,NU,,,,,165,0*44"; StringBuilder sLocation = new
     * StringBuilder(); StringBuilder sLocationTimer = new StringBuilder();
     */
    StringBuilder mLocationTimer = new StringBuilder();
    String mString = new String();
    String mStringTimer = new String();
    String mynmea = new String();
    GpsSatellite satellite;

    Timer timer;
    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 1:
                float snr = 0;
                int PNR;
                int mysnr;
                mIt = mLocationManager.getGpsStatus(null).getSatellites()
                        .iterator();
                if (!mIt.hasNext()) {
                    satenext = 0;
                    txtSnr.setText(R.string.not_applicable);
                    txtsateID.setText(R.string.not_applicable);
                } else {
                    if (sateID != null) {
                        satenext %= satenumber;
                        txtSnr.setText("" + satesnr[satenext]);
                        txtsateID.setText("" + sateID[satenext]);
                        satenext++;
                    }
                }
                String str = FormatTime(j);
                setTitle("" + "Seconds:" + " " + j);
                j++;
                // mLocationTimer.append(mynmea);
                break;
            }
            super.handleMessage(msg);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gps);

        tvLatitude = (TextView) findViewById(R.id.txtLatitude);
        tvLongitude = (TextView) findViewById(R.id.txtLongitude);
        tvDateTime = (TextView) findViewById(R.id.txtDateTimeAndProvider);
        tvAltitude = (TextView) findViewById(R.id.txtAltitude);
        txtSatellites = (TextView) findViewById(R.id.txtSatellites);
        txtSnr = (TextView) findViewById(R.id.txtSnr);
        txtLoggingTo = (TextView) findViewById(R.id.txtLoggingTo);
        txtFrequency = (TextView) findViewById(R.id.txtFrequency);
        txtDistance = (TextView) findViewById(R.id.txtDistance);
        txtFilename = (TextView) findViewById(R.id.txtFileName);
        txtsateID = (TextView) findViewById(R.id.txtsateID);
        nmeastatus = (TextView) findViewById(R.id.nmeastatus);
        buttonOnOff = (ToggleButton) findViewById(R.id.buttonOnOff);
        tvStatus = (TextView) findViewById(R.id.textStatus);

        buttonOnOff.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (!checkSDCard()) {
                    mMakeTextToast(getResources().getText(R.string.str_err)
                            .toString(), true);
                    tvStatus.setText("Please check the SDcard!");
                    buttonOnOff.setChecked(false);
                } else {
                    try {
                        if (buttonOnOff.isChecked()) {
                            StartGpsManager();
                            ResetCurrentFileName();
                            ClearSummary();
                        } else {
                            SDCardWrite(mycurrentFileName, "" + mLocationTimer);
                            StopGpsManager();
                            ShowSummary();
                        }
                    } catch (Exception ex) {
                        TctLog.e("onCheckedChanged", ex.getMessage());
                        SetStatus(R.string.button_click_error);
                    }
                }
            }
        });

    }

    public void onDestroy() {
        super.onDestroy();
        if (startFlag)
            StopGpsManager();
    }

    public void StartGpsManager() {
        startFlag = true;
        j = 0;
        timer = new Timer();
        TimerTask task = new TimerTask() {

            public void run() {
                TctLog.w("run", "enter run");
                Message message = new Message();
                message.what = 1;
                handler.sendMessage(message);
            }
        };
        timer.schedule(task, 2000, 1000);
        statusListener = new GpsStatus.Listener() {
            @Override
            public void onGpsStatusChanged(int event) {
                statusnew = mLocationManager.getGpsStatus(null);
                // mIt = statusnew.getSatellites().iterator();
                updateGpsStatus(event, statusnew);
            }
        };

        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                updateWithNewLocation(location);
            }

            public void onProviderDisabled(String provider) {
                updateWithNewLocation(null);
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status,
                    Bundle extras) {
            }
        };

        mygpsnmealistener = new GpsStatus.NmeaListener() {

            @Override
            public void onNmeaReceived(long timestamp, String nmea) {
                // TODO Auto-generated method stub
                if (sateID != null)
                    mLocationTimer.append(nmea);
                mynmea = nmea;
                TctLog.i("lihui", mynmea);
                // mLocationTimer.append(nmea);
                nmeastatus.setText(R.string.nmeastatus);

            }
        };

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = mLocationManager
                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                0, 0, locationListener);
        mLocationManager.addGpsStatusListener(statusListener);
        mLocationManager.addNmeaListener(mygpsnmealistener);
        statusnew = mLocationManager.getGpsStatus(null);
        // mIt = statusnew.getSatellites().iterator();
        SetStatus(R.string.started);
        updateWithNewLocation(location);
        updateGpsStatus(0, null);
    }

    public void StopGpsManager() {

        if (locationListener != null) {
            mLocationManager.removeUpdates(locationListener);
            mLocationManager.removeGpsStatusListener(statusListener);
            mLocationManager.removeNmeaListener(mygpsnmealistener);
        }
        if(timer!=null)
            timer.cancel();
        ClearForm();
        // sLocation.delete(0, sLocation.length());
        mLocationTimer.delete(0, mLocationTimer.length());
        SetStatus(R.string.stopped);
        i = 0;
        j = 0;
        mIt = null;
        // statusnew=null;
        sateID = null;
        satesnr = null;

    }

    private void updateGpsStatus(int event, GpsStatus status) {
        if (status == null) {
            txtSatellites.setText(R.string.not_applicable);
            txtSnr.setText(R.string.not_applicable);
            txtsateID.setText(R.string.not_applicable);
        } else if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            maxSatellites = status.getMaxSatellites();
            Iterator<GpsSatellite> it = status.getSatellites().iterator();
            int count = 0;
            int l = 0;
            sateID = new int[maxSatellites];
            satesnr = new float[maxSatellites];
            String str = FormatTime(i);
            try {// File SDFile = new File(Environment
                 // .getExternalStorageDirectory(), "GPSLogger");
                 // if(!SDFile.exists())
                 // SDFile.mkdir();
                 // File logFile = new File(SDFile, "GPSLogger.TctLog");
                 // FileWriter myoutputStream = new FileWriter(logFile,true);
                 // if(!logFile.exists())
                 // logFile.createNewFile();
                while (it.hasNext() && count <= maxSatellites) {
                    s = it.next();
                    int snr = (int) s.getSnr();
                    sateID[l] = s.getPrn();
                    int mysnr = (int) s.getSnr() * 10;
                    satesnr[l] = (float) mysnr / 10;
                    TctLog.i("lihui", "begin to safe the prn snr!" + sateID[l]
                            + satesnr[l]);
                    // myoutputStream.write("begin to safe the prn snr!"+"sateID="+sateID[l]+"satesnr="+satesnr[l]+"\n");
                    l++;
                    count++;
                }
                // myoutputStream.write("begin to safe the satenum! + satenumber="+count+"\n");
                // myoutputStream.flush();
                // myoutputStream.close();
            } catch (Exception e) {
                // TODO: handle exception
            }
            satenumber = count;
            TctLog.i("lihui", "begin to safe the prn snr! + satenumber="
                    + satenumber);
            txtSatellites.setText("" + count);
            i++;
        }
    }

    private void updateWithNewLocation(Location location) {

        if (location != null) {
            DisplayLocationInfo(location);
        } else {

        }
    }

    private boolean checkSDCard() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    private void ClearForm() {
        tvLatitude.setText("");
        tvLongitude.setText("");
        tvDateTime.setText("");
        tvAltitude.setText("");
        txtSatellites.setText("");
        txtSnr.setText("");
        txtLoggingTo.setText("");
        txtFrequency.setText("");
        txtDistance.setText("");
        txtFilename.setText("");
        txtsateID.setText("");
        nmeastatus.setText("");
        nmeastatus.setText("");
    }

    public void mMakeTextToast(String str, boolean isLong) {
        if (isLong == true) {
            Toast.makeText(NmeaGps.this, str, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(NmeaGps.this, str, Toast.LENGTH_SHORT).show();
        }
    }

    public static void SDCardWrite(String filename, String stext) {
        String sDStateString = android.os.Environment.getExternalStorageState();

        if (sDStateString.equals(android.os.Environment.MEDIA_MOUNTED)) {
            try {
                File SDFile = new File(
                        Environment.getExternalStorageDirectory(), "GPSLogger");
                if (!SDFile.exists()) {
                    SDFile.mkdirs();
                }

                File myFile = new File(SDFile.getPath() + File.separator
                        + filename + ".TctLog");
                while (myFile.exists()) {
                    numoflog++;
                    ResetCurrentFileName();
                    myFile = new File(SDFile.getAbsolutePath() + File.separator
                            + mycurrentFileName + ".TctLog");
                }

                boolean result = myFile.createNewFile();
                TctLog.i("lihui", "myFile.createNewFile()=" + result);
                FileOutputStream outputStream = new FileOutputStream(myFile);
                outputStream.write(stext.getBytes());
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                // TODO: handle exception
                TctLog.i("lihui", "catch (Exception e)");
                e.printStackTrace();
            }
        }
    }

    public void SetStatus(int stringId) {
        String s = getString(stringId);
        tvStatus.setText(s);
    }

    public void DisplayLocationInfo(Location loc) {
        try {
            tvDateTime.setText(new Date().toLocaleString()
                    + getString(R.string.providername_using));
            tvLatitude.setText(String.valueOf(loc.getLatitude()));
            tvLongitude.setText(String.valueOf(loc.getLongitude()));

            if (loc.hasAltitude()) {
                double altitude = loc.getAltitude();
                tvAltitude.setText(String.valueOf(altitude)
                        + getString(R.string.meters));
            } else {
                tvAltitude.setText(R.string.not_applicable);
            }

            // if (loc.hasAccuracy()) {
            // float accuracy = loc.getAccuracy();
            // txtAccuracy.setText(getString(R.string.accuracy_within,
            // String.valueOf(accuracy), getString(R.string.meters)));
            // } else {
            // txtAccuracy.setText(R.string.not_applicable);
            // }

        } catch (Exception ex) {
            SetStatus(R.string.error_displaying);
        }
    }

    public void ShowSummary() {
        txtLoggingTo.setText("Saved in SDcard: GPSLogger/" + mycurrentFileName
                + ".TctLog");
        txtFrequency.setText("1 second");
        txtDistance.setText("0 meter");
        txtFilename.setText(mycurrentFileName + ".TctLog");
    }

    public void ClearSummary() {
        txtLoggingTo.setText("");
        txtFrequency.setText("");
        txtDistance.setText("");
        txtFilename.setText("");
    }

    private static void ResetCurrentFileName() {
        if (numoflog < 10)
            mycurrentFileName = "Nmea_" + "00" + numoflog;
        else if (numoflog < 100)
            mycurrentFileName = "Nmea_" + "0" + numoflog;
        else
            mycurrentFileName = "Nmea_" + numoflog;
    }

    private String FormatTime(int num) {
        int q = 0, b, g;
        b = num / 60;
        g = num % 60;
        if (b >= 60) {
            q = b / 60;
            b = b % 60;
        }
        int m = (q * 10000 + b * 100 + g);
        String str = String.format("%06d", m);
        return str;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(NmeaGps.this)
                    .setMessage(getString(R.string.dialog_title))
                    .setPositiveButton(getString(R.string.dialog_button_yes),
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    finish();
                                }

                            })
                    .setNegativeButton(getString(R.string.dialog_button_no),
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    dialog.dismiss();
                                }

                            }).show();
        }
        return false;

    }
}

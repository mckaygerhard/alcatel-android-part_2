/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                      BatteryTempTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import android.util.TctLog;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.TextView;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;

/*
 * Battery temperature
 * We use native Android API here, also we can read the standard Linux sysfs node i.e. 
 * /sys/devices/platform/msm-battery/power_supply/battery/temp
 */
class BatteryTempTest extends Test {
    String TAG = Test.TAG + "BatteryTempTest";
    BufferedReader mBattTempReader;

    private IntentFilter mIntentFilter;
    private int mBattTempValue;

    // NOTE: values from Intent is 10x, i.e. 330 stand for 33.0 C.
    private final int HIGH = 500;
    private final int LOW = 200;

    private TextView tvbody;
    private TestLayout1 tl;

    BatteryTempTest(ID pid, String s) {
        super(pid, s);
    }

    final private String SYS_BATTERY_PATH = "/sys/devices/platform/msm-battery/power_supply/battery/";

    private synchronized String getBattTemp() {
        String result = null;
        try {
            mBattTempReader = new BufferedReader(new FileReader(
                    SYS_BATTERY_PATH + "temp"), 8);
            result = mBattTempReader.readLine();
            TctLog.i(TAG, "AC online : " + result);
        } catch (IOException e) {
            TctLog.e(TAG, " plug status file can't be accessed " + e);
        } finally {
            try {
                if (mBattTempReader != null)
                    mBattTempReader.close();
            } catch (IOException excep) {
                TctLog.e(TAG, "can't close file" + excep);
            }
        }
        return result;// can be null
    }

    /**
     * Format a number of tenths-units as a decimal string without using a
     * conversion to float. E.g. 347 -> "34.7"
     */
    private final String tenthsToFixedString(int x) {
        int tens = x / 10;
        return Integer.toString(tens) + "." + (x - 10 * tens);
    }

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                mBattTempValue = intent.getIntExtra("temperature", 0);
                Run();
            }
        }
    };

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT: // init the test, shows the first screen
            mIntentFilter = new IntentFilter();
            mIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
            mContext.registerReceiver(mIntentReceiver, mIntentFilter);
            mState++;
            break;

        case INIT + 1:
            String s;
            String v = tenthsToFixedString(mBattTempValue);
            if (mBattTempValue < LOW || mBattTempValue > HIGH) {

                tvbody = new TextView(mContext);
                tvbody.setGravity(Gravity.CENTER);
                // tvbody.setTypeface(Typeface.MONOSPACE, 1);
                tvbody.setTextAppearance(mContext,
                        android.R.style.TextAppearance_Large);

                // means 20~50 degree centigrade.
                s = "FAIL!!\n" + "range is 20~50 C\n\n" + "current is : " + v
                        + " C";
                tvbody.setText(s);
                tvbody.setTextColor(Color.RED);

                tl = new TestLayout1(mContext, mName, tvbody, "PASS", "FAIL"); // for
                                                                               // bgcolor.
                // tl.setAutoTestButtons(true, false);
            } else {
                s = "OK\n\n" + v + " C";
                tl = new TestLayout1(mContext, mName, s);
                SetTimer(500, Cb); // miliseconds
            }

            mContext.setContentView(tl.ll);
            break;

        case END:
            // tl.setAutoTestButtons(false); /*fix venus bug [164259] , disable
            // keys to ensure it won't be process in next test by JiangDong*/
            StopTimer();
            mContext.unregisterReceiver(mIntentReceiver);
            break;

        default:
            break;
        }
    }

    CallBack Cb = new CallBack() {
        public void c() {
            // tl.setAutoTestButtons(true);
        }
    };

}

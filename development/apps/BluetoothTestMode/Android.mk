# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng debug
#[BUGFIX]-Del-BEGIN by TCTNB.dongdong.gong,12/25/2015, 1207510 ,
#It blocks compiling,so remove it.And the mechanism of tct-ext is old.
#LOCAL_JAVA_LIBRARIES := frameworks_ext
#[BUGFIX]-Del-END by TCTNB.dongdong.gong

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := BluetoothTestMode
LOCAL_CERTIFICATE := platform

include $(BUILD_PACKAGE)
